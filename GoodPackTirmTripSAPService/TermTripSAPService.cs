﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.Models;
using GPArchitecture.SAPService;
using GPArchitecture.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GoodPackTirmTripSAPService
{
    public class TermTripSAPService
    {
        internal static ILog log = LogManager.GetLogger(typeof(TermTripSAPService));
        public bool isProcessing = false; 
        public TermTripSAPService()
        {
            log.Info("Entering into TermTripSAPService constructor");
        }
        public void Initialize()
        {
            try
            {
                log.Debug("Entering TermTripSAPServiceInitialize method");
                StartEvent();
                log.Debug("Leaving TermTripSAPService Initialize method");
            }
            catch(Exception e)
            {
                isProcessing = false;
            }
        }

        public void StartEvent()
        {
            this.CheckTermTripUpdated();
        }
        public void CheckTermTripUpdated()
        {
            try
            {
                isProcessing = true;
                log.Debug("CheckTermTripUpdated Entered");
                TermTripResponseService termtripService = new TermTripResponseService();
                
                using (var context = new GoodpackEDIEntities())
                {
                    var statusWaitingId=context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).FirstOrDefault();
                    List<Trn_SCTransLine> listTransaction = context.Trn_SCTransLine.Where(x => x.TermTripSAP == false && x.StatusId == statusWaitingId).ToList();
                    foreach (var translineContent in listTransaction)
                    {
                        try
                        {
                            List<ResponseModel> listResonse = null;
                            listResonse = termtripService.TermTripResponse(translineContent.CustomerRefNumber, translineContent.CustomerCode, translineContent.ItrNumber);
                            foreach (var response in listResonse)
                            {
                                log.Info("CustomerRefNumber-" + translineContent.CustomerRefNumber + ",CustomerCode-" + translineContent.CustomerCode + "ItrNumber-" + translineContent.ItrNumber);
                                log.Info(response.Message);
                                if (response.Success == true)
                                {
                                    Trn_SCTransLine transline = context.Trn_SCTransLine.Where(x => x.Id == translineContent.Id).SingleOrDefault();
                                    transline.StatusId = context.Trn_LineStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();
                                    transline.TermTripSAP = true;
                                    Trn_SCTransLineContent transLineContent = context.Trn_SCTransLineContent.Where(x => x.LineID == transline.Id).SingleOrDefault();
                                    transLineContent.SapResponseMessage = response.Message;
                                    transLineContent.StatusID = context.Trn_LineStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();
                                    context.SaveChanges();
                                    UpdateTransBatch(translineContent.BatchId, true);
                                }
                                else if (response.Success == false)
                                {
                                    Trn_SCTransLine transline = context.Trn_SCTransLine.Where(x => x.Id == translineContent.Id).SingleOrDefault();
                                    transline.StatusId = context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault();
                                    transline.TermTripSAP = true;
                                    Trn_SCTransLineContent transLineContent = context.Trn_SCTransLineContent.Where(x => x.LineID == transline.Id).SingleOrDefault();
                                    transLineContent.SapResponseMessage = response.Message;
                                    transLineContent.StatusID = context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault();
                                    context.SaveChanges();
                                    UpdateTransBatch(translineContent.BatchId, false);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            log.Debug("SCdata CustRef:-" + translineContent.CustomerRefNumber + ",ItrNo:-" + translineContent.ItrNumber);
                            log.Error("SCUpdate error -" + e);
                        }
                    }
                    List<Trn_SSCTransLineNew> listSSCTransaction = context.Trn_SSCTransLineNew.Where(x => x.TermTripSAP == false && x.StatusId == statusWaitingId).ToList();
                    // log.Debug("SSCdata :-" + listSSCTransaction);
                    foreach (var translineContent in listSSCTransaction)
                    {
                        List<ResponseModel> listResonse = null;
                        listResonse = termtripService.TermTripResponse(translineContent.CustomerRefNumber, translineContent.CustomerCode, translineContent.ItrNumber);
                        foreach (var response in listResonse)
                        {
                            try
                            {
                                if (response.Success == true)
                                {
                                    int statusId = context.Trn_LineStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();
                                    Trn_SSCTransLineNew transline = context.Trn_SSCTransLineNew.Where(x => x.Id == translineContent.Id).SingleOrDefault();
                                    transline.StatusId = context.Trn_LineStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();
                                    transline.TermTripSAP = true;

                                    List<Trn_SSCTransLineContentNew> transLineContent = context.Trn_SSCTransLineContentNew.Where(x => x.LineId == transline.Id).ToList();
                                    transLineContent.ForEach(i => { i.SapResponseMessage = response.Message; i.StatusId = statusId; });

                                    //transLineContent.SapResponseMessage = response.Message;
                                    //transLineContent.StatusId = statusId;
                                    //Change by anoop for showing sapstatus and message in bindetails for termtrip

                                    //var lineIds = transLineContent.Select(i => i.ScannedDataLineId).ToList();
                                    //var sscDetail = context.Trn_ScannedShipmentDetails.Where(j => lineIds.Contains(j.Id)).Select(i => new { i.CustomerReferenceNumber, i.BatchId }).ToList();
                                    //var sscDetailIds = sscDetail.Select(i => i.CustomerReferenceNumber).ToList();
                                    //var sscBatchId = sscDetail.Select(i => i.BatchId).FirstOrDefault();
                                    //List<Trn_ScannedShipmentDetails> details = context.Trn_ScannedShipmentDetails.Where(i => sscDetailIds.Contains(i.CustomerReferenceNumber) && i.BatchId == sscBatchId).ToList();
                                    //details.ForEach(i => { i.SAPBinStatus = statusId; i.SAPBinSyncMessage = response.Message; });
                                    //Old code for above
                                    //List<Trn_ScannedShipmentDetails> details = context.Trn_ScannedShipmentDetails.Where(j => lineIds.Contains(j.Id)).ToList();                                    
                                    //details.SAPBinStatus = statusId;
                                    //details.SAPBinSyncMessage = response.Message;

                                    context.SaveChanges();
                                    UpdateSSCTransBatch(translineContent.BatchId, true);
                                }
                                else if (response.Success == false)
                                {
                                    int statusId = context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault();

                                    Trn_SSCTransLineNew transline = context.Trn_SSCTransLineNew.Where(x => x.Id == translineContent.Id).SingleOrDefault();
                                    transline.StatusId = context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault();
                                    transline.TermTripSAP = true;

                                    List<Trn_SSCTransLineContentNew> transLineContent = context.Trn_SSCTransLineContentNew.Where(x => x.LineId == transline.Id).ToList();
                                    transLineContent.ForEach(i => { i.SapResponseMessage = response.Message; i.StatusId = statusId; });

                                    //transLineContent.SapResponseMessage = response.Message;
                                    //transLineContent.StatusId = statusId;
                                    //Change by anoop for showing sapstatus and message in bindetails for termtrip
                                    var lineIds = transLineContent.Select(i => i.ScannedDataLineId).ToList();
                                    var sscDetail = context.Trn_ScannedShipmentDetails.Where(j => lineIds.Contains(j.Id)).Select(i => new { i.CustomerReferenceNumber, i.BatchId }).ToList();
                                    var sscDetailIds = sscDetail.Select(i => i.CustomerReferenceNumber).ToList();
                                    var sscBatchId = sscDetail.Select(i => i.BatchId).FirstOrDefault();
                                    List<Trn_ScannedShipmentDetails> details = context.Trn_ScannedShipmentDetails.Where(i => sscDetailIds.Contains(i.CustomerReferenceNumber) && i.BatchId == sscBatchId).ToList();
                                    details.ForEach(i => { i.SAPBinStatus = statusId; i.SAPBinSyncMessage = response.Message; });

                                    //Old code for above
                                    //var lineIds = transLineContent.Select(i => i.ScannedDataLineId).ToList();
                                    //List<Trn_ScannedShipmentDetails> details = context.Trn_ScannedShipmentDetails.Where(j => lineIds.Contains(j.Id)).ToList();
                                    //details.ForEach(i => { i.SAPBinStatus = statusId; i.SAPBinSyncMessage = response.Message; });
                                    //details.SAPBinStatus = statusId;
                                    //details.SAPBinSyncMessage = response.Message;

                                    context.SaveChanges();
                                    UpdateSSCTransBatch(translineContent.BatchId, false);
                                }
                            }
                            catch (Exception e)
                            {
                                log.Debug("SSCdata CustRef:-" + translineContent.CustomerRefNumber + ",ItrNo:-" + translineContent.ItrNumber + "Sap resp:-" + listResonse);
                                log.Error("SSCTransaction error -" + e);
                            }
                        }
                    }
                }
                log.Debug("CheckTermTripUpdated Left");
                isProcessing = false;
            }
            catch(Exception e)
            {
                log.Debug("CheckTermTripUpdated error -" + e);
                isProcessing = false;
            }
        }
        private void UpdateTransBatch(int batchId,bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int totalLine = context.Trn_SCTransLine.Where(x => x.BatchId == batchId).Count();                
                Trn_SCTransBatch transbatch = context.Trn_SCTransBatch.Where(x => x.Id == batchId).SingleOrDefault();
                int successLine = context.Trn_SCTransLine.Where(x => x.BatchId == batchId &&  x.Trn_LineStatus.StatusCode == "SUCCESS").Count();
                int errorLine = context.Trn_SCTransLine.Where(x => x.BatchId == batchId && x.Trn_LineStatus.StatusCode == "ERROR").Count();
                if ((successLine + errorLine) == totalLine)
                {
                    transbatch.TermTripSAPResponseReceived = true;                    
                    transbatch.StatusId = context.Ref_BatchStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();                    
                }                
                transbatch.RecordCountSuccess = successLine;
                transbatch.RecordCountSAPError = errorLine;
                context.SaveChanges();
            }
          //  EmailSend(batchId, 2);
        }

        private void UpdateSSCTransBatch(int batchId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int totalLine = context.Trn_SSCTransLineNew.Where(x => x.BatchId == batchId).Count();
                Trn_SSCTransBatch transbatch = context.Trn_SSCTransBatch.Where(x => x.Id == batchId).SingleOrDefault();
                int successLine = context.Trn_SSCTransLineNew.Where(x => x.BatchId == batchId  && x.Trn_LineStatus.StatusCode == "SUCCESS").Count();
                int errorLine = context.Trn_SSCTransLineNew.Where(x => x.BatchId == batchId  && x.Trn_LineStatus.StatusCode == "ERROR").Count();
                if ((successLine + errorLine) == totalLine)
                {
                    transbatch.TermTripSAPResponseReceived = true;
                    transbatch.StatusId = context.Ref_BatchStatus.Where(x => x.StatusCode == "SUCCESS").Select(x => x.Id).SingleOrDefault();
                }
                transbatch.RecordCountSuccess = successLine;
                transbatch.RecordCountSAPError = errorLine;
                context.SaveChanges();
            }
           // EmailSend(batchId,1);
        }
        private string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();
            //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
        //type 1-SSC ,2-SC
        private void EmailSend(int batchId,int type)
        {
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    if (type == 1)
                    {
                        Trn_SSCTransBatch transBatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                        var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                        where s.BatchId == transBatch.Id
                                        select s.DeviceId).FirstOrDefault();
                        var data = (from s in context.Trn_DeviceDetails
                                    where s.DeviceID == deviceId
                                    select new { s.CustomerCode, s.CustomerEmail, s.SubsiId, s.MapperName, s.UserId }).FirstOrDefault();
                        //var cusromerName = (from s in context.Gen_EDIEntities
                        //                    where s.EntityCode == data.CustomerCode
                        //                    select s.EntityName).FirstOrDefault();
                        var userName = (from s in context.Gen_User
                                        where s.Id == data.UserId
                                        select s.FullName).FirstOrDefault();
                        string[] customerEmail = new string[] { data.CustomerEmail };
                        Dictionary<string, string> placeholders = new Dictionary<string, string>();
                        placeholders.Add("$$CustomerName$$", userName);
                        placeholders.Add("$$BatchID$$", batchId.ToString());
                        placeholders.Add("$$UserName$$", userName);
                        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                        placeholders.Add("$$EmailID$$", data.CustomerEmail);
                        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(int.Parse(data.SubsiId)));

                        GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", null);
                        GPTools.SendEmail(placeholders, getAllRecipients(data.MapperName), "NOTIFY_SC_EMAIL", null);
                    }
                    else if(type==2)
                    {
                        Trn_SCTransBatch transBatch = context.Trn_SCTransBatch.FirstOrDefault(i => i.Id == batchId);                 
                        var userName = (from s in context.Gen_User
                                        where s.Id == transBatch.UserId 
                                        select s.FullName).FirstOrDefault();
                        string[] customerEmail = new string[] { transBatch.EmailId };
                        Dictionary<string, string> placeholders = new Dictionary<string, string>();
                        placeholders.Add("$$CustomerName$$", userName);
                        placeholders.Add("$$BatchID$$", batchId.ToString());
                        placeholders.Add("$$UserName$$", userName);
                        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                        placeholders.Add("$$EmailID$$", transBatch.EmailId);
                        placeholders.Add("$$FileName$$", transBatch.FileName);
                        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(transBatch.StatusId));
                        GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", null);
                        GPTools.SendEmail(placeholders, getAllRecipients(transBatch.MapperFileName), "NOTIFY_SC_EMAIL", null);
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("Email Send error" + e);
            }
        }
        private string getSubsyNameFromId(int SubsiId)
        {
            string SubsiName;
            using (var context = new GoodpackEDIEntities())
            {
                SubsiName = (from s in context.Gen_EDISubsies
                             where s.Id == SubsiId
                             select s.SubsiName).FirstOrDefault();
            }
            return SubsiName;


        }
    }
}
