﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace GoodPackTirmTripSAPService
{
    public partial class TermTripUpdaterService : ServiceBase
    {
        internal static ILog log = LogManager.GetLogger(typeof(TermTripUpdaterService));
        public System.Timers.Timer termTripUpdaterTimer;
        private static int NoofAttempts = 0;
        TermTripSAPService termTripService = new TermTripSAPService();
        public TermTripUpdaterService()
        {
            InitializeComponent();
            log.Info("Entering into constructor");
            InitializeComponent();
            log.Info("Leaving from constructor");
        }
        public void debugger()
        {
            log.Info("Entering into Start method");
            OnStart(null);
            log.Info("Leaving Start method");
        }
        protected override void OnStart(string[] args)
        {
            log.Debug("Entering TermTripUpdater OnStart");
            this.termTripUpdaterTimer = new System.Timers.Timer();            
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            this.termTripUpdaterTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.termTripUpdaterTimer.Elapsed += termTripUpdaterTimer_Elapsed;
            this.termTripUpdaterTimer.Enabled = true;
            this.termTripUpdaterTimer.Start();
            log.Debug("Leaving TermTripUpdater Service OnStart");
            //while (true) { }
        }

        void termTripUpdaterTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            log.Debug("Entering termTripUpdaterTimer_Elapsed event");
            if (!termTripService.isProcessing)
                termTripService.Initialize();
            log.Debug("Leaving termTripUpdaterTimer_Elapsed event");
        }

        protected override void OnStop()
        {
            this.termTripUpdaterTimer.Stop();
            while (termTripService.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    log.Warn("TermTrip Services Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
