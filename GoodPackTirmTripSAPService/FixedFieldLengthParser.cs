﻿using GoodpackEDI.BusinessLayer.SCManagementLayer;
using GoodpackEDI.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace GoodpackEDI.Parsers
{
    public class FixedFieldLengthParser
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(FixedFieldLengthParser));
        public static List<SourceMessage> Parse(SourceMessage msgSpecs, string message, ref bool isSuccess, ref string errorDesc)
        {
            // Init vars
            string[] msgLines;
            isSuccess = true;
            int lineCount = 0;
            List<SourceMessage> parsedResult = new List<SourceMessage>();

            // Split messages lines into a string array
            msgLines = Regex.Split(message, Environment.NewLine);

            try
            {
                // Loop through each lines in customer message
                foreach (string msgLine in msgLines)
                {
                    // if isSuccess is set to false, then break processing
                    if (!isSuccess)
                        break;

                    // Increment line count
                    lineCount++;

                    // If line length is < 2, then ignore and skip
                    if (msgLine.Length < 2)
                        continue;

                    // Init vars
                    SourceMessage msgLineObj = new SourceMessage(msgSpecs);
                    msgLineObj.OriginalLineContent = msgLine;
                    int offset = 0;

                    // Loop through each field item in msgLine
                    foreach (KeyValuePair<int, SourceMessageField> lineFieldItem in msgLineObj.Fields)
                    {
                        SourceMessageField lineField = lineFieldItem.Value;

                        // Check if the message line length is long enough to accommodate the substring
                        if (msgLine.Length < offset + lineField.Length)
                        {
                            isSuccess = false;
                            errorDesc = string.Format(
                                ResourceMessages.REQUEST_MESSAGE_LINE_TOO_SHORT,
                                lineCount.ToString()
                                );
                            break;
                        }

                        // Set value of field based on offset and length
                        lineField.Value = msgLine.Substring(offset, lineField.Length).Trim();

                        // DISABLED: Update value of offset. If offset is less than one, it means that the first field
                        // is being processed.  Need to decrement initial offset before setting value of
                        // offset for next field.
                        //if (offset < 1)
                        //    offset--;
                        offset += lineField.Length;
                    }

                    // Add message line object to list
                    parsedResult.Add(msgLineObj);

                }
            }
            catch (System.Exception ex)
            {
                log.Error(" Parse in FixedFieldLengthParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ".  Exception : Message- " + ex.Message);
                throw ex;
            }

            return parsedResult;
        }
    }
}