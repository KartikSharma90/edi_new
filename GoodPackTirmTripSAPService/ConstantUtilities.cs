﻿
namespace GoodpackEDI.Utilities
{
    public static class ConstantUtilities
    {
       
        public static string RegSuccessfullMessage = "Successfully Registered";
        public static string RegUserAlreadyRegisteredMessage = "User Already Registered";      
        public static string SAPUsername = System.Configuration.ConfigurationManager.AppSettings["SAPUsername"].ToString();
        public static string SAPPassword = System.Configuration.ConfigurationManager.AppSettings["SAPPassword"].ToString();
        public static string ADDomain = "CUSTPORTAL";  
        public static string Admin="Admin";
        public static string Status = "completed";
        public const string SemiColonDelimited = "SemiColon Delimited";
        public const string Excel = "Excel";
        public const string CSV = "CSV";       
        public const string Semi_Colon = ";";
        public const string Comma = ",";
        public const string Fixed = "FIXED";
        public const string Service = "GPMapper";
        public const string LookupName_CustomerCode = "Customer Code";
        public static string[] delimiters = new string[] { "|" };
        public static string Quantity = "1";
        public static string DateFormat = "dd.MM.yyyy";
        public const string Packer = "Packer";
        public const string Consignee = "Consignee";
        public const string OrderQuantity = "Order quantity";
        public const string SIQuantity = "Quantity";
        public const string CustomerCode = "Customer Code";
        public const string POL = "Port";
        public const string POD = "Port";
        public const string Port = "Port";
        public const string ShippingLine="Shipping Line";
        public const string GoodyearSOMapper = "GoodYearSalesOrder";
        public const string GoodyearSCMapper = "GoodYearShipmentConfirmation";
        public const string SalesDocumentItem = "10";
        public const string ContinentalSOMapper = "ContinentalSOMapper";
       
    
    }
}