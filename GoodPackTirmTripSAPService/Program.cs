﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;


namespace GoodPackTirmTripSAPService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        /// 
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Entered in Program.cs");
#if DEBUG
            TermTripUpdaterService service = new TermTripUpdaterService();
            service.debugger();
# else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
               new TermTripUpdaterService() 
            };
            ServiceBase.Run(ServicesToRun);
# endif
        }
    }
}
