﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class ResourceMessages
    {
        public static string FIELD_COUNT_TOO_LOW = "The number of fields derived from line #{0} in the customer message does not meet the number of fields defined in the message specification";
        public static string CSV_PARSING_ERROR = "There was an error parsing line #{0} of the customer request message.";
        public static string CSV_FILE_INCONSISTENT = "The customer request message has an inconsistent format.  The number of fields in each line is not consistent.";
        public static string GENERAL_PARSING_ISSUE = "The message cannot be parsed due to an issue with the structure of the message.";
        public static string REQUEST_MESSAGE_LINE_TOO_SHORT = "The length of one of the lines from the customer message does not meet the message specifications defined for this customer.  Error occurred while processing message line #{0}.";
    }
}