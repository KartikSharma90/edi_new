﻿

using GPArchitecture.DataLayer.Models;
using System.Linq;

namespace GPArchitecture.Accessors
{
    public class EntityMaster
    {
        /// <summary>
        /// Performs a matching analysis on the given email address based on the data available in
        /// the Trn_MapperNameToEmailIdMapper table.  Returns the Mapper name if a match is found.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static string DeriveMapperNameFromEmailSenderAddress(string emailAddress,string transactionType)
        {
            // Init vars
            string mapperName = null;
           
            emailAddress = emailAddress.ToLower();            
           
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    int transactionId = (from s in context.Ref_TransactionTypes
                                         where s.TransactionCode == transactionType
                                         select s.Id).FirstOrDefault();

                     mapperName = (from s in context.Trn_MapperNameToEmailIdMapper 
                                         join p in context.Trn_SubsiToTransactionFileMapper
                                         on s.MapperId equals p.Id
                                         where ((s.EmailId == emailAddress)&&(s.TransactionId==transactionId))
                                         select p.Filename).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return mapperName;
        }

        /// <summary>
        ///  Returns the subsiId if a match is found.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static int DeriveSubsiIdFromSubsiName(string SubsiName)
        {
            int SubsiId;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    SubsiId = (from s in context.Gen_EDISubsies
                               where s.SubsiName == SubsiName
                               select s.Id).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return SubsiId;
        }
        
         public static int DeriveUserIdFromMapperName(string mapperFileName)
        {
            using (var context=new GoodpackEDIEntities())
            {
                return (from s in context.Trn_SubsiToTransactionFileMapper
                        where s.Filename == mapperFileName
                        select s.LastUpdatedBy).FirstOrDefault();
            }
        }

        /// <summary>
        /// Performs a matching analysis on the given email address based on the data available in
        /// the Trn_SubsiToTransactionFileMapper table.  Returns the subsiId if a match is found.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static int DeriveSubsiIdFromMapperName(string mapperName)
        {
            // Init vars
            int subsiId = 0;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                                  where s.Filename == mapperName
                                  select s.EDISubsiId).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return subsiId;
        }


        /// <summary>
        /// Performs a matching analysis on the given email address based on the data available in
        /// the Trn_SubsiToTransactionFileMapper table.  Returns the subsiId if a match is found.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static string DeriveUsernameFromSubsiId(int subsiId)
        {
            // Init vars
            string username;
          //  int userId;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    username = (from s in context.Trn_SubsiToTransactionFileMapper
                                join p in context.Gen_User
                                on s.LastUpdatedBy equals p.Id
                                where s.EDISubsiId == subsiId
                                select p.Username).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return username;
        }
        public static string DeriveUsernameFromMapperName(string mapperName)
        {
            // Init vars
            string username;
            int userId;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    userId = (from s in context.Trn_SubsiToTransactionFileMapper
                              where s.Filename == mapperName
                              select s.LastUpdatedBy).FirstOrDefault();
                    username = (from u in context.Gen_User where u.Id == userId select u.Username).FirstOrDefault();
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return username;
        }
        ///// <summary>
        /////  Returns the subsiId if a match is found.
        ///// </summary>
        ///// <param name="emailAddress"></param>
        ///// <returns></returns>
        //public static int DeriveSubsiIdFromSubsiName(string SubsiName)
        //{
        //    int SubsiId;
        //    try
        //    {
        //        using (var context = new GoodpackEDIEntities())
        //        {
        //            SubsiId = (from s in context.Gen_EDISubsies
        //                       where s.SubsiName == SubsiName
        //                       select s.Id).FirstOrDefault();
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return SubsiId;
        //}
    }
}
