﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.CommonTypes
{
    public class Map
    {
        private int intId;
        private List<MapItem> objLstMapItems = new List<MapItem>();
        public int ID
        {
            set { intId = value; }
            get { return intId; }
        }
        public List<MapItem> MapItems
        {
            set { objLstMapItems = value; }
            get { return objLstMapItems; }
        }
    }
    public class MapItem
    {
        private int intSourceFieldSeq;
        private int intTargetFieldSeq;
        private string strSourceDateFormat;
        private string strFixedValue;
        private int intsourceFieldSeq2;
        private int intsourceFieldSeq3;
        private string strMappingType;
        private int? intSourceFieldSeq1SubStringStartPosition;
        private int? intSourceFieldSeq1SubStringLength;

        public int SourceFieldSeq
        {
            set { intSourceFieldSeq = value; }
            get { return intSourceFieldSeq; }
        }
        public int TargetFieldSeq
        {
            set { intTargetFieldSeq = value; }
            get { return intTargetFieldSeq; }
        }
        public string SourceDateFormat
        {
            set { strSourceDateFormat = value; }
            get { return strSourceDateFormat; }
        }
        public string FixedValue
        {
            set { strFixedValue = value; }
            get { return strFixedValue; }
        }
        public int SourceFieldSeq2
        {
            set { intsourceFieldSeq2 = value; }
            get { return intsourceFieldSeq2; }
        }
        public int SourceFieldSeq3
        {
            set { intsourceFieldSeq3 = value; }
            get { return intsourceFieldSeq3; }
        }
        public int? SourceFieldSeq1SubStringStartPosition
        {
            set { intSourceFieldSeq1SubStringStartPosition = value; }
            get { return intSourceFieldSeq1SubStringStartPosition; }
        }

        public int? SourceFieldSeq1SubStringLength
        {
            set { intSourceFieldSeq1SubStringLength = value; }
            get { return intSourceFieldSeq1SubStringLength; }
        }

        public string MappingType
        {
            set { strMappingType = value; }
            get { return strMappingType; }
        }
    }
}