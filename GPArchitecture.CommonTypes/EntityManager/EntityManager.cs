﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.CommonTypes
{
    public class EntityManager : IEntityManager
    {

        public IList<EntityModel> GetAllEntitiesforUser(string username)
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {

                Gen_User user = (from s in dataContext.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                string role = (from s in dataContext.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();
                if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                {

                    var x = dataContext.Trn_CustomerToPackerMapper
            .Where(a => dataContext.Trn_UserToSubsiMapper
                            .Where(b => b.UserId.Equals(user.Id))
                            .Select(c => c.SubsiId)
                            .Contains(a.SubsiCode))
            .Select(d => d.CustomerCode).Distinct().ToList();


                    return (from s in dataContext.Gen_EDIEntities
                            where x.Contains(s.EntityCode)
                            orderby s.EntityCode ascending
                            select new EntityModel
                            {
                                EntityCode = s.EntityCode,
                                EntityName = s.SearchTerm + "(" + SqlFunctions.StringConvert((double)s.EntityCode) + ")"
                            }).ToList();
                }
                else
                {

                    var x = dataContext.Trn_CustomerToPackerMapper
                           .Select(d => d.CustomerCode).Distinct().ToList();

                    return (from s in dataContext.Gen_EDIEntities
                            where x.Contains(s.EntityCode)
                            orderby s.EntityCode ascending
                            select new EntityModel
                            {
                                EntityCode = s.EntityCode,
                                EntityName = s.SearchTerm + "(" + SqlFunctions.StringConvert((double)s.EntityCode) + ")"
                            }).ToList();

                }

            }

            //throw new NotImplementedException();
        }

        public IList<EntityModel> GetAllPackerforCustomer(int customerCode)
        {

            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                var customerList = (from p in dataContext.Trn_CustomerToPackerMapper
                                    where p.CustomerCode == customerCode
                                    select p.PackerCode).ToList();

                return (from s in dataContext.Gen_EDIEntities
                        where customerList.Contains(s.EntityCode)
                        select new EntityModel
                        {
                            EntityCode = s.EntityCode,
                            EntityName = s.SearchTerm + "(" + SqlFunctions.StringConvert((double)s.EntityCode) + ")"
                        }).ToList();

            }
        }

        public IList<EntityModel> GetAllEntityforSubsy(int subsyCode)
        {

            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                var entityList = (from p in dataContext.Gen_EDIEntities
                                  where p.SubsiId == subsyCode
                                  select new EntityModel
                                  {
                                      EntityCode = p.EntityCode,
                                      EntityName = p.EntityName
                                  }).ToList();

                return entityList;

            }
        }

        public IList<EntityModel> GetAllCustomersforUser(string username)
        {

            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {

                Gen_User user = (from s in dataContext.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                string role = (from s in dataContext.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();

                if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                {
                    var x = dataContext.Gen_EDIEntities
                    .Where(a => dataContext.Trn_UserToSubsiMapper
                                           .Where(b => b.UserId.Equals(user.Id))
                                           .Select(c => c.SubsiId)
                                           .Contains(a.SubsiId)).Where(c=>c.EntityTypeId == 1  );
                      
                    var y = x.Select(d => new EntityModel
                    {
                        EntityCode = d.EntityCode,
                        EntityName = d.SearchTerm + "(" + SqlFunctions.StringConvert((double)d.EntityCode) + ")"
                    } ).ToList();
                    

                    return y;
                }
                else
                {



                    return (from p in dataContext.Gen_EDIEntities.Where(a=>a.EntityTypeId==1)
                            orderby p.EntityCode ascending
                            select new EntityModel
                            {
                                EntityCode = p.EntityCode,
                                EntityName = p.SearchTerm + "(" + SqlFunctions.StringConvert((double)p.EntityCode) + ")"

                            }).ToList();


                    //var x = dataContext.Gen_EDIEntities
                    //       .Where(a => dataContext.Trn_UserToSubsiMapper
                    //        .Where(b => b.UserId.Equals(user.Id))
                    //        .Select(c => c.SubsiId)
                    //          .Contains(a.SubsiId)
                    //          && a.EntityTypeId == 1
                    //        );

                    //var y = x.Select(d => new EntityModel
                    //{
                    //    EntityCode = d.EntityCode,
                    //    EntityName = d.SearchTerm + "(" + SqlFunctions.StringConvert((double)d.EntityCode) + ")"
                    //}).ToList();

                    //return y;


                }
            }

        }

        public string GetSubsiforEntity(int entityCode)
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                var subsiId = (from p in dataContext.Gen_EDIEntities
                               where p.EntityCode == entityCode
                               select p.SubsiId).First();

                var subsiCode = (from p in dataContext.Gen_EDISubsies
                                 where p.Id == subsiId
                                 select p.SubsiCode).First();

                return subsiCode.ToString();

            }

        }
        public IList<MapperModel> GetAllMapperforSelectedSubsy(int subsyCode, string username)
        {

            using (var context = new GoodpackEDIEntities())
            {

                return (from s in context.Trn_SubsiToTransactionFileMapper
                        where s.EDISubsiId == subsyCode && s.IsEnabled == true
                        select new MapperModel
                        {
                            MapperCode = s.Filename,
                            MapperName = s.Filename,

                        }).ToList();

            }
        }


        public IList<EntityModel> GetAllLookupNames()
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                return (from p in dataContext.Ref_LookupNames
                        select new EntityModel
                       {
                           EntityCode = p.Id,
                           EntityName = p.LookupName,

                       }).ToList();
            }
        }

        public IList<ReasonModel> GetAllReasonsForEdit()
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                return (from p in dataContext.Ref_SOEditReasonCodes 
                        select new ReasonModel
                        {
                            ReasonType = p.Type,
                            ReasonDescription = p.Description,
                            ReasonCode=p.Id

                        }).ToList();
            }

        }

        public IList<ReasonModel> GetAllReasonsForCancel()
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                string type="Cancel";
                return (from p in dataContext.Ref_SOEditReasonCodes where p.Type==type
                        select new ReasonModel
                        {
                            ReasonType = type,
                            ReasonDescription = p.Description,
                            ReasonCode = p.Id

                        }).ToList();
            }

        }
        public IList<EntityModel> GetPrivilegeforEdit(string username)
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                int roleId=(from p in dataContext.Gen_User where p.Username==username select p.RoleId).FirstOrDefault();
                List<int> privilegeId = (from p in dataContext.Trn_RoleToPrivilegeMapper where p.RoleId == roleId select p.PrivilegeId).ToList();
                EntityModel model = new EntityModel();
                List<EntityModel> modelList = new List<EntityModel>();
                for (int i = 0; i < privilegeId.Count; i++)
                {
                    int pId = privilegeId[i];
                    model.EntityCode = (from p in dataContext.Ref_Privilege where p.Id == pId select p.Id).FirstOrDefault();
                    model.EntityName = (from p in dataContext.Ref_Privilege where p.Id == pId select p.PrivilegeName).FirstOrDefault();
                    modelList.Add(model);

                }
                return modelList;
               
            }

        }

        public bool GetisMapperASN(string mapperName)
        {
            bool returnValue = false;
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                var count=(dataContext.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename == mapperName && i.Ref_TransactionTypes.TransactionCode == "ASN").Select(i => i.Id)).Union
                    (dataContext.Trn_DeviceDetails.Where(i => i.TransactionTypes == "ASN" && i.MapperName == mapperName).Select(i => i.Id)).Count();
                returnValue = count > 0 ? true : false;
            }
            return returnValue;
        }

        public IList<EntityModel> GetCustomerPackerEntitiesforUser(string lookUpName,string userName)
        {
            using (GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                IList<EntityModel> returnModel = null;
                if (lookUpName.ToLower().IndexOf("customer code") > -1)
                {
                    returnModel = (from s in dataContext.Gen_EDIEntities
                                   where s.EntityCode > 300000 && s.EntityCode < 400000
                                   select new EntityModel
                                   {
                                       EntityCode = s.EntityCode,
                                       EntityName = s.EntityName+ "(" + SqlFunctions.StringConvert((double)s.EntityCode) + ")",

                                   }).ToList();
                }
                else if (lookUpName.ToLower() == "packer")
                {
                    returnModel = (from s in dataContext.Gen_EDIEntities
                                   where s.EntityCode > 500000 && s.EntityCode < 600000
                                   select new EntityModel
                                   {
                                       EntityCode = s.EntityCode,
                                       EntityName = s.EntityName + "(" + SqlFunctions.StringConvert((double)s.EntityCode) + ")",

                                   }).ToList();
                }
                return returnModel;
            }
            
        }
    }
}