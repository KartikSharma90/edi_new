﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace GPArchitecture.CommonTypes
{
    public enum TRANSACTION_TYPES
    {
        SO = 1, SC = 2, FC = 3, SSC = 4, ACI = 5, BP = 6, PSC = 8, PSSC = 9, BFT = 11
    }
    public enum ErrorCodes
    {
        NONE = 0,
        /// <summary>
        /// Customer request message failed file name or file format validation
        /// </summary>
        V_FILE = 1,

        V_SRC_SPEC = 2,
        /// <summary>
        /// Parsing of the customer request message failed.
        /// </summary>
        V_PARSE_REQ = 3,
        /// <summary>
        /// Field validation failure after mapping.
        /// </summary>
        V_FIELD = 4,
        /// <summary>
        /// Customer is either disabled in master data.
        /// </summary>
        V_CUST = 5,
        /// <summary>
        /// Customer contract is inactive.
        /// </summary>
        V_CONTRCT = 6,

        /// <summary>
        /// Customer request message failed during disassembly
        /// </summary>
        V_DASM = 7,
        /// <summary>
        /// Mapping failed.
        /// </summary>
        E_MAP = 8,
        /// <summary>
        /// Batch processing encountered general error.
        /// </summary>
        E_GEN = 9,
        /// <summary>
        /// Error encountered while submitting message to SAP.
        /// </summary>
        E_TO_SAP = 10,
        /// <summary>
        /// Batch processing in SAP encountered an error.
        /// </summary>
        E_SAP = 11,
        /// <summary>
        /// There was an error retrieving the source message specs.
        /// May mean that customer name in the request message is
        /// incorrect or the mapping for the customer+transaction
        /// hasn’t been created yet.
        /// </summary>
        E_SRC_SPEC = 12,
        /// <summary>
        /// There was an error encountered during archiving.
        /// </summary>
        E_ARCHIVE = 13,
        /// <summary>
        /// A partial error was encountered during batch processing in SAP.
        /// </summary>
        E_SAP_PARTIAL = 14,
        /// <summary>
        /// Validation error with ACI message
        /// </summary>
        V_ACI = 15,
        /// <summary>
        /// Validation error for SO's which use skiponvalidationfailure
        /// </summary>
        V_SKIPRECORDS = 16,
        /// <summary>
        /// No pre-processing action was found for customer head company and transaction
        /// </summary>
        V_NO_PREPROCESSING = 17,
        /// <summary>
        /// validation for SSC transaction where lookup failed for bin pairing
        /// </summary>
        V_FAIL_LOOKUP_BP = 18,
        /// <summary>
        /// GYO Chemical SC input file is not in expected Excel format
        /// </summary>
        V_GYO_CHEM_SC = 19
    }

    public enum SOURCE_TYPES
    {
        EMAIL = 1, FILE_SYSTEM = 2, WEB_SERVICE = 3
    }
    public enum FILE_TYPES
    {
        CSV = 1, TEXT = 2, EXCEL = 3
    }
    public enum TargetMessageInputTypes
    {
        MAP_FROM_SOURCE = 1,
        FIXED = 2,
        USER_FIXED = 3,
        DATE = 4,
        OPERATION_ON_SOURCE_FIELD = 5,
        OPERATION_NO_INPUT = 6,
        MAP_FROM_SOURCE_OR_USER_FIXED_INPUT = 7,
        OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED = 8,
        NONE = 9,
        OPERATION_ON_SOURCE_FIELDS2 = 10,
        MAP_ETD_FROM_SOURCE = 11,
        MAP_FROM_SOURCE_WITH_SUBSTRING = 12,
        OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING = 13,
        DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD = 14

    }
    public enum TargetMessageFieldTypes
    {
        ANY = 0,
        NUMERIC = 1,
        YYYYMMDD = 2,
        DDMMYYYY = 3,
        WWYYYY = 4,
        MAPFROMSOURCE = 5
    };

    public enum TargetMessageFormat
    {
        TAB_DELIMITED = 0
    }
    public enum BatchStatus
    {
        RCVD = 1, PROCSD = 2, IN_SAP = 3, FOR_MANUAL = 4, CLOSED = 5, ERROR = 6, CLOSED_ERROR = 7, CLOSED_FORCED = 8, PARTIAL_SUCCESS = 9, SUCCESS = 10
    }
    public enum LineStatus
    {
        NEW = 0, SUBMITTED = 1, SUCCESS = 2, REJECT = 3, ERROR = 4, INFORMATION = 5, IN_SAP = 6, IN_SAP_ERROR = 9
    }

    public enum NotificationActions
    {
        NO_ACTION = 0, EMAIL_ADMIN = 1, EMAIL_CUST = 2, EMAIL_ADMIN_CUST = 3
    }

    public class MessageFormats
    {
        public const string CSV = "CSV";
        public const string FixedFieldLength = "Fixed_Field_Length";
        public const string TabDelimited = "Tab_Delimited";
        public const string SemiColonDelimited = "SemiColon Delimited";
        public const string Excel = "Excel";
    }
    public class EnumConverTo
    {
        public static Object GenericEnumConverter(Type ActualEnumType, string strEnunValue)
        {
            Object objGenericEnum = null;
            foreach (FieldInfo objFieldInfo in ActualEnumType.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                if (objFieldInfo.Name == strEnunValue || objFieldInfo.GetRawConstantValue().ToString() == strEnunValue)
                {
                    objGenericEnum = Enum.Parse(ActualEnumType, objFieldInfo.Name);
                    break;
                }
            }
            return objGenericEnum;
        }

        public static TRANSACTION_TYPES TranType(string strTranType)
        {
            TRANSACTION_TYPES enumTRAN_TYPE = TRANSACTION_TYPES.SO;
            switch (strTranType)
            {
                case "SO":
                    enumTRAN_TYPE = TRANSACTION_TYPES.SO;
                    break;
                case "SC":
                    enumTRAN_TYPE = TRANSACTION_TYPES.SC;
                    break;
                case "FC":
                    enumTRAN_TYPE = TRANSACTION_TYPES.FC;
                    break;
                case "SSC":
                    enumTRAN_TYPE = TRANSACTION_TYPES.SSC;
                    break;
                case "ACI":
                    enumTRAN_TYPE = TRANSACTION_TYPES.ACI;
                    break;
                case "BP":
                    enumTRAN_TYPE = TRANSACTION_TYPES.BP;
                    break;
                case "PSC":
                    enumTRAN_TYPE = TRANSACTION_TYPES.PSC;
                    break;
                case "PSSC":
                    enumTRAN_TYPE = TRANSACTION_TYPES.PSSC;
                    break;
                case "BFT":
                    enumTRAN_TYPE = TRANSACTION_TYPES.BFT;
                    break;
            }
            return enumTRAN_TYPE;
        }

        public static string TranType(TRANSACTION_TYPES transType)
        {
            string returnVal = string.Empty;
            switch (transType)
            {
                case TRANSACTION_TYPES.FC:
                    returnVal = "Demand Forecast";
                    break;
                case TRANSACTION_TYPES.SC:
                    returnVal = "Shipment Confirmation";
                    break;
                case TRANSACTION_TYPES.SO:
                    returnVal = "Sales Order";
                    break;
                case TRANSACTION_TYPES.SSC:
                    returnVal = "Scanned Shipment Confirmation";
                    break;
                case TRANSACTION_TYPES.ACI:
                    returnVal = "Auto Collection ITR";
                    break;
                case TRANSACTION_TYPES.BP:
                    returnVal = "Bin Pairing";
                    break;
                case TRANSACTION_TYPES.PSC:
                    returnVal = "Packer Shipment Confirmation";
                    break;
                case TRANSACTION_TYPES.PSSC:
                    returnVal = "Packer Scanned Shipment Confirmation";
                    break;
                case TRANSACTION_TYPES.BFT:
                    returnVal = "Brazil File Translation";
                    break;

            }
            return returnVal;
        }
        public static TargetMessageInputTypes TargetMessageInputs(string strTargetMessageInputs)
        {
            TargetMessageInputTypes enumTargetMessageInputs = TargetMessageInputTypes.MAP_FROM_SOURCE;
            switch (strTargetMessageInputs)
            {
                case "MAP_FROM_SOURCE":
                    enumTargetMessageInputs = TargetMessageInputTypes.MAP_FROM_SOURCE;
                    break;
                case "FIXED":
                    enumTargetMessageInputs = TargetMessageInputTypes.FIXED;
                    break;
                case "USER_FIXED":
                    enumTargetMessageInputs = TargetMessageInputTypes.USER_FIXED;
                    break;
                case "DATE":
                    enumTargetMessageInputs = TargetMessageInputTypes.DATE;
                    break;
                case "OPERATION_ON_SOURCE_FIELD":
                    enumTargetMessageInputs = TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD;
                    break;
                case "OPERATION_NO_INPUT":
                    enumTargetMessageInputs = TargetMessageInputTypes.OPERATION_NO_INPUT;
                    break;
                case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
                    enumTargetMessageInputs = TargetMessageInputTypes.MAP_FROM_SOURCE_OR_USER_FIXED_INPUT;
                    break;
                case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                    enumTargetMessageInputs = TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED;
                    break;
                case "NONE":
                    enumTargetMessageInputs = TargetMessageInputTypes.NONE;
                    break;
                case "OPERATION_ON_SOURCE_FIELDS2":
                    enumTargetMessageInputs = TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELDS2;
                    break;
                case "MAP_ETD_FROM_SOURCE":
                    enumTargetMessageInputs = TargetMessageInputTypes.MAP_ETD_FROM_SOURCE;
                    break;
                case "MAP_FROM_SOURCE_WITH_SUBSTRING":
                    enumTargetMessageInputs = TargetMessageInputTypes.MAP_FROM_SOURCE_WITH_SUBSTRING;
                    break;
                case "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING":
                    enumTargetMessageInputs = TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING;
                    break;
                case "DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD":
                    enumTargetMessageInputs = TargetMessageInputTypes.DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD;
                    break;

            }
            return enumTargetMessageInputs;
        }
    }
}