﻿using GPArchitecture.EnumsAndConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.CommonTypes
{
    public class MsgSapBFTLine
    {
        // Field list
        StringBuilder message;

        // Fields
        string ITRnumber;
        string BLnumber;
        string DateofASNsent;
        string DateofcopyBLsent;
        string DateofCopyBLPackingListsenttoCustomer;
        string DateofOrignalBLreceivedbyGPBR;
        string DateofOrignalInvoiceANDPackingListreceivedbyGPBR;
        string ATA;

        public MsgSapBFTLine()
        {
        }

        public MsgSapBFTLine(IList<string> fieldListSrc)
        {
            if (fieldListSrc.Count < 8)
            {
                throw new Exception("The SAP BFT line does not have enough fields.");
            }

            ITRnumber = fieldListSrc[0];
            BLnumber = fieldListSrc[1];
            DateofASNsent = fieldListSrc[2];
            DateofcopyBLsent = fieldListSrc[3];
            DateofCopyBLPackingListsenttoCustomer = fieldListSrc[4];
            DateofOrignalBLreceivedbyGPBR = fieldListSrc[5];
            DateofOrignalInvoiceANDPackingListreceivedbyGPBR = fieldListSrc[6];
            ATA = fieldListSrc[7];
        }


        public void GenerateSAPMessage()
        {
            message = new StringBuilder();
            message.Append(ITRnumber).Append("\t");
            message.Append(BLnumber).Append("\t");
            message.Append(DateofASNsent).Append("\t");
            message.Append(DateofcopyBLsent).Append("\t");
            message.Append(DateofCopyBLPackingListsenttoCustomer).Append("\t");
            message.Append(DateofOrignalBLreceivedbyGPBR).Append("\t");
            message.Append(DateofOrignalInvoiceANDPackingListreceivedbyGPBR).Append("\t");
            message.Append(ATA);
        }


        public string Message
        {
            get { return this.message.ToString(); }
        }


        public void setBFTFieldProperties(SAPMessageFieldsModel fieldSpecs, string field)
        {
            if (field.Equals("ITR number"))
            {
                fieldSpecs.FieldName = field;
                fieldSpecs.InputType = TargetMessageInputTypes.MAP_FROM_SOURCE_WITH_SUBSTRING;
                fieldSpecs.DefaultValue = "";
                fieldSpecs.IsMandatory = true;
                fieldSpecs.MaxLength = 10;
            }
            else if (field.Equals("BL number"))
            {
                fieldSpecs.FieldName = field;
                fieldSpecs.InputType = TargetMessageInputTypes.MAP_FROM_SOURCE_WITH_SUBSTRING;
                fieldSpecs.DefaultValue = "";
                fieldSpecs.IsMandatory = false;
                fieldSpecs.MaxLength = 25;
            }
            else
            {
                fieldSpecs.FieldName = field;
                fieldSpecs.InputType = TargetMessageInputTypes.DATE;
                fieldSpecs.DefaultValue = "";
                fieldSpecs.IsMandatory = false;
                fieldSpecs.MaxLength = 10;
                fieldSpecs.FieldType = TargetMessageFieldTypes.YYYYMMDD;
            }

        }


        public string[] getBFTFields()
        {
            string[] fields = { "ITR number", "BL number", "Date of ASN sent", "Date of copy BL sent", "Date of Copy BL, Packing List sent to Customer", "Date of Orignal BL received by GPBR", "Date of Orignal Invoice & Packing List received by GPBR", "ATA" };
            return fields;
        }

    }
}
