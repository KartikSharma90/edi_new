﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.CommonTypes
{
    public class MapperModel
    {
        public string MapperCode { get; set; }
        public string MapperName { get; set; }
    }
}