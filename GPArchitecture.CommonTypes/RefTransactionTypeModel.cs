﻿using GPArchitecture.EnumsAndConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.CommonTypes
{
   public class RefTransactionTypeModel
    {
        private int intId;
        private string strTransactionName;
        private Dictionary<int, SAPMessageFieldsModel> objfields = new Dictionary<int, SAPMessageFieldsModel>();
        private string strFormat;
        private string strOutputFileExtension;

        public int ID
        {
            set { intId = value; }
            get { return intId; }
        }
        public string TargetMessageName
        {
            set { strTransactionName = value; }
            get { return strTransactionName; }
        }
        public TRANSACTION_TYPES TransactionType;

        public string OutputFileExtension
        {
            set { strOutputFileExtension = value; }
            get { return strOutputFileExtension; }
        }
        public string Format
        {
            set { strFormat = value; }
            get { return strFormat; }
        }
        public Dictionary<int, SAPMessageFieldsModel> Fields
        {
            set { objfields = value; }
            get { return objfields; }
        }
    }
    public class SAPMessageFieldsModel
    {
        private int intId;
        private string strFieldName;
        private string strDefaultValue;
        private string strOperations;
        private string strNote;
        private string strOperationParam;
        private int intFieldSeq;
        private int intMaxLength;
        private bool blnIsMappedFromSourceRefField;
        private bool isMandatory = false;
        private string validationError = string.Empty;
        private bool skipOnValidationFailure = false;

        public TargetMessageFieldTypes FieldType;

        public int ID
        {
            set { intId = value; }
            get { return intId; }
        }

        public string FieldName
        {
            set { strFieldName = value; }
            get { return strFieldName; }
        }

        public int FieldSeq
        {
            set { intFieldSeq = value; }
            get { return intFieldSeq; }
        }
        public TargetMessageInputTypes InputType;

        public string DefaultValue
        {
            set { strDefaultValue = value; }
            get { return strDefaultValue; }
        }

        public string OperationName
        {
            set { strOperations = value; }
            get { return strOperations; }
        }

        public string OperationParam
        {
            set { strOperationParam = value; }
            get { return strOperationParam; }
        }
        public int MaxLength
        {
            set { intMaxLength = value; }
            get { return intMaxLength; }
        }
        public bool IsMappedFromSourceRefField
        {
            set { blnIsMappedFromSourceRefField = value; }
            get { return blnIsMappedFromSourceRefField; }
        }
        public string Note
        {
            set { strNote = value; }
            get { return strNote; }
        }

        public bool IsMandatory
        {
            set { this.isMandatory = value; }
            get { return this.isMandatory; }
        }

        public string ValidationError
        {
            set { this.validationError = value; }
            get { return this.validationError; }
        }

        public bool SkipOnValidationFailure
        {
            set { this.skipOnValidationFailure = value; }
            get { return this.skipOnValidationFailure; }
        }
    }
}