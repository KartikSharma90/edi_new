﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using log4net;

//Internal
using GPArchitecture.Adapters;
using GPArchitecture.Adapters.Exceptions;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.Tools;

// Third Party
using WinSCP;
using System.Collections;

namespace GPArchitecture.Adapters.SFTP
{
    public class Sftp : IAdapters
    {
        #region Private Members
        private Gen_SFTPAccount sftpAccount;
        private SessionOptions sftpSessionOptions;
        private Session sftpSession;
        private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region CTORs

        /// <summary>
        /// Initializes a new instance of the <see cref="Sftp"/> class.
        /// </summary>
        /// <param name="sftpAccount">The SFTP account.</param>
        public Sftp(Gen_SFTPAccount sftpAccount)
        {
            this.sftpAccount = sftpAccount;
            // Setup session options
            sftpSessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = sftpAccount.Host,
                PortNumber = (sftpAccount.Port != null) ? sftpAccount.Port.Value : 0, // Default to 0 to use default port based on protocol selected
                UserName = sftpAccount.User,
                Password = sftpAccount.Password,
                SshHostKeyFingerprint = sftpAccount.SshHostKeyFingerPrint,
                SshPrivateKeyPath = sftpAccount.SshPrivateKeyPath

            };
        }

        #endregion

        #region DTORs
        ~Sftp()
        {
            if (this.sftpSession != null)
            {
                this.sftpSession.Dispose();
            }
        }
        #endregion

        #region IAdapter Interface

        /// <summary>
        /// Opens the connection.
        /// </summary>
        public void OpenConnection()
        {
            if (this.sftpSession != null && this.sftpSession.Opened)
            {
                this.CloseConnection();
            }

            this.sftpSession = new Session();
           this.sftpSession.ExecutablePath = GPArchitecture.Cache.Config.getConfig("WinSCPExecutablePath");
            //this.sftpSession.ExecutablePath ="D:\\Projects\\GoodPack\\Goodpack EDI\\Services\\FileTransferService\\WinSCP.exe";
           // this.sftpSession.ExecutablePath ="D:\\GoodpackEDI-Projects\\UAT_Final\\GoodpackEDI-clone\\GoodyearFileExchangeService\\WinSCP.exe";
            this.sftpSession.Open(this.sftpSessionOptions);
        }

        /// <summary>
        /// Closes the connection.
        /// </summary>
        public void CloseConnection()
        {
            this.sftpSession.Dispose();
        }

        /// <summary>
        /// Does the file exist in host.
        /// </summary>
        /// <param name="ftpFilePath">The FTP file path.</param>
        /// <returns><c>true</c> if ftpFilePath exists, <c>false</c> otherwise.</returns>
        public bool DoesFileExistInHost(string ftpFilePath)
        {
            return this.sftpSession.FileExists(ftpFilePath);
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="remoteFilePath">The remote file path.</param>
        /// <returns><c>true</c> if file exists, <c>false</c> otherwise.</returns>
        public bool DeleteFile(string remoteFilePath)
        {
            bool isDeleteSuccess = false;

            // Default return value to true if file doesn't exist
            if (!this.DoesFileExistInHost(remoteFilePath))
            {
                isDeleteSuccess = true;
            }
            else
            {
                this.sftpSession.RemoveFiles(remoteFilePath);
                isDeleteSuccess = true;
            }

            return isDeleteSuccess;
        }

        public bool IsConnected
        {
            get
            {
                return this.sftpSession.Opened;
            }
        }

        /// <summary>
        /// Gets the file list.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="pattern">The pattern.</param>
        /// <returns>List{System.String}.</returns>
        public List<string> GetFileList(string path, string pattern)
        {
            List<string> fileList = new List<string>();
            RemoteDirectoryInfo resultList = null;

            // Sanitize path value
            if (string.IsNullOrWhiteSpace(path))
            {
                path = @"./";
            }
            else
            {
                if (!path.EndsWith(@"/"))
                {
                    path += @"/";
                }
            }

            // Replace pattern
            pattern = WildCardRegex.WildcardToRegex(pattern.ToLower());

            if (path != null)
            {
                resultList = this.sftpSession.ListDirectory(path);
            }

            if (resultList != null && resultList.Files.Count > 0)
            {
                foreach (RemoteFileInfo file in resultList.Files)
                {
                    if (file.IsDirectory == false
                        && file.Name != "."
                        && file.Name != ".."
                        && Regex.Match(file.Name.ToLower(), pattern).Success)
                    {
                        fileList.Add(path + file.Name);
                    }
                }
            }

            return fileList;
        }

        /// <summary>
        /// Downloads the file.
        /// </summary>
        /// <param name="downloadSourcePath">The download source path.</param>
        /// <param name="downloadToPath">The download to path.</param>
        /// <param name="deleteSourceFileAfterDownload">if set to <c>true</c> [delete source file after download].</param>
        /// <returns><c>true</c> if download, <c>false</c> otherwise.</returns>
        /// <exception cref="GPSFTPAdapterException">Adapter failed while downloading file  + downloadSourcePath + .  + ex.Message</exception>
        public bool DownloadFile(string downloadSourcePath, string downloadToPath, bool deleteSourceFileAfterDownload)
        {
            objILog.Debug("Entering DownloadFile method");
            bool downloadSuccessful = false;
            try
            {
                RemoteDirectoryInfo directory = this.sftpSession.ListDirectory(downloadSourcePath);
                foreach (RemoteFileInfo fileInfo in directory.Files)
                {

                    if (fileInfo.IsDirectory == false)
                    {
                        //bool startName = fileInfo.Name.StartsWith("GOCPL_");
                        //if (startName)
                        //{
                            try
                            {
                                // Download the file and throw on any error
                                string downloadPath = downloadSourcePath + fileInfo.Name;
                                string localPath = downloadToPath + "\\" + fileInfo.Name;
                                this.sftpSession.GetFiles(downloadPath, localPath, deleteSourceFileAfterDownload).Check();
                                Console.WriteLine("Download to backup done.");
                                downloadSuccessful = true;
                            }
                            catch (WinSCP.SessionException ex)
                            {
                                objILog.Error("Error encountered while downloading file from SFTP location" + ex.Message, ex);
                                throw new GPSFTPAdapterException(this.sftpAccount, downloadSourcePath, "Adapter failed while downloading file " + downloadSourcePath + ". " + ex.Message, ex);
                            }
                        //}
                    }
                }

                return downloadSuccessful;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                objILog.Error("Error encountered while processing download file method" + e.Message, e);
                return false;
            }
        }

        /// <summary>
        /// Uploads the file.
        /// </summary>
        /// <param name="uploadSourcePath">The upload source path.</param>
        /// <param name="uploadDestinationPath">The upload destination path.</param>
        /// <param name="deleteSourceFileAfterDownload">if set to <c>true</c> [delete source file after download].</param>
        /// <returns><c>true</c> if upload file, <c>false</c> otherwise.</returns>
        /// <exception cref="GPSFTPAdapterException">Adapter failed while uploading file  + uploadSourcePath + .  + ex.Message</exception>
        public bool UploadFile(string uploadSourcePath, string uploadDestinationPath, bool deleteSourceFileAfterDownload)
        {
            bool uploadSuccessful = false;
            try
            {
                //Checking files on local locations
                bool fileExist = (Directory.GetFiles(uploadSourcePath).Length > 0) ? true : false;
                if (fileExist)
                {
                    IList uploadFiles = null;
                    uploadFiles = Directory.GetFiles(uploadSourcePath);

                    //Upload files from local location to SFTP server
                    try
                    {
                        foreach (var uploadFromLocation in uploadFiles)
                        {
                            this.sftpSession.PutFiles(uploadFromLocation.ToString(), uploadDestinationPath, deleteSourceFileAfterDownload).Check();
                            Console.WriteLine("Upload done.");
                        }
                        uploadSuccessful = true;
                    }
                    catch (WinSCP.SessionException ex)
                    {
                        objILog.Error("Error encountered while uploading file from SFTP location" + ex.Message, ex);
                        throw new GPSFTPAdapterException(this.sftpAccount, uploadSourcePath, "Adapter failed while uploading file " + uploadSourcePath + ". " + ex.Message, ex);
                    }
                }
                else
                {
                    Console.WriteLine("File does not exist yet", uploadSourcePath);
                }
                return uploadSuccessful;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                objILog.Error("Error encountered while processing upload file method" + e.Message, e);
                return false;
            }
        }

        #endregion

        #region Public Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Clones the FTP account.
        /// </summary>
        /// <returns>Gen_SFTPAccount.</returns>
        private Gen_SFTPAccount CloneFtpAccount()
        {
            return GPArchitecture.Cache.Clone.DeepClone<Gen_SFTPAccount>(this.sftpAccount);
        }
        #endregion


    }
}
