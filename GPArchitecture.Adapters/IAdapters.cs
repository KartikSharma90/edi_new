﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Adapters
{
    public interface IAdapters
    {
        void OpenConnection();
        void CloseConnection();
        bool IsConnected { get; }
       // bool UploadFile(string uploadFromPath, string uploadToPath, bool deleteSourceFileAfterDownload);
        bool DeleteFile(string targetFilePath);
        bool DoesFileExistInHost(string filePath);
        List<string> GetFileList(string path, string pattern);
        bool DownloadFile(string downloadFromPath, string downloadToPath, bool deleteSourceFileAfterDownload);
    }
}
