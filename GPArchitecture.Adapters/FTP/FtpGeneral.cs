﻿using GPArchitecture.Adapters.Exceptions;
using GPArchitecture.CommonTypes;
using GPArchitecture.Tools;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WinSCP;

namespace GPArchitecture.Adapters.FTP
{
    public class FtpGeneral
    {
        private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private FtpModel ftpAccount;
        private SessionOptions ftpSessionOptions;
        private Session ftpSession;

        public FtpGeneral(FtpModel ftpAccount)
        {
            this.ftpAccount = ftpAccount;
            // Setup session options
            ftpSessionOptions = new SessionOptions
            {
                Protocol = Protocol.Ftp,
                HostName = ftpAccount.Host,
                PortNumber = (ftpAccount.Port != null) ? ftpAccount.Port.Value : 0, // Default to 0 to use default port based on protocol selected
                UserName = ftpAccount.User,
                Password = ftpAccount.Password

            };            
            if (string.IsNullOrEmpty(ftpAccount.EncryptionType))
            {
                ftpAccount.EncryptionType = string.Empty;
            }
            switch (ftpAccount.EncryptionType.ToLower())
            {
                case "explicittls":
                    ftpSessionOptions.FtpSecure = FtpSecure.ExplicitTls;
                    break;
                case "explicitssl":
                    ftpSessionOptions.FtpSecure = FtpSecure.ExplicitSsl;
                    ftpSessionOptions.TlsHostCertificateFingerprint = ftpAccount.SslHostCertificateFingerprint;
                    break;
                case "implicit":
                    ftpSessionOptions.FtpSecure = FtpSecure.Implicit;
                    ftpSessionOptions.TlsHostCertificateFingerprint = ftpAccount.SslHostCertificateFingerprint;
                    break;
                default:
                    ftpSessionOptions.FtpSecure = FtpSecure.None;
                    break;
            }
        }

        public void OpenConnection()
        {
            if (this.ftpSession != null && this.ftpSession.Opened)
            {
                this.CloseConnection();
            }

            this.ftpSession = new Session();
            this.ftpSession.ExecutablePath = GPArchitecture.Cache.Config.getConfig("WinSCPExecutablePath");
            this.ftpSession.Open(this.ftpSessionOptions);
        }

        public void CloseConnection()
        {
            this.ftpSession.Dispose();
        }

        public bool DownloadFile(string downloadSourcePath, string downloadToPath, bool deleteSourceFileAfterDownload)
        {
            objILog.Debug("Entering DownloadFile method");
            bool downloadSuccessful = false;
            try
            {
                RemoteDirectoryInfo directory = this.ftpSession.ListDirectory(downloadSourcePath);
                foreach (RemoteFileInfo fileInfo in directory.Files)
                {

                    if (fileInfo.IsDirectory == false)
                    {
                        bool startName = fileInfo.Name.StartsWith("GOCPL_");
                        if (startName)
                        {
                            try
                            {
                                // Download the file and throw on any error
                                string downloadPath = downloadSourcePath + fileInfo.Name;
                                string localPath = downloadToPath + "\\" + fileInfo.Name;
                                this.ftpSession.GetFiles(downloadPath, localPath, deleteSourceFileAfterDownload).Check();
                                objILog.Debug("Download to backup done.");
                                downloadSuccessful = true;
                            }
                            catch (WinSCP.SessionException ex)
                            {
                                objILog.Error("Error encountered while downloading file from FTP location" + ex.Message, ex);
                                throw new GPFTPAdapterException(this.ftpAccount, downloadSourcePath, "Adapter failed while downloading file " + downloadSourcePath + ". " + ex.Message, ex);
                            }
                        }
                    }
                }

                return downloadSuccessful;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e);
                objILog.Error("Error encountered while processing download file method" + e.Message, e);
                return false;
            }
        }

        public bool DoesFileExistInHost(string ftpFilePath)
        {
            return this.ftpSession.FileExists(ftpFilePath);
        }

        public bool DeleteFile(string remoteFilePath)
        {
            bool isDeleteSuccess = false;

            // Default return value to true if file doesn't exist
            if (!this.DoesFileExistInHost(remoteFilePath))
            {
                isDeleteSuccess = true;
            }
            else
            {
                this.ftpSession.RemoveFiles(remoteFilePath);
                isDeleteSuccess = true;
            }

            return isDeleteSuccess;
        }

        public bool IsConnected
        {
            get
            {
                return this.ftpSession.Opened;
            }
        }

        public List<string> GetFileList(string path, string pattern)
        {
            List<string> fileList = new List<string>();
            RemoteDirectoryInfo resultList = null;

            // Sanitize path value
            if (string.IsNullOrWhiteSpace(path))
            {
                path = @"./";
            }
            else
            {
                if (!path.EndsWith(@"/"))
                {
                    path += @"/";
                }
            }

            // Replace pattern
            pattern = WildCardRegex.WildcardToRegex(pattern.ToLower());

            if (path != null)
            {
                resultList = this.ftpSession.ListDirectory(path);
            }

            if (resultList != null && resultList.Files.Count > 0)
            {
                foreach (RemoteFileInfo file in resultList.Files)
                {
                    if (file.IsDirectory == false
                        && file.Name != "."
                        && file.Name != ".."
                        && Regex.Match(file.Name.ToLower(), pattern).Success)
                    {
                        fileList.Add(path + file.Name);
                    }
                }
            }

            return fileList;
        }

        public bool IsDirectoryExists(string path)
        {
            RemoteDirectoryInfo dirInfo = this.ftpSession.ListDirectory(path);
            if (dirInfo.Files.Count > 0)
                return true;
            else
                return false;
        }

        //private FtpGeneral CloneFtpAccount()
        //{
        //    return GoodpackEDI.Utilities.GPTools.DeepClone<FtpGeneral>(this.ftpAccount);
        //}
    }
}
