using System;
 using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Net;
 using System.Text;
 using System.Text.RegularExpressions;
 using log4net;
 
 //Internal
 using GPArchitecture.Adapters;
 using GPArchitecture.Adapters.Exceptions;
 using GPArchitecture.DataLayer.Models;
 using GPArchitecture.Tools;
 
 // Third Party
 using WinSCP;
 using System.Collections;
 
 namespace GPArchitecture.Adapters.FTP
 {
     public class Ftp : IAdapters
     {
         #region Private Members
         private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
         private Gen_FTPAccount ftpAccount;
         private SessionOptions ftpSessionOptions;
         private Session ftpSession;
         #endregion
 
         #region CTORs
         public Ftp(Gen_FTPAccount ftpAccount)
         {
             this.ftpAccount = ftpAccount;
             // Setup session options
             ftpSessionOptions = new SessionOptions
             {
                 Protocol = Protocol.Ftp,
                 HostName = ftpAccount.Host,
                 PortNumber = (ftpAccount.Port != null) ? ftpAccount.Port.Value : 0, // Default to 0 to use default port based on protocol selected
                 UserName = ftpAccount.User,
                 Password = ftpAccount.Password
 
             };
 
             // If protocol is Sftp, set the encryption type
             if (string.IsNullOrEmpty(ftpAccount.EncryptionType))
             {
                 ftpAccount.EncryptionType = string.Empty;
             }
             switch (ftpAccount.EncryptionType.ToLower())
             {
                 case "explicittls":
                     ftpSessionOptions.FtpSecure = FtpSecure.ExplicitTls;
                     break;
                 case "explicitssl":
                     ftpSessionOptions.FtpSecure = FtpSecure.ExplicitSsl;
                     ftpSessionOptions.TlsHostCertificateFingerprint = ftpAccount.SslHostCertificateFingerprint;
                     break;
                 case "implicit":
                     ftpSessionOptions.FtpSecure = FtpSecure.Implicit;
                     ftpSessionOptions.TlsHostCertificateFingerprint = ftpAccount.SslHostCertificateFingerprint;
                     break;
                 default:
                     ftpSessionOptions.FtpSecure = FtpSecure.None;
                     break;
             }
         }
         #endregion
 
         #region DTORs
         ~Ftp()
         {
             if (this.ftpSession != null)
             {
                 this.ftpSession.Dispose();
             }
         }
         #endregion
 
         #region IAdapter Interface
         public void OpenConnection()
         {
             if (this.ftpSession != null && this.ftpSession.Opened)
             {
                 this.CloseConnection();
             }
 
             this.ftpSession = new Session();
             this.ftpSession.ExecutablePath = GPArchitecture.Cache.Config.getConfig("WinSCPExecutablePath");
             this.ftpSession.Open(this.ftpSessionOptions);
         }
 
         public void CloseConnection()
         {
             this.ftpSession.Dispose();
         }
 
         /// <summary>
         /// Downloads the file.
         /// </summary>
         /// <param name="downloadSourcePath">The download source path.</param>
         /// <param name="downloadToPath">The download to path.</param>
         /// <param name="deleteSourceFileAfterDownload">if set to <c>true</c> [delete source file after download].</param>
         /// <returns><c>true</c> if download, <c>false</c> otherwise.</returns>
         /// <exception cref="GPSFTPAdapterException">Adapter failed while downloading file  + downloadSourcePath + .  + ex.Message</exception>
         public bool DownloadFile(string downloadSourcePath, string downloadToPath, bool deleteSourceFileAfterDownload)
         {
             objILog.Debug("Entering DownloadFile method");
             bool downloadSuccessful = false;
             try
             {
                 RemoteDirectoryInfo directory = this.ftpSession.ListDirectory(downloadSourcePath);
                 foreach (RemoteFileInfo fileInfo in directory.Files)
                 {
                     if (fileInfo.IsDirectory == false)
                     {
                         try
                         {
                             // Download the file and throw on any error
                             string downloadPath = downloadSourcePath + fileInfo.Name;
                             string localPath = downloadToPath + "\\" + fileInfo.Name;
                             this.ftpSession.GetFiles(downloadPath, localPath, deleteSourceFileAfterDownload).Check();
                             objILog.Debug("Download to backup done.");
                             downloadSuccessful = true;
                         }
                         catch (WinSCP.SessionException ex)
                         {
                             objILog.Error("Error encountered while downloading file from FTP location" + ex.Message, ex);
                             throw new GPFTPAdapterException(this.ftpAccount, downloadSourcePath, "Adapter failed while downloading file " + downloadSourcePath + ". " + ex.Message, ex);
                         }
                     }
                 }
                 return downloadSuccessful;
             }
             catch (Exception e)
             {
                 Console.WriteLine("Error: {0}", e);
                 objILog.Error("Error encountered while processing download file method" + e.Message, e);
                 return false;
             }
         }
 
 
         public bool UploadFile(string uploadSourcePath, string uploadDestinationPath, bool deleteSourceFileAfterDownload)
         {
             // init Vars
             bool uploadSuccessful = false;
 
             // Check first if file exists
             if (!File.Exists(uploadSourcePath))
             {
                 throw new GPAdapterException(
                     this.CloneFtpAccount()
                     , uploadSourcePath
                     , string.Format("File {0} not found in {1}.", uploadSourcePath, this.ftpAccount.Host)
                     );
             }
 
             TransferOperationResult result = this.ftpSession.PutFiles(uploadSourcePath, uploadDestinationPath, deleteSourceFileAfterDownload);
 
             try
             {
                 result.Check();
             }
             catch (WinSCP.SessionException ex)
             {
                 throw new GPFTPAdapterException(this.ftpAccount, uploadSourcePath, "Adapter failed while uploading file " + uploadSourcePath + ". " + ex.Message, ex);
             }
 
             uploadSuccessful = result.IsSuccess;
             return uploadSuccessful;
         }
 
         //public bool DownloadFile(string downloadSourcePath, string downloadToPath, bool deleteSourceFileAfterDownload)
         //{
         //    // Init vars
         //    bool download = true;
         //    bool isSuccess = false;
 
         //    try
         //    {
         //        // Manual "remote to local" synchronization.
         //        // You can achieve the same using:
         //        // session.SynchronizeDirectories(
         //        //     SynchronizationMode.Local, localPath, remotePath, false, false, SynchronizationCriteria.Time, 
         //        //     new TransferOptions { IncludeMask = fileName }).Check();
         //        if (this.ftpSession.FileExists(downloadSourcePath))
         //        {
         //            DateTime remoteWriteTime = this.ftpSession.GetFileInfo(downloadSourcePath).LastWriteTime;
         //            DateTime localWriteTime = File.GetLastWriteTime(downloadToPath);
 
         //            // Check if file should be downloaded
         //            if (remoteWriteTime > localWriteTime)
         //            {
         //                download = true;
         //            }
         //            else
         //            {
         //                download = false;
         //                isSuccess = true;
         //            }
         //        }
 
         //        if (download)
         //        {
         //            // Download the file and throw on any error
         //            TransferOperationResult result = this.ftpSession.GetFiles(downloadSourcePath, downloadToPath, deleteSourceFileAfterDownload);
         //            try
         //            {
         //                result.Check();
         //            }
         //            catch (WinSCP.SessionException ex)
         //            {
         //                throw new GPFTPAdapterException(this.ftpAccount, downloadSourcePath, "Adapter failed while downloading file " + downloadSourcePath + ". " + ex.Message, ex);
         //            }
         //            isSuccess = result.IsSuccess;
         //        }
         //    }
         //    catch (Exception ex)
         //    {
         //        throw ex;
         //    }
 
         //    return isSuccess;
         //}
 
 
         public bool DoesFileExistInHost(string ftpFilePath)
         {
             return this.ftpSession.FileExists(ftpFilePath);
         }
 
         public bool DeleteFile(string remoteFilePath)
         {
             bool isDeleteSuccess = false;
 
             // Default return value to true if file doesn't exist
             if (!this.DoesFileExistInHost(remoteFilePath))
             {
                 isDeleteSuccess = true;
             }
             else
             {
                 this.ftpSession.RemoveFiles(remoteFilePath);
                 isDeleteSuccess = true;
             }
 
             return isDeleteSuccess;
         }
 
         public bool IsConnected
         {
             get
             {
                 return this.ftpSession.Opened;
             }
         }
 
         public List<string> GetFileList(string path, string pattern)
         {
             List<string> fileList = new List<string>();
             RemoteDirectoryInfo resultList = null;
 
             // Sanitize path value
             if (string.IsNullOrWhiteSpace(path))
             {
                 path = @"./";
             }
             else
             {
                 if (!path.EndsWith(@"/"))
                 {
                     path += @"/";
                 }
             }
 
             // Replace pattern
             pattern = WildCardRegex.WildcardToRegex(pattern.ToLower());
 
             if (path != null)
             {
                 resultList = this.ftpSession.ListDirectory(path);
             }
 
             if (resultList != null && resultList.Files.Count > 0)
             {
                 foreach (RemoteFileInfo file in resultList.Files)
                 {
                     if (file.IsDirectory == false
                         && file.Name != "."
                         && file.Name != ".."
                         && Regex.Match(file.Name.ToLower(), pattern).Success)
                     {
                         fileList.Add(path + file.Name);
                     }
                 }
             }
 
             return fileList;
         }
         #endregion
 
         #region Public Methods
         #endregion
 
         #region Private Methods
         private Gen_FTPAccount CloneFtpAccount()
         {
             return GPArchitecture.Cache.Clone.DeepClone<Gen_FTPAccount>(this.ftpAccount);
         }
         #endregion
 
 
     }
 }

 

