﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Internal
using GPArchitecture.Adapters;
using GPArchitecture.Adapters.Exceptions;
using GPArchitecture.DataLayer;
using System.IO;
using GPArchitecture.Tools;
using System.Text.RegularExpressions;

namespace GPArchitecture.Adapters
{
    public class WindowsShare : IAdapters
    {
        #region Private Members
        #endregion

        #region CTORs
        public WindowsShare()
        {
        }
        #endregion

        #region DTORs
        ~WindowsShare()
        {

        }
        #endregion

        #region IAdapter Interface
        public void OpenConnection()
        {

        }

        public void CloseConnection()
        {
        }

        public bool UploadFile(string uploadSourcePath, string uploadDestinationPath, bool deleteSourceFileAfterUpload)
        {
            // init Vars
            bool uploadSuccessful = false;

            try
            {
                this.MoveFile(uploadSourcePath, uploadDestinationPath, deleteSourceFileAfterUpload);
            }
            catch (System.Exception ex)
            {
                throw new GPAdapterException("Adapter failed while uploading file " + uploadSourcePath + ". " + ex.Message, ex);
            }

            uploadSuccessful = true;
            return uploadSuccessful;
        }

        public bool DownloadFile(string downloadSourcePath, string downloadToPath, bool deleteSourceFileAfterDownload)
        {
            // Init vars
            bool isSuccess = false;

            try
            {
                this.MoveFile(downloadSourcePath, downloadToPath, deleteSourceFileAfterDownload);
            }
            catch (System.Exception ex)
            {
                throw new GPAdapterException("Adapter failed while downloading file " + downloadSourcePath + ". " + ex.Message, ex);
            }

            return isSuccess;
        }

        public bool DoesFileExistInHost(string ftpFilePath)
        {
            return File.Exists(ftpFilePath);
        }

        public bool DeleteFile(string remoteFilePath)
        {
            bool isDeleteSuccess = false;

            // Default return value to true if file doesn't exist
            if (!this.DoesFileExistInHost(remoteFilePath))
            {
                isDeleteSuccess = true;
            }
            else
            {
                File.Delete(remoteFilePath);
                isDeleteSuccess = true;
            }

            return isDeleteSuccess;
        }

        /// <summary>
        /// Always returns true
        /// </summary>
        public bool IsConnected
        {
            get
            {
                return true;
            }
        }

        public List<string> GetFileList(string path, string pattern)
        {
            List<string> fileList = new List<string>();
            List<FileInfo> fileInfoList = new List<FileInfo>();
            string[] fileListArray = null;

            // Sanitize path value
            if (string.IsNullOrWhiteSpace(path))
            {
                path = string.Empty;
            }
            else
            {
                if (!path.EndsWith(@"\"))
                {
                    path += @"\";
                }
            }

            // Replace pattern
            pattern = WildCardRegex.WildcardToRegex(pattern.ToLower());

            if (!string.IsNullOrWhiteSpace(path))
            {
                fileListArray = Directory.GetFiles(path, "*.*");
            }

            if (fileListArray != null && fileListArray.Count() > 0)
            {
                foreach (string file in fileListArray)
                {
                    if (file != "."
                        && file != ".."
                        && Regex.Match(file.ToLower(), pattern).Success)
                    {
                        fileList.Add(file);
                    }
                }
            }

            return fileList;
        }
        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        private void MoveFile(string sourceFilePath, string destinationFilePath, bool deleteSourceFile)
        {
            if (File.Exists(destinationFilePath))
            {
                File.Delete(destinationFilePath);
            }
            if (deleteSourceFile == true)
            {
                File.Move(sourceFilePath, destinationFilePath);
            }
            else
            {
                File.Copy(sourceFilePath, destinationFilePath);
            }
        }
        #endregion


    }
}
