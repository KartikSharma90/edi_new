using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 
 // Internal
 using GPArchitecture.DataLayer.Models;
 using GPArchitecture.CommonTypes;
 
 namespace GPArchitecture.Adapters.Exceptions
 {
     public class GPFTPAdapterException : System.Exception
     {
         #region Private Members
         Gen_FTPAccount ftpAccount;
         FtpModel ftpAccountNew;
         SFtpModel sftpAccountNew;
         string filePath;
         StringBuilder messageBuilder = new StringBuilder();
         #endregion
 
         public GPFTPAdapterException(Gen_FTPAccount ftpAccount, string filePath)
             : base()
         {
             this.ftpAccount = ftpAccount;
             this.filePath = filePath;
         }
 
         public GPFTPAdapterException(Gen_FTPAccount ftpAccount, string filePath, string message)
             : base(message)
         {
             this.ftpAccount = ftpAccount;
             this.filePath = filePath;
         }
 
         public GPFTPAdapterException(Gen_FTPAccount ftpAccount, string filePath, string message, Exception ex)
             : base(message, ex)
         {
             this.ftpAccount = ftpAccount;
             this.filePath = filePath;
         }
         public GPFTPAdapterException(FtpModel ftpAccount, string filePath, string message, Exception ex)
             : base(message, ex)
         {
             this.ftpAccountNew = ftpAccount;
             this.filePath = filePath;
         }
         public GPFTPAdapterException(SFtpModel ftpAccount, string filePath, string message, Exception ex)
             : base(message, ex)
         {
             this.sftpAccountNew = ftpAccount;
             this.filePath = filePath;
         }
 
     }
 }



