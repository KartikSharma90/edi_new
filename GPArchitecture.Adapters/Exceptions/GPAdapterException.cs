using GPArchitecture.DataLayer.Models;
 using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 
 namespace GPArchitecture.Adapters.Exceptions
 {
     public class GPAdapterException : System.Exception
     {
         public GPAdapterException() : base() { }
 
         public GPAdapterException(string message) : base(message) { }
 
         public GPAdapterException(string message, System.Exception innerException) : base(message, innerException) { }
 
         public GPAdapterException(Gen_FTPAccount ftpAccount, string filePath, string message) : base(message)
         {  }
     }
 }

 

