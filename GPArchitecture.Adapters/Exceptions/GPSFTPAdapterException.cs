﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Internal
using GPArchitecture.DataLayer.Models;
using GPArchitecture.CommonTypes;

namespace GPArchitecture.Adapters.Exceptions
{
    public class GPSFTPAdapterException : System.Exception
    {
        #region Private Members
        Gen_SFTPAccount sftpAccount;
        string filePath;
        SFtpModel sftpAccountNew;
        StringBuilder messageBuilder = new StringBuilder();
        #endregion

        public GPSFTPAdapterException(Gen_SFTPAccount sftpAccount, string filePath)
            : base()
        {
            this.sftpAccount = sftpAccount;
            this.filePath = filePath;
        }

        public GPSFTPAdapterException(Gen_SFTPAccount sftpAccount, string filePath, string message)
            : base(message)
        {
            this.sftpAccount = sftpAccount;
            this.filePath = filePath;
        }

        public GPSFTPAdapterException(Gen_SFTPAccount sftpAccount, string filePath, string message, Exception ex)
            : base(message, ex)
        {
            this.sftpAccount = sftpAccount;
            this.filePath = filePath;
        }

        public GPSFTPAdapterException(SFtpModel sftpAccount, string filePath, string message, Exception ex)
            : base(message, ex)
        {
            this.sftpAccountNew = sftpAccount;
            this.filePath = filePath;
        }
    }
}
