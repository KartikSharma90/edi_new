﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GPArchitecture.Cache.ViewModels;
using GPArchitecture.DataLayer.Models;

namespace GPArchitecture.Cache
{
    public class Config
    {
        private static Dictionary<string, string> objCFG_MAIN;
        public static string getConfig(string Key)
        {
            string strValue;
            if (objCFG_MAIN == null)
            {
                //CacheDAO objCacheDAO;
                objCFG_MAIN = new Dictionary<string, string>();
                //objCacheDAO = new CacheDAO(DBConnection.SqlConnString);
                //DataTable objCFGtable = objCacheDAO.loadTable(CacheResource.CFG_MAIN);
                //foreach (DataRow drCFG in objCFGtable.Rows)
                //{
                //    objCFG_MAIN.Add(drCFG["ConfigItem"].ToString(), drCFG["Value"].ToString());
                //}
                IList<ConfigModel> configModel=null;

                using (var context = new GoodpackEDIEntities())
                {
                     configModel = (from s in context.Gen_Configuration
                                                      select new ConfigModel
                                                      {
                                                          ConfigItem = s.ConfigItem,
                                                          Value = s.Value,
                                                          Description = s.Description
                                                      }).ToList();
                }
                foreach (var item in configModel)
                {
                    if (!objCFG_MAIN.Keys.Contains(item.ConfigItem.ToString()))
                        objCFG_MAIN.Add(item.ConfigItem.ToString(), item.Value.ToString());
                }

            }
            objCFG_MAIN.TryGetValue(Key, out strValue);
            return strValue;
        }
        public static List<string>  getConfigList(string Key)
        {
            IList<ConfigModel> configModel = new List<ConfigModel>();
            using (var context = new GoodpackEDIEntities())
            {
                configModel = (from s in context.Gen_Configuration
                               select new ConfigModel
                               {
                                   ConfigItem = s.ConfigItem,
                                   Value = s.Value,
                                   Description = s.Description
                               }).ToList();
            }          
            return configModel.Where(x => x.ConfigItem == Key).Select(x => x.Value).ToList();
            
        }
    }
}
