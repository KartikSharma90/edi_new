﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Cache.ViewModels
{
    class ConfigModel
    {
        public string ConfigItem { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
