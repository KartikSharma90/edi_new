﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.CommonTypes
{
    public class FtpModel
    {        
        public string Name { get; set; }
        public string Host { get; set; }
        public Nullable<int> Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string EncryptionType { get; set; }
        public string SslHostCertificateFingerprint { get; set; }
        public string Location { get; set; }
    }

    public class SFtpModel
    {
        public string Name { get; set; }
        public string Host { get; set; }
        public Nullable<int> Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string EncryptionType { get; set; }
        public string SslHostCertificateFingerprint { get; set; }
        public string Location { get; set; }
        public string SshHostKeyFingerPrint { get; set; }
        public string SshPrivateKeyPath { get; set; }
    }
}
