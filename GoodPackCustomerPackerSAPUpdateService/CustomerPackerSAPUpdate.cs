﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackCustomerPackerSAPUpdateService
{
    public partial class CustomerPackerSAPUpdate : ServiceBase
    {
        internal static ILog log = LogManager.GetLogger(typeof(CustomerPackerSAPUpdate));
        public System.Timers.Timer custPackerUpdateTimer;
        private static int NoofAttempts = 0;
        CustomerPackerUpdateSync custPackUpdate = new CustomerPackerUpdateSync();
        public CustomerPackerSAPUpdate()
        {
            log.Info("Entering into constructor");
            InitializeComponent();
            log.Info("Leaving into constructor");
        }
        public void debugger()
        {
            log.Info("Entering into Start method");
            OnStart(null);
            log.Info("Leaving Start method");
        }
        protected override void OnStart(string[] args)
        {
            log.Debug("Entering CustomerPackerSAPUpdate OnStart");
            this.custPackerUpdateTimer = new System.Timers.Timer();
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            this.custPackerUpdateTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.custPackerUpdateTimer.Elapsed += updateCustomerPackerUpdate_Elapsed;
            this.custPackerUpdateTimer.Enabled = true;
            this.custPackerUpdateTimer.Start();
            log.Debug("Leaving CustomerPackerSAPUpdate Service OnStart");
            //while (true) { }
        }

        void updateCustomerPackerUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            log.Debug("updateCustomerPackerUpdate Start");
            if (!CustomerPackerUpdateSync.isProcessing)
                custPackUpdate.GetCustomerPackerList();
            log.Debug("updateCustomerPackerUpdate End");
        }
        protected override void OnStop()
        {
            this.custPackerUpdateTimer.Stop();
            while (CustomerPackerUpdateSync.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    log.Warn("CustomerPackerUpdateSync Services Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
