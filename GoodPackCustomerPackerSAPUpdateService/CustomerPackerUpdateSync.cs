﻿using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Configuration;


namespace GoodPackCustomerPackerSAPUpdateService
{
    public class CustomerPackerUpdateSync
    {
        internal static ILog log = LogManager.GetLogger(typeof(CustomerPackerUpdateSync));
        public static bool isProcessing = false;
        private readonly GoodpackEDIEntities context = null;

        #region Public     

        public CustomerPackerUpdateSync()
        {
           
            context = new GoodpackEDIEntities();
        }
        public void GetCustomerPackerList()
        {
            log.Debug("GetCustomerPackerList Entered");
            isProcessing = true;
            try
            {
                UpdateSubsiDetails();
                UpdateCustomerPackerConsigneeWarehousePortDetails();
                UpdateCustomerPackerRelation();
            }
            catch (Exception e)
            {
                log.Error("GetCustomerPackerList error -" + e);
            }
            finally
            {
                isProcessing = false;              
            }
            log.Debug("GetCustomerPackerList left");
        }
        #endregion

        #region Private

        private void UpdateCustomerPackerConsigneeWarehousePortDetails()
        {
            try
            {
                log.Debug("UpdateCustomerPackerConsigneeWarehousePortDetails Entered");
                List<Gen_EDIEntities> newList = new List<Gen_EDIEntities>();
                List<Gen_EDIEntities> currentList = context.Gen_EDIEntities.ToList();

                CustomerPackerSAPSyncService custPackService = new CustomerPackerSAPSyncService();
//for reading configuration from Appconfig
                List<string> configValue = ConfigurationManager.AppSettings["AccountGroup"].Split(',').ToList<string>();
                foreach (string accountGroup in configValue)
                {
                    List<Dictionary<string, string>> listCustomer = custPackService.GetCustPackrConsineWarehousePortList(accountGroup);
                    foreach (Dictionary<string, string> values in listCustomer)
                    {
                        int customerCode = int.Parse(values["CustomerCode"]);
                        Gen_EDIEntities ediEntity = new Gen_EDIEntities();
                        ediEntity.EntityCode = customerCode;
                        ediEntity.EntityName = values["CustomerName"];
                        ediEntity.SearchTerm = values["CustomerSearchName"];
                        int subsiCode = int.Parse(values["SubsiCode"]);
                        ediEntity.SubsiId = context.Gen_EDISubsies.Where(p => p.SubsiCode == subsiCode).Select(p => p.Id).FirstOrDefault();
                        ediEntity.EmailIdList = "anoop.varghese@goodpack.com";
                        ediEntity.ContractNumber = "NA";
                        ediEntity.ContractStartDate = DateTime.Now;
                        ediEntity.ContractEndDate = DateTime.Now;
                        ediEntity.Vertical = "NA";
                        ediEntity.GoodpackSubsidary = "";
                        ediEntity.GoodpackSubsidaryEmailIdList = "";
                        ediEntity.IsEnabled = true;
                        ediEntity.SourcePath = null;
                        ediEntity.EntityTypeId = (customerCode > 300000 && customerCode < 400000) ? 1 : (customerCode > 400000 && customerCode < 500000) ? 2 : 3;
                        newList.Add(ediEntity);
                    }
                }
                var listDistinctValues = newList.GroupBy(x => x.EntityCode).Select(x => x.First()).ToList();
                //List<Gen_EDIEntities> finalValue = listDistinctValues.Where(p => !currentList.Any(p1 => (p1.EntityCode == p.EntityCode || p1.EntityName.ToString().Trim() == p.EntityName.ToString().Trim() || p1.SearchTerm.ToString().Trim() == p.SearchTerm.ToString().Trim()))
                //                                                    && p.SubsiId != 0).ToList();
                List<Gen_EDIEntities> finalValue = listDistinctValues.Where(p => !currentList.Any(p1 => (p1.EntityCode == p.EntityCode)) && p.SubsiId != 0).ToList();
                foreach (Gen_EDIEntities value in finalValue)
                {
                    try
                    {
                        context.Gen_EDIEntities.Add(value);
                        log.Info("entered value -" + value);
                        context.SaveChanges();
                    }
                    catch (Exception exec1)
                    {
                        log.Error("Gen_EDIEntities db save values :-" + JsonConvert.SerializeObject(value));
                        log.Error("Exception :-" + exec1);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("GetCustomerPackerList,CustomerPackerSync error -" + ex);
            }
            log.Debug("UpdateCustomerPackerConsigneeWarehousePortDetails left");
        }

        private void UpdateCustomerPackerRelation()
        {
            log.Debug("UpdateCustomerPackerRelation Entered");
            try
            {
                List<Trn_CustomerToPackerMapper> newListCustPackRel = new List<Trn_CustomerToPackerMapper>();
                List<Trn_CustomerToPackerMapper> currentCustomerPacker = context.Trn_CustomerToPackerMapper.ToList();
//for reading configuration from Appconfig
                List<string> configValue = ConfigurationManager.AppSettings["PartnerFunction"].Split(',').ToList<string>();
                configValue.RemoveAll(i => string.IsNullOrEmpty(i));
                CustomerPackerRelationSyncService custpackRelation = new CustomerPackerRelationSyncService();
                foreach (string partnerFunction in configValue)
                {
                    List<Dictionary<string, string>> listCustomerPackRelation = custpackRelation.GetCustomerPackerRelation(partnerFunction);
                    foreach (Dictionary<string, string> values in listCustomerPackRelation)
                    {
                        int subsiCode = int.Parse(values["Subsi"]);
                        Trn_CustomerToPackerMapper custPackMap = new Trn_CustomerToPackerMapper();
                        custPackMap.CustomerCode = int.Parse(values["Customer"]);
                        custPackMap.PackerCode = int.Parse(values["Packer"]);
                        custPackMap.SubsiCode = context.Gen_EDISubsies.Where(i => i.SubsiCode == subsiCode).Select(i => i.Id).FirstOrDefault();
                        newListCustPackRel.Add(custPackMap);
                    }
                }
                List<Trn_CustomerToPackerMapper> listFinalCustPackRel = newListCustPackRel.Where(i => !currentCustomerPacker.Any(i1 => i1.CustomerCode == i.CustomerCode && i1.PackerCode == i.PackerCode && i1.SubsiCode == i.SubsiCode) && i.SubsiCode != 0).ToList();
                foreach (Trn_CustomerToPackerMapper value in listFinalCustPackRel)
                {
                    try
                    {
                        context.Trn_CustomerToPackerMapper.Add(value);
                        context.SaveChanges();
                    }
                    catch (Exception exec)
                    {
                        log.Error("Trn_CustomerToPackerMapper db save values :-" + JsonConvert.SerializeObject(value));
                        log.Error("Exception :-" + exec);
                    }
                }
            }
            catch (Exception ex1)
            {
                log.Error("GetCustomerPackerList error -" + ex1);
            }
            log.Debug("UpdateCustomerPackerRelation left");
        }

        private void UpdateSubsiDetails()
        {
            log.Debug("UpdateSubsiDetails Entered");
            try
            {
                List<Gen_EDISubsies> listNewSubsies = new List<Gen_EDISubsies>();
                List<Gen_EDISubsies> listCurrentSubsies = context.Gen_EDISubsies.ToList();
                SubsiSyncService subsiSync = new SubsiSyncService();
                List<Dictionary<string, string>> listCustomerPackRelation = subsiSync.GetSubsiDetails();
                foreach (Dictionary<string, string> values in listCustomerPackRelation)
                {                    
                    Gen_EDISubsies custPackMap = new Gen_EDISubsies();
                    custPackMap.SubsiName = values["SubsiName"];
                    custPackMap.SubsiCode=int.Parse(values["SubsiCode"]);
                    custPackMap.IsEnabled=true;
                    custPackMap.LastUpdatedDate=DateTime.Now;
                    listNewSubsies.Add(custPackMap);
                }
                List<Gen_EDISubsies> listFinal = listNewSubsies.Where(i => !listCurrentSubsies.Any(i1 => i1.SubsiCode == i.SubsiCode || i1.SubsiName == i.SubsiName)).ToList();
                foreach (Gen_EDISubsies value in listFinal)
                {
                    try
                    {
                        context.Gen_EDISubsies.Add(value);
                        context.SaveChanges();
                    }
                    catch (Exception exec)
                    {
                        log.Error("Gen_EDISubsies db save values :-" + JsonConvert.SerializeObject(value));
                        log.Error("Exception :-" + exec);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("UpdateSubsiDetails exception -" + e);
            }
            log.Debug("UpdateSubsiDetails left");
        }

        #endregion
    }
}
