﻿
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Parsers
{
    public class LineContentParser
    {
        public string CodeValue { get; set; }

        public int LinePositionFinder(string sapField,TRANSACTION_TYPES transType,string fileName )
        {
            string transactionCode=transType.ToString();
            using (var context=new GoodpackEDIEntities())
            {
                int sapId = (from s in context.Ref_TransactionTypes
                             where s.TransactionCode == transactionCode
                             select s.Id).FirstOrDefault();
                int sapFieldSequence=(from s in context.Ref_SAPMessageFields
                                      where ((s.SAPFieldName==sapField)&&(s.TransactionId==sapId))
                                      select s.FieldSequence).FirstOrDefault();

                int mappingId = (from s in context.Trn_SubsiToTransactionFileMapper join
                                    p in context.Trn_MappingSubsi
                                    on s.Id equals p.SubsiToTransactionFileMapperId
                                    where s.Filename==fileName                            
                                    select p.Id).FirstOrDefault();

                int linePosition = (from s in context.Trn_MappingConfiguration
                                    where ((s.MappingSubsiId == mappingId) && (s.SapFieldSeq == sapFieldSequence))
                                    select s.SourceFieldSequence).FirstOrDefault()??0;
                if (linePosition == 0)
                {
                  linePosition=  (from s in context.Trn_MappingConfiguration
                                  where ((s.MappingSubsiId == mappingId) && (s.SapFieldSeq == sapFieldSequence))
                                  select s.SourceFieldSeq2).FirstOrDefault() ?? 0;
                }

                return linePosition;
            }           
        }

        public string DecodeValueFinder(string lineContent, int linePosition, string mapperName, string LookUpName)
        {
            string[] contents = lineContent.Split(ConstantUtilities.delimitersNew[0].ToCharArray()[0]);

            string codeValue = contents[(linePosition - 1)];
            CodeValue = codeValue;
            using (var context = new GoodpackEDIEntities())
            {
                int lookUpId = (from s in context.Ref_LookupNames
                                where s.LookupName == LookUpName
                                select s.Id).FirstOrDefault();

                string decodeValue = (from s in context.Trn_LookupValues
                                      where ((s.LookupCode == codeValue) && (s.MapperName == mapperName) && (s.LookupNameId == lookUpId))
                                      select s.LookupDecodeValue).FirstOrDefault();

                if (decodeValue == null)
                {
                    if ((codeValue.StartsWith("3")) || (codeValue.StartsWith("5")) || (codeValue.StartsWith("6")))
                    {
                        return codeValue;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return decodeValue;
                }
            }
        }


    }
}