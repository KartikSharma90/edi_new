﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.FileIO;
using GPArchitecture.CommonTypes;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.Parsers
{
    public class CharacterDelimitedParser
    {
        /// <summary>
        /// Performs character-delimited parsing on the message parameter based
        /// on the provided message specifications.
        /// </summary>
        /// <param name="msgSpecs">Message specification</param>
        /// <param name="message">Message to parse</param>
        /// <param name="isSuccess"></param>
        /// <param name="errorDesc"></param>
        public static List<SourceMessage> Parse(SourceMessage msgSpecs, string message, string delimiter, bool validateExpectedFieldCount, ref bool isSuccess, ref string errorDesc)
        {
            // Init vars
            isSuccess = true;
            int lineCount = 0;
            List<SourceMessage> parsedResult = new List<SourceMessage>();


            try
            {
                using (TextFieldParser parser = new TextFieldParser(new StringReader(message)))
                {
                    parser.Delimiters = new string[] { delimiter };
                    parser.HasFieldsEnclosedInQuotes = true;

                    // Loop through each line
                    while (true)
                    {
                        string[] lineFields = parser.ReadFields();
                        lineCount++;

                        // If line is null, we've reached EOF
                        if (lineFields == null)
                        {
                            break;
                        }

                        // Validate field count against expected field count
                        if (validateExpectedFieldCount && lineFields.Length < msgSpecs.Fields.Count)
                        {
                            isSuccess = false;
                            errorDesc = string.Format(ResourceMessages.FIELD_COUNT_TOO_LOW, lineCount);
                            break;
                        }

                        // Init vars
                        SourceMessage msgLineObj = new SourceMessage(msgSpecs);

                        // Loop through each field item in msgLine
                        foreach (KeyValuePair<int, SourceMessageField> lineFieldItem in msgLineObj.Fields)
                        {
                            // If validation of expected field count is set to false, then break this loop as soon
                            // as the field count of the parsed record is reached.
                            if (!validateExpectedFieldCount
                                && lineFields.Length < lineFieldItem.Key
                                )
                            {
                                break;
                            }

                            SourceMessageField lineField = lineFieldItem.Value;

                            // Set value
                            lineField.Value = lineFields[lineFieldItem.Key - 1];
                        }

                        // Add message line object to list
                        parsedResult.Add(msgLineObj);
                    }
                }
            }
            catch (Microsoft.VisualBasic.FileIO.MalformedLineException)
            {
                isSuccess = false;
                errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

            return parsedResult;
        }
    }
}
