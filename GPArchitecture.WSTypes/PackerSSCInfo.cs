﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.WSTypes
{
   public class PackerSSCInfo
    {
        public string BinNumber { get; set; }
        public string PackerCode { get; set; }
        public string SiNumber { get; set; }
        public string ETADate { get; set; }
        public string ETDDate { get; set; }
        public string custRefNum { get; set; }
        public string BinType { get; set; }
        public string Destination { get; set; }
        public string CustomerName { get; set; }
    }
}
