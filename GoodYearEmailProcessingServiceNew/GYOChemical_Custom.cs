﻿using GPArchitecture.CommonTypes;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Parsers;
using GPArchitecture.Utilities;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEmailProcessingService
{
    class GYOChemical_Custom
    {
        public static string Parse(string message, List<string> boxDetailData)
        {
            // Init vars
            bool isSuccess = true;
            int lineCount = 0;
            StringBuilder batchMessage = new StringBuilder();
            List<GPArchitecture.Utilities.SourceMessage> parsedResult = new List<GPArchitecture.Utilities.SourceMessage>();
            int quantity = 0;
            List<string> deliveryNumbersProcessed = new List<string>();

            try
            {
                // Initialize csv reader object
                CsvReader csvReader = new CsvReader(
                    new StringReader(message)
                    , false, ',');

                // Iterate through each lines in customer message
                while (csvReader.ReadNextRecord())
                {
                    string msgLine = "";
                    // If isSuccess is false, then end processing
                    if (!isSuccess)
                        break;

                    // Verify that all fields in the CSV line have values.  If not, then skip.
                    lineCount++;
                    quantity = 0;

                    // Verify the number of fields derived from a message line
                    // against the expected min. number of fields
                    if (csvReader.FieldCount < 18)
                    {
                        throw new System.Exception("GYO Chemical input file does not adhere to the expected format.  Excel file will be moved to " + Config.getConfig("UnprocessedFilesPath"));
                    }
                    // Check for parsing error
                    if (csvReader.ParseErrorFlag)
                    {
                        isSuccess = false;
                        break;
                    }

                    // Init vars
                    GPArchitecture.Utilities.SourceMessage msgLineObj = new GPArchitecture.Utilities.SourceMessage();
                    msgLineObj.Fields = new Dictionary<int, GPArchitecture.Utilities.SourceMessageField>();
                    // Init fields
                    msgLineObj.Fields.Add(1, new GPArchitecture.Utilities.SourceMessageField("Non Goodyear", 100));
                    msgLineObj.Fields.Add(2, new GPArchitecture.Utilities.SourceMessageField("Document", 100));
                    msgLineObj.Fields.Add(3, new GPArchitecture.Utilities.SourceMessageField("Purchase order number", 100));
                    msgLineObj.Fields.Add(4, new GPArchitecture.Utilities.SourceMessageField("Delivery", 100));
                    msgLineObj.Fields.Add(5, new GPArchitecture.Utilities.SourceMessageField("Item", 100));
                    msgLineObj.Fields.Add(6, new GPArchitecture.Utilities.SourceMessageField("Ship-to", 100));
                    msgLineObj.Fields.Add(7, new GPArchitecture.Utilities.SourceMessageField("Material", 100));
                    msgLineObj.Fields.Add(8, new GPArchitecture.Utilities.SourceMessageField("Name 1", 100));
                    msgLineObj.Fields.Add(9, new GPArchitecture.Utilities.SourceMessageField("ShPt", 100));
                    msgLineObj.Fields.Add(10, new GPArchitecture.Utilities.SourceMessageField("Ac.GI date", 100));
                    msgLineObj.Fields.Add(11, new GPArchitecture.Utilities.SourceMessageField("Means-of-transp.ID", 100));
                    msgLineObj.Fields.Add(12, new GPArchitecture.Utilities.SourceMessageField("Cumul.net weight", 100));
                    msgLineObj.Fields.Add(13, new GPArchitecture.Utilities.SourceMessageField("Cumul.net weight", 100));
                    msgLineObj.Fields.Add(14, new GPArchitecture.Utilities.SourceMessageField("DlvTy", 100));
                    msgLineObj.Fields.Add(15, new GPArchitecture.Utilities.SourceMessageField("Mns of transp.crossing border.", 100));
                    msgLineObj.Fields.Add(16, new GPArchitecture.Utilities.SourceMessageField("Means of transport", 100));
                    msgLineObj.Fields.Add(17, new GPArchitecture.Utilities.SourceMessageField("Sold-to pt", 100));
                    msgLineObj.Fields.Add(18, new GPArchitecture.Utilities.SourceMessageField("Return", 100));
                    msgLineObj.Fields.Add(19, new GPArchitecture.Utilities.SourceMessageField("Quantity", 100));

                    // Map field values
                    msgLineObj.Fields[1].Value = (string.IsNullOrEmpty(csvReader[0])) ? string.Empty : csvReader[0];
                    msgLineObj.Fields[2].Value = (string.IsNullOrEmpty(csvReader[1])) ? string.Empty : csvReader[1];
                    msgLineObj.Fields[3].Value = (string.IsNullOrEmpty(csvReader[2])) ? string.Empty : csvReader[2];
                    msgLineObj.Fields[4].Value = (string.IsNullOrEmpty(csvReader[3])) ? string.Empty : csvReader[3];
                    msgLineObj.Fields[5].Value = (string.IsNullOrEmpty(csvReader[4])) ? string.Empty : csvReader[4];
                    msgLineObj.Fields[6].Value = (string.IsNullOrEmpty(csvReader[5])) ? string.Empty : csvReader[5];
                    msgLineObj.Fields[7].Value = (string.IsNullOrEmpty(csvReader[6])) ? string.Empty : csvReader[6];
                    msgLineObj.Fields[8].Value = (string.IsNullOrEmpty(csvReader[7])) ? string.Empty : csvReader[7];
                    msgLineObj.Fields[9].Value = (string.IsNullOrEmpty(csvReader[8])) ? string.Empty : csvReader[8];
                    msgLineObj.Fields[10].Value = (string.IsNullOrEmpty(csvReader[9])) ? string.Empty : csvReader[9];
                    msgLineObj.Fields[11].Value = (string.IsNullOrEmpty(csvReader[10])) ? string.Empty : csvReader[10];
                    msgLineObj.Fields[12].Value = (string.IsNullOrEmpty(csvReader[11])) ? string.Empty : csvReader[11];
                    msgLineObj.Fields[13].Value = (string.IsNullOrEmpty(csvReader[12])) ? string.Empty : csvReader[12];
                    msgLineObj.Fields[14].Value = (string.IsNullOrEmpty(csvReader[13])) ? string.Empty : csvReader[13];
                    msgLineObj.Fields[15].Value = (string.IsNullOrEmpty(csvReader[14])) ? string.Empty : csvReader[14];
                    msgLineObj.Fields[16].Value = (string.IsNullOrEmpty(csvReader[15])) ? string.Empty : csvReader[15];
                    msgLineObj.Fields[17].Value = (string.IsNullOrEmpty(csvReader[16])) ? string.Empty : csvReader[16];
                    msgLineObj.Fields[18].Value = (string.IsNullOrEmpty(csvReader[17])) ? string.Empty : csvReader[17];
                    //check if file contains 19th column i.e Quantity and populate the heading or value accordingly
                    if (csvReader.FieldCount > 18)
                    {
                        msgLineObj.Fields[19].Value = (string.IsNullOrEmpty(csvReader[18])) ? string.Empty : csvReader[18];
                    }
                    else
                    {
                        msgLineObj.Fields[19].Value = (lineCount == 1) ? "Quantity" : string.Empty;
                    }

                    if (lineCount > 1)
                    {
                        string deliveryValue = msgLineObj.Fields[4].Value;

                        //If the source has multiple same delivery numbers, map to one record only.
                        if (deliveryNumbersProcessed.Contains(deliveryValue))
                        {
                            continue;
                        }
                        else
                        {
                            deliveryNumbersProcessed.Add(deliveryValue);
                        }

                        //count instances of the DeliveryValue in 2nd sheet
                        //Qty = (BoxDetailsData.Length - BoxDetailsData.Replace(DeliveryValue, "").Length) / DeliveryValue.Length;
                        quantity = boxDetailData.Where(x => x.Equals(deliveryValue)).Count();
                        //if count > 0, use this value in the Quantity column
                        //if count =0 and the value in the input file is non-numeric, use this value in the Quantity column
                        //if count =0 and the value in the input file is numeric, retain the value in the Quantity column
                        //if ((quantity == 0 && !GPTools.IsNumeric(msgLineObj.Fields[19].Value)) || (quantity > 0))
                        //{
                        //    msgLineObj.Fields[19].Value = quantity.ToString();
                        //}
                    }

                    msgLineObj.OriginalLineContent = msgLine;

                    // Add message line object to list
                    parsedResult.Add(msgLineObj);
                }

                // Loop through source message records
                foreach (GPArchitecture.Utilities.SourceMessage msgRecord in parsedResult)
                {
                    foreach (KeyValuePair<int, GPArchitecture.Utilities.SourceMessageField> msgField in msgRecord.Fields)
                    {
                        msgField.Value.Value = msgField.Value.Value.Replace("\"", "\"\"");
                        batchMessage.Append("\"").Append(msgField.Value.Value).Append("\"");
                        if (msgRecord.Fields.Count > msgField.Key)
                        {
                            batchMessage.Append(",");
                        }
                    }

                    batchMessage.Append(Environment.NewLine);
                }
            }
            catch (LumenWorks.Framework.IO.Csv.MalformedCsvException)
            {
                isSuccess = false;
                //errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                isSuccess = false;
                throw ex;
            }

            return batchMessage.ToString();
        }

        public static void ProcessGYOChemicalSC(GPCustomerMessage objGPCustomerMessage, string filePath)
        {
            string strBoxDetailsData = string.Empty;
            if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".XLS") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".XLSX"))
            {
                objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(filePath, 0, 0);

                //read data from 2nd sheet
                strBoxDetailsData = ExcelParser.ExcelToCSV(filePath, 0, 1);
            }
            else
            {
               // log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
               // log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileSourceAddress;
                objGPCustomerMessage.ErrorCode = ErrorCodes.V_GYO_CHEM_SC;
                //Sending Mail when cant receive mails
               // GPTools.ProcessErrorAction(FacadeService.Service, objGPCustomerMessage.BatchID, objGPCustomerMessage.ErrorCode, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, DateTime.Now.ToShortDateString(), "GYO Chemical SC input file is not in expected Excel format.", filePath);
            }
            objGPCustomerMessage.Message = GYOChemical_Custom.Parse(objGPCustomerMessage.Message, Tools.CsvStringToList(strBoxDetailsData));
        }
    }
}

