﻿using GoodpackEmailProcessingService;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GoodYearPreprocessingNew
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Starting email service");
            //EmailService service = new EmailService();
            //service.debugger();

#if DEBUG

            EmailService service = new EmailService();
            service.debugger();

# else

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new EmailService() 
			};
            ServiceBase.Run(ServicesToRun);

# endif

            log.Info("Leaving ");
        
        }
    }
}
