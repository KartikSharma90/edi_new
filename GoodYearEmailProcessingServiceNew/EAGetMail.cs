﻿using EAGetMail;
using GPArchitecture.Accessors;
using GPArchitecture.Cache;
using GPArchitecture.EnumsAndConstants;
using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GoodpackEmailProcessingService
{
    //public class EAGetMail
    //{
    //}

    public class EAGetMail
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EAGetMail));
        string strmailserver = null;
        string strusername = null;
        string strpassword = null;
        string protocol = "";
        private TRANSACTION_TYPES objTransactionCode;
       // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EAGetMail(EmailServerAccount objEmailAccount)
        {
            if (objEmailAccount != null)
            {
                strmailserver = objEmailAccount.MailServer;
                strusername = objEmailAccount.Account;
                strpassword = objEmailAccount.Password;
                objTransactionCode = objEmailAccount.TransactionCode;
                protocol = objEmailAccount.Protocol;
            }
        }
        public void ProcessEmail()
        {
            //log.Info("Currently inside the process mail");
            MailClient oclient = null;
            MailServer oserver = null;
            MailInfo[] arrinfo = null;
            MailInfo oinfo = null;
            Mail omail = null;
            string strTmpFileName = "";
            ServerProtocol objServerProtocol;
            int ictr = 0;
            string path = GPArchitecture.Cache.Config.getConfig("FileReceivedTempfolder");
          //  log.Info("Path in process Email" + path);
            string mailFolder = String.Format("{0}\\{1}", path, objTransactionCode.ToString());
          //  log.Info("Mail Folder :" + mailFolder);
            if (!Directory.Exists(mailFolder))
                Directory.CreateDirectory(mailFolder);
            try
            {
                if (protocol == "POP3")
                {
                    objServerProtocol = ServerProtocol.Pop3;
                }
                else
                {
                    objServerProtocol = ServerProtocol.Imap4;
                }
                //retrieve mail server details from config file
                System.Console.WriteLine("connecting to mail server...");
                //  log.Info("Connecting to mail server");
                oclient = new MailClient(GPArchitecture.Cache.Config.getConfig("EAGetMailLicenseKey"));
                //   log.Info("Mail Client");
                //   log.Info("Server:" + strmailserver+",Username:"+strusername+",strpassword");
                oserver = new MailServer(strmailserver, strusername, strpassword, true, ServerAuthType.AuthLogin, objServerProtocol);
                //   log.Info("Server:"+strmailserver);


                oclient.OnAuthorized += new MailClient.OnAuthorizedEventHandler(OnAuthorized);
                oclient.OnConnected += new MailClient.OnConnectedEventHandler(OnConnected);
                oclient.OnIdle += new MailClient.OnIdleEventHandler(OnIdle);
                oclient.OnSecuring += new MailClient.OnSecuringEventHandler(OnSecuring);
                oclient.OnReceivingDataStream += new MailClient.OnReceivingDataStreamEventHandler(OnReceivingDataStream);

                oclient.Connect(oserver);
                oclient.LogFileName=Directory.GetCurrentDirectory()+@"\Email_Log.txt";

                log.Info("Connected to the mail server with username:" + strusername);
                //moving mail to destination folder
                Imap4Folder destFolder = null;
                Imap4Folder UnknownSourceEmails = null;
                if (objServerProtocol == ServerProtocol.Pop3)
                {
                    arrinfo = oclient.GetMailInfos();
                }
                else
                {
                    //source folder(Inbox)
                    Imap4Folder[] folders = oclient.Imap4Folders;                    
                    int folderCount = folders.Length;
                    for (int i = 0; i < folderCount; i++)
                    {
                        Imap4Folder folder = folders[i];
                        if (String.Compare("Inbox", folder.Name.ToUpper(), true) == 0)
                        {
                            //select "HTML" folder
                            oclient.SelectFolder(folder);
                            arrinfo = oclient.GetMailInfos();
                            break;
                        }
                    }
                    for (int i = 0; i < folderCount; i++)
                    {
                        Imap4Folder folder = folders[i];
                        if (String.Compare("Processed Email", folder.Name, true) == 0)
                        {
                            destFolder = folder;
                            break;
                        }
                    }

                    for (int i = 0; i < folderCount; i++)
                    {
                        Imap4Folder folder = folders[i];
                        if (String.Compare(GPArchitecture.Cache.Config.getConfig("UnknownSourceEmails"), folder.Name, true) == 0)
                        {
                            UnknownSourceEmails = folder;
                            break;
                        }
                    }
                    //if the folder does not exists create it
                    if (destFolder == null)
                    {
                        destFolder = oclient.CreateFolder(null, "Processed Email");
                        if (!destFolder.Subscribed)
                            oclient.SubscribeFolder(destFolder);
                    }
                    if (UnknownSourceEmails == null)
                    {
                        UnknownSourceEmails = oclient.CreateFolder(null, GPArchitecture.Cache.Config.getConfig("UnknownSourceEmails"));
                        if (!UnknownSourceEmails.Subscribed)
                            oclient.SubscribeFolder(UnknownSourceEmails);
                    }
                }
                for (ictr = arrinfo.Length - 1; ictr >= 0; ictr--)
                {
                    oinfo = arrinfo[ictr];
                    omail = oclient.GetMail(oinfo);

                    System.DateTime d = System.DateTime.Now;
                    bool blnValidAttachment = false;
                    System.Globalization.CultureInfo cur = new System.Globalization.CultureInfo("en-US");

                    foreach (Attachment attach in omail.Attachments)
                    {
                        string folder = String.Format("{0}\\{1}\\", mailFolder, omail.From.Address);
                        string fileName = String.Format("{0}{1}", folder, attach.Name);

                        FILE_TYPES objFileType;

                        // Check file extension. Do not save if not supported.
                        if (!SupportedFileTypes.IsFileTypeSupported(attach.Name, out objFileType))
                        {
                            continue;
                        }

                        // Check if directory with source email exists
                        Directory.CreateDirectory(folder);
                        blnValidAttachment = true;
                        strTmpFileName = fileName;
                        attach.SaveAs(fileName, true);

                    }
                    omail.Clear();
                    if (objServerProtocol != ServerProtocol.Pop3)
                    {
                        if (blnValidAttachment == false || string.IsNullOrEmpty(EntityMaster.DeriveMapperNameFromEmailSenderAddress(omail.From.Address, objTransactionCode.ToString())))
                        {
                            string errorMsg = "Unknown Email Id";
                            Tools.InsertRejectedMailDetails(objTransactionCode.ToString(), omail.From.Address, strTmpFileName, errorMsg);
                            destFolder = UnknownSourceEmails;
                        }
                        //mark mailfor copy to destination folder and delete
                        oclient.Copy(oinfo, destFolder);
                        oclient.Delete(oinfo);
                    }
                }
                if (objServerProtocol != ServerProtocol.Pop3)
                {
                    //delete mails
                    oclient.Expunge();
                }
            }
            catch (Exception ex)
            {
                // log4net.GlobalContext.Properties["TransactionType"] = objTransactionCode.ToString();
                // log4net.GlobalContext.Properties["FileName"] = strTmpFileName;
                // objILog.Error("Exception in CheckMails", ex);
                log.Error("EAGetMail ProcessEmail:MailFolder-" + mailFolder + "Transaction type:" + objTransactionCode.ToString() + ", File name:" + strTmpFileName);
                throw ex;

                ////Sending Mail when cant receive mails
                //EmailMessage objEmailMessage = new EmailMessage();
                //string[] recipientList = Config.getConfig("AdminEmail").Split(',');
                //objEmailMessage.Subject = "Unable to connect to the email server " + strmailserver;
                //objEmailMessage.From = new System.Net.Mail.MailAddress(Config.getConfig("FromEmailAddress"));
                //SMTP objSMTPSendMail = new SMTP();
                //System.Net.Mail.MailAddressCollection objMailAddressCollection = objEmailMessage.To;
                //string strMessage = ex.Message + " " + ex.StackTrace;
                //objEmailMessage.Body = strMessage;

                //foreach (string recipient in recipientList)
                //{
                //    objMailAddressCollection.Add(new System.Net.Mail.MailAddress(recipient));
                //}
                //objSMTPSendMail.Send(objEmailMessage);
            }
            finally
            {
                if (oclient != null)
                {
                    oclient.Quit();
                    oclient.Close();
                }
            }
        }


        public void ProcessEmailNew()
        {
            using (Imap imap = new Imap())
            {
                string strTmpFileName = "";
                string path = GPArchitecture.Cache.Config.getConfig("FileReceivedTempfolder");
                string mailFolder = String.Format("{0}\\{1}", path, objTransactionCode.ToString());
                try
                {                    
                    ServerProtocol objServerProtocol;
                    if (!Directory.Exists(mailFolder))
                        Directory.CreateDirectory(mailFolder);
                    if (protocol == "POP3")
                    {
                        objServerProtocol = ServerProtocol.Pop3;
                    }
                    else
                    {
                        objServerProtocol = ServerProtocol.Imap4;
                    }
                    imap.ConnectSSL(strmailserver); //  for SSL
                    imap.UseBestLogin(strusername, strpassword);

                    //imap.Connect("pod51012.outlook.com"); //  for SSL
                    //imap.UseBestLogin("ediso@goodpack.com", "pass@123");

                    imap.SelectInbox();
                    List<long> uids = imap.Search(Flag.All);
                    List<FolderInfo> all = imap.GetFolders();
                    string unknownSourceEmailFolder = GPArchitecture.Cache.Config.getConfig("UnknownSourceEmails");
                    bool existsProcessedFolder = all.Find(x => x.Name == "Processed Email") != null;
                    if (!existsProcessedFolder)
                        imap.CreateFolder("Processed Email");
                    bool existsUnknownFOlder = all.Find(x => x.Name == unknownSourceEmailFolder) != null;
                    if (!existsUnknownFOlder)
                        imap.CreateFolder(unknownSourceEmailFolder);

                    foreach (long uid in uids)
                    {
                        var eml = imap.GetMessageByUID(uid);
                        string destFolder = "Processed Email";
                        IMail email = new MailBuilder().CreateFromEml(eml);
                        bool blnValidAttachment = false;
                        foreach (MimeData mime in email.Attachments)
                        {
                            string folder = String.Format("{0}\\{1}\\", mailFolder, email.From.Select(i => i.Address).FirstOrDefault());
                            string fileName = String.Format("{0}{1}", folder, mime.FileName);
                            FILE_TYPES objFileType;

                            if (!SupportedFileTypes.IsFileTypeSupported(mime.FileName, out objFileType))
                            {
                                continue;
                            }
                            Directory.CreateDirectory(folder);
                            blnValidAttachment = true;
                            strTmpFileName = fileName;
                            mime.Save(fileName);                            

                        }                        
                        if (objServerProtocol != ServerProtocol.Pop3)
                        {
                            if (blnValidAttachment == false || string.IsNullOrEmpty(EntityMaster.DeriveMapperNameFromEmailSenderAddress(email.From.Select(i => i.Address).FirstOrDefault().ToString(), objTransactionCode.ToString())))
                            {
                                string errorMsg = "Unknown Email Id";
                                Tools.InsertRejectedMailDetails(objTransactionCode.ToString(), email.From.Select(i => i.Address).FirstOrDefault().ToString(), strTmpFileName, errorMsg);
                                destFolder = unknownSourceEmailFolder;
                            }
                            //move mail to new destination --Processed Email/UnknownSourceEmails
                            imap.MoveByUID(uid, destFolder);
                         }
                    }
                }
                catch(Exception e)
                {
                    log.Error("EAGetMail ProcessEmail:MailFolder-" + mailFolder + "Transaction type:" + objTransactionCode.ToString() + ", File name:" + strTmpFileName);
                    throw e;
                }
                finally
                {
                    imap.Close();
                }
            }
        }


        #region EAGetMail Event Handler
        private void OnConnected(object sender, ref bool cancel)
        {
           // objILog.Debug("Connected ...");
        }

        private void OnQuit(object sender, ref bool cancel)
        {
           // objILog.Debug("Quit ...");
        }

        private void OnReceivingDataStream(object sender, MailInfo info, int received, int total, ref bool cancel)
        {
           // objILog.Debug("Receving ...");
        }

        private void OnIdle(object sender, ref bool cancel)
        {
            //objILog.Debug("Idle ...");
        }

        private void OnAuthorized(object sender, ref bool cancel)
        {
            //objILog.Debug("Authorized ...");
        }

        private void OnSecuring(object sender, ref bool cancel)
        {
           // objILog.Debug("Securing ...");
        }
        #endregion
    }

    public class EmailServerAccount
    {
        private string strMailServer;
        private string strAccount;
        private string strPassword;
        private string strProtocol;

        public string MailServer
        {
            set { strMailServer = value; }
            get { return strMailServer; }
        }
        public string Account
        {
            set { strAccount = value; }
            get { return strAccount; }
        }
        public string Password
        {
            set { strPassword = value; }
            get { return strPassword; }
        }
        public string Protocol
        {
            set { strProtocol = value; }
            get { return strProtocol; }
        }
        public TRANSACTION_TYPES TransactionCode;
    }
}
