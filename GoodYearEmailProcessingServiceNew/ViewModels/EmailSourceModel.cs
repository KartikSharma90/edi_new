﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEmailProcessingService.ViewModels
{
    class EmailSourceModel
    {
        public string MailServer { get; set; }
        public string Protocol { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string TransactionCode { get; set; }
    }
}
