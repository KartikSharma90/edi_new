A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" Calendar
* LIST (\HasNoChildren) "/" Clutter
* LIST (\HasChildren) "/" Contacts
* LIST (\HasNoChildren \Trash) "/" "Deleted Items"
* LIST (\HasNoChildren \Drafts) "/" Drafts
* LIST (\HasNoChildren) "/" GmailRedirect
* LIST (\Marked \HasNoChildren) "/" INBOX
* LIST (\HasNoChildren) "/" Journal
* LIST (\HasNoChildren \Junk) "/" "Junk Email"
* LIST (\HasNoChildren) "/" Notes
* LIST (\HasNoChildren) "/" Outbox
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren \Sent) "/" "Sent Items"
* LIST (\HasNoChildren) "/" Tasks
* LIST (\HasNoChildren) "/" UnknownSource
A0004 OK LIST completed.
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" UnknownSource
A0005 OK LSUB completed.
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" Calendar
* LIST (\HasNoChildren) "/" Clutter
* LIST (\HasChildren) "/" Contacts
* LIST (\HasNoChildren \Trash) "/" "Deleted Items"
* LIST (\HasNoChildren \Drafts) "/" Drafts
* LIST (\HasNoChildren) "/" GmailRedirect
* LIST (\Marked \HasNoChildren) "/" INBOX
* LIST (\HasNoChildren) "/" Journal
* LIST (\HasNoChildren \Junk) "/" "Junk Email"
* LIST (\HasNoChildren) "/" Notes
* LIST (\HasNoChildren) "/" Outbox
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren \Sent) "/" "Sent Items"
* LIST (\HasNoChildren) "/" Tasks
* LIST (\HasNoChildren) "/" UnknownSource
A0004 OK LIST completed.
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" UnknownSource
A0005 OK LSUB completed.
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" Calendar
* LIST (\HasNoChildren) "/" Clutter
* LIST (\HasChildren) "/" Contacts
* LIST (\HasNoChildren \Trash) "/" "Deleted Items"
* LIST (\HasNoChildren \Drafts) "/" Drafts
* LIST (\HasNoChildren) "/" GmailRedirect
* LIST (\Marked \HasNoChildren) "/" INBOX
* LIST (\HasNoChildren) "/" Journal
* LIST (\HasNoChildren \Junk) "/" "Junk Email"
* LIST (\HasNoChildren) "/" Notes
* LIST (\HasNoChildren) "/" Outbox
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren \Sent) "/" "Sent Items"
* LIST (\HasNoChildren) "/" Tasks
* LIST (\HasNoChildren) "/" UnknownSource
A0004 OK LIST completed.
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" UnknownSource
A0005 OK LSUB completed.
A0006 LIST "" "INBOX"
* LIST (\Marked \HasNoChildren) "/" INBOX
A0006 OK LIST completed.
A0007 SELECT "INBOX"
* 0 EXISTS
* 0 RECENT
* FLAGS (\Seen \Answered \Flagged \Deleted \Draft $MDNSent)
* OK [PERMANENTFLAGS (\Seen \Answered \Flagged \Deleted \Draft $MDNSent)] Permanent flags
* OK [UIDVALIDITY 14] UIDVALIDITY value
* OK [UIDNEXT 6778] The next unique identifier value
A0007 OK [READ-WRITE] SELECT completed.
A0008 EXPUNGE
* 0 EXISTS
A0008 OK EXPUNGE completed.
A0009 LOGOUT
* BYE Microsoft Exchange Server 2016 IMAP4 server signing off.
A0009 OK LOGOUT completed.
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 73] Predicted next UID.
* OK [HIGHESTMODSEQ 18811]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 126] Predicted next UID.
* OK [HIGHESTMODSEQ 27338]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 1 EXISTS
* 0 RECENT
* OK [UIDNEXT 17] Predicted next UID.
* OK [HIGHESTMODSEQ 2479]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 FETCH 1:* (UID RFC822.SIZE FLAGS)
* 1 FETCH (UID 16 RFC822.SIZE 67653 FLAGS ())
A0008 OK Success
A0009 UID FETCH 16 (BODY.PEEK[])
A0010 UID COPY 16 "Processed Email"
* 1 FETCH (UID 16 FLAGS ())
A0010 OK [COPYUID 12 16 5] (Success)
A0011 UID STORE 16 +FLAGS.SILENT (\Deleted)
* 1 EXPUNGE
* 0 EXISTS
A0011 OK Success
A0012 EXPUNGE
A0012 OK Success
A0013 EXPUNGE
A0013 OK Success
A0014 LOGOUT
* BYE LOGOUT Requested
A0014 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 73] Predicted next UID.
* OK [HIGHESTMODSEQ 18811]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 126] Predicted next UID.
* OK [HIGHESTMODSEQ 27338]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 17] Predicted next UID.
* OK [HIGHESTMODSEQ 2508]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 1 EXISTS
* 0 RECENT
* OK [UIDNEXT 18] Predicted next UID.
* OK [HIGHESTMODSEQ 2550]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 FETCH 1:* (UID RFC822.SIZE FLAGS)
* 1 FETCH (UID 17 RFC822.SIZE 67654 FLAGS ())
A0008 OK Success
A0009 UID FETCH 17 (BODY.PEEK[])
A0010 UID COPY 17 "Processed Email"
* 1 FETCH (UID 17 FLAGS ())
A0010 OK [COPYUID 12 17 6] (Success)
A0011 UID STORE 17 +FLAGS.SILENT (\Deleted)
* 1 EXPUNGE
* 0 EXISTS
A0011 OK Success
A0012 EXPUNGE
A0012 OK Success
A0013 EXPUNGE
A0013 OK Success
A0014 LOGOUT
* BYE LOGOUT Requested
A0014 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 73] Predicted next UID.
* OK [HIGHESTMODSEQ 18811]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 126] Predicted next UID.
* OK [HIGHESTMODSEQ 27338]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
A0004 LIST "" "*"
* LIST (\HasNoChildren) "/" "INBOX"
* LIST (\HasNoChildren) "/" "Processed Email"
* LIST (\HasNoChildren) "/" "UnknownSource"
* LIST (\HasChildren \Noselect) "/" "[Gmail]"
* LIST (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LIST (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LIST (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LIST (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LIST (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LIST (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LIST (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0004 OK Success
A0005 LSUB "" "*"
* LSUB (\HasNoChildren) "/" "INBOX"
* LSUB (\HasNoChildren) "/" "Processed Email"
* LSUB (\HasNoChildren) "/" "UnknownSource"
* LSUB (\HasChildren \Noselect) "/" "[Gmail]"
* LSUB (\All \HasNoChildren) "/" "[Gmail]/All Mail"
* LSUB (\Drafts \HasNoChildren) "/" "[Gmail]/Drafts"
* LSUB (\HasNoChildren \Important) "/" "[Gmail]/Important"
* LSUB (\HasNoChildren \Sent) "/" "[Gmail]/Sent Mail"
* LSUB (\HasNoChildren \Junk) "/" "[Gmail]/Spam"
* LSUB (\Flagged \HasNoChildren) "/" "[Gmail]/Starred"
* LSUB (\HasNoChildren \Trash) "/" "[Gmail]/Trash"
A0005 OK Success
A0006 LIST "" "INBOX"
* LIST (\HasNoChildren) "/" "INBOX"
A0006 OK Success
A0007 SELECT "INBOX"
* FLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing)
* OK [PERMANENTFLAGS (\Answered \Flagged \Draft \Deleted \Seen $NotPhishing $Phishing \*)] Flags permitted.
* OK [UIDVALIDITY 1] UIDs valid.
* 0 EXISTS
* 0 RECENT
* OK [UIDNEXT 18] Predicted next UID.
* OK [HIGHESTMODSEQ 2578]
A0007 OK [READ-WRITE] INBOX selected. (Success)
A0008 EXPUNGE
A0008 OK Success
A0009 LOGOUT
* BYE LOGOUT Requested
A0009 OK 73 good day (Success)
