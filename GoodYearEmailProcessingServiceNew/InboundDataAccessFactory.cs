﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodpackEmailProcessingService.ViewModels;
using GPArchitecture.DataLayer.Models;
using log4net;

namespace GoodpackEmailProcessingService
{
    class InboundDataAccessFactory
    {
       // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //public InboundDataAccessFactory(string connString)
        //{
        //    strConnectionString = connString;

        //}
        private static readonly ILog log = LogManager.GetLogger(typeof(InboundDataAccessFactory));
        public IList<EmailSourceModel> getEmailSourcesAccounts()
        {
            IList<EmailSourceModel> emailSourceModel = null;
            try
            {               
                using (var context=new GoodpackEDIEntities())
                {
                   emailSourceModel =(from s in context.Ref_EmailSources
                                      where (s.IsEnabled == true)
                                      select new EmailSourceModel()
                                                                {
                                                                    Account = s.Account,
                                                                    MailServer = s.MailServer,
                                                                    Password = s.Password,
                                                                    Protocol = s.Protocol,
                                                                    TransactionCode = s.TransactionCode
                                                                }).ToList();
                }
            }
            catch (Exception exp)
            {
                log.Error("Error while loading CFG_EMAIL_SOURCES table"+ exp.Message);
                throw exp;
            }
           // objILog.Debug("Leaving getEmailSourcesAccounts method");
            return emailSourceModel;
        }
    

    }
}

