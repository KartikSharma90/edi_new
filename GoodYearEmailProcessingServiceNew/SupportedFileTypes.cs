﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.Cache.ViewModels;
using GPArchitecture.CommonTypes;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;



namespace GoodpackEmailProcessingService
{
    public class SupportedFileTypes
    {
        private static List<string> objFileTypes ;
        public static List<string> GetSupportedFileTypes()
        {
            if (objFileTypes == null)
            {
                objFileTypes = new List<string>();
                IList<FileTypes> fileTypes = null;
                using (var context = new GoodpackEDIEntities())
                {
                    fileTypes = (from s in context.Ref_FileTypes
                                 select new FileTypes
                                 {
                                     Id = s.Id,
                                     FileType = s.FileType
                                 }).ToList();
                }


                foreach (var item in fileTypes)
                {
                    objFileTypes.Add(item.FileType);
                }
            }
            return objFileTypes;
        }
        public static bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
        {
            bool blnValidFileType = false;
            IList<string> lstFileExtns = GetSupportedFileTypes();
            enumFileType = FILE_TYPES.CSV;
            foreach (string strFileExt in lstFileExtns)
            {
                if (strFileName.EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (strFileExt.ToLower() == ".csv")
                        enumFileType = FILE_TYPES.CSV;
                    else
                        enumFileType = FILE_TYPES.TEXT;
                    blnValidFileType = true;
                    break;
                }
            }
            return blnValidFileType;
        }

    }
}




