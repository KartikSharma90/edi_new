﻿//using GPArchitecture.CommonTypes;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace GoodpackEmailProcessingService
//{
//    class Yokohama_Custom
//    {
//        public static string Parse(string message)
//        {
//            bool isSuccess = true;
//            bool headerReached = false;
//            int ctr;
//            StringBuilder batchMessage = new StringBuilder();
//            List<SourceMessage> parsedResult = new List<SourceMessage>();
//            Dictionary<string, string> SINumberKeys = new Dictionary<string, string>();
//            Dictionary<string, string> PlantKeys = new Dictionary<string, string>();
//            Dictionary<string, string> FillerKeys = new Dictionary<string, string>();

//            // Initialize csv reader object
//            CsvReader csvReader = new CsvReader(
//                new StringReader(message)
//                , false // has headers
//                , ',');

//            // get the custom fileds for yokohama
//            DataTable objCFGtable = YokohomaFields.getFieldValues();
//            foreach (DataRow drCFG in objCFGtable.Rows)
//            {
//                switch (drCFG["FieldName"].ToString())
//                {
//                    case FacadeServiceConstants.SINumber:
//                        {
//                            SINumberKeys.Add(drCFG["key"].ToString(), drCFG["value"].ToString());
//                            break;
//                        }
//                    case FacadeServiceConstants.Plant:
//                        {
//                            PlantKeys.Add(drCFG["key"].ToString(), drCFG["value"].ToString());
//                            break;
//                        }
//                    case FacadeServiceConstants.Filler:
//                        {
//                            FillerKeys.Add(drCFG["key"].ToString(), drCFG["value"].ToString());
//                            break;
//                        }

//                }//-- End switch
//            }

//            // Iterate through each lines in customer message
//            while (csvReader.ReadNextRecord())
//            {
//                // If isSuccess is false, then end processing
//                if (!isSuccess)
//                    break;

//                int iContainsFieldCount = 0;
//                for (ctr = 0; ctr < csvReader.FieldCount; ctr++)
//                {
//                    //Modified to count number of non-empty fields
//                    if (!string.IsNullOrEmpty(csvReader[ctr]))
//                    {
//                        //csvContainsFieldValue = true;
//                        iContainsFieldCount++;
//                        //break;
//                    }
//                }

//                //If number of non-empty fields is less than a minimum configured number, ignore the record
//                // If field count is greater than 6 ,then check if it's the field header row
//                if (iContainsFieldCount < 10)
//                {
//                    continue;
//                }
//                else if (headerReached == false)
//                {
//                    if ((!string.IsNullOrEmpty(csvReader[0]) && csvReader[0].Trim().ToUpper() == "CONT.NO.")
//                        && (!string.IsNullOrEmpty(csvReader[1]) && csvReader[1].Trim().ToUpper() == "SEQ.NO.")
//                        && (!string.IsNullOrEmpty(csvReader[2]) && csvReader[2].Trim().ToUpper() == "SHIPMENT")
//                        && (!string.IsNullOrEmpty(csvReader[3]) && csvReader[3].Trim().ToUpper() == "MONTH")
//                        && (!string.IsNullOrEmpty(csvReader[4]) && csvReader[4].Trim().ToUpper() == "GRADE")
//                        )
//                    {
//                        headerReached = true;
//                    }
//                }

//                // Check for parsing error
//                if (csvReader.ParseErrorFlag)
//                {
//                    isSuccess = false;
//                    break;
//                }

//                // Init vars
//                SourceMessage msgLineObj = new SourceMessage();
//                msgLineObj.Fields = new Dictionary<int, SourceMessageField>();
//                // Init fields
//                msgLineObj.Fields.Add(1, new SourceMessageField("CONT.NO.", 100));
//                msgLineObj.Fields.Add(2, new SourceMessageField("SEQ.NO.", 100));
//                msgLineObj.Fields.Add(3, new SourceMessageField("SHIPMENT", 100));
//                msgLineObj.Fields.Add(4, new SourceMessageField("MONTH", 100));
//                msgLineObj.Fields.Add(5, new SourceMessageField("GRADE", 100));
//                msgLineObj.Fields.Add(6, new SourceMessageField("CODE NO.", 100));
//                msgLineObj.Fields.Add(7, new SourceMessageField("SUPPLIER", 100));
//                msgLineObj.Fields.Add(8, new SourceMessageField("FACTORY", 100));
//                msgLineObj.Fields.Add(9, new SourceMessageField("PACKING", 100));
//                msgLineObj.Fields.Add(10, new SourceMessageField("QTY (KG)", 100));
//                msgLineObj.Fields.Add(11, new SourceMessageField("MB units", 100));
//                msgLineObj.Fields.Add(12, new SourceMessageField("FOB", 100));
//                msgLineObj.Fields.Add(13, new SourceMessageField("CARRIER", 100));
//                msgLineObj.Fields.Add(14, new SourceMessageField("1ST VSSL.", 100));
//                msgLineObj.Fields.Add(15, new SourceMessageField("ETD", 100));
//                msgLineObj.Fields.Add(16, new SourceMessageField("Destn", 100));
//                msgLineObj.Fields.Add(17, new SourceMessageField("ETA", 100));
//                msgLineObj.Fields.Add(18, new SourceMessageField("remark", 100));
//                msgLineObj.Fields.Add(19, new SourceMessageField("SI Number", 100));
//                msgLineObj.Fields.Add(20, new SourceMessageField("Plant", 100));
//                msgLineObj.Fields.Add(21, new SourceMessageField("Filler", 100));

//                // Map field values
//                msgLineObj.Fields[1].Value = (string.IsNullOrEmpty(csvReader[0])) ? string.Empty : csvReader[0];
//                msgLineObj.Fields[2].Value = (string.IsNullOrEmpty(csvReader[1])) ? string.Empty : csvReader[1];
//                msgLineObj.Fields[3].Value = (string.IsNullOrEmpty(csvReader[2])) ? string.Empty : csvReader[2];
//                msgLineObj.Fields[4].Value = (string.IsNullOrEmpty(csvReader[3])) ? string.Empty : csvReader[3];
//                msgLineObj.Fields[5].Value = (string.IsNullOrEmpty(csvReader[4])) ? string.Empty : csvReader[4];
//                msgLineObj.Fields[6].Value = (string.IsNullOrEmpty(csvReader[5])) ? string.Empty : csvReader[5];
//                msgLineObj.Fields[7].Value = (string.IsNullOrEmpty(csvReader[6])) ? string.Empty : csvReader[6];
//                msgLineObj.Fields[8].Value = (string.IsNullOrEmpty(csvReader[7])) ? string.Empty : csvReader[7];
//                msgLineObj.Fields[9].Value = (string.IsNullOrEmpty(csvReader[8])) ? string.Empty : csvReader[8];
//                msgLineObj.Fields[10].Value = (string.IsNullOrEmpty(csvReader[9])) ? string.Empty : csvReader[9];
//                msgLineObj.Fields[11].Value = (string.IsNullOrEmpty(csvReader[10])) ? string.Empty : csvReader[10];
//                msgLineObj.Fields[12].Value = (string.IsNullOrEmpty(csvReader[11])) ? string.Empty : csvReader[11];
//                msgLineObj.Fields[13].Value = (string.IsNullOrEmpty(csvReader[12])) ? string.Empty : csvReader[12];
//                msgLineObj.Fields[14].Value = (string.IsNullOrEmpty(csvReader[13])) ? string.Empty : csvReader[13];
//                msgLineObj.Fields[15].Value = (string.IsNullOrEmpty(csvReader[14])) ? string.Empty : csvReader[14];
//                msgLineObj.Fields[16].Value = (string.IsNullOrEmpty(csvReader[15])) ? string.Empty : csvReader[15];
//                msgLineObj.Fields[17].Value = (string.IsNullOrEmpty(csvReader[16])) ? string.Empty : csvReader[16];
//                msgLineObj.Fields[18].Value = (string.IsNullOrEmpty(csvReader[17])) ? string.Empty : csvReader[17];

//                // Map SI Number
//                if (msgLineObj.Fields[1].Value.Trim().ToUpper() == "CONT.NO.")
//                {
//                    msgLineObj.Fields[19].Value = "SI#"; // Column name
//                }
//                if (string.IsNullOrEmpty(msgLineObj.Fields[2].Value))
//                {
//                    msgLineObj.Fields[19].Value = msgLineObj.Fields[1].Value;
//                }
//                else if (msgLineObj.Fields[2].Value.ToUpper().StartsWith("IPO")
//                    || msgLineObj.Fields[2].Value.ToUpper().StartsWith("IP0")) // if starts with IPO, trim IPO part
//                {
//                    msgLineObj.Fields[19].Value = msgLineObj.Fields[1].Value + "-" + msgLineObj.Fields[2].Value.Substring(3, msgLineObj.Fields[2].Value.Length - 3);
//                }
//                else
//                {
//                    msgLineObj.Fields[19].Value = msgLineObj.Fields[1].Value + "-" + msgLineObj.Fields[2].Value;
//                }





//                // Map Plant
//                if (msgLineObj.Fields[1].Value.Trim().ToUpper() == "CONT.NO.")
//                {
//                    msgLineObj.Fields[20].Value = "PLANT";
//                }
//                /*   else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("YR"))
//                   {
//                       msgLineObj.Fields[20].Value = "YRCJP";
//                   }
//                   else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("NRC"))
//                   {
//                       msgLineObj.Fields[20].Value = "YRCUS";
//                   }
//                   else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("IPO")
//                       || msgLineObj.Fields[1].Value.ToUpper().StartsWith("IP0"))
//                   {
//                       msgLineObj.Fields[20].Value = "YRCPH";
//                   }
//                   else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("CHZY"))
//                   {
//                       msgLineObj.Fields[20].Value = "YRCZY";
//                   }
//                   else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("CSZY"))
//                   {
//                       msgLineObj.Fields[20].Value = "YRCSZ";
//                   }
//                   else
//                   {
//                       msgLineObj.Fields[20].Value = string.Empty;
//                   }*/

//                else
//                {

//                    foreach (KeyValuePair<string, string> keyvalue in PlantKeys)
//                    {
//                        if (msgLineObj.Fields[1].Value.ToUpper().StartsWith(keyvalue.Key))
//                        {
//                            msgLineObj.Fields[20].Value = keyvalue.Value;
//                            break;
//                        }
//                        else
//                        {
//                            msgLineObj.Fields[20].Value = string.Empty;
//                        }
//                    }
//                }



//                // Map empty string to filler field
//                if (msgLineObj.Fields[1].Value.Trim().ToUpper() == "CONT.NO.")
//                {
//                    msgLineObj.Fields[21].Value = "FILLER";
//                }
//                else
//                {
//                    msgLineObj.Fields[21].Value = string.Empty;
//                }

//                // Add message line object to list
//                parsedResult.Add(msgLineObj);
//            }//-- End looping on CSV reader

//            // If 
//            if (!headerReached)
//            {
//                throw new System.Exception("Yokohama input file does not adhere to the expected format.  The column fields may be misaligned.  Please review the first worksheet in the Excel document and check for blank leading columns.  Excel file will be moved to " + Config.getConfig("UnprocessedFilesPath"));
//            }


//            // Loop through source message records
//            foreach (SourceMessage msgRecord in parsedResult)
//            {
//                foreach (KeyValuePair<int, SourceMessageField> msgField in msgRecord.Fields)
//                {
//                    msgField.Value.Value = msgField.Value.Value.Replace("\"", "\"\"");
//                    batchMessage.Append("\"").Append(msgField.Value.Value).Append("\"");
//                    if (msgRecord.Fields.Count > msgField.Key)
//                    {
//                        batchMessage.Append(",");
//                    }
//                }

//                batchMessage.Append(Environment.NewLine);
//            }

//            return batchMessage.ToString();
//        }
//    }
//}
