﻿using GoodpackEmailProcessingService.ViewModels;
using GPArchitecture.Accessors;
using GPArchitecture.Cache;
using GPArchitecture.CommonTypes;
using GPArchitecture.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using GPArchitecture.Parsers;
using log4net;
using GPArchitecture.DataLayer.Models;
using System.Net.Mail;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Utilities;
using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
using GPArchitecture.Models;
using GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer;


namespace GoodpackEmailProcessingService
{
    class EAGetmailEndPoint : InboundEndPoint
    {
        IList<EmailServerAccount> objLstEmailServerAccount;
        public bool isProcessing = false;
        FilePolling objFilePolling;
        InboundDataAccessFactory objDataAccess;
        private string strProcessingFolder;
        //public Timer EmailEndPointProcessTimer;
        private DateTime lastTimeErrorNotificationWasSentDuetoEmailFailure = System.DateTime.Now.AddHours(-2);
        private static ILog log = LogManager.GetLogger(typeof(EAGetmailEndPoint));

        public override void Initialize()
        {
            log.Debug("Entering Email End point Initialize method");            
            base.Initialize();
            if (isProcessing == false)
            {
                objDataAccess = new InboundDataAccessFactory();
                IList<EmailSourceModel> objdtFileSourcepaths = objDataAccess.getEmailSourcesAccounts();                
                Type TranType = typeof(TRANSACTION_TYPES);
                objLstEmailServerAccount = new List<EmailServerAccount>();
                foreach (var objEmailAcc in objdtFileSourcepaths)
                {
                    EmailServerAccount objEmailServerAccount = new EmailServerAccount();
                    objEmailServerAccount.MailServer = objEmailAcc.MailServer;
                    objEmailServerAccount.Account = objEmailAcc.Account;
                    objEmailServerAccount.Password = objEmailAcc.Password;
                    objEmailServerAccount.Protocol = objEmailAcc.Protocol;
                    objEmailServerAccount.TransactionCode = (TRANSACTION_TYPES)EnumConverTo.GenericEnumConverter(typeof(TRANSACTION_TYPES), objEmailAcc.TransactionCode);
                    objLstEmailServerAccount.Add(objEmailServerAccount);
                }        
                StartEvent();
            }
            log.Debug("Leaving Email End point Initialize method");
        }

        private void StartEvent()
        {
            log.Info("Entering Start Event -" + isProcessing);            
            this.Process();
        }
        protected override bool Validate(GPCustomerMessage objGPCustomerMessage)
        {
           // objILog.Debug("Entering  Validate event");

            // Init vars
            bool isValid = true;

            // Run generic validator
            isValid &= base.Validate(objGPCustomerMessage);

            return isValid;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objGPCustomerMessage"></param>
        protected override void GetMessageDetails(GPCustomerMessage objGPCustomerMessage)
        {
            // Get message details
            base.GetMessageDetails(objGPCustomerMessage);

            // Decode the source customer head company which sent the message
            //if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI)
            //{
            //    //Defaut Headcompany and customercode for ACI transaction
            //    string mailID = "acitestentity@goodpack.com";
            //    objGPCustomerMessage.CustomerHeadCompany = EntityMaster.DeriveMapperNameFromEmailSenderAddress(mailID);
            //    objGPCustomerMessage.CustomerCode = 502633;
            //    Customer objCustomer = EntityMaster.GetCustomer(objGPCustomerMessage.CustomerCode);
            //    objGPCustomerMessage.CustomerName = objCustomer.CustomerName;
            //}
            //else
            //{
            objGPCustomerMessage.MapperName = EntityMaster.DeriveMapperNameFromEmailSenderAddress(objGPCustomerMessage.BatchFileSourceAddress.ToLower(), objGPCustomerMessage.TransactionType.ToString());
             //}

            if (objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName) || GPArchitecture.Cache.Config.getConfigList("SumitomoShipmentConfirmation").Contains(objGPCustomerMessage.MapperName))
            {
                objGPCustomerMessage.SubsiName = "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE";
            }

                if (string.IsNullOrEmpty(objGPCustomerMessage.MapperName))
                {
                    //  log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                    //  log4net.GlobalContext.Properties["FileName"] = string.Format("{0} {1}.", objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                    //  objILog.Warn("Source customer head company of message cannot be identified.");
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;
                    return;
                }
                else
                {
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    objGPCustomerMessage.Username = EntityMaster.DeriveUsernameFromSubsiId(objGPCustomerMessage.SubsiId);
                }

            // Set File path
            string filePath = strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;


            if ((objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName)) && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
            {
                if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                {
                    objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(filePath, 21, 0);
                }
                objGPCustomerMessage.Message = ExcelParser.GetSumitomoFileContent(objGPCustomerMessage.Message);
                //placing delimeters inside file content
                objGPCustomerMessage.Message = objGPCustomerMessage.Message.Replace(',', '|');
            }


            // CR 2013-001 - Interim solution for YRC customer pre-processing
            //if (objGPCustomerMessage.CustomerHeadCompany.ToUpper() == "YOKOHAMA CORP" && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
            //{
            //    if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
            //    {
            //        objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(filePath, 19, 0);
            //    }
            //    objGPCustomerMessage.Message = Yokohama_Custom.Parse(objGPCustomerMessage.Message);
            //}
            //pre-processing for GYO Chemical
            //else if (objGPCustomerMessage.CustomerHeadCompany.ToUpper() == Config.getConfig("GYOChemicalHeadCompany").ToUpper() && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SC)
            //{
            //    GYOChemical_Custom.ProcessGYOChemicalSC(objGPCustomerMessage, filePath);
            //}
            //else if (objGPCustomerMessage.CustomerHeadCompany.ToUpper() == "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE" && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
            //{
            //    if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
            //    {
            //        objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(filePath, 21, 0);
            //    }
            //    objGPCustomerMessage.Message = ExcelParser.GetSumitomoFileContent(objGPCustomerMessage.Message);
            //}
            // Get the customer name and code from the source file.  Use the mapper to achieve this.
            //if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI)
            //{
            //    GPMapper mapper = null;
            //    MapOutput mapResult = null;
            //    try
            //    {
            //        // Set mapping service
            //        objGPCustomerMessage.Service = "GPMapper";

            //        mapper = new GPMapper(objGPCustomerMessage);
            //        mapResult = mapper.ProcessMap(true);
            //        objGPCustomerMessage.CustomerCode = mapResult.CustomerCode;
            //        objGPCustomerMessage.CustomerName = mapResult.CustomerName;
            //    }
            //    finally
            //    {

            //    }
            //}
        }


        /// <summary>
        /// Processes this instance.
        /// </summary>
        protected override void Process()
        {
            log.Info("Entering Process");
            isProcessing = true;
            GPCustomerMessage objGPCustomerMessage = new GPCustomerMessage();
            try
            {
                // Get temp attachment folder
                string strEmailTempFolder = GPArchitecture.Cache.Config.getConfig("FileReceivedTempfolder");
                string filePath;

                foreach (EmailServerAccount objEmailServerAccount in objLstEmailServerAccount)
                {
                    EAGetMail objEAGetMail = new EAGetMail(objEmailServerAccount);

                    // Get temp attachment folder
                    strProcessingFolder = String.Format("{0}\\{1}", strEmailTempFolder, objEmailServerAccount.TransactionCode.ToString());
                   log.Info("Processing folder:" + strProcessingFolder);
                   log.Info("Entering into Process mail");
                   try
                   {
                       objEAGetMail.ProcessEmail();                      
                   }
                   catch (Exception e)
                   {
                       log.Error("Process Email error-" + e);
                       continue;
                   }                  
                    IList<string> objFolderPaths = new List<string>();                   
                    List<string> fileExtensions = SupportedFileTypes.GetSupportedFileTypes();
                    objFolderPaths.Add(strProcessingFolder);
                    objFilePolling = new FilePolling(objFolderPaths, fileExtensions);
                    FileInfo[] objFileInfos = objFilePolling.GetFiles();
                    bool blnValidFile;
                    foreach (FileInfo objFileinfo in objFileInfos)
                    {
                        try
                        {
                            // Set validation success flag
                            blnValidFile = true;

                            // Set email address
                            string strEmail = objFileinfo.Directory.Name;

                            // Create and set the GP Customer Message object
                            objGPCustomerMessage = new GPCustomerMessage();
                            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                            objGPCustomerMessage.BatchFileSourceAddress = strEmail;
                            objGPCustomerMessage.BatchFileName = objFileinfo.Name;
                            objGPCustomerMessage.TransactionType = objEmailServerAccount.TransactionCode;
                            objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                            objGPCustomerMessage.DateCreated = DateTime.Now;

                            // Validate if file name is supported
                            blnValidFile = this.Validate(objGPCustomerMessage);

                            // Read email attachment
                            blnValidFile = this.ReadEmailAttachment(objGPCustomerMessage);

                            try
                            {
                                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BFT)
                                {
                                    this.GetMessageDetails(objGPCustomerMessage);
                                }
                            }
                            catch (System.Exception ex)
                            {
                                //   log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                                //    log4net.GlobalContext.Properties["FileName"] = string.Format("{0} {1}.", objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                                log.Error("Error encountered while using the mapper to get the source customer of request message. " + ex.Message);
                                objGPCustomerMessage.ErrorDesc = ex.Message;
                                objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;

                                //------------Use email method here.-Mothy-------------------------------
                                //GPTools.ProcessErrorAction(EmailService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_GEN, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objEmailServerAccount.TransactionCode, objFileinfo.Name, DateTime.Now.ToShortDateString(), ex.Message);
                            }

                            if (objGPCustomerMessage.MapperName != null)
                            {
                                ITransactionDataManager scmanager = new TransactionDataManager();
                                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                                GenericItemModel itemModel;
                                scmanager.FileInfo(strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.Message, strEmail);
                                filePath = strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress;
                                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                                {
                                    genericModel = scmanager.SOFileData(objGPCustomerMessage.MapperName, objGPCustomerMessage.SubsiId, false, filePath);

                                }
                                else if ((objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SC)
                                    || (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SSC))
                                {

                                    genericModel = scmanager.SCFileData(objGPCustomerMessage.MapperName, objGPCustomerMessage.SubsiId, false, filePath);
                                }
                                if (genericModel[genericModel.Count - 1].ErrorMessage.Length != 0)
                                {
                                    string transType = objGPCustomerMessage.TransactionType.ToString();
                                    using (var context = new GoodpackEDIEntities())
                                    {
                                        int transactionId = (from s in context.Ref_TransactionTypes
                                                             where s.TransactionCode == transType
                                                             select s.Id).FirstOrDefault();
                                        Gen_TransactionValidationTracker validator = new Gen_TransactionValidationTracker();
                                        validator.CreatedDateTime = DateTime.Now;
                                        validator.EmailId = strEmail;
                                        validator.FileContent = objGPCustomerMessage.Message;
                                        validator.FilePath = strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress;
                                        validator.FileName = objGPCustomerMessage.BatchFileName;
                                        validator.FileType = SOURCE_TYPES.EMAIL.ToString();
                                        validator.MapperName = objGPCustomerMessage.MapperName;
                                        validator.SubsiId = objGPCustomerMessage.SubsiId;
                                        validator.ValidationErrorMessage = genericModel[genericModel.Count - 1].ErrorMessage;
                                        validator.TransactionId = transactionId;
                                        validator.isValidEmail = true;
                                        context.Gen_TransactionValidationTracker.Add(validator);
                                        context.SaveChanges();
                                    }

                                    Dictionary<string, string> placeholders = new Dictionary<string, string>();
                                    placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                                    placeholders.Add("$$Transaction$$", transType);
                                    placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                                    placeholders.Add("$$EmailID$$", strEmail);
                                    placeholders.Add("$$Error$$", genericModel[genericModel.Count - 1].ErrorMessage);
                                    //string[] recipientList = new string[] { strEmail };
                                    // GPTools.SendEmail(placeholders, recipientList, "NOTIFY_CUSTOMER", null);
                                    //scmanager.getAllRecipients(objGPCustomerMessage.MapperName)

                                    List<Attachment> attachments1 = new List<Attachment>();
                                    string strFile;
                                    strFile = strProcessingFolder;
                                    strFile += "\\" + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
                                    Attachment attachment1 = new Attachment(strFile);
                                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                                    attachments1.Add(attachment1);
                                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                                    string[] customerEmail = new string[] { strEmail };
                                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_EMAIL_CUSTOMER", attachments1);
                                    List<Attachment> attachments2 = new List<Attachment>();
                                    Attachment attachment2 = new Attachment(strFile);
                                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                                    attachments2.Add(attachment2);
                                    GPTools.SendEmail(placeholders, scmanager.getAllRecipients(objGPCustomerMessage.MapperName), "NOTIFY_EMAIL_VALIDATION_ERROR", attachments2);
                                    log.Info("Email send to customer" + strEmail);
                                }
                                else
                                {
                                    scmanager = new TransactionDataManager();
                                    genericModel = new List<GenericItemModel>();
                                    IList<GenericItemModel> model = null;
                                    string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                                    for (int i = 0; i < messageData.Length; i++)
                                    {
                                        itemModel = new GenericItemModel();
                                        itemModel.Id = i;
                                        itemModel.Name = messageData[i];
                                        itemModel.DataValues = objGPCustomerMessage.Message;
                                        itemModel.BatchFileSourceAddress = strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress;
                                        itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                        itemModel.Message = objGPCustomerMessage.Message;
                                        itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                        itemModel.EmailId = strEmail;
                                        genericModel.Add(itemModel);
                                    }
                                    if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SC)
                                    {
                                        model = scmanager.verifyData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, false);
                                    }
                                    else if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SSC)
                                    {
                                        try
                                        {
                                            SSCManagerNew sscManager = new SSCManagerNew();
                                            model = sscManager.submitSSCData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, 2);
                                            // model = scmanager.verifySSCData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, false);
                                        }
                                        catch(Exception e)
                                        {
                                            log.Error("SSC email SAP submit error -" + e);
                                            ClearSourceFile(objGPCustomerMessage);
                                            continue;

                                        }
                                    }
                                    else
                                    {
                                        string custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                                        model = scmanager.verifySOData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), custPOdate, genericModel, false);
                                    }
                                }
                            }



                            if (objGPCustomerMessage.ErrorCode == ErrorCodes.V_CUST)
                            {
                                // Send email back to sender that their request cannot be processed. Send notification to administrator as well.
                                Tools.SendEmail_FailedToDeriveCustomerName(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName, objFileinfo.FullName, objGPCustomerMessage.ErrorDesc, objGPCustomerMessage.TransactionType.ToString());
                            }
                            else if (objGPCustomerMessage.ErrorCode == ErrorCodes.V_GYO_CHEM_SC)
                            {
                            }
                            //else if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BFT)
                            //{

                            //    string errorDesc = "";
                            //    try
                            //    {
                            //        SourceMessage sourceMsg = new SourceMessage();
                            //        sourceMsg.MinFieldValCount = 1;
                            //        Map map = new Map();

                            //        RefTransactionTypeModel objTargetMessageSpecs = new RefTransactionTypeModel();
                            //        Dictionary<int, SAPMessageFieldsModel> objfields = new Dictionary<int, SAPMessageFieldsModel>();

                            //        MsgSapBFTLine bftLine = new MsgSapBFTLine();

                            //        string[] fields = bftLine.getBFTFields();

                            //        int fieldCount = 1;
                            //        foreach (string field in fields)
                            //        {
                            //            SourceMessageField objSourceMessageField = new SourceMessageField();
                            //            objSourceMessageField.FieldName = field;
                            //            sourceMsg.Fields.Add(fieldCount, objSourceMessageField);

                            //            MapItem mapitm = new MapItem();
                            //            mapitm.TargetFieldSeq = fieldCount;
                            //            mapitm.SourceFieldSeq = fieldCount;
                            //            map.MapItems.Add(mapitm);

                            //            SAPMessageFieldsModel fieldSpecs = new SAPMessageFieldsModel();

                            //            bftLine.setBFTFieldProperties(fieldSpecs, field);
                            //            objfields.Add(fieldCount, fieldSpecs);

                            //            fieldCount++;
                            //        }

                            //        objTargetMessageSpecs.Fields = objfields;

                            //        bool isSuccess = true;
                            //        //string errorDesc = "";
                            //        List<SourceMessage> parsedMessage = null;
                            //        MapOutput mapOut = new MapOutput();
                            //        mapOut.OutputMessage = new StringBuilder();
                            //        IList<MsgSapBFTLine> BFTMsg = new List<MsgSapBFTLine>();

                            //        parsedMessage = GenericDelimiterParser.Parse(sourceMsg, objGPCustomerMessage.Message, true, ',', ref isSuccess, ref errorDesc);

                            //        if (parsedMessage.Count != 0)
                            //        {
                            //            GPMapper mapper = new GPMapper(objGPCustomerMessage);
                            //            string fieldsWhichFailedValidation = "";
                            //            bool performFieldValidationAfterMapping = true;
                            //            bool validationFailed = false;
                            //            string errordescLookup = "";
                            //            ErrorCodes errorCode = ErrorCodes.NONE;

                            //            foreach (SourceMessage objSourceMsg in parsedMessage)
                            //            {
                            //                IList<string> messageLineFields = mapper.MapBFT(objGPCustomerMessage, map, objTargetMessageSpecs, objSourceMsg, ref isSuccess, ref errorCode, ref errorDesc, ref fieldsWhichFailedValidation, performFieldValidationAfterMapping, ref validationFailed, ref errordescLookup);
                            //                if (!isSuccess)
                            //                {
                            //                    break;
                            //                }
                            //                else
                            //                {
                            //                    BFTMsg.Add(new MsgSapBFTLine(messageLineFields));
                            //                }
                            //            }
                            //        }
                            //        else
                            //        {
                            //            isSuccess = false;
                            //            errorDesc = string.Format("Error encountered while processing BFT transaction.All field on the file seems to be empty");
                            //        }
                            //        if (isSuccess)
                            //        {
                            //            foreach (MsgSapBFTLine objMsgBFTMsg in BFTMsg)
                            //            {
                            //                objMsgBFTMsg.GenerateSAPMessage();
                            //                if (mapOut.OutputMessage.Length > 0)
                            //                {
                            //                    mapOut.OutputMessage.Append(Environment.NewLine);
                            //                }
                            //                mapOut.OutputMessage.Append(objMsgBFTMsg.Message);

                            //            }
                            //            createSAPBFTFile(objGPCustomerMessage, mapOut.OutputMessage, ref isSuccess, ref errorDesc);
                            //        }
                            //        else
                            //        {
                            //            ProcessErrorActionForBFT(objGPCustomerMessage, errorDesc);
                            //        }
                            //    }

                            //    catch (Exception ex)
                            //    {

                            //      //  objILog.Error("Error encountered while processing BFT request " + ex.Message);

                            //        Dictionary<string, string> messageKeyPlaceholders = new Dictionary<string, string>();
                            //        string[] recipients = { objGPCustomerMessage.BatchFileSourceAddress };
                            //        messageKeyPlaceholders.Add("$$Service$$", "Brazil File Translation");
                            //        messageKeyPlaceholders.Add("$$TransactionType$$", TRANSACTION_TYPES.BFT.ToString());
                            //        messageKeyPlaceholders.Add("$$Filename$$", objGPCustomerMessage.BatchFileName);
                            //        messageKeyPlaceholders.Add("$$CustomerName$$", recipients[0]);
                            //        messageKeyPlaceholders.Add("$$Info$$", errorDesc);
                            //        messageKeyPlaceholders.Add("$$BatchID$$", "");
                            //        messageKeyPlaceholders.Add("$$CustomerCode$$", "");
                            //        GPTools.SendEmail(messageKeyPlaceholders, recipients, "V_FIELD", true);
                            //        GPTools.SendEmailToAdmin(messageKeyPlaceholders, "V_FIELD", TRANSACTION_TYPES.BFT.ToString());
                            //    }


                            //}
                            else
                            {
                                // Insert to TRANS_BATCH table
                                //objDataAccess.InsertBatchTransaction(objGPCustomerMessage);
                                //if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
                                //{
                                //    string fcFileNameExtention = (objGPCustomerMessage.BatchFileName).Substring((objGPCustomerMessage.BatchFileName).LastIndexOf('.'));
                                //    string fcFileName = objGPCustomerMessage.BatchFileName.Replace(fcFileNameExtention, "");
                                //    //string archivePath = String.Format("{0}\\{1}", Config.getConfig("ReqOutPathFCCOPY"), fcFileName);
                                //    string archivePath = Config.getConfig("ReqOutPathFCCOPY");
                                //    GPTools.ArchiveFC(objGPCustomerMessage.Message.ToString(), archivePath, fcFileName);
                                //}
                                try
                                {
                                    // Archive
                                    //string strDestinationpath = String.Format("{0}\\{1}", Config.getConfig("ArchivePath"), ArchiveSources.REQ_IN.ToString());
                                    ////Modified to pass the appropriate value to archive Excel files
                                    //if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                                    //    GPTools.Archive(objFileinfo.Directory.FullName + "\\" + objGPCustomerMessage.BatchFileName, ArchiveSources.REQ_IN, objGPCustomerMessage.BatchID.ToString(), strDestinationpath);
                                    //else
                                    //    GPTools.Archive(objGPCustomerMessage.Message, ArchiveSources.REQ_IN, objGPCustomerMessage.BatchID.ToString(), strDestinationpath);
                                }
                                catch (Exception exp)
                                {
                                    log.Error("Error while archiving received file :" + exp.Message);
                                    //    GPTools.ProcessErrorAction(FacadeService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_ARCHIVE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), exp.Message);
                                }
                            }

                            // If file is not valid, send email
                            if (!blnValidFile)
                            {
                                //    GPTools.ProcessErrorAction(FacadeService.Service, objGPCustomerMessage.BatchID, ErrorCodes.V_FILE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objEmailServerAccount.TransactionCode, objFileinfo.Name, DateTime.Now.ToShortDateString(), "");
                            }
                        }
                        catch (System.Exception ex)
                        {
                            if (string.IsNullOrEmpty(objGPCustomerMessage.BatchFileName))
                                objGPCustomerMessage.BatchFileName = string.Empty;

                            //log4net.GlobalContext.Properties["CustomerCode"] = null;
                            //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                            // log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
                            // Send notification email
                            // GPTools.ProcessErrorAction(FacadeService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_GEN, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, DateTime.Now.ToShortDateString(), "The Facade Service encountered an error while processing a customer request received via email. " + ex.Message);

                            log.Error("Error encountered while processing customer request received via email. " + ex.Message);
                        }

                        // Clear file
                        ClearSourceFile(objGPCustomerMessage);
                    }
                }
            }
            catch (Exception exp)
            {
                // Set batch file name
                if (string.IsNullOrEmpty(objGPCustomerMessage.BatchFileName))
                {
                    objGPCustomerMessage.BatchFileName = string.Empty;
                }
                else
                {
                    // Move file to unprocessed folder
                    string filePath = objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
                    //GPTools.MoveFile(filePath, Config.getConfig("UnprocessedFilesPath"));
                }

               // log4net.GlobalContext.Properties["CustomerCode"] = null;
               // log4net.GlobalContext.Properties["TransactionType"] = null;
               // log4net.GlobalContext.Properties["FileName"] = null;

                // Send notification email. If email was sent in past 2 hours, then do not send notificatoin
                if (this.lastTimeErrorNotificationWasSentDuetoEmailFailure.AddHours(2) < System.DateTime.Now)
                {
                  //  GPTools.ProcessErrorAction(FacadeService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_GEN, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, DateTime.Now.ToShortDateString(), "Error encountered in EAGetMailEndPoint of Goodpack Facade Service. " + exp.Message);
                    this.lastTimeErrorNotificationWasSentDuetoEmailFailure = System.DateTime.Now;
                }

                log.Error("Error while processing EAGetmailEndPoint", exp);
            }
            finally
            {
                isProcessing = false;
            }
        }

        //private void createSAPBFTFile(GPCustomerMessage objGPCustomerMessage, StringBuilder outputMessage, ref bool isSuccess, ref string errorDesc)
        //{

        //    try
        //    {
        //        string strDropPath = "";
        //        string strTempPath = "";
        //        string tempSAPReqPath = Config.getConfig("SAPRequestTempFolder");
        //        if (!tempSAPReqPath.EndsWith(@"\"))
        //            tempSAPReqPath += @"\";
        //        strDropPath = Config.getConfig("ReqOutPathBFT");

        //        DirectoryInfo objDirectoryInfo = new DirectoryInfo(strDropPath);
        //        if (!objDirectoryInfo.Exists)
        //            GPTools.createFolder(objDirectoryInfo.FullName);
        //        string filename = String.Format("{0}{1:yyyyMMddHHmmss}.txt", objGPCustomerMessage.TransactionType.ToString(), DateTime.UtcNow);
        //        string strFullFileName = String.Format("{0}\\{1}", strDropPath, filename);
        //        strTempPath = String.Format("{0}{1}", tempSAPReqPath, filename);

        //        if (!System.IO.File.Exists(strFullFileName))
        //        {
        //            using (System.IO.StreamWriter objStreamWriter = new System.IO.StreamWriter(strTempPath, false))
        //            {
        //                objStreamWriter.Write(outputMessage);
        //            }

        //             Move to target folder
        //            File.Move(strTempPath, strFullFileName);
        //        }

        //        Dictionary<string, string> messageKeyPlaceholders = new Dictionary<string, string>();
        //        messageKeyPlaceholders.Add("$$TransactionType$$", "Brazil File Translation");
        //        messageKeyPlaceholders.Add("$$Filename$$", objGPCustomerMessage.BatchFileName);
        //        messageKeyPlaceholders.Add("$$CustomerName$$", "sir/madam");

        //        string[] recipients = { objGPCustomerMessage.BatchFileSourceAddress };
        //        GPTools.SendEmail(messageKeyPlaceholders, recipients, "REQ_SUCCESS", true, null);
        //    }

        //    catch (Exception ex)
        //    {
        //        isSuccess = false;
        //        errorDesc = string.Format("Error encountered", ex.Message);
        //        objILog.Error(errorDesc);
        //        throw ex;
        //    }

        //}

        protected void ClearSourceFile(GPCustomerMessage objGPCustomerMessage)
        {
            string strSourcePath = strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
            try
            {
                File.Delete(strSourcePath);
            }
            catch (Exception exp)
            {
                log.Error("Error deleting customer file: " + strSourcePath+" Exception Message:"+ exp.Message);
            }
        }

        /// <summary>
        /// Reads the content of the email attachment
        /// </summary>
        /// <param name="objGPCustomerMessage"></param>
        /// <returns></returns>
        private bool ReadEmailAttachment(GPCustomerMessage objGPCustomerMessage)
        {
            // Init vars
            bool isValid = true;

            // Get info of email attachment
            FileInfo objFileInfo = new FileInfo(strProcessingFolder + "\\" + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName);

            // Check if the attachment file is still present in the temporary folder
            if (!objFileInfo.Exists)
            {
                isValid = false;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorDesc = "The email attachment file cannot be found in temporary folder " + objFileInfo.FullName;
            }
            else
            {
                // Read file content and store in GP Customer Message object
                try
                {
                    //if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI)
                    //{
                    //    //Defaut Headcompany and customercode for ACI transaction
                    //    string mailID = "acitestentity@goodpack.com";
                    //    objGPCustomerMessage.CustomerHeadCompany = EntityMaster.DeriveCustomerHeadCompanyFromEmailSenderAddress(mailID);
                    //    objGPCustomerMessage.CustomerCode = 502633;
                    //    Customer objCustomer = EntityMaster.GetCustomer(objGPCustomerMessage.CustomerCode);
                    //    objGPCustomerMessage.CustomerName = objCustomer.CustomerName;
                    //}
                    //else

                    //{
                    objGPCustomerMessage.MapperName = EntityMaster.DeriveMapperNameFromEmailSenderAddress(objGPCustomerMessage.BatchFileSourceAddress.ToLower(), objGPCustomerMessage.TransactionType.ToString());
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    //}
                    if (objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName) || GPArchitecture.Cache.Config.getConfigList("SumitomoShipmentConfirmation").Contains(objGPCustomerMessage.MapperName))
                    {
                        objGPCustomerMessage.SubsiName = "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE";
                    }
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    //}
                    if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                    {
                        if ((objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder")|| GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName)) && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                        //&& objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO
                        {
                            objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 21, 0);
                        }
                        else
                        {
                            objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 0, 0);

                        }
                    }
                    else
                    {
                        using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                        {
                            string message = objStreamReader.ReadToEnd();
                            objGPCustomerMessage.Message = message.Replace(",","|");
                            objStreamReader.Close();
                            objStreamReader.Dispose();
                        }
                    }
                }
                catch (Exception exp)
                {
                    isValid = false;
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    log.Error("File is not in correct format", exp);
                }
            }

            return isValid;

        }

        //public void ProcessErrorActionForBFT(GPCustomerMessage objGPCustomerMessage, string errorDesc)
        //{
        //    objILog.Error("Error encountered while processing BFT request ");  

        //    string[] recipients = { objGPCustomerMessage.BatchFileSourceAddress };
        //    Dictionary<string, string> messageKeyPlaceholders = new Dictionary<string, string>();
        //    messageKeyPlaceholders.Add("$$Service$$", "Brazil File Translation");
        //    messageKeyPlaceholders.Add("$$TransactionType$$", TRANSACTION_TYPES.BFT.ToString());
        //    messageKeyPlaceholders.Add("$$Filename$$", objGPCustomerMessage.BatchFileName);
        //    messageKeyPlaceholders.Add("$$CustomerName$$", recipients[0]);
        //    messageKeyPlaceholders.Add("$$Info$$", errorDesc);
        //    messageKeyPlaceholders.Add("$$BatchID$$", "");
        //    messageKeyPlaceholders.Add("$$CustomerCode$$", "");
        //    GPTools.SendEmail(messageKeyPlaceholders, recipients, "V_FIELD", true);
        //    GPTools.SendEmailToAdmin(messageKeyPlaceholders, "V_FIELD", TRANSACTION_TYPES.BFT.ToString());
        //}
    }
}

