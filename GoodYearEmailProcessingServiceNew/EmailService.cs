﻿using GoodpackEmailProcessingService;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
//using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace GoodpackEmailProcessingService
{
    public partial class EmailService : ServiceBase
    {
        public const string Service = "Goodpack Facade Service";
        private static int NoofAttempts = 0;
        private static ILog log = LogManager.GetLogger(typeof(EmailService));
        EAGetmailEndPoint objEAGetmailEndPoint = new EAGetmailEndPoint();
        //Thread hostThread;
        public Timer EmailEndPointProcessTimer;
        //FileReceiverEndPoint objFileReceiverEndPoint = new FileReceiverEndPoint();
        public EmailService()
        {
            InitializeComponent();
           // hostThread = new Thread(new ThreadStart(StartMethod));
        }

        public void debugger()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            log.Info("Entering FacadeService OnStart");
            this.EmailEndPointProcessTimer = new Timer();           
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("RcvMailboxPollInterval")));
            // ProcessTimer
            // log.Info("Time in milliseconds :"+objTimeInterval.TotalMilliseconds.ToString());
            this.EmailEndPointProcessTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.EmailEndPointProcessTimer.Elapsed += new ElapsedEventHandler(EmailEndPointProcessTimer_Elapsed);
            this.EmailEndPointProcessTimer.Enabled = true;
            this.EmailEndPointProcessTimer.Start();
            //if (System.Configuration.ConfigurationManager.AppSettings["debug"].ToString() == "true")
            //    System.Diagnostics.Debugger.Launch();
            //hostThread.Start();
           // objEAGetmailEndPoint.Initialize();
            //objFileReceiverEndPoint.Initialize();
            log.Debug("Leaving FacadeService OnStart");
            while (true) { }
            
        }

        void EmailEndPointProcessTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            log.Info("Entering  EmailEndPointProcessTimerFirst_Elapsed event" );
           // if (!objEAGetmailEndPoint.isProcessing)
                objEAGetmailEndPoint.Initialize();
            log.Info("Leaving  EmailEndPointProcessTimerFirst_Elapsed event");
        }
        //private void StartMethod()
        //{
        //    log.Info("Enter Start Method");
        //    objEAGetmailEndPoint.Initialize();
        //}

        protected override void OnStop()
        {
            this.EmailEndPointProcessTimer.Stop();
           // objEAGetmailEndPoint.FileEndPointProcessTimer.Stop();
            while (objEAGetmailEndPoint.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                     log.Warn("Goodpack Facade Service Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
