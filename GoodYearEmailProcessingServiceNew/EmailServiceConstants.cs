﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEmailProcessingService
{
    class EmailServiceConstants
    {
        public const string SINumber = "SI Number";
        public const string Plant = "Plant";
        public const string Filler = "Filler";
    }
}
