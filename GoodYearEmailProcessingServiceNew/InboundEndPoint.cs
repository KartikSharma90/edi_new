﻿using GPArchitecture.CommonTypes;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GoodpackEmailProcessingService
{
    abstract class InboundEndPoint
    {
        protected IList<GPCustomerMessage> objLstGPCustomerMessage;
        private static readonly ILog log = LogManager.GetLogger(typeof(InboundEndPoint));
       // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public virtual void Initialize()
        {
        
        }

        protected abstract void Process();

        protected virtual bool Validate(GPCustomerMessage objGPCustomerMessage)
        {
         //   objILog.Debug("Entering Validate method");

            // Init vars
            bool blnValidFileType = true;

            // Perform validation
            blnValidFileType &= this.Validate_IsFileTypeSupported(objGPCustomerMessage);

            //objILog.Debug("Leaving Validate method");
            return blnValidFileType;
        }

        protected virtual void GetMessageDetails(GPCustomerMessage objGPCustomerMessage)
        {
            //objILog.Debug("Entering GetMessageDetails method");
            string[] strFileName = objGPCustomerMessage.BatchFileName.Split('.');
            if (strFileName.LongLength > 1 && strFileName[0].Length > 1)
            {
                int ver = 0;
                int.TryParse(strFileName[0].Substring(strFileName[0].Length - 2), out ver);
                objGPCustomerMessage.BatchFileVersion = ver;
            }
          //  objILog.Debug("Leaving GetMessageDetails method");
        }

        /// <summary>
        /// Validates whether the file extension of the file received from customer is valid or not.
        /// </summary>
        /// <param name="objGPCustomerMessage"></param>
        /// <returns></returns>
        private bool Validate_IsFileTypeSupported(GPCustomerMessage objGPCustomerMessage)
        {
            //objILog.Debug("Entering Validate_IsFileTypeSupported method");
            bool blnValidFileType = false;
            try
            {
                FILE_TYPES fileType;
                blnValidFileType = SupportedFileTypes.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                if (blnValidFileType == false)
                {
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    log.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                }
            }
            catch (Exception exp)
            {
                log.Error("Error while Validating the file", exp);
                throw exp;
            }
           // objILog.Debug("Leaving Validate_IsFileTypeSupported method");
            return blnValidFileType;
        }
    }
}
