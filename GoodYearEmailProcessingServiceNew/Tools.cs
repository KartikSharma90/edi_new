﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.DataLayer.Models;
using System.IO;
using log4net;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Parsers;

namespace GoodpackEmailProcessingService
{
    class Tools
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(EAGetMail));    
        internal static void InsertRejectedMailDetails(string transactionType, string emailId, string filePath,string errorMsg)
        {
            try
            {
                string fileContent;
                using (var context = new GoodpackEDIEntities())
                {
                    FileInfo objFileInfo = new FileInfo(filePath);
                    if (objFileInfo.Name.EndsWith("xls") || objFileInfo.Name.EndsWith("xlsx") || objFileInfo.Name.EndsWith("XLS") || objFileInfo.Name.EndsWith("XLSX"))
                    {                     
                       fileContent = ExcelParser.ExcelToCSV(objFileInfo.FullName, 0, 0);

                    }
                    else
                    {
                        using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                        {
                            string message = objStreamReader.ReadToEnd();
                            fileContent = message.Replace(",", "|");
                            objStreamReader.Close();
                            objStreamReader.Dispose();
                        }
                    }
                    int transactionId = (from s in context.Ref_TransactionTypes
                                         where s.TransactionCode == transactionType
                                         select s.Id).FirstOrDefault();
                    Gen_TransactionValidationTracker validator = new Gen_TransactionValidationTracker();
                    validator.CreatedDateTime = DateTime.Now;
                    validator.EmailId = emailId;
                    validator.FileContent = fileContent;
                    validator.FilePath = filePath;
                    validator.FileName = objFileInfo.Name;
                    validator.FileType = SOURCE_TYPES.EMAIL.ToString();
                    validator.MapperName = String.Empty;
                    validator.SubsiId = null;
                    validator.ValidationErrorMessage = errorMsg;
                    validator.TransactionId = transactionId;
                    validator.isValidEmail = false;
                    context.Gen_TransactionValidationTracker.Add(validator);
                    context.SaveChanges();
                }

            }
            catch (System.Exception ex)
            {
                log.Error("Tools:Inserting Gen_TransactionValidationTracker,Transaction type:" + transactionType + ", File name:" + filePath);
            }
        }

        internal static void SendEmail_ArchiveError(int batchId, string customer, long customerCode, string transaction, string message)
        {
            try
            {
                Dictionary<string, string> messageKeys = new Dictionary<string, string>();
                messageKeys.Add("$$BatchId$$", batchId.ToString());
                messageKeys.Add("$$Customer$$", customer);
                messageKeys.Add("$$Transaction$$", customerCode.ToString());
                messageKeys.Add("$$CustomerCode$$", transaction);
                messageKeys.Add("$$Message$$", message);

               // GPTools.SendEmailToAdmin(messageKeys, "ARCHIVE_FAILED", transaction);

            }
            catch (System.Exception ex)
            {
               // FacadeService.objILog.Error("Error in method SendEmail_ArchiveError(). ", ex);
            }
        }

        internal static void SendEmail_FailedToDeriveCustomerName(string sourceEmail, string filename, string filepath, string errorMessage, string transaction)
        {
            try
            {
                // Set recipient list for customer
                string[] customerRecipientList = new string[1];
                customerRecipientList[0] = sourceEmail;

                // Sey message key placeholder list
                Dictionary<string, string> messageKeyPlaceholders = new Dictionary<string, string>();
                messageKeyPlaceholders.Add("$$Filename$$", filename);
                messageKeyPlaceholders.Add("$$SourceEmail$$", sourceEmail);
                messageKeyPlaceholders.Add("$$Info$$", errorMessage);

                // Send email to customer
             //   GPTools.SendEmail(messageKeyPlaceholders, customerRecipientList, "V_CUST", true, null);

                // Send email to administrator
              //  GPTools.SendEmailToAdmin(messageKeyPlaceholders, "V_CUST", filepath, filename, "", transaction);

            }
            catch (System.Exception ex)
            {
            //    FacadeService.objILog.Error("Error in method SendEmail_FailedToDeriveCustomerName(). ", ex);
            }
        }

        internal static List<string> CsvStringToList(string stringdata)
        {
            List<string> lstData = new List<string>();
            stringdata = stringdata.Replace("\r\n", ",");
            foreach (string str in stringdata.Split(','))
            {
                lstData.Add(str.Replace("\"", ""));
            }
            return lstData;
        }
    }
}

