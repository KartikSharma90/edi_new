﻿//using GPArchitecture.CommonTypes;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace GoodpackEmailProcessingService
//{
//    class GPCustomerMessage
//    {
//        private string strSourceAddr;
//        private string strFileName;
//        private int intFileVersion;
//        private int intBatchID;
//        private string strSourceRefID;
//        private string strMessage;
//        private string strSourceCustomer;


//        public SourceType Sourcetype;

//        /// <summary>
//        /// sender email address or source file path.
//        /// </summary>
//        public string SourceAddress
//        {
//            set
//            {
//                strSourceAddr = value;
//            }
//            get
//            {
//                return strSourceAddr;
//            }
//        }

//        /// <summary>
//        /// sender email address or source file path.
//        /// </summary>
//        public string SourceCustomer
//        {
//            set
//            {
//                strSourceAddr = value;
//            }
//            get
//            {
//                return strSourceAddr;
//            }
//        }

//        /// <summary>
//        /// name of file
//        /// </summary>
//        public string FileName
//        {
//            set
//            {
//                strFileName = value;
//            }
//            get
//            {
//                return strFileName;
//            }
//        }

//        /// <summary>
//        /// name of file
//        /// </summary>
//        public enum FileFormat
//        {
//            CSV = 0, FixedLength = 1
//        }

//        /// <summary>
//        /// Version of the File
//        /// </summary>
//        public int FileVersion
//        {
//            set
//            {
//                intFileVersion = value;
//            }
//            get
//            {
//                return intFileVersion;
//            }
//        }
//        /// <summary>
//        /// Represents Transactions like Sales Order, 
//        /// Shipment Confirmation, Forecast.
//        /// </summary>
//        public TRANSACTION_TYPES TransactionType;

//        /// <summary>
//        /// Represent unique Batch Id created for the file received.
//        /// </summary>
//        public int BatchID
//        {
//            set
//            {
//                intBatchID = value;
//            }
//            get
//            {
//                return intBatchID;
//            }
//        }

//        public BatchStatus Batchstatus;


//        public ErrorCodes ErrorCode;

//        /// <summary>
//        /// Message – content of message
//        /// </summary>
//        public string Message
//        {
//            set
//            {
//                strMessage = value;
//            }
//            get
//            {
//                return strMessage;
//            }
//        }
//    }

//    /// <summary>
//    /// Sources of Files received 
//    /// </summary>
//    public enum SourceType
//    {
//        /// <summary>
//        /// Files received through Email Attachments 
//        /// </summary>
//        EMAIL = 0,
//        /// <summary>
//        /// Files Uploded through FTP 
//        /// </summary>
//        FileSystem = 1
//    }
//}
