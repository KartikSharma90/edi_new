﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodyearFileExchangeService
{
    /// <summary>
    /// Class ConstantVariables.
    /// </summary>
    internal class ConstantVariables
    {
        internal const string FileTransferServiceInitializationInterval = "FileTransferServiceInitializationInterval";
        internal const string FileTransferInterimDownloadFilePath = "FileTransferInterimDownloadFilePath";
        internal const string FileTransferArchivePath = "FileTransferArchivePath";
        internal const string FTP = "FTP";
        internal const string SFTP = "SFTP";
        internal const string WindowsShare = "WindowsShare";
        internal const string DownloadFile = "DOWNLOAD";
        internal const string UploadFile = "UPLOAD";
    }
}
