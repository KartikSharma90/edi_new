﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using GPArchitecture.Cache;
using System.Timers;
using GPArchitecture.Adapters;
using GPArchitecture.DataLayer.Models;

namespace GoodyearFileExchangeService
{
    /// <summary>
    /// Class FileExchangeProcessor.
    /// </summary>
    public class FileExchangeProcessor
    {
        /// <summary>
        /// The is processing
        /// </summary>
        
        public bool isProcessing = false; // public get property
        //public System.Timers.Timer fileExchangeTimer;
       // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog objILog = LogManager.GetLogger(typeof(FileExchangeProcessor));
        #region Initialize()

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            objILog.Debug("Entering FileExchange Service Initialize method");
            //this.fileExchangeTimer = new System.Timers.Timer();
            ////TimeSpan objTimeInterval = new TimeSpan(0, 0, 60);
            //TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            //this.fileExchangeTimer.Interval = objTimeInterval.TotalMilliseconds;
            //this.fileExchangeTimer.Elapsed += new ElapsedEventHandler(FileExchangeTimer_Elapsed);
            //this.fileExchangeTimer.Enabled = true;
            //this.fileExchangeTimer.Start();
            StartEvent();
            objILog.Debug("Leaving Response Processor Service Initialize method");
        }
        #endregion

        /// <summary>
        /// Handles the Elapsed event of the FileExchangeTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        //void FileExchangeTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    objILog.Debug("Entering processTimer_Elapsed event");
        //    objILog.Debug("Value of isProcessing :" + isProcessing);
        //    if (!isProcessing)
        //    {
        //        objILog.Debug("Entering method Process");
        //        this.Process();
        //    }
        //    objILog.Debug("Leaving processTimer_Elapsed event");
        //}


        void StartEvent()
        {
            objILog.Debug("Entering Start event");
            objILog.Debug("Value of isProcessing :" + isProcessing);
            if (!isProcessing)
            {
                objILog.Debug("Entering method Process");
                this.Process();
            }
            objILog.Debug("Leaving Start event");
        }
        /// <summary>
        /// Processes this instance.
        /// </summary>
        private void Process()
        {
            objILog.Debug("Entering FileExchange Process method");
            try
            {
                FileExchangeDAFactory objRespDAFactory = new FileExchangeDAFactory();
                List<Gen_FTPAccount> ftpAccountDetails = new List<Gen_FTPAccount>();

                //Getting account details
                ftpAccountDetails = objRespDAFactory.GetFTPAccounts();
                foreach (Gen_FTPAccount ftpAccount in ftpAccountDetails)
                {
                    IAdapters accountAdapter = Utilities.GetAdapterFromFileTransferAccount(ftpAccount);
                    try
                    {
                        // Open connection
                        accountAdapter.OpenConnection();
                        List<Gen_FileTransfer> filePaths = new List<Gen_FileTransfer>();

                        //Getting From and To location from Gen_FileTransfer table
                        filePaths = objRespDAFactory.GetFileTransferPath(ftpAccount.SftpId);
                        foreach (Gen_FileTransfer filePath in filePaths)
                        {
                            string sourcePath = filePath.LocationFrom;
                            string destPath = filePath.LocationTo;
                            bool deleteFile = Convert.ToBoolean(filePath.IsDeleteFile);
                            //Download file from FTP server to local location
                            if (filePath.FileTransferType == ConstantVariables.DownloadFile)
                            {
                                accountAdapter.DownloadFile(sourcePath, destPath, deleteFile);
                            }

                            ////Upload file from local location to FTP server
                            //else if (filePath.FileTransferType == ConstantVariables.UploadFile)
                            //{
                            //    accountAdapter.UploadFile(sourcePath, destPath, deleteFile);
                            //}
                        }

                    }
                    catch (Exception ex)
                    {
                        objILog.Error("Error encountered while processing " + ex.Message, ex);
                    }
                    finally
                    {
                        if (accountAdapter != null && accountAdapter.IsConnected)
                        {
                            //Close connection
                            accountAdapter.CloseConnection();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                objILog.Error("Error encountered while processing " + ex.Message, ex);
            }

            objILog.Debug("Leaving FileExchange Process method");
        }


    }
}
