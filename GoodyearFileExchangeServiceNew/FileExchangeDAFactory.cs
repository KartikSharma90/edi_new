﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using GPArchitecture.DataLayer.Models;

namespace GoodyearFileExchangeService
{
    /// <summary>
    /// Class FileExchangeDAFactory.
    /// </summary>
    class FileExchangeDAFactory 
    {
        /// <summary>
        /// The object i log
        /// </summary>
        //private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog objILog = LogManager.GetLogger(typeof(FileExchangeDAFactory));
        ///// <summary>
        ///// Initializes a new instance of the <see cref="FileExchangeDAFactory"/> class.
        ///// </summary>
        ///// <param name="connString">The connection string.</param>
        //public FileExchangeDAFactory(string connString)
        //{
        //    strConnectionString = connString;
        //}

        /// <summary>
        /// Gets the file transfer path.
        /// </summary>
        /// <param name="sftpID">The SFTP identifier.</param>
        /// <returns>List{Gen_FileTransfer}.</returns>
        public List<Gen_FileTransfer> GetFileTransferPath(int sftpID)
        {
            objILog.Debug("Entering GetFileTransferPath method");
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    List<Gen_FileTransfer> fileTransferPath = (from p in context.Gen_FileTransfer
                                                               where (p.SftpID == sftpID && p.FileTransferType == ConstantVariables.DownloadFile)
                                                                select p).ToList();
                    return fileTransferPath;
                }
            }
            //p.Gen_SFTPAccount.SftpId== sftpID
            catch (Exception ex)
            {
                objILog.Error("Error encountered while processing " + ex.Message);
                throw ex;
            }

        }//-- End GetFileTransferPath()

        /// <summary>
        /// Gets the FTP accounts.
        /// </summary>
        /// <returns>List{Gen_FTPAccount}.</returns>
        public List<Gen_FTPAccount> GetFTPAccounts()
        {
            objILog.Debug("Entering GetFTPAccounts method");
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    List<Gen_FTPAccount> ftpAccounts = new List<Gen_FTPAccount>();
                    ftpAccounts = (from p in context.Gen_FTPAccount
                                    select p).ToList();
                    return ftpAccounts;
                }
            }
            catch (Exception ex)
            {
                objILog.Error("Error encountered while processing " + ex.Message);
                throw ex;
            }

        }//-- End GetFTPAccounts()
    }
}
