﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using GoodyearFileExchangeService;

namespace GoodyearFileExchangeServiceNew
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
#if DEBUG
            FileExchangeService service = new FileExchangeService();
            service.debugger();
# else
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Starting preprocessing service");
            log.Info("Release Mode");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new FileExchangeService() 
			};            
            ServiceBase.Run(ServicesToRun);
            log.Info("Leaving ");
# endif
        }
    }
}
