﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GPArchitecture.DataLayer.Models;
using GPArchitecture.Adapters;
using GPArchitecture.Adapters.FTP;

namespace GoodyearFileExchangeService
{
    internal class Utilities
    {
        #region Get Adapter From File Exchange

        /// <summary>
        /// Gets the adapter from file transfer account.
        /// </summary>
        /// <param name="accountName">Name of the account.</param>
        /// <returns>IAdapter.</returns>
        public static IAdapters GetAdapterFromFileTransferAccount(Gen_FTPAccount accountName)
        {

            if (accountName.Name == ConstantVariables.SFTP && accountName.Name != null)
            {
                return new Ftp(accountName);
            }
            else if (accountName.Name == ConstantVariables.FTP && accountName.Name != null)
            {
                return new Ftp(accountName);
            }
            else if (accountName.Name == ConstantVariables.WindowsShare)
            {
                return new WindowsShare();
            }

            return null;
        }
        #endregion

    }
 
}
