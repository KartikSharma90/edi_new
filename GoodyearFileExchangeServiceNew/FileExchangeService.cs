﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Configuration;
using System.Timers;


namespace GoodyearFileExchangeService
{
    public partial class FileExchangeService : ServiceBase
    {
        public const string Service = "Goodpack FileExchange Service";
        public System.Timers.Timer fileExchangeTimer;
        private static int NoofAttempts = 0;
        private static readonly ILog objILog = LogManager.GetLogger(typeof(FileExchangeService));
      //  internal static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        FileExchangeProcessor objFileExchangeProcess = new FileExchangeProcessor();
        
        public FileExchangeService()
        {
            objILog.Debug("Entering FileExchangeService Intialize Component");
            InitializeComponent();            
            objILog.Debug("Leaving FileExchangeService Intialize Component");
        }
        public void debugger()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            objILog.Debug("Entering FileExchange OnStart");
           // DBConnection.InitializeSQLConn(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
          //  if (System.Configuration.ConfigurationManager.AppSettings["debug"].ToString() == "true")
         //       System.Diagnostics.Debugger.Launch();            
            this.fileExchangeTimer = new System.Timers.Timer();
            //TimeSpan objTimeInterval = new TimeSpan(0, 0, 60);
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            this.fileExchangeTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.fileExchangeTimer.Elapsed += new ElapsedEventHandler(FileExchangeTimer_Elapsed);
            this.fileExchangeTimer.Enabled = true;
            this.fileExchangeTimer.Start();            
            objILog.Debug("Leaving FileExchange Service OnStart");
            //while (true) { }
        }

        void FileExchangeTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            objILog.Debug("Entering processTimer_Elapsed event");
            objFileExchangeProcess.Initialize();
            objILog.Debug("Leaving processTimer_Elapsed event");
        }

        protected override void OnStop()
        {
            this.fileExchangeTimer.Stop();
            while (objFileExchangeProcess.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    objILog.Warn("FileExchange Services Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
