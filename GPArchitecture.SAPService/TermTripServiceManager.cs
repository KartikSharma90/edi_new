using System;
 using log4net;
 using System.Collections.Generic;
 using System.Linq;
 using System.Web;
 using System.Web.Configuration;
 using GPArchitecture.SAPService.ServiceReference_TermTripCheck_QA;
 using GPArchitecture.SAPService.ConstantUtils;
 
 
 
 namespace GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer
 {
     public class TermTripServiceManager
     {
         private static readonly ILog log = LogManager.GetLogger(typeof(TermTripServiceManager));
         /// <summary>
         /// 
         /// </summary>
         /// <param name="CustomeCode"></param>
         /// <returns> 1-TermTrip , 2-Trip , 3-Invalid </returns>
         public int CheckTermTripOrNot(string CustomeCode)
         {
             int returnVal = 0;
             try
             {
                 ZBAPI_CHECK_TERM_TRIPClient client = new ZBAPI_CHECK_TERM_TRIPClient();
                 client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_TermTripCheck"]);
                 client.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
                 client.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;
                 ZbapiCheckTermTrip termtripCheck = new ZbapiCheckTermTrip();
                 termtripCheck.Kunnr = "0000" + CustomeCode;
                 ZbapiCheckTermTripResponse response = client.ZbapiCheckTermTrip(termtripCheck);
                 returnVal= String.IsNullOrEmpty(response.Flag) ? 0 : int.Parse(response.Flag);
                 return returnVal;
             }
             catch(Exception e)
             {
                 log.Error("CheckTermTripOrNot error -" + e);
                 return returnVal;
             }
         }
     }
 }

 

