﻿
namespace GoodpackEDI.ViewModels
{
    public class ResponseModel
    {
        public bool? Success { get; set; }
        public string Message { get; set; }        
        public object ResponseItems { get; set; }
    }
}