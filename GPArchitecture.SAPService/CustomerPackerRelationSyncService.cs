﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService.CustomerPackerRelationService;
using System.Configuration;
using GPArchitecture.SAPService.ConstantUtils;


namespace GPArchitecture.SAPService
{
    public class CustomerPackerRelationSyncService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomerPackerRelationSyncService));

        public List<Dictionary<string, string>> GetCustomerPackerRelation(string partnerFunction)
        {
            try
            {
                List<Dictionary<string, string>> returnData = new List<Dictionary<string, string>>();
                log.Debug("SSCBinSyncService Entered");
                ZWSDL_CUSPACK_LIST21 client = new ZWSDL_CUSPACK_LIST21();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_CustomerPackerRelationSync"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                ZwsdlCuspackList2 custpackList = new ZwsdlCuspackList2();
                custpackList.IvParvw = partnerFunction;
                ZwsdlCuspackList2Response response = client.ZwsdlCuspackList2(custpackList);
                ZsdCuspack2[] custPack =  response.Packers;
                foreach(var data in custPack)
                {
                    Dictionary<string, string> returnDictionary = new Dictionary<string, string>();
                    returnDictionary.Add("Customer", data.Customer);
                    returnDictionary.Add("Packer", data.Packer);
                    returnDictionary.Add("Subsi", data.Vkorg);
                    returnData.Add(returnDictionary);
                }
                return returnData;
            }
            catch(Exception e)
            {
                log.Error("CustomerPackerRelationSyncService Error Exception-" + e);
                throw e;
            }
        }

    }
}
