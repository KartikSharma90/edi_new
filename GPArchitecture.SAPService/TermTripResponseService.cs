﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using GPArchitecture.SAPService.TermTripStatus;
using System.Configuration;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.SAPService
{
    public class TermTripResponseService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TermTripResponseService));
        public TermTripResponseService()
        {
            log.Debug("TermTripResponseService Entered");

        }
        public List<ResponseModel>  TermTripResponse(string customerRef,string Customer,string itrNumber)
        {
            log.Info(" TermTripResponse customerRef:-" + customerRef + " ,Customer:-" + Customer + " ,itrNumber:-" + itrNumber);
            List<ResponseModel> responseList = new List<ResponseModel>();
            try
            {
                ZWS_TERM_TRIP_RESPONSE_V3 client = new ZWS_TERM_TRIP_RESPONSE_V3();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_TermTrip_Response"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                ZbapiTermTripResponse request = new ZbapiTermTripResponse();
                request.Custref = customerRef.Trim();
                request.Kunnr = Customer.Length == 6 ? "0000" + Customer.Trim() : Customer.Trim();
                request.Key = itrNumber.Trim();
                ZediScError [] errorresult = new ZediScError[0];
                request.Result = errorresult;
                ZbapiTermTripResponseResponse response = client.ZbapiTermTripResponse(request);
                int responseCount = response.Result.Count();
                ResponseModel responseModel;
               // bool error = false;               
                for (int i = 0; i < responseCount; i++)
                {
                    log.Info("Customer Number :-" + response.Result[i].Kunnr + "\n Customer Reference:-" + response.Result[i].Custref);
                    log.Info("Response from SC:-" + response.Result[i].Mtype);
                    log.Info("Message from SC:-" + response.Result[i].Mess);
                    itrNumber = response.Result[i].Itrno;
                    if (response.Result[i].Mtype == "E" )
                    {                
                        responseModel = new ResponseModel();
                        responseModel.Success = false;
                        responseModel.ResponseItems = itrNumber;
                        responseModel.Message = response.Result[i].Mess;
                        responseList.Add(responseModel);
                    }
                    else if (String.IsNullOrEmpty(response.Result[i].Mtype) || response.Result[i].Mtype == "W")
                    {
                        responseModel = new ResponseModel();
                        responseModel.Success = null;
                        responseModel.Message = response.Result[i].Mess;
                        responseList.Add(responseModel);
                    }
//To update as success it the last response is success
                    if((i+1)==responseCount)
                    {
                        if (response.Result[i].Mtype == "S")
                        {
                            responseList = new List<ResponseModel>();
                        }
                    }
                }
                if (responseList.Count == 0)
                {
                    responseModel = new ResponseModel();
                    responseModel.Success = true; responseModel.ResponseItems = itrNumber;
                    responseModel.Message = "Processed in SAP successfully";
                    responseList.Add(responseModel);
                }
                return responseList;
            }
            catch(Exception e)
            {
                log.Info(" TermTripResponse customerRef:-" + customerRef + " ,Customer:-" + Customer + " ,itrNumber:-" + itrNumber);
                log.Error("TermTripResponse Exception -"+e);
                ResponseModel responseModel;
                responseModel = new ResponseModel();
                responseModel.Success = null;
               // responseModel.Message = "Failed in SAP mode";
                responseList.Add(responseModel);
                return responseList;
            }
        }
        
    }
}
