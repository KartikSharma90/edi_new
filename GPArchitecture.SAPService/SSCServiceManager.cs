﻿
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using GoodpackEDI.SSCServiceReference1;  //QA pointing, 
using GPArchitecture.DataLayer.Models;
using System.Web.Configuration;
using GPArchitecture.Models;
using GPArchitecture.SAPService.SSCServiceReference;
using GPArchitecture.SAPService.ConstantUtils;
using GPArchitecture.EnumsAndConstants;


namespace GPArchitecture.SAPService.SSCManagementLayer
{
    public class SSCServiceManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SSCServiceManager));

        public List<ResponseModel> sscServiceCaller(List<Dictionary<string, string>> scDataVal, string serviceTypes, ref int successCount, ref int errorCount, string username)
        {

            ZWS_SSC_EDI_CREATE1Client client = new ZWS_SSC_EDI_CREATE1Client();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["SSCServiceReference1"]);
            client.ClientCredentials.UserName.UserName = GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPPassword;
            ZbapiSscEdi scData = new ZbapiSscEdi();
            scData.ZediSscFormat = new ZediSscFormat[scDataVal.Count + 1];
            for (int i = 0; i < scDataVal.Count; i++)
            {
             
                scData.ZediSscFormat[i] = new ZediSscFormat();
                if (serviceTypes != null)
                    scData.Mode = serviceTypes;
                scData.ZediSscFormat[i].Zscbno = scDataVal[i][SCSapMessageFields.BatchNumber];
                scData.ZediSscFormat[i].Zsmvtype = scDataVal[i][SCSapMessageFields.MvtType];
                scData.ZediSscFormat[i].ZitrnoEdi = scDataVal[i][SCSapMessageFields.EdiNumber];

                if ((scDataVal[i][SCSapMessageFields.FRMLocation] != ""))
                {
                    if (scDataVal[i][SCSapMessageFields.FRMLocation].Length != 10)
                    {
                        scData.ZediSscFormat[i].Zfrom = "0000" + scDataVal[i][SCSapMessageFields.FRMLocation];
                    }
                    else
                    {
                        scData.ZediSscFormat[i].Zfrom = scDataVal[i][SCSapMessageFields.FromLocation];
                    }
                }
                if ((scDataVal[i][SCSapMessageFields.TOLocation] != ""))
                {
                    if (scDataVal[i][SCSapMessageFields.TOLocation].Length != 10)
                    {
                        scData.ZediSscFormat[i].Zto = "0000" + scDataVal[i][SCSapMessageFields.TOLocation];
                    }
                    else
                    {
                        scData.ZediSscFormat[i].Zto = scDataVal[i][SCSapMessageFields.TOLocation];
                    }
                }
                scData.ZediSscFormat[i].Zpinno = scDataVal[i][SCSapMessageFields.TwoDPin];


                if ((scDataVal[i][SCSapMessageFields.CustomerReferenceNumber] != ""))
                    scData.ZediSscFormat[i].Cusref = scDataVal[i][SCSapMessageFields.CustomerReferenceNumber];
                if ((scDataVal[i][SCSapMessageFields.MaterialNumber] != ""))
                    scData.ZediSscFormat[i].Matnr = scDataVal[i][SCSapMessageFields.MaterialNumber];

                scData.ZediSscFormat[i].Menge = GPArchitecture.EnumsAndConstants.ConstantUtilities.Quantity;

                if ((scDataVal[i][SCSapMessageFields.ContainerNumber] != ""))
                    scData.ZediSscFormat[i].Zcontainer = scDataVal[i][SCSapMessageFields.ContainerNumber];
                if ((scDataVal[i][SCSapMessageFields.CustomerCode] != ""))
                {
                    if (scDataVal[i][SCSapMessageFields.CustomerCode].Length != 10)
                    {
                        scData.ZediSscFormat[i].Kunnr = "0000" + scDataVal[i][SCSapMessageFields.CustomerCode];
                    }
                    else
                    {
                        scData.ZediSscFormat[i].Kunnr = scDataVal[i][SCSapMessageFields.CustomerCode];
                    }
                }
                if ((scDataVal[i][SCSapMessageFields.ETDDate] != ""))
                {
                    string dateIssue = scDataVal[i][SCSapMessageFields.ETDDate];
                    if (dateIssue != null)
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        scData.ZediSscFormat[i].Etddate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                }
                if ((scDataVal[i][SCSapMessageFields.ETADate] != ""))
                {
                    string dateIssue = scDataVal[i][SCSapMessageFields.ETADate];
                    if (dateIssue != null)
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        scData.ZediSscFormat[i].Etadate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                }

                if ((scDataVal[i][SCSapMessageFields.ScanDate] != ""))
                {
                    string dtIssue = scDataVal[i][SCSapMessageFields.ScanDate];
                    if (dtIssue != null)
                    {
                        string dateVal = dtIssue.Substring(0, 2);
                        string monthVal = dtIssue.Substring(2, 2);
                        string yearVal = dtIssue.Substring(4, 4);
                        scData.ZediSscFormat[i].ZscanDate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                }

                scData.ZediSscFormat[i].Remark = username;
                scData.Result = new ZediScError[i];
            }
            ZbapiSscEdiResponse response = client.ZbapiSscEdi(scData);
            int responseCount = response.Result.Count();
            List<ResponseModel> responseList = new List<ResponseModel>();
            ResponseModel responseModel;
            bool error = false;
            for (int i = 0; i < responseCount; i++)
            {
                log.Info("From location :-" + scData.ZediSscFormat[0].Zfrom + "\n ToLocation:-" + scData.ZediSscFormat[0].Zto);
                log.Info("Response from SC:-" + response.Result[i].Mtype);
                log.Info("Message from SC:-" + response.Result[i].Mess);
                if (response.Result[i].Mtype == "E")
                {
                    if (!error)
                    {
                        errorCount++;
                        error = true;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = false;
                    if (response.Result[i].Mess != "")
                    {
                        responseModel.Message = response.Result[i].Mess;
                    }
                    else
                    {
                        responseModel.Message = "Error occured in SSC service call in SAP";
                    }
                    responseList.Add(responseModel);
                }

            }

            if (responseList.Count == 0)
            {
                if (!error)
                {
                    successCount++;
                }
                responseModel = new ResponseModel();
                responseModel.Success = true;
                if (serviceTypes != null)
                {
                    responseModel.Message = "Successful verification of data in verified mode";
                }
                else
                {
                    responseModel.Message = "Successful verification of data in processed mode";
                }
                responseList.Add(responseModel);
            }
            return responseList;
        }





        public List<ResponseModel> sscResubmitCaller(List<Trn_SSCTransLine> sscLine, string status, ref int successCount, ref int errorCount, string username, string mvtType)
        {
            ZWS_SSC_EDI_CREATE1Client client = new ZWS_SSC_EDI_CREATE1Client();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["SSCServiceReference1"]);
            client.ClientCredentials.UserName.UserName = GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPPassword;
            ZbapiSscEdi sscData = new ZbapiSscEdi();
            // sscData.ZediSscFormat = new ZediSscFormat[1];
            sscData.ZediSscFormat = new ZediSscFormat[sscLine.Count + 1];
            for (int i = 0; i < sscLine.Count; i++)
            {
                sscData.ZediSscFormat[i] = new ZediSscFormat();
                if (status != null)
                {
                    sscData.Mode = "X";
                }
                sscData.ZediSscFormat[i].Zscbno = sscLine[i].BatchNumber;
                sscData.ZediSscFormat[i].Zsmvtype = mvtType;
                sscData.ZediSscFormat[i].Zfrom = sscLine[i].FromLocation;
                sscData.ZediSscFormat[i].Zto = sscLine[i].ToLocation;
                sscData.ZediSscFormat[i].Zpinno = sscLine[i].TwoDPinNo;
                sscData.ZediSscFormat[i].Cusref = sscLine[i].CustomerReferenceNumber;
                sscData.ZediSscFormat[i].BlNo = sscLine[i].SourceReference;
                sscData.ZediSscFormat[i].ZitrnoEdi = sscLine[i].EDIReference;
                //sscData.ZediSscFormat[0].ZitrnoEdi = ITRNo;fixed
                sscData.ZediSscFormat[i].Matnr = sscLine[i].MaterialNumber;
                sscData.ZediSscFormat[i].Menge = GPArchitecture.EnumsAndConstants.ConstantUtilities.Quantity;
                //sscData.ZediSscFormat[0].Zcontainer =sscLine.CO;
                //sscData.ZediSscFormat[0].Meins = BaseunitofMeasure;fixed
                //sscData.ZediSscFormat[0].Zcontainer = sscLine.containerno;
                //DateTime.Now.ToString("yyyy-MM-dd");
                sscData.ZediSscFormat[i].Etadate = sscLine[i].ETADate;
                sscData.ZediSscFormat[i].Etddate = sscLine[i].ETDDate;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                sscData.ZediSscFormat[i].ZscanDate = sscLine[i].ScanDate;
                sscData.ZediSscFormat[i].Kunnr = sscLine[i].CustomerCode;
                sscData.ZediSscFormat[i].Remark = username;
                sscData.Result = new ZediScError[i];
            }
            ZbapiSscEdiResponse response = client.ZbapiSscEdi(sscData);
            int responseCount = response.Result.Count();
            List<ResponseModel> responseList = new List<ResponseModel>();
            ResponseModel responseModel;
            bool error = false;

            for (int i = 0; i < responseCount; i++)
            {
                log.Info("From location :-" + sscData.ZediSscFormat[0].Zfrom + "\n ToLocation:-" + sscData.ZediSscFormat[0].Zto);
                log.Info("Response from SSC:-" + response.Result[i].Mtype);
                log.Info("Message from SSC:-" + response.Result[i].Mess);
                if (response.Result[i].Mtype == "E")
                {
                    if (!error)
                    {
                        errorCount++;
                        error = true;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = false;
                    if (response.Result[i].Mess != "")
                        responseModel.Message = response.Result[i].Mess;
                    else
                        responseModel.Message = "Error Occured in line after SAP submission";
                    responseList.Add(responseModel);
                }

            }

            if (responseList.Count == 0)
            {
                if (!error)
                {
                    successCount++;
                }
                responseModel = new ResponseModel();
                responseModel.Success = true;
                if (status != null)
                {
                    responseModel.Message = "Successful verification of data in verified mode";
                }
                else
                {
                    responseModel.Message = "Successful verification of data mode in processed mode";
                }
                responseList.Add(responseModel);
            }
            return responseList;
        }
    }
}