﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GPArchitecture.DataLayer.Models;
using log4net;
using System.Globalization;
using System.Web.Configuration;
using GPArchitecture.SAPService.ServiceReference_SOEditThreeReasons;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.GoodPackBusinessLayer.SOManagementLayer
{
    public class SOServiceManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SOServiceManager));

        public List<ResponseModel> soServiceCaller(Dictionary<string, string> scDataVal, string serviceTypes, ref int successCount, ref int errorCount, string username, string subsiId,string custPOdate)
        {
            log.Info("SO Service calling. SO Data below");
            log.Info(scDataVal);
            Zws_BAPI_SO_EDI_changeClient client = new Zws_BAPI_SO_EDI_changeClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_SOEditThreeReasons"]);
            client.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;

            ZediSoFormat[] salesDataArray = new ZediSoFormat[1];

            salesDataArray[0] = new ZediSoFormat();

            salesDataArray[0].Auart = "Z01";/*scDataVal[SOSapMessageFields.SalesDocumentType];*/
            salesDataArray[0].Vkorg = getSubsiCode(Convert.ToInt32(subsiId)).ToString();
            salesDataArray[0].Vtweg = "00";/*scDataVal[SOSapMessageFields.DistributionChannel];*/
            salesDataArray[0].Spart = "00";
            salesDataArray[0].Vkbur = getSubsiCode(Convert.ToInt32(subsiId)).ToString();
            salesDataArray[0].Bstkd = scDataVal[SOSapMessageFields.CustPOnumber];
            salesDataArray[0].Bstdk = custPOdate; 

            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.Customer]))
            {
                if (scDataVal[SOSapMessageFields.Customer].Length != 10)
                {
                    salesDataArray[0].Sokunnr = "0000" + scDataVal[SOSapMessageFields.Customer];
                }
                else
                {
                    salesDataArray[0].Sokunnr =scDataVal[SOSapMessageFields.Customer];
                }
            }

            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.Packer]))
            {
                if (scDataVal[SOSapMessageFields.Packer].Length != 10)
                {
                    salesDataArray[0].Packer = "0000" + scDataVal[SOSapMessageFields.Packer];
                }
                else
                {
                    salesDataArray[0].Packer = scDataVal[SOSapMessageFields.Packer];
                }
            }
            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.Requestdeliverydate]))
            {
                string dateIssue = scDataVal[SOSapMessageFields.Requestdeliverydate];
                    string dateVal = dateIssue.Substring(0, 2);
                    string monthVal = dateIssue.Substring(2, 2);
                    string yearVal = dateIssue.Substring(4, 4);
                    salesDataArray[0].Vdatu = dateVal + "." + monthVal + "." + yearVal;
            }

            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.Orderdate]))
            {
                string dateIssue = scDataVal[SOSapMessageFields.Orderdate];
                    string dateVal = dateIssue.Substring(0, 2);
                    string monthVal = dateIssue.Substring(2, 2);
                    string yearVal = dateIssue.Substring(4, 4);
                    salesDataArray[0].Audat = dateVal + "." + monthVal + "." + yearVal;
            }

            if (String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.DeliveryDate]))
            {
                salesDataArray[0].Edatu = salesDataArray[0].Vdatu;               
            }
            else
            {
                string deldate = scDataVal[SOSapMessageFields.DeliveryDate];
                    string dateVal = deldate.Substring(0, 2);
                    string monthVal = deldate.Substring(2, 2);
                    string yearVal = deldate.Substring(4, 4);
                    salesDataArray[0].Edatu = dateVal + "." + monthVal + "." + yearVal;
            }

            salesDataArray[0].Htext = username;
            salesDataArray[0].Posnr = "000010";
            salesDataArray[0].Matnr = scDataVal[SOSapMessageFields.Material];

            salesDataArray[0].Kwmeng = scDataVal[SOSapMessageFields.Orderquantity];
            salesDataArray[0].Consigne = scDataVal[SOSapMessageFields.Consignee];

            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.VesselETA]))
            {
                string dateIssue = scDataVal[SOSapMessageFields.VesselETA];
                    string dateVal = dateIssue.Substring(0, 2);
                    string monthVal = dateIssue.Substring(2, 2);
                    string yearVal = dateIssue.Substring(4, 4);
                    salesDataArray[0].Zzveta = dateVal + "." + monthVal + "." + yearVal;
  
            }
            if (!String.IsNullOrWhiteSpace(scDataVal[SOSapMessageFields.VesselETD]))
            {
                string dateIssue = scDataVal[SOSapMessageFields.VesselETD];

                    string dateVal = dateIssue.Substring(0, 2);
                    string monthVal = dateIssue.Substring(2, 2);
                    string yearVal = dateIssue.Substring(4, 4);
                    salesDataArray[0].Zzvetd = dateVal + "." + monthVal + "." + yearVal;
            }

             salesDataArray[0].Zzvnam = scDataVal[SOSapMessageFields.Vesselname];
             salesDataArray[0].Zzvoyref = scDataVal[SOSapMessageFields.Voyagereference];
             salesDataArray[0].Zzshipln = scDataVal[SOSapMessageFields.Shippingline];
             salesDataArray[0].Zzpol = scDataVal[SOSapMessageFields.POL];
             salesDataArray[0].Zzpod = scDataVal[SOSapMessageFields.POD];
             salesDataArray[0].Itext = scDataVal[SOSapMessageFields.ItemRemarks];
             salesDataArray[0].Lifsk = scDataVal[SOSapMessageFields.DeliveryBlockOrders];
             salesDataArray[0].ZqtyChange = "";
             salesDataArray[0].ZetdChange = "";
             salesDataArray[0].ZskuChange = "";

            ZbapiSoEdi salesData = new ZbapiSoEdi();
            salesData.Result = new ZediSoError[0];
            salesData.ZediSoFormat = salesDataArray;
            ZbapiSoEdiResponse response = client.ZbapiSoEdi(salesData);
            int responseCount = response.Result.Count();
            List<ResponseModel> responseList = new List<ResponseModel>();
            ResponseModel responseModel;
            bool error = false;
            for (int i = 0; i < responseCount; i++)
            {
                if (response.Result[i].Msgty == "E")
                {
                    if (!error)
                    {
                        errorCount++;
                        error = true;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = false;
                    responseModel.Message = response.Result[i].Msg;
                    responseList.Add(responseModel);
                }         

            }
            if (responseList.Count == 0)
            {
                if (!error)
                {
                    successCount++;
                }
                responseModel = new ResponseModel();
                responseModel.Success = true;
                responseModel.Message = response.Result[0].Msg;
                responseList.Add(responseModel);
            }
            return responseList;

        }

       
        public int getSubsiCode(int id) 
        {
            using(GoodpackEDIEntities dataContext = new GoodpackEDIEntities())
            {
                Gen_EDISubsies subsies = (from s in dataContext.Gen_EDISubsies
                                 where s.Id == id
                                 select s).FirstOrDefault();

                return subsies.SubsiCode;

            }
        
        }
    }

}