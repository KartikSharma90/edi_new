﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService.SSC_BinNumberUpload;
using System.Configuration;
using log4net;
using GPArchitecture.SAPService.ConstantUtils;
using GoodpackEDI.ViewModels;

namespace GPArchitecture.SAPService
{
    public class SSCBinNumberUploadService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SSCBinNumberUploadService));
        public List<ResponseModel> SSCBinSyncService(string Customer, string CustomerRef, string BinNumber)
        {
            List<ResponseModel> responseList = new List<ResponseModel>();
            try
            {
                log.Debug("SSCBinSyncService Entered");
                ZWS_SSC_UPLOAD client = new ZWS_SSC_UPLOAD();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_SSCBinUpload"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                ZbapiSscUpload[] uploadArr = new ZbapiSscUpload[1];
                ZbapiSscUpload upload = new ZbapiSscUpload();
                upload.Kunnr = Customer.Length == 6 ? "0000" + Customer : Customer;
                upload.Cusref = CustomerRef;
                upload.ZrfidTag = BinNumber;
                uploadArr[0] =  upload;               
                ZbapiSscUploadRet [] uploadRet = new ZbapiSscUploadRet[0];
                ZbapiSscDataUpload dataupload=new ZbapiSscDataUpload();
                dataupload.ZbapiSscUpload = uploadArr;
                dataupload.ZbapiSscUploadRet = uploadRet;
                ZbapiSscDataUploadResponse returnVal = client.ZbapiSscDataUpload(dataupload);
                ResponseModel responseModel = new ResponseModel();
                for (int i = 0; i < returnVal.ZbapiSscUploadRet.Length; i++)
                {
                    if (returnVal.ZbapiSscUploadRet[0].Type == "E")
                    {
                        responseModel.Success = false;
                        responseModel.Message = returnVal.ZbapiSscUploadRet[0].Message;
                    }
                    else if (returnVal.ZbapiSscUploadRet[0].Type == "S")
                    {
                        responseModel.Success = true;
                        responseModel.Message = returnVal.ZbapiSscUploadRet[0].Message;
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "No response from SAP";
                    }
                    responseList.Add(responseModel);
                }
                log.Debug("SSCBinSyncService SAP response : " + returnVal.ZbapiSscUploadRet[0].Message);
            }
            catch (Exception e)
            {
                log.Error("SSCBinSyncService Error" + e);
                ResponseModel responseModel = new ResponseModel();
                responseModel.Success = null;
                responseModel.Message = e.Message;
                responseList.Add(responseModel);
            } 
            return responseList;
        }
        
    }
}
