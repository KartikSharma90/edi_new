﻿using System;
using System.Linq;
//using GoodpackEDI.ServiceReference_LSPCR;     //QA pointing, 
//using GoodpackEDI.ServiceReference_LSPCR_Update;    //QA pointing, 
//using GoodpackEDI.ServiceReference_LSPCRDevNew;
//using GoodpackEDI.ServiceReference_LSPCR_UpdateDevNew;
using GPArchitecture.DataLayer.Models;
using log4net;
using System.Web.Configuration;
using GPArchitecture.Models.ViewModels;
using GPArchitecture.Models;
using GPArchitecture.SAPService.ServiceReference_LSPCR_Update_PROD;
using GPArchitecture.SAPService.ServiceReference_LSPCR_PROD;

namespace GPArchitecture.SAPService.LSPCRManagementLayer
{
    public class LSPCRServiceManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LSPCRServiceManager));
        public LSPCRModel ValidateJobId(string jobId, string code)
        {

            //LSPCRModel model = new LSPCRModel();
            //model.IsValid = true;
            //model.Message = "Valid";
            //model.JobId = jobId;
            //model.From = "604265";
            //model.FromAddress = "Goodpack";
            //model.To = "501731";
            //model.ToAddress = "Gp Korea";
            //model.SKU = "MB5";
            //model.Quantity = "2";
            //model.ProposedPickUpDate = "2014-08-22";
            //model.ConfirmedPickUpDate = "2014-07-20";
            //model.Vendor = "Attinad";
            //model.Remarks = "All the best";
            //return model;
            log.Info("Validate jodId:" + jobId + ",code:" + code);

            return validateService(jobId, code);
        }
        public ResponseModel SubmitLSPData(LSPCRModel model)
        {
            log.Info("Submit LSP data:proposed date");
            ResponseModel responseModel = new ResponseModel();
            string responseType;
            try
            {
                log.Info("Submit LSP data:proposed date:" + model.ProposedPickUpDate + ",confirmed Date:" + model.ConfirmedPickUpDate + "for jobId:" + model.JobId);
                ZBAPI_WSDL_LSPJOB_UPDATEClient client = new ZBAPI_WSDL_LSPJOB_UPDATEClient();
                client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_LSPCR_Update"]);
                client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
                client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

                string PrPickDate = model.ProposedPickUpDate;
                string CfPickDate = model.ConfirmedPickUpDate;

                log.Info("Submit LSP data:proposed date:" + DateTime.Parse(PrPickDate).ToString("yyyy-MM-dd") + ",confirmed Date:" + DateTime.Parse(CfPickDate).ToString("yyyy-MM-dd") + "for jobId:" + model.JobId);
                ZbapiWsdlLspjobUpdate jobUpdate = new ZbapiWsdlLspjobUpdate();
                GPArchitecture.SAPService.ServiceReference_LSPCR_Update_PROD.ZstLspJob lspjob = new GPArchitecture.SAPService.ServiceReference_LSPCR_Update_PROD.ZstLspJob();
                lspjob.CfPickDate = DateTime.Parse(CfPickDate).ToString("yyyy-MM-dd");
                lspjob.Jobid = model.JobId;
                lspjob.Lifnr = model.Vendor;
                lspjob.PrPickDate = DateTime.Parse(PrPickDate).ToString("yyyy-MM-dd");
                lspjob.Qty =Convert.ToDecimal(model.Quantity);
                lspjob.Sku = model.SKU;
                lspjob.Zfrom = model.From;
                lspjob.Zremarks = "";
                lspjob.Zto = model.To;

                jobUpdate.LspJob = lspjob;
                ZbapiWsdlLspjobUpdateResponse response = client.ZbapiWsdlLspjobUpdate(jobUpdate);

                responseType = response.Return.Type;
                log.Info("SAP response:" + responseType);
                if (responseType == "S")
                {
                    responseModel.Success = true;
                    responseModel.Message = response.Return.Message;
                    log.Info("SAP response Message:" + responseModel.Message);
                }
                else
                {
                    responseModel.Success = false;
                    responseModel.Message = response.Return.Message;
                    log.Info("SAP response Message:" + responseModel.Message);
                }
            }
            catch (Exception exc)
            {

                log.Error("Error encountered in SubmitLSPData method. Message:" + exc.Message);
                throw exc;
            }


                using (var dataContext = new GoodpackEDIEntities())
                {

                    Trn_LSPCR lspDetails;
                    log.Info("Entering database with new data for jobId:" + model.JobId);
                    lspDetails = dataContext.Trn_LSPCR.FirstOrDefault(i => i.JobId == model.JobId);
                    if (lspDetails == null)
                    {
                        log.Info("Job details is null.Updating LSP data in the database with new data for jobId:" + model.JobId);
                        lspDetails = new Trn_LSPCR();
                        lspDetails.CreatedDate = DateTime.Now.ToString();
                        log.Info("Created Date:" + DateTime.Now.ToString());
                        lspDetails.ConfirmPickUpDate = model.ConfirmedPickUpDate;
                        lspDetails.From = model.From;
                        lspDetails.JobId = model.JobId;
                        lspDetails.ProposedPickUpDate = model.ProposedPickUpDate;
                        lspDetails.Quantity = model.Quantity;
                        lspDetails.Remarks = model.Remarks;
                        lspDetails.ResponseMessage = responseModel.Message;
                        lspDetails.ResponseType = responseType;
                        lspDetails.SKU = model.SKU;
                        lspDetails.To = model.To;
                        lspDetails.Vendor = model.Vendor;
                        dataContext.Trn_LSPCR.Add(lspDetails);
                        try
                        {

                            dataContext.SaveChanges();
                        }
                        catch (Exception exc)
                        {
                            log.Info("Created Date:" + DateTime.Now.ToString());
                            log.Error("Error encountered in SubmitLSPData insert method when inserting data to the databse. Message:" + exc.Message);
                            throw exc;

                        }
                        log.Info("Updating LSP data in the database with new data for jobId:" + model.JobId);
                    }
                    else
                    {
                        log.Info("Job details not null.Inserting LSP data in the database with new data for jobId:" + model.JobId);
                        lspDetails.CreatedDate = DateTime.Now.ToString();
                        log.Info("Created Date Update:" + DateTime.Now.ToString());
                        lspDetails.ConfirmPickUpDate = model.ConfirmedPickUpDate;
                        lspDetails.From = model.From;
                        lspDetails.JobId = model.JobId;
                        lspDetails.ProposedPickUpDate = model.ProposedPickUpDate;
                        lspDetails.Quantity = model.Quantity;
                        lspDetails.Remarks = model.Remarks;
                        lspDetails.ResponseMessage = responseModel.Message;
                        lspDetails.ResponseType = responseType;
                        lspDetails.SKU = model.SKU;
                        lspDetails.To = model.To;
                        lspDetails.Vendor = model.Vendor;
                        try
                        {
                            dataContext.SaveChanges();
                        }
                        catch (Exception exc)
                        {
                            log.Info("Created Date:" + DateTime.Now.ToString());
                            log.Error("Error encountered in SubmitLSPData update method when inserting data to the databse. Message:" + exc.Message);
                            throw exc;

                        }
                        log.Info("Inserting LSP data in the database with new data for jobId:" + model.JobId);
                    }
                
            }         

            return responseModel;
        }
        private LSPCRModel validateService(string jobId, string code)
        {
            LSPCRModel model = new LSPCRModel();
            try
            {
                log.Info("validateService for jobId:" + jobId + ",code:" + code);
                ZBAPI_WSDL_LSPCRClient client = new ZBAPI_WSDL_LSPCRClient();
                client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_LSPCR"]);
                client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
                client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

                ZbapiWsdlLspcr jobIdArray = new ZbapiWsdlLspcr();
                jobIdArray.Code = code;
                jobIdArray.Jobid = jobId;

                ZbapiWsdlLspcrResponse response = client.ZbapiWsdlLspcr(jobIdArray);

                string type = response.Return.Type;
                log.Info("SAP response:" + type);
                if (type == "S")
                {
                    model.IsValid = true;
                    model.Message = response.Return.Message;
                    model.JobId = response.Joblog.Jobid;
                    model.From = response.Joblog.Zfrom;
                    model.To = response.Joblog.Zto;
                    model.SKU = response.Joblog.Sku;
                    model.Quantity = response.Joblog.Qty.ToString();
                    string proposedPickUpDate = response.Joblog.PrPickDate;
                    model.ProposedPickUpDate = DateTime.Parse(proposedPickUpDate).ToString("dd-MMM-yyyy");

                    string confirmedPickUpDate = response.Joblog.CfPickDate;
                    if (confirmedPickUpDate == "0000-00-00")
                    {
                        model.ConfirmedPickUpDate = model.ProposedPickUpDate;
                    }
                    else
                    {
                        model.ConfirmedPickUpDate = DateTime.Parse(confirmedPickUpDate).ToString("dd-MMM-yyyy");
                    }

                    model.Vendor = response.Joblog.Lifnr;
                    model.Remarks = response.Joblog.Zremarks;
                    log.Info("SAP response Message:" + model.Message);
                }
                else
                {
                    model.IsValid = false;
                    model.Message = response.Return.Message;
                    log.Info("SAP response Message:" + model.Message);
                }
            }
            catch (Exception exc)
            {
                model.IsValid = false;
                model.Message = "Unable to complete action. Please try again";
                log.Error("Error encountered in validateService method . Message:" + exc.Message);
            }
            return model;
        }

    }
}