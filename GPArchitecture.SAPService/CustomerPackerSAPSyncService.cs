﻿using log4net;
using System;
using System.Collections.Generic;
using GPArchitecture.SAPService.CustomerPackerSAPSync;
using System.Configuration;
using GPArchitecture.SAPService.ConstantUtils;


namespace GPArchitecture.SAPService
{
    public class CustomerPackerSAPSyncService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomerPackerSAPSyncService));

        public List<Dictionary<string, string>> GetCustPackrConsineWarehousePortList(string type)
        {
            try
            {
                List<Dictionary<string, string>> returnList = new List<Dictionary<string, string>>();
                ZWSDL_CUSPACK_LIST1 client = new ZWSDL_CUSPACK_LIST1();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_CustomerPackerSAPSync"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                ZwsdlCuspackList1 list = new ZwsdlCuspackList1();
                list.IvKtokd = type;
                ZwsdlCuspackList1Response response = client.ZwsdlCuspackList1(list);
                ZsdCuspack[] customerpackerList = response.Customers;
                foreach (var listVal in customerpackerList)
                {
                    Dictionary<string, string> returnDictionary = new Dictionary<string, string>();
                    returnDictionary.Add("CustomerCode", listVal.Kunnr);
                    returnDictionary.Add("CustomerName", listVal.Name1);
                    returnDictionary.Add("CustomerSearchName", listVal.Sort1);
                    returnDictionary.Add("SubsiCode", listVal.Vkorg);
                    returnList.Add(returnDictionary);
                }
                return returnList;
            }
            catch(Exception e)
            {
                log.Error("CustomerPackerSAPSyncService Error Exception-" + e);
                throw e;
            }
        }
    }
}
