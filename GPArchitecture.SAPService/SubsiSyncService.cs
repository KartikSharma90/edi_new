﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService.SubsiSAPSyncService;
using System.Configuration;

using log4net;
using GPArchitecture.SAPService.ConstantUtils;

namespace GPArchitecture.SAPService
{
    public  class SubsiSyncService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SubsiSyncService));

        public List<Dictionary<string, string>> GetSubsiDetails()
        {
            try
            {
                List<Dictionary<string, string>> returnList = new List<Dictionary<string, string>>();
                ZWSDL_BU_LIST client = new ZWSDL_BU_LIST();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_SubsiSAPsync"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                ZwsdlBuList list = new ZwsdlBuList();
                ZwsdlBuListResponse response = client.ZwsdlBuList(list);
                ZsdBu[] subsiList = response.EtBulist;
                foreach (var listVal in subsiList)
                {
                    Dictionary<string, string> returnDictionary = new Dictionary<string, string>();
                    returnDictionary.Add("SubsiName", listVal.Vtext);
                    returnDictionary.Add("SubsiCode", listVal.Vkorg);
                    returnList.Add(returnDictionary);
                }
                return returnList;
            }
            catch (Exception e)
            {
                log.Error("SubsiSyncService Error Exception-" + e);
                throw e;
            }
        }
    }
}
