﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService.LSPCRUpdateService;
using System.Configuration;
using GPArchitecture.SAPService.ConstantUtils;



namespace GPArchitecture.SAPService
{
    public class LSPCRValidateService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LSPCRValidateService));

        public Dictionary<string, string> LSPCRValidate(string jobId, string code)
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            try
            {
                log.Info("LSPCRValidate for jobId:" + jobId + ",code:" + code);
                ZBAPI_WSDL_LSPCR_DWLDService client = new ZBAPI_WSDL_LSPCR_DWLDService();
                client.Url = ConfigurationManager.AppSettings["ServiceReference_LSPACRValidateService"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);

                ZbapiWsdlLspcr jobIdArray = new ZbapiWsdlLspcr();
                jobIdArray.Code = code;
                jobIdArray.Jobid = jobId;
                ZbapiWsdlLspcrResponse response = client.ZbapiWsdlLspcr(jobIdArray);
                string type = response.Return.Type;
                log.Info("SAP response:" + type);
                if (type == "S")
                {
                    returnValue.Add("IsValid", "true");
                    returnValue.Add("Message", response.Return.Message);
                    returnValue.Add("JobId", response.Joblog.Jobid);
                    returnValue.Add("From", response.Joblog.Zfrom);
                    returnValue.Add("FromAddress", response.Joblog.FromAdd);
                    returnValue.Add("To", response.Joblog.Zto);
                    returnValue.Add("ToAddress", response.Joblog.ToAdd);
                    returnValue.Add("SKU", response.Joblog.Sku);
                    returnValue.Add("lspname", response.Joblog.Vname1);
                    returnValue.Add("ItrQuantity", response.Joblog.ItrQty);
                    returnValue.Add("Quantity", response.Joblog.Qty.ToString());
                    returnValue.Add("ProposedPickUpDate", DateTime.Parse(response.Joblog.PrPickDate).ToString("dd-MMM-yyyy"));
                    returnValue.Add("ConfirmedPickUpDate", response.Joblog.CfPickDate == "0000-00-00" ? DateTime.Parse(response.Joblog.PrPickDate).ToString("dd-MMM-yyyy") : DateTime.Parse(response.Joblog.CfPickDate).ToString("dd-MMM-yyyy"));
                    returnValue.Add("Vendor", response.Joblog.Lifnr);
                    returnValue.Add("Remarks", response.Joblog.Zremarks);

                    log.Info("SAP response success Message:" + response.Return.Message);                    
                }
                else
                {
                    returnValue.Add("IsValid", "false");
                    returnValue.Add("Message", response.Return.Message);
                    log.Info("SAP response fail Message:" + response.Return.Message);
                }
            }
            catch (Exception exc)
            {                
                returnValue.Add("IsValid", "false");
                returnValue.Add("Message", "Unable to complete action. Please try again");
                log.Info("LSPCRValidate fail Message:" + exc);
            }
            return returnValue;            
        }
    }
}
