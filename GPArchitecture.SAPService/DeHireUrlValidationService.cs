﻿using System.Collections.Generic;
using log4net;
using GPArchitecture.SAPService.DeHireUrlValidationProd;
using System.Configuration;

using System;
using GPArchitecture.SAPService.ConstantUtils;

namespace GPArchitecture.SAPService
{
    public class DeHireUrlValidationService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DeHireUrlValidationService));

        public Dictionary<string, string> DeHireUrlValues(string key)
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            try
            {                
                ZWS_PRODH_CODE clientDetail = new ZWS_PRODH_CODE();
                //clientDetail.Endpoint.Address = new System.ServiceModel.EndpointAddress(ConfigurationManager.AppSettings["ServiceReference_DeHireUrlValues"]);
                //clientDetail.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
                //clientDetail.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;
                clientDetail.Url = ConfigurationManager.AppSettings["ServiceReference_DeHireUrlValues"];
                clientDetail.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);

                ZWsdlProactiveDhCode deHireUrl = new ZWsdlProactiveDhCode();
                deHireUrl.Code = key;
                deHireUrl.ZproactiveDh = new ZproactiveDh[0];

                ZWsdlProactiveDhCodeResponse response = clientDetail.ZWsdlProactiveDhCode(deHireUrl);
                if (response.Return.Message != "Code is invalid")
                {
                    returnValue.Add("ConsigneeCode", response.ZproactiveDh[0].Zfrom);
                    returnValue.Add("StartDate", response.ZproactiveDh[0].Sdate);
                    returnValue.Add("EndDate", response.ZproactiveDh[0].Edate);
                    returnValue.Add("BinTypes", response.ZproactiveDh[0].Sku);
                }
            }
            catch(Exception e)
            {
                log.Error("DeHireUrlValues error -" + e);
            }
            return returnValue; 
        }        
    }
}
