using log4net;
 using System;
 using System.Collections.Generic;
 using System.ComponentModel;
 using System.Data;
 using System.Diagnostics;
 using System.Linq;
 using System.ServiceProcess;
 using System.Text;
 using System.Threading.Tasks;
 
 namespace GoodPackFileTransferService
 {
     public partial class FileTransferService : ServiceBase
     {
         internal static ILog log = LogManager.GetLogger(typeof(FileTransferService));        
         public System.Timers.Timer fileTransferSyncTimer;
         private static int NoofAttempts = 0;
         FileTransfer fileTransfer = new FileTransfer();
         public FileTransferService()
         {
             InitializeComponent();
         }
         public void debugger()
         {
             log.Info("Entering into Start method");
             OnStart(null);
             log.Info("Leaving Start method");
         }
         protected override void OnStart(string[] args)
         {
             log.Debug("Entering FileTransferService OnStart");
             this.fileTransferSyncTimer = new System.Timers.Timer();
             TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
             this.fileTransferSyncTimer.Interval = objTimeInterval.TotalMilliseconds;
             this.fileTransferSyncTimer.Elapsed += fileTransferSyncTimer_Elapsed;
             this.fileTransferSyncTimer.Enabled = true;
             this.fileTransferSyncTimer.Start();
             log.Debug("Leaving FileTransferService Service OnStart");
             //while (true) { }
         }
 
         void fileTransferSyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
         {
             if (!fileTransfer.isProcessing)
                 fileTransfer.FtpDownload();
         }
 
         protected override void OnStop()
         {
             this.fileTransferSyncTimer.Stop();
             while (fileTransfer.isProcessing)
             {
                 int timeMillisec = 5000;
                 NoofAttempts++;
                 this.RequestAdditionalTime(timeMillisec);
                 System.Threading.Thread.Sleep(timeMillisec);
                 if (NoofAttempts > 2)
                 {
                     log.Warn("TermTrip Services Stops Abruptly");
                     break;
                 }
             }
             this.ExitCode = 0;
 
         }
     }
 }

 

