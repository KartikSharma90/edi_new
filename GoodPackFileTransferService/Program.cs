 using log4net;
 using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.ServiceProcess;
 using System.Text;
 using System.Threading.Tasks;
 
 namespace GoodPackFileTransferService
 {
     static class Program
     {
         /// <summary>
         /// The main entry point for the application.
         /// </summary>
         ///
         /// 
         internal static ILog log = LogManager.GetLogger(typeof(Program));
         static void Main()
         {
             log4net.Config.XmlConfigurator.Configure();
             #if DEBUG
                 log.Info("Entered in Program.cs");
                 FileTransferService service = new FileTransferService();
                 service.debugger();
             # else
                 ServiceBase[] ServicesToRun;
                 ServicesToRun = new ServiceBase[] 
                 { 
                     new FileTransferService() 
                 };
                 ServiceBase.Run(ServicesToRun);
             # endif
         }
     }
 }

 

