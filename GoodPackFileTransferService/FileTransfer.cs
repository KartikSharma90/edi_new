using GPArchitecture.Adapters.FTP;
 using GPArchitecture.Adapters.SFTP;
 using GPArchitecture.DataLayer.Models;
 using GPArchitecture.EnumsAndConstants;
 using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
 using GPArchitecture.GoodPackBusinessLayer.SOManagerLayer;
 using GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer;
 using GPArchitecture.Models;
 using GPArchitecture.Parsers;
 using GPArchitecture.Utilities;
 using log4net;
 using System;
 using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Net.Mail;
 using System.Text;
 using System.Threading.Tasks;
 
 namespace GoodPackFileTransferService
 {
     public class FileTransfer
     {
         internal static ILog log = LogManager.GetLogger(typeof(FileTransfer));
         public bool isProcessing = false;
         private readonly GoodpackEDIEntities context = null;
         public FileTransfer() 
         {
             context = new GoodpackEDIEntities();
         }
 
         public void FtpDownload()
         {
             try
             {                
                 isProcessing = true;
                 DownloadFilesFromFTPToLocal();
                 ProcessDownloadedFTPFiles();
                 DownloadFilesFromSFTPToLocal();                
                 ProcessDownloadedSFTPFiles();
                 UploadFilesFromLocalToRemoteFTP();
                 UploadFilesFromLocalToRemoteSFTP();
             }
             catch (Exception e)
             {
                 log.Error("FileTransfer error-" + e);
             }
              
             isProcessing = false;
         }
 
         private void DownloadFilesFromFTPToLocal()
         {
             try
             {
                 List<Trn_File_Transfer> ftpDownloadList = (from s in context.Trn_File_Transfer
                                                            where s.FileTransferType == "DOWNLOAD"
                                                            select s).ToList();
                 foreach (Trn_File_Transfer files in ftpDownloadList)
                 {
                     Gen_FTPAccount ftpAccntDetail = context.Gen_FTPAccount.Where(i => i.SftpId == files.SftpID).FirstOrDefault();
                     Ftp ftpClient = new Ftp(ftpAccntDetail);
                     ftpClient.OpenConnection();
                     if (ftpClient.IsConnected)
                     {
                         string fromLocation = (files.LocationFrom.LastIndexOf(@"/") == files.LocationFrom.Length - 1) ? files.LocationFrom : files.LocationFrom + "/";
                         bool hasDownloaded = ftpClient.DownloadFile(fromLocation, files.LocationTo, files.IsDeleteFile);
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("DownloadFilesFromFTPToLocal error-" + e);
             }
         }
 
         private void DownloadFilesFromSFTPToLocal()
         {
             try
             {
                 List<Trn_SFTP_File_Transfer> ftpDownloadList = (from s in context.Trn_SFTP_File_Transfer
                                                                 where s.FileTransferType == "DOWNLOAD" 
                                                            select s).ToList();
                 foreach (Trn_SFTP_File_Transfer files in ftpDownloadList)
                 {
                     Gen_SFTPAccount ftpAccntDetail = context.Gen_SFTPAccount.Where(i => i.SftpId == files.SftpID).FirstOrDefault();
                     Sftp ftpClient = new Sftp(ftpAccntDetail);
                     ftpClient.OpenConnection();
                     if (ftpClient.IsConnected)
                     {
                         string fromLocation = (files.LocationFrom.LastIndexOf(@"/") == files.LocationFrom.Length - 1) ? files.LocationFrom : files.LocationFrom + "/";
                         bool hasDownloaded = ftpClient.DownloadFile(fromLocation, files.LocationTo, files.IsDeleteFile);
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("DownloadFilesFromSFTPToLocal error-" + e);
             }
         }
 
         private void ProcessDownloadedFTPFiles()
         {
             try
             {
                 List<Trn_File_Transfer> ftpDownloadList = (from s in context.Trn_File_Transfer
                                                            where s.FileTransferType == "DOWNLOAD" && s.IsTransferAndProcess == true
                                                            select s).ToList();
                 foreach (Trn_File_Transfer files in ftpDownloadList)
                 {
                     try
                     {
                         string toLocation = (files.LocationTo.LastIndexOf(@"\") == files.LocationTo.Length - 1) ? files.LocationTo : files.LocationTo + @"\";
                         string[] filePaths = Directory.GetFiles(toLocation);
                         string transactionType = context.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename.Trim() == files.MapperName).Select(i => i.Ref_TransactionTypes.TransactionCode).FirstOrDefault();
                         foreach (string path in filePaths)
                         {
                             int subsi = Convert.ToInt32(files.SubsiId);
                             string fileName = Path.GetFileName(path);
                             GPCustomerMessage objGPCustomerMessage = new GPCustomerMessage();
                             objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                             objGPCustomerMessage.BatchFileName = fileName;
                             objGPCustomerMessage.TransactionType = (transactionType == "SO") ? TRANSACTION_TYPES.SO : (transactionType == "SC") ? TRANSACTION_TYPES.SC : (transactionType == "SSC") ? TRANSACTION_TYPES.SSC : TRANSACTION_TYPES.ASN;
                             objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                             objGPCustomerMessage.DateCreated = DateTime.Now;
                             objGPCustomerMessage.BatchFileSourceAddress = path;
                             objGPCustomerMessage.MapperName = files.MapperName;
                             objGPCustomerMessage.Username = context.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename == files.MapperName).Select(i => i.Gen_User.Username).FirstOrDefault();
                             objGPCustomerMessage.SubsiId = subsi;
                             ReadFileData(objGPCustomerMessage, path);
                             if (subsi > 0)
                             {
                                 if (transactionType == "SO")
                                 {
                                     try
                                     {
                                         IList<GenericItemModel> soReturnValue = null;
                                         SOTransactionManager trnSoManager = new SOTransactionManager();
                                         soReturnValue = trnSoManager.SOFileData(files.MapperName, subsi, 3, path);                                       
                                         ProcessErrorFile(objGPCustomerMessage);                                   
                                     }
                                     catch (Exception e)
                                     {
                                         log.Error("ProcessDownloadedFiles -SO processing -" + e);
                                         ProcessErrorFile(objGPCustomerMessage);
                                     }
                                 }
                                 else if (transactionType == "SC")
                                 {
                                     try
                                     {
                                         IList<GenericItemModel> scReturnValue = null;
                                         SCTransactionManager trnScManager = new SCTransactionManager();
                                         scReturnValue = trnScManager.SCFileData(files.MapperName, subsi, 3, path);                                      
                                         ProcessFtpFile(scReturnValue, transactionType, objGPCustomerMessage);                                     
                                     }
                                     catch (Exception e)
                                     {
                                         log.Error("ProcessDownloadedFiles -SC processing -" + e);
                                         ProcessErrorFile(objGPCustomerMessage);
                                     }
                                 }
                                 else if (transactionType.Trim() == "SSC")
                                 {
                                     try
                                     {
                                         IList<GenericItemModel> sscReturnValue = null;
                                         IList<GenericItemModel> sscResult = null;
                                         ISSCManagerNew sscManager = new SSCManagerNew();
                                         SCTransactionManager trnSSCManager = new SCTransactionManager();
                                         var userId = (from s in context.Trn_SubsiToTransactionFileMapper
                                                       where s.Filename == files.MapperName.Trim()
                                                       select s.Gen_User.Username).FirstOrDefault();
                                         sscReturnValue = trnSSCManager.SCFileData(files.MapperName, subsi, 3, path);
 
                                         ProcessFtpFile(sscReturnValue, transactionType, objGPCustomerMessage);
                                     }
                                     catch (Exception e)
                                     {
                                         log.Error("ProcessDownloadedFiles -SSC processing -" + e);
                                         ProcessErrorFile(objGPCustomerMessage);
                                     }
                                 }
                             }
                         }
                     }
                     catch(Exception e)
                     {
                         log.Error("ProcessDownloadedFTPFiles error -" + e);
                     }
                 }
             }
             catch(Exception e)
             {
                 log.Error("ProcessDownloadedFiles error-" + e);
             }
         }
 
         private void ProcessDownloadedSFTPFiles()
         {
             try
             {
                 List<Trn_SFTP_File_Transfer> ftpDownloadList = (from s in context.Trn_SFTP_File_Transfer
                                                                 where s.FileTransferType == "DOWNLOAD" && s.IsTransferAndProcess == true && s.IsLanxess==false
                                                            select s).ToList();
                 foreach (Trn_SFTP_File_Transfer files in ftpDownloadList)
                 {
                     string toLocation = (files.LocationTo.LastIndexOf(@"\") == files.LocationTo.Length - 1) ? files.LocationTo : files.LocationTo + @"\";
                     log.Info("to location path -" + toLocation);
                     string[] filePaths = Directory.GetFiles(toLocation);
                     string transactionType = context.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename.Trim() == files.MapperName).Select(i => i.Ref_TransactionTypes.TransactionCode).FirstOrDefault();
                     foreach (string path in filePaths)
                     {
                         int subsi = Convert.ToInt32(files.SubsiId);
                         string fileName = Path.GetFileName(path);
                          GPCustomerMessage objGPCustomerMessage = new GPCustomerMessage();
                         objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                         objGPCustomerMessage.BatchFileName = fileName;
                         objGPCustomerMessage.TransactionType = (transactionType == "SO") ? TRANSACTION_TYPES.SO : (transactionType == "SC") ? TRANSACTION_TYPES.SC : (transactionType == "SSC") ? TRANSACTION_TYPES.SSC : TRANSACTION_TYPES.ASN;
                         objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                         objGPCustomerMessage.DateCreated = DateTime.Now;
                         objGPCustomerMessage.BatchFileSourceAddress = path;
                         objGPCustomerMessage.MapperName = files.MapperName;
                         objGPCustomerMessage.Username = context.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename == files.MapperName).Select(i => i.Gen_User.Username).FirstOrDefault();
                         objGPCustomerMessage.SubsiId = subsi;
                         ReadFileData(objGPCustomerMessage, path);
                         if (subsi > 0)
                         {
                             if (transactionType == "SO")
                             {
                                 try
                                 {
                                     IList<GenericItemModel> soReturnValue = null;
                                     SOTransactionManager trnSoManager = new SOTransactionManager();
                                     soReturnValue = trnSoManager.SOFileData(files.MapperName, subsi, 3, path);                                  
                                     ProcessFtpFile(soReturnValue, transactionType, objGPCustomerMessage);
                                 }
                                 catch (Exception e)
                                 {
                                     log.Error("ProcessDownloadedSFTPFiles -SO processing -" + e);
                                     ProcessErrorFile(objGPCustomerMessage);
                                 }
                             }
                             else if (transactionType == "SC")
                             {
                                 try
                                 {
                                     IList<GenericItemModel> scReturnValue = null;
                                     SCTransactionManager trnScManager = new SCTransactionManager();
                                     scReturnValue = trnScManager.SCFileData(files.MapperName, subsi, 3, path);
                                     ProcessFtpFile(scReturnValue, transactionType, objGPCustomerMessage);
                                 }
                                 catch (Exception e)
                                 {
                                     log.Error("ProcessDownloadedSFTPFiles -SC processing -" + e);
                                     ProcessErrorFile(objGPCustomerMessage);
                                 }
                             }
                             else if (transactionType.Trim() == "SSC")
                             {
                                 try
                                 {
                                     IList<GenericItemModel> sscReturnValue = null;
                                     IList<GenericItemModel> sscResult = null;
                                     ISSCManagerNew sscManager = new SSCManagerNew();
                                     SCTransactionManager trnSSCManager = new SCTransactionManager();
                                     var userId = (from s in context.Trn_SubsiToTransactionFileMapper
                                                   where s.Filename == files.MapperName.Trim()
                                                   select s.Gen_User.Username).FirstOrDefault();
                                     sscReturnValue = trnSSCManager.SCFileData(files.MapperName, subsi, 3, path);
 
                                     ProcessFtpFile(sscReturnValue, transactionType, objGPCustomerMessage);
 
                                    
                                      
                                     //string copyfileName = objGPCustomerMessage.MapperName + "_" + Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName) + "_" + DateTime.Now.ToString("dd-MMM-yyyyHH.MM.ss") + Path.GetExtension(objGPCustomerMessage.BatchFileSourceAddress);
                                     //string destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpProcessedFilePath").Select(i => i.Value).FirstOrDefault() + "\\" + copyfileName;
                                     //System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                                 }
                                 catch (Exception e)
                                 {
                                     log.Error("ProcessDownloadedSFTPFiles -SSC processing -" + e);
                                     ProcessErrorFile(objGPCustomerMessage);
                                 }
                             }
                         }
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("ProcessDownloadedFiles error-" + e);
             }
         }
       
         private void ProcessFtpFile(IList<GenericItemModel> genericModel, string transactionType, GPCustomerMessage objGPCustomerMessage)
         {
             GenericItemModel itemModel = null;
             string strEmail = "";            
             string fileName = objGPCustomerMessage.MapperName + "_" + Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName) + "_" + DateTime.Now.ToString("dd-MMM-yyyyHH.MM.ss") + Path.GetExtension(objGPCustomerMessage.BatchFileSourceAddress);
           
             if (genericModel[genericModel.Count - 1].ErrorMessage.Length != 0)
             {
                 string transType = objGPCustomerMessage.TransactionType.ToString();
                 string destinationFilePath = "";
                 using (var context = new GoodpackEDIEntities())
                 {
                     destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpNonProcessedFilePath").Select(i => i.Value).FirstOrDefault() + @"\" + fileName;
                     log.Info("ProcessFtpFile source - " + objGPCustomerMessage.BatchFileSourceAddress + " destination - " + destinationFilePath);
                     System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                     int transactionId = (from s in context.Ref_TransactionTypes
                                          where s.TransactionCode == transType
                                          select s.Id).FirstOrDefault();
                     var strMapperid = context.Trn_SubsiToTransactionFileMapper.Where(i => i.EDISubsiId == objGPCustomerMessage.SubsiId).Select(i => i.Id).FirstOrDefault();
                     strEmail = String.Join(",", context.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == context.Trn_SubsiToTransactionFileMapper.Where(j => j.EDISubsiId == objGPCustomerMessage.SubsiId).Select(j => j.Id).FirstOrDefault()).Select(i => i.EmailId).ToList());
                     Gen_TransactionValidationTracker validator = new Gen_TransactionValidationTracker();
                     validator.CreatedDateTime = DateTime.Now;
                     validator.EmailId = strEmail;
                     validator.FileContent = objGPCustomerMessage.Message;
                     //need to update the path
                     validator.FilePath = destinationFilePath;
                     validator.FileName = objGPCustomerMessage.BatchFileName;
                     validator.FileType = SOURCE_TYPES.FTP_SERVICE.ToString();
                     validator.MapperName = objGPCustomerMessage.MapperName;
                     validator.SubsiId = objGPCustomerMessage.SubsiId;
                     validator.ValidationErrorMessage = genericModel[genericModel.Count - 1].ErrorMessage;
                     validator.TransactionId = transactionId;
                     validator.isValidEmail = true;
                     context.Gen_TransactionValidationTracker.Add(validator);
                     context.SaveChanges();
                 }
                 Dictionary<string, string> placeholders = new Dictionary<string, string>();
                 placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                 placeholders.Add("$$Transaction$$", transType);
                 placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                 placeholders.Add("$$EmailID$$", strEmail);
                 placeholders.Add("$$Error$$", genericModel[genericModel.Count - 1].ErrorMessage);
 
 
                 List<Attachment> attachments1 = new List<Attachment>();
                 string strFile = "";
                 //  strFile = strProcessingFolder;
                 strFile = destinationFilePath;
                 Attachment attachment1 = new Attachment(strFile);
                 attachment1.Name = objGPCustomerMessage.BatchFileName;
                 attachments1.Add(attachment1);
                 log.Info("Attachment for email in path" + destinationFilePath);
                 string[] customerEmail = new string[] { strEmail };
                 GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_EMAIL_CUSTOMER", attachments1);
                 List<Attachment> attachments2 = new List<Attachment>();
                 Attachment attachment2 = new Attachment(strFile);
                 attachment2.Name = objGPCustomerMessage.BatchFileName;
                 attachments2.Add(attachment2);
                 SOTransactionManager soTrnManager = new SOTransactionManager();
                 GPTools.SendEmail(placeholders, soTrnManager.getAllRecipients(objGPCustomerMessage.MapperName), "NOTIFY_EMAIL_VALIDATION_ERROR", attachments2);
                 log.Info("Email send to customer" + strEmail);
             }
             else
             {
                 try
                 {
                     genericModel = new List<GenericItemModel>();
                     IList<GenericItemModel> model = null;
                     string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                     for (int i = 0; i < messageData.Length; i++)
                     {
                         itemModel = new GenericItemModel();
                         itemModel.Id = i;
                         itemModel.Name = messageData[i];
                         itemModel.DataValues = objGPCustomerMessage.Message;
                         //need to update the path
                         itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                         itemModel.FileName = objGPCustomerMessage.BatchFileName;
                         itemModel.Message = objGPCustomerMessage.Message;
                         itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                         itemModel.EmailId = strEmail;
                         genericModel.Add(itemModel);
                     }
                     if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SC)
                     {
 
                         SCTransactionManager scManager = new SCTransactionManager();
                         model = scManager.verifyData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, 3, false);
                     }
                     else if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SSC)
                     {
                         ISSCManagerNew sscManager = new SSCManagerNew();                        
                         model = sscManager.submitSSCData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, 3);
                     }
                     else
                     {
                         SOTransactionManager somanager = new SOTransactionManager();
                         string custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                         model = somanager.verifySOData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), custPOdate, genericModel, 3, false);
                     }
                     string destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpProcessedFilePath").Select(i => i.Value).FirstOrDefault() + "//" + fileName;
                     System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                 }
                 catch (Exception e)
                 {
                     log.Error("ProcessFtpFile -" + e);
                 }
             }
         }
 
         private void UploadFilesFromLocalToRemoteFTP()
         {
             try
             {
                 List<Trn_File_Transfer> ftpDownloadList = (from s in context.Trn_File_Transfer
                                                            where s.FileTransferType == "UPLOAD"
                                                            select s).ToList();
                 foreach (Trn_File_Transfer files in ftpDownloadList)
                 {
                     try
                     {
                         Gen_FTPAccount ftpAccntDetail = context.Gen_FTPAccount.Where(i => i.SftpId == files.SftpID).FirstOrDefault();
                         Ftp ftpClient = new Ftp(ftpAccntDetail);
                         ftpClient.OpenConnection();
                         if (ftpClient.IsConnected)
                         {
                             string fromLocation = (files.LocationFrom.LastIndexOf(@"/") == files.LocationFrom.Length - 1) ? files.LocationFrom : files.LocationFrom + "/";
                             bool hasDownloaded = ftpClient.UploadFile(fromLocation, files.LocationTo, files.IsDeleteFile);
                         }
                     }
                     catch(Exception e)
                     {
                         log.Error("UploadFilesFromLocalToRemoteFTP loop error-" + e);
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("UploadFilesFromLocalToRemoteFTP error-" + e);
             }
         }
 
         private void UploadFilesFromLocalToRemoteSFTP()
         {
             try
             {
                 List<Trn_SFTP_File_Transfer> ftpDownloadList = (from s in context.Trn_SFTP_File_Transfer
                                                                 where s.FileTransferType == "UPLOAD"
                                                                 select s).ToList();
                 foreach (Trn_SFTP_File_Transfer files in ftpDownloadList)
                 {
                     try
                     {
 
                         Gen_SFTPAccount ftpAccntDetail = context.Gen_SFTPAccount.Where(i => i.SftpId == files.SftpID).FirstOrDefault();
                         Sftp ftpClient = new Sftp(ftpAccntDetail);
                         ftpClient.OpenConnection();
                         if (ftpClient.IsConnected)
                         {
                             string fromLocation = (files.LocationFrom.LastIndexOf(@"/") == files.LocationFrom.Length - 1) ? files.LocationFrom : files.LocationFrom + "/";
                             bool hasDownloaded = ftpClient.UploadFile(fromLocation, files.LocationTo, files.IsDeleteFile);
                         }
                     }
                     catch (Exception e)
                     {
                         log.Error("UploadFilesFromLocalToRemoteSFTP loop error-" + e);
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("UploadFilesFromLocalToRemoteSFTP error-" + e);
             }
         }
 
         private bool ReadFileData(GPCustomerMessage objGPCustomerMessage, string path)
         {            
             bool isValid = true;
             FileInfo objFileInfo = new FileInfo(path);
             try
             {
                 if (objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName) || GPArchitecture.Cache.Config.getConfigList("SumitomoShipmentConfirmation").Contains(objGPCustomerMessage.MapperName))
                 {
                     objGPCustomerMessage.SubsiName = "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE";
                 }             
                 if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                 {
                     if ((objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder")|| GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName)) && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                     {
                         objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 21, 0);
                     }
                     else
                     {
                         objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 0, 0);
                     }
                 }
                 else
                 {
                     using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                     {
                         string message = objStreamReader.ReadToEnd();
                         objGPCustomerMessage.Message = message.Replace(",", "|");
                         objStreamReader.Close();
                         objStreamReader.Dispose();
                     }
                 }
             }
             catch (Exception exp)
             {
                 isValid = false;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                 objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                 log.Error("File is not in correct format", exp);
             }
             return isValid;
         }
 
         private void ProcessErrorFile(GPCustomerMessage objGPCustomerMessage)
         {
             string copyfileName = objGPCustomerMessage.MapperName + "_" + Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName) + "_" + DateTime.Now.ToString("dd-MMM-yyyyHH.MM.ss") + Path.GetExtension(objGPCustomerMessage.BatchFileSourceAddress);
             string destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpNonProcessedFilePath").Select(i => i.Value).FirstOrDefault() + "//" + copyfileName;
             System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
             string transType = objGPCustomerMessage.TransactionType.ToString();
             using (var context1 = new GoodpackEDIEntities())
             {
                 System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                 string strEmail = "";
                 int transactionId = (from s in context.Ref_TransactionTypes
                                      where s.TransactionCode == transType
                                      select s.Id).FirstOrDefault();
                 var strMapperid = context1.Trn_SubsiToTransactionFileMapper.Where(i => i.EDISubsiId == objGPCustomerMessage.SubsiId).Select(i => i.Id).FirstOrDefault();
                 strEmail = String.Join(",", context1.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == context1.Trn_SubsiToTransactionFileMapper.Where(j => j.EDISubsiId == objGPCustomerMessage.SubsiId).Select(j => j.Id).FirstOrDefault()).Select(i => i.EmailId).ToList());
                 Gen_TransactionValidationTracker validator = new Gen_TransactionValidationTracker();
                 validator.CreatedDateTime = DateTime.Now;
                 validator.EmailId = strEmail;
                 validator.FileContent = objGPCustomerMessage.Message;
                 //need to update the path
                 validator.FilePath = destinationFilePath;
                 validator.FileName = objGPCustomerMessage.BatchFileName;
                 validator.FileType = SOURCE_TYPES.FTP_SERVICE.ToString();
                 validator.MapperName = objGPCustomerMessage.MapperName;
                 validator.SubsiId = objGPCustomerMessage.SubsiId;
                 validator.ValidationErrorMessage = "Unable to process the file";
                 validator.TransactionId = transactionId;
                 validator.isValidEmail = true;
                 context.Gen_TransactionValidationTracker.Add(validator);
                 context.SaveChanges();
             }
         }
     }    
 }

 

