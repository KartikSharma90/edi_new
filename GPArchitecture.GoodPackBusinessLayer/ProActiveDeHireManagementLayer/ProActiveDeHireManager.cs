﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GPArchitecture.SAPService;
using Newtonsoft.Json.Linq;
using System.Text;
using log4net;
using GPArchitecture.Models.ViewModels;

namespace GPArchitecture.GoodPackBusinessLayer.ProActiveDeHireManagementLayer
{
    public class ProActiveDeHireManager:IProActiveDeHireManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ProActiveDeHireManager));
        public bool ProActiveDeHireUrlValidator(string consigneeCode, string startDate, string endDate, string binTypes,string key)
        {
            bool returnVal = false;
            DeHireUrlValidationService urlValidator = new DeHireUrlValidationService();
            Dictionary<string, string> returnValue= urlValidator.DeHireUrlValues(key);
            if (returnValue.Count()==0)
            {       returnVal = false;          }
            else if (returnValue["ConsigneeCode"] == consigneeCode && returnValue["StartDate"] == startDate && returnValue["EndDate"] == endDate && returnValue["BinTypes"] == binTypes)
            {       returnVal = true;           }
           return returnVal;
        }

        public ProActiveDehireReturnModel SubmitDehireData(JObject submitData, string Sdate, string Edate, string Consigneecode, string Bintype, string Key)
        {
            ProActiveDehireReturnModel returnModel = new ProActiveDehireReturnModel();
            try
            {
                StringBuilder sb = new StringBuilder();               
                var submitedDehireData = submitData["DehireData"].Value<JArray>();
                List<ProActiveDehireSubmitModel> listJsonData = new List<ProActiveDehireSubmitModel>();
                List<string> binType = Bintype.Split(',').ToList<string>();
                foreach (var test in submitedDehireData)
                {
                    ProActiveDehireSubmitModel jsonData = new ProActiveDehireSubmitModel();
                    jsonData.CollectionDate = Convert.ToDateTime(test["Collection Date"]).ToString("yyyy-MM-dd");
                    Dictionary<string, string> binData = new Dictionary<string, string>();
                    foreach (string binTypeValue in binType)
                    {
                        binData.Add(binTypeValue, Convert.ToString(test[binTypeValue.Trim()+" Qty"]));
                    }    
                    jsonData.BinQty = binData;
                    jsonData.Remark = Convert.ToString(test["Remark"]);
                    jsonData.Consigneecode = Consigneecode;
                    listJsonData.Add(jsonData);
                }
                List<ProactiveResponseData> returnData = new List<ProactiveResponseData>();
                foreach (ProActiveDehireSubmitModel dehireModel in listJsonData)
                {
                    foreach (string bin in binType)
                    {
                        if (!string.IsNullOrEmpty(dehireModel.BinQty[bin.Trim()]))
                        {
                            DeHireSAPService dehireService = new DeHireSAPService();
                            Dictionary<string, string> dehireReturn = dehireService.DeHireDataInsert(dehireModel.Consigneecode, dehireModel.CollectionDate, bin.Trim(), dehireModel.BinQty[bin.Trim()], dehireModel.Remark);
                            var referenceItem = dehireReturn["referenceItem"];
                            var status = dehireReturn["status"];
                            var errorMessage = dehireReturn["errorMessage"];
                            ProactiveResponseData data = new ProactiveResponseData();
                            if (status == "S")
                            {
                                data.BinType = bin.Trim();
                                data.CollectionDate = dehireModel.CollectionDate;
                                data.Quantity = dehireModel.BinQty[bin.Trim()];
                                data.Remark = dehireModel.Remark;
                                data.ResponseMessage = referenceItem;
                                data.Status = true;
                            }
                            else
                            {
                                data.Status = false;
                                data.BinType = bin.Trim();
                                data.CollectionDate = dehireModel.CollectionDate;
                                data.Quantity = dehireModel.BinQty[bin.Trim()];
                                data.Remark = dehireModel.Remark;
                                data.ResponseMessage = errorMessage;
                            }
                            returnData.Add(data);
                        }
                    }
                }
                returnModel.Message = "Success";
                returnModel.ResponseData = returnData;
                returnModel.Status = "true";                
            }
            catch (Exception e)
            {
                log.Error("SubmitDehireData error -" + e);
                throw e;
            }
            return returnModel;
        }
    }
}