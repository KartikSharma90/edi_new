﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodpackEDI.ViewModels;

namespace GPArchitecture.GoodPackBusinessLayer
{
    interface IEntityManager
    {
        IList<EntityModel> GetAllEntitiesforUser(string username);
        IList<EntityModel> GetAllCustomersforUser(string username);
        IList<EntityModel> GetAllPackerforCustomer(int customerCode);
        IList<EntityModel> GetAllEntityforSubsy(int subsyCode);
        string GetSubsiforEntity(int entityCode);
        IList<MapperModel> GetAllMapperforSelectedSubsy(int subsiCode, string username);
        IList<EntityModel> GetAllLookupNames();
        IList<ReasonModel> GetAllReasonsForEdit();
        IList<EntityModel> GetPrivilegeforEdit(string username);
        IList<ReasonModel> GetAllReasonsForCancel();
        bool GetisMapperASN(string mapperName);
        IList<EntityModel> GetCustomerPackerEntitiesforUser(string lookUpName, string userName);
    }
}
