﻿
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;
using log4net;
using LumenWorks.Framework.IO.Csv;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace GPArchitecture.GoodPackBusinessLayer.MappingManagementLayer
{
    public class HeaderReaderManager : IHeaderManager
    {
        DataTable dtAddHeader = new DataTable();
        private static readonly ILog log = LogManager.GetLogger(typeof(HeaderReaderManager));
        public DataTable readHeaders(string mapperFilePath, MappingModel mappingModel)
        {
            DataTable dtheaders;
            string fileType; 
            using(var context=new GoodpackEDIEntities())
            {
                fileType = (from s in context.Ref_MessageFormat
                            where s.Id == mappingModel.FileTypeId
                            select s.MessageFormat).FirstOrDefault();           
            
            }
            try
            {
                switch (fileType)
                {
                    case ConstantUtilities.SemiColonDelimited:
                        dtheaders = GetCustomerRequestHeaderFields(mapperFilePath, ';');
                        break;
                    case ConstantUtilities.Excel:
                        dtheaders = GetExcelHeaders(mapperFilePath,mappingModel);
                        break;
                    case ConstantUtilities.CSV:
                        dtheaders = GetCSVHeaderFields(mapperFilePath, ',',mappingModel);
                        break;
                    default:
                        dtheaders = GetCustomerRequestHeaderFields(mapperFilePath, ',');
                        break;
                }

                dtAddHeader.Columns.Add("SRNO");
                dtAddHeader.Columns.Add("HeaderName");
                for (int i = 0; i < dtheaders.Columns.Count; i++)
                {
                    DataRow dr = dtAddHeader.NewRow();
                    dr["SRNO"] = i + 1;
                    //Hard code header names for empty columns
                    if (dtheaders.Columns[i].Caption.StartsWith("Column"))
                        dr["HeaderName"] = "Column" + "" + (i + 1).ToString();
                    else
                        dr["HeaderName"] = dtheaders.Columns[i].Caption;
                    dtAddHeader.Rows.Add(dr);
                    //if (txtHeader.Text == string.Empty)
                    //    txtHeader.Text += dtheaders.Columns[i].Caption;
                    //else
                    //{
                    //    if (fileType == ConstantUtilities.SemiColonDelimited)
                    //        txtHeader.Text += ConstantUtilities.Semi_Colon + dtheaders.Columns[i].Caption;
                    //    else
                    //        txtHeader.Text += ConstantUtilities.Comma + dtheaders.Columns[i].Caption;
                    //}

                }
                //if (mappingModel..IsHeaderPresent == 1)
                //{
                //    grdMappingWithHeader.DataSource = dtAddHeader;
                //    grdMappingWithHeader.DataBind();
                //    grdMappingWithHeader.Visible = true;
                //}
                //else
                //{
                //    grdMappingNoHeader.DataSource = dtAddHeader;
                //    grdMappingNoHeader.DataBind();
                //    grdMappingNoHeader.Visible = true;
                //}
                //Session[CommonConstants.Session_Mapping] = dtAddHeader;
                return dtAddHeader;
            }
            catch (Exception ex)
            {
                log.Error(" readHeaders in HeaderReaderManager.mapperFilePath:" + mapperFilePath + ", mappingModel:" + mappingModel + ". Api Exception : Message- " + ex.Message);
                throw ex;
            }
        }



        //New method added to parse both the semi colon delimited and csv files during mapping configuration
        public static DataTable GetCustomerRequestHeaderFields(string path, char delimiter)
        {
            DataTable dtParsedRows = new DataTable();
            try
            {
                string text;
                using (TextReader reader = File.OpenText(path))
                {
                    text = reader.ReadToEnd();
                }

                // Initialize csv reader object
                CsvReader csvReader = new CsvReader(new StringReader(text), false, delimiter);

                // Read first line
                csvReader.ReadNextRecord();

                // Iterate through each field in first line in customer message
                for (int ctr = 0; ctr < csvReader.FieldCount; ctr++)
                {
                    int ctradd = 0;
                    if (dtParsedRows.Columns.Contains(csvReader[ctr]))
                    {
                        string cellName = csvReader[ctr] + ctradd.ToString();
                        dtParsedRows.Columns.Add(cellName);
                    }
                    else
                    {
                        dtParsedRows.Columns.Add(csvReader[ctr]);
                    }
                }//-- end for loop
            }
            catch (System.Exception ex)
            {
                log.Error(" GetCustomerRequestHeaderFields in HeaderReaderManager.path:" + path + ", delimiter:" + delimiter + ". Api Exception : Message- " + ex.Message);
            }

            return dtParsedRows;

        }



        //New method added to parse  csv files during mapping configuration done by vivek
        public DataTable GetCSVHeaderFields(string path, char delimiter,MappingModel mappingModel)
        {
            DataTable dtParsedRows = new DataTable();
            try
            {
                string text;
                using (TextReader reader = File.OpenText(path))
                {
                    text = reader.ReadToEnd();
                }

                // Initialize csv reader object
                CsvReader csvReader = new CsvReader(new StringReader(text), false, delimiter);

                int rowNumber;
                int columnNumber;
                if (mappingModel.IsCustomizedInput)
                {
                    rowNumber = mappingModel.RowNumber;
                    columnNumber = mappingModel.ColumnNumber;
                    columnNumber = columnNumber - 1;

                    for (int i = 0; i < rowNumber; i++)
                    {
                        csvReader.ReadNextRecord();
                    }

                }
                else
                {
                    rowNumber = 0;
                    columnNumber = 0;

                    csvReader.ReadNextRecord();
                }

                // Iterate through each field in first line in customer message
                for (int ctr = columnNumber; ctr < csvReader.FieldCount; ctr++)
                {
                    int ctradd = 0;
                    if (dtParsedRows.Columns.Contains(csvReader[ctr]))
                    {
                        string cellName = csvReader[ctr] + ctradd.ToString();
                        dtParsedRows.Columns.Add(cellName);
                    }
                    else
                    {
                        dtParsedRows.Columns.Add(csvReader[ctr]);
                    }
                }//-- end for loop
            }
            catch (System.Exception ex)
            {
                log.Error(" GetCSVHeaderFields in HeaderReaderManager.path:" + path + ", delimiter:" + delimiter + "mappingmodel:" + mappingModel + ".  Exception : Message- " + ex.Message);
            }

            return dtParsedRows;

        }


        //New method added to parse the excel file during mapping configuration //the methos was static before;changed by vivek for mapper customization
        public DataTable GetExcelHeaders(string path,MappingModel mappingModel)
        {
            //  objILog.Debug("Enter GetExcelHeaders");
            DataTable dtParsedRows = new DataTable();

            try
            {
                //choose the class depending upon the type of Excel file
                ISheet sheet;
                if (path.Substring(path.LastIndexOf('.')) == ".xls")
                {
                    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        HSSFWorkbook workbook = new HSSFWorkbook(file);
                        sheet = workbook.GetSheetAt(0);
                    }
                }
                else
                {
                    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        XSSFWorkbook workbook = new XSSFWorkbook(file);
                        sheet = workbook.GetSheetAt(0);
                    }
                }

                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

                // for mapper customization by vivek
                IRow hdrrow;
                int columnNumber;
                int rowNumber;

                if (mappingModel.IsCustomizedInput)
                {
                    rowNumber = mappingModel.RowNumber;
                    columnNumber = mappingModel.ColumnNumber;

                    hdrrow = sheet.GetRow(rowNumber - 1);
                    columnNumber = columnNumber - 1;
                }

                else
                {
                    //fetch only the header row
                    hdrrow = sheet.GetRow(0);
                    columnNumber = 0;
                }


                //add the headers to the columns of the datatable
                for (int i = columnNumber; i < hdrrow.LastCellNum; i++)
                {
                    ICell cell = hdrrow.GetCell(i);
                    string cellName = cell.ToString();
                    int ctr = 0;

                    // If there are duplicate header values, then append number
                    while (dtParsedRows.Columns.Contains(cellName))
                    {
                        ctr++;
                        cellName = cell.ToString() + ctr.ToString();
                    }
                    if (cellName != "" || cellName.Length != 0)
                    {
                        dtParsedRows.Columns.Add(cellName);
                    }
                }
            }
            catch (System.Exception ex)
            {
                log.Error("An error was encountered with the GetExcelHeaders in HeaderReaderManager: " + ex.InnerException + " " + ex.Source + ex.StackTrace + ".  Exception : Message- " + ex.Message);              
            }           
            return dtParsedRows;
        }
        
        
    }
}