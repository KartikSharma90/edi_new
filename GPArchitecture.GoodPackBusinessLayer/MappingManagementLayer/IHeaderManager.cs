﻿using GPArchitecture.Models;
using System.Data;

namespace GPArchitecture.GoodPackBusinessLayer.MappingManagementLayer
{
    public interface IHeaderManager
    {
        DataTable readHeaders(string mapperFilePath, MappingModel mappingModel);
    }
}