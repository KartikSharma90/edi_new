using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Web;
 using System.IO;
 using GPArchitecture.DataLayer.Models;
 using log4net;
 using GPArchitecture.CommonTypes;
 using GPArchitecture.Adapters.FTP;
 using System.Transactions;
 using GPArchitecture.Models;
 using GPArchitecture.EnumsAndConstants;
 using GPArchitecture.Parsers;
 using GPArchitecture.Adapters.SFTP;
 
 namespace GPArchitecture.GoodPackBusinessLayer.MappingManagementLayer
 {
     public class MapperManager : IMapperManager
     {
         private Trn_MappingSubsi mappingSubsi;
         private Trn_MappingConfiguration mappingConfig;
         private static readonly ILog log = LogManager.GetLogger(typeof(MapperManager));
 
         public IList<GenericItemModel> TransactionTypes()
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_TransactionTypes
                         where ((s.IsEnabled == true) && (s.IsDropdownDisplay == true))
                         orderby s.TransactionName
                         select new GenericItemModel
                         {
                             Id = s.Id,
                             Name = s.TransactionName
                         }).ToList();
 
             }
         }
 
         public IList<GenericItemModel> FileTypes()
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_MessageFormat
                         where ((s.IsEnabled == true) && (s.IsDropdownDisplay == true))
                         orderby s.MessageFormat
                         select new GenericItemModel
                         {
                             Id = s.Id,
                             Name = s.MessageFormat
                         }).ToList();
 
             }
         }
 
         public IList<SAPMappingModel> SAPMessageFields(int transactionId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_SAPMessageFields
                         orderby s.FieldSequence
                         where (s.TransactionId == transactionId)
                         select new SAPMappingModel
                         {
                             Id = s.Id,
                             FieldName = s.SAPFieldName,
                             Note = s.Note,
                             InputType = s.InputType,
                             IsDisplay = s.IsDisplay,
                             IsMandatory = s.IsMandatory,
                             FieldSequence = s.FieldSequence
                         }).ToList();
             }
         }
 
         public ResponseModel SaveHeaderFieldsForSSCRegistration(MappingModel mappingModel, string username, string mappingFilePath)
         {
             ResponseModel responseModel = new ResponseModel();
             try
             {
 
                 using (var context = new GoodpackEDIEntities())
                 {
                     Trn_SubsiToTransactionFileMapper uploadfileMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                          where s.Filename == mappingModel.FileName
                                                                          select s).FirstOrDefault();
                     if (uploadfileMapper != null)
                     {
                         responseModel.Success = false;
                         responseModel.Message = "File name already exists.Please input another file name";
                         return responseModel;
                     }
                     else
                     {
 
                         int userId = (from s in context.Gen_User
                                       where s.Username == username
                                       select s.Id).FirstOrDefault();
                         //string filePath = MoveMapperFile(username, mappingFilePath);
                         Trn_SubsiToTransactionFileMapper fileMapper = new Trn_SubsiToTransactionFileMapper();
                         fileMapper.EDISubsiId = mappingModel.SubsiId;
                         fileMapper.IsEnabled = true;
                         fileMapper.IsHeaderPresent = true;
                         fileMapper.LastUpdatedBy = userId;
                         fileMapper.LastUpdatedDate = DateTime.Now;
                         fileMapper.MessageFormatId = mappingModel.FileTypeId;
                         fileMapper.MinFieldValueCount = Convert.ToInt32(mappingModel.FieldCountValue);
                         fileMapper.Service = ConstantUtilities.Service;
                         fileMapper.Filename = mappingModel.FileName;
                         fileMapper.FileServerPath = "";
                         fileMapper.TransactionCodeId = mappingModel.TransactionId;
                         context.Trn_SubsiToTransactionFileMapper.Add(fileMapper);
                         context.SaveChanges();
 
                         Trn_SubsiToTransactionFileMapper headerMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                          where (s.Filename == mappingModel.FileName)
                                                                          select s).FirstOrDefault();
 
                         if (mappingModel.EnableEmail)
                         {
                             string[] emailIds = mappingModel.EmailIds.Split(',');
                             foreach (var item in emailIds)
                             {
                                 Trn_MapperNameToEmailIdMapper emailIdMapper = new Trn_MapperNameToEmailIdMapper();
                                 emailIdMapper.EmailId = item;
                                 emailIdMapper.MapperId = fileMapper.Id;
                                 context.Trn_MapperNameToEmailIdMapper.Add(emailIdMapper);
                                 context.SaveChanges();
                             }
                         }
 
                         // Inserting values to Trn_MapperFieldSpecs table
                         for (int i = 0; i < mappingModel.FileHeaders.Count; i++)
                         {
 
                             Trn_MapperFieldSpecs headerFields = new Trn_MapperFieldSpecs();
                             //Input file header
                             headerFields.FieldName = mappingModel.FileHeaders[i].HeaderName;
                             headerFields.FieldSequence = i + 1;
                             headerFields.Length = 0;
                             headerFields.SubsiToTransactionMapperId = headerMapper.Id;
 
                             context.Trn_MapperFieldSpecs.Add(headerFields);
                             context.SaveChanges();
 
                         }
 
                         // Inserting values to Trn_MappingSubsi table table
 
                         mappingSubsi = new Trn_MappingSubsi();
                         mappingSubsi.IsEnabled = true;
                         mappingSubsi.LastUpdatedBy = userId;
                         mappingSubsi.LastUpdatedDate = DateTime.Now;
                         mappingSubsi.SubsiToTransactionFileMapperId = headerMapper.Id;
                         mappingSubsi.SapMessageFieldId = mappingModel.TransactionId;
                         context.Trn_MappingSubsi.Add(mappingSubsi);
                         context.SaveChanges();
                         Trn_MappingSubsi subsi = (from s in context.Trn_MappingSubsi
                                                   where (s.SubsiToTransactionFileMapperId == headerMapper.Id) && (s.SapMessageFieldId == mappingModel.TransactionId) && (s.LastUpdatedBy == userId)
                                                   select s).FirstOrDefault();
                         // Inserting values to Trn_MappingConfiguration table
 
                         for (int item = 0; item < mappingModel.SAPFieldHeaders.Count; item++)
                         {
 
                             mappingConfig = new Trn_MappingConfiguration();
                             mappingConfig.MappingSubsiId = subsi.Id;
                             mappingConfig.SourceFieldSequence = (from s in mappingModel.MappedFileModel
                                                                  where s.HeaderName == mappingModel.SAPFieldHeaders[item].FieldName
                                                                  select s.FieldSequence).FirstOrDefault();
                             BindControlsValues(mappingModel, item);
                             context.Trn_MappingConfiguration.Add(mappingConfig);
                             context.SaveChanges();
                             // context.SaveChanges();
 
                         }
                         responseModel.Success = true;
                         responseModel.Message = "Header mapping done successfully ";
                         return responseModel;
 
                     }
                 }
             }
             catch (Exception ex)
             {
                 log.Error(" SaveHeaderFields in MapperManager .username:" + username + ", mappingFilePath:" + mappingFilePath + ",mappingModel:" + mappingModel + ". Api Exception : Message- " + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Unable to process your request.Please try again";
                 return responseModel;
             }
         }
 
         public IList<ViewMapperDataModel> GetAllMappingDetails(string username)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 Gen_User user = (from s in context.Gen_User
                                  where s.Username == username
                                  select s).FirstOrDefault();
 
                 string role = (from s in context.Ref_UserRoles
                                where s.Id == user.RoleId
                                select s.UserRole).FirstOrDefault();
 
                 List<ViewMapperDataModel> ListViewMapperModel = new List<ViewMapperDataModel>();
 
                 if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                 {
                     List<Trn_SubsiToTransactionFileMapper> ViewMapperDetails = (context.Trn_SubsiToTransactionFileMapper.Where(i => i.LastUpdatedBy == user.Id && i.IsEnabled == true).OrderByDescending(c => c.Id).ToList());
                     for (int i = 0; i < ViewMapperDetails.Count; i++)
                     {
                         ViewMapperDataModel ViewMapperModel = new ViewMapperDataModel();
                         ViewMapperModel.UniqueId = ViewMapperDetails[i].Id;
                         ViewMapperModel.AddedBy = ViewMapperDetails[i].Gen_User.Username;
                         ViewMapperModel.DateAdded = ViewMapperDetails[i].LastUpdatedDate;
                         ViewMapperModel.FileType = ViewMapperDetails[i].Ref_MessageFormat.MessageFormat;
                         ViewMapperModel.MapperName = ViewMapperDetails[i].Filename;
                         ViewMapperModel.SubsyName = ViewMapperDetails[i].Gen_EDISubsies.SubsiName;
                         ViewMapperModel.TransactionType = ViewMapperDetails[i].Ref_TransactionTypes.TransactionCode;
                         ListViewMapperModel.Add(ViewMapperModel);
                     }
                     return ListViewMapperModel;
                 }
                 else
                 {
                     List<Trn_SubsiToTransactionFileMapper> ViewMapperDetails = (context.Trn_SubsiToTransactionFileMapper.Where(i => i.IsEnabled == true).OrderByDescending(c => c.Id).ToList());
                     for (int i = 0; i < ViewMapperDetails.Count; i++)
                     {
                         ViewMapperDataModel ViewMapperModel = new ViewMapperDataModel();
                         ViewMapperModel.UniqueId = ViewMapperDetails[i].Id;
                         ViewMapperModel.AddedBy = ViewMapperDetails[i].Gen_User.Username;
                         ViewMapperModel.DateAdded = ViewMapperDetails[i].LastUpdatedDate;
                         ViewMapperModel.FileType = ViewMapperDetails[i].Ref_MessageFormat.MessageFormat;
                         ViewMapperModel.MapperName = ViewMapperDetails[i].Filename;
                         ViewMapperModel.SubsyName = ViewMapperDetails[i].Gen_EDISubsies.SubsiName;
                         ViewMapperModel.TransactionType = ViewMapperDetails[i].Ref_TransactionTypes.TransactionCode;
                         ListViewMapperModel.Add(ViewMapperModel);
                     }
                     return ListViewMapperModel;
                 }
             }
 
         }
 
         public IList<ViewSelectedMapperDataModel> GetSelectedMappingDetails(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
 
                 List<ViewSelectedMapperDataModel> ListViewMapperModel = new List<ViewSelectedMapperDataModel>();
 
                 int MapperFieldSpecId = context.Trn_MappingSubsi.Where(i => i.SubsiToTransactionFileMapperId == susbsyToTransactionMapperId).FirstOrDefault().Id;
                 int TransactionId = context.Trn_MappingSubsi.Where(i => i.Id == MapperFieldSpecId).FirstOrDefault().SapMessageFieldId;
                 List<Ref_SAPMessageFields> SAPMessageFields = context.Ref_SAPMessageFields.Where(i => i.TransactionId == TransactionId).ToList();
 
 
                 for (int i = 0; i < SAPMessageFields.Count; i++)
                 {
                     ViewSelectedMapperDataModel ViewMapperModel = new ViewSelectedMapperDataModel();
                     int FieldSequence = SAPMessageFields[i].FieldSequence;
 
 
                     switch (SAPMessageFields[i].InputType)
                     {
                         case "FIXED":
                             ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                             ViewMapperModel.Notes = SAPMessageFields[i].Note;
                             ViewMapperModel.InputField = SAPMessageFields[i].DefaultValue;
                             ListViewMapperModel.Add(ViewMapperModel);
                             break;
 
                         case "USER_FIXED":
                             ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                             ViewMapperModel.Notes = SAPMessageFields[i].Note;
                             ViewMapperModel.InputField = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                             ListViewMapperModel.Add(ViewMapperModel);
                             break;
 
                         case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                             ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                             ViewMapperModel.Notes = SAPMessageFields[i].Note;
                             string inputfield = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                             if (inputfield == null || inputfield == "")
                             {
                                 int? SourceFieldSequenceId = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                                 if (SourceFieldSequenceId == null || SourceFieldSequenceId == 0)
                                 {
                                     ViewMapperModel.InputField = null;
                                 }
                                 else
                                 {
                                     ViewMapperModel.InputField = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId).FirstOrDefault().FieldName;
                                 }
                             }
                             else
                             {
                                 ViewMapperModel.InputField = inputfield;
                             }
                             ListViewMapperModel.Add(ViewMapperModel);
                             break;
 
 
                         case "OPERATION_ON_SOURCE_FIELDS2":
 
                             ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                             ViewMapperModel.Notes = SAPMessageFields[i].Note;
 
                             int? SourceFieldSequenceId2 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSeq2;
                             if (SourceFieldSequenceId2 == null || SourceFieldSequenceId2 == 0)
                             {
                                 ViewMapperModel.InputField = null;
                             }
                             else
                             {
                                 ViewMapperModel.InputField = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId2).FirstOrDefault().FieldName;
                             }
                             ListViewMapperModel.Add(ViewMapperModel);
 
                             break;
 
 
                         default:
                             ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                             ViewMapperModel.Notes = SAPMessageFields[i].Note;
 
                             int? SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                             if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                             {
                                 ViewMapperModel.InputField = null;
                             }
                             else
                             {
                                 ViewMapperModel.InputField = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
                             }
                             ListViewMapperModel.Add(ViewMapperModel);
 
                             break;
 
                     }
 
                 }
 
                 return ListViewMapperModel;
 
             }
 
         }
 
         public ResponseModel EmailIdMapper(string emailIds, string transactionId)
         {
             ResponseModel responseModel = new ResponseModel();
             using (var context = new GoodpackEDIEntities())
             {
                 int Id = Convert.ToInt32(transactionId);
                 string[] emailIdValues = emailIds.Split(',');
                 foreach (var item in emailIdValues)
                 {
 
                     Trn_MapperNameToEmailIdMapper emailIdMapper = (from s in context.Trn_MapperNameToEmailIdMapper
                                                                    where ((s.EmailId == item) && (s.TransactionId == Id))
                                                                    select s).FirstOrDefault();
                     if (emailIdMapper != null)
                     {
                         responseModel.Success = false;
                         responseModel.Message = "Email id " + item + " has mapping already.Please enter another email id";
                         return responseModel;
                     }
                 }
 
                 responseModel.Success = true;
                 return responseModel;
             }
         }
 
         public IList<EditHeaderModel> GetHeadersForEdit(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 List<Trn_MapperFieldSpecs> FieldSpecs = context.Trn_MapperFieldSpecs.Where(i => i.SubsiToTransactionMapperId == susbsyToTransactionMapperId).ToList();
                 return (from f in FieldSpecs
                         select new EditHeaderModel
                         {
                             HeaderName = f.FieldName,
                             SRNO = f.FieldSequence
                         }).ToList();
             }
 
         }
 
         public IList<MapperEmailModel> GetEmailIdsForEdit(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
 
                 List<Trn_MapperNameToEmailIdMapper> MapperEmailIds = context.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == susbsyToTransactionMapperId).ToList();
                 return (from m in MapperEmailIds
                         select new MapperEmailModel
                         {
                             Emailid = m.EmailId
                         }).ToList();
             }
 
 
         }
 
         public IList<EditMapperHeaderModel> GetFilDetailsForEdit(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 List<EditMapperHeaderModel> ListMapperModel = new List<EditMapperHeaderModel>();
                 EditMapperHeaderModel MapperModel = new EditMapperHeaderModel();
                 var listData = context.Trn_SubsiToTransactionFileMapper.Where(j => j.Id == susbsyToTransactionMapperId).FirstOrDefault();
                 MapperModel.SubsiId = listData.EDISubsiId;
                 MapperModel.FileTypeId = listData.MessageFormatId;
                 MapperModel.FieldCountValue = listData.MinFieldValueCount;
                 MapperModel.TransactionId = listData.TransactionCodeId;
                 MapperModel.FileName = listData.Filename;
                 MapperModel.FilePath = listData.FileServerPath;
                 MapperModel.MapperId = susbsyToTransactionMapperId;
                 int count = context.Ref_SAPMessageFields.Where(i => i.InputType == "DATE" && i.TransactionId == MapperModel.TransactionId).Count();
                 for (int i = 0; i < count; i++)
                 {
                     bool isdisplay = context.Ref_SAPMessageFields.Where(j => j.InputType == "DATE" && j.TransactionId == MapperModel.TransactionId && j.IsAllowNullValue == false).FirstOrDefault().IsDisplay;
                     bool ismandatory = context.Ref_SAPMessageFields.Where(j => j.InputType == "DATE" && j.TransactionId == MapperModel.TransactionId && j.IsAllowNullValue == false).FirstOrDefault().IsMandatory;
                     if (isdisplay == true || ismandatory == true)
                     {
                         int sapid = context.Ref_SAPMessageFields.Where(j => j.InputType == "DATE" && j.TransactionId == MapperModel.TransactionId && j.IsDisplay == true && j.IsAllowNullValue == false).FirstOrDefault().FieldSequence;
                         int MapperFieldSpecId = context.Trn_MappingSubsi.Where(j => j.SubsiToTransactionFileMapperId == susbsyToTransactionMapperId).FirstOrDefault().Id;
                         MapperModel.DateFormat = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == sapid).FirstOrDefault().InputDateFormat;
                     }
                     else
                     {
                         MapperModel.DateFormat = null;
 
                     }
                 }
                 MapperModel.FtpIsEnabledForMapper = listData.IsFTPEnabled;
                 if (listData.IsFTPEnabled)
                 {
                     if (listData.FTPType == 1)
                     {
                         var listFtp = context.Gen_FTPAccount.Where(i => i.SftpId == context.Trn_File_Transfer.Where(j => j.MapperName == MapperModel.FileName).Select(j => j.SftpID).FirstOrDefault()).FirstOrDefault();
                         var ftpDetails = context.Trn_File_Transfer.Where(j => j.MapperName == MapperModel.FileName).FirstOrDefault();
                         MapperModel.FtpPort = Convert.ToInt32(listFtp.Port);
                         MapperModel.IsFtpEnable = listFtp.IsEnabled;
                         MapperModel.FtpType = listFtp.Name;
                         MapperModel.FtpHost = listFtp.Host;
                         MapperModel.FtpLocation = ftpDetails.LocationFrom;
                         MapperModel.FtpUser = listFtp.User;
                         MapperModel.FtpPassword = listFtp.Password;
                         MapperModel.FtpEncryptiontype = listFtp.EncryptionType;
                         MapperModel.FtpSslHostCertificateFingerprint = listFtp.SslHostCertificateFingerprint;
                     }else
                     {
                         var listFtp = context.Gen_SFTPAccount.Where(i => i.SftpId == context.Trn_File_Transfer.Where(j => j.MapperName == MapperModel.FileName).Select(j => j.SftpID).FirstOrDefault()).FirstOrDefault();
                         var ftpDetails = context.Trn_SFTP_File_Transfer.Where(j => j.MapperName == MapperModel.FileName).FirstOrDefault();
                         MapperModel.FtpPort = Convert.ToInt32(listFtp.Port);
                         MapperModel.IsFtpEnable = listFtp.IsEnabled;
                         MapperModel.FtpType = listFtp.Name;
                         MapperModel.FtpHost = listFtp.Host;
                         MapperModel.FtpLocation = ftpDetails.LocationFrom;
                         MapperModel.FtpUser = listFtp.User;
                         MapperModel.FtpPassword = listFtp.Password;
                         MapperModel.FtpSslHostCertificateFingerprint = listFtp.SshHostKeyFingerPrint;                     
                     }
                 }
                 ListMapperModel.Add(MapperModel);
                 return ListMapperModel;
             }
 
         }
 
         public IList<EditMapperHeaderModel> GetMappedFileDetailsForEdit(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 List<EditMapperHeaderModel> ListMapperModel = new List<EditMapperHeaderModel>();
                 int MapperFieldSpecId = context.Trn_MappingSubsi.Where(i => i.SubsiToTransactionFileMapperId == susbsyToTransactionMapperId).FirstOrDefault().Id;
                 int TransactionId = context.Trn_MappingSubsi.Where(i => i.Id == MapperFieldSpecId).FirstOrDefault().SapMessageFieldId;
                 List<Ref_SAPMessageFields> SAPMessageFields = context.Ref_SAPMessageFields.Where(i => i.TransactionId == TransactionId && i.IsDisplay == true).ToList();
                 List<Trn_MappingConfiguration> MapConfig = context.Trn_MappingConfiguration.Where(i => i.MappingSubsiId == MapperFieldSpecId).ToList();
 
                 for (int i = 0; i < SAPMessageFields.Count; i++)
                 {
                     EditMapperHeaderModel MapperModel = new EditMapperHeaderModel();
                     int FieldSequence = SAPMessageFields[i].FieldSequence;
                     switch (SAPMessageFields[i].InputType)
                     {
                         case "FIXED":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             MapperModel.HeaderName = SAPMessageFields[i].DefaultValue;
                             MapperModel.InputType = SAPMessageFields[i].InputType;
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
                             MapperModel.SRNO = Convert.ToInt32(MapConfig[i].SourceFieldSequence);
                             ListMapperModel.Add(MapperModel);
                             break;
 
                         case "USER_FIXED":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             MapperModel.HeaderName = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                             MapperModel.InputType = SAPMessageFields[i].InputType;
                             MapperModel.HeaderName = MapConfig[i].FixedValue;
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
                             MapperModel.SRNO = Convert.ToInt32(MapConfig[i].SourceFieldSequence);
                             ListMapperModel.Add(MapperModel);
                             break;
                         case "OPERATION_ON_SOURCE_FIELD":
                         case "MAP_FROM_SOURCE":
                         case "MAP_FROM_SOURCE_WITH_SUBSTRING":
                         //case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
                         //case "OPERATION_ON_SOURCE_FIELDS2":
                         case "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             //int? SourceFieldSequenceId = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                             int? SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).Select(X=>X.SourceFieldSequence).FirstOrDefault();
                             if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                             {
                                 MapperModel.HeaderName = null;
                             }
                             else
                             {
                                 MapperModel.HeaderName = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
 
                                 //  context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId).FirstOrDefault().FieldName;
                                 MapperModel.SRNO = Convert.ToInt32(SourceFieldSequenceId1);
                             }
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
                             MapperModel.InputType = SAPMessageFields[i].InputType;
 
                             ListMapperModel.Add(MapperModel);
                             break;
                         case "OPERATION_ON_SOURCE_FIELDS2":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             //int? SourceFieldSequenceId = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                             int? SourceFieldSequenceId2 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSeq2;
                             if (SourceFieldSequenceId2 == null || SourceFieldSequenceId2 == 0)
                             {
                                 MapperModel.HeaderName = null;
                             }
                             else
                             {
                                 MapperModel.HeaderName = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId2).FirstOrDefault().FieldName;
 
                                 //  context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId).FirstOrDefault().FieldName;
                                 MapperModel.SRNO = Convert.ToInt32(SourceFieldSequenceId2);
                             }
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
                             MapperModel.InputType = SAPMessageFields[i].InputType;
 
                             ListMapperModel.Add(MapperModel);
                             break;
 
                         case "DATE":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                             if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                             {
                                 MapperModel.HeaderName = null;
 
                             }
                             else
                             {
                                 MapperModel.DateFormat = MapConfig[i].InputDateFormat;
                                 MapperModel.HeaderName = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
 
                                 MapperModel.SRNO = Convert.ToInt32(SourceFieldSequenceId1);
                             }
                             MapperModel.InputType = SAPMessageFields[i].InputType;
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
 
                             ListMapperModel.Add(MapperModel);
                             break;
                         case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
                         case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             string inputfield = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                             if (inputfield == null || inputfield == "")
                             {
                                 SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                                 if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                                 {
                                     MapperModel.HeaderName = null;
                                 }
                                 else
                                 {
                                     MapperModel.HeaderName = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
                                     // MapperModel.HeaderName = MapConfig[i].FixedValue;
                                     MapperModel.SRNO = Convert.ToInt32(SourceFieldSequenceId1);
                                 }
                             }
                             else
                             {
                                 MapperModel.HeaderName = inputfield;
                             }
                             MapperModel.InputType = SAPMessageFields[i].InputType;
                             MapperModel.FieldSequence = MapConfig[i].SapFieldSeq;
                             ListMapperModel.Add(MapperModel);
                             break;
                         default:
                             MapperModel.FieldName = SAPMessageFields[i].SAPFieldName;
                             MapperModel.Id = SAPMessageFields[i].Id;
                             // MapperModel.Note = SAPMessageFields[i].Note;
                             SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                             if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                             {
                                 MapperModel.HeaderName = null;
                             }
                             else
                             {
                                 MapperModel.HeaderName = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
                                 MapperModel.SRNO = Convert.ToInt32(SourceFieldSequenceId1);
                             }
                             MapperModel.InputType = SAPMessageFields[i].InputType;
                             ListMapperModel.Add(MapperModel);
 
                             break;
                     }
                 }
 
                 return ListMapperModel;
             }
 
         }
 
         public ResponseModel EditHeaderFields(MappingModel mappingModel, string username)
         {
             ResponseModel responseModel = new ResponseModel();
             try
             {
 
                 using (var context = new GoodpackEDIEntities())
                 {
 
                     if (mappingModel.EmailIds != null)
                     {
                         string[] emailIds = mappingModel.EmailIds.Split(',').Select(sValue => sValue.Trim()).ToArray();
                         foreach (var item in emailIds)
                         {
 
                             Trn_MapperNameToEmailIdMapper emailIdMapper = (from s in context.Trn_MapperNameToEmailIdMapper
                                                                            where (s.EmailId == item) && (s.MapperId != mappingModel.MapperId) && (s.TransactionId == mappingModel.TransactionId)
                                                                            select s).FirstOrDefault();
                             if (emailIdMapper != null)
                             {
                                 responseModel.Success = false;
                                 responseModel.Message = "Email id " + item + " has mapping already.Please enter another email id";
                                 return responseModel;
                             }
                         }
                     }
 
 
                     int userId = (from s in context.Gen_User
                                   where s.Username == username
                                   select s.Id).FirstOrDefault();
                     Trn_SubsiToTransactionFileMapper fileMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                    where s.Filename == mappingModel.FileName
                                                                    select s).FirstOrDefault();
                     fileMapper.LastUpdatedBy = userId;
                     fileMapper.LastUpdatedDate = DateTime.Now;
                     context.SaveChanges();
 
                     Trn_SubsiToTransactionFileMapper headerMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                      where (s.Filename == mappingModel.FileName)
                                                                      select s).FirstOrDefault();
 
                     mappingSubsi = new Trn_MappingSubsi();
                     mappingSubsi.LastUpdatedBy = userId;
                     mappingSubsi.LastUpdatedDate = DateTime.Now;
                     context.SaveChanges();
                     Trn_MappingSubsi subsi = (from s in context.Trn_MappingSubsi
                                               where (s.SubsiToTransactionFileMapperId == headerMapper.Id) && (s.SapMessageFieldId == mappingModel.TransactionId) && (s.LastUpdatedBy == userId)
                                               select s).FirstOrDefault();
                     // Updating values of Trn_MappingConfiguration table
 
                     for (int item = 0; item < mappingModel.SAPFieldHeaders.Count; item++)
                     {
                         List<Trn_MappingConfiguration> MapConfig = context.Trn_MappingConfiguration.Where(i => i.MappingSubsiId == subsi.Id).ToList();
                         BindControlsValuesForEdit(MapConfig, mappingModel, item);
                         context.SaveChanges();
                     }
                     if (mappingModel.EmailIds != null)
                     {
                         List<Trn_MapperNameToEmailIdMapper> emailmapper = context.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == mappingModel.MapperId).ToList();
                         for (int item = 0; item < emailmapper.Count; item++)
                         {
                             int emailtableId = (from s in context.Trn_MapperNameToEmailIdMapper where s.MapperId == mappingModel.MapperId select s.Id).FirstOrDefault();
 
                             Trn_MapperNameToEmailIdMapper emailTable = context.Trn_MapperNameToEmailIdMapper.Find(emailtableId);
                             context.Trn_MapperNameToEmailIdMapper.Remove(emailTable);
                             context.SaveChanges();
                         }
 
 
                         //if (mappingModel.EmailIds != null)
                         //{
                         string[] EmailIds = mappingModel.EmailIds.Split(',').Select(sValue => sValue.Trim()).ToArray();
                         var e = from s in EmailIds
                                 select s;
 
                         int c = e.Count();
                         for (int item = 0; item < c; item++)
                         {
                             Trn_MapperNameToEmailIdMapper newEmailTable = new Trn_MapperNameToEmailIdMapper();
                             newEmailTable.EmailId = EmailIds[item];
                             newEmailTable.MapperId = mappingModel.MapperId;
                             newEmailTable.TransactionId = mappingModel.TransactionId;
                             context.Trn_MapperNameToEmailIdMapper.Add(newEmailTable);
                             context.SaveChanges();
                         }
                     }
 
 
                     //if (mappingModel.EmailIds != null)
                     //    {
                     //        string[] EmailIds = mappingModel.EmailIds.Split(',').Select(sValue => sValue.Trim()).ToArray();
                     //        for (int item = 0; item < emailmapper.Count; item++)
                     //        {
                     //            emailmapper[item].EmailId = EmailIds[item];
                     //            context.SaveChanges();
                     //        }
                     //    }
                     responseModel.Success = true;
                     responseModel.Message = "Header mapping done successfully ";
                     return responseModel;
 
                 }
                 //  }
 
             }
             catch (Exception ex)
             {
                 responseModel.Success = false;
                 responseModel.Message = "Unable to process your request.Please try again";
                 return responseModel;
             }
         }
 
         public bool DeleteSelectedMappingDetails(int susbsyToTransactionMapperId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 Trn_SubsiToTransactionFileMapper MapperDetails = context.Trn_SubsiToTransactionFileMapper.Find(susbsyToTransactionMapperId);
                 MapperDetails.IsEnabled = false;
                 List<Trn_MapperNameToEmailIdMapper> emailmapper = context.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == MapperDetails.Id).ToList();
                 for (int item = 0; item < emailmapper.Count; item++)
                 {
                     int emailtableId = (from s in context.Trn_MapperNameToEmailIdMapper where s.MapperId == MapperDetails.Id select s.Id).FirstOrDefault();
 
                     Trn_MapperNameToEmailIdMapper emailTable = context.Trn_MapperNameToEmailIdMapper.Find(emailtableId);
                     context.Trn_MapperNameToEmailIdMapper.Remove(emailTable);
                     context.SaveChanges();
                 }
                 context.SaveChanges();
                 return true;
             }
         }
 
         public ResponseModel SaveHeaderFields(MappingModel mappingModel, string username, string mappingFilePath)
         {
             ResponseModel responseModel = new ResponseModel();
             using (var scope = new TransactionScope())
             {
                 try
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
                         Trn_SubsiToTransactionFileMapper uploadfileMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                              where s.Filename == mappingModel.FileName
                                                                              select s).FirstOrDefault();
                         if (uploadfileMapper != null)
                         {
                             responseModel.Success = false;
                             responseModel.Message = "File name already exists.Please input another file name";
                             return responseModel;
                         }
                         else
                         {
 
                             int userId = (from s in context.Gen_User
                                           where s.Username == username
                                           select s.Id).FirstOrDefault();
 
 
                             string filePath = MoveMapperFile(username, mappingFilePath);
                             Trn_SubsiToTransactionFileMapper fileMapper = new Trn_SubsiToTransactionFileMapper();
                             fileMapper.EDISubsiId = mappingModel.SubsiId;
                             fileMapper.IsEnabled = true;
                             fileMapper.IsHeaderPresent = true;
                             fileMapper.LastUpdatedBy = userId;
                             fileMapper.LastUpdatedDate = DateTime.Now;
                             fileMapper.MessageFormatId = mappingModel.FileTypeId;
                             fileMapper.MinFieldValueCount = Convert.ToInt32(mappingModel.FieldCountValue);
                             fileMapper.Service = ConstantUtilities.Service;
                             fileMapper.Filename = mappingModel.FileName;
                             fileMapper.FileServerPath = filePath;
                             fileMapper.IsFTPEnabled = mappingModel.FtpIsEnabledForMapper;
                             fileMapper.TransactionCodeId = mappingModel.TransactionId;
                             fileMapper.FTPType = mappingModel.FtpType == "SFTP" ? 2 : 1;//2-SFTP,1-FTP
                             context.Trn_SubsiToTransactionFileMapper.Add(fileMapper);
                             context.SaveChanges();
 
                             if (mappingModel.FtpIsEnabledForMapper)
                             {
                                 string updatedDate = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss ");
                                 byte[] bytes = new byte[updatedDate.Length * sizeof(char)];
                                 System.Buffer.BlockCopy(updatedDate.ToCharArray(), 0, bytes, 0, bytes.Length);
 
                                 if (mappingModel.FtpType == "SFTP")
                                 {
                                     Gen_SFTPAccount ftpAccount = new Gen_SFTPAccount();
                                     ftpAccount.DateCreated = DateTime.Now;
                                     ftpAccount.Host = mappingModel.FtpHost;
                                     ftpAccount.IsEnabled = mappingModel.IsFtpEnable;
                                     ftpAccount.LastUpdatedBy = userId.ToString();
                                     ftpAccount.Name = mappingModel.FtpType;
                                     ftpAccount.Password = mappingModel.FtpPassword;
                                     ftpAccount.Port = mappingModel.FtpPort;
                                     ftpAccount.SshHostKeyFingerPrint = mappingModel.FtpSslHostCertificateFingerprint;
                                     ftpAccount.User = mappingModel.FtpUser;
                                     context.Gen_SFTPAccount.Add(ftpAccount);
                                     context.SaveChanges();
                                     string mapperFTP = CreateFTPFIleSaveDirectory(mappingModel.FileName);
                                     CreateFileTransferData(mappingModel.FileName, mappingModel.FtpLocation, mapperFTP, ftpAccount.SftpId, mappingModel.SubsiId, mappingModel.FtpType);
                                 }
                                 else
                                 {
                                     Gen_FTPAccount ftpAccount = new Gen_FTPAccount();
                                     ftpAccount.DateCreated = DateTime.Now;
                                     ftpAccount.EncryptionType = mappingModel.FtpEncryptiontype;
                                     ftpAccount.Host = mappingModel.FtpHost;
                                     ftpAccount.IsEnabled = mappingModel.IsFtpEnable;
                                     ftpAccount.LastUpdatedBy = userId.ToString();
                                     ftpAccount.Name = mappingModel.FtpType;
                                     ftpAccount.Password = mappingModel.FtpPassword;
                                     ftpAccount.Port = mappingModel.FtpPort;
                                     ftpAccount.SslHostCertificateFingerprint = mappingModel.FtpSslHostCertificateFingerprint;
                                     ftpAccount.User = mappingModel.FtpUser;
                                     context.Gen_FTPAccount.Add(ftpAccount);
                                     context.SaveChanges();
                                     string mapperFTP = CreateFTPFIleSaveDirectory(mappingModel.FileName);
                                     CreateFileTransferData(mappingModel.FileName, mappingModel.FtpLocation, mapperFTP, ftpAccount.SftpId, mappingModel.SubsiId, mappingModel.FtpType);
                                 }
 
 
                             }
 
 
                             Trn_SubsiToTransactionFileMapper headerMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                              where (s.Filename == mappingModel.FileName)
                                                                              select s).FirstOrDefault();
 
                             if (mappingModel.EnableEmail)
                             {
                                 string[] emailIds = mappingModel.EmailIds.Split(',');
                                 foreach (var item in emailIds)
                                 {
                                     Trn_MapperNameToEmailIdMapper emailIdMapper = new Trn_MapperNameToEmailIdMapper();
                                     emailIdMapper.EmailId = item;
                                     emailIdMapper.MapperId = fileMapper.Id;
                                     context.Trn_MapperNameToEmailIdMapper.Add(emailIdMapper);
                                     context.SaveChanges();
                                 }
                             }
 
                             // Inserting values to Trn_MapperFieldSpecs table
                             for (int i = 0; i < mappingModel.FileHeaders.Count; i++)
                             {
 
                                 Trn_MapperFieldSpecs headerFields = new Trn_MapperFieldSpecs();
                                 //Input file header
                                 headerFields.FieldName = mappingModel.FileHeaders[i].HeaderName.Replace(Environment.NewLine, " ").Replace("\n", " ");
                                 headerFields.FieldSequence = i + 1;
                                 headerFields.Length = 0;
                                 headerFields.SubsiToTransactionMapperId = headerMapper.Id;
 
                                 context.Trn_MapperFieldSpecs.Add(headerFields);
                                 context.SaveChanges();
 
                             }
 
                             // Inserting values to Trn_MappingSubsi table table
 
                             mappingSubsi = new Trn_MappingSubsi();
                             mappingSubsi.IsEnabled = true;
                             mappingSubsi.LastUpdatedBy = userId;
                             mappingSubsi.LastUpdatedDate = DateTime.Now;
                             mappingSubsi.SubsiToTransactionFileMapperId = headerMapper.Id;
                             mappingSubsi.SapMessageFieldId = mappingModel.TransactionId;
                             context.Trn_MappingSubsi.Add(mappingSubsi);
                             context.SaveChanges();
                             Trn_MappingSubsi subsi = (from s in context.Trn_MappingSubsi
                                                       where (s.SubsiToTransactionFileMapperId == headerMapper.Id) && (s.SapMessageFieldId == mappingModel.TransactionId) && (s.LastUpdatedBy == userId)
                                                       select s).FirstOrDefault();
                             // Inserting values to Trn_MappingConfiguration table
 
                             for (int item = 0; item < mappingModel.SAPFieldHeaders.Count; item++)
                             {
 
                                 mappingConfig = new Trn_MappingConfiguration();
                                 mappingConfig.MappingSubsiId = subsi.Id;
                                 BindControlsValues(mappingModel, item);
                                 context.Trn_MappingConfiguration.Add(mappingConfig);
                                 context.SaveChanges();
                                 // context.SaveChanges();
 
                             }
                             scope.Complete();
                             responseModel.Success = true;
                             responseModel.Message = "Header mapping done successfully ";
                             return responseModel;
 
                         }
                     }
 
                 }
                 catch (Exception ex)
                 {
                     scope.Dispose();
                     log.Error(" SaveHeaderFields in MapperManager .username:" + username + ", mappingFilePath:" + mappingFilePath + ",mappingModel:" + mappingModel + ". Api Exception : Message- " + ex.Message);
                     responseModel.Success = false;
                     responseModel.Message = "Unable to process your request.Please try again";
                     return responseModel;
                 }
             }
         }
 
         public ResponseModel IsValidFtp(FtpModel model)
         {
             ResponseModel response = new ResponseModel();
             try
             {
                 if (model.Name == "SFTP")
                 {
                     SFtpModel ftpnew = new SFtpModel();
                     ftpnew.Host = model.Host;
                     ftpnew.EncryptionType = model.EncryptionType;
                     ftpnew.Location = model.Location;
                     ftpnew.Name = model.Name;
                     ftpnew.Password = model.Password;
                     ftpnew.Port = model.Port;
                     ftpnew.SshHostKeyFingerPrint = model.SslHostCertificateFingerprint;
                     ftpnew.User = model.User;
 
                     SFtpGeneral sftp = new SFtpGeneral(ftpnew);
                     sftp.OpenConnection();
                     response.Success = sftp.IsConnected;
                     response.Message = "SFTP server is valid";
                     try
                     {
                         bool dirExist = sftp.IsDirectoryExists(model.Location);
                     }
                     catch (Exception e)
                     {
                         response.Message = "SFTP server is valid,But location is invalid / not accessable";
                         response.Success = false;
                     }
                     sftp.CloseConnection();
                 }
                 else
                 {
 
                     FtpGeneral ftp = new FtpGeneral(model);
                     ftp.OpenConnection();
                     response.Success = ftp.IsConnected;
                     response.Message = "FTP server is valid";
                     try
                     {
                         bool dirExist = ftp.IsDirectoryExists(model.Location);
                     }
                     catch (Exception e)
                     {
                         response.Message = "FTP server is valid,But location is invalid / not accessable";
                         response.Success = false;
                     }
                     ftp.CloseConnection();
                 }
             }
             catch (Exception e)
             {
                 log.Error("IsValidFtp error -" + e);
                 response.Message = "Unable to connect to FTP server.Check if server is valid";
                 response.Success = false;
             }
             return response;
         }
 
         public string MapperFileDate(string mapperName)
         {
             ViewSelectedMapperDataModel dataModel = new ViewSelectedMapperDataModel();
             IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
             var data = string.Join(",", mappedData.Select(x => x.FieldName).ToArray());
             dataModel.InputField = data;
             string filePath = HttpContext.Current.Server.MapPath("~/MappedFiles/Mappers");
             if (!Directory.Exists(filePath))
             {
                 Directory.CreateDirectory(filePath);
             }
             string destinationFile = Path.Combine(filePath, mapperName + ".xlsx");
             ExcelCreator.CreateExcel(data, mapperName, filePath);
 
             return destinationFile;
         }
 
         #region private
 
         private IList<Trn_MapperFieldSpecs> ValidateData(string fileName)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 IList<Trn_MapperFieldSpecs> mapperFields = (from s in context.Trn_SubsiToTransactionFileMapper
                                                             join p in context.Trn_MapperFieldSpecs on s.Id equals p.SubsiToTransactionMapperId
                                                             where (s.Filename == fileName)
                                                             select p).ToList();
 
                 return mapperFields;
             }
 
         }
         private string CreateFTPFIleSaveDirectory(string mapperName)
         {
             string filePath = "";
             try
             {
                 using (var context = new GoodpackEDIEntities())
                 {
                     filePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpFilePath").Select(i => i.Value).FirstOrDefault();
                     filePath = filePath + @"\" + mapperName;
                     if (!System.IO.File.Exists(filePath))
                     {
                         System.IO.Directory.CreateDirectory(filePath);
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("CreateFTPFIleSaveDirectory error-" + e);
                 throw e;
             }
             return filePath;
         }
 
         private bool CreateFileTransferData(string mapperName, string fromLocation, string toLocation, int sftpId, int SubsiId, string Type)
         {
             try
             {
                 using (var context = new GoodpackEDIEntities())
                 {
                     if (Type == "SFTP")
                     {
                         Trn_SFTP_File_Transfer fileTransfer = new Trn_SFTP_File_Transfer();
                         fileTransfer.EventType = mapperName + "_SFTP";
                         fileTransfer.FileTransferType = "DOWNLOAD";
                         fileTransfer.IsDeleteFile = true;
                         fileTransfer.LocationFrom = fromLocation;
                         fileTransfer.LocationTo = toLocation;
                         fileTransfer.SftpID = sftpId;
                         fileTransfer.MapperName = mapperName;
                         fileTransfer.SubsiId = SubsiId;
                         fileTransfer.IsTransferAndProcess = true;
                         context.Trn_SFTP_File_Transfer.Add(fileTransfer);
                         context.SaveChanges();
                         return true;
                     }
                     else
                     {
                         Trn_File_Transfer fileTransfer = new Trn_File_Transfer();
                         fileTransfer.EventType = mapperName + "_FTP";
                         fileTransfer.FileTransferType = "DOWNLOAD";
                         fileTransfer.IsDeleteFile = true;
                         fileTransfer.LocationFrom = fromLocation;
                         fileTransfer.LocationTo = toLocation;
                         fileTransfer.SftpID = sftpId;
                         fileTransfer.MapperName = mapperName;
                         fileTransfer.SubsiId = SubsiId;
                         fileTransfer.IsTransferAndProcess = true;
                         context.Trn_File_Transfer.Add(fileTransfer);
                         context.SaveChanges();
                         return true;
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("CreateFileTransferData error -" + e);
                 return false;
             }
         }
 
         private void BindControlsValuesForEdit(List<Trn_MappingConfiguration> MapConfig, MappingModel mappingModel, int item)
         {
             try
             {
                 switch (mappingModel.SAPFieldHeaders[item].InputType.ToUpper())
                 {
                     case "NONE":
                     case "OPERATION_NO_INPUT":
                     case "FIXED":
 
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "USER_FIXED":
                         MapConfig[item].FixedValue = mappingModel.MappedFileModel[item].HeaderName;
 
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELD":
                     case "MAP_FROM_SOURCE":
 
                         if (mappingModel.MappedFileModel != null)
                         {
                             int sapId = mappingModel.SAPFieldHeaders[item].Id;
                             MapperHeaderModel result = mappingModel.MappedFileModel.Find(x => x.Id == sapId);
 
                             if (result != null)
                             {
                                 MapConfig[item].SourceFieldSequence = result.SRNO;
                             }
                             else
                             {
                                 MapConfig[item].SourceFieldSequence = null;
                             }
 
 
                             MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         }
                         break;
                     case "MAP_FROM_SOURCE_WITH_SUBSTRING":
                     case "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING":
 
 
                         int sapItemId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel mappingResult = mappingModel.MappedFileModel.Find(x => x.Id == sapItemId);
 
                         if (mappingResult != null)
                         {
                             MapConfig[item].SourceFieldSequence = mappingResult.SRNO;
                         }
                         else
                         {
                             MapConfig[item].SourceFieldSequence = 0;
                         }
 
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "DATE":
                         int sapHeaderId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel header = mappingModel.MappedFileModel.Find(x => x.Id == sapHeaderId);
 
                         if (header != null)
                         {
                             MapConfig[item].SourceFieldSequence = header.SRNO;
                             if (mappingModel.DateFormat != null)
                             {
                                 MapConfig[item].InputDateFormat = mappingModel.DateFormat.ToUpper();
                             }
                         }
                         else
                         {
                             MapConfig[item].SourceFieldSequence = null;
 
                         }
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                     case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
 
                         int sapMapId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel mapHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapMapId);
 
                         if (mapHeader != null)
                         {
 
                             if ((mapHeader.HeaderName != string.Empty) && (mapHeader.FieldSequence == 0))
                             {
                                 MapConfig[item].FixedValue = mapHeader.HeaderName.ToUpper();
                             }
                             else
                             {
                                 MapConfig[item].FixedValue = null;
                                 MapConfig[item].SourceFieldSequence = mapHeader.SRNO;
                             }
                         }
                         else
                         {
                             MapConfig[item].SourceFieldSequence = null;
                         }
 
 
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELDS2":
                         int sapOperationMapId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel operationHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapOperationMapId);
 
                         if (operationHeader != null)
                         {
                             MapConfig[item].SourceFieldSeq2 = operationHeader.SRNO;
                             MapConfig[item].SourceFieldSequence = null;
                         }
                         else
                         {
                             MapConfig[item].SourceFieldSeq2 = null;
                             MapConfig[item].SourceFieldSequence = null;
                         }
 
                         MapConfig[item].SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
 
                         break;
 
                 }//-- End Switch(inputType)
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
 
         private string MoveMapperFile(string username, string sourceFile)
         {
             string filePath = HttpContext.Current.Server.MapPath("~/MappedFiles/" + username);
             if (!Directory.Exists(filePath))
             {
                 Directory.CreateDirectory(filePath);
             }
             string destinationFile = Path.Combine(filePath, Path.GetFileName(sourceFile));
 
             File.Move(sourceFile, destinationFile);
             return destinationFile;
         }
 
         #region - BindControls
         //<summary>
         //based on the type bind the controls in screen
         //</summary>
         //<param name="mappingModel"></param>
         //<param name="item"></param>
         private void BindControlsValues(MappingModel mappingModel, int item)
         {
             try
             {
 
 
                 switch (mappingModel.SAPFieldHeaders[item].InputType.ToUpper())
                 {
                     case "NONE":
                     case "OPERATION_NO_INPUT":
                     case "FIXED":
 
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "USER_FIXED":
                         mappingConfig.FixedValue = mappingModel.MappedFileModel[item].HeaderName;
 
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELD":
                     case "MAP_FROM_SOURCE":
 
                         if (mappingModel.MappedFileModel != null)
                         {
                             int sapId = mappingModel.SAPFieldHeaders[item].Id;
                             MapperHeaderModel result = mappingModel.MappedFileModel.Find(x => x.Id == sapId);
 
                             if (result != null)
                             {
                                 mappingConfig.SourceFieldSequence = result.SRNO;
                             }
                             else
                             {
                                 mappingConfig.SourceFieldSequence = null;
                             }
 
 
                             mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         }
                         break;
                     case "MAP_FROM_SOURCE_WITH_SUBSTRING":
                     case "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING":
 
 
                         int sapItemId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel mappingResult = mappingModel.MappedFileModel.Find(x => x.Id == sapItemId);
 
                         if (mappingResult != null)
                         {
                             mappingConfig.SourceFieldSequence = mappingResult.SRNO;
                         }
                         else
                         {
                             mappingConfig.SourceFieldSequence = 0;
                         }
 
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "DATE":
                         int sapHeaderId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel header = mappingModel.MappedFileModel.Find(x => x.Id == sapHeaderId);
 
                         if (header != null)
                         {
                             mappingConfig.SourceFieldSequence = header.SRNO;
                             if (mappingModel.DateFormat != null)
                             {
                                 mappingConfig.InputDateFormat = mappingModel.DateFormat.ToUpper();
                             }
                         }
                         else
                         {
                             mappingConfig.SourceFieldSequence = null;
 
                         }
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                     case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
                         //TableRow tableRow = (TableRow)tableCell.Controls[0].Controls[0];
                         //TextBox tbFixedValue = (TextBox)tableRow.Cells[0].Controls[1];
                         //ddlSource = (DropDownList)tableRow.Cells[0].Controls[3].Controls[0];
                         //Label lblInputType = (Label)tableRow.Cells[0].Controls[4];
 
                         int sapMapId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel mapHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapMapId);
 
                         if (mapHeader != null)
                         {
 
                             if ((mapHeader.HeaderName != string.Empty) && (mapHeader.FieldSequence == 0))
                             {
                                 mappingConfig.FixedValue = mapHeader.HeaderName.ToUpper();
                             }
                             else
                             {
                                 mappingConfig.SourceFieldSequence = mapHeader.SRNO;
                             }
                         }
                         else
                         {
                             mappingConfig.SourceFieldSequence = null;
                         }
 
 
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                         break;
                     case "OPERATION_ON_SOURCE_FIELDS2":
 
                         //ucSourceHeader2Fields ucOperationOnSourceFields2 = (ucSourceHeader2Fields)tableCell.Controls[0];
                         //if (ucOperationOnSourceFields2.SelectedValue != CommonConstants.DEFAULT_COMBOVALUE)
                         //    objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields2.SelectedValue);
                         //else
                         //    objMappingConfig.SourceFieldSeq = null;
                         //if (ucOperationOnSourceFields2.SelectedValue1 != CommonConstants.DEFAULT_COMBOVALUE)
                         //    objMappingConfig.SourceFieldSeq2 = Convert.ToInt32(ucOperationOnSourceFields2.SelectedValue1);
                         //else
                         //    objMappingConfig.SourceFieldSeq2 = null;
 
                         int sapOperationMapId = mappingModel.SAPFieldHeaders[item].Id;
                         MapperHeaderModel operationHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapOperationMapId);
 
                         if (operationHeader != null)
                         {
                             mappingConfig.SourceFieldSeq2 = operationHeader.SRNO;
                             mappingConfig.SourceFieldSequence = null;
                         }
                         else
                         {
                             mappingConfig.SourceFieldSeq2 = null;
                             mappingConfig.SourceFieldSequence = null;
                         }
 
                         mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
 
                         break;
                     //case "MAP_ETD_FROM_SOURCE":
                     //    ucSourceHeader3Fields ucOperationOnSourceFields3 = (ucSourceHeader3Fields)tableCell.Controls[0];
                     //    if (ucOperationOnSourceFields3.SelectedValueMappingType == "1")
                     //    {
                     //        if (ucOperationOnSourceFields3.SelectedValueDateField != CommonConstants.DEFAULT_COMBOVALUE)
                     //            objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValueDateField);
                     //        if (ucOperationOnSourceFields3.DateFormatDirectTextBox != null)
                     //        {
                     //            objMappingConfig.SourceDateFormat = ucOperationOnSourceFields3.DateFormatDirectTextBox.Text.ToUpper();
                     //        }
                     //    }
                     //    else if (ucOperationOnSourceFields3.SelectedValueMappingType == "2")
                     //    {
                     //        if (ucOperationOnSourceFields3.SelectedValuePOL != CommonConstants.DEFAULT_COMBOVALUE)
                     //            objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValuePOL);
                     //        if (ucOperationOnSourceFields3.SelectedValuePOD != CommonConstants.DEFAULT_COMBOVALUE)
                     //            objMappingConfig.SourceFieldSeq2 = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValuePOD);
                     //        if (ucOperationOnSourceFields3.SelectedValueArrivalDate != CommonConstants.DEFAULT_COMBOVALUE)
                     //            objMappingConfig.SourceFieldSeq3 = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValueArrivalDate);
                     //        if (ucOperationOnSourceFields3.DateFormatETDTextBox != null)
                     //            objMappingConfig.SourceDateFormat = ucOperationOnSourceFields3.DateFormatETDTextBox.Text.ToUpper();
                     //    }
 
                     //    objMappingConfig.MappingType = ucOperationOnSourceFields3.SelectedValueMappingType;
 
 
 
                     //    objMappingHead.TargetMessageFieldSpecs.ID = Convert.ToInt32(targetFieldId);
                     //    objMappingConfig.TargetFieldSeq = Convert.ToInt32(targetFieldSeq);
 
                     //    break;
                     case "DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD":
                         {
                             //ucSingleSourceWithSubStringandOperation ucSingleSource_Operation_Substring = (ucSingleSourceWithSubStringandOperation)tableCell.Controls[0];
 
                             //if (ucSingleSource_Operation_Substring.SourceSelectedValue != CommonConstants.DEFAULT_COMBOVALUE)
                             //    mappingConfig.SourceFieldSequence = Convert.ToInt32(ucSingleSource_Operation_Substring.SourceSelectedValue);
 
 
 
                             int sapDynamicItemId = mappingModel.SAPFieldHeaders[item].Id;
                             MapperHeaderModel dynamicMappingResult = mappingModel.MappedFileModel.Find(x => x.Id == sapDynamicItemId);
                             if (dynamicMappingResult != null)
                             {
                                 mappingConfig.SourceFieldSequence = dynamicMappingResult.SRNO;
                             }
                             //if (ucSingleSource_Operation_Substring.IsSubstringFunctionEnabled)
                             // {
                             //     if (ucSingleSource_Operation_Substring.SubStringStartPositionTextBox.Text != string.Empty)
                             //    {
                             //         mappingConfig.SourceFieldSeq1SubStringStartPosition = Convert.ToInt32(ucSingleSource_Operation_Substring.SubStringStartPositionTextBox.Text);
                             //     }
                             //     else
                             //     {
                             //         mappingConfig.SourceFieldSeq1SubStringStartPosition = null;
                             //     }
 
                             //     if (ucSingleSource_Operation_Substring.SubStringLengthTextBox.Text != string.Empty)
                             //     {
                             //         mappingConfig.SourceFieldSeq1SubStringLength = Convert.ToInt32(ucSingleSource_Operation_Substring.SubStringLengthTextBox.Text);
                             //     }
                             //     else
                             //     {
                             //         mappingConfig.SourceFieldSeq1SubStringLength = null;
                             //     }
                             // }
 
                             // objMappingHead.TargetMessageFieldSpecs.ID = Convert.ToInt32(targetFieldId);
                             mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                             mappingConfig.MappingType = "NO OPERATION";
                             break;
                         }
                 }//-- End Switch(inputType)
             }
             catch (Exception ex)
             {
                 throw ex;
             }
         }
         #endregion
 
         #endregion
     }
 }



