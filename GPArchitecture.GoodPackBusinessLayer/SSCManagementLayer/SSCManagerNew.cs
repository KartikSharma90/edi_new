﻿using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace GPArchitecture.GoodPackBusinessLayer.SSCManagementLayer
{
    public class SSCManagerNew
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SSCManagerNew));
        private SCTransactionModel objGPCustomerMessage;
        public bool isTermTrip { get; set; }
        private Dictionary<int, Dictionary<string, string>> sapSubmitTransData;
        private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
        private string _UserName { get; set; }
        //IList<GenericItemModel>
        public IList<GenericItemModel> submitSSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData)
        {
            IList<GenericItemModel> returnVlaue = null; ;
            try
            {
                _UserName = username;
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                var defaultVal = "";
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    //itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());
                    var transactionType=objGPCustomerMessage.TransactionType.ToString();
                    var mappingSubsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                                    join m in context.Trn_MappingSubsi
                                    on s.Id equals m.SubsiToTransactionFileMapperId
                                    where s.Filename == mapperFile
                                    select m.Id).FirstOrDefault();
                    var sapFieldseq= context.Ref_SAPMessageFields.Where(i=>i.SAPFieldName=="Material Number" && i.Ref_TransactionTypes.TransactionCode==transactionType).Select(i=>i.FieldSequence).FirstOrDefault();
                    defaultVal = (from s in context.Trn_MappingConfiguration
                                      where s.MappingSubsiId == mappingSubsiId
                                      && s.SapFieldSeq == sapFieldseq
                                      select s.FixedValue).FirstOrDefault();                       
                }
                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                objGPCustomerMessage.mappingServiceValues.ForEach(i =>
                {
                    if (string.IsNullOrEmpty(i[SCSapMessageFields.MaterialNumber]))
                    {
                        i[SCSapMessageFields.MaterialNumber] = string.IsNullOrEmpty(defaultVal) ? "" : defaultVal;
                    }
                });
                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                         && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    List<int> listInt = InsertSSCDatatoShipmentDetailTable(objGPCustomerMessage.mappingServiceValues, mapperFile, Convert.ToInt32(subsiId));
                    List<SScTOScConvertionModel> listSSC = CreateIntermediateData(listInt, Convert.ToInt32(subsiId), mapperFile);
                    int batchId = InsertSSCTransBatch(listSSC, objGPCustomerMessage.BatchFileSourceType.ToString(),objGPCustomerMessage.BatchFileName);
                    itemModel[0].BatchId = batchId.ToString();
                    InsertSSCTransLine(listSSC, batchId, listInt);
                    returnVlaue = GetResponseData(itemModel, listSSC);
                }
                return returnVlaue;
            }
            catch (Exception e)
            {
                log.Error("submitSSCData :-" + e);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = e.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                UpdateSSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;
            }
        }
        private IList<GenericItemModel> GetResponseData(IList<GenericItemModel> itemModel, List<SScTOScConvertionModel> data)
        {
            GenericItemModel itemModelVal = itemModel[1];
            IList<GenericItemModel> genericModel = null;
            genericModel = SScTestResult(itemModel, data);

            //if (genericModel.Count > 0)
            //{
            //    genericModel[0].sapResponseData = sapSubmitTransData;
            //}
            //for (int i = 0; i < genericModel.Count; i++)
            //{
            //    itemModelData.Add(genericModel[i]);
            //    if (i == 0)
            //    {
            //        itemModelData.Add(itemModelVal);
            //    }
            //}
            return genericModel;
        }

        public IList<GenericItemModel> SScTestResult(IList<GenericItemModel> itemModel, List<SScTOScConvertionModel> sscData)
        {
            using (var context = new GoodpackEDIEntities())
            {
                IList<GenericItemModel> returnModel = new List<GenericItemModel>(); 
                for (int i = 0; i < sscData.Count; i++)
                {
                    int lineId = Convert.ToInt32(sscData[i].TransLineId);
                    Trn_SSCTransLineNew transLine = context.Trn_SSCTransLineNew.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();
                    var errorMessage = String.Join(",", context.Trn_SSCTransLineContentNew.Where(x => x.LineId == lineId).Select(x => x.SapResponseMessage).ToArray());
                    int m = 0;
                    foreach(var data in itemModel)
                    {
                        if(Array.Exists(itemModel[m].Name.Split('|'), x => x == sscData[i].SInumber))
                        {
                            itemModel[m].LineId = lineId;
                            itemModel[m].Status = statusCode;
                            itemModel[m].ErrorMessage = errorMessage;
                        }
                        m++;
                    }
                    //itemModel[i + 1].LineId = lineId;
                    //itemModel[i + 1].Status = statusCode;
                    //var test = Array.Exists(itemModel[i + 1].Name.Split('|'), x => x == "APLU 0602269/1");
                    //var test1 = Array.FindIndex(itemModel[i + 1].Name.Split('|'), x => x == "APLU 0602269/1");
                    //var test2 = Array.FindIndex(itemModel[i + 1].Name.Split('|'), x => x == transLine.SINumber);
                }
                return itemModel;
            }
        }
        private List<int> InsertSSCDatatoShipmentDetailTable(List<Dictionary<string, string>> data,string mapper,int subsy)
        {
            List<int> listInt = new List<int>();
            try
            {                
                foreach (var value in data)
                {
                    using (var context = new GoodpackEDIEntities())
                    {
                        Trn_ScannedShipmentDetails scannedDetails = new Trn_ScannedShipmentDetails();
                        var barcode = value[SCSapMessageFields.TwoDPin];
                        bool leadingZeroadded = false;
                        var length = barcode.Length;
                        if (length < 9)
                        {
                            leadingZeroadded = true;
                            barcode = barcode.PadLeft(9, '0');
                        }
                        scannedDetails.IsLeadingZeroAdded = leadingZeroadded;
                        try
                        {
                            if (barcode.Equals(ConvertRFIDTo1D(Convert1DToRFID(barcode))))
                            {
                                scannedDetails.IsOneDBarcode = true;
                                scannedDetails.IsCheckSumValid = true;
                            }
                            else
                            {
                                scannedDetails.IsOneDBarcode = false;
                                scannedDetails.IsCheckSumValid = false;
                            }
                        }
                        catch (Exception e)
                        {
                            scannedDetails.IsOneDBarcode = false;
                            scannedDetails.IsCheckSumValid = false;
                        }
                        scannedDetails.Barcode = barcode;//value[SCSapMessageFields.TwoDPin];
                        scannedDetails.BinType = value[SCSapMessageFields.MaterialNumber];
                        scannedDetails.ConsigneeId = value[SCSapMessageFields.ToLocation];
                        scannedDetails.CreatedDate = DateTime.Now;
                        scannedDetails.CustomerReferenceNumber = value[SCSapMessageFields.CustomerReferenceNumber];
                        scannedDetails.DeviceId = value[SCSapMessageFields.DeviceId];
                        scannedDetails.ETA = value[SCSapMessageFields.DTADate];
                        scannedDetails.ETD = value[SCSapMessageFields.ETDDate];
                        scannedDetails.FileSource = SOURCE_TYPES.FILE_SYSTEM.ToString();
                        scannedDetails.SINumber = value[SCSapMessageFields.CustomerReferenceNumber];
                        scannedDetails.CustomerCode = GetDecodeValue(value[SCSapMessageFields.CustomerNumber], "Customer Number", mapper, subsy);
                        scannedDetails.PackerCode = GetDecodeValue(value[SCSapMessageFields.FromLocation], "From Location", mapper, subsy);
                        scannedDetails.Subsy = subsy.ToString();
                        context.Trn_ScannedShipmentDetails.Add(scannedDetails);
                        context.SaveChanges();
                        listInt.Add(scannedDetails.Id);
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("InsertSSCDatatoShipmentDetailTable " + e);
                throw e;
            }
            return listInt;
        }
        private List<SScTOScConvertionModel> CreateIntermediateData(List<int> shipmentDetailsIds,int subsy,string mapper)
        {
            List<SScTOScConvertionModel> ssctoScConvertionModel = new List<SScTOScConvertionModel>();
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    List<DataConvertionModel> data = (from queryResult in context.Trn_ScannedShipmentDetails
                                                      where shipmentDetailsIds.Contains(queryResult.Id)
                                                      group queryResult by queryResult.SINumber into rowGroup
                                                      select new DataConvertionModel
                                                      {
                                                          SINumber = rowGroup.Key,
                                                          Count = rowGroup.Count()
                                                      }).ToList();

                    foreach (var t in data)
                    {
                        var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                        where shipmentDetailsIds.Contains(s.Id)
                                        select s.DeviceId).FirstOrDefault();
                        if(deviceId==null)
                        {
                           // throw new FormatException("Device Id is not Valid");
                        }
                        //var packingPlantCustomerCode = GetPackerCodeFromDeviceId(deviceId);
                        //string packingPlantString = packingPlantCustomerCode.PackerCode.ToString();
                        //string customerCode = packingPlantCustomerCode.CustomerCode.ToString();
                        //var subsyVal = int.Parse(packingPlantCustomerCode.Subsy.ToString().Trim());
                        //int susyId = context.Gen_EDISubsies.Where(i => i.SubsiCode == subsyVal).Select(i => i.Id).SingleOrDefault();
                        SScTOScConvertionModel ssctoscModel = (from s in context.Trn_ScannedShipmentDetails
                                                               where shipmentDetailsIds.Contains(s.Id) && s.SINumber == t.SINumber
                                                               select new SScTOScConvertionModel
                                                               {
                                                                   BinType = s.BinType,
                                                                   Consignee = s.ConsigneeId,
                                                                   CustomerName =s.CustomerCode,
                                                                   ETA = s.ETA,
                                                                   ETD = s.ETD,
                                                                   DeviceId = deviceId,
                                                                   PackingPlant = s.PackerCode,
                                                                   Quantity = t.Count,
                                                                   Remark = "",
                                                                   MapperName = mapper,
                                                                   LineId = s.Id,
                                                                   SubsiId =subsy,
                                                                   SInumber = s.SINumber
                                                               }).FirstOrDefault();
                        ssctoScConvertionModel.Add(ssctoscModel);
                    }
                    DataTable dataTable = ConvertToDatatable(ssctoScConvertionModel);
                    var batchData = WriteDataTable(dataTable);
                    ssctoScConvertionModel.ForEach(a => a.Data = batchData);
                }
            }
            catch (Exception e)
            {
                log.Error("CreateIntermediateData " + e);
                throw e;
            }
            return ssctoScConvertionModel;
        }

        private string Convert1DToRFID(string code_1D)
        {
            string str1_8 = code_1D.Substring(0, 8);
            //MessageBox.Show("|" + str1_8 + "|");
            string hexValue = int.Parse(str1_8).ToString("x").ToUpper();

            string zeroCount = new String('0', 9 - hexValue.Length);
            string hexadecimalAsset = zeroCount + hexValue;
            string goodPackcode = "33161E88D800004";
            string rfidGRAINum = goodPackcode + hexadecimalAsset;
            return rfidGRAINum;
        }
        private string ConvertRFIDTo1D(string rfid)
        {
            string processedStr = rfid.Substring(rfid.Length - 9, 9);
            string decValue = int.Parse(processedStr, System.Globalization.NumberStyles.HexNumber).ToString();
            string zeroCount = new String('0', 8 - decValue.Length);
            string concatenatedStr = zeroCount + decValue;

            int i1X1 = (int.Parse(concatenatedStr.Substring(0, 1)) * 1);
            int i2X3 = (int.Parse(concatenatedStr.Substring(1, 1)) * 3);
            int i3X1 = (int.Parse(concatenatedStr.Substring(2, 1)) * 1);
            int i4X1 = (int.Parse(concatenatedStr.Substring(3, 1)) * 3);
            int i5X3 = (int.Parse(concatenatedStr.Substring(4, 1)) * 1);
            int i6X1 = (int.Parse(concatenatedStr.Substring(5, 1)) * 3);
            int i7X1 = (int.Parse(concatenatedStr.Substring(6, 1)) * 1);
            int i8X3 = (int.Parse(concatenatedStr.Substring(7, 1)) * 3);

            string counterSum = (100000 - (i1X1 + i2X3 + i3X1 + i4X1 + i5X3 + i6X1 + i7X1 + i8X3)).ToString();
            string checkDigit = counterSum.Substring(counterSum.Length - 1, 1);
            string str1DCode = concatenatedStr + checkDigit;
            return str1DCode;
        }

        private string GetDecodeValue(string data, string header, string mapperName, int subsiId)
        {
            string decodeValue = null;
            int lookupNameId = 0;
            int transactionId;
            string inputType = String.Empty;
            IDecodeValueManager decodeValueManager = new DecodeValueManager();
            using (var context = new GoodpackEDIEntities())
            {
                transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                 where s.Filename == mapperName
                                 select s.TransactionCodeId).FirstOrDefault();

                inputType = (from s in context.Ref_SAPMessageFields
                             where ((s.SAPFieldName == header) && (s.TransactionId == transactionId))
                             select s.InputType).FirstOrDefault();
            }
            switch (header.ToUpper())
            {
                case "CUSTOMER CODE":
                case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                    break;
                case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                    break;
                case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                    break;
                case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                    break;
                case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                    break;
                case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                    break;
                case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                    break;
                case "FRM LOCATION":
                case "FROM LOCATION": if (data.StartsWith("3") && data.Length == 6)
                    {
                        lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                    }
                    else
                    {
                        if (transactionId == 12)
                        {
                            lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                        }
                        else
                        {
                            lookupNameId = GetLookupId(ConstantUtilities.Packer);
                        }
                    }
                    break;
                case "TO LOCATION": if (data.StartsWith("5") && data.Length == 6)
                    {
                        lookupNameId = GetLookupId(ConstantUtilities.Packer);
                    }
                    else
                    {
                        lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                    }
                    break;
                case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                    break;
            }
            TargetMessageInputTypes inputSAPType = (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);
            decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data, mapperName, subsiId, lookupNameId);
            if (String.IsNullOrEmpty(decodeValue))
                decodeValue = data;
            return decodeValue;
        }
       
        public int GetLookupId(string LookupName)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames
                        where s.LookupName == LookupName
                        select s.Id).FirstOrDefault();

            }

        }
        private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
        {
            IValidationTypeManger validator = new ValidationTypeManager();
            Dictionary<string, string> validatorParams = new Dictionary<string, string>();
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.InputType, inputType);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.MapperName, mapperName);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.FileData, fileValue);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.SAPField, sapField);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
            switch (inputType)
            {
                case GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);

                default:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
            }


        }
        private DeviceDetail GetPackerCodeFromDeviceId(string deviceId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                var packerCustomerCode = (from s in context.Trn_DeviceDetails
                                          where s.DeviceID == deviceId
                                          select new DeviceDetail { PackerCode = s.PackerCode, MapperName = s.MapperName, CustomerCode = s.CustomerCode,Subsy=s.SubsiId }).SingleOrDefault();
                return packerCustomerCode;
            }

        }

        private int InsertSSCTransBatch(List<SScTOScConvertionModel> listSSC,string transactionType,string fileName)
        {
            int batchId = 0;
            int totalCount = listSSC.Count;
            int errorcount = listSSC.Where(r => r.ErrorMessage != null).Count();
            using (var context = new GoodpackEDIEntities())
            {
                foreach (var sscData in listSSC)
                {
                    ITirmTripManager termTrip = new TirmTripManager();
                    isTermTrip = termTrip.IsTirmTripORNot(sscData.CustomerName);
                    var userid = (from s in context.Gen_User
                                  where s.Username == _UserName
                                  select s.Id).SingleOrDefault();
                    Trn_SSCTransBatch scFileData = new Trn_SSCTransBatch();
                    scFileData.UserId = Convert.ToInt32(userid);
                    scFileData.SubsyId = sscData.SubsiId;
                    scFileData.TransactionId = 3;
                    scFileData.MapperFileName = sscData.MapperName;
                    scFileData.StatusId = 17;
                    scFileData.ErrorId = 28;
                    scFileData.IsTermTrip = isTermTrip;
                    scFileData.FileName = fileName;
                    scFileData.FileSource = transactionType;
                    //scFileData.FileSourceAddress =;
                    scFileData.BatchContent = sscData.Data;
                    scFileData.ErrorDescription = "";
                    scFileData.DateCreated = DateTime.Now;
                    scFileData.DateUpdated = null;
                    scFileData.RecordCountTotal = totalCount;
                    scFileData.RecordCountSkipValFailure = errorcount;
                    scFileData.RecordCountFieldValFailure = 0;
                    scFileData.RecordCountSAPError = 0;
                    scFileData.RecordCountSuccess = 0;
                    scFileData.RecordCountSAPInProgress = 0;
                    scFileData.DateExportedToTargetSystem = null;
                    //scFileData.IsTestMode = true;
                    scFileData.EmailId = null;
                    context.Trn_SSCTransBatch.Add(scFileData);
                    context.SaveChanges();
                    batchId = scFileData.Id;
                    break;
                }
            }
            return batchId;
        }

        private void InsertSSCTransLine(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids)
        {
            try
            { 
                using (var context = new GoodpackEDIEntities())
                {
                    foreach (var sscData in listSSC)
                    {
                        ITirmTripManager termtrip = new TirmTripManager();
                        bool termTrip = termtrip.IsTirmTripORNot(sscData.CustomerName);
                        Trn_SSCTransLineNew sscTrsansLine = new Trn_SSCTransLineNew();
                        sscTrsansLine.BatchId = batchId;
                        sscTrsansLine.BinType = sscData.BinType;                        
                        sscTrsansLine.CustomerCode = sscData.CustomerName;
                        sscTrsansLine.CustomerRefNumber = sscData.SInumber;
                        sscTrsansLine.ETA = sscData.ETA;
                        sscTrsansLine.ETD = sscData.ETD;
                        sscTrsansLine.FromLocation = sscData.PackingPlant;
                        sscTrsansLine.InTestMode = true;
                        sscTrsansLine.IsActive = true;
                        sscTrsansLine.ItemNumber = "10";
                        sscTrsansLine.ItrNumber = "10";
                        sscTrsansLine.ItrType = "SHCN";
                        sscTrsansLine.LastUpdated = DateTime.Now;
                        sscTrsansLine.LineContent = "";
                        sscTrsansLine.LineNumber = sscData.LineId;
                        sscTrsansLine.StatusId = 3;
                        if (termTrip)
                            sscTrsansLine.TermTripSAP = false;
                        else
                            sscTrsansLine.TermTripSAP = null;
                        sscTrsansLine.Quantity = sscData.Quantity;
                        sscTrsansLine.SubsiId = sscData.ErrorMessage == null ? 6 : 1;
                        sscTrsansLine.ToLocation = sscData.Consignee;
                        context.Trn_SSCTransLineNew.Add(sscTrsansLine);
                        context.SaveChanges();
                        sscData.TransLineId = sscTrsansLine.Id;
                       // UpdateScannedShipmentDetailsLineId(sscData.SInumber, batchId, sscTrsansLine.Id);
                    }
                }
                SubmitToSAPandUpdateSSCTransBatch(listSSC, batchId, Ids, false);
            }
            catch (Exception e)
            {
                log.Error("InsertSSCTransLine :-" + e);
            }
        }
        private void UpdateScannedShipmentDetailsLineId(string SInumber,int BatchId, int LineNumber)
        {
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    List<Trn_ScannedShipmentDetails> listScannedShipmentDtails = context.Trn_ScannedShipmentDetails.Where(i => i.BatchId == BatchId && i.SINumber == SInumber).ToList();
                    listScannedShipmentDtails.ForEach(i => { i.SSCLineNumber = LineNumber; });
                    context.SaveChanges();
                }
            }
            catch(Exception e)
            {
                log.Error("UpdateScannedShipmentDetailsLineId error-" + e);
            }

        }
        private void SubmitToSAPandUpdateSSCTransBatch(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids, bool isUpdate)
        {
            try
            {
                int successCount = 0;
                int errorCount = 0;
                int totalFailedCount = 0;
                List<ResponseModel> listResponse = null;
                SCServiceManager serviceCaller = new SCServiceManager();
                List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
                List<SScTOScConvertionModel> failureList = listSSC.Where(i => i.ErrorMessage != null).ToList();
                foreach (var errorData in failureList)
                {
                    ITirmTripManager termTrip = new TirmTripManager();
                    isTermTrip = termTrip.IsTirmTripORNot(errorData.CustomerName);
                    List<ResponseModel> responseModel = new List<ResponseModel>();
                    ResponseModel response = new ResponseModel();
                    response.Success = false;
                    response.Message = errorData.ErrorMessage;
                    responseModel.Add(response);
                    if (isUpdate)
                    {
                        UpdateSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                    }
                    else
                    {
                        InsertSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                    }
                    using (var context = new GoodpackEDIEntities())
                    {
                        Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                        sscTransbatch.RecordCountSAPError = failureList.Count();
                        //sscTransbatch.RecordCountSuccess = successCount;
                        context.SaveChanges();
                    }
                    UpdateSSCTransLineStatus(errorData.LineId, responseModel);
                }
                foreach (var data in successList)
                {
                    ITirmTripManager termTrip = new TirmTripManager();
                    isTermTrip = termTrip.IsTirmTripORNot(data.CustomerName);
                    var username = "";
                    int? userId = 0;
                    using (var context = new GoodpackEDIEntities())
                    {
                        userId = (from s in context.Trn_DeviceDetails
                                  where s.DeviceID == data.DeviceId
                                  select s.UserId).FirstOrDefault();
                        username = (from s in context.Gen_User
                                    where s.Id == userId
                                    select s.Username).FirstOrDefault();
                    }
                    try
                    {
                        listResponse = serviceCaller.scServiceCaller(CreateSCSAPDictionary(data), null, ref successCount, ref errorCount, username);
                    }
                    catch(Exception ex)
                    {
                        log.Error("Sc SAP service error ;" + ex);                       
                        ResponseModel responseModel = new ResponseModel();
                        responseModel.Success = false;
                        responseModel.Message = "Error contacting SAP";
                        listResponse.Add(responseModel);
                    }
                    if (isUpdate)
                    {
                        totalFailedCount = UpdateSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                        UpdateSSCTransLineStatus(data.LineId, listResponse);
                    }
                    else
                    {
                        totalFailedCount = InsertSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                        UpdateSSCTransLineStatus(data.LineId, listResponse);
                    }
                }
                
                using (var context = new GoodpackEDIEntities())
                {
                    totalFailedCount = context.Trn_SSCTransLineNew.Where(i => i.BatchId == batchId && i.Trn_LineStatus.StatusCode == "ERROR").Count();
                    var succsCount = context.Trn_SSCTransLineNew.Where(i => i.BatchId == batchId && i.Trn_LineStatus.StatusCode == "SUCCESS").Count();
                    Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                    sscTransbatch.RecordCountSAPError = totalFailedCount;
                    if (!isTermTrip)
                        sscTransbatch.RecordCountSuccess = succsCount;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                log.Error("SubmitToSAPandUpdateSSCTransBatch :-" + e);
            }
        }
        private int UpdateSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel, int Id, List<int> updateIds, bool isTermTrip)
        {
            string lineStatus = null;
            int failedCount = 0;
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < responseModel.Count; i++)
                {
                    Trn_SSCTransLineContentNew lineContent = context.Trn_SSCTransLineContentNew.FirstOrDefault(j => j.LineId == lineId && j.BatchID == batchId);
                    if (responseModel[i].Success)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        failedCount++;
                        lineStatus = LineStatus.ERROR.ToString();
                    }

                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    string sapMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                ? "Waiting for SAP response " : responseModel[i].Message;
                    lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                    ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                    var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                    scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; a.SAPBinSyncMessage = sapMessage; });
                    if (isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() == lineStatusId)
                        scannedData.ForEach(a => { a.SAPBinStatus = lineStatusId; });
                    lineContent.StatusId = lineStatusId;
                    //lineContent.SapResponseMessage = responseModel[i].Message;
                    lineContent.SapResponseMessage = sapMessage;
                    context.SaveChanges();
                }
            }
            return failedCount;
        }
        private int InsertSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel, int Id, List<int> updateIds, bool isTermTrip)
        {
            string lineStatus = null;
            int failedCount = 0;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SSCTransLineContentNew lineContent = new Trn_SSCTransLineContentNew();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    if (responseModel[i].Success)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        failedCount++;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                  ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                    string sapMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                ? "Waiting for SAP response " : responseModel[i].Message;
                    //UpdateScannedShipMentDetails
                    var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                    scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; a.SAPBinSyncMessage = sapMessage; });
                    if (isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() == lineStatusId)
                        scannedData.ForEach(a => { a.SAPBinStatus = lineStatusId; });

                    lineContent.BatchID = batchId;
                    lineContent.LineId = lineId;
                    lineContent.StatusId = lineStatusId;
                    //lineContent.SapResponseMessage = responseModel[i].Message;
                    lineContent.SapResponseMessage = sapMessage;
                    lineContent.ScannedDataLineId = Id; //Ids.FirstOrDefault();
                    context.Trn_SSCTransLineContentNew.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return failedCount;
        }
        private Dictionary<string, string> CreateSCSAPDictionary(SScTOScConvertionModel list)
        {
            Dictionary<string, string> scData = new Dictionary<string, string>();
            scData.Add(SCSapMessageFields.DocumentType,"SHCN");
            scData.Add(SCSapMessageFields.ITRNumber, "10");
            scData.Add(SCSapMessageFields.ReferenceITRNumber, "");
            scData.Add(SCSapMessageFields.FromLocation, list.PackingPlant);
            scData.Add(SCSapMessageFields.ToLocation, list.Consignee);
            scData.Add(SCSapMessageFields.CustomerReferenceNumber, list.SInumber);
            scData.Add(SCSapMessageFields.ItemNo, "10");
            scData.Add(SCSapMessageFields.MaterialNumber, list.BinType);
            scData.Add(SCSapMessageFields.Quantity, list.Quantity.ToString());
            scData.Add(SCSapMessageFields.ContainerNumber, "");
            scData.Add(SCSapMessageFields.CustomerNumber, list.CustomerName);
            scData.Add(SCSapMessageFields.SINumber, list.SInumber);
            scData.Add(SCSapMessageFields.SalesDocument, "");
            scData.Add(SCSapMessageFields.ETDDate, list.ETD);
            scData.Add(SCSapMessageFields.DTADate, list.ETA);
            scData.Add(SCSapMessageFields.Remarks, list.Remark);
            return scData;
        }
        private void UpdateSSCTransLineStatus(int lineId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            int lineStatusId = -1;
            string itrNumber=null;
            using (var context = new GoodpackEDIEntities())
            {
                var datatoUpdate = context.Trn_SSCTransLineNew.Where(i => i.LineNumber == lineId).ToList();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    if (responseModel[i].Success)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {

                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    if (responseModel[i].ResponseItems != null)
                        itrNumber = responseModel[i].ResponseItems.ToString();
                    else
                        itrNumber = "10";
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == lineStatus
                                    select s.Id).FirstOrDefault();
                    lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                  ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                }
                datatoUpdate.ForEach(a => { a.StatusId = lineStatusId; a.InTestMode = false; a.ItrNumber = itrNumber; });
                context.SaveChanges();
            }
        }
        public string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();
            //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GoodpackEDI.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
        public void UpdateSSCBatchFileData(SCTransactionModel transBtachDetails)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = transBtachDetails.StatusCode.ToString();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();

                Trn_SSCTransBatch scFileData = context.Trn_SSCTransBatch.FirstOrDefault(x => (x.Id == transBtachDetails.BatchID)); //&& x.StatusId != 10 && x.StatusId != 12 && x.StatusId != 13));//Give where condition in valuetype
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateUpdated = DateTime.Now;
                context.SaveChanges();
            }

        }
        private static string WriteDataTable(DataTable sourceTable)
        {
            StringBuilder s = new StringBuilder();
            List<string> headerValues = new List<string>();
            foreach (DataColumn column in sourceTable.Columns)
            {
                headerValues.Add(QuoteValue(column.ColumnName));
            }
            s.Append(String.Join(",", headerValues.ToArray()) + Environment.NewLine);
            string[] items = null;
            foreach (DataRow row in sourceTable.Rows)
            {
                items = row.ItemArray.Select(o => QuoteValue(o.ToString())).ToArray();
                s.Append(String.Join(",", items) + Environment.NewLine);
            }
            return s.ToString();
        }
        private static string QuoteValue(string value)
        {
            return String.Concat("\"", value.Replace("\"", "\"\""), "\"");
        }
        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public void NotifyGoodpackMailSend(string customerName, int batchId, string customerEmail, string fileName, int subsiId, string totalCount, string successCount, string errorCount, string mapperName)
        {
            Dictionary<string, string> placeholders = new Dictionary<string, string>();
            placeholders.Add("$$CustomerName$$", customerName);
            placeholders.Add("$$BatchID$$", batchId.ToString());
            placeholders.Add("$$UserName$$", customerName);
            placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
            placeholders.Add("$$EmailID$$", customerEmail);
            placeholders.Add("$$FileName$$", fileName);
            placeholders.Add("$$SubsyName$$", getSubsyNameFromId(subsiId));
            placeholders.Add("$$TotalCount$$", totalCount);
            placeholders.Add("$$SuccessCount$$", successCount);
            placeholders.Add("$$ErrorCount$$", errorCount);
            GPTools.SendEmail(placeholders, getAllRecipients(mapperName), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
        }
        public void NotifyCustomerMailSend(string customerName, int batchId, string customerEmail, string fileName, int subsiId, string totalCount, string successCount, string errorCount, string mapperName)
        {
            Dictionary<string, string> placeholders = new Dictionary<string, string>();
            placeholders.Add("$$CustomerName$$", customerName);
            placeholders.Add("$$BatchID$$", batchId.ToString());
            placeholders.Add("$$UserName$$", customerName);
            placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
            placeholders.Add("$$EmailID$$", customerEmail);
            placeholders.Add("$$FileName$$", fileName);
            placeholders.Add("$$SubsyName$$", getSubsyNameFromId(subsiId));
            placeholders.Add("$$TotalCount$$", totalCount);
            placeholders.Add("$$SuccessCount$$", successCount);
            placeholders.Add("$$ErrorCount$$", errorCount);
            string[] customerEmailArray = new string[] { customerEmail };
            GPTools.SendEmail(placeholders, customerEmailArray, "NOTIFY_CUSTOMER_SO_EMAIL", null);
        }
        public string getSubsyNameFromId(int SubsiId)
        {
            string SubsiName;
            using (var context = new GoodpackEDIEntities())
            {
                SubsiName = (from s in context.Gen_EDISubsies
                             where s.Id == SubsiId
                             select s.SubsiName).FirstOrDefault();
                
            }
            return SubsiName;
        }
    }
}