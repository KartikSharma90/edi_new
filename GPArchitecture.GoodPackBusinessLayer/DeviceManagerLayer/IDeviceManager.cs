﻿
using GPArchitecture.Models;
using System.Collections.Generic;

namespace GPArchitecture.GoodPackBusinessLayer.DeviceManagerLayer
{
    public interface IDeviceManager
    {
        List<DeviceList> GetDeviceList();
        ResponseModel CreateDevice(DeviceList detail, string userName);
        ResponseModel UpdateDevice(DeviceList detail, string userName);
        ResponseModel DeleteDevice(int Id, string userName);

        List<ShipmentResponse> ShipmentData(ShipmentData shipmentdata);
        bool DeviceRegistered(ShipmentData data);
        RegisterScannerResponse RegisterScanner(ShipmentData data);
        void SendEmailToGoodPackAdmin(string DeviceId);
        List<VerticalModel> GetVerticalData();
        bool ReSubmitErrorData(int BatchId);
        List<VerticalModel> GetDeviceTransactionType();
        List<VerticalModel> GetSubsyData();
   
    }
}
