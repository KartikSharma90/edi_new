﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.GoodPackBusinessLayer.SubsiEntityManagementLayer
{
    public class SubsiManager : ISubsiManager
    {
        //public IList<SubsiModel> GetAllSubsies()
        //{
        //    using (var context = new GoodpackEDIEntities())
        //    {

        //        return (from s in context.Gen_EDISubsies
        //                where s.IsEnabled == true
        //                select new SubsiModel
        //                {
        //                    SubsiId = s.Id,
        //                    SubsiName = s.SubsiName
        //                }).ToList();
        //    }

        //}

        public IList<SubsiModel> GetAllSubsies(string username)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();
                if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                {

                    return (from s in context.Trn_UserToSubsiMapper
                            join p in context.Gen_EDISubsies on s.SubsiId equals p.Id
                            where s.UserId == user.Id
                            orderby p.SubsiName
                            select new SubsiModel
                            {
                                SubsiId = p.Id,
                                SubsiName = p.SubsiName
                            }).ToList();
                }
                else
                {
                    return (from s in context.Gen_EDISubsies
                            where s.IsEnabled == true
                            select new SubsiModel
                            {
                                SubsiId = s.Id,
                                SubsiName = s.SubsiName
                            }).ToList();

                }
            }

        }

        public UserToSubsiModel GetSubsiData(int userId, bool isMapping)
        {
            using (var context = new GoodpackEDIEntities())
            {

                UserToSubsiModel userToSubsi = new UserToSubsiModel();
                IList<SubsiModel> subsies = new List<SubsiModel>();
                if (!isMapping)
                {

                    subsies = (from item in context.Gen_EDISubsies

                               where !(from p in context.Trn_UserToSubsiMapper
                                       where p.UserId == userId
                                       select p.SubsiId).Contains(item.Id)
                               select new SubsiModel
                               {
                                   SubsiId = item.Id,
                                   SubsiName = item.SubsiName
                               }).ToList();
                }

                IList<SubsiModel> mappedSubsies =
                                                 (from s in context.Trn_UserToSubsiMapper
                                                  join p in context.Gen_EDISubsies on s.SubsiId equals p.Id
                                                  where s.UserId == userId
                                                  orderby p.SubsiName
                                                  select new SubsiModel
                                                  {
                                                      SubsiId = p.Id,
                                                      SubsiName = p.SubsiName
                                                  }).ToList();
                userToSubsi.SubsiList = subsies;
                userToSubsi.MappedSubsi = mappedSubsies;
                return userToSubsi;
            }
        }

        /// <summary>
        /// Get the previleges of user
        /// </summary>
        /// <param name="roleId">Username</param>
        /// <returns></returns>
        public ResponseModel MapUserToSubsi(List<SubsiModel> subsiModel, int userId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                int userRoleId = (from s in context.Gen_User
                                  where s.Id == userId
                                  select s.RoleId).FirstOrDefault();
                string role = (from s in context.Ref_UserRoles
                               where s.Id == userRoleId
                               select s.UserRole).FirstOrDefault();

                if (((role == ConstantUtility.SubsiUser) && (subsiModel.Count == 1)) || (role != ConstantUtility.SubsiUser))
                {


                    IList<Trn_UserToSubsiMapper> subsiMapper = (from s in context.Trn_UserToSubsiMapper
                                                                where s.UserId == userId
                                                                select s).ToList();


                    foreach (var item in subsiMapper)
                    {
                        context.Trn_UserToSubsiMapper.Remove(item);
                        context.SaveChanges();
                    }

                    foreach (var items in subsiModel)
                    {
                        Trn_UserToSubsiMapper subsiMapperItem = new Trn_UserToSubsiMapper();
                        subsiMapperItem.SubsiId = items.SubsiId;
                        subsiMapperItem.UserId = userId;
                        context.Trn_UserToSubsiMapper.Add(subsiMapperItem);
                        context.SaveChanges();
                    }
                }
                else
                {
                    return new ResponseModel { Success = false, Message = "Subsi user cannot be mapped with more than one subsi" };
                }
            }
            return new ResponseModel { Success = true };
        }

    }
}