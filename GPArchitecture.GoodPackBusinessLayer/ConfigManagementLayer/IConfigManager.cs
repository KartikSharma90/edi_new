﻿using System.Collections.Generic;
using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.ConfigManagementLayer
{
    public interface IConfigManager
    {
        IList<ConfigDetailsModel> GetConfigDetails();
        ConfigDetailsModel AddConfigDetails(ConfigDetailsModel addConfigDetailsModel, string username);
        bool UpdateConfigDetails(ConfigDetailsModel editConfigDetailsModel, string username);
        bool DeleteConfigDetails(int configId);

    }
}