﻿using GPArchitecture.EnumsAndConstants;
using System.Collections.Generic;
using System.Linq;

namespace GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer
{
    public class TirmTripManager : ITirmTripManager
    {
        public bool CheckMultipleTripExist(List<Dictionary<string, string>> dataValue)
        {
            List<string> listCustomerRefall = new List<string>();
            List<string> listCustomerRefDist = new List<string>();
            List<int> listIstermTripResponse = new List<int>();
            foreach(Dictionary<string,string> dictionary in dataValue)
            {
                listCustomerRefall.Add(dictionary[SCSapMessageFields.CustomerNumber]);
            }
            listCustomerRefDist = listCustomerRefall.Distinct().ToList();
            foreach(string s in  listCustomerRefDist)
            {
                TermTripServiceManager termTripService = new TermTripServiceManager();
                listIstermTripResponse.Add(termTripService.CheckTermTripOrNot(s));
            }
            return listIstermTripResponse.Distinct().ToList().Count() > 1 ? true : false;
        }
        public bool CheckValidCustomer(List<Dictionary<string, string>> dataValue)
        { 
            List<string> listCustomerRefall = new List<string>();
            List<string> listCustomerRefDist = new List<string>();
            List<int> listIstermTripResponse = new List<int>();
            foreach (Dictionary<string, string> dictionary in dataValue)
            {
                listCustomerRefall.Add(dictionary[SCSapMessageFields.CustomerNumber]);
            }
            listCustomerRefDist = listCustomerRefall.Distinct().ToList();
            foreach (string s in listCustomerRefDist)
            {
                TermTripServiceManager termTripService = new TermTripServiceManager();
                listIstermTripResponse.Add(termTripService.CheckTermTripOrNot(s));
            }
            return (!listIstermTripResponse.Contains(3));
        }
        public bool IsTirmTripOrNot(Dictionary<string, string> dataValue)
        {            
            TermTripServiceManager termTripService = new TermTripServiceManager();
            return termTripService.CheckTermTripOrNot(dataValue[SCSapMessageFields.CustomerNumber]) == 1 ? true :false ;
        }
        public bool IsTirmTripORNot(string CustomerCode)
        {
            TermTripServiceManager termTripService = new TermTripServiceManager();
            return termTripService.CheckTermTripOrNot(CustomerCode) == 1 ? true : false;
        }
    }
}