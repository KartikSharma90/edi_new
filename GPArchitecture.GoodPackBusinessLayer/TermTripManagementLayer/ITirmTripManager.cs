﻿using System.Collections.Generic;

namespace GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer
{
    public interface ITirmTripManager
    {
        bool CheckMultipleTripExist(List<Dictionary<string, string>> dataValue);
        bool CheckValidCustomer(List<Dictionary<string, string>> dataValue);
        bool IsTirmTripOrNot(Dictionary<string, string> dataValue);
        bool IsTirmTripORNot(string CustomerCode);
    }
}
