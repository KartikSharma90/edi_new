﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Collections;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using GoodpackEDI.ViewNewSOServiceReference;  //QA pointing, 
//using GoodpackEDI.ServiceReference_SOEditThreeReasons;    //QA pointing,
using GPArchitecture.DataLayer.Models;
using log4net;
using System.Web.Configuration;
using GPArchitecture.Models;



namespace GPArchitecture.GoodPackBusinessLayer.SOManagerLayer
{
    public class SalesOrderManager : ISalesOrderManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SalesOrderManager));
        List<SOResponseModel> massResponseModel = new List<SOResponseModel>();
        List<ResponseModel> responseList;

        SOResponseModel soResponse = new SOResponseModel();
        public List<SalesOrderModel> SalesOrderData(string startDate, string endDate, string customerNumbr)
        {
            List<SalesOrderModel> salesDataNw = new List<SalesOrderModel>();

            zws_cust_so_historyClient client = new zws_cust_so_historyClient();           
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ViewSalesOrderServiceReference"]);

            client.ClientCredentials.UserName.UserName = Utilities.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = Utilities.ConstantUtilities.SAPPassword;
            GoodpackEDI.ViewSalesOrderServiceReference.ZcustSoHistory reqObj = new GPArchitecture.SAPService.ViewSalesOrderServiceReference.ZcustSoHistory();
            reqObj.PKunnr = customerNumbr;
            reqObj.PFsietd = startDate;
            reqObj.PTsietd = endDate;
            reqObj.ZsdSoWsdl = new GPArchitecture.SAPService.ViewSalesOrderServiceReference.ZsdSoWsdl[0];

            ZcustSoHistoryResponse response = client.ZcustSoHistory(reqObj);

            salesDataNw = (from salsNw in response.ZsdSoWsdl
                           select new SalesOrderModel
                           {
                               cntryKey = salsNw.Land1,
                               congName = salsNw.Cons,
                               congCustNum = salsNw.ConsC,
                               reg = salsNw.Regio,
                               pkrName = salsNw.Packer,
                               packCustNum = salsNw.PackC,
                               matNm = salsNw.Matnr,
                               custPrchOrdNo = salsNw.Bstkd,
                               schLneDte = salsNw.Edatu,
                               leadDys = salsNw.Leaddays,
                               itrCnfDte = salsNw.Zcrdate,
                               plndWk = salsNw.Plweek,
                               cuOrdQty = salsNw.Kwmeng,
                               itrCnfQty = salsNw.Zcrqty,
                               dte = salsNw.Wkstrt
                           }).ToList();

            return salesDataNw;

        }
        public List<SalesOrderModel> NewSalesOrderData(string startDate, string endDate, string customerNumbr)
        {
            List<SalesOrderModel> salesDataNw = new List<SalesOrderModel>();

            // zws_cust_so_historyClient client = new zws_cust_so_historyClient();
            zws_cust_so_history_nrrClient client = new zws_cust_so_history_nrrClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ViewNewSOServiceReference"]);

            client.ClientCredentials.UserName.UserName = Utilities.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = Utilities.ConstantUtilities.SAPPassword;
            GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory reqObj = new GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory();
            reqObj.PKunnr = customerNumbr;
            reqObj.PFsietd = startDate;
            reqObj.PTsietd = endDate;
            reqObj.ZsdSoWsdl = new GoodpackEDI.ViewNewSOServiceReference_PROD.ZsdSoWsdl[0];

            GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse response = client.ZcustSoHistory(reqObj);
          
            salesDataNw = (from salsNw in response.ZsdSoWsdl
                           select new SalesOrderModel
                           {
                               cntryKey = salsNw.Land1,
                               congName = salsNw.Cons,
                               congCustNum = salsNw.ConsC,
                               reg = salsNw.Regio,
                               pkrName = salsNw.Packer,
                               packCustNum = salsNw.PackC,
                               matNm = salsNw.Matnr,
                               custPrchOrdNo = salsNw.Bstkd,
                               schLneDte = salsNw.Edatu,
                               leadDys = salsNw.Leaddays,
                               itrCnfDte = salsNw.Zcrdate,
                               plndWk = salsNw.Plweek,
                               cuOrdQty = salsNw.Kwmeng,
                               itrCnfQty = salsNw.Zcrqty,
                               dte = salsNw.Wkstrt,
                               vbeln = salsNw.Vbeln,
                               vdatu = salsNw.Vdatu
                           }).ToList();

            return salesDataNw;

        }
        public List<SalesOrderModel> getReport(string xmlFile)
        {
            //Parse XML data and returned to caller
            XDocument xdoc = XDocument.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XMLFiles/SalesOrderDeliveryReport.xml"));
            List<SalesOrderModel> sales = (from sals in xdoc.Element("SalesOrderReport").Elements("SalesOrder")
                                           select new SalesOrderModel
                                    {

                                        cntryKey = sals.Element("PackerCntry").Value,
                                        congName = sals.Element("Consignee").Value,
                                        reg = sals.Element("CustomerConsignee").Value,
                                        packCustNum = sals.Element("PackerRegion").Value,
                                        pkrName = sals.Element("PackerName").Value,
                                        matNm = sals.Element("Sku").Value,
                                        custPrchOrdNo = sals.Element("CustomerPONO").Value,
                                        schLneDte = sals.Element("SIETD").Value,
                                        //leadDys = sals.Element("LoadDays").Value,
                                        plndWk = sals.Element("Received").Value

                                    }).ToList();
            return sales;
        }

       
        public List<SOResponseModel> EditSO(string username, ArrayList rowDetailsArr)
        {

            log.Info("SO Service calling using new order. SO Data below");
            log.Info(rowDetailsArr);
            string reasonCode1;
            string reasonCode2;
            string reasonCode3;
           // var newOrder = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());
            bool IsNewOrder = Convert.ToBoolean(rowDetailsArr[0].ToString());
            string reason1 = Convert.ToString(rowDetailsArr[1].ToString());
            string reason2 = Convert.ToString(rowDetailsArr[2].ToString());
            string reason3 = Convert.ToString(rowDetailsArr[3].ToString());

         if (!IsNewOrder)
         {
            
             reasonCode1 = "";
             reasonCode2 = "";
             reasonCode3 = "";
            using (var context = new GoodpackEDIEntities())
            {
                if (reason1.ToString() != "0")
            {
                int reasonId1 = int.Parse(reason1.ToString());
                reasonCode1 = (from s in context.Ref_SOEditReasonCodes
                               where s.Id == reasonId1
                               select s.ReasonCode).FirstOrDefault();
            }
                if (reason2.ToString() != "0")
            {
                int reasonId2 = int.Parse(reason2.ToString());
                reasonCode2 = (from s in context.Ref_SOEditReasonCodes
                               where s.Id == reasonId2
                               select s.ReasonCode).FirstOrDefault();
            }
                if (reason3.ToString() != "0")
            {
                int reasonId3 = int.Parse(reason3.ToString());
                reasonCode3 = (from s in context.Ref_SOEditReasonCodes
                               where s.Id == reasonId3
                               select s.ReasonCode).FirstOrDefault();
            }                                                           
            }
         }
            else
            {
                reasonCode1 = reason1.ToString();
                reasonCode2 = reason2.ToString();
                reasonCode3 = reason3.ToString();
            }

            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);
          
            IEntityManager entityManager = new EntityManager();
            int entityCode = int.Parse(((string[])rowDetailsArr[0])[0]);
            string subsiCode = entityManager.GetSubsiforEntity(entityCode);

            Zws_BAPI_SO_EDI_changeClient client = new Zws_BAPI_SO_EDI_changeClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_SOEditThreeReasons"]);
            client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

             ZediSoFormat[] salesDataArray = new ZediSoFormat[rowDetailsArr.Count];
             try
             {
                 log.Info("Edit data service with data-" + rowDetailsArr);
             }
             catch (Exception e)
             {

             }
            for (int i = 0; i < rowDetailsArr.Count; i++)
            {

                salesDataArray[i] = new ZediSoFormat();
                string date = ((string[])rowDetailsArr[i])[2];
                string bstkd = ((string[])rowDetailsArr[i])[5];
                string sokunnr = ((string[])rowDetailsArr[i])[0];
                string matnr = ((string[])rowDetailsArr[i])[3];
                string packer = ((string[])rowDetailsArr[i])[1];
                string kwmeng= ((string[])rowDetailsArr[i])[4];
                salesDataArray[i].Vkorg = subsiCode;
                salesDataArray[i].Vtweg = "00";
                salesDataArray[i].Spart = "00";
                salesDataArray[i].Vkbur = subsiCode;
                salesDataArray[i].Bstkd = bstkd.ToString();
                salesDataArray[i].Bstdk = date.ToString();
                salesDataArray[i].Sokunnr = "0000" + sokunnr.ToString();  /*"0000301557"*/;
                salesDataArray[i].Vdatu = date.ToString();
                salesDataArray[i].Posnr = "000010";
                salesDataArray[i].Matnr = matnr.ToString();
                salesDataArray[i].Packer = packer.ToString();/*"0000500011"*/;
                salesDataArray[i].Edatu = date.ToString();
                salesDataArray[i].Kwmeng =kwmeng.ToString();
                salesDataArray[i].Zzveta = date.ToString();
                salesDataArray[i].Zzvetd = date.ToString();
                salesDataArray[i].Htext = username;
                salesDataArray[i].ZqtyChange = reasonCode2;
                salesDataArray[i].ZetdChange = reasonCode3;
                salesDataArray[i].ZskuChange = reasonCode1;

            } 
            try
            {

                ZbapiSoEdi salesData = new ZbapiSoEdi();
                salesData.Result = new ZediSoError[0];
                salesData.ZediSoFormat = salesDataArray;
                ZbapiSoEdiResponse response = client.ZbapiSoEdi(salesData);
                ZediSoError[] soError = response.Result;
                for (int i = 0; i < soError.Length; i++)
                {
                    SOResponseModel ResponseModel = new SOResponseModel();
                    ResponseModel.ItemNumber = soError[i].Itemno;
                    ResponseModel.RowNumber = soError[i].Row;
                    ResponseModel.PONumber = soError[i].Bstkd;
                    ResponseModel.MessageType = soError[i].Msgty;
                    ResponseModel.MessageContent = soError[i].Msg;
                    massResponseModel.Add(ResponseModel);

                }
                
                
            }
            catch (Exception exc)
            {
                TrackEditSO(rowDetailsArr,reasonCode1,reasonCode2,reasonCode3,massResponseModel, username, IsNewOrder ,true, exc.Message);
                throw exc;
            }

           TrackEditSO(rowDetailsArr,reasonCode1,reasonCode2,reasonCode3, massResponseModel, username, IsNewOrder, false, "");

           return massResponseModel;
        }
        private void TrackEditSO(ArrayList rowDetailsArr, string reasonCode1, string reasonCode2, string reasonCode3, List<SOResponseModel> listResponseModel, string username, bool IsNewOrder, bool IsException, string exceptionMessage)
        {
            int orderType;
            if (IsException)
            {
                using (var context = new GoodpackEDIEntities())
                {
                    orderType = (from s in context.Ref_SalesOrderType where s.Type == "Edit" select s.Id).FirstOrDefault();
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,

                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    for (int i = 0; i < rowDetailsArr.Count; i++)
                    {
                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = ((string[])rowDetailsArr[i])[0],
                                DeliveryLocation = ((string[])rowDetailsArr[i])[1],
                                DeliveryDate = ((string[])rowDetailsArr[i])[2],
                                SKU = ((string[])rowDetailsArr[i])[3],
                                Quantity = ((string[])rowDetailsArr[i])[4],
                                CustomerPONumber = ((string[])rowDetailsArr[i])[5],
                                ReasonCode1 = reasonCode1,
                                ReasonCode2 = reasonCode2,
                                ReasonCode3 = reasonCode3,
                                ReasonCode4 = "",

                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();

                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = "Exception",
                                SAPResponseMessage = exceptionMessage,
                                SODetailsId = soTrackingDetails.Id
                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }
                        catch (Exception exc)
                        {

                        }

                    }
                }

            }
            else
            {
                using (var context = new GoodpackEDIEntities())
                {
                    orderType = (from s in context.Ref_SalesOrderType where s.Type == "Edit" select s.Id).FirstOrDefault();
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,
                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    for (int i = 0; i < rowDetailsArr.Count; i++)
                    {

                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = ((string[])rowDetailsArr[i])[0],
                                DeliveryLocation = ((string[])rowDetailsArr[i])[1],
                                DeliveryDate = ((string[])rowDetailsArr[i])[2],
                                SKU = ((string[])rowDetailsArr[i])[3],
                                Quantity = ((string[])rowDetailsArr[i])[4],
                                CustomerPONumber = ((string[])rowDetailsArr[i])[5],
                                ReasonCode1 = reasonCode1,
                                ReasonCode2 = reasonCode2,
                                ReasonCode3 = reasonCode3,
                                ReasonCode4 = "",
                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();
                            keyValue.Add(soTrackingDetails.CustomerPONumber, soTrackingDetails.Id);
                        }

                        catch (Exception exc)
                        {

                        }
                    }

                    foreach (SOResponseModel soResponseModel in listResponseModel)
                    {
                        try
                        {

                            int detailsId = keyValue[soResponseModel.PONumber];
                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = soResponseModel.MessageType,
                                SAPResponseMessage = soResponseModel.MessageContent,
                                SODetailsId = detailsId

                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }

                        catch (Exception exc)
                        {

                        }
                    }
                }
            }
        }
        public List<SOResponseModel> postSO(string username, ArrayList rowDetailsArr)
        {

            log.Info("SO Service calling using new order. SO Data below");
            log.Info(rowDetailsArr);
            string reasonCode1;
            string reasonCode2;
            string reasonCode3;
            var newOrder = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());
            bool IsNewOrder = Convert.ToBoolean(newOrder[0].ToString());
            var reason1 = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[1].ToString());
            var reason2 = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[2].ToString());
            var reason3 = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[3].ToString());

            if (!IsNewOrder)
            {

                reasonCode1 = "";
                reasonCode2 = "";
                reasonCode3 = "";
                using (var context = new GoodpackEDIEntities())
                {
                    if (reason1.ToString() != "0")
                    {
                        int reasonId1 = int.Parse(reason1.ToString());
                        reasonCode1 = (from s in context.Ref_SOEditReasonCodes
                                       where s.Id == reasonId1
                                       select s.ReasonCode).FirstOrDefault();
                    }
                    if (reason2.ToString() != "0")
                    {
                        int reasonId2 = int.Parse(reason2.ToString());
                        reasonCode2 = (from s in context.Ref_SOEditReasonCodes
                                       where s.Id == reasonId2
                                       select s.ReasonCode).FirstOrDefault();
                    }
                    if (reason3.ToString() != "0")
                    {
                        int reasonId3 = int.Parse(reason3.ToString());
                        reasonCode3 = (from s in context.Ref_SOEditReasonCodes
                                       where s.Id == reasonId3
                                       select s.ReasonCode).FirstOrDefault();
                    }                                               
                }
            }
            else
            {
                reasonCode1 = "";
                reasonCode2 = "";
                reasonCode3 = "";
            }

            //  string reasonCode = reason[0].ToString();
            // int reasonCode = int.Parse(reason[0].ToString());
            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);
            rowDetailsArr.RemoveAt(0);

            var parsedObjectfirstRow = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());
            IEntityManager entityManager = new EntityManager();
            int entityCode = int.Parse(parsedObjectfirstRow[0].ToString());
            string subsiCode = entityManager.GetSubsiforEntity(entityCode);


            // zws_so_edi_create1Client client = new zws_so_edi_create1Client();
            // Zws_BAPI_SO_EDI_reasonClient client = new Zws_BAPI_SO_EDI_reasonClient();
            Zws_BAPI_SO_EDI_changeClient client = new Zws_BAPI_SO_EDI_changeClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_SOEditThreeReasons"]);
            client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

            ZediSoFormat[] salesDataArray = new ZediSoFormat[rowDetailsArr.Count];

            for (int i = 0; i < rowDetailsArr.Count; i++)
            {

                salesDataArray[i] = new ZediSoFormat();

                var parsedObject = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[i].ToString());
                var date = parsedObject[2];

                salesDataArray[i].Vkorg = subsiCode;
                salesDataArray[i].Vtweg = "00";
                salesDataArray[i].Spart = "00";
                salesDataArray[i].Vkbur = subsiCode;
                salesDataArray[i].Bstkd = parsedObject[5].ToString();
                salesDataArray[i].Bstdk = date.ToString();
                salesDataArray[i].Sokunnr = "0000" + parsedObject[0].ToString();/*"0000301557"*/;
                salesDataArray[i].Vdatu = date.ToString();
                salesDataArray[i].Posnr = "000010";
                salesDataArray[i].Matnr = parsedObject[3].ToString();
                salesDataArray[i].Packer = parsedObject[1].ToString();/*"0000500011"*/;
                salesDataArray[i].Edatu = date.ToString();
                salesDataArray[i].Kwmeng = parsedObject[4].ToString();
                salesDataArray[i].Zzveta = date.ToString();
                salesDataArray[i].Zzvetd = date.ToString();
                salesDataArray[i].Htext = username;
                salesDataArray[i].ZqtyChange = reasonCode2;
                salesDataArray[i].ZetdChange = reasonCode3;
                salesDataArray[i].ZskuChange = reasonCode1;
                

            }
            List<SOResponseModel> listResponseModel = new List<SOResponseModel>();
            //  List<SOResponseModel> listMassResponseModel;
            try
            {

                ZbapiSoEdi salesData = new ZbapiSoEdi();
                salesData.Result = new ZediSoError[0];
                salesData.ZediSoFormat = salesDataArray;
                ZbapiSoEdiResponse response = client.ZbapiSoEdi(salesData);
                ZediSoError[] soError = response.Result;
                for (int i = 0; i < soError.Length; i++)
                {
                    SOResponseModel ResponseModel = new SOResponseModel();
                    ResponseModel.ItemNumber = soError[i].Itemno;
                    ResponseModel.RowNumber = soError[i].Row;
                    ResponseModel.PONumber = soError[i].Bstkd;
                    ResponseModel.MessageType = soError[i].Msgty;
                    ResponseModel.MessageContent = soError[i].Msg;
                    listResponseModel.Add(ResponseModel);

                }


            }
            catch (Exception exc)
            {
                TrackSO(rowDetailsArr, reasonCode1, reasonCode2, reasonCode3, listResponseModel, username, IsNewOrder, true, exc.Message);
                // TrackSO(rowDetailsArr, listResponseModel, username, IsNewOrder, true, exc.Message);
                throw exc;
            }

            TrackSO(rowDetailsArr, reasonCode1, reasonCode2, reasonCode3, listResponseModel, username, IsNewOrder, false, "");
            // TrackSO(rowDetailsArr, listResponseModel, username, IsNewOrder, false, "");

            Gen_User userDetails;
            using (var dataContext = new GoodpackEDIEntities())
            {
                userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
            }

            Dictionary<string, string> placeholders = new Dictionary<string, string>();
            placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
            string[] recipientList = new string[] { userDetails.EmailId };
            GPTools.SendEmail(placeholders, recipientList, "NOTIFY_SO_CUSTOMER", null);

            return listResponseModel;
        }
        private void TrackSO(ArrayList rowDetailsArr, string reasonCode1, string reasonCode2, string reasonCode3, List<SOResponseModel> listResponseModel, string username, bool IsNewOrder, bool IsException, string exceptionMessage)
        {
            int orderType;
            if (IsException)
            {
                using (var context = new GoodpackEDIEntities())
                {
                    orderType = (from s in context.Ref_SalesOrderType where s.Type == "Add" select s.Id).FirstOrDefault();
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,

                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    foreach (JArray detailslist in rowDetailsArr)
                    {
                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = detailslist[0].ToString(),
                                DeliveryLocation = detailslist[1].ToString(),
                                DeliveryDate = detailslist[2].ToString(),
                                SKU = detailslist[3].ToString(),
                                Quantity = detailslist[4].ToString(),
                                CustomerPONumber = detailslist[5].ToString(),
                                ReasonCode1 = reasonCode1,
                                ReasonCode2 = reasonCode2,
                                ReasonCode3 = reasonCode2,
                                ReasonCode4 = "",

                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();

                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = "Exception",
                                SAPResponseMessage = exceptionMessage,
                                SODetailsId = soTrackingDetails.Id
                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }
                        catch (Exception exc)
                        {

                        }

                    }
                }

            }
            else
            {
                using (var context = new GoodpackEDIEntities())
                {
                    orderType = (from s in context.Ref_SalesOrderType where s.Type == "Add" select s.Id).FirstOrDefault();
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,
                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    foreach (JArray detailslist in rowDetailsArr)
                    {

                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = detailslist[0].ToString(),
                                DeliveryLocation = detailslist[1].ToString(),
                                DeliveryDate = detailslist[2].ToString(),
                                SKU = detailslist[3].ToString(),
                                Quantity = detailslist[4].ToString(),
                                CustomerPONumber = detailslist[5].ToString(),
                                ReasonCode1 = reasonCode1,
                                ReasonCode2 = reasonCode2,
                                ReasonCode3 = reasonCode3,
                                ReasonCode4 = "",
                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();
                            keyValue.Add(soTrackingDetails.CustomerPONumber, soTrackingDetails.Id);
                        }

                        catch (Exception exc)
                        {

                        }
                    }

                    foreach (SOResponseModel soResponseModel in listResponseModel)
                    {
                        try
                        {

                            int detailsId = keyValue[soResponseModel.PONumber];
                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = soResponseModel.MessageType,
                                SAPResponseMessage = soResponseModel.MessageContent,
                                SODetailsId = detailsId

                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }

                        catch (Exception exc)
                        {

                        }
                    }
                }
            }
        }

        public List<SOResponseModel> massEditSO(string username, string cusCode, List<string> customerPNos, List<string> deliveryLocations, List<string> deliveryDates, List<string> matNos, List<string> orderQtys, string materialNo, int orderQty, string deliveryDt, List<string> reason1, List<string> reason2, List<string> reason3)
        {
            List<SOResponseModel> listResponseModel = new List<SOResponseModel>();
            SOResponseModel ResponseModel = new SOResponseModel();
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < customerPNos.Count; i++)
                {
                    string matNo = materialNo;
                    string ddate = deliveryDt;
                    int qty = orderQty;
                    string dateVal = deliveryDates[i].Substring(8, 2);
                    string monthVal = deliveryDates[i].Substring(5, 2);
                    string yearVal = deliveryDates[i].Substring(0, 4);
                    deliveryDates[i] = dateVal + "." + monthVal + "." + yearVal;
                    Trn_SOTrackingDetails SOline = new Trn_SOTrackingDetails();
                    List<Trn_SOTrackingDetails> SOlines = new List<Trn_SOTrackingDetails>();
                    string custpoNo = customerPNos[i].ToString();
                    string delLocation = deliveryLocations[i].ToString();
                   // string cusCode = customerCode;
                    //string cusCode=(from s in context.Trn_SOTrackingDetails
                    //          where s.DeliveryLocation==delLocation
                    //          select s.CustomerCode).FirstOrDefault();
                    //int counts;
                    //counts = (from p in context.Trn_SOTrackingDetails
                    //          where p.CustomerPONumber == custpoNo
                    //          select p).Count();
                    //if (counts > 1)
                    //{
                    //    SOlines = (from s in context.Trn_SOTrackingDetails
                    //               where s.CustomerPONumber == custpoNo
                    //               select s).ToList();
                    //    SOline = SOlines.Last();

                    //}
                    //else
                    //{
                    //SOline = (from s in context.Trn_SOTrackingDetails
                    //          where s.CustomerPONumber == custpoNo 
                    //          select s).FirstOrDefault();
                    //}
                    ArrayList rowDetails = new ArrayList();
                    rowDetails.Add("false");
                    rowDetails.Add(reason1[i]);
                    rowDetails.Add(reason2[i]);
                    rowDetails.Add(reason3[i]);
                    if (matNo == "" && ddate != "" && qty.ToString() != "0")
                    {
                        matNo = matNos[i];
                    }
                    else if (matNo != "" && ddate == "" && qty.ToString() != "0")
                    {
                        ddate = deliveryDates[i];
                    }
                    else if (matNo != "" && ddate != "" && qty.ToString() == "0")
                    {
                        qty = int.Parse(orderQtys[i]);
                    }
                    else if (matNo == "" && ddate == "" && qty.ToString() != "0")
                    {
                        matNo = matNos[i];
                        ddate = deliveryDates[i];
                    }
                    else if (matNo != "" && ddate == "" && qty.ToString() == "0")
                    {
                        ddate = deliveryDates[i];
                        qty = int.Parse(orderQtys[i]);
                    }
                    else if (matNo == "" && ddate != "" && qty.ToString() == "0")
                    {
                        matNo = matNos[i];
                        qty = int.Parse(orderQtys[i]);
                    }
                    string[] a1 = { cusCode, delLocation, ddate, matNo, qty.ToString(), custpoNo };
                    rowDetails.Add(a1);
                    listResponseModel = EditSO(username, rowDetails);
                }

                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }
                //Dictionary<string, string> placeholders = new Dictionary<string, string>();
                //placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                //string[] recipientList = new string[] { userDetails.EmailId };
                //GPTools.SendEmail(placeholders, recipientList, "NOTIFY_SO_CUSTOMER", null);

                return listResponseModel;
            }
        }
        public List<SOResponseModel> massCancelSO(string username, CancelSOModel SOModel)
        {
            List<SOResponseModel> listResponseModel = new List<SOResponseModel>();
            SOResponseModel ResponseModel = new SOResponseModel();
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < SOModel.LineContent.Count; i++)
                {                   
                    string custpoNo = SOModel.LineContent[i].customerPONos.ToString();
                    string matNo = SOModel.LineContent[i].materialNos.ToString();
                    string orderQty = SOModel.LineContent[i].orderQtys.ToString();
                    string delDate = SOModel.LineContent[i].deliveryDates.ToString();
                    string delLoc = SOModel.LineContent[i].deliveryLocations.ToString();
                    string reason1 = SOModel.reason1.ToString();
                    //string customerCode= (from s in context.Trn_SOTrackingDetails
                    //          where s.DeliveryLocation==delLoc
                    //          select s.CustomerCode).FirstOrDefault();
                    string customerCode = SOModel.customerCode;
                    ArrayList rowDetails = new ArrayList();
                    rowDetails.Add(reason1);
                    string dateVal = delDate.Substring(8, 2);
                    string monthVal = delDate.Substring(5, 2);
                    string yearVal = delDate.Substring(0, 4);
                    delDate = dateVal + "." + monthVal + "." + yearVal;
                    string[] a1 = {customerCode, delLoc, delDate, matNo,orderQty,custpoNo };
                    rowDetails.Add(a1);
                    listResponseModel = CancelSO(username, rowDetails);
                }

                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                string[] recipientList = new string[] { userDetails.EmailId };
                GPTools.SendEmail(placeholders, recipientList, "NOTIFY_SO_CUSTOMER", null);

                return listResponseModel;
            }
        }
        private void TrackCancelSO(ArrayList rowDetailsArr, string reasonCode4, List<SOResponseModel> listResponseModel, string username, int orderType, bool IsException, string exceptionMessage)
        {

            if (IsException)
            {
                using (var context = new GoodpackEDIEntities())
                {
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,

                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    for (int i = 0; i < rowDetailsArr.Count; i++)
                    {
                        log.Info("Tracking Sales Order Details with data:" + rowDetailsArr);
                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = ((string[])rowDetailsArr[i])[0],
                                DeliveryLocation = ((string[])rowDetailsArr[i])[1],
                                DeliveryDate = ((string[])rowDetailsArr[i])[2],
                                SKU = ((string[])rowDetailsArr[i])[3],
                                Quantity = ((string[])rowDetailsArr[i])[4],
                                CustomerPONumber = ((string[])rowDetailsArr[i])[5],
                                ReasonCode1 = "",
                                ReasonCode2 = "",
                                ReasonCode3 = "",
                                ReasonCode4 = reasonCode4,

                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();

                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = "Exception",
                                SAPResponseMessage = exceptionMessage,
                                SODetailsId = soTrackingDetails.Id
                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }
                        catch (Exception exc)
                        {
                            log.Error("Error encountered in the method:TrackCancelSO. Exception Message:" + exc.Message);
                        }

                    }
                }

            }
            else
            {
                using (var context = new GoodpackEDIEntities())
                {
                    var soTracking = new Trn_SOTracking()
                    {
                        Username = username,
                        OrderType = orderType,
                        TransactionDate = DateTime.Now,
                    };
                    context.Trn_SOTracking.Add(soTracking);
                    context.SaveChanges();

                    Dictionary<string, int> keyValue = new Dictionary<string, int>();

                    for (int i = 0; i < rowDetailsArr.Count; i++)
                    {
                        log.Info("Tracking Sales Order Details with data:" + rowDetailsArr);
                        try
                        {
                            var soTrackingDetails = new Trn_SOTrackingDetails()
                            {
                                SOTrackId = soTracking.Id,
                                CustomerCode = ((string[])rowDetailsArr[i])[0],
                                DeliveryLocation = ((string[])rowDetailsArr[i])[1],
                                DeliveryDate = ((string[])rowDetailsArr[i])[2],
                                SKU = ((string[])rowDetailsArr[i])[3],
                                Quantity = ((string[])rowDetailsArr[i])[4],
                                CustomerPONumber = ((string[])rowDetailsArr[i])[5],
                                ReasonCode1 = "",
                                ReasonCode2 = "",
                                ReasonCode3 = "",
                                ReasonCode4 = reasonCode4,
                            };
                            context.Trn_SOTrackingDetails.Add(soTrackingDetails);
                            context.SaveChanges();
                            keyValue.Add(soTrackingDetails.CustomerPONumber, soTrackingDetails.Id);
                        }

                        catch (Exception exc)
                        {
                            log.Error("Error encountered in the method:TrackCancelSO. Exception Message:" + exc.Message);
                        }
                    }

                    foreach (SOResponseModel soResponseModel in listResponseModel)
                    {
                        try
                        {

                            int detailsId = keyValue[soResponseModel.PONumber];
                            var sapResponse = new Trn_SOTrackingSAPResponse()
                            {
                                SOTrackId = soTracking.Id,
                                SAPMessageType = soResponseModel.MessageType,
                                SAPResponseMessage = soResponseModel.MessageContent,
                                SODetailsId = detailsId

                            };
                            context.Trn_SOTrackingSAPResponse.Add(sapResponse);
                            context.SaveChanges();
                        }

                        catch (Exception exc)
                        {
                            log.Error("Error encountered in the method:TrackCancelSO. Exception Message:" + exc.Message);
                        }
                    }
                }
            }
        }

        public List<SOResponseModel> CancelSO(string username, ArrayList rowDetailsArr)
        {

            log.Info("SO Service calling using cancel Sales order. SO Data below");
            log.Info(rowDetailsArr);
            string reasonCode4;
            // var newOrder = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());
            string reason1 = Convert.ToString(rowDetailsArr[0].ToString());
            int orderType;

            reasonCode4 = "";
            using (var context = new GoodpackEDIEntities())
            {
                orderType = (from s in context.Ref_SalesOrderType where s.Type == "Cancel" select s.Id).FirstOrDefault();
                if (reason1.ToString() != "0")
                {
                    int reasonId1 = int.Parse(reason1.ToString());
                    reasonCode4 = (from s in context.Ref_SOEditReasonCodes
                                   where s.Id == reasonId1
                                   select s.ReasonCode).FirstOrDefault();
                    log.Info("Reason for Cancel:" + reasonCode4);
                }

            }

            rowDetailsArr.RemoveAt(0);

            IEntityManager entityManager = new EntityManager();
            int entityCode = int.Parse(((string[])rowDetailsArr[0])[0]);
            string subsiCode = entityManager.GetSubsiforEntity(entityCode);

            Zws_BAPI_SO_EDI_changeClient client = new Zws_BAPI_SO_EDI_changeClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_SOEditThreeReasons"]);
            client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

            ZediSoFormat[] salesDataArray = new ZediSoFormat[rowDetailsArr.Count];

            for (int i = 0; i < rowDetailsArr.Count; i++)
            {

                salesDataArray[i] = new ZediSoFormat();
                string date = ((string[])rowDetailsArr[i])[2];
                string bstkd = ((string[])rowDetailsArr[i])[5];
                string sokunnr = ((string[])rowDetailsArr[i])[0];
                string matnr = ((string[])rowDetailsArr[i])[3];
                string packer = ((string[])rowDetailsArr[i])[1];
                string kwmeng = ((string[])rowDetailsArr[i])[4];
                salesDataArray[i].Vkorg = subsiCode;
                salesDataArray[i].Vtweg = "00";
                salesDataArray[i].Spart = "00";
                salesDataArray[i].Vkbur = subsiCode;
                salesDataArray[i].Bstkd = bstkd.ToString();
                salesDataArray[i].Bstdk = date.ToString();
                salesDataArray[i].Sokunnr = "0000" + sokunnr.ToString();  /*"0000301557"*/;
                salesDataArray[i].Vdatu = date.ToString();
                salesDataArray[i].Posnr = "000010";
                salesDataArray[i].Matnr = matnr.ToString();
                salesDataArray[i].Packer = packer.ToString();/*"0000500011"*/;
                salesDataArray[i].Edatu = date.ToString();
                salesDataArray[i].Kwmeng = kwmeng.ToString();
                salesDataArray[i].Zzveta = date.ToString();
                salesDataArray[i].Zzvetd = date.ToString();
                salesDataArray[i].Htext = username;
                salesDataArray[i].ZqtyChange = "";
                salesDataArray[i].ZetdChange = "";
                salesDataArray[i].ZskuChange = "";
                salesDataArray[i].Abgru = reasonCode4;

            }
            try
            {

                ZbapiSoEdi salesData = new ZbapiSoEdi();
                salesData.Result = new ZediSoError[0];
                salesData.ZediSoFormat = salesDataArray;
                ZbapiSoEdiResponse response = client.ZbapiSoEdi(salesData);
                ZediSoError[] soError = response.Result;
                for (int i = 0; i < soError.Length; i++)
                {
                    log.Info("SAP response:" + soError[i].Msgty);
                    SOResponseModel ResponseModel = new SOResponseModel();
                    ResponseModel.ItemNumber = soError[i].Itemno;
                    ResponseModel.RowNumber = soError[i].Row;
                    ResponseModel.PONumber = soError[i].Bstkd;
                    ResponseModel.MessageType = soError[i].Msgty;
                    ResponseModel.MessageContent = soError[i].Msg;
                    massResponseModel.Add(ResponseModel);

                }


            }
            catch (Exception exc)
            {
                TrackCancelSO(rowDetailsArr, reasonCode4, massResponseModel, username, orderType, true, exc.Message);
                log.Error("Error encountered in the method:CancelSO. Exception Message:" + exc.Message);
                throw exc;
            }

            TrackCancelSO(rowDetailsArr, reasonCode4, massResponseModel, username, orderType, false, "");

            return massResponseModel;
        }
        public List<ResponseModel> ChangeorCancelSOserviceCaller(Dictionary<string, string> scDataVal,ref int successCount, ref int errorCount, string username, int subsiId)
        {

            log.Info("SO Service calling using new order. SO Data below");
            log.Info(scDataVal);
            responseList = new List<ResponseModel>();
           
            IEntityManager entityManager = new EntityManager();

            string subsiCode;
            using (var context = new GoodpackEDIEntities())
            {
                subsiCode = (from s in context.Gen_EDISubsies
                             where s.Id == subsiId
                             select s.SubsiCode).FirstOrDefault().ToString();

            }

            Zws_BAPI_SO_EDI_changeClient client = new Zws_BAPI_SO_EDI_changeClient();
            client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["ServiceReference_SOEditThreeReasons"]);
            client.ClientCredentials.UserName.UserName = ConstantUtils.ConstantUtilities.SAPUsername;
            client.ClientCredentials.UserName.Password = ConstantUtils.ConstantUtilities.SAPPassword;

            ZediSoFormat[] salesDataArray = new ZediSoFormat[scDataVal.Count];

          

                salesDataArray[0] = new ZediSoFormat();
                string date =scDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                string dateVal = date.Substring(0, 2);
                string monthVal = date.Substring(3, 2);
                string yearVal = date.Substring(6, 4);
                date = dateVal + "." + monthVal + "." + yearVal;
                string bstkd =scDataVal[SOChangeCancelSAPFields.CustomerPONumber];
                string sokunnr = scDataVal[SOChangeCancelSAPFields.Customer];
                string matnr = scDataVal[SOChangeCancelSAPFields.Material];
                string packer =scDataVal[SOChangeCancelSAPFields.Packer];
                string kwmeng = scDataVal[SOChangeCancelSAPFields.OrderQuantity];
                salesDataArray[0].Vkorg = subsiCode;
                salesDataArray[0].Vtweg = "00";
                salesDataArray[0].Spart = "00";
                salesDataArray[0].Vkbur = subsiCode;
                salesDataArray[0].Bstkd = bstkd.ToString();
                salesDataArray[0].Bstdk = date.ToString();
                salesDataArray[0].Sokunnr = "0000" + sokunnr.ToString();  /*"0000301557"*/;
                salesDataArray[0].Vdatu = date.ToString();
                salesDataArray[0].Posnr = "000010";
                salesDataArray[0].Matnr = matnr.ToString();
                salesDataArray[0].Packer = packer.ToString();/*"0000500011"*/;
                salesDataArray[0].Edatu = date.ToString();
                salesDataArray[0].Kwmeng = kwmeng.ToString();
                salesDataArray[0].Zzveta = date.ToString();
                salesDataArray[0].Zzvetd = date.ToString();
                salesDataArray[0].Htext = username;
                salesDataArray[0].ZqtyChange = scDataVal[SOChangeCancelSAPFields.ReasonforQuantityChange];
                salesDataArray[0].ZetdChange = scDataVal[SOChangeCancelSAPFields.ReasonforETDchange];
                salesDataArray[0].ZskuChange =scDataVal[SOChangeCancelSAPFields.ReasonforSKUchange];
                salesDataArray[0].Abgru = scDataVal[SOChangeCancelSAPFields.ReasonforCancellation];

        
            try
            {

                ZbapiSoEdi salesData = new ZbapiSoEdi();
                salesData.Result = new ZediSoError[0];
                salesData.ZediSoFormat = salesDataArray;
                ZbapiSoEdiResponse response = client.ZbapiSoEdi(salesData);
                ZediSoError[] soError = response.Result;
                //for (int i = 0; i < soError.Length; i++)
                //{
                //    SOResponseModel ResponseModel = new SOResponseModel();
                //    ResponseModel.ItemNumber = soError[i].Itemno;
                //    ResponseModel.RowNumber = soError[i].Row;
                //    ResponseModel.PONumber = soError[i].Bstkd;
                //    ResponseModel.MessageType = soError[i].Msgty;
                //    ResponseModel.MessageContent = soError[i].Msg;
                //    massResponseModel.Add(ResponseModel);

                //}
                ResponseModel responseModel;
                responseModel = new ResponseModel();
               
                bool error = false;
                for (int i = 0; i < soError.Length; i++)
                {
                    log.Info("Response from SAP for SO change or Cancel:-" + soError[i].Msgty) ;
                   
                    if (soError[i].Msgty == "E")
                    {
                        if (!error)
                        {
                           // errorCount++;
                            error = true;
                        }
                        responseModel = new ResponseModel();
                        responseModel.Success = false;
                        responseModel.Message = response.Result[i].Msg;
                        responseList.Add(responseModel);
                    }
                    else
                    {
                        if (!error)
                        {
                           // successCount++;
                        }
                        responseModel = new ResponseModel();
                        responseModel.Success = true;
                        responseModel.Message = response.Result[i].Msg;
                        responseList.Add(responseModel);
                    }


                }
                if(error)
                {
                    errorCount++; 
                    log.Info("Status from SAP:-ERROR");
                }
                else
                {
                    successCount++;
                    log.Info("Status from SAP:-SUCESS");
                }
               

            }
            catch (Exception exc)
            {
                //  TrackEditSO(rowDetailsArr, reasonCode1, reasonCode2, reasonCode3, massResponseModel, username, IsNewOrder, true, exc.Message);
                throw exc;
            }

            //  TrackEditSO(rowDetailsArr, reasonCode1, reasonCode2, reasonCode3, massResponseModel, username, IsNewOrder, false, "");

            return responseList;
        }

    }
}