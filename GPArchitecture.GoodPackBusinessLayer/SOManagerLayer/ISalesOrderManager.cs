﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.SOManagerLayer
{
    interface ISalesOrderManager
    {
        List<SalesOrderModel> SalesOrderData(string startDate, string endDate, string customerNumbr);
        List<SalesOrderModel> NewSalesOrderData(string startDate, string endDate, string customerNumbr);
        List<SalesOrderModel> getReport(string xmlFile);
        List<SOResponseModel> postSO(string username,ArrayList rowDetails);
        List<SOResponseModel> massEditSO(string username, string cusCode, List<string> customerPNos, List<string> deliveryLocations, List<string> deliveryDates, List<string> matNos, List<string> corderQtys, string materialNo, int orderQty, string deliveryDt, List<string> reason1, List<string> reason2, List<string> reason3);
        List<SOResponseModel> massCancelSO(string username,CancelSOModel SOModel);
        List<ResponseModel> ChangeorCancelSOserviceCaller(Dictionary<string, string> scDataVal, ref int successCount, ref int errorCount, string username, int subsiId);
    }
}