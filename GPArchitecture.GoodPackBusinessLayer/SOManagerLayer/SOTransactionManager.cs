﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.SOManagementLayer;
using GPArchitecture.Models;
using GPArchitecture.Parsers;
using GPArchitecture.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;

namespace GPArchitecture.GoodPackBusinessLayer.SOManagerLayer
{
    public class SOTransactionManager
    {
        private string scPath;
        private SCTransactionModel objGPCustomerMessage;
        private StringBuilder errorDescription = new StringBuilder();
      
        private static readonly ILog log = LogManager.GetLogger(typeof(SOTransactionManager));
        private Dictionary<int, Dictionary<string, string>> sapSubmitTransData;
        private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();

        public IList<GenericItemModel> SOFileData(string mapperName, int subsiId, int fileType, string path)
        {
            log.Debug("Traansaction Manager -SOFileData Begin :-" + path + "Mapper Name :-" + mapperName);
            scPath = path;
            bool isValidFile = false;
            string message = "";
            objGPCustomerMessage = new SCTransactionModel();
            FileInfo fileInfo = null;
            if (fileType == 1)
            {            

                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                objGPCustomerMessage.BatchFileName = fileInfo.Name;
            }
            else if (fileType == 2)
            {
                scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
            }
            else if (fileType == 3)
            {            
                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileName = Path.GetFileName(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
            }

            //bool countLimit = CountFileLines();
            //if (countLimit)
            //{
            objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
            IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
            GenericItemModel itemModel;
            isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
            if (isValidFile)
            {
                string scData;
                bool isFirstVal = true;
                int i = 0;
                string[] data;

                if (mapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(mapperName))
                {
                    log.Debug("Traansaction Manager -SOFileData SOSumiData");
                    data = SOSumiData(fileInfo);

                }
                else if (mapperName == GPArchitecture.Cache.Config.getConfig("YokohamaSOMapper") || GPArchitecture.Cache.Config.getConfigList("YokohamaSOMapper").Contains(mapperName))
                {
                    log.Debug("Traansaction Manager -SOFileData YokohamaSOMapper");
                    if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                    {

                        scData = ExcelParser.ExcelToCSV(scPath, 20, 0);
                        //scData
                        if (scData.Contains(','))
                            throw new IOException("Data has special character , in it. Please remove it and upload");
                        scData = scData.Replace("|", ",");
                        objGPCustomerMessage.Message = scData;
                        data = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    }
                    else
                    {
                        using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                        {
                            scData = objStreamReader.ReadToEnd();
                            Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",
                            RegexOptions.Compiled | RegexOptions.IgnoreCase);
                            scData = regData.Replace(scData, ",");
                            //scData = scData.Replace(",", "|");
                            objGPCustomerMessage.Message = scData;
                            objStreamReader.Close();
                            objStreamReader.Dispose();

                        }
                    }
                    log.Debug("Traansaction Manager -SOFileData Yokohama Parser");
                    data = Yokohama_Custom.Parse(objGPCustomerMessage.Message, out message);
                }
                else
                {
                    data = SOData(fileInfo, out message);
                }
             //   data = data.ToString().Replace(",", "|");
                IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
                foreach (var item in data)
                {
                    if (item.Length != 0)
                    {
                        i++;
                        string val = item.Replace("\"", "").Replace("\"", "");
                        string[] stringData = val.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                        //char[] arrChar = { ',', '|'};
                        //string[] stringData = val.Split(arrChar, StringSplitOptions.None);
                       
                        if (isFirstVal)
                        {
                            isFirstVal = false;
                            if (stringData.Length == mappedData.Count)
                            {
                                bool isMappedData = false;
                                for (int j = 0; j < mappedData.Count; j++)
                                {
                                    if (stringData[j] == mappedData[j].FieldName)
                                    {
                                        isMappedData = true;
                                        break;
                                    }
                                }
                                if (isMappedData)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.Name = val;
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    scDataModel.Add(itemModel);
                                    itemModel = new GenericItemModel();
                                    i++;
                                    itemModel.Id = i;
                                    itemModel.Name = SAPHeaders(mapperName).Replace("\"", "").Replace("\r", "");
                                    scDataModel.Add(itemModel);
                                    ///////////////////////////
                                }
                                else
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Status = "false";
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;                                  
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                    itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file " + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                    scDataModel.Add(itemModel);
                                    return scDataModel;
                                }
                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false";
                                itemModel.DataValues = message;
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;                               
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file" + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }
                        }
                        else
                        {
                            if (stringData.Length == mappedData.Count)
                            {
                                int counter = 0;
                                for (int j = 0; j < stringData.Length; j++)
                                {
                                    if (stringData[j] == "")
                                    {
                                        counter++;
                                    }
                                }
                                if (counter != stringData.Length)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    itemModel.Name = item.Replace("\"", "");
                                    //SAP headers and items in the file
                                    string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);              
                                    itemModel.DecodeValue = dataWithDecodeValue;
                                    scDataModel.Add(itemModel);          
                                }
                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false";
                                itemModel.DataValues = message;
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;                          
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file and Mapper";
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }
                        }
                    }
                }
                scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                return scDataModel;
            }
            else
            {
                itemModel = new GenericItemModel();
                itemModel.Status = "false";
                itemModel.DataValues = message;
                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                itemModel.Message = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                itemModel.ErrorMessage = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                scDataModel.Add(itemModel);
                return scDataModel;
            }
        }

        public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, int fileType)
        {
            try
            {
                objGPCustomerMessage = new SCTransactionModel();
                GenericItemModel itemModelVal = itemModel[1];
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                bool isFileData = false;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.Message = dataValue[0].ToString();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (fileType == 1)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    isFileData = true;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else if (fileType == 2)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                else if (fileType == 3)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                }

                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
                // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = AddSOBatchFileData(objGPCustomerMessage, mapperFile);
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        //string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];

                        int transID = InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);

                        trnsLineItems.Add(transID);
                        i++;
                    }
                }


                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
                UpdateSCBatchFileData(objGPCustomerMessage);
                UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");




                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                    SOServiceManager serviceCaller = new SOServiceManager();
                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        int trLineID = trnsLineItems[i];
                        log.Info("Updating SO TransLine table with excel data");
                        UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i], custPOdate);

                        try
                        {

                            List<ResponseModel> responseModel;
                            log.Info("Calling SO webservice");
                            if (isFileData)
                            {
                                try
                                {
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId, custPOdate);
                                }
                                catch (Exception e)
                                {
                                    log.Error("SC serviceCaller error-" + e);
                                    List<ResponseModel> newResponse = new List<ResponseModel>();
                                    ResponseModel response = new ResponseModel();
                                    response.Message = "Error contacting SAP";
                                    response.Success = false;                                    
                                    errorCount++;
                                    newResponse.Add(response);
                                    responseModel = newResponse;
                                }
                            }
                            else
                            {
                                try
                                {
                                    custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId, custPOdate);
                                }
                                catch (Exception e)
                                {
                                    log.Error("SC serviceCaller error-" + e);
                                    errorCount++;
                                    List<ResponseModel> newResponse = new List<ResponseModel>();
                                    ResponseModel response = new ResponseModel();
                                    response.Message = "Error contacting SAP";
                                    response.Success = false;
                                    newResponse.Add(response);
                                    responseModel = newResponse;
                                }
                            }
                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

                            bool isSuccess = InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
                                UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
                            }

                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                            UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                        }
                        catch (Exception e)
                        {

                            log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;

                            InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                            UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);

                            serviceResponse.Add(trLineID, responseModel);

                        }

                    }
                }


                int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = SoTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    genericModel = SoTestResult(itemModel, trnsLineItems);

                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
                placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
                placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address

                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSO;
                    strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSO);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSO);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSO;
                        strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
                        // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;

                        Attachment attachment = new Attachment(strGoodyearSO);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
                    }
                    else
                    {
                        //fileupload SO
                        //send email to users

                        // List<Attachment> attachments = new List<Attachment>();
                        // string responseFile;
                        // responseFile = objGPCustomerMessage.BatchFileSourceAddress;
                        //// responseFile += "\\" + objGPCustomerMessage.BatchFileName;
                        // responseFile += "\\" + "ResponseFileGenerated" + objGPCustomerMessage.BatchID;
                        // if (!File.Exists(responseFile))
                        // {
                        //     File.Create(responseFile).Close();
                        // }
                        // responseFile = getResponseSO(responseFile, objGPCustomerMessage.BatchID);                      
                        // Attachment attachment = new Attachment(responseFile);
                        // attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        // attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        // GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", attachments);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;

            }
            catch (Exception exp)
            {

                log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }

        }
        public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, int fileType, bool isWeb)
        {
            try
            {
                objGPCustomerMessage = new SCTransactionModel();
                GenericItemModel itemModelVal = itemModel[1];
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                bool isFileData = false;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.Message = dataValue[0].ToString();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (fileType == 1)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    isFileData = true;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else if (fileType == 2)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                else if (fileType == 3)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                }
                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;          
                List<int> trnsLineItems = new List<int>();              
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = AddSOBatchFileData(objGPCustomerMessage, mapperFile);
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        //string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];

                        int transID = InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);

                        trnsLineItems.Add(transID);
                        i++;
                    }
                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
                UpdateSCBatchFileData(objGPCustomerMessage);
                UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                    SOServiceManager serviceCaller = new SOServiceManager();
                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        int trLineID = trnsLineItems[i];
                        log.Info("Updating SO TransLine table with excel data");
                        UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i], custPOdate);

                        try
                        {
                            List<ResponseModel> responseModel;
                            log.Info("Calling SO webservice");
                            if (isFileData)
                            {
                                try
                                {
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId, custPOdate);
                                }
                                catch (Exception e)
                                {
                                    log.Error("SO serviceCaller error-" + e);
                                    List<ResponseModel> newResponse = new List<ResponseModel>();
                                    ResponseModel response = new ResponseModel();
                                    response.Message = "Error contacting SAP";
                                    response.Success = false;
                                    newResponse.Add(response);
                                    responseModel = newResponse;
                                }
                            }
                            else
                            {
                                try
                                {
                                    custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId, custPOdate);
                                }
                                catch (Exception e)
                                {
                                    log.Error("SO serviceCaller error-" + e);
                                    List<ResponseModel> newResponse = new List<ResponseModel>();
                                    ResponseModel response = new ResponseModel();
                                    response.Message = "Error contacting SAP";
                                    response.Success = false;
                                    newResponse.Add(response);
                                    responseModel = newResponse;
                                }
                            }
                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

                            bool isSuccess = InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
                                UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
                            }
                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                            UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;

                            InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                            UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);

                            serviceResponse.Add(trLineID, responseModel);

                        }
                    }      
                }

                int iCountSentToSAP = 0;
               
                UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = SoTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    genericModel = SoTestResult(itemModel, trnsLineItems);

                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
                placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
                placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address

                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSO;
                    strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSO);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSO);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSO;
                        strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
                        // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;
                        if (isWeb)
                            strGoodyearSO = strGoodyearSO + "\\" + objGPCustomerMessage.BatchFileName;

                        Attachment attachment = new Attachment(strGoodyearSO);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
                    }
                    else
                    {                    
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);                 
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;
            }
        }

        public string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();
            //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
        public string[] getAllRecipients(int subsiId)
        {
            List<string> mailList = new List<string>();
            using (var context = new GoodpackEDIEntities())
            {

                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }

        #region OldVerifYFunction
       
        //public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, int fileType, bool isFileData)
        //{
        //    try
        //    {
        //        objGPCustomerMessage = new SCTransactionModel();
        //        GenericItemModel itemModelVal = itemModel[1];
        //        List<string> dataValue;
        //        List<string> fileName;
        //        List<string> filePath;
        //        List<string> message;
        //        List<string> emailId;
        //        dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
        //        fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
        //        filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
        //        message = itemModel.Select(x => x.Message).Distinct().ToList();
        //        emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
        //        objGPCustomerMessage.Message = dataValue[0].ToString();
        //        objGPCustomerMessage.BatchFileName = fileName[0].ToString();
        //        objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
        //        objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
        //        objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
        //        if (fileType == 1)
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
        //            itemModel.RemoveAt(1);
        //            using (var context = new GoodpackEDIEntities())
        //            {
        //                objGPCustomerMessage.EmailId = (from s in context.Gen_User
        //                                                where s.Username == username
        //                                                select s.EmailId).FirstOrDefault();
        //            }
        //        }
        //        else if (fileType == 2)
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
        //        }
        //        else if (fileType == 3)
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
        //        }

        //        using (var context = new GoodpackEDIEntities())
        //        {
        //            int userId = (from s in context.Gen_User
        //                          where s.Username == username
        //                          select s.Id).FirstOrDefault();
        //            objGPCustomerMessage.UserId = userId;
        //        }

        //        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
        //        objGPCustomerMessage.DateCreated = DateTime.Now;
        //        objGPCustomerMessage.Service = "GPMapper";
        //        objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
        //        objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
        //        List<int> trnsLineItems = new List<int>();
        //        GPMapper mapper = new GPMapper();
        //        MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

        //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
        //        {
        //            //Insert lines in Trn_TransBatch table                    
        //            objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
        //            objGPCustomerMessage.BatchID = AddSOBatchFileData(objGPCustomerMessage, mapperFile);
        //            int i = 0;
        //            // Update/Insert lines in TRANS_LINE table
        //            foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
        //            {
        //                objLineReferences.StatusCode = LineStatus.NEW;
        //                int transID = InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);
        //                trnsLineItems.Add(transID);
        //                i++;
        //            }
        //        }


        //        // Update status of transaction to processed.
        //        objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
        //        objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
        //        objGPCustomerMessage.ErrorDesc = string.Empty;
        //        log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
        //        UpdateSCBatchFileData(objGPCustomerMessage);
        //        UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
        //          0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");

        //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
        //        {
        //            objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
        //            // Update TRANS_LINE status
        //            log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
        //            UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
        //            GPArchitecture.GoodPackBusinessLayer.SOManagementLayer.SOServiceManager serviceCaller = new GPArchitecture.GoodPackBusinessLayer.SOManagementLayer.SOServiceManager();
        //            Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
        //            sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
        //            int successCount = 0;
        //            int errorCount = 0;
        //            for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
        //            {
        //                int trLineID = trnsLineItems[i];
        //                log.Info("Updating SO TransLine table with excel data");
        //                UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i], custPOdate);

        //                try
        //                {

        //                    List<ResponseModel> responseModel;
        //                    log.Info("Calling SO webservice");
        //                    if (isFileData)
        //                    {
        //                        responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId, custPOdate);
        //                    }
        //                    else
        //                    {
        //                        custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
        //                        responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId, custPOdate);
        //                    }
        //                    serviceResponse.Add(trLineID, responseModel);

        //                    log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

        //                    bool isSuccess = InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
        //                    log.Info("Insertion of trans line content success status:" + isSuccess);
        //                    if (isSuccess)
        //                    {
        //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
        //                        UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
        //                    }
        //                    else
        //                    {
        //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
        //                        UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
        //                    }

        //                    sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
        //                    UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
        //                }
        //                catch (Exception e)
        //                {
        //                    log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
        //                    List<ResponseModel> responseModel = new List<ResponseModel>();
        //                    ResponseModel model = new ResponseModel();
        //                    model.Success = false;
        //                    model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
        //                    responseModel.Add(model);
        //                    errorCount++;
        //                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
        //                    objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
        //                    objGPCustomerMessage.ErrorDesc = e.Message;

        //                    InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
        //                    //need to look
        //                    UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, false);
        //                    UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, false);
        //                    serviceResponse.Add(trLineID, responseModel);
        //                }
        //            }
        //        }

        //        int iCountSentToSAP = 0;
        //        UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
        //            iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
        //        IList<GenericItemModel> genericModel = null;
        //        if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
        //            genericModel = SoTestResultGoodyear(itemModel, trnsLineItems);
        //        }
        //        else
        //        {
        //            genericModel = SoTestResult(itemModel, trnsLineItems);

        //        }
        //        Gen_User userDetails;
        //        using (var dataContext = new GoodpackEDIEntities())
        //        {
        //            userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
        //        }
        //        Dictionary<string, string> placeholders = new Dictionary<string, string>();
        //        placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
        //        placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
        //        placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
        //        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
        //        placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
        //        placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
        //        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
        //        placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
        //        placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
        //        placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
        //        if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
        //        {
        //            //send email to customer and internal users of goodpack
        //            //emailid of customer get from batchfilesource address

        //            string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
        //            List<Attachment> attachments1 = new List<Attachment>();
        //            string strFileSO;
        //            strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
        //            strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

        //            Attachment attachment1 = new Attachment(strFileSO);
        //            attachment1.Name = objGPCustomerMessage.BatchFileName;
        //            attachments1.Add(attachment1);
        //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


        //            GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
        //            List<Attachment> attachments2 = new List<Attachment>();
        //            Attachment attachment2 = new Attachment(strFileSO);
        //            attachment2.Name = objGPCustomerMessage.BatchFileName;
        //            attachments2.Add(attachment2);
        //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //            GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
        //        }
        //        else
        //        {
        //            if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
        //            {
        //                //goodyear notification
        //                List<Attachment> attachments = new List<Attachment>();
        //                string strGoodyearSO;
        //                strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
        //                // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;

        //                Attachment attachment = new Attachment(strGoodyearSO);
        //                attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
        //                attachments.Add(attachment);
        //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
        //            }
        //            else
        //            {
        //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //                // GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", attachments);
        //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
        //            }
        //        }
        //        log.Info("Email send to customer" + userDetails.FullName.ToString());
        //        for (int i = 0; i < genericModel.Count; i++)
        //        {
        //            itemModelData.Add(genericModel[i]);
        //            if (i == 0)
        //            {
        //                itemModelData.Add(itemModelVal);
        //            }
        //        }
        //        log.Info("Leaving from verifyData in SCManager");
        //        return itemModelData;
        //    }
        //    catch (Exception exp)
        //    {
        //        log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
        //        IList<GenericItemModel> genericModel = new List<GenericItemModel>();
        //        GenericItemModel itemsModel = new GenericItemModel();
        //        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
        //        objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
        //        objGPCustomerMessage.ErrorDesc = exp.Message;
        //        itemsModel.Status = "false";
        //        itemsModel.Message = "Unable to process your request.";
        //        // Update TRANS_BATCH
        //        genericModel.Add(itemsModel);
        //        UpdateSCBatchFileData(objGPCustomerMessage);
        //        Dictionary<string, string> placeholder = new Dictionary<string, string>();
        //        placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
        //        GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
        //        return genericModel;
        //    }
        //}
        //public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, bool isFileData, bool isWeb)
        //{
        //    try
        //    {
        //        objGPCustomerMessage = new SCTransactionModel();
        //        GenericItemModel itemModelVal = itemModel[1];
        //        List<string> dataValue;
        //        List<string> fileName;
        //        List<string> filePath;
        //        List<string> message;
        //        List<string> emailId;
        //        dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
        //        fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
        //        filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
        //        message = itemModel.Select(x => x.Message).Distinct().ToList();
        //        emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
        //        objGPCustomerMessage.Message = dataValue[0].ToString();
        //        objGPCustomerMessage.BatchFileName = fileName[0].ToString();
        //        objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
        //        objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
        //        objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
        //        if (isFileData)
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
        //            itemModel.RemoveAt(1);
        //            using (var context = new GoodpackEDIEntities())
        //            {
        //                objGPCustomerMessage.EmailId = (from s in context.Gen_User
        //                                                where s.Username == username
        //                                                select s.EmailId).FirstOrDefault();
        //            }
        //        }
        //        else
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
        //        }

        //        using (var context = new GoodpackEDIEntities())
        //        {
        //            int userId = (from s in context.Gen_User
        //                          where s.Username == username
        //                          select s.Id).FirstOrDefault();
        //            objGPCustomerMessage.UserId = userId;
        //        }

        //        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
        //        objGPCustomerMessage.DateCreated = DateTime.Now;
        //        objGPCustomerMessage.Service = "GPMapper";
        //        objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
        //        objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
        //        // bool processOneLineOnly = false;
        //        List<int> trnsLineItems = new List<int>();
        //        // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
        //        GPMapper mapper = new GPMapper();
        //        MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

        //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
        //        {
        //            //Insert lines in Trn_TransBatch table                    
        //            objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
        //            objGPCustomerMessage.BatchID = AddSOBatchFileData(objGPCustomerMessage, mapperFile);
        //            int i = 0;
        //            // Update/Insert lines in TRANS_LINE table
        //            foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
        //            {
        //                objLineReferences.StatusCode = LineStatus.NEW;
        //                //string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];

        //                int transID = InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);

        //                trnsLineItems.Add(transID);
        //                i++;
        //            }
        //        }


        //        // Update status of transaction to processed.
        //        objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
        //        objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
        //        objGPCustomerMessage.ErrorDesc = string.Empty;
        //        log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
        //        UpdateSCBatchFileData(objGPCustomerMessage);
        //        UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
        //          0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");




        //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
        //        {
        //            objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
        //            // Update TRANS_LINE status
        //            log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
        //            UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
        //            SOServiceManager serviceCaller = new SOServiceManager();
        //            SalesOrderManager soserviceManager = new SalesOrderManager();
        //            Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
        //            sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
        //            int successCount = 0;
        //            int errorCount = 0;
        //            for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
        //            {
        //                int trLineID = trnsLineItems[i];
        //                log.Info("Updating SO TransLine table with excel data");
        //                UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i], custPOdate);

        //                try
        //                {

        //                    List<ResponseModel> responseModel;
        //                    log.Info("Calling SO webservice");

        //                    if (isFileData)
        //                    {
        //                        responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId, custPOdate);

        //                    }
        //                    else
        //                    {
        //                        custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
        //                        responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId, custPOdate);
        //                    }

        //                    serviceResponse.Add(trLineID, responseModel);

        //                    log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

        //                    bool isSuccess = InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
        //                    log.Info("Insertion of trans line content success status:" + isSuccess);
        //                    if (isSuccess)
        //                    {
        //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
        //                        UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
        //                    }
        //                    else
        //                    {
        //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
        //                        UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
        //                    }

        //                    sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
        //                    UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
        //                }
        //                catch (Exception e)
        //                {

        //                    log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
        //                    List<ResponseModel> responseModel = new List<ResponseModel>();
        //                    ResponseModel model = new ResponseModel();
        //                    model.Success = false;
        //                    model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
        //                    responseModel.Add(model);
        //                    errorCount++;
        //                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
        //                    objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
        //                    objGPCustomerMessage.ErrorDesc = e.Message;

        //                    InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
        //                    UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
        //                    UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
        //                    serviceResponse.Add(trLineID, responseModel);
        //                }
        //            }
        //        }

        //        int iCountSentToSAP = 0;
        //        //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
        //        //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
        //        UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
        //            iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
        //        IList<GenericItemModel> genericModel = null;
        //        if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
        //        {
        //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
        //            genericModel = SoTestResultGoodyear(itemModel, trnsLineItems);
        //        }
        //        else
        //        {
        //            genericModel = SoTestResult(itemModel, trnsLineItems);

        //        }
        //        Gen_User userDetails;
        //        using (var dataContext = new GoodpackEDIEntities())
        //        {
        //            userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
        //        }

        //        Dictionary<string, string> placeholders = new Dictionary<string, string>();
        //        placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
        //        placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
        //        placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
        //        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
        //        placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
        //        placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
        //        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
        //        placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
        //        placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
        //        placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
        //        if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
        //        {
        //            //send email to customer and internal users of goodpack
        //            //emailid of customer get from batchfilesource address

        //            string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
        //            List<Attachment> attachments1 = new List<Attachment>();
        //            string strFileSO;
        //            strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
        //            strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

        //            Attachment attachment1 = new Attachment(strFileSO);
        //            attachment1.Name = objGPCustomerMessage.BatchFileName;
        //            attachments1.Add(attachment1);
        //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


        //            GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
        //            List<Attachment> attachments2 = new List<Attachment>();
        //            Attachment attachment2 = new Attachment(strFileSO);
        //            attachment2.Name = objGPCustomerMessage.BatchFileName;
        //            attachments2.Add(attachment2);
        //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //            GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
        //        }
        //        else
        //        {
        //            if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
        //            {
        //                //goodyear notification
        //                List<Attachment> attachments = new List<Attachment>();
        //                string strGoodyearSO;
        //                strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
        //                // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;
        //                if (isWeb)
        //                    strGoodyearSO = strGoodyearSO + "\\" + objGPCustomerMessage.BatchFileName;

        //                Attachment attachment = new Attachment(strGoodyearSO);
        //                attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
        //                attachments.Add(attachment);
        //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
        //            }
        //            else
        //            {
        //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
        //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
        //            }
        //        }
        //        log.Info("Email send to customer" + userDetails.FullName.ToString());
        //        for (int i = 0; i < genericModel.Count; i++)
        //        {
        //            itemModelData.Add(genericModel[i]);
        //            if (i == 0)
        //            {
        //                itemModelData.Add(itemModelVal);
        //            }
        //        }
        //        log.Info("Leaving from verifyData in SCManager");
        //        return itemModelData;

        //    }
        //    catch (Exception exp)
        //    {

        //        log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
        //        IList<GenericItemModel> genericModel = new List<GenericItemModel>();
        //        GenericItemModel itemsModel = new GenericItemModel();
        //        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
        //        objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
        //        objGPCustomerMessage.ErrorDesc = exp.Message;
        //        itemsModel.Status = "false";
        //        itemsModel.Message = "Unable to process your request.";
        //        // Update TRANS_BATCH
        //        genericModel.Add(itemsModel);
        //        UpdateSCBatchFileData(objGPCustomerMessage);
        //        Dictionary<string, string> placeholder = new Dictionary<string, string>();
        //        // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
        //        placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
        //        GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
        //        return genericModel;

        //    }

        //}

        #endregion

        #region Private

        public string getSubsyNameFromId(int SubsiId)
        {
            string SubsiName;
            using (var context = new GoodpackEDIEntities())
            {
                SubsiName = (from s in context.Gen_EDISubsies
                             where s.Id == SubsiId
                             select s.SubsiName).FirstOrDefault();
            }
            return SubsiName;


        }
        public IList<GenericItemModel> SoTestResultGoodyear(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }
        public IList<GenericItemModel> SoTestResult(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }
        public void UpdateSOTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_SOTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_SOTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = false;
                //scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess + successCount;
                //scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess ?? 0;
                scDataValue.RecordCountSAPError = errorCount;
                scDataValue.RecordCountSuccess = successCount;
                context.SaveChanges();
            }
        }
        public void UpdateResponseSOTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SOTransLine transLine = (context.Trn_SOTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = false;
                context.SaveChanges();
                log.Info("Updated Trn_SOTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public bool InsertSOTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SOTransLineContent lineContent = new Trn_SOTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool responseStatus=Convert.ToBoolean(responseModel[i].Success);
                    if (responseStatus)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SOTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }
        private void UpdateSOTransLineForData(int trLineID, Dictionary<string, string> scDataVal, string custPODate)
        {
            using (var context = new GoodpackEDIEntities())
            {

                Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                if (transLine != null)
                {

                    transLine.SalesOrganisation = scDataVal[SOSapMessageFields.SalesOrganization];
                    transLine.DistributionChannel = scDataVal[SOSapMessageFields.DistributionChannel];
                    transLine.Division = scDataVal[SOSapMessageFields.Division];
                    transLine.SalesOffice = scDataVal[SOSapMessageFields.SalesOffice];
                    transLine.CustomerPONumber = scDataVal[SOSapMessageFields.CustPOnumber];
                    // transLine.CustomerPODate = scDataVal[SOSapMessageFields.CustomerPOdate];
                    transLine.CustomerPODate = custPODate;
                    transLine.CustomerNumber = scDataVal[SOSapMessageFields.Customer];
                    transLine.ReqDeliveryDate = scDataVal[SOSapMessageFields.Requestdeliverydate];
                    transLine.ItemNumber = scDataVal[SOSapMessageFields.SOItemnumber];
                    transLine.MaterialNumber = scDataVal[SOSapMessageFields.Material];
                    transLine.FromLocation = scDataVal[SOSapMessageFields.Packer];
                    // transLine.ScheduleLineDate = scDataVal[SOSapMessageFields.Orderdate];
                    transLine.OrderDate = scDataVal[SOSapMessageFields.Orderdate];
                    transLine.DeliveryDate = scDataVal[SOSapMessageFields.DeliveryDate];
                    transLine.SOQuantity = scDataVal[SOSapMessageFields.Orderquantity];
                    transLine.ETA = scDataVal[SOSapMessageFields.VesselETA];
                    transLine.ETD = scDataVal[SOSapMessageFields.VesselETD];
                    transLine.POD = scDataVal[SOSapMessageFields.POD];
                    transLine.POL = scDataVal[SOSapMessageFields.POL];
                    transLine.Consignee = scDataVal[SOSapMessageFields.Consignee];
                    transLine.VesselName = scDataVal[SOSapMessageFields.Vesselname];
                    transLine.VoyageReference = scDataVal[SOSapMessageFields.Voyagereference];
                    transLine.ShippingLine = scDataVal[SOSapMessageFields.Shippingline];
                    transLine.Remarks = scDataVal[SOSapMessageFields.ItemRemarks];
                    transLine.DeliveryBlockOrder = scDataVal[SOSapMessageFields.DeliveryBlockOrders];
                    context.SaveChanges();
                }
            }
        }
        private int InsertSOTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_SOTransLine transLine = new Trn_SOTransLine();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = lineReference.LineContentItems;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }

                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                transLine.InTestMode = false;
                context.Trn_SOTransLine.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }
     
        private int AddSOBatchFileData(SCTransactionModel transBtachDetails, string mapperFile)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string transactionType = transBtachDetails.TransactionType.ToString();
                int transTypeID = (from s in context.Ref_TransactionTypes
                                   where s.TransactionCode == transactionType
                                   select s.Id).FirstOrDefault();
                string statusCode = transBtachDetails.StatusCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();


                Trn_SCTransBatch scFileData = new Trn_SCTransBatch();
                scFileData.UserId = transBtachDetails.UserId;
                scFileData.SubsyId = transBtachDetails.SubsiId;
                scFileData.TransactionId = transTypeID;
                scFileData.MapperFileName = mapperFile;
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.FileName = transBtachDetails.BatchFileName;
                scFileData.FileSource = transBtachDetails.BatchFileSourceType.ToString();
                scFileData.FileSourceAddress = transBtachDetails.BatchFileSourceAddress;
                scFileData.BatchContent = transBtachDetails.Message;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateCreated = DateTime.Now;
                scFileData.DateUpdated = null;
                scFileData.PODate = null;
                scFileData.RecordCountTotal = 0;
                scFileData.RecordCountSkipValFailure = 0;
                scFileData.RecordCountFieldValFailure = 0;
                scFileData.RecordCountSAPError = 0;
                scFileData.RecordCountSuccess = 0;
                scFileData.RecordCountSAPInProgress = 0;
                scFileData.DateExportedToTargetSystem = null;
                scFileData.IsTestMode = false;
                scFileData.EmailId = transBtachDetails.EmailId;
                context.Trn_SCTransBatch.Add(scFileData);
                context.SaveChanges();
                int scFileId = (from s in context.Trn_SCTransBatch
                                where (s.Id == scFileData.Id)
                                select s.Id).FirstOrDefault();


                return scFileId;


            }

        }
        private void UpdateSCBatchFileData(SCTransactionModel transBtachDetails)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = transBtachDetails.StatusCode.ToString();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();

                Trn_SCTransBatch scFileData = context.Trn_SCTransBatch.FirstOrDefault(x => (x.Id == transBtachDetails.BatchID)); //&& x.StatusId != 10 && x.StatusId != 12 && x.StatusId != 13));//Give where condition in valuetype
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateUpdated = DateTime.Now;
                context.SaveChanges();
            }

        }
        private void UpdateSCBatchCounts(int batchId, int errorCount, int successCount)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scData.RecordCountSuccess = successCount;
                scData.RecordCountSAPError = errorCount;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransBatch table of batch Id:" + batchId + " with success count:" + successCount + "and error count:" + errorCount);
            }
        }
        private void UpdateSCBatchCounts(int BatchId, int RecordCountTotal, int RecordCountSkipValFailure, int RecordCountFieldValFailure,
                int RecordCountSAPInProgress, int RecordCountSAPError, int RecordCountSuccess, string strService)
        {
            using (var context = new GoodpackEDIEntities())
            {
                if (strService == "Request")
                {
                    Trn_SCTransBatch scBatch = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scBatch.RecordCountTotal = RecordCountTotal;
                    scBatch.RecordCountSkipValFailure = RecordCountSkipValFailure;
                    scBatch.RecordCountFieldValFailure = RecordCountFieldValFailure;
                    // scBatch.RecordCountSuccess = RecordCountSuccess;
                    scBatch.RecordCountSAPInProgress = RecordCountSAPInProgress;
                    context.SaveChanges();

                    if (RecordCountSAPInProgress != 0)
                    {
                        Trn_SCTransBatch scBatchData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                        scBatchData.DateExportedToTargetSystem = DateTime.Now;
                        context.SaveChanges();
                    }
                }
                else
                {
                    //UPDATE TRANS_BATCH SET RecordCountSuccess=@SuccessCount, RecordCountSAPError=@SAPErrorCount

                    //   where BatchID=@BatchId

                    Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scData.RecordCountSuccess = RecordCountSuccess;
                    scData.RecordCountSAPError = RecordCountSAPError;
                    context.SaveChanges();

                    Trn_SCTransBatch scTransData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scTransData.RecordCountSAPInProgress = RecordCountTotal - RecordCountSkipValFailure - RecordCountFieldValFailure - RecordCountSAPError - RecordCountSuccess;
                    context.SaveChanges();
                }
            }
        }
        private void UpdateSOTransLine(int batchId, int subsiId, LineStatus? statusCode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                string checkstatusValue = LineStatus.NEW.ToString();
                int checkStatusID = (from s in context.Trn_LineStatus
                                     where s.StatusCode == checkstatusValue
                                     select s.Id).FirstOrDefault();

                var transLine = (context.Trn_SOTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();

                foreach (var item in transLine)
                {
                    if (item.StatusId != 0)
                        item.StatusId = statusID;
                    item.SubsiId = subsiId;
                    item.BatchId = batchId;
                    context.SaveChanges();
                }
            }
        }

        private bool Validate_IsFileTypeSupported(SCTransactionModel objGPCustomerMessage)
        {
            //objILog.Debug("Entering Validate_IsFileTypeSupported method");
            bool blnValidFileType = false;
            try
            {
                FILE_TYPES fileType;
                blnValidFileType = this.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                if (blnValidFileType == false)
                {
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    //objILog.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                }
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager Validate_IsFileTypeSupported method.objGPCustomerMessage:" + objGPCustomerMessage + ". Api Exception : Message- " + exp.Message);
                throw exp;
            }
            //objILog.Debug("Leaving Validate_IsFileTypeSupported method");
            return blnValidFileType;
        }

        private bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
                {
                    bool blnValidFileType = false;
                    //get value from db file types table
                    IList<string> lstFileExtns = new List<string>();
                    lstFileExtns.Add(".txt");
                    lstFileExtns.Add(".csv");
                    lstFileExtns.Add(".xls");
                    lstFileExtns.Add(".xlsx");
                    enumFileType = FILE_TYPES.CSV;
                    foreach (string strFileExt in lstFileExtns)
                    {
                        if (strFileName.ToLower().EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (strFileExt.ToLower() == ".csv")
                                enumFileType = FILE_TYPES.CSV;
                            else
                                enumFileType = FILE_TYPES.TEXT;
                            blnValidFileType = true;
                            break;
                        }
                    }
                    return blnValidFileType;
                }

        private string[] SCSumiData(FileInfo fileInfo)
        {
            string scData;

            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                scData = scData.Replace(",", "|");
                objGPCustomerMessage.Message = GetDataWithoutEmptyColumns(scData);

            }

            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    scData = objStreamReader.ReadToEnd();
                    scData = scData.Replace("\t", "|");
                    objGPCustomerMessage.Message = scData;
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                }
            }
            //  return scData.Split('\r\n');  
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        }

        private string GetDataWithoutEmptyColumns(string scData)
        {
            bool firstItem = true;
            string returnValue = "";
            int indexValue = 0;
            List<String> lines = new List<string>();
            string[] lineArray = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            foreach (string linedata in lineArray)
            {
                if (linedata != "")
                {
                    if (firstItem)
                    {
                        if (linedata.LastIndexOf('|') + 1 == linedata.Length)
                        {
                            if (linedata.LastIndexOf("||") + 2 == linedata.Length)
                            {
                                string tempLine = linedata.Replace("||", "");
                                string lineValue = (tempLine.LastIndexOf('|') + 1 == tempLine.Length) ? tempLine.Remove(tempLine.LastIndexOf('|')) : tempLine;
                                returnValue = returnValue + lineValue.Trim() + Environment.NewLine;
                                indexValue = lineValue.Split('|').Length;

                            }
                            else
                            {
                                returnValue = returnValue + linedata.Remove(linedata.LastIndexOf('|')).Trim() + Environment.NewLine;
                                indexValue = linedata.Remove(linedata.LastIndexOf('|')).Split('|').Length;
                            }
                            firstItem = false;
                        }
                        else
                        {
                            returnValue = returnValue + linedata.ToString().Trim() + Environment.NewLine;
                            indexValue = linedata.Split('|').Length;
                            firstItem = false;
                        }
                    }
                    else
                    {
                        string[] linedetails;
                        linedetails = linedata.Split('|').Select(e => e.Replace(" \"", "\"")).ToArray();
                        returnValue = returnValue + string.Join("|", linedetails, 0, indexValue) + Environment.NewLine;
                    }
                }
            }
            return returnValue;
        }

        private string[] SOSumiData(FileInfo fileInfo)
        {
            string scData;

            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                scData = ExcelParser.ExcelToCSV(scPath, 21, 0);
                scData = ExcelParser.GetSumitomoFileContent(scData);
                //scData = scData.Replace(",", "|");
                objGPCustomerMessage.Message = GetDataWithoutEmptyColumns(scData);
            }

            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    scData = objStreamReader.ReadToEnd();
                    objGPCustomerMessage.Message = scData;
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                }
            }
            //  return scData.Split('\r\n');   
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        }

        private string[] SOData(FileInfo fileInfo, out string ObjMessage)
        {
            string scData;

            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                //bool firstItem = true;
                //string returnValue = "";
                //int indexValue = 0;
                //List<String> lines = new List<string>();
                scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                //string[] lineArray = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                //foreach (string linedata in lineArray)
                //{
                //    if (linedata!="")
                //    {
                //        if (firstItem)
                //        {
                //            if (linedata.LastIndexOf('|') + 1 == linedata.Length)                               
                //            {
                //                if (linedata.LastIndexOf("||") + 2 == linedata.Length)
                //                {
                //                    string tempLine=linedata.Replace("||", "");
                //                    string lineValue = (tempLine.LastIndexOf('|') + 1 == tempLine.Length) ? tempLine.Remove(tempLine.LastIndexOf('|')) : tempLine;
                //                    returnValue = returnValue + lineValue + Environment.NewLine;
                //                    indexValue = lineValue.Split('|').Length;

                //                }
                //                else
                //                {
                //                    returnValue = returnValue + linedata.Remove(linedata.LastIndexOf('|')) + Environment.NewLine;
                //                    indexValue = linedata.Remove(linedata.LastIndexOf('|')).Split('|').Length;
                //                }
                //                firstItem = false;
                //            }
                //        }
                //        else
                //        {
                //            string[] linedetails;
                //            linedetails = linedata.Split('|');
                //            returnValue = returnValue + string.Join("|", linedetails, 0, indexValue) + Environment.NewLine;
                //        }
                //    }
                //}
                scData = GetDataWithoutEmptyColumns(scData);
                objGPCustomerMessage.Message = scData;
                ObjMessage = scData;
            }

            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    var line = "";
                    List<String> lines = new List<string>();
                    bool firstItem = true;
                    int indexValue = 0;
                    //For removing blank column from CSV
                    while ((line = objStreamReader.ReadLine()) != null)
                    {
                        if (Regex.IsMatch(line, "^[a-zA-Z0-9]"))
                        {
                            if (firstItem)
                            {
                                if (line.LastIndexOf(',') + 1 == line.Length)
                                {
                                    line = line.Remove(line.LastIndexOf(','));
                                }
                                indexValue = line.Split(',').Length;
                                firstItem = false;
                            }
                            else
                            {
                                string[] values;
                                if ((line.Split(',').Length) != indexValue)
                                {
                                    values = line.Split(',');
                                    line = string.Join(",", values, 0, indexValue);
                                }
                            }
                            lines.Add(line);
                        }
                    }
                    // scData = objStreamReader.ReadToEnd();
                    scData = string.Join(Environment.NewLine, lines.ToArray());
                    Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",
                    RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    scData = regData.Replace(scData, "|");
                    // scData = scData.Replace(",", "|");
                    objGPCustomerMessage.Message = scData;
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                    ObjMessage = scData;
                }
            }
            //  return scData.Split('\r\n');  
            scData = scData.Replace(",", "|");
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        }

        private IList<Trn_MapperFieldSpecs> ValidateData(string fileName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                IList<Trn_MapperFieldSpecs> mapperFields = (from s in context.Trn_SubsiToTransactionFileMapper
                                                            join p in context.Trn_MapperFieldSpecs on s.Id equals p.SubsiToTransactionMapperId
                                                            where (s.Filename == fileName)
                                                            select p).ToList();

                return mapperFields;
            }

        }

        private string CheckForDecodeValueValidation(string fileData, string headers, string mapperName, int subsiId)
        {
            StringBuilder fileDecodedData = new StringBuilder();

            string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            IDecodeValueManager decodeValueManager = new DecodeValueManager();
            for (int i = 0; i < headerValues.Length; i++)
            {
                string sapFieldName = headerValues[i];
                string inputType = String.Empty;
                int lookupNameId = 0;
                int transactionId;
                if (sapFieldName != "")
                {
                    using (var context = new GoodpackEDIEntities())
                    {
                        transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                         where s.Filename == mapperName
                                         select s.TransactionCodeId).FirstOrDefault();

                        inputType = (from s in context.Ref_SAPMessageFields
                                     where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                     select s.InputType).FirstOrDefault();
                        //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                    }
                    switch (sapFieldName.ToUpper())
                    {
                        case "CUSTOMER CODE":
                        case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            break;
                        case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            break;
                        case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                            break;
                        case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                            break;
                        case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                            break;
                        case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                            break;
                        case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                            break;
                        case "FRM LOCATION":
                        case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            }
                            else
                            {
                                if (transactionId == 12)
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                                }
                                else
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Packer);
                                }
                            }
                            break;
                        case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Packer);
                            }
                            else
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                            }
                            break;
                        case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                            break;
                    }
                    TargetMessageInputTypes inputSAPType =
                            (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);


                    if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                    {
                        string decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId);
                        if (decodeValue != null)
                        {
                            if (decodeValue.Length != 0)
                                errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName));
                            else
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                            fileDecodedData.Append(decodeValue);

                        }
                        else
                        {
                            bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                            if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
                            {
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                                //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
                                fileDecodedData.Append("false");
                            }
                        }
                    }
                    else
                    {
                        errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                    }

                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
                else
                {
                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
            }
            fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
            return fileDecodedData.ToString();
        }

        private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
        {
            IValidationTypeManger validator = new ValidationTypeManager();
            Dictionary<string, string> validatorParams = new Dictionary<string, string>();
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
            switch (inputType)
            {
                case GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator:
                    return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);

                default:
                    return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
            }


        }

        private bool GetLookupIsNull(string lookupName, int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_SAPMessageFields
                        where s.SAPFieldName == lookupName && s.TransactionId == transactionId
                        select s.IsAllowNullValue).FirstOrDefault();

            }

        }

        private int GetLookupId(string LookupName)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames
                        where s.LookupName == LookupName
                        select s.Id).FirstOrDefault();

            }

        }

        private string SAPHeaders(string filename)
        {
            StringBuilder sapFields = new StringBuilder();
            using (var context = new GoodpackEDIEntities())
            {
                int fileMapperId = (from s in context.Trn_SubsiToTransactionFileMapper
                                    where s.Filename == filename
                                    select s.Id).FirstOrDefault();
                List<Trn_MapperFieldSpecs> specs = (from s in context.Trn_MapperFieldSpecs
                                                    where s.SubsiToTransactionMapperId == fileMapperId
                                                    select s).ToList(); 
                Trn_MappingSubsi mapperData = (from s in context.Trn_MappingSubsi
                                               where s.SubsiToTransactionFileMapperId == fileMapperId
                                               select s).FirstOrDefault();
                foreach (var item in specs)
                {
                    int sapFieldSequenceId = (from s in context.Trn_MappingConfiguration
                                              where (((s.SourceFieldSequence == item.FieldSequence) || (s.SourceFieldSeq2 == item.FieldSequence)) && (s.MappingSubsiId == mapperData.Id))
                                              select s.SapFieldSeq).FirstOrDefault();
                    string sapField = (from s in context.Ref_SAPMessageFields
                                       where ((s.FieldSequence == sapFieldSequenceId) && (s.TransactionId == mapperData.SapMessageFieldId))
                                       select s.SAPFieldName).FirstOrDefault();
                    sapFields.Append(sapField);
                    sapFields.Append("" + ConstantUtilities.delimiters[0]);
                }
                return sapFields.Remove(sapFields.Length - 1, 1).ToString();
            }
        }

        #endregion
    }
}
