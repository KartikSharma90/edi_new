﻿using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement;

namespace GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer
{
    public interface IDecodeValueManager
    {
     ITransTypeDecodeValueManager DecodeType(TRANSACTION_TYPES types);       
    }
}
