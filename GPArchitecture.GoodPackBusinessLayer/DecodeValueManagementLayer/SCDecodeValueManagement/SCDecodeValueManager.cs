﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using System;
using System.Linq;

namespace GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement
{
    public class SCDecodeValueManager : ITransTypeDecodeValueManager
    {
        /// <summary>
        /// Input type finder to get the decode value
        /// </summary>
        /// <param name="inputType"></param>
        /// <param name="lookUpCode"></param>
        /// <param name="mapperFile"></param>
        /// <param name="subsiId"></param>
        /// <returns></returns>
        public bool InputTypeFinder(TargetMessageInputTypes inputType)
        {
            switch (inputType)
            { 
                case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING:
                case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELDS2:
                   return true;                  
                default:return false;
            }        
        }
        public string GetLookupName(int lookupNameId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames
                        where s.Id == lookupNameId
                        select s.LookupName).FirstOrDefault();

            }

        }

        /// <summary>
        /// Retrieves decode value from lookUpCode 
        /// </summary>
        /// <param name="lookUpCode"></param>
        /// <param name="mapperFile"></param>
        /// <param name="subsiId"></param>
        /// <returns></returns>
        public string FindDecodeValue(string lookUpCode,string mapperFile,int subsiId,int lookupNameId)
        {
            using(var context=new GoodpackEDIEntities())
            {
                int codeLeng;
                bool isConsignee = false;
                bool isPacker = false;
                bool isCustomercode = false;
                codeLeng = lookUpCode.Length;
                string lookupName = GetLookupName(lookupNameId);
                switch(lookupName.ToUpper())
                {
                    case "CONSIGNEE":if(lookUpCode.StartsWith("6") && codeLeng==6)
                        {
                            isConsignee = true;
                        }
                        break;
                    case "CUSTOMER CODE": if (lookUpCode.StartsWith("3") && codeLeng == 6)
                        {
                            isCustomercode = true;
                        }
                        break;
                    case "PACKER": if (lookUpCode.StartsWith("5") && codeLeng == 6)
                        {
                            isPacker = true;
                        }
                        break;
                }
               // if((lookUpCode.StartsWith("5"))||(lookUpCode.StartsWith("6"))||(lookUpCode.StartsWith("3")) && codeLeng==6)
                if(isConsignee||isPacker||isCustomercode)
                {
                return String.Empty;
                }
                else
                {
                   return (from s in context.Trn_LookupValues
                                    where ((s.LookupCode == lookUpCode) && (s.SubsiId == subsiId) && (s.MapperName == mapperFile) && (s.LookupNameId==lookupNameId))
                                    select s.LookupDecodeValue).FirstOrDefault();               
                }
            }
        }
    }
}