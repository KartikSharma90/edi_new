﻿using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement
{
   public interface ITransTypeDecodeValueManager
    {
         bool InputTypeFinder(TargetMessageInputTypes inputType);
         string FindDecodeValue(string lookUpCode, string mapperFile, int subsiId,int lookupNameId);       
        
    }
}
