﻿using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement;

namespace GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer
{

    public class DecodeValueManager : IDecodeValueManager
    {


        public ITransTypeDecodeValueManager DecodeType(TRANSACTION_TYPES types)
        {          
            switch (types)
            {
                case TRANSACTION_TYPES.SC: 
                    return new SCDecodeValueManager();                   
                   
                default: return new SCDecodeValueManager();
            } 
        }
    }
}