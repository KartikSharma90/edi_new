﻿using System.Collections;
using System.Collections.Generic;
using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.LookUpManagementLayer
{
    public interface ILookupManager
    {
        void AddLookup(ArrayList rowDetailsArr, string Username);
        void AddLookupForUpload(ArrayList rowDetailsArr, string Username);
        List<ViewLookupModel> GetLookupDetails(string username, int susbyId, int lookupNameId, string mapperName);

        void EditLookup(LookupModel LookupModelData, string Username);
        void EditLookupNew(LookUpModelUpdate LookupModelData, string Username);
        void DeleteSelectedLookupDetails(int lookupRowId, string Username);

    }
}
