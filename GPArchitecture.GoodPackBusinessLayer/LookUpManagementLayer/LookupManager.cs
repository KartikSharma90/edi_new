﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Transactions;
using GPArchitecture.DataLayer.Models;
using log4net;
using System.Data.Entity.Infrastructure;
using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.LookUpManagementLayer
{
    public static class LinqExtensions
    {
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            if (condition)
                return source;
            else
                return source.Where(predicate);
        }
    }


    public class LookupManager : ILookupManager
    {
        ResponseModel responseModel = new ResponseModel();
        private static readonly ILog log = LogManager.GetLogger(typeof(LookupManager));

         public void AddLookup(ArrayList rowDetailsArr, string Username)
        {

            var parsedObjectfirstRow = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());

            int subsyId = int.Parse(parsedObjectfirstRow[0].ToString());
            string mapperName = (parsedObjectfirstRow[1].ToString());
            int lookupNameId = int.Parse(parsedObjectfirstRow[2].ToString());
            using (TransactionScope transaction = new TransactionScope())
            {

                GoodpackEDIEntities dataContext = new GoodpackEDIEntities();
                Gen_User user = (from s in dataContext.Gen_User
                                 where s.Username == Username
                                 select s).FirstOrDefault();


                for (int i = 1; i < (rowDetailsArr.Count); i++)
                {
                    try
                    {
                        var parsedObject = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[i].ToString());

                        var lookupValueDetails = new Trn_LookupValues()
                        {
                            SubsiId = subsyId,
                            LookupNameId = lookupNameId,
                            MapperName = mapperName,
                            IsDuplicated = false,
                            LookupCode = parsedObject[0].ToString().Trim(),
                            LookupDecodeValue = parsedObject[1].ToString().Trim(),
                            Userid = user.Id
                        };

                        dataContext.Trn_LookupValues.Add(lookupValueDetails);
                        dataContext.SaveChanges();

                    }
                    catch(DbUpdateException exe)
                    {
                        log.Error(" AddLookup method in LookupManager for DbupdateException .rowDetailsArr:" + rowDetailsArr + ", Username:" + Username + ". Api Exception : Message- " + exe.Message);
                       
                        throw exe;
                    }
                    catch (Exception ex)
                    {
                        
                        log.Error(" AddLookup method in LookupManager .rowDetailsArr:" + rowDetailsArr + ", Username:" + Username + ". Api Exception : Message- " + ex.Message);
                        throw ex;

                    }
                    

                }
                transaction.Complete();
            }
        }
         public void AddLookupForUpload(ArrayList rowDetailsArr, string Username)
         {

             var parsedObjectfirstRow = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[0].ToString());

             int subsyId = int.Parse(parsedObjectfirstRow[0].ToString());
             string mapperName = (parsedObjectfirstRow[1].ToString());
             string lookupNameId = parsedObjectfirstRow[2].ToString();


             using (TransactionScope transaction = new TransactionScope())
             {

                 GoodpackEDIEntities dataContext = new GoodpackEDIEntities();
                 Gen_User user = (from s in dataContext.Gen_User
                                  where s.Username == Username
                                  select s).FirstOrDefault();

                 var lookUpId = (from s in dataContext.Ref_LookupNames
                                 where s.LookupName == lookupNameId
                                 select s.Id).FirstOrDefault();
                 for (int i = 1; i < (rowDetailsArr.Count); i++)
                 {
                     try
                     {
                         var parsedObject = JsonConvert.DeserializeObject<JArray>(rowDetailsArr[i].ToString());

                         var lookupValueDetails = new Trn_LookupValues()
                         {
                             SubsiId = subsyId,
                             LookupNameId = lookUpId,
                             MapperName = mapperName,
                             IsDuplicated = false,
                             LookupCode = parsedObject[0].ToString(),
                             LookupDecodeValue = parsedObject[1].ToString(),
                             Userid = user.Id
                         };

                         dataContext.Trn_LookupValues.Add(lookupValueDetails);
                         dataContext.SaveChanges();

                     }
                     catch (DbUpdateException exe)
                     {
                         log.Error(" AddLookup method in LookupManager for DbupdateException .rowDetailsArr:" + rowDetailsArr + ", Username:" + Username + ". Api Exception : Message- " + exe.Message);

                         throw exe;
                     }
                     catch (Exception ex)
                     {

                         log.Error(" AddLookup method in LookupManager .rowDetailsArr:" + rowDetailsArr + ", Username:" + Username + ". Api Exception : Message- " + ex.Message);
                         throw ex;

                     }


                 }
                 transaction.Complete();
             }
         }
        public void EditLookup(LookupModel LookupModelData, string Username)
        {
          
                GoodpackEDIEntities dataContext = new GoodpackEDIEntities();
                Gen_User user = (from s in dataContext.Gen_User
                                 where s.Username == Username
                                 select s).FirstOrDefault();
               
                    try
                    {
                        Trn_LookupValues lookupValueDetails = (from s in dataContext.Trn_LookupValues
                                                               where (s.Id == LookupModelData.UniqueId)
                                                               select s).FirstOrDefault();
                        lookupValueDetails.LookupCode = LookupModelData.CodeValue.ToString();
                        lookupValueDetails.LookupDecodeValue = LookupModelData.DecodeValue.ToString();             
                        dataContext.SaveChanges();

                    }
                    catch (DbUpdateException exe)
                    {
                        log.Error(" EditLookup method in LookupManager for DbupdateException " + ". Api Exception : Message- " + exe.Message);

                        throw exe;
                    }
                    catch (Exception ex)
                    {
                        log.Error(" EditLookup method in LookupManager,LookupModelData:" +LookupModelData + ", Username:" + Username + ". Api Exception : Message- " + ex.Message);
                        throw ex;
                    }

        }
        public void DeleteSelectedLookupDetails(int lookupRowId, string Username)
        {
            using (var context = new GoodpackEDIEntities())
            {
                try
                {
                    Trn_LookupValues lookupValueDetails = context.Trn_LookupValues.Find(lookupRowId);
                    context.Trn_LookupValues.Remove(lookupValueDetails);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    log.Error("DeleteLookup method in LookupManager" + ", Username:" + Username + " Api Exception : Message- " + ex.Message);
                    throw ex;
                }
            }
        }

        public void EditLookupNew(LookUpModelUpdate LookupModelData, string Username)
        {

            GoodpackEDIEntities dataContext = new GoodpackEDIEntities();
            Gen_User user = (from s in dataContext.Gen_User
                             where s.Username == Username
                             select s).FirstOrDefault();

            try
            {
                Trn_LookupValues lookupdata = (from s in dataContext.Trn_LookupValues
                                    where s.Ref_LookupNames.LookupName==LookupModelData.LookupName && s.MapperName==LookupModelData.MapperName
                                    && s.LookupCode==LookupModelData.LookupCode
                                    select s).FirstOrDefault();
                lookupdata.LookupDecodeValue = LookupModelData.LookupDecode;
                dataContext.SaveChanges();   

            }
            catch (DbUpdateException exe)
            {
                log.Error(" EditLookup method in LookupManager for DbupdateException " + ". Api Exception : Message- " + exe.Message);

                throw exe;
            }
            catch (Exception ex)
            {
                log.Error(" EditLookup method in LookupManager,LookupModelData:" + LookupModelData + ", Username:" + Username + ". Api Exception : Message- " + ex.Message);
                throw ex;
            }

        }
        public List<ViewLookupModel> GetLookupDetails(string username, int susbyId, int lookupNameId, string mapperName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();

                List<ViewLookupModel> ListViewLookupModel = new List<ViewLookupModel>();

                //if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                //{

                //    List<Trn_LookupValues> ListLookupValuesModel = (context.Trn_LookupValues.Where(i => i.SubsiId == susbyId && i.MapperName == mapperName && i.LookupNameId == lookupNameId && i.Userid == user.Id).ToList());
                //    List<Trn_LookupValues> ListLookupValuesModel = (context.Trn_LookupValues
                //        .Where(i => i.Userid == user.Id)
                //        .WhereIf(string.IsNullOrEmpty(mapperName), i => i.MapperName.Contains(mapperName))
                //        .WhereIf(lookupNameId == 0, i => i.LookupNameId == lookupNameId)
                //        .WhereIf(susbyId == 0, i => i.SubsiId == susbyId)
                //        .WhereIf(susbyId == 0, i => i.SubsiId == susbyId).ToList());


                //    for (int i = 0; i < ListLookupValuesModel.Count; i++)
                //    {
                //        ViewLookupModel LookupModel = new ViewLookupModel();
                //        LookupModel.CodeValue = ListLookupValuesModel[i].LookupCode;
                //        LookupModel.DecodeValue = ListLookupValuesModel[i].LookupDecodeValue;
                //        LookupModel.LookupName = ListLookupValuesModel[i].Ref_LookupNames.LookupName;
                //        LookupModel.MapperName = ListLookupValuesModel[i].MapperName;
                //        LookupModel.SubsyName = ListLookupValuesModel[i].Gen_EDISubsies.SubsiName;
                //        LookupModel.UniqueId = ListLookupValuesModel[i].Id;
                //        ListViewLookupModel.Add(LookupModel);
                //    }

                //    return ListViewLookupModel;
                //}
                // else
                // {

                List<Trn_LookupValues> ListLookupValuesModel = (context.Trn_LookupValues
                    .WhereIf(string.IsNullOrEmpty(mapperName), i => i.MapperName.Contains(mapperName))
                    .WhereIf(lookupNameId == 0, i => i.LookupNameId == lookupNameId)
                    .WhereIf(susbyId == 0, i => i.SubsiId == susbyId)
                    .WhereIf(susbyId == 0, i => i.SubsiId == susbyId).ToList());

                for (int i = 0; i < ListLookupValuesModel.Count; i++)
                {
                    ViewLookupModel LookupModel = new ViewLookupModel();
                    LookupModel.CodeValue = ListLookupValuesModel[i].LookupCode;
                    LookupModel.DecodeValue = ListLookupValuesModel[i].LookupDecodeValue;
                    LookupModel.LookupName = ListLookupValuesModel[i].Ref_LookupNames.LookupName;
                    LookupModel.MapperName = ListLookupValuesModel[i].MapperName;
                    LookupModel.SubsyName = ListLookupValuesModel[i].Gen_EDISubsies.SubsiName;
                    LookupModel.UniqueId = ListLookupValuesModel[i].Id;
                    ListViewLookupModel.Add(LookupModel);
                }

                return ListViewLookupModel;
                // }
            }
        }
    }
}