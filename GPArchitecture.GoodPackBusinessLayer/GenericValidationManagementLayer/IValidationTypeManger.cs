﻿
namespace GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer
{
   public interface IValidationTypeManger
    {
       IDataValidationManager ValidationType(string validationType);
    }
}
