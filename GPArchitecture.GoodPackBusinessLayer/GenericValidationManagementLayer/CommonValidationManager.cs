using GPArchitecture.Models.ViewModels;
 using System;
 using System.Collections.Generic;
 using System.Text;
 
 namespace GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer
 {
     public class CommonValidationManager : IDataValidationManager
     {
         StringBuilder validationMessage = new StringBuilder();
         public string DataValidator(Dictionary<string, string> dataParams)
         {
             string characterMessage = new CharacterValidationManager().DataValidator(dataParams);
             string valueMessage = new ValueValidationManager().DataValidator(dataParams);
             if (characterMessage != String.Empty)
             {
                 validationMessage.Append(characterMessage);
             }
             if (valueMessage != String.Empty)
             {
                 validationMessage.Append(valueMessage);
             }
             return validationMessage.ToString();
         }
         public string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel)
         {
             string characterMessage = new CharacterValidationManager().DataValidator(dataParams, validationModel);
             string valueMessage = new ValueValidationManager().DataValidator(dataParams, validationModel);
             if (characterMessage != String.Empty)
             {
                 validationMessage.Append(characterMessage);
             }
             if (valueMessage != String.Empty)
             {
                 validationMessage.Append(valueMessage);
             }
             return validationMessage.ToString();
         }
 
 
 
     }
 }

 

