﻿

using GPArchitecture.SAPService.ConstantUtils;
namespace GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer
{
    public class ValidationTypeManager :IValidationTypeManger
    {
      public IDataValidationManager ValidationType(string validationType)
        {            
            switch (validationType)
            {
                case ConstantUtilities.DateValidator: return new DateValidationManager();
                default: return new CommonValidationManager();            
            }        
        }

       
    }
}