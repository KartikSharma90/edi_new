﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
using GPArchitecture.Models.ViewModels;
using GPArchitecture.Utilities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer
{
    public class DateValidationManager : IDataValidationManager
    {
        StringBuilder dateValidation = new StringBuilder();

        public string DataValidator(Dictionary<string, string> dataParams)
        {
            string mapperName = dataParams[GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName];
            string fileData = dataParams[GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData];

            string dateFormat = null;
            CultureInfo provider = CultureInfo.InvariantCulture;
            using (var context = new GoodpackEDIEntities())
            {
                dateFormat = (from s in context.Trn_SubsiToTransactionFileMapper
                              join p in context.Trn_MappingSubsi on s.Id equals p.SubsiToTransactionFileMapperId
                              join m in context.Trn_MappingConfiguration on p.Id equals m.MappingSubsiId
                              where (s.Filename == mapperName) && (m.InputDateFormat != null)
                              select m.InputDateFormat).FirstOrDefault();
            }
            try
            {
                if (dateFormat.ToLower() == "ww.yyyy")
                {

                    // TryParseDate(string sourceMessageFormat, string targetFieldDateFormat, string sourceMessageDateFormat, ref bool isDateParsingSuccessful, ref string dateParsingErrorMessage, string dateStringToBeParsed)

                    string val = "";
                    bool isDateParsingSuccessful = true;
                    string fieldValue = GPMapper.TryParseDate("Excel", "DDMMYYYY", dateFormat, ref isDateParsingSuccessful, ref val, fileData);
                    fileData = fieldValue;
                    DateTime.ParseExact(fileData, "ddMMyyyy", provider);
                    return dateValidation.ToString();

                }
                else
                {
                    bool status = false;
                    string message = "";

                    string rsult = SCDataVerificationManager.ExtractDateElements("ddMMyyyy", dateFormat, fileData, out status, out message);

                    if (!status)
                    {
                        message = string.Concat(message, GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.NewLine);
                    }

                    return message;
                }
                //if(dateFormat.ToLower() == ConstantUtilities.MonthDateFormat)
                //{
                //    DateTime fromDateValue;

                //    var formats = new[] { "MM/dd/yyyy", "M/dd/yyyy" };
                //    if (!DateTime.TryParseExact(fileData, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDateValue))
                //    {
                //        dateValidation.Append(ConstantUtilities.DateFormatValidationMessage);
                //        dateValidation.Append(fileData);
                //        dateValidation.Append(ConstantUtilities.NewLine);         
                //    }


                // }

                //else
                //{
                //    DateTime.ParseExact(fileData, dateFormat.ToLower(), provider);
                //}

            }
            catch (Exception)
            {
                return checkDateFormat(fileData, dateFormat.ToLower());
            }


        }

        public string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel)
        {
            string mapperName = dataParams[GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName];
            string fileData = dataParams[GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData];

            string dateFormat = "";

            CultureInfo provider = CultureInfo.InvariantCulture;
            dateFormat = validationModel.DateString;
            //using (var context = new GoodpackEDIEntities())
            //{
            //    dateFormat = (from s in context.Trn_SubsiToTransactionFileMapper
            //                  join p in context.Trn_MappingSubsi on s.Id equals p.SubsiToTransactionFileMapperId
            //                  join m in context.Trn_MappingConfiguration on p.Id equals m.MappingSubsiId
            //                  where (s.Filename == mapperName) && (m.InputDateFormat != null)
            //                  select m.InputDateFormat).FirstOrDefault();
            //}
            try
            {
                if (dateFormat.ToLower() == "ww.yyyy")
                {
                    string val = "";
                    bool isDateParsingSuccessful = true;
                    string fieldValue = GPMapper.TryParseDate("Excel", "DDMMYYYY", dateFormat, ref isDateParsingSuccessful, ref val, fileData);
                    fileData = fieldValue;
                    DateTime.ParseExact(fileData, "ddMMyyyy", provider);
                    return dateValidation.ToString();

                }
                else
                {
                    bool status = false;
                    string message = "";

                    string rsult = SCDataVerificationManager.ExtractDateElements("ddMMyyyy", dateFormat, fileData, out status, out message);

                    if (!status)
                    {
                        message = string.Concat(message, GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.NewLine);
                    }
                    return message;
                }

            }
            catch (Exception)
            {
                return checkDateFormat(fileData, dateFormat.ToLower());
            }


        }
        public string checkDateFormat(string date, string format)
        {
            try
            {
                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime.ParseExact(date, format, provider);
            }
            catch (Exception e)
            {
                dateValidation.Append(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateFormatValidationMessage);
                dateValidation.Append(date);
                dateValidation.Append(" ." + GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateFormatValidationeMapperMessage + "( " + format + " )");
                dateValidation.Append(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.NewLine);
            }
            return dateValidation.ToString();
        }

    }

}