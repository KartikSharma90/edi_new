using GPArchitecture.Models.ViewModels;
 using System.Collections.Generic;
 
 namespace GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer
 {
    public interface IDataValidationManager
     {
          string DataValidator(Dictionary<string,string> dataParams);
          string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel);
          
     }
 }



