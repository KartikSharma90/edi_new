﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GPArchitecture.GoodPackBusinessLayer.PSSCManagementLayer
{
    public class MsgSapPSSCLine
    {
        // Field list
        StringBuilder message;

        // Fields
        public string BatchNumber;
        string MvtType;
        string FromLocation;
        string ToLocation;
        public string BinNo;
        public string CustomerReferenceNumber;
        string CustomerPONumber;
        public string ITRNumber;
        public string MaterialNumber;
        string Quantity;
        string BaseUOM;
        string ContainerNumber;
        string ETADate;
        string ETDDate;
        string CustomerName;
        string ScanDate;
        string Status;
        public string CustomerCode;
        string CreatedDate;
        string EntryTime;
        private string psscLineContent;
        public MsgSapPSSCLine(IList<string> fieldListSrc)
        {
            if (fieldListSrc.Count < 20)
            {
                throw new Exception("The SAP Scanned SC line does not have enough fields.");
            }

            BatchNumber = fieldListSrc[0];
            MvtType = fieldListSrc[1];
            FromLocation = fieldListSrc[2];
            ToLocation = fieldListSrc[3];
            BinNo = fieldListSrc[4];
            CustomerReferenceNumber = fieldListSrc[5];
            CustomerPONumber = fieldListSrc[6];
            ITRNumber = fieldListSrc[7];
            MaterialNumber = fieldListSrc[8];
            Quantity = fieldListSrc[9];
            BaseUOM = fieldListSrc[10];
            ContainerNumber = fieldListSrc[11];
            ETADate = fieldListSrc[12];
            ETDDate = fieldListSrc[13];
            CustomerName = fieldListSrc[14];
            ScanDate = fieldListSrc[15];
            Status = fieldListSrc[16];
            CustomerCode = fieldListSrc[17];
            CreatedDate = fieldListSrc[18];
            EntryTime = fieldListSrc[19];
        }

        public long getPackerCode()
        {
            if (FromLocation == "" || FromLocation == null)
            { return -1; }
            else
                return long.Parse(FromLocation);
        }

        public void GenerateSAPMessage()
        {
            message = new StringBuilder();
            message.Append(BatchNumber).Append("\t");
            message.Append(MvtType).Append("\t");
            message.Append(FromLocation).Append("\t");
            message.Append(ToLocation).Append("\t");
            message.Append(BinNo).Append("\t");
            message.Append(CustomerReferenceNumber).Append("\t");
            message.Append(CustomerPONumber).Append("\t");
            message.Append(ITRNumber).Append("\t");
            message.Append(MaterialNumber).Append("\t");
            message.Append(Quantity).Append("\t");
            message.Append(BaseUOM).Append("\t");
            message.Append(ContainerNumber).Append("\t");
            message.Append(ETADate).Append("\t");
            message.Append(ETDDate).Append("\t");
            message.Append(CustomerName).Append("\t");
            message.Append(ScanDate).Append("\t");
            message.Append(Status).Append("\t");
            message.Append(CustomerCode).Append("\t");
            message.Append(CreatedDate).Append("\t");
            message.Append(EntryTime);
        }


        public string Message
        {
            get { return this.message.ToString(); }
        }

        public string PSSCLineContent
        {
            get { return this.psscLineContent; }
            set { this.psscLineContent = value; }
        }
    }
}