﻿using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer
{
   public interface ITransactionManager
    {
        SCTransBatchReturnModel GetSCTransBatchDetails(string username, int subsyCode, int transactionCode, int page, int rows);
        List<SCTransLineModel> GetSCTransLineDetails(int transactionId);
        IList<EntityModel> GetAllTransaction();
        List<SSCTransLineModel> GetSSCTransLineDetails(int transactionId);
        List<SOTransLineModel> GetSOTransLineDetails(int transactionId);
        List<ASNTransLineModel> GetASNTransLineDetails(int transactionId);
        ErrorEmailReturnModel GetEmailErrorDetails(string username, int page, int rows);
        List<SCTransLineModel> GetScannedTransLineDetails(int transactionId);
        List<SCTransBatchModel> GetSSCTransBatchDetails(string username, int subsyCode, int transactionCode);
        List<SSCTransLineModelNew> GetScannedTransLineDetailsNew(int transactionId);
        List<SCTransBatchModel> GetSCTransBatchDetails(string username, int subsyCode, int transactionCode);
        List<SOTransLineModel> GetSOTransLineDetailsNew(int transactionId);
        
        //SCTransBatchReturnModel GetSearchField(string p, int subsyCode, int transactionCode, int page, int rows);
    }
}
