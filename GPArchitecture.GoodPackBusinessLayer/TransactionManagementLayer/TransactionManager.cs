﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GPArchitecture.DataLayer.Models;
using System.Text;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer
{
    public class TransactionManager : ITransactionManager
    {

        public List<SSCTransLineModel> GetSSCTransLineDetails(int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                List<SSCTransLineModel> ListTransLineModel = new List<SSCTransLineModel>();

                List<Trn_SSCTransLine> SSCTransLineDetails = (context.Trn_SSCTransLine.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SSCTransLineDetails.Count; i++)
                {
                    var responseList = new List<string>();
                    SSCTransLineModel TransLineModel = new SSCTransLineModel();
                    int lineID = SSCTransLineDetails[i].Id;
                    List<Trn_SSCTransLineContent> SSCTransLineContentDetails = (context.Trn_SSCTransLineContent.Where(s => s.BatchID == transactionId && s.LineId == lineID).ToList());
                    for (int j = 0; j < SSCTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        SAPResponse = SSCTransLineContentDetails[j].SapResponseMessage;
                        responseList.Add(SAPResponse);
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SSCTransLineDetails[i].LineContent;
                    TransLineModel.Status = SSCTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SSCTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SSCTransLineDetails[i].Id;
                    if (SSCTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }
                    // TransLineModel.Mode = SCTransLineDetails[i].InTestMode;
                    TransLineModel.ReferenceNumber = SSCTransLineDetails[i].CustomerReferenceNumber;
                    TransLineModel.packerLocation = SSCTransLineDetails[i].FromLocation;
                    TransLineModel.consigneeLocation = SSCTransLineDetails[i].ToLocation;
                    TransLineModel.BinType = SSCTransLineDetails[i].MaterialNumber;
                    TransLineModel.ETD = SSCTransLineDetails[i].ETDDate;
                    TransLineModel.ETA = SSCTransLineDetails[i].ETADate;
                    TransLineModel.ScanDate = SSCTransLineDetails[i].ScanDate;

                    ListTransLineModel.Add(TransLineModel);
                }

                return ListTransLineModel;
            }

        }

        public List<SOTransLineModel> GetSOTransLineDetails(int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                List<SOTransLineModel> ListTransLineModel = new List<SOTransLineModel>();
                List<Trn_SOTransLine> SOTransLineDetails = (context.Trn_SOTransLine.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());

                for (int i = 0; i < SOTransLineDetails.Count; i++)
                {
                    var responseList = new List<string>();
                    SOTransLineModel TransLineModel = new SOTransLineModel();
                    int lineID = SOTransLineDetails[i].Id;
                    List<Trn_SOTransLineContent> SOTransLineContentDetails = (context.Trn_SOTransLineContent.Where(s => s.BatchID == transactionId && s.LineID == lineID).ToList());
                    for (int j = 0; j < SOTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        SAPResponse = SOTransLineContentDetails[j].SapResponseMessage;
                        responseList.Add(SAPResponse);
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SOTransLineDetails[i].LineContent;
                    TransLineModel.Status = SOTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SOTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SOTransLineDetails[i].Id;
                    if (SOTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }

                    TransLineModel.CustomerPONumber = SOTransLineDetails[i].CustomerPONumber;
                    TransLineModel.CustomerPODate = SOTransLineDetails[i].CustomerPODate;
                    TransLineModel.CustomerNumber = SOTransLineDetails[i].CustomerNumber;
                    TransLineModel.ReqDeliveryDate = SOTransLineDetails[i].ReqDeliveryDate;
                    TransLineModel.MaterialNumber = SOTransLineDetails[i].MaterialNumber;
                    TransLineModel.FromLocation = SOTransLineDetails[i].FromLocation;
                    TransLineModel.SOQuantity = SOTransLineDetails[i].SOQuantity;

                    TransLineModel.ETA = SOTransLineDetails[i].ETA;
                    TransLineModel.ETD = SOTransLineDetails[i].ETD;
                    TransLineModel.POL = SOTransLineDetails[i].POL;
                    TransLineModel.POD = SOTransLineDetails[i].POD;

                    ListTransLineModel.Add(TransLineModel);
                }


                return ListTransLineModel;
            }

        }
        //public SCTransBatchReturnModel  GetSearchField(string serachString, int subsyCode, int transactionCode, int page, int rows)
        // {
        //      using (var context = new GoodpackEDIEntities())
        //      {


        //      }

        //  }

        public SCTransBatchReturnModel GetSCTransBatchDetails(string username, int subsyCode, int transactionCode, int page, int rows)
        {

            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                bool GYuser = false;
                int subsiId = (from s in context.Trn_UserToSubsiMapper
                               where s.UserId == user.Id
                               select s.SubsiId).FirstOrDefault();
                string SubsiName = (from s in context.Gen_EDISubsies
                                    where s.Id == subsiId
                                    select s.SubsiName).FirstOrDefault();
                if (SubsiName == GPArchitecture.Cache.Config.getConfig("GOODYEARSUBSY"))
                {
                    GYuser = true;
                }

                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();

                SCTransBatchReturnModel TransBatchReturnModel = new SCTransBatchReturnModel();
                List<SCTransBatchModel> ListTransBatchModel = new List<SCTransBatchModel>();

                if ((role == ConstantUtility.SubsiUser && GYuser == true) || (role == ConstantUtility.HQUser && GYuser == true))
                {
                    string GYMapperFileSO = GPArchitecture.Cache.Config.getConfig("GoodYearSalesOrder");
                    string GYMapperFileSC = GPArchitecture.Cache.Config.getConfig("GoodYearShipmentConfirmation");
                    int totalrows1;
                    if (transactionCode != 11)
                    {
                        // int totalrows1 = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode && (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO || i.UserId == user.Id)).ToList()).Count();
                        totalrows1 = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).ToList()).Count();
                        List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            TransBatchModel.DeletedCount = SCTransBatchDetails[i].DeletedRecordCount;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                    //**for including Scanner details to the list Anoop**//
                    else
                    {
                        totalrows1 = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).ToList()).Count();
                        List<Trn_SSCTransBatch> SSCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        for (int i = 0; i < SSCTransBatchDetails.Count; i++)
                        {

                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SSCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SSCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SSCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SSCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = " ";
                            TransBatchModel.TotalRecords = SSCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.MapperName = SSCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SSCTransBatchDetails[i].DateCreated;
                           // TransBatchModel.DeletedCount = SCTransBatchDetails[i].DeletedRecordCount;
                            ListTransBatchModel.Add(TransBatchModel);
                        }

                    }
                    //****//
                    // TransBatchReturnModel.totalRecordCount = totalrows;
                    TransBatchReturnModel.totalRecordCount = totalrows1;
                    TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
                    return TransBatchReturnModel;
                }
                else if ((role == ConstantUtility.SubsiUser && GYuser == false) || (role == ConstantUtility.HQUser && GYuser == false))
                {

                    int totalrows;
                    if (transactionCode != 11)
                    {
                        //  int totalrows = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.UserId == user.Id && i.TransactionId == transactionCode).ToList()).Count();
                        totalrows = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).ToList()).Count();
                        List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        //List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.UserId == user.Id && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());

                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                    //**for including Scanner details to the list Anoop**//
                    else
                    {
                        totalrows = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode).ToList()).Count();
                        List<Trn_SSCTransBatch> SSCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        for (int i = 0; i < SSCTransBatchDetails.Count; i++)
                        {

                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SSCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SSCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SSCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SSCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = " ";
                            TransBatchModel.TotalRecords = SSCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.MapperName = SSCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SSCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                    //****//
                    TransBatchReturnModel.totalRecordCount = totalrows;
                    //TransBatchReturnModel.totalRecordCount = totalrows1;
                    TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
                    return TransBatchReturnModel;

                }
                else
                {
                    int totalrows;
                    if (transactionCode != 11)
                    {
                        totalrows = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).ToList()).Count();
                        List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                    //**for including Scanner details to the list Anoop**//
                    else
                    {
                        totalrows = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode).ToList()).Count();
                        List<Trn_SSCTransBatch> SSCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                        for (int i = 0; i < SSCTransBatchDetails.Count; i++)
                        {

                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SSCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SSCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SSCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SSCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = " ";
                            TransBatchModel.TotalRecords = SSCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount = Convert.ToInt32(SSCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.MapperName = SSCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SSCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                    //****//
                    TransBatchReturnModel.totalRecordCount = totalrows;
                    TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
                    return TransBatchReturnModel;
                }
            }

        }

        public ErrorEmailReturnModel GetEmailErrorDetails(string username, int page, int rows)
        {
            int totalrows;
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();
                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();


                ErrorEmailReturnModel EmailReturnModel = new ErrorEmailReturnModel();
                List<ErrorEmailDetailsModel> ListEmailBatchModel = new List<ErrorEmailDetailsModel>();
                if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                {
                    List<int> subsiCode = (from s in context.Trn_UserToSubsiMapper
                                           where s.UserId == user.Id
                                           select s.SubsiId).ToList();


                    totalrows = (context.Gen_TransactionValidationTracker.Where(p => subsiCode.Any(p2 => p2 == p.SubsiId)).ToList()).Count();
                    List<Gen_TransactionValidationTracker> ErrorEmailDetails = (context.Gen_TransactionValidationTracker.Where(p => subsiCode.Any(p2 => p2 == p.SubsiId)).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());

                    for (int i = 0; i < ErrorEmailDetails.Count; i++)
                    {
                        ErrorEmailDetailsModel EmailBatchModel = new ErrorEmailDetailsModel();
                        EmailBatchModel.UniqueId = ErrorEmailDetails[i].Id;
                        if (ErrorEmailDetails[i].SubsiId == null)
                        {
                            EmailBatchModel.SubsyName = String.Empty;
                        }
                        else
                        {
                            EmailBatchModel.SubsyName = ErrorEmailDetails[i].Gen_EDISubsies.SubsiName;
                        }

                        EmailBatchModel.EmailId = ErrorEmailDetails[i].EmailId;
                        EmailBatchModel.TransactionName = ErrorEmailDetails[i].Ref_TransactionTypes.TransactionCode;
                        EmailBatchModel.FileName = ErrorEmailDetails[i].FileName;
                        EmailBatchModel.FileContent = ErrorEmailDetails[i].FileContent;
                        EmailBatchModel.MapperName = ErrorEmailDetails[i].MapperName;
                        EmailBatchModel.CreatedDate = ErrorEmailDetails[i].CreatedDateTime.ToString();
                        EmailBatchModel.ErrorMessage = ErrorEmailDetails[i].ValidationErrorMessage;
                        ListEmailBatchModel.Add(EmailBatchModel);
                    }
                    EmailReturnModel.totalRecordCount = totalrows;
                    EmailReturnModel.EmailBatchModel = ListEmailBatchModel;


                }
                else
                {
                    totalrows = (context.Gen_TransactionValidationTracker.ToList()).Count();
                    List<Gen_TransactionValidationTracker> ErrorEmailDetails = (context.Gen_TransactionValidationTracker.OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());
                    for (int i = 0; i < ErrorEmailDetails.Count; i++)
                    {
                        ErrorEmailDetailsModel EmailBatchModel = new ErrorEmailDetailsModel();
                        EmailBatchModel.UniqueId = ErrorEmailDetails[i].Id;
                        if (ErrorEmailDetails[i].SubsiId == null)
                        {
                            EmailBatchModel.SubsyName = String.Empty;
                        }
                        else
                        {
                            EmailBatchModel.SubsyName = ErrorEmailDetails[i].Gen_EDISubsies.SubsiName;
                        }

                        EmailBatchModel.EmailId = ErrorEmailDetails[i].EmailId;
                        EmailBatchModel.TransactionName = ErrorEmailDetails[i].Ref_TransactionTypes.TransactionCode;
                        EmailBatchModel.FileName = ErrorEmailDetails[i].FileName;
                        EmailBatchModel.FileContent = ErrorEmailDetails[i].FileContent;
                        EmailBatchModel.MapperName = ErrorEmailDetails[i].MapperName;
                        EmailBatchModel.CreatedDate = ErrorEmailDetails[i].CreatedDateTime.ToString();
                        EmailBatchModel.ErrorMessage = ErrorEmailDetails[i].ValidationErrorMessage;
                        ListEmailBatchModel.Add(EmailBatchModel);
                    }

                    EmailReturnModel.totalRecordCount = totalrows;
                    EmailReturnModel.EmailBatchModel = ListEmailBatchModel;
                }
                return EmailReturnModel;
            }
        }


        public List<SCTransLineModel> GetSCTransLineDetails(int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                List<SCTransLineModel> ListTransLineModel = new List<SCTransLineModel>();

                List<Trn_SCTransLine> SCTransLineDetails = (context.Trn_SCTransLine.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SCTransLineDetails.Count; i++)
                {
                    SCTransLineModel TransLineModel = new SCTransLineModel();
                    var responseList = new List<string>();
                    int lineID = SCTransLineDetails[i].Id;
                    List<Trn_SCTransLineContent> SCTransLineContentDetails = (context.Trn_SCTransLineContent.Where(s => s.BatchID == transactionId && s.LineID == lineID).ToList());
                    for (int j = 0; j < SCTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        if (SCTransLineContentDetails[j].StatusID == 6)
                        {
                            if (SCTransLineDetails[i].InTestMode)
                                SAPResponse = "Verified with SAP but not processed";
                            else
                                SAPResponse = "Success";
                            responseList.Add(SAPResponse);
                        }
                        else
                        {
                            SAPResponse = SCTransLineContentDetails[j].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SCTransLineDetails[i].LineContent;
                    TransLineModel.Status = SCTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SCTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SCTransLineDetails[i].Id;
                    if (SCTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }
                    //TransLineModel.Mode = SCTransLineDetails[i].InTestMode;
                    TransLineModel.ReferenceNumber = SCTransLineDetails[i].CustomerRefNumber;
                    TransLineModel.packerLocation = SCTransLineDetails[i].FromLocation;
                    TransLineModel.consigneeLocation = SCTransLineDetails[i].ToLocation;
                    TransLineModel.BinType = SCTransLineDetails[i].BinType;
                    TransLineModel.Qty = SCTransLineDetails[i].Quantity.ToString();
                    TransLineModel.ETD = SCTransLineDetails[i].ETD;
                    TransLineModel.CustomerCode = SCTransLineDetails[i].CustomerCode;
                    ListTransLineModel.Add(TransLineModel);
                }

                return ListTransLineModel;
            }

        }
        public List<ASNTransLineModel> GetASNTransLineDetails(int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                List<ASNTransLineModel> ListTransLineModel = new List<ASNTransLineModel>();

                List<Trn_ASNTransLine> SCTransLineDetails = (context.Trn_ASNTransLine.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SCTransLineDetails.Count; i++)
                {
                    ASNTransLineModel TransLineModel = new ASNTransLineModel();
                    var responseList = new List<string>();
                    int lineID = SCTransLineDetails[i].Id;
                    List<Trn_ASNTransLineContent> SCTransLineContentDetails = (context.Trn_ASNTransLineContent.Where(s => s.BatchID == transactionId && s.LineID == lineID).ToList());
                    for (int j = 0; j < SCTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        if (SCTransLineContentDetails[j].StatusID == 6)
                        {
                            SAPResponse = "Success";
                            responseList.Add(SAPResponse);
                        }
                        else
                        {
                            SAPResponse = SCTransLineContentDetails[j].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SCTransLineDetails[i].LineContent;
                    TransLineModel.Status = SCTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SCTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SCTransLineDetails[i].Id;
                    if (SCTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }
                    //TransLineModel.Mode = SCTransLineDetails[i].InTestMode;
                    TransLineModel.ReferenceNumber = SCTransLineDetails[i].CustomerRefNumber;
                    TransLineModel.packerLocation = SCTransLineDetails[i].FromLocation;
                    TransLineModel.consigneeLocation = SCTransLineDetails[i].ToLocation;
                    TransLineModel.BinType = SCTransLineDetails[i].BinType;
                    TransLineModel.ETD = SCTransLineDetails[i].ETD;
                    ListTransLineModel.Add(TransLineModel);
                }

                return ListTransLineModel;
            }

        }

        public IList<EntityModel> GetAllTransaction()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_TransactionTypes
                        where ((s.IsEnabled == true) && (s.IsHomePageDisplay == true))
                        orderby s.TransactionName
                        select new EntityModel
                        {
                            EntityCode = s.Id,
                            EntityName = s.TransactionName
                        }).ToList();

            }
        }


        public List<SCTransLineModel> GetScannedTransLineDetails(int transactionId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<SCTransLineModel> ListTransLineModel = new List<SCTransLineModel>();
                List<Trn_SSCTransLineNew> SCTransLineDetails = (context.Trn_SSCTransLineNew.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SCTransLineDetails.Count; i++)
                {
                    SCTransLineModel TransLineModel = new SCTransLineModel();
                    var responseList = new List<string>();
                    int lineID = SCTransLineDetails[i].Id;
                    List<Trn_SSCTransLineContentNew> SCTransLineContentDetails = (context.Trn_SSCTransLineContentNew.Where(s => s.BatchID == transactionId && s.LineId == lineID).ToList());
                    for (int j = 0; j < SCTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        if (SCTransLineContentDetails[j].StatusId == 6)
                        {
                            SAPResponse = "Success";
                            responseList.Add(SAPResponse);
                        }
                        else
                        {
                            SAPResponse = SCTransLineContentDetails[j].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SCTransLineDetails[i].LineContent;
                    TransLineModel.Status = SCTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SCTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SCTransLineDetails[i].Id;
                    if (SCTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }
                    //TransLineModel.Mode = SCTransLineDetails[i].InTestMode;
                    TransLineModel.ReferenceNumber = SCTransLineDetails[i].CustomerRefNumber;
                    TransLineModel.packerLocation = SCTransLineDetails[i].FromLocation;
                    TransLineModel.consigneeLocation = SCTransLineDetails[i].ToLocation;
                    TransLineModel.BinType = SCTransLineDetails[i].BinType;
                    TransLineModel.Qty = SCTransLineDetails[i].Quantity.ToString();
                    TransLineModel.ETD = SCTransLineDetails[i].ETD;
                    ListTransLineModel.Add(TransLineModel);
                }
                return ListTransLineModel;
            }
        }

        public List<SSCTransLineModelNew> GetScannedTransLineDetailsNew(int transactionId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<SSCTransLineModelNew> ListTransLineModel = new List<SSCTransLineModelNew>();
                List<Trn_SSCTransLineNew> SCTransLineDetails = (context.Trn_SSCTransLineNew.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SCTransLineDetails.Count; i++)
                {
                    SSCTransLineModelNew TransLineModel = new SSCTransLineModelNew();
                    var responseList = new List<string>();
                    int lineID = SCTransLineDetails[i].Id;
                    List<Trn_SSCTransLineContentNew> SCTransLineContentDetails = (context.Trn_SSCTransLineContentNew.Where(s => s.BatchID == transactionId && s.LineId == lineID).ToList());
                    for (int j = 0; j < SCTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        if (SCTransLineContentDetails[j].StatusId == 6)
                        {
                            SAPResponse = "Success";
                            responseList.Add(SAPResponse);
                        }
                        else
                        {
                            SAPResponse = SCTransLineContentDetails[j].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SCTransLineDetails[i].LineContent;
                    TransLineModel.Status = SCTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SCTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SCTransLineDetails[i].Id;
                    if (SCTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }
                    //TransLineModel.Mode = SCTransLineDetails[i].InTestMode;
                    TransLineModel.ReferenceNumber = SCTransLineDetails[i].CustomerRefNumber;
                    TransLineModel.packerLocation = SCTransLineDetails[i].FromLocation;
                    TransLineModel.consigneeLocation = SCTransLineDetails[i].ToLocation;
                    TransLineModel.BinType = SCTransLineDetails[i].BinType;
                    TransLineModel.Qty = SCTransLineDetails[i].Quantity.ToString();
                    TransLineModel.ETD = SCTransLineDetails[i].ETD;
                    int batchId = Convert.ToInt32(SCTransLineDetails[i].BatchId);
                    TransLineModel.CustomerCode = SCTransLineDetails[i].CustomerCode;
                    StringBuilder sb=new StringBuilder();
                    int? lineNumber=SCTransLineDetails[i].LineNumber;
                    string siValue = context.Trn_ScannedShipmentDetails.Where(x => x.Id == lineNumber).Select(x => x.SINumber).FirstOrDefault();
                    var barcodeVlaue = context.Trn_ScannedShipmentDetails.Where(x => x.SINumber == siValue && x.BatchId == batchId).Select(y => new { y.Barcode, y.SAPBinStatus, y.SAPBinSyncMessage }).ToArray();
                        //context.Trn_ScannedShipmentDetails.Where(x => x.BatchId == batchId).Select(y => new { y.Barcode,y.SAPBinStatus,y.SAPBinSyncMessage }).ToArray();
                    int count=0;
                    foreach (var s in barcodeVlaue)
                    {
                        string status="";
                        int statusID=0;
                        string sapBinStatus="";
                        if (!string.IsNullOrEmpty(s.SAPBinStatus.ToString()))
                            statusID = Convert.ToInt32(s.SAPBinStatus.ToString());
                        if (s.SAPBinSyncMessage != null)
                            sapBinStatus = s.SAPBinSyncMessage.Replace(',', ';');
                        if (count == barcodeVlaue.Length - 1)
                        {
                            status = string.IsNullOrEmpty(s.SAPBinStatus.ToString()) ? "" : context.Trn_LineStatus.Where(j => j.Id == statusID).Select(j => j.StatusCode).FirstOrDefault();
                            sb.Append(s.Barcode + '|' + status + '|' + sapBinStatus);
                        }
                        else
                        {
                            status = string.IsNullOrEmpty(s.SAPBinStatus.ToString()) ? "" : context.Trn_LineStatus.Where(j => j.Id == statusID).Select(j => j.StatusCode).FirstOrDefault();
                            sb.Append(s.Barcode + '|' + status + '|' + sapBinStatus + ',');
                        }
                        count++;
                    }
                    TransLineModel.BarcodeNumbers = sb.ToString();// string.Join(",",context.Trn_ScannedShipmentDetails.Where(x => x.BatchId == batchId).Select(y =>y.Barcode).ToArray());
                    ListTransLineModel.Add(TransLineModel);
                }
                return ListTransLineModel;
            }
        }

        public List<SCTransBatchModel> GetSSCTransBatchDetails(string username, int subsyCode, int transactionCode)
        {
            transactionCode = transactionCode == 11 ? 3 : transactionCode;
            List<SCTransBatchModel> ListTransBatchModel = new List<SCTransBatchModel>();
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    Gen_User user = (from s in context.Gen_User
                                     where s.Username == username
                                     select s).FirstOrDefault();

                    bool GYuser = false;
                    int subsiId = (from s in context.Trn_UserToSubsiMapper
                                   where s.UserId == user.Id
                                   select s.SubsiId).FirstOrDefault();
                    string SubsiName = (from s in context.Gen_EDISubsies
                                        where s.Id == subsiId
                                        select s.SubsiName).FirstOrDefault();
                    if (SubsiName == GPArchitecture.Cache.Config.getConfig("GOODYEARSUBSY"))
                    {
                        GYuser = true;
                    }

                    string role = (from s in context.Ref_UserRoles
                                   where s.Id == user.RoleId
                                   select s.UserRole).FirstOrDefault();

                    SCTransBatchReturnModel TransBatchReturnModel = new SCTransBatchReturnModel();


                    if ((role == ConstantUtility.SubsiUser && GYuser == true) || (role == ConstantUtility.HQUser && GYuser == true))
                    {
                        string GYMapperFileSO = GPArchitecture.Cache.Config.getConfig("GoodYearSalesOrder");
                        string GYMapperFileSC = GPArchitecture.Cache.Config.getConfig("GoodYearShipmentConfirmation");

                        // int totalrows1 = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode && (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO || i.UserId == user.Id)).ToList()).Count();
                        // totalrows1 = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).ToList()).Count();
                        List<Trn_SSCTransBatch> SCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = "";
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount =Convert.ToInt32( SCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount =Convert.ToInt32( SCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }

                        // TransBatchReturnModel.totalRecordCount = totalrows;
                        //TransBatchReturnModel.totalRecordCount = 0;
                        //TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
                        return ListTransBatchModel;
                    }
                    else if ((role == ConstantUtility.SubsiUser && GYuser == false) || (role == ConstantUtility.HQUser && GYuser == false))
                    {
                        List<Trn_SSCTransBatch> SCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).ToList());
                        //List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.UserId == user.Id && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).Skip((page - 1) * rows).Take(rows).ToList());

                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName ="";
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = Convert.ToInt32(SCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount = Convert.ToInt32(SCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                        //TransBatchReturnModel.totalRecordCount = 0;
                        ////TransBatchReturnModel.totalRecordCount = totalrows1;
                        //TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
                        return ListTransBatchModel;
                    }
                    else
                    {
                        List<Trn_SSCTransBatch> SCTransBatchDetails = (context.Trn_SSCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).OrderByDescending(c => c.Id).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = "";
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.SuccessCount = Convert.ToInt32(SCTransBatchDetails[i].RecordCountSuccess);
                            TransBatchModel.ErrorCount = Convert.ToInt32(SCTransBatchDetails[i].RecordCountSAPError);
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            //TransBatchReturnModel.totalRecordCount = 0;
            //TransBatchReturnModel.TransBatchModel = ListTransBatchModel;
            return ListTransBatchModel;
        }

        public List<SCTransBatchModel> GetSCTransBatchDetails(string username, int subsyCode, int transactionCode)
        {

            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                bool GYuser = false;
                int subsiId = (from s in context.Trn_UserToSubsiMapper
                               where s.UserId == user.Id
                               select s.SubsiId).FirstOrDefault();
                string SubsiName = (from s in context.Gen_EDISubsies
                                    where s.Id == subsiId
                                    select s.SubsiName).FirstOrDefault();
                if (SubsiName == GPArchitecture.Cache.Config.getConfig("GOODYEARSUBSY"))
                {
                    GYuser = true;
                }

                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();

                SCTransBatchReturnModel TransBatchReturnModel = new SCTransBatchReturnModel();
                List<SCTransBatchModel> ListTransBatchModel = new List<SCTransBatchModel>();

                if ((role == ConstantUtility.SubsiUser && GYuser == true) || (role == ConstantUtility.HQUser && GYuser == true))
                {
                    string GYMapperFileSO = GPArchitecture.Cache.Config.getConfig("GoodYearSalesOrder");
                    string GYMapperFileSC = GPArchitecture.Cache.Config.getConfig("GoodYearShipmentConfirmation");
                    List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode || (i.MapperFileName == GYMapperFileSC || i.MapperFileName == GYMapperFileSO)).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }          
             
               
                    return ListTransBatchModel;
                }
                else if ((role == ConstantUtility.SubsiUser && GYuser == false) || (role == ConstantUtility.HQUser && GYuser == false))
                { 
                    List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).ToList());
                      
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }                   
      
                    return ListTransBatchModel;
                }
                else
                {               
                        List<Trn_SCTransBatch> SCTransBatchDetails = (context.Trn_SCTransBatch.Where(i => i.SubsyId == subsyCode && i.TransactionId == transactionCode).ToList());
                        for (int i = 0; i < SCTransBatchDetails.Count; i++)
                        {
                            SCTransBatchModel TransBatchModel = new SCTransBatchModel();
                            TransBatchModel.UniqueId = SCTransBatchDetails[i].Id;
                            TransBatchModel.SubsyName = SCTransBatchDetails[i].Gen_EDISubsies.SubsiName;
                            TransBatchModel.TransactionName = SCTransBatchDetails[i].Ref_TransactionTypes.TransactionCode;
                            TransBatchModel.TransactionType = SCTransBatchDetails[i].FileSource;
                            TransBatchModel.FileName = SCTransBatchDetails[i].FileName;
                            TransBatchModel.TotalRecords = SCTransBatchDetails[i].RecordCountTotal;
                            TransBatchModel.MapperName = SCTransBatchDetails[i].MapperFileName;
                            TransBatchModel.SuccessCount = SCTransBatchDetails[i].RecordCountSuccess;
                            TransBatchModel.ErrorCount = SCTransBatchDetails[i].RecordCountSAPError;
                            TransBatchModel.CreatedDate = SCTransBatchDetails[i].DateCreated;
                            ListTransBatchModel.Add(TransBatchModel);
                        }                        
                    return ListTransBatchModel;
                }
            }
        }

        public List<SOTransLineModel> GetSOTransLineDetailsNew(int transactionId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<SOTransLineModel> ListTransLineModel = new List<SOTransLineModel>();
                List<Trn_SOTransLine> SOTransLineDetails = (context.Trn_SOTransLine.Where(i => i.BatchId == transactionId && i.IsActive == true).ToList());
                for (int i = 0; i < SOTransLineDetails.Count; i++)
                {
                    var responseList = new List<string>();
                    SOTransLineModel TransLineModel = new SOTransLineModel();
                    int lineID = SOTransLineDetails[i].Id;
                    List<Trn_SOTransLineContent> SOTransLineContentDetails = (context.Trn_SOTransLineContent.Where(s => s.BatchID == transactionId && s.LineID == lineID).ToList());
                    for (int j = 0; j < SOTransLineContentDetails.Count; j++)
                    {
                        string SAPResponse;
                        SAPResponse = SOTransLineContentDetails[j].SapResponseMessage;
                        responseList.Add(SAPResponse);
                    }
                    TransLineModel.SAPStatus = string.Join(",", responseList.ToArray());
                    TransLineModel.LineContent = SOTransLineDetails[i].LineContent;
                    TransLineModel.Status = SOTransLineDetails[i].Trn_LineStatus.StatusCode;
                    TransLineModel.Batchid = SOTransLineDetails[i].BatchId;
                    TransLineModel.UniqueId = SOTransLineDetails[i].Id;
                    if (SOTransLineDetails[i].InTestMode) { TransLineModel.Mode = "Verified"; }
                    else { TransLineModel.Mode = "Processed"; }

                    TransLineModel.CustomerPONumber = SOTransLineDetails[i].CustomerPONumber;
                    TransLineModel.CustomerPODate = SOTransLineDetails[i].CustomerPODate;
                    TransLineModel.CustomerNumber = SOTransLineDetails[i].CustomerNumber;
                    TransLineModel.ReqDeliveryDate = SOTransLineDetails[i].ReqDeliveryDate;
                    TransLineModel.MaterialNumber = SOTransLineDetails[i].MaterialNumber;
                    TransLineModel.FromLocation = SOTransLineDetails[i].FromLocation;
                    TransLineModel.SOQuantity = SOTransLineDetails[i].SOQuantity;
                    TransLineModel.Consignee= SOTransLineDetails[i].Consignee;
                    TransLineModel.ETA = SOTransLineDetails[i].ETA;
                    TransLineModel.ETD = SOTransLineDetails[i].ETD;
                    TransLineModel.POL = SOTransLineDetails[i].POL;
                    TransLineModel.POD = SOTransLineDetails[i].POD;

                    ListTransLineModel.Add(TransLineModel);
                }
                return ListTransLineModel;
            }
        }

       

    }       
}
