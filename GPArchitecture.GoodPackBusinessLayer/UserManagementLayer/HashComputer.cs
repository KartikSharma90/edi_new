﻿using System.Security.Cryptography;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public class HashComputer
    {
        /// <summary>
        /// Password Hashing from salt
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string GetPasswordHashAndSalt(string message)
        {
            // Let us use SHA256 algorithm to 
            // generate the hash from this salted password
            SHA256 sha = new SHA256CryptoServiceProvider();
            byte[] dataBytes = PasswordUtility.GetBytes(message);
            byte[] resultBytes = sha.ComputeHash(dataBytes);

            // return the hash string to the caller
            return PasswordUtility.GetString(resultBytes);
        }
    }
}