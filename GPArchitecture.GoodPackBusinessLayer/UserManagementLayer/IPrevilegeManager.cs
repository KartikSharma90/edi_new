﻿using System.Collections.Generic;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;


namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IPrevilegeManager
    {
        IList<PrivilegeModel> GetPrevileges(int roleId);
        IList<PrivilegeModel> MappedPrevilege(int role);
        bool CreatePrivilege(List<PrivilegeModel> privilegeModel, int role);
        bool IsPrevilegeToAccess(string username, EnumAppPrevileges typeOfPrevilege);
        PrivilegeAccessModel LinkAccessPrivileges(string username);
    }
}
