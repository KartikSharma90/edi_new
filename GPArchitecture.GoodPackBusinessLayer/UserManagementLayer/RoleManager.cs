﻿

using GPArchitecture.DataLayer.Models;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public class RoleManager : IRoleManager
    {
        public object GetAllRoles()
        {
            using (var context=new GoodpackEDIEntities())
            {
                IList<DropdownModel> query = (from m in context.Ref_UserRoles
                                              orderby m.UserRole
                                              select new DropdownModel()
                                              {
                                                  Value = m.Id,
                                                  DisplayText = m.UserRole
                                              }).ToList();
                return new { Result = "OK", Options = query };
            }        
        }
    }
}