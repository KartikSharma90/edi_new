﻿using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IRegistrationManager
    {
        ResponseModel UserRegistration(Record registerParams, string username);
    }
}
