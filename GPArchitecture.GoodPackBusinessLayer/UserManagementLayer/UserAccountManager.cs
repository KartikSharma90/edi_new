﻿
using GPArchitecture.DataLayer.Models;
using System.Linq;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public class UserAccountManager:IUserAccountManager
    {
       

        /// <summary>
        /// Checks whether username/email id already exists
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="emailId">Eai</param>
        /// <returns></returns>
        public string CheckUserExists(string userName,string emailId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                var userExists = (
                        from s in context.Gen_User
                        where s.Username == userName                       
                        select s.Username)
                                      .FirstOrDefault();
                return userExists;
            }
        }        
    }
}