﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IPasswordManager
    {
       // ResponseModel ChangePassword(ChangePasswordModel accountParams,string username);
        string GeneratePasswordHash(string plainTextPassword, out string salt);
        bool IsPasswordMatch(string password, string salt, string hash);
    }
}
