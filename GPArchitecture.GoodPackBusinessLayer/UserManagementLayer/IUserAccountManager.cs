﻿

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IUserAccountManager
    {       
        string CheckUserExists(string userName, string emailId);
        
    }
}
