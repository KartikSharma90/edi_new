﻿

using GPArchitecture.Models;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IAuthenticationManager
    {        
        LoginResponseModel AuthenticateUser(UserLoginModel loginModel);
    }
}
