﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;
//using GoodpackEDI.ConstantUtils;
//using log4net;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public class PrevilegeManager : IPrevilegeManager
    {
        //        private static readonly ILog log = LogManager.GetLogger(typeof(PrevilegeManager));



        /// <summary>
        /// Checks whether user have previlege to access the Api
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="typeOfPrevilege">Enum constant of Previlege</param>
        /// <returns></returns>

        public bool IsPrevilegeToAccess(string username, EnumAppPrevileges typeOfPrevilege)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();
                if (user != null)
                {
                    Ref_UserRoles roles = (from s in context.Ref_UserRoles
                                           where s.Id == user.RoleId
                                           select s).FirstOrDefault();
                    if ((roles.UserRole != ConstantUtility.HQUser) && (roles.UserRole != ConstantUtility.SubsiUser))
                    {

                        return true;
                    }
                    else
                    {
                        string privilegeName = Enum.GetName(typeOfPrevilege.GetType(), typeOfPrevilege);
                        IList<int> mapper = (from map in context.Trn_RoleToPrivilegeMapper
                                             where map.RoleId == user.RoleId
                                             select map.PrivilegeId).ToList();

                        int privilegeId = (from s in context.Ref_Privilege
                                           where s.EnumPrivilegeName == privilegeName
                                           select s.Id).FirstOrDefault();
                        if (mapper.Contains(privilegeId))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }

        }






        /// <summary>
        /// Get the previleges of user
        /// </summary>
        /// <param name="roleId">Username</param>
        /// <returns></returns>
        public IList<PrivilegeModel> GetPrevileges(int role)
        {
            IList<DropdownModel> query = new List<DropdownModel>();
            string roleType;
            switch (role)
            {
                case 0: roleType = ConstantUtility.HQUser;
                    break;
                case 1: roleType = ConstantUtility.SubsiUser;
                    break;
                case 2: roleType = ConstantUtility.HQAdminUser;
                    break;
                default: roleType = null;
                    break;
            }
            if (roleType != null)
            {

                // _repository.CityRepository.GetAllCities().Select(c => new { DisplayText = c.CityName, Value = c.CityId });
                using (var context = new GoodpackEDIEntities())
                {
                    Ref_UserRoles roles = (from r in context.Ref_UserRoles
                                           where r.UserRole == roleType
                                           select r).FirstOrDefault();


                    IList<PrivilegeModel> privilege =
                                                    (from item in context.Ref_Privilege
                                                     orderby item.PrivilegeName
                                                     where !(from p in context.Trn_RoleToPrivilegeMapper
                                                             where p.RoleId == roles.Id
                                                             select p.PrivilegeId).Contains(item.Id)
                                                     select new PrivilegeModel
                                                     {
                                                         PrivilegeId = item.Id,
                                                         PrivilegeName = item.PrivilegeName
                                                     }).ToList();

                    //(from s in context.Ref_Privilege
                    //                              join m in context.Trn_RoleToPrivilegeMapper on
                    //                              new { Id = s.Id }
                    //                              equals
                    //                              new {Id=(m.PrivilegeId) }
                    //                              select s).ToList();
                    return privilege;
                    //IList<Trn_RoleToPrivilegeMapper> mapper = (from s in context.Trn_RoleToPrivilegeMapper
                    //                                           where s.RoleId == roleId
                    //                                           select s).ToList();
                }
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Get the previleges of user
        /// </summary>
        /// <param name="roleId">Username</param>
        /// <returns></returns>
        public IList<PrivilegeModel> MappedPrevilege(int role)
        {
            IList<DropdownModel> query = new List<DropdownModel>();
            string roleType;
            switch (role)
            {
                case 0: roleType = ConstantUtility.HQUser;
                    break;
                case 1: roleType = ConstantUtility.SubsiUser;
                    break;
                default: roleType = null;
                    break;
            }
            if (roleType != null)
            {


                using (var context = new GoodpackEDIEntities())
                {
                    Ref_UserRoles roles = (from r in context.Ref_UserRoles
                                           where r.UserRole == roleType
                                           select r).FirstOrDefault();


                    IList<PrivilegeModel> privilege =
                                                  (from s in context.Trn_RoleToPrivilegeMapper
                                                   join p in context.Ref_Privilege on s.PrivilegeId equals p.Id
                                                   where s.RoleId == roles.Id
                                                   orderby p.PrivilegeName
                                                   select new PrivilegeModel
                                                   {
                                                       PrivilegeId = p.Id,
                                                       PrivilegeName = p.PrivilegeName
                                                   }).ToList();


                    //(from s in context.Ref_Privilege
                    //                              join m in context.Trn_RoleToPrivilegeMapper on
                    //                              new { Id = s.Id }
                    //                              equals
                    //                              new {Id=(m.PrivilegeId) }
                    //                              select s).ToList();
                    return privilege;
                    //IList<Trn_RoleToPrivilegeMapper> mapper = (from s in context.Trn_RoleToPrivilegeMapper
                    //                                           where s.RoleId == roleId
                    //                                           select s).ToList();
                }
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Get the previleges of user
        /// </summary>
        /// <param name="roleId">Username</param>
        /// <returns></returns>
        public bool CreatePrivilege(List<PrivilegeModel> privilegeModel, int role)
        {
            IList<DropdownModel> query = new List<DropdownModel>();
            string roleType;
            switch (role)
            {
                case 0: roleType = ConstantUtility.HQUser;
                    break;
                case 1: roleType = ConstantUtility.SubsiUser;
                    break;
                default: roleType = null;
                    break;
            }
            if (roleType != null)
            {
                using (var context = new GoodpackEDIEntities())
                {
                    Ref_UserRoles roles = (from r in context.Ref_UserRoles
                                           where r.UserRole == roleType
                                           select r).FirstOrDefault();

                    IList<Trn_RoleToPrivilegeMapper> privilegeMapper = (from s in context.Trn_RoleToPrivilegeMapper
                                                                        where s.RoleId == roles.Id
                                                                        select s).ToList();


                    foreach (var item in privilegeMapper)
                    {
                        context.Trn_RoleToPrivilegeMapper.Remove(item);
                        context.SaveChanges();
                    }

                    foreach (var items in privilegeModel)
                    {
                        Trn_RoleToPrivilegeMapper privilgeMapperItem = new Trn_RoleToPrivilegeMapper();
                        privilgeMapperItem.PrivilegeId = items.PrivilegeId;
                        privilgeMapperItem.RoleId = roles.Id;
                        context.Trn_RoleToPrivilegeMapper.Add(privilgeMapperItem);
                        context.SaveChanges();
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }


        public PrivilegeAccessModel LinkAccessPrivileges(string username)
        {
            using (var context=new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();
                PrivilegeAccessModel accessmodel = new PrivilegeAccessModel();
                if (user != null)
                {

                    Ref_UserRoles roles = (from s in context.Ref_UserRoles
                                           where s.Id == user.RoleId
                                           select s).FirstOrDefault();
                    if ((roles.UserRole != ConstantUtility.HQUser) && (roles.UserRole != ConstantUtility.SubsiUser))
                    {

                        accessmodel.IsAdminLink = true;
                        accessmodel.IsDataLookUpLink = true;
                        accessmodel.IsMappingConfigLink = true;
                        accessmodel.IsSCLink = true;
                        accessmodel.IsSOLink = true;
                        accessmodel.IsSubsiLink = true;
                        accessmodel.IsSSCLink = true;
                        accessmodel.IsEditLink = true;
                        accessmodel.IsCancelSOLink = true;
                        accessmodel.IsASNLink = true;
                        accessmodel.IsErrorMailDetailsLink = true;
                    }
                    else
                    {
                        IList<Trn_RoleToPrivilegeMapper> mapper = (from s in context.Trn_RoleToPrivilegeMapper
                                                                   where s.RoleId == user.RoleId
                                                                   select s).ToList();

                        foreach (var item in mapper)
                        {
                            string privilege = (from s in context.Ref_Privilege
                                                where s.Id == item.PrivilegeId
                                                select s.EnumPrivilegeName).FirstOrDefault();
                            EnumAppPrevileges previlegeName =
                              (EnumAppPrevileges)Enum.Parse(typeof(EnumAppPrevileges), privilege);
                            switch (previlegeName)
                            {
                                case EnumAppPrevileges.Administrator:
                                    accessmodel.IsAdminLink = true;
                                    break;
                                case EnumAppPrevileges.DataLookUp:
                                    accessmodel.IsDataLookUpLink = true;
                                    break;
                                case EnumAppPrevileges.MappingConfiguration:
                                    accessmodel.IsMappingConfigLink = true;
                                    break;
                                case EnumAppPrevileges.SalesOrder:
                                    accessmodel.IsSOLink = true;
                                    break;
                                case EnumAppPrevileges.ShipmentConfirmation:
                                    accessmodel.IsSCLink = true;
                                    break;
                                case EnumAppPrevileges.SubsiMapping:
                                    accessmodel.IsSubsiLink = true;
                                    break;
                                case EnumAppPrevileges.ScannedShipmentConfirmation:
                                    accessmodel.IsSSCLink = true;
                                    break;
                                case EnumAppPrevileges.SalesOrderMassEdit:
                                    accessmodel.IsEditLink = true;
                                    break;
                                case EnumAppPrevileges.AdvancedShipmentNotification:
                                    accessmodel.IsASNLink = true;
                                    break;
                                case EnumAppPrevileges.SalesOrderCancel:
                                    accessmodel.IsCancelSOLink= true;
                                    break;
                                case EnumAppPrevileges.ErrorEmailDetails:
                                    accessmodel.IsErrorMailDetailsLink = true;
                                    break;

                            }
                        }
                    }
                }
                return accessmodel;
                
            }
        
        }

    }
}


