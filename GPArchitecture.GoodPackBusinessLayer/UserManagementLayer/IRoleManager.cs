﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.GoodPackBusinessLayer.UserManagementLayer
{
    public interface IRoleManager
    {
        object GetAllRoles();
        
    }
}
