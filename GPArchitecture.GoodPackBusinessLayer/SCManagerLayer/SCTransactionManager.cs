using GPArchitecture.DataLayer.Models;
 using GPArchitecture.EnumsAndConstants;
 using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.SCManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer;
 using GPArchitecture.Models;
 using GPArchitecture.Models.ViewModels;
 using GPArchitecture.Parsers;
 using GPArchitecture.Utilities;
 using log4net;
 using System;
 using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Net.Mail;
 using System.Text;
 using System.Text.RegularExpressions;
 
 namespace GPArchitecture.GoodPackBusinessLayer.SCManagerLayer
 {
     public class SCTransactionManager
     {
         private string scPath;
         private SCTransactionModel objGPCustomerMessage;
         private StringBuilder errorDescription = new StringBuilder();
         private Dictionary<int, Dictionary<string, string>> sapSubmitTransData;
         private int trLineID = 0;
         private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
         private static readonly ILog log = LogManager.GetLogger(typeof(SCTransactionManager));
 
         public IList<GenericItemModel> SCFileData(string mapperName, int subsiId, int fileType, string path)
         {
             try
             {
                 scPath = path;
                 bool isValidFile = false;
                 FileInfo fileInfo = null;
                 string message = "";
                 objGPCustomerMessage = new SCTransactionModel();
                 if (fileType == 1)
                 {
                     fileInfo = new FileInfo(scPath);
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                     objGPCustomerMessage.BatchFileName = fileInfo.Name;
                 }
                 else if (fileType == 2)
                 {
                     scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                     fileInfo = new FileInfo(scPath);
                     // objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                 }
                 else if (fileType == 3)
                 {
                     //scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                     fileInfo = new FileInfo(path);
                     objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                     objGPCustomerMessage.BatchFileName = Path.GetFileName(scPath);
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                 }
                 IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
                 GenericItemModel itemModel;
                 isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
                 if (isValidFile)
                 {
                     bool isFirstVal = true;
                     int i = 0;
                     int transactionId;
                     string[] data;
                     if (mapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation"))
                     {
                         data = SCSumiData(fileInfo);
                     }
                     else if (GPArchitecture.Cache.Config.getConfigList("SynthosisScannedShipmentConfirmation").Contains(mapperName))
                     {
                         data = SCDataSynthosis(fileInfo, out message);
                     }
                     else
                     {
                         data = SCData(fileInfo, out message);
                     }
                     IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                          join p in context.Ref_TransactionTypes
                                                                                                                          on s.TransactionCodeId equals p.Id
                                                                                                                          where s.Filename == mapperName
                                                                                                                          select p.TransactionCode).FirstOrDefault());
                         transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                          where s.Filename == mapperName
                                          select s.TransactionCodeId).FirstOrDefault();
                     }
                     data = data.ToList().Where(s => !string.IsNullOrEmpty(s)).ToArray();
                     //For restresting file line count to 50 -included by Anoop S
 
                     //if (objGPCustomerMessage.TransactionType.ToString()=="SC" && data.Length > 51)
                     //{
                     //    itemModel = new GenericItemModel();
                     //    itemModel.Message = "Maximum line's allowed in a file is 50";
                     //    itemModel.Status = "false";
                     //    scDataModel.Add(itemModel);
                     //    return scDataModel;
                     //}
                     log.Debug("SCFileData data read end");
                     IList<Ref_LookupNames> lookUpList = GetLookupList();
                     IList<Trn_LookupValues> lookUpValueList = GetLookupValues(mapperName);
                     ValidationDataModel validationData = GetValidationDataForField(mapperName, transactionId);
                     log.Debug("SCFileData data config read start");
                     foreach (var item in data)
                     {
                         if (item.Length != 0)
                         {
                             i++;
                             string val = item.Replace("\"", "").Replace("\"", "");
                             string[] stringData = val.ToUpper().Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                             if (isFirstVal)
                             {
                                 isFirstVal = false;
 
                                 if (stringData.Length == mappedData.Count)
                                 {
                                     bool isMappedData = false;
                                     for (int j = 0; j < mappedData.Count; j++)
                                     {
                                         if (stringData[j].ToUpper() == mappedData[j].FieldName.ToUpper())
                                         {
                                             isMappedData = true;
                                             break;
                                         }
                                     }
                                     //var dataHeader = new string[] { };
                                     //dataHeader = mappedData.Select(x => x.FieldName.ToUpper()).ToArray().Select(x => x.Replace(Environment.NewLine, " ").Replace("\n", " ")).ToArray();
                                     //if (dataHeader.SequenceEqual(stringData))
                                     //    isMappedData = true;
                                     if (isMappedData)
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Id = i;
                                         itemModel.Name = val;
                                         //itemModel.DataValues = message;
                                         if (i == 1)
                                         {
                                             itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                             itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                             itemModel.Message = objGPCustomerMessage.Message;
                                         }
                                         scDataModel.Add(itemModel);
                                         itemModel = new GenericItemModel();
                                         i++;
                                         itemModel.Id = i;
                                         //if(objGPCustomerMessage.TransactionType==TRANSACTION_TYPES.ASN && )
                                         //{
 
                                         //}
                                         itemModel.Name = SAPHeaders(mapperName).Replace("\"", "").Replace("\r", "");
 
                                         scDataModel.Add(itemModel);
                                         ///////////////////////////
                                     }
                                     else
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Status = "false";
                                         //itemModel.DataValues = message;
                                         itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                         itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                         itemModel.Message = objGPCustomerMessage.Message;
                                         itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                         itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file " + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                         scDataModel.Add(itemModel);
                                         log.Debug("SCFileData End");
                                         return scDataModel;
                                     }
                                 }
                                 else
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Status = "false";
                                     // itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                     itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file" + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                     scDataModel.Add(itemModel);
                                     log.Debug("SCFileData End");
                                     return scDataModel;
                                 }
                             }
                             else
                             {
                                 if (stringData.Length == mappedData.Count)
                                 {
                                     int counter = 0;
                                     for (int j = 0; j < stringData.Length; j++)
                                     {
                                         if (stringData[j] == "")
                                         {
                                             counter++;
                                         }
                                     }
                                     if (counter != stringData.Length)
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Id = i;
                                         //itemModel.DataValues = message;
                                         if (i == 1)
                                         {
                                             itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                             itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                             itemModel.Message = objGPCustomerMessage.Message;
                                         }
                                         itemModel.Name = item.Replace("\"", "");
                                         //SAP headers and items in the file
                                         //string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);
                                         //string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId,transactionId, lookUpList, lookUpValueList);
                                         string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId, transactionId, lookUpList, lookUpValueList, validationData);
                                         // itemModel.CustomerPackerRelation = CheckCustomerPackerRelation(item.Replace("\"", ""), scDataModel[1].Name, dataWithDecodeValue);
                                         // if (dataWithDecodeValue != null)
                                         // {
                                         itemModel.DecodeValue = dataWithDecodeValue;
 
 
                                         scDataModel.Add(itemModel);
                                         //  }
                                         //else
                                         //{
                                         //    scDataModel = new List<GenericItemModel>(); ;
                                         //    itemModel = new GenericItemModel();
                                         //    itemModel.Status = "false";
                                         //    itemModel.Message = "Decode value is missing.Please add decode values";
                                         //    scDataModel.Add(itemModel);
                                         //    return scDataModel;
 
                                         //}
                                     }
 
                                 }
                                 else
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Status = "false";
                                     //itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file and Mapper";
                                     itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                     scDataModel.Add(itemModel);
                                     log.Debug("SCFileData End");
                                     return scDataModel;
                                 }
 
                             }
 
                         }
 
                     }
                     log.Debug("SCFileData data config read end");
                     scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                     // HttpRuntime.Cache.Insert("mothy_SC", message);
                     log.Debug("SCFileData End");
                     return scDataModel;
                 }
                 else
                 {
                     itemModel = new GenericItemModel();
                     itemModel.Status = "false";
                     //   itemModel.DataValues = message;
                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                     itemModel.Message = objGPCustomerMessage.Message;
                     itemModel.ErrorMessage = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                     itemModel.Message = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                     scDataModel.Add(itemModel);
                     log.Debug("SCFileData End");
                     return scDataModel;
                 }
                 //}
                 //else
                 //{
                 //    IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
                 //    GenericItemModel itemModel;
                 //    itemModel = new GenericItemModel();
                 //    itemModel.Status = "false";
                 //    itemModel.Message = "Please check the uploaded file...should contain less than 50 lines";
                 //    scDataModel.Add(itemModel);
                 //    return scDataModel;
 
                 //}
 
                 log.Debug("SCFileData End");
             }
             catch (Exception e)
             {
                 log.Debug("SCFileData End");
                 //  log.Error("SCController-FileData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new List<GenericItemModel>();
             }
         }
 
         public IList<Ref_LookupNames> GetLookupList()
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_LookupNames
                         select s).ToList();
             }
         }
         private ValidationDataModel GetValidationDataForField(string mapperName, int transactionType)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 ValidationDataModel dataModel = new ValidationDataModel();
                 var dateField = (from s in context.Trn_SubsiToTransactionFileMapper
                                  join p in context.Trn_MappingSubsi on s.Id equals p.SubsiToTransactionFileMapperId
                                  join m in context.Trn_MappingConfiguration on p.Id equals m.MappingSubsiId
                                  where (s.Filename == mapperName) && (m.InputDateFormat != null)
                                  select m.InputDateFormat).FirstOrDefault();
 
                 var characterList = (from s in context.Ref_TransactionTypes
                                      join p in context.Ref_SAPMessageFields
                                          on s.Id equals p.TransactionId
                                      where (s.Id == transactionType)
                                      select new CharacterList { MaxLength = p.MaxLength, SAPName = p.SAPFieldName }).ToList();
 
                 var boolList = (from s in context.Ref_SAPMessageFields
                                 join p in context.Ref_TransactionTypes
                                     on s.TransactionId equals p.Id
                                 where ((p.Id == transactionType))
                                 select new BoolList { IsNullAllowed = s.IsAllowNullValue, SAPName = s.SAPFieldName }).ToList();
                 dataModel.DateString = dateField;
                 dataModel.CharList = characterList;
                 dataModel.BoolList = boolList;
                 return dataModel;
             }
         }
         private IList<Trn_LookupValues> GetLookupValues(string mapperName)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Trn_LookupValues
                         where s.MapperName == mapperName
                         select s).ToList();
             }
         }
 
         private string CheckForDecodeValueValidationNew(string fileData, string headers, string mapperName, int subsiId, int transactionId, IList<Ref_LookupNames> lookUpList, IList<Trn_LookupValues> lookUpValueList, ValidationDataModel validationData)
         {
             StringBuilder fileDecodedData = new StringBuilder();
 
             string[] fileValues = fileData.Split(ConstantUtilities.delimitersNew, StringSplitOptions.None);
             string[] headerValues = headers.Split(ConstantUtilities.delimitersNew, StringSplitOptions.None);
             IDecodeValueManager decodeValueManager = new DecodeValueManager();
             for (int i = 0; i < headerValues.Length; i++)
             {
                 string sapFieldName = headerValues[i];
                 string inputType = String.Empty;
                 int lookupNameId = 0;
                 string barcodeHeader = "";
                 if (sapFieldName != "")
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
 
                         inputType = (from s in context.Ref_SAPMessageFields
                                      where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                      select s.InputType).FirstOrDefault();
                         //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                         barcodeHeader = (from s in context.Ref_SAPMessageFields
                                          where s.Ref_TransactionTypes.TransactionCode == "SSC" && s.FieldSequence == 37
                                          select s.SAPFieldName).First();
                     }
                     switch (sapFieldName.ToUpper())
                     {
                         case "CUSTOMER CODE":
                         case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                             break;
                         case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                             break;
                         case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                             break;
                         case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
                             break;
                         case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port, lookUpList);
                             break;
                         case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL, lookUpList);
                             break;
                         case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD, lookUpList);
                             break;
                         case "FRM LOCATION":
                         case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                             }
                             else
                             {
                                 if (transactionId == 12)
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                                 }
                                 else
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
                                 }
                             }
                             break;
                         case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                             }
                             else
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                             }
                             break;
                         case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine, lookUpList);
                             break;
                     }
                     TargetMessageInputTypes inputSAPType =
                             (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);
 
 
                     if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                     {
                         string decodeValue = FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId, lookUpValueList);
                         if (decodeValue != null)
                         {
                             if (decodeValue.Length != 0)
                                 errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName, validationData));
                             else
                                 errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData));
                             fileDecodedData.Append(decodeValue);
 
                         }
                         else
                         {
                             bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                             if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
                             {
                                 string message = CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData);
                                 errorDescription.Append(message);
                                 //if (!string.IsNullOrEmpty(errorDescription.ToString()))  
                                 if (string.IsNullOrEmpty(message))
                                 {
                                     errorDescription.Append(GetErrorMessage(lookupNameId, fileValues[i]) + ConstantUtilities.delimitersNew[2]);
                                 }
                                 fileDecodedData.Append("false");
                             }
                         }
                     }
                     else
                     {
                         if (sapFieldName == barcodeHeader)
                             errorDescription.Append(CheckBarcodeValidation(mapperName, fileValues[i]));
                         else
                             errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData));
                     }
 
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
                 else
                 {
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
             }
             fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
             return fileDecodedData.ToString();
         }
 
 
         private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
         {
             IValidationTypeManger validator = new ValidationTypeManager();
             Dictionary<string, string> validatorParams = new Dictionary<string, string>();
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
             switch (inputType)
             {
                 case GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator:
                     return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);
 
                 default:
                     return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
             }
 
 
         }
         //New function for improving performance -Anoop S
         private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField, ValidationDataModel validationData)
         {
             IValidationTypeManger validator = new ValidationTypeManager();
             Dictionary<string, string> validatorParams = new Dictionary<string, string>();
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
             switch (inputType)
             {
                 case ConstantUtilities.DateValidator:
                     return validator.ValidationType(ConstantUtilities.DateValidator).DataValidator(validatorParams, validationData);
 
                 default:
                     return validator.ValidationType(ConstantUtilities.CommonValidator).DataValidator(validatorParams, validationData);
             }
 
 
         }
         public int GetLookupId(string LookupName, IList<Ref_LookupNames> lookUpList)
         {
             return (lookUpList.Where(i => i.LookupName == LookupName).Select(i => i.Id).FirstOrDefault());
         }
 
         private string GetErrorMessage(int lookupNameId, string lookUpCode)
         {           
             string returnMessage = "";
             try
             {
                 string lookupName = GetLookupName(lookupNameId);
                 int codeLeng = lookUpCode.Length;
                 switch (lookupName.ToUpper())
                 {
                     case "CONSIGNEE": if (!lookUpCode.StartsWith("6") || codeLeng != 6)
                         {
                             returnMessage = "Consignee decode value does not exist ";
                         }
                         break;
                     case "CUSTOMER CODE": if (!lookUpCode.StartsWith("3") && codeLeng != 6)
                         {
                             returnMessage = "Customer decode value does not exist ";
                         }
                         break;
                     case "PACKER": if (!lookUpCode.StartsWith("5") && codeLeng != 6)
                         {
                             returnMessage = "Packer decode value does not exist ";
                         }
                         break;
                 }
             }
             catch(Exception e)
             {
                 log.Error("GetErrorMessage function error- " + e);
             }
             return returnMessage;
         }
         private string GetLookupName(int lookupNameId)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_LookupNames
                         where s.Id == lookupNameId
                         select s.LookupName).FirstOrDefault();
             }
         }
 
         private string FindDecodeValue(string lookUpCode, string mapperFile, int subsiId, int lookupNameId, IList<Trn_LookupValues> lookUpValueList)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 int codeLeng;
                 bool isConsignee = false;
                 bool isPacker = false;
                 bool isCustomercode = false;
                 codeLeng = lookUpCode.Length;
                 string lookupName = GetLookupName(lookupNameId);
                 switch (lookupName.ToUpper())
                 {
                     case "CONSIGNEE": if (lookUpCode.StartsWith("6") && codeLeng == 6)
                         {
                             isConsignee = true;
                         }
                         break;
                     case "CUSTOMER CODE": if (lookUpCode.StartsWith("3") && codeLeng == 6)
                         {
                             isCustomercode = true;
                         }
                         break;
                     case "PACKER": if (lookUpCode.StartsWith("5") && codeLeng == 6)
                         {
                             isPacker = true;
                         }
                         break;
                 }
                 if (isConsignee || isPacker || isCustomercode)
                 {
                     return String.Empty;
                 }
                 else
                 {
                     return lookUpValueList.Where(i => i.LookupCode.ToUpper().Trim() == lookUpCode.ToUpper().Trim() && i.SubsiId == subsiId && i.MapperName == mapperFile && i.LookupNameId == lookupNameId).Select(i => i.LookupDecodeValue).FirstOrDefault();
                     //return (from s in context.Trn_LookupValues
                     //        where ((s.LookupCode == lookUpCode) && (s.SubsiId == subsiId) && (s.MapperName == mapperFile) && (s.LookupNameId == lookupNameId))
                     //        select s.LookupDecodeValue).FirstOrDefault();
                 }
             }
         }
 
         private string CheckBarcodeValidation(string mapperName, string fileValue)
         {
             Regex r = new Regex("^[a-zA-Z0-9]*$");
             string s = "";
             if (!r.IsMatch(fileValue))
                 s = "Pin Number ( " + fileValue + " ) should not contain special character(s), ";
             return s;
 
         }
 
         public IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, int fileType)
         {
             try
             {
                 log.Info("VerifyData function start");
                 GenericItemModel itemModelVal = itemModel[1];
                 objGPCustomerMessage = new SCTransactionModel();
                 List<string> dataValue;
                 List<string> fileName;
                 List<string> filePath;
                 List<string> message;
                 List<string> emailId;
                 bool isFileData = false;
                 dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                 fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                 filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                 message = itemModel.Select(x => x.Message).Distinct().ToList();
                 emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                 objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                 objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                 objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                 objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                 if (fileType == 1)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     isFileData = true;
                     itemModel.RemoveAt(1);
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                         where s.Username == username
                                                         select s.EmailId).FirstOrDefault();
                     }
                 }
                 else if (fileType == 2)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                 }
                 else if (fileType == 3)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                 }
                 using (var context = new GoodpackEDIEntities())
                 {
                     int userId = (from s in context.Gen_User
                                   where s.Username == username
                                   select s.Id).FirstOrDefault();
                     objGPCustomerMessage.UserId = userId;
 
                     objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                      join e in context.Ref_TransactionTypes
                                                                                                                      on s.TransactionCodeId equals e.Id
                                                                                                                      where s.Filename == mapperFile
                                                                                                                      select e.TransactionCode).FirstOrDefault());
                 }
 
                 objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                 objGPCustomerMessage.DateCreated = DateTime.Now;
                 objGPCustomerMessage.Service = "GPMapper";
                 objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
 
                 // bool processOneLineOnly = false;
                 List<int> trnsLineItems = new List<int>();
                 // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                 GPMapper mapper = new GPMapper();
                 MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                 List<string> ediNumber = new List<string>();
 
                 if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                        && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 {
                     //Insert lines in Trn_TransBatch table                    
                     objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                     objGPCustomerMessage.BatchID = AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                     itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                     int i = 0;
                     // Update/Insert lines in TRANS_LINE table
                     foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                     {
                         objLineReferences.StatusCode = LineStatus.NEW;
                         string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                         string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                         ediNumber.Add(objLineReferences.EdiNumber);
                         int transID = 0;
                         //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
                         switch (objGPCustomerMessage.TransactionType)
                         {
                             case TRANSACTION_TYPES.SC: transID = InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                 break;
                             //case TRANSACTION_TYPES.SSC: transID = InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
                             //    break;
                         }
                         trnsLineItems.Add(transID);
                         i++;
                     }
                 }
 
                 // Update status of transaction to processed.
                 objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                 objGPCustomerMessage.ErrorDesc = string.Empty;
                 log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                 UpdateSCBatchFileData(objGPCustomerMessage);
                 UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                   0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
 
 
                 // Archive message
                 //no need to archive for bin pairing transaction
                 //if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 //{
                 //    try
                 //    {
                 //        string archiveFilename = (Guid.NewGuid().ToString()) + ".txt";
                 //        string archivePath = String.Format("{0}\\{1}", Config.getConfig("ArchivePath"), ArchiveSources.REQ_OUT.ToString());
                 //        GPTools.Archive(mappingOutput.OutputMessage.ToString(), ArchiveSources.REQ_OUT, objGPCustomerMessage.BatchID.ToString(), archivePath);
 
                 //        //To copy the FC translated file to some FC archive folder
                 //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
                 //        {
                 //            string fcArchiveFilename = System.IO.Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName);
                 //            GPTools.ArchiveFC(mappingOutput.OutputMessage.ToString(), Config.getConfig("FCArchivePath"), fcArchiveFilename);
                 //        }
 
                 //    }
                 //    catch (System.Exception ex)
                 //    {
                 //        objILog.Error("Error while archiving received file.", ex);
                 //        GPTools.ProcessErrorAction(SAPClientService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_ARCHIVE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), ex.Message);
                 //    }
                 //}
 
                 // Send to SAP
                 //No need to send to SAP for Bin Pairing transaction
                 //if (SendToSAP(objGPCustomerMessage, mappingOutput.OutputMessage))
                 //{
                 //objDataAccess = new BatchProcessorDAFactory(DBConnection.SqlConnString);
                 if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                     || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                 {
                     objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                 }
                 else
                 {
                     objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                     // Update TRANS_LINE status
                     log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                     switch (objGPCustomerMessage.TransactionType)
                     {
                         case TRANSACTION_TYPES.SC: UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                             break;
                         //case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                         //    break;
                     }
 
                     Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                     sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                     int successCount = 0;
                     int errorCount = 0;
                     for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                     {
                         trLineID = trnsLineItems[i];
                         log.Info("Updating TransLine table with excel data");
                         switch (objGPCustomerMessage.TransactionType)
                         {
                             case TRANSACTION_TYPES.SC: UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                 break;
                             //case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                             //    break;
                         }
 
                         try
                         {
                             List<ResponseModel> responseModel = null;
                             SCServiceManager serviceCaller = new SCServiceManager();
                             // SSCServiceManager sscCaller = new SSCServiceManager();
                             log.Info("Calling SC webservice");
                             if (isFileData)
                             {
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                         break;
                                     //case TRANSACTION_TYPES.SSC:
                                     //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
                                     //    break;
                                 }
                             }
                             else
                             {
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                         break;
                                     //    case TRANSACTION_TYPES.SSC:
                                     //responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId, ediNumber[i]);
                                     //break;
                                 }
                             }
 
 
                             serviceResponse.Add(trLineID, responseModel);
 
                             log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                             bool isSuccess = false;
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC:
                                     isSuccess = InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                     break;
 
                                 //    case TRANSACTION_TYPES.SSC:
                                 //        isSuccess = InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                 //        break;
                             }
                             log.Info("Insertion of trans line content success status:" + isSuccess);
                             if (isSuccess)
                             {
                                 log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                         break;
                                 }
                             }
                             else
                             {
                                 log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
 
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                         break;
 
                                 }
                             }
                             sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
 
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC: UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                     break;
                             }
                         }
                         catch (Exception e)
                         {
                             log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                             // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                             List<ResponseModel> responseModel = new List<ResponseModel>();
                             ResponseModel model = new ResponseModel();
                             model.Success = false;
                             // model.Message = "Exception Message :" + e.Message;    
                             model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                             responseModel.Add(model);
                             errorCount++;
                             // GenericItemModel itemsModel = new GenericItemModel();
                             objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                             objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                             objGPCustomerMessage.ErrorDesc = e.Message;
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC:
                                     SCTransactionData(responseModel, isFileData);
                                     UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                     break;
                             }
 
                             serviceResponse.Add(trLineID, responseModel);
                         }
                     }
                 }
 
                 //  int iCountSentToSAP = 0;
                 //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                 //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                 //below code commented by abina
                 //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                 //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                 IList<GenericItemModel> genericModel = null;
                 if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     genericModel = ScTestResultGoodyear(itemModel, trnsLineItems);
                 }
                 else
                 {
                     switch (objGPCustomerMessage.TransactionType)
                     {
                         case TRANSACTION_TYPES.SC:
                             genericModel = ScTestResult(itemModel, trnsLineItems);
                             break;
                         // case TRANSACTION_TYPES.SSC:
                         //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
                         //break;
                     }
                 }
                 if (genericModel.Count > 0)
                 {
                     genericModel[0].sapResponseData = sapSubmitTransData;
                 }
                 Gen_User userDetails;
                 using (var dataContext = new GoodpackEDIEntities())
                 {
                     userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                 }
 
                 Dictionary<string, string> placeholders = new Dictionary<string, string>();
                 placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                 placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                 placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                 placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                 placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                 placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                 if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                 {
                     //send email to customer and internal users of goodpack
                     //emailid of customer get from batchfilesource address
                     string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                     List<Attachment> attachments1 = new List<Attachment>();
                     string strFileSC;
                     strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                     strFileSC += "\\" + objGPCustomerMessage.BatchFileName;
 
                     Attachment attachment1 = new Attachment(strFileSC);
                     attachment1.Name = objGPCustomerMessage.BatchFileName;
                     attachments1.Add(attachment1);
                     log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
 
 
                     GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                     List<Attachment> attachments2 = new List<Attachment>();
                     Attachment attachment2 = new Attachment(strFileSC);
                     attachment2.Name = objGPCustomerMessage.BatchFileName;
                     attachments2.Add(attachment2);
                     log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                     GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                 }
                 else
                 {
                     if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                     {
                         //goodyear notification
                         List<Attachment> attachments = new List<Attachment>();
                         string strGoodyearSC;
                         strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                         //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                         Attachment attachment = new Attachment(strGoodyearSC);
                         attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                         attachments.Add(attachment);
                         log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                         GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                     }
                     else
                     {
                         //fileupload SO
                         //send email to users
                         GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                     }
                 }
                 log.Info("Email send to customer" + userDetails.FullName.ToString());
                 for (int i = 0; i < genericModel.Count; i++)
                 {
                     itemModelData.Add(genericModel[i]);
                     if (i == 0)
                     {
                         itemModelData.Add(itemModelVal);
                     }
                 }
                 log.Info("Leaving from verifyData in SCManager");
                 return itemModelData;
             }
             catch (Exception exp)
             {
                 log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                 IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                 GenericItemModel itemsModel = new GenericItemModel();
                 objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                 objGPCustomerMessage.ErrorDesc = exp.Message;
                 itemsModel.Status = "false";
                 itemsModel.Message = "Unable to process your request.";
                 // Update TRANS_BATCH
                 genericModel.Add(itemsModel);
                 UpdateSCBatchFileData(objGPCustomerMessage);
                 Dictionary<string, string> placeholder = new Dictionary<string, string>();
                 // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                 placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                 return genericModel;
 
             }
             //log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
             //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
             //log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
             //objILog.Error("Error while processing in ProcessBatch", exp);
 
         }
         public IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, int fileType, bool isWeb)
         {
             try
             {
                 log.Info("VerifyData function start");
                 GenericItemModel itemModelVal = itemModel[1];
                 objGPCustomerMessage = new SCTransactionModel();
                 List<string> dataValue;
                 List<string> fileName;
                 List<string> filePath;
                 List<string> message;
                 List<string> emailId;
                 bool isFileData = false;
                 dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                 fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                 filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                 message = itemModel.Select(x => x.Message).Distinct().ToList();
                 emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                 objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                 objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                 objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                 objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                 if (fileType == 1)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     isFileData = true;
                     itemModel.RemoveAt(1);
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                         where s.Username == username
                                                         select s.EmailId).FirstOrDefault();
                     }
                 }
                 else if (fileType == 2)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                 }
                 else if (fileType == 3)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                 }
                 using (var context = new GoodpackEDIEntities())
                 {
                     int userId = (from s in context.Gen_User
                                   where s.Username == username
                                   select s.Id).FirstOrDefault();
                     objGPCustomerMessage.UserId = userId;
 
                     objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                      join e in context.Ref_TransactionTypes
                                                                                                                      on s.TransactionCodeId equals e.Id
                                                                                                                      where s.Filename == mapperFile
                                                                                                                      select e.TransactionCode).FirstOrDefault());
                 }
 
                 objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                 objGPCustomerMessage.DateCreated = DateTime.Now;
                 objGPCustomerMessage.Service = "GPMapper";
                 objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
 
                 // bool processOneLineOnly = false;
                 List<int> trnsLineItems = new List<int>();
                 // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                 GPMapper mapper = new GPMapper();
                 MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                 List<string> ediNumber = new List<string>();
 
                 if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                        && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 {
                     //Insert lines in Trn_TransBatch table                    
                     objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                     objGPCustomerMessage.BatchID = AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                     itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                     int i = 0;
                     // Update/Insert lines in TRANS_LINE table
                     foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                     {
                         objLineReferences.StatusCode = LineStatus.NEW;
                         string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                         string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                         ediNumber.Add(objLineReferences.EdiNumber);
                         int transID = 0;
                         //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
                         switch (objGPCustomerMessage.TransactionType)
                         {
                             case TRANSACTION_TYPES.SC: transID = InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                 break;
                             //case TRANSACTION_TYPES.SSC: transID = InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
                             //    break;
                         }
                         trnsLineItems.Add(transID);
                         i++;
                     }
                 }
 
                 // Update status of transaction to processed.
                 objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                 objGPCustomerMessage.ErrorDesc = string.Empty;
                 log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                 UpdateSCBatchFileData(objGPCustomerMessage);
                 UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                   0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
 
 
                 // Archive message
                 //no need to archive for bin pairing transaction
                 //if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 //{
                 //    try
                 //    {
                 //        string archiveFilename = (Guid.NewGuid().ToString()) + ".txt";
                 //        string archivePath = String.Format("{0}\\{1}", Config.getConfig("ArchivePath"), ArchiveSources.REQ_OUT.ToString());
                 //        GPTools.Archive(mappingOutput.OutputMessage.ToString(), ArchiveSources.REQ_OUT, objGPCustomerMessage.BatchID.ToString(), archivePath);
 
                 //        //To copy the FC translated file to some FC archive folder
                 //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
                 //        {
                 //            string fcArchiveFilename = System.IO.Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName);
                 //            GPTools.ArchiveFC(mappingOutput.OutputMessage.ToString(), Config.getConfig("FCArchivePath"), fcArchiveFilename);
                 //        }
 
                 //    }
                 //    catch (System.Exception ex)
                 //    {
                 //        objILog.Error("Error while archiving received file.", ex);
                 //        GPTools.ProcessErrorAction(SAPClientService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_ARCHIVE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), ex.Message);
                 //    }
                 //}
 
                 // Send to SAP
                 //No need to send to SAP for Bin Pairing transaction
                 //if (SendToSAP(objGPCustomerMessage, mappingOutput.OutputMessage))
                 //{
                 //objDataAccess = new BatchProcessorDAFactory(DBConnection.SqlConnString);
                 if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                     || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                 {
                     objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                 }
                 else
                 {
                     objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                     // Update TRANS_LINE status
                     log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                     switch (objGPCustomerMessage.TransactionType)
                     {
                         case TRANSACTION_TYPES.SC: UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                             break;
                         //case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                         //    break;
                     }
 
                     Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                     sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                     int successCount = 0;
                     int errorCount = 0;
                     for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                     {
                         trLineID = trnsLineItems[i];
                         log.Info("Updating TransLine table with excel data");
                         switch (objGPCustomerMessage.TransactionType)
                         {
                             case TRANSACTION_TYPES.SC: UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                 break;
                             //case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                             //    break;
                         }
 
                         try
                         {
                             List<ResponseModel> responseModel = null;
                             SCServiceManager serviceCaller = new SCServiceManager();
                             // SSCServiceManager sscCaller = new SSCServiceManager();
                             log.Info("Calling SC webservice");
                             if (isFileData)
                             {
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         responseModel = new List<ResponseModel>();
                                         //responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                         ResponseModel response = new ResponseModel();
                                         try
                                         {
                                             var returnMessage = CheckValidDecode(objGPCustomerMessage.mappingServiceValues[i]);                                            
                                             if (string.IsNullOrEmpty(returnMessage))
                                             {
                                                 response.Success = true;
                                                 successCount++;
                                                 responseModel.Add(response);
                                             }
                                             else
                                             {
                                                 response.Success = false;
                                                 response.Message = returnMessage;
                                                 errorCount++;
                                                 responseModel.Add(response);
                                             }
                                         }catch(Exception e)
                                         {
                                             response.Success = false;
                                             response.Message = "Customer/Packer/Consignee does not have Decode value  ";
                                             errorCount++;
                                             responseModel.Add(response);
                                         }
                                         break;
                                     //case TRANSACTION_TYPES.SSC:
                                     //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
                                     //    break;
                                 }
                             }
                             else
                             {
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         try
                                         {
                                             responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                         }
                                         catch (Exception e)
                                         {
                                             log.Error("SC serviceCaller error-" + e);
                                             errorCount++;
                                             List<ResponseModel> newResponse = new List<ResponseModel>();
                                             ResponseModel response = new ResponseModel();
                                             response.Message = "Error contacting SAP";
                                             response.Success = false;
                                             response.ResponseItems = "10";
                                             newResponse.Add(response);
                                             responseModel = newResponse;
                                         }
                                         break;
                                 }
                             }
 
 
                             serviceResponse.Add(trLineID, responseModel);
                             log.Info("Response Model :-" + responseModel);
                             log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                             bool isSuccess = false;
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC:
                                     isSuccess = InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                     break;
 
                                 //    case TRANSACTION_TYPES.SSC:
                                 //        isSuccess = InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                 //        break;
                             }
                             log.Info("Insertion of trans line content success status:" + isSuccess);
                             if (isSuccess)
                             {
                                 log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                         break;
                                 }
                             }
                             else
                             {
                                 log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
 
                                 switch (objGPCustomerMessage.TransactionType)
                                 {
                                     case TRANSACTION_TYPES.SC:
                                         UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                         break;
 
                                 }
                             }
                             sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
 
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC: UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                     break;
                             }
                         }
                         catch (Exception e)
                         {
                             log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                             // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                             List<ResponseModel> responseModel = new List<ResponseModel>();
                             ResponseModel model = new ResponseModel();
                             model.Success = false;
                             // model.Message = "Exception Message :" + e.Message;    
                             model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                             responseModel.Add(model);
                             errorCount++;
                             // GenericItemModel itemsModel = new GenericItemModel();
                             objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                             objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                             objGPCustomerMessage.ErrorDesc = e.Message;
                             switch (objGPCustomerMessage.TransactionType)
                             {
                                 case TRANSACTION_TYPES.SC:
                                     SCTransactionData(responseModel, isFileData);
                                     UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                     break;
                             }
 
                             serviceResponse.Add(trLineID, responseModel);
                         }
                     }
                 }
 
                 //  int iCountSentToSAP = 0;
                 //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                 //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                 //below code commented by abina
                 //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                 //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                 IList<GenericItemModel> genericModel = null;
                 if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     genericModel = ScTestResultGoodyear(itemModel, trnsLineItems);
                 }
                 else
                 {
                     switch (objGPCustomerMessage.TransactionType)
                     {
                         case TRANSACTION_TYPES.SC:
                             genericModel = ScTestResult(itemModel, trnsLineItems);
                             break;
                         // case TRANSACTION_TYPES.SSC:
                         //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
                         //break;
                     }
                 }
                 if (genericModel.Count > 0)
                 {
                     genericModel[0].sapResponseData = sapSubmitTransData;
                 }
                 Gen_User userDetails;
                 using (var dataContext = new GoodpackEDIEntities())
                 {
                     userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                 }
 
                 Dictionary<string, string> placeholders = new Dictionary<string, string>();
                 placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                 placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                 placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                 placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                 placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                 placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                 if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                 {
                     //send email to customer and internal users of goodpack
                     //emailid of customer get from batchfilesource address
                     string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                     List<Attachment> attachments1 = new List<Attachment>();
                     string strFileSC;
                     strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                     strFileSC += "\\" + objGPCustomerMessage.BatchFileName;
 
                     Attachment attachment1 = new Attachment(strFileSC);
                     attachment1.Name = objGPCustomerMessage.BatchFileName;
                     attachments1.Add(attachment1);
                     log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
 
 
                     GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                     List<Attachment> attachments2 = new List<Attachment>();
                     Attachment attachment2 = new Attachment(strFileSC);
                     attachment2.Name = objGPCustomerMessage.BatchFileName;
                     attachments2.Add(attachment2);
                     log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                     GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                 }
                 else
                 {
                     if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                     {
                         //goodyear notification
                         List<Attachment> attachments = new List<Attachment>();
                         string strGoodyearSC;
                         strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                         if (isWeb)
                             strGoodyearSC = strGoodyearSC + "\\" + objGPCustomerMessage.BatchFileName;
                         //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                         Attachment attachment = new Attachment(strGoodyearSC);
                         attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                         attachments.Add(attachment);
                         log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                         GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                     }
                     else
                     {
                         //fileupload SO
                         //send email to users
                         GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                     }
                 }
                 log.Info("Email send to customer" + userDetails.FullName.ToString());
                 for (int i = 0; i < genericModel.Count; i++)
                 {
                     itemModelData.Add(genericModel[i]);
                     if (i == 0)
                     {
                         itemModelData.Add(itemModelVal);
                     }
                 }
                 log.Info("Leaving from verifyData in SCManager");
                 return itemModelData;
             }
             catch (Exception exp)
             {
                 log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                 IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                 GenericItemModel itemsModel = new GenericItemModel();
                 objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                 objGPCustomerMessage.ErrorDesc = exp.Message;
                 itemsModel.Status = "false";
                 itemsModel.Message = "Unable to process your request.";
                 // Update TRANS_BATCH
                 genericModel.Add(itemsModel);
                 UpdateSCBatchFileData(objGPCustomerMessage);
                 Dictionary<string, string> placeholder = new Dictionary<string, string>();
                 // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                 placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                 return genericModel;
 
             }
         }
 
 
         #region OldVerify
         //public IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, int fileType, bool isFileData)
         //{
         //    try
         //    {
         //        GenericItemModel itemModelVal = itemModel[1];
         //        objGPCustomerMessage = new SCTransactionModel();
         //        List<string> dataValue;
         //        List<string> fileName;
         //        List<string> filePath;
         //        List<string> message;
         //        List<string> emailId;
         //        dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
         //        fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
         //        filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
         //        message = itemModel.Select(x => x.Message).Distinct().ToList();
         //        emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
         //        objGPCustomerMessage.BatchFileName = fileName[0].ToString();
         //        objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
         //        objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
         //        objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
         //        if (fileType == 1)
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
         //            itemModel.RemoveAt(1);
         //            using (var context = new GoodpackEDIEntities())
         //            {
         //                objGPCustomerMessage.EmailId = (from s in context.Gen_User
         //                                                where s.Username == username
         //                                                select s.EmailId).FirstOrDefault();
         //            }
         //        }
         //        else if (fileType == 2)
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
         //        }
         //        else if (fileType == 3)
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
         //        }
         //        using (var context = new GoodpackEDIEntities())
         //        {
         //            int userId = (from s in context.Gen_User
         //                          where s.Username == username
         //                          select s.Id).FirstOrDefault();
         //            objGPCustomerMessage.UserId = userId;
 
         //            objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
         //                                                                                                             join e in context.Ref_TransactionTypes
         //                                                                                                             on s.TransactionCodeId equals e.Id
         //                                                                                                             where s.Filename == mapperFile
         //                                                                                                             select e.TransactionCode).FirstOrDefault());
         //        }
         //        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
         //        objGPCustomerMessage.DateCreated = DateTime.Now;
         //        objGPCustomerMessage.Service = "GPMapper";
         //        objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
         //        List<int> trnsLineItems = new List<int>();
         //        GPMapper mapper = new GPMapper();
         //        MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
         //        List<string> ediNumber = new List<string>();
         //        if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
         //               && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
         //        {
         //            //Insert lines in Trn_TransBatch table                    
         //            objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
         //            objGPCustomerMessage.BatchID = AddSCBatchFileData(objGPCustomerMessage, mapperFile);
         //            itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
         //            int i = 0;
         //            // Update/Insert lines in TRANS_LINE table
         //            foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
         //            {
         //                objLineReferences.StatusCode = LineStatus.NEW;
         //                string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
         //                string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
         //                ediNumber.Add(objLineReferences.EdiNumber);
         //                int transID = 0;
         //                //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
         //                switch (objGPCustomerMessage.TransactionType)
         //                {
         //                    case TRANSACTION_TYPES.SC: transID = InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
         //                        break;
         //                    //case TRANSACTION_TYPES.SSC: transID = InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
         //                    //    break;
         //                }
         //                trnsLineItems.Add(transID);
         //                i++;
         //            }
         //        }
 
         //        // Update status of transaction to processed.
         //        objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
         //        objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
         //        objGPCustomerMessage.ErrorDesc = string.Empty;
         //        log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
         //        UpdateSCBatchFileData(objGPCustomerMessage);
         //        UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
         //          0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
 
         //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
         //            || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
         //        {
         //            objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
         //        }
         //        else
         //        {
         //            objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
         //            // Update TRANS_LINE status
         //            log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
         //            switch (objGPCustomerMessage.TransactionType)
         //            {
         //                case TRANSACTION_TYPES.SC: UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
         //                    break;
         //                //case TRANSACTION_TYPES.SSC: UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
         //                //    break;
         //            }
 
         //            Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
         //            sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
         //            int successCount = 0;
         //            int errorCount = 0;
         //            for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
         //            {
         //                trLineID = trnsLineItems[i];
         //                log.Info("Updating TransLine table with excel data");
         //                switch (objGPCustomerMessage.TransactionType)
         //                {
         //                    case TRANSACTION_TYPES.SC: UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
         //                        break;
         //                    //case TRANSACTION_TYPES.SSC: UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
         //                    //    break;
         //                }
 
         //                try
         //                {
         //                    List<ResponseModel> responseModel = null;
         //                    SCServiceManager serviceCaller = new SCServiceManager();
         //                    // SSCServiceManager sscCaller = new SSCServiceManager();
         //                    log.Info("Calling SC webservice");
         //                    if (isFileData)
         //                    {
         //                        switch (objGPCustomerMessage.TransactionType)
         //                        {
         //                            case TRANSACTION_TYPES.SC:
         //                                responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
         //                                break;
         //                            //case TRANSACTION_TYPES.SSC:
         //                            //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
         //                            //    break;
         //                        }
         //                    }
         //                    else
         //                    {
         //                        switch (objGPCustomerMessage.TransactionType)
         //                        {
         //                            case TRANSACTION_TYPES.SC:
         //                                responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
         //                                break;
         //                            //    case TRANSACTION_TYPES.SSC:
         //                            //responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId, ediNumber[i]);
         //                            //break;
         //                        }
         //                    }
 
 
         //                    serviceResponse.Add(trLineID, responseModel);
 
         //                    log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
         //                    bool isSuccess = false;
         //                    switch (objGPCustomerMessage.TransactionType)
         //                    {
         //                        case TRANSACTION_TYPES.SC:
         //                            isSuccess = InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
         //                            break;
 
         //                        //case TRANSACTION_TYPES.SSC:
         //                        //    isSuccess = InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
         //                        //    break;
         //                    }
         //                    log.Info("Insertion of trans line content success status:" + isSuccess);
         //                    if (isSuccess)
         //                    {
         //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
         //                        switch (objGPCustomerMessage.TransactionType)
         //                        {
         //                            case TRANSACTION_TYPES.SC:
         //                                UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
         //                                break;
         //                            //case TRANSACTION_TYPES.SSC: UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
         //                            //    break;
         //                        }
         //                    }
         //                    else
         //                    {
         //                        log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
 
         //                        switch (objGPCustomerMessage.TransactionType)
         //                        {
         //                            case TRANSACTION_TYPES.SC:
         //                                UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
         //                                break;
         //                            //case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
         //                            //    break;
         //                        }
         //                    }
         //                    sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
 
         //                    switch (objGPCustomerMessage.TransactionType)
         //                    {
         //                        case TRANSACTION_TYPES.SC: UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
         //                            break;
         //                        //case TRANSACTION_TYPES.SSC: UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
         //                        //    break;
         //                    }
         //                }
         //                catch (Exception e)
         //                {
         //                    log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
         //                    List<ResponseModel> responseModel = new List<ResponseModel>();
         //                    ResponseModel model = new ResponseModel();
         //                    model.Success = false;
         //                    model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
         //                    responseModel.Add(model);
         //                    errorCount++;
         //                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
         //                    objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
         //                    objGPCustomerMessage.ErrorDesc = e.Message;
         //                    switch (objGPCustomerMessage.TransactionType)
         //                    {
         //                        case TRANSACTION_TYPES.SC:
         //                            SCTransactionData(responseModel, isFileData);
         //                            UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
         //                            break;
         //                        //case TRANSACTION_TYPES.SSC:
         //                        //    SSCTransactionData(responseModel, isFileData);
         //                        //    scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
         //                        //    break;
         //                    }
         //                    serviceResponse.Add(trLineID, responseModel);
         //                }
         //            }
         //        }
         //        IList<GenericItemModel> genericModel = null;
         //        if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
         //            genericModel = ScTestResultGoodyear(itemModel, trnsLineItems);
         //        }
         //        else
         //        {
         //            switch (objGPCustomerMessage.TransactionType)
         //            {
         //                case TRANSACTION_TYPES.SC:
         //                    genericModel = ScTestResult(itemModel, trnsLineItems);
         //                    break;
         //                // case TRANSACTION_TYPES.SSC:
         //                //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
         //                //break;
         //            }
         //        }
         //        if (genericModel.Count > 0)
         //        {
         //            genericModel[0].sapResponseData = sapSubmitTransData;
         //        }
         //        Gen_User userDetails;
         //        using (var dataContext = new GoodpackEDIEntities())
         //        {
         //            userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
         //        }
 
         //        Dictionary<string, string> placeholders = new Dictionary<string, string>();
         //        placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
         //        placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
         //        placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
         //        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
         //        placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
         //        placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
         //        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
         //        if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
         //        {
         //            //send email to customer and internal users of goodpack
         //            //emailid of customer get from batchfilesource address
         //            string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
         //            List<Attachment> attachments1 = new List<Attachment>();
         //            string strFileSC;
         //            strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
         //            strFileSC += "\\" + objGPCustomerMessage.BatchFileName;
 
         //            Attachment attachment1 = new Attachment(strFileSC);
         //            attachment1.Name = objGPCustomerMessage.BatchFileName;
         //            attachments1.Add(attachment1);
         //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
 
 
         //            GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
         //            List<Attachment> attachments2 = new List<Attachment>();
         //            Attachment attachment2 = new Attachment(strFileSC);
         //            attachment2.Name = objGPCustomerMessage.BatchFileName;
         //            attachments2.Add(attachment2);
         //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
         //            GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
         //        }
         //        else
         //        {
         //            if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
         //            {
         //                //goodyear notification
         //                List<Attachment> attachments = new List<Attachment>();
         //                string strGoodyearSC;
         //                strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
         //                //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
         //                Attachment attachment = new Attachment(strGoodyearSC);
         //                attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
         //                attachments.Add(attachment);
         //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
         //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
         //            }
         //            else
         //            {
         //                //fileupload SO
         //                //send email to users
         //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
         //            }
         //        }
         //        log.Info("Email send to customer" + userDetails.FullName.ToString());
         //        for (int i = 0; i < genericModel.Count; i++)
         //        {
         //            itemModelData.Add(genericModel[i]);
         //            if (i == 0)
         //            {
         //                itemModelData.Add(itemModelVal);
         //            }
         //        }
         //        log.Info("Leaving from verifyData in SCManager");
         //        return itemModelData;
         //    }
         //    catch (Exception exp)
         //    {
         //        log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
         //        IList<GenericItemModel> genericModel = new List<GenericItemModel>();
         //        GenericItemModel itemsModel = new GenericItemModel();
         //        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
         //        objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
         //        objGPCustomerMessage.ErrorDesc = exp.Message;
         //        itemsModel.Status = "false";
         //        itemsModel.Message = "Unable to process your request.";
         //        // Update TRANS_BATCH
         //        genericModel.Add(itemsModel);
         //        UpdateSCBatchFileData(objGPCustomerMessage);
         //        Dictionary<string, string> placeholder = new Dictionary<string, string>();
         //        // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
         //        placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
         //        GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
         //        return genericModel;
         //    }
 
         //}
 
         #endregion
         public string getSubsyNameFromId(int SubsiId)
         {
             string SubsiName;
             using (var context = new GoodpackEDIEntities())
             {
                 SubsiName = (from s in context.Gen_EDISubsies
                              where s.Id == SubsiId
                              select s.SubsiName).FirstOrDefault();
             }
             return SubsiName;
 
 
         }
 
         #region private
 
         private string CheckValidDecode(Dictionary<string, string> dataVal)
         {
             StringBuilder returnVal = new StringBuilder();
             if (!dataVal[SCSapMessageFields.CustomerNumber].StartsWith("3") || (dataVal[SCSapMessageFields.CustomerNumber].Length != 6))
             {
                 returnVal.Append("Customer does not have Decode value " + Environment.NewLine);
             }
             if (!dataVal[SCSapMessageFields.FromLocation].StartsWith("5") || (dataVal[SCSapMessageFields.FromLocation].Length != 6))
             {
                 returnVal.Append("Packing plant does not have Decode value " + Environment.NewLine);
             }
             if (!dataVal[SCSapMessageFields.ToLocation].StartsWith("6") || (dataVal[SCSapMessageFields.FromLocation].Length != 6))
             {
                 returnVal.Append("Consignee does not have Decode value " + Environment.NewLine);
             }
             return returnVal.ToString();
         }
 
         public string[] getAllRecipients(string mapperFile)
         {
             List<string> mailList = new List<string>();
             //  mailList.Add(objGPCustomerMessage.EmailId);            
             using (var context = new GoodpackEDIEntities())
             {
                 int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                                where s.Filename == mapperFile
                                select s.EDISubsiId).FirstOrDefault();
                 IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                         where s.SubsiId == subsiId
                                         select s.UserId).ToList();
 
                 foreach (var item in userLists)
                 {
                     string mailId = (from s in context.Gen_User
                                      where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                      select s.EmailId).FirstOrDefault();
                     log.Info("Mail sending to " + mailId);
                     if (mailId != null)
                     {
                         mailList.Add(mailId);
                     }
 
 
                 }
                 string emailId = (from s in context.Gen_Configuration
                                   where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                   select s.Value).FirstOrDefault();
                 mailList.Add(emailId);
                 return mailList.ToArray();
             }
 
 
         }
 
         public string[] getAllRecipients(int subsiId)
         {
             List<string> mailList = new List<string>();
             //  mailList.Add(objGPCustomerMessage.EmailId);            
             using (var context = new GoodpackEDIEntities())
             {
                 //int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                 //               where s.Filename == mapperFile
                 //               select s.EDISubsiId).FirstOrDefault();
                 IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                         where s.SubsiId == subsiId
                                         select s.UserId).ToList();
 
                 foreach (var item in userLists)
                 {
                     string mailId = (from s in context.Gen_User
                                      where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                      select s.EmailId).FirstOrDefault();
                     log.Info("Mail sending to " + mailId);
                     if (mailId != null)
                     {
                         mailList.Add(mailId);
                     }
 
 
                 }
                 string emailId = (from s in context.Gen_Configuration
                                   where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                   select s.Value).FirstOrDefault();
                 mailList.Add(emailId);
                 return mailList.ToArray();
             }
 
 
         }
 
         private IList<GenericItemModel> ScTestResult(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 for (int i = 0; i < trnsLineItems.Count; i++)
                 {
                     int lineId = trnsLineItems[i];
                     Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                     int statusId = transLine.StatusId;
                     string statusCode = (from s in context.Trn_LineStatus
                                          where s.Id == statusId
                                          select s.StatusCode).FirstOrDefault();
 
                     itemModel[i + 1].LineId = lineId;
                     itemModel[i + 1].Status = statusCode;
 
                 }
                 return itemModel;
             }
         }
 
         private IList<GenericItemModel> ScTestResultGoodyear(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 for (int i = 0; i < trnsLineItems.Count; i++)
                 {
                     int lineId = trnsLineItems[i];
                     Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                     int statusId = transLine.StatusId;
                     string statusCode = (from s in context.Trn_LineStatus
                                          where s.Id == statusId
                                          select s.StatusCode).FirstOrDefault();
 
                     itemModel[i].LineId = lineId;
                     itemModel[i].Status = statusCode;
 
                 }
                 return itemModel;
             }
         }
 
         private void SCTransactionData(List<ResponseModel> responseModel, bool isFileData)
         {
             InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
             UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
         }
 
         private int AddSCBatchFileData(SCTransactionModel transBtachDetails, string mapperFile)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string transactionType = transBtachDetails.TransactionType.ToString();
                 int transTypeID = (from s in context.Ref_TransactionTypes
                                    where s.TransactionCode == transactionType
                                    select s.Id).FirstOrDefault();
                 string statusCode = transBtachDetails.StatusCode.ToString();
                 int statusID = (from s in context.Ref_BatchStatus
                                 where s.StatusCode == statusCode
                                 select s.Id).FirstOrDefault();
                 string errorCode = transBtachDetails.ErrorCode.ToString();
                 int errorCodeID = (from s in context.Ref_ErrorCodes
                                    where s.ErrorCode == errorCode
                                    select s.Id).FirstOrDefault();
 
 
                 Trn_SCTransBatch scFileData = new Trn_SCTransBatch();
                 scFileData.UserId = transBtachDetails.UserId;
                 scFileData.SubsyId = transBtachDetails.SubsiId;
                 scFileData.TransactionId = transTypeID;
                 scFileData.MapperFileName = mapperFile;
                 scFileData.StatusId = statusID;
                 scFileData.ErrorId = errorCodeID;
                 scFileData.FileName = transBtachDetails.BatchFileName;
                 scFileData.FileSource = transBtachDetails.BatchFileSourceType.ToString();
                 scFileData.FileSourceAddress = transBtachDetails.BatchFileSourceAddress;
                 scFileData.BatchContent = transBtachDetails.Message;
                 scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                 scFileData.DateCreated = DateTime.Now;
                 scFileData.DateUpdated = null;
                 scFileData.PODate = null;
                 scFileData.RecordCountTotal = 0;
                 scFileData.RecordCountSkipValFailure = 0;
                 scFileData.RecordCountFieldValFailure = 0;
                 scFileData.RecordCountSAPError = 0;
                 scFileData.RecordCountSuccess = 0;
                 scFileData.RecordCountSAPInProgress = 0;
                 scFileData.DateExportedToTargetSystem = null;
                 scFileData.IsTestMode = true;
                 scFileData.EmailId = transBtachDetails.EmailId;
                 context.Trn_SCTransBatch.Add(scFileData);
                 context.SaveChanges();
                 int scFileId = (from s in context.Trn_SCTransBatch
                                 where (s.Id == scFileData.Id)
                                 select s.Id).FirstOrDefault();
 
 
                 return scFileId;
 
 
             }
 
         }
 
         private int InsertTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference, string customerReference)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCodeVal = lineReference.StatusCode.ToString();
                 int statusID = (from s in context.Trn_LineStatus
                                 where s.StatusCode == statusCodeVal
                                 select s.Id).FirstOrDefault();
 
                 Trn_SCTransLine transLine = new Trn_SCTransLine();
                 transLine.BatchId = transactionModel.BatchID;
                 transLine.LastUpdated = DateTime.Now;
                 transLine.LineContent = lineReference.LineContentItems;
                 transLine.CustomerCode = lineReference.CustomerCode;
                 transLine.BinType = lineReference.MaterialCode;
                 transLine.CustomerRefNumber = customerReference;
                 if (lineReference.LineNumber == null)
                 {
                     transLine.LineNumber = 0;
                 }
                 else
                 {
                     transLine.LineNumber = int.Parse(lineReference.LineNumber);
                 }
 
                 transLine.SourceReference = lineReference.SourceReferenceCode;
                 transLine.GPReference = gpReference;
                 transLine.EDIReference = lineReference.EdiNumber;
                 transLine.StatusId = statusID;
                 transLine.SubsiId = transactionModel.SubsiId;
                 transLine.IsActive = true;
                 context.Trn_SCTransLine.Add(transLine);
                 context.SaveChanges();
                 return transLine.Id;
             }
         }
 
         private void UpdateSCBatchFileData(SCTransactionModel transBtachDetails)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCode = transBtachDetails.StatusCode.ToString();
                 string errorCode = transBtachDetails.ErrorCode.ToString();
                 int statusID = (from s in context.Ref_BatchStatus
                                 where s.StatusCode == statusCode
                                 select s.Id).FirstOrDefault();
                 int errorCodeID = (from s in context.Ref_ErrorCodes
                                    where s.ErrorCode == errorCode
                                    select s.Id).FirstOrDefault();
 
                 Trn_SCTransBatch scFileData = context.Trn_SCTransBatch.FirstOrDefault(x => (x.Id == transBtachDetails.BatchID)); //&& x.StatusId != 10 && x.StatusId != 12 && x.StatusId != 13));//Give where condition in valuetype
                 scFileData.StatusId = statusID;
                 scFileData.ErrorId = errorCodeID;
                 scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                 scFileData.DateUpdated = DateTime.Now;
                 context.SaveChanges();
             }
 
         }
 
         private void UpdateSCBatchCounts(int BatchId, int RecordCountTotal, int RecordCountSkipValFailure, int RecordCountFieldValFailure,
                                         int RecordCountSAPInProgress, int RecordCountSAPError, int RecordCountSuccess, string strService)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 if (strService == "Request")
                 {
                     Trn_SCTransBatch scBatch = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                     scBatch.RecordCountTotal = RecordCountTotal;
                     scBatch.RecordCountSkipValFailure = RecordCountSkipValFailure;
                     scBatch.RecordCountFieldValFailure = RecordCountFieldValFailure;
                     // scBatch.RecordCountSuccess = RecordCountSuccess;
                     scBatch.RecordCountSAPInProgress = RecordCountSAPInProgress;
                     context.SaveChanges();
 
                     if (RecordCountSAPInProgress != 0)
                     {
                         Trn_SCTransBatch scBatchData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                         scBatchData.DateExportedToTargetSystem = DateTime.Now;
                         context.SaveChanges();
                     }
                 }
                 else
                 {
                     //UPDATE TRANS_BATCH SET RecordCountSuccess=@SuccessCount, RecordCountSAPError=@SAPErrorCount
 
                     //   where BatchID=@BatchId
 
                     Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                     scData.RecordCountSuccess = RecordCountSuccess;
                     scData.RecordCountSAPError = RecordCountSAPError;
                     context.SaveChanges();
 
                     Trn_SCTransBatch scTransData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                     scTransData.RecordCountSAPInProgress = RecordCountTotal - RecordCountSkipValFailure - RecordCountFieldValFailure - RecordCountSAPError - RecordCountSuccess;
                     context.SaveChanges();
                 }
             }
         }
 
         private void UpdateTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCode = LineStatus.ERROR.ToString();
                 int statusCodeId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusCode
                                     select s.Id).FirstOrDefault();
                 int transLine = (from s in context.Trn_SCTransLine
                                  where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                  select s.Id).FirstOrDefault();
                 string batchStatusVal = null;
 
 
                 if (transLine == 0)
                 {
                     batchStatusVal = BatchStatus.CLOSED.ToString();
                 }
                 else
                 {
                     batchStatusVal = BatchStatus.ERROR.ToString();
                 }
                 log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                 int batchStatusId = (from s in context.Ref_BatchStatus
                                      where s.StatusCode == batchStatusVal
                                      select s.Id).FirstOrDefault();
                 Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                 ITirmTripManager termtrip = new TirmTripManager();
                 scDataValue.StatusId = batchStatusId;
                 scDataValue.IsTestMode = InTestMode;
                 var value = scDataValue.Trn_SCTransLine.Select(x => x.CustomerCode).FirstOrDefault();
                 scDataValue.IsTermTrip = termtrip.IsTirmTripORNot(value.Length > 6 ? value.Substring(4, 6) : value);
                 scDataValue.TermTripSAPResponseReceived = false;
                 // scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess+successCount;
                 // scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess??0;
                 scDataValue.RecordCountSAPError = errorCount;
                 scDataValue.RecordCountSuccess = successCount;
                 context.SaveChanges();
             }
         }
 
         private void UpdateTransBatch(int batchId, int successCount, int errorCount, bool InTestMode, bool isTermTrip)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCode = LineStatus.ERROR.ToString();
                 int statusCodeId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusCode
                                     select s.Id).FirstOrDefault();
                 int transLine = (from s in context.Trn_SCTransLine
                                  where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                  select s.Id).FirstOrDefault();
                 string batchStatusVal = null;
 
 
                 if (transLine == 0)
                 {
                     batchStatusVal = BatchStatus.CLOSED.ToString();
                 }
                 else
                 {
                     batchStatusVal = BatchStatus.ERROR.ToString();
                 }
                 log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                 int batchStatusId = (from s in context.Ref_BatchStatus
                                      where s.StatusCode == batchStatusVal
                                      select s.Id).FirstOrDefault();
                 Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                 batchStatusId = isTermTrip ? context.Ref_BatchStatus.Where(x => x.StatusCode == "RCVD").Select(x => x.Id).SingleOrDefault() : batchStatusId;
                 scDataValue.StatusId = batchStatusId;
                 scDataValue.IsTestMode = InTestMode;
                 scDataValue.IsTermTrip = isTermTrip;
                 scDataValue.TermTripSAPResponseReceived = false;
                 scDataValue.RecordCountSAPError = errorCount;
                 scDataValue.RecordCountSuccess = successCount;
                 context.SaveChanges();
             }
         }
 
         private void UpdateTransLineForData(int trLineID, Dictionary<string, string> scDataVal)
         {
             try
             {
                 using (var context = new GoodpackEDIEntities())
                 {
 
                     Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();
 
                     if (transLine != null)
                     {
 
                         transLine.CustomerCode = scDataVal[SCSapMessageFields.CustomerNumber];
                         string dateIssue = scDataVal[SCSapMessageFields.ETDDate];
                         if ((dateIssue != "") || (dateIssue != null))
                         {
                             string dateVal = dateIssue.Substring(0, 2);
                             string monthVal = dateIssue.Substring(2, 2);
                             string yearVal = dateIssue.Substring(4, 4);
                             transLine.ETD = yearVal + "-" + monthVal + "-" + dateVal;
                         }
 
                         string dateIssued = scDataVal[SCSapMessageFields.DTADate];
                         if ((dateIssued != "") || (dateIssued != null))
                         {
                             string dateVal = dateIssued.Substring(0, 2);
                             string monthVal = dateIssued.Substring(2, 2);
                             string yearVal = dateIssued.Substring(4, 4);
                             transLine.ETA = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                         }
 
                         if (scDataVal[SCSapMessageFields.FromLocation].Length != 10)
                         {
                             transLine.FromLocation = "0000" + scDataVal[SCSapMessageFields.FromLocation];
                         }
                         else
                         {
                             transLine.FromLocation = scDataVal[SCSapMessageFields.FromLocation];
                         }
                         transLine.LastUpdated = DateTime.Now;
                         transLine.Remarks = scDataVal[SCSapMessageFields.Remarks];
                         transLine.SINumber = scDataVal[SCSapMessageFields.SINumber];
                         if (scDataVal[SCSapMessageFields.Quantity].Length != 0)
                         {
                             int s;
                             int.TryParse(scDataVal[SCSapMessageFields.Quantity], out s);
                             if (s == 0)
                                 throw new InvalidCastException("Quantity should be integer");
                             transLine.Quantity = Convert.ToInt32(scDataVal[SCSapMessageFields.Quantity]);
 
                         }
                         else
                         {
                             transLine.Quantity = 0;
                         }
                         if (scDataVal[SCSapMessageFields.ToLocation].Length != 10)
                         {
                             transLine.ToLocation = "0000" + scDataVal[SCSapMessageFields.ToLocation];
                         }
                         else
                         {
                             transLine.ToLocation = scDataVal[SCSapMessageFields.ToLocation];
                         }
                         transLine.BinType = scDataVal[SCSapMessageFields.MaterialNumber];
                         transLine.CustomerRefNumber = scDataVal[SCSapMessageFields.CustomerReferenceNumber];
                         transLine.ItrType = scDataVal[SCSapMessageFields.DocumentType];
                         transLine.ItrNumber = scDataVal[SCSapMessageFields.ITRNumber];
                         transLine.RefItrNumber = scDataVal[SCSapMessageFields.ReferenceITRNumber];
                         transLine.ItemNumber = scDataVal[SCSapMessageFields.ItemNo];
                         transLine.ContainerNumber = scDataVal[SCSapMessageFields.ContainerNumber];
                         transLine.SalesDocument = scDataVal[SCSapMessageFields.SalesDocument];
                         if (transLine.SalesDocument.Length != 0)
                         {
                             transLine.SalesDocumentItem = ConstantUtilities.SalesDocumentItem;
                         }
                         context.SaveChanges();
                     }
                 }
             }
             catch (Exception e)
             {
                 log.Error("UpdateTransLineForData-" + e);
                 throw e;
             }
         }
 
         private void UpdateTransLine(int batchId, int subsiId, LineStatus? statusCode)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCodeVal = statusCode.ToString();
                 int statusID = (from s in context.Trn_LineStatus
                                 where s.StatusCode == statusCodeVal
                                 select s.Id).FirstOrDefault();
 
                 string checkstatusValue = LineStatus.NEW.ToString();
                 int checkStatusID = (from s in context.Trn_LineStatus
                                      where s.StatusCode == checkstatusValue
                                      select s.Id).FirstOrDefault();
 
                 var transLine = (context.Trn_SCTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();
 
                 foreach (var item in transLine)
                 {
                     if (item.StatusId != 0)
                         item.StatusId = statusID;
                     item.SubsiId = subsiId;
                     item.BatchId = batchId;
                     context.SaveChanges();
                 }
             }
         }
 
         private void UpdateSCBatchCounts(int batchId, int errorCount, int successCount)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                 scData.RecordCountSuccess = successCount;
                 scData.RecordCountSAPError = errorCount;
                 context.SaveChanges();
                 log.Info("Updated Trn_SCTransBatch table of batch Id:" + batchId + " with success count:" + successCount + "and error count:" + errorCount);
             }
         }
 
         private bool InsertTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
         {
             string lineStatus = null;
             bool isSuccess = true;
             using (var context = new GoodpackEDIEntities())
             {
                 Trn_SCTransLineContent lineContent = new Trn_SCTransLineContent();
                 for (int i = 0; i < responseModel.Count; i++)
                 {
                     bool responseStatus=Convert.ToBoolean(responseModel[i].Success);
                     if (responseStatus)
                     {
                         lineStatus = LineStatus.SUCCESS.ToString();
                     }
                     else
                     {
                         isSuccess = false;
                         lineStatus = LineStatus.ERROR.ToString();
                     }
                     log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                     int lineStatusId = (from s in context.Trn_LineStatus
                                         where s.StatusCode == lineStatus
                                         select s.Id).FirstOrDefault();
                     lineContent.BatchID = batchId;
                     lineContent.LineID = lineId;
                     lineContent.StatusID = lineStatusId;
                     lineContent.SapResponseMessage = responseModel[i].Message;
                     context.Trn_SCTransLineContent.Add(lineContent);
                     context.SaveChanges();
                 }
             }
             return isSuccess;
         }
 
         private void UpdateResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 string statusCodeVal = statusCode.ToString();
                 int statusID = (from s in context.Trn_LineStatus
                                 where s.StatusCode == statusCodeVal
                                 select s.Id).FirstOrDefault();
 
                 Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                 transLine.StatusId = statusID;
                 transLine.InTestMode = status;
                 context.SaveChanges();
                 log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
             }
         }
         private bool Validate_IsFileTypeSupported(SCTransactionModel objGPCustomerMessage)
         {
             //objILog.Debug("Entering Validate_IsFileTypeSupported method");
             bool blnValidFileType = false;
             try
             {
                 FILE_TYPES fileType;
                 blnValidFileType = this.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                 if (blnValidFileType == false)
                 {
                     objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                     objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                     //objILog.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                 }
             }
             catch (Exception exp)
             {
                 log.Error(" fileUpload in SCManager Validate_IsFileTypeSupported method.objGPCustomerMessage:" + objGPCustomerMessage + ". Api Exception : Message- " + exp.Message);
                 throw exp;
             }
             //objILog.Debug("Leaving Validate_IsFileTypeSupported method");
             return blnValidFileType;
         }
         private bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
         {
             bool blnValidFileType = false;
             //get value from db file types table
             IList<string> lstFileExtns = new List<string>();
             lstFileExtns.Add(".txt");
             lstFileExtns.Add(".csv");
             lstFileExtns.Add(".xls");
             lstFileExtns.Add(".xlsx");
             enumFileType = FILE_TYPES.CSV;
             foreach (string strFileExt in lstFileExtns)
             {
                 if (strFileName.ToLower().EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
                 {
                     if (strFileExt.ToLower() == ".csv")
                         enumFileType = FILE_TYPES.CSV;
                     else
                         enumFileType = FILE_TYPES.TEXT;
                     blnValidFileType = true;
                     break;
                 }
             }
             return blnValidFileType;
         }
         private string[] SCSumiData(FileInfo fileInfo)
         {
             string scData;
 
             if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
             {
                 scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                 scData = scData.Replace(",", "|");
                 objGPCustomerMessage.Message = GetDataWithoutEmptyColumns(scData);
 
             }
 
             else
             {
                 using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                 {
                     scData = objStreamReader.ReadToEnd();
                     scData = scData.Replace("\t", "|");
                     objGPCustomerMessage.Message = scData;
                     objStreamReader.Close();
                     objStreamReader.Dispose();
                 }
             }
             //  return scData.Split('\r\n');  
             return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
 
         }
         private string[] SCDataSynthosis(FileInfo fileInfo, out string ObjMessage)
         {
             string scData;
 
             if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
             {
                 scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                 scData = GetDataWithoutEmptyColumns(scData);
                 objGPCustomerMessage.Message = scData;
                 ObjMessage = scData;
             }
             else
             {
                 using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                 {
                     var line = "";
                     List<String> lines = new List<string>();
                     bool firstItem = true;
                     int indexValue = 0;
                     //For removing blank column from CSV
                     while ((line = objStreamReader.ReadLine()) != null)
                     {
                         if (Regex.IsMatch(line, "^[a-zA-Z0-9]"))
                         {
                             if (firstItem)
                             {
                                 if (line.LastIndexOf(';') + 1 == line.Length)
                                 {
                                     line = line.Remove(line.LastIndexOf(';'));
                                 }
                                 indexValue = line.Split(';').Length;
                                 firstItem = false;
                             }
                             else
                             {
                                 string[] values;
                                 if ((line.Split(';').Length) != indexValue)
                                 {
                                     values = line.Split(';');
                                     line = string.Join(";", values, 0, indexValue);
                                 }
                             }
                             lines.Add(line.Replace(";",","));
                         }
                     }
                     scData = string.Join(Environment.NewLine, lines.ToArray());
                     Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",
                     RegexOptions.Compiled | RegexOptions.IgnoreCase);
                     scData = regData.Replace(scData, "|");
                     objGPCustomerMessage.Message = scData;
                     objStreamReader.Close();
                     objStreamReader.Dispose();
                     ObjMessage = scData;
                 }
             }
             scData = scData.Replace(",", "|");
             return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
 
         }
 
         private string[] SCData(FileInfo fileInfo, out string ObjMessage)
         {
             string scData;
 
             if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
             {
                 scData = ExcelParser.ExcelToCSV(scPath, 0, 0);           
                 scData = GetDataWithoutEmptyColumns(scData);
                 objGPCustomerMessage.Message = scData;
                 ObjMessage = scData;
             }
             else
             {
                 using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                 {
                     var line = "";
                     List<String> lines = new List<string>();
                     bool firstItem = true;
                     int indexValue = 0;
                     //For removing blank column from CSV
                     while ((line = objStreamReader.ReadLine()) != null)
                     {
                         if (Regex.IsMatch(line, "^[a-zA-Z0-9]"))
                         {
                             if (firstItem)
                             {
                                 if (line.LastIndexOf(',') + 1 == line.Length)
                                 {
                                     line = line.Remove(line.LastIndexOf(','));
                                 }
                                 indexValue = line.Split(',').Length;
                                 firstItem = false;
                             }
                             else
                             {
                                 string[] values;
                                 if ((line.Split(',').Length) != indexValue)
                                 {
                                     values = line.Split(',');
                                     line = string.Join(",", values, 0, indexValue);
                                 }
                             }
                             lines.Add(line);
                         }
                     }                    
                     scData = string.Join(Environment.NewLine, lines.ToArray());
                     Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",
                     RegexOptions.Compiled | RegexOptions.IgnoreCase);
                     scData = regData.Replace(scData, "|");                    
                     objGPCustomerMessage.Message = scData;
                     objStreamReader.Close();
                     objStreamReader.Dispose();
                     ObjMessage = scData;
                 }
             }   
             scData = scData.Replace(",", "|");
             return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
 
         }
         private string SAPHeaders(string filename)
         {
             StringBuilder sapFields = new StringBuilder();
             using (var context = new GoodpackEDIEntities())
             {
                 int fileMapperId = (from s in context.Trn_SubsiToTransactionFileMapper
                                     where s.Filename == filename
                                     select s.Id).FirstOrDefault();
 
                 List<Trn_MapperFieldSpecs> specs = (from s in context.Trn_MapperFieldSpecs
                                                     where s.SubsiToTransactionMapperId == fileMapperId
                                                     select s).ToList();
                 Trn_MappingSubsi mapperData = (from s in context.Trn_MappingSubsi
                                                where s.SubsiToTransactionFileMapperId == fileMapperId
                                                select s).FirstOrDefault();
 
                 foreach (var item in specs)
                 {
                     int sapFieldSequenceId = (from s in context.Trn_MappingConfiguration
                                               where (((s.SourceFieldSequence == item.FieldSequence) || (s.SourceFieldSeq2 == item.FieldSequence)) && (s.MappingSubsiId == mapperData.Id))
                                               select s.SapFieldSeq).FirstOrDefault();
                     string sapField = (from s in context.Ref_SAPMessageFields
                                        where ((s.FieldSequence == sapFieldSequenceId) && (s.TransactionId == mapperData.SapMessageFieldId))
                                        select s.SAPFieldName).FirstOrDefault();
                     sapFields.Append(sapField);
                     sapFields.Append("" + ConstantUtilities.delimiters[0]);
                 }
                 return sapFields.Remove(sapFields.Length - 1, 1).ToString();
 
             }
 
         }
         private IList<Trn_MapperFieldSpecs> ValidateData(string fileName)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 IList<Trn_MapperFieldSpecs> mapperFields = (from s in context.Trn_SubsiToTransactionFileMapper
                                                             join p in context.Trn_MapperFieldSpecs on s.Id equals p.SubsiToTransactionMapperId
                                                             where (s.Filename == fileName)
                                                             select p).ToList();
 
                 return mapperFields;
             }
 
         }
         private string GetDataWithoutEmptyColumns(string scData)
         {
             bool firstItem = true;
             string returnValue = "";
             int indexValue = 0;
             List<String> lines = new List<string>();
             string[] lineArray = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
             foreach (string linedata in lineArray)
             {
                 if (linedata != "")
                 {
                     if (firstItem)
                     {
                         if (linedata.LastIndexOf('|') + 1 == linedata.Length)
                         {
                             if (linedata.LastIndexOf("||") + 2 == linedata.Length)
                             {
                                 string tempLine = linedata.Replace("||", "");
                                 string lineValue = (tempLine.LastIndexOf('|') + 1 == tempLine.Length) ? tempLine.Remove(tempLine.LastIndexOf('|')) : tempLine;
                                 returnValue = returnValue + lineValue.Trim() + Environment.NewLine;
                                 indexValue = lineValue.Split('|').Length;
 
                             }
                             else
                             {
                                 returnValue = returnValue + linedata.Remove(linedata.LastIndexOf('|')).Trim() + Environment.NewLine;
                                 indexValue = linedata.Remove(linedata.LastIndexOf('|')).Split('|').Length;
                             }
                             firstItem = false;
                         }
                         else
                         {
                             returnValue = returnValue + linedata.ToString().Trim() + Environment.NewLine;
                             indexValue = linedata.Split('|').Length;
                             firstItem = false;
                         }
                     }
                     else
                     {
                         string[] linedetails;
                         linedetails = linedata.Split('|').Select(e => e.Replace(" \"", "\"")).ToArray();
                         returnValue = returnValue + string.Join("|", linedetails, 0, indexValue) + Environment.NewLine;
                     }
                 }
             }
             return returnValue;
         }
         private string CheckForDecodeValueValidation(string fileData, string headers, string mapperName, int subsiId)
         {
             StringBuilder fileDecodedData = new StringBuilder();
 
             string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
             string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
             IDecodeValueManager decodeValueManager = new DecodeValueManager();
             for (int i = 0; i < headerValues.Length; i++)
             {
                 string sapFieldName = headerValues[i];
                 string inputType = String.Empty;
                 int lookupNameId = 0;
                 int transactionId;
                 if (sapFieldName != "")
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
                         transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                          where s.Filename == mapperName
                                          select s.TransactionCodeId).FirstOrDefault();
 
                         inputType = (from s in context.Ref_SAPMessageFields
                                      where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                      select s.InputType).FirstOrDefault();
                         //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                     }
                     switch (sapFieldName.ToUpper())
                     {
                         case "CUSTOMER CODE":
                         case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             break;
                         case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             break;
                         case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                             break;
                         case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                             break;
                         case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                             break;
                         case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                             break;
                         case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                             break;
                         case "FRM LOCATION":
                         case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             }
                             else
                             {
                                 if (transactionId == 12)
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                                 }
                                 else
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Packer);
                                 }
                             }
                             break;
                         case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Packer);
                             }
                             else
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                             }
                             break;
                         case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                             break;
                     }
                     TargetMessageInputTypes inputSAPType =
                             (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);
 
 
                     if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                     {
                         string decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId);
                         if (decodeValue != null)
                         {
                             if (decodeValue.Length != 0)
                                 errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName));
                             else
                                 errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                             fileDecodedData.Append(decodeValue);
 
                         }
                         else
                         {
                             bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                             if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
                             {
                                 errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                                 //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
                                 fileDecodedData.Append("false");
                             }
                         }
                     }
                     else
                     {
                         errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                     }
 
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
                 else
                 {
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
             }
             fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
             return fileDecodedData.ToString();
         }
         private bool GetLookupIsNull(string lookupName, int transactionId)
         {
 
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_SAPMessageFields
                         where s.SAPFieldName == lookupName && s.TransactionId == transactionId
                         select s.IsAllowNullValue).FirstOrDefault();
 
             }
 
         }
         private int GetLookupId(string LookupName)
         {
 
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_LookupNames
                         where s.LookupName == LookupName
                         select s.Id).FirstOrDefault();
 
             }
 
         }
       
 
         #endregion
     }
 }

 

