﻿
using GPArchitecture.CommonTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.GoodPackBusinessLayer.SCManagerLayer
{
    interface ISCDataVerificationManager
    {
        //MapperSpecModel mapperDetails(string mapperFile);
        SourceMessage mapperDetails(string mapperFile);
       // static List<SourceMessage> Parse(SCTransactionModel objGPCustomerMessage, SourceMessage objSourceMessage, ref bool isSuccess, ref string errorDesc);
    }
}
