﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Numerics;
using System.IO;
using GPArchitecture.DataLayer.Models;
using log4net;
using System.Text;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Parsers;
using GPArchitecture.CommonTypes;

namespace GPArchitecture.GoodPackBusinessLayer.SCManagerLayer
{
    public class SCDataVerificationManager : ISCDataVerificationManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SCDataVerificationManager));

        public SourceMessage mapperDetails(string mapperFile)
        {
            SourceMessage specModel = new SourceMessage();
            using (var context = new GoodpackEDIEntities())
            {
                SCSubsiToTransactionModel mapperModel = (from s in context.Trn_SubsiToTransactionFileMapper
                                                         where s.Filename == mapperFile
                                                         select new SCSubsiToTransactionModel
                                                         {
                                                             Id = s.Id,
                                                             IsHeaderPresent = s.IsHeaderPresent,
                                                             MessageFormatId = s.MessageFormatId,
                                                             MinFieldValueCount = s.MinFieldValueCount,
                                                             SubsiId = s.EDISubsiId,
                                                             TransactionCodeId = s.TransactionCodeId
                                                         }).FirstOrDefault();

                List<MappingFieldSpecModel> fileModel = (from s in context.Trn_MapperFieldSpecs
                                                         where s.SubsiToTransactionMapperId == mapperModel.Id
                                                         select new MappingFieldSpecModel
                                                         {
                                                             FieldName = s.FieldName,
                                                             FieldSequence = s.FieldSequence,
                                                             Length = s.Length ?? 0

                                                         }).ToList();
                specModel.ID = mapperModel.Id;
                specModel.IsHeaderPresent = mapperModel.IsHeaderPresent;
                specModel.MinFieldValCount = mapperModel.MinFieldValueCount;
                specModel.SourceMessageFormat = mapperModel.MessageFormatId.ToString();
                specModel.CustomerHeadCompany = mapperModel.SubsiId.ToString();
                // specModel.
                //specModel.SubsiTransModel = mapperModel;
                //specModel.SpecModel = fileModel;
                //specModel.CustomerHeadCompany = mapperModel.SubsiId;
                //fileModel.ForEach(c =>
                //{

                //    if (c.FieldName != null)
                //    {
                //        specModel.Fields[i] = c.FieldName;
                //    }
                //});
                SourceMessageField messageField;
                for (int i = 0; i < fileModel.Count; i++)
                {
                    messageField = new SourceMessageField();
                    messageField.FieldName = fileModel[i].FieldName;
                    messageField.Length = fileModel[i].Length;
                    messageField.Value = fileModel[i].FieldSequence.ToString();
                    specModel.Fields.Add(i, messageField);
                }

            }
            return specModel;

        }


        public static List<SourceMessage> ParseFromWSObject(SCTransactionModel objGPCustomerMessage, SourceMessage objSourceMessage, object wsInfoArr, ref bool isSuccess, ref string errorDesc)
        {
            if (wsInfoArr == null)
            {
                return ParseFromWSMessage(objGPCustomerMessage, objSourceMessage, ref isSuccess, ref errorDesc);
            }

            try
            {
                List<SourceMessage> parsedMessage = new List<SourceMessage>();
                errorDesc = string.Empty;
                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.PSSC)
                {
                    GPArchitecture.WSTypes.PackerSSCInfo[] packerSSCInfoArr = (GPArchitecture.WSTypes.PackerSSCInfo[])wsInfoArr;
                    SourceMessage msg = null;
                    foreach (GPArchitecture.WSTypes.PackerSSCInfo packerSSCInfo in packerSSCInfoArr)
                    {
                        msg = new SourceMessage(objSourceMessage);

                        msg.OriginalLineContent = GetSeperatedMsg(objGPCustomerMessage, msg, packerSSCInfo);
                        parsedMessage.Add(msg);
                    }
                }
                isSuccess = true;
                return parsedMessage;
            }
            catch (Exception e)
            {
                isSuccess = false;
                errorDesc = e.Message;
                return null;
            }

        }
        private static string GetSeperatedMsg(SCTransactionModel objGPCustomerMessage, SourceMessage msg, object wsInfo)
        {
            StringBuilder commaSeperatedMsg = new StringBuilder();
            if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.PSSC)
            {
                GPArchitecture.WSTypes.PackerSSCInfo packerSSInfo = (GPArchitecture.WSTypes.PackerSSCInfo)wsInfo;
                msg.Fields[0].Value = packerSSInfo.BinNumber;
                commaSeperatedMsg.Append(packerSSInfo.BinNumber);
                commaSeperatedMsg.Append("|");

                msg.Fields[1].Value = packerSSInfo.PackerCode;
                commaSeperatedMsg.Append(packerSSInfo.PackerCode);
                commaSeperatedMsg.Append("|");

                msg.Fields[2].Value = packerSSInfo.SiNumber;
                commaSeperatedMsg.Append(packerSSInfo.SiNumber);
                commaSeperatedMsg.Append("|");

                msg.Fields[3].Value = packerSSInfo.ETDDate;
                commaSeperatedMsg.Append(packerSSInfo.ETADate);
                commaSeperatedMsg.Append("|");

                msg.Fields[4].Value = packerSSInfo.ETDDate;
                commaSeperatedMsg.Append(packerSSInfo.ETDDate);
                commaSeperatedMsg.Append("|");

                msg.Fields[5].Value = packerSSInfo.custRefNum;
                commaSeperatedMsg.Append(packerSSInfo.custRefNum);
                commaSeperatedMsg.Append("|");

                msg.Fields[6].Value = packerSSInfo.BinType;
                commaSeperatedMsg.Append(packerSSInfo.BinType);
                commaSeperatedMsg.Append("|");

                msg.Fields[7].Value = packerSSInfo.Destination;
                commaSeperatedMsg.Append(packerSSInfo.Destination);
                commaSeperatedMsg.Append("|");

                msg.Fields[8].Value = packerSSInfo.CustomerName;
                commaSeperatedMsg.Append(packerSSInfo.CustomerName);
                commaSeperatedMsg.Append("|");
            }
            return commaSeperatedMsg.ToString();

        }
        private static List<SourceMessage> ParseFromWSMessage(SCTransactionModel objGPCustomerMessage, SourceMessage objSourceMessage, ref bool isSuccess, ref string errorDesc)
        {
            try
            {
                List<SourceMessage> parsedMessage = new List<SourceMessage>();
                errorDesc = string.Empty;
                SourceMessage msg = null;
                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.PSSC)
                {
                    string[] stringSeparators = new string[] { "\n" };
                    string[] messageLines = objGPCustomerMessage.Message.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    bool isFirstTime = true;
                    foreach (string message in messageLines)
                    {
                        if (isFirstTime)
                        {
                            isFirstTime = false;
                            continue;
                        }

                        msg = new SourceMessage(objSourceMessage);
                        msg.OriginalLineContent = message;
                        string[] messageCols = message.Split('|');
                        for (int idx = 0; idx < messageCols.Length; idx++)
                        {
                            msg.Fields[idx + 1].Value = messageCols[idx];
                        }
                        parsedMessage.Add(msg);
                    }
                }
                isSuccess = true;
                return parsedMessage;
            }
            catch (Exception e)
            {
                isSuccess = false;
                errorDesc = e.Message;
                return null;
            }
        }

        public static List<SourceMessage> Parse(SCTransactionModel objCustomerMessage, SourceMessage objSourceMessage, ref bool isSuccess, ref string errorDesc)
        {
            List<SourceMessage> parsedMessage = null;

            errorDesc = string.Empty;

            string strFileExt = objCustomerMessage.BatchFileName.Substring(objCustomerMessage.BatchFileName.LastIndexOf('.'));
            //if (objSourceMessage.TransactionType == TRANSACTION_TYPES.FC)
            //{
            //    if (strFileExt == ".xls" || strFileExt == ".xlsx" || strFileExt == ".XLS" || strFileExt == ".XLSX")
            //        objSourceMessage.SourceMessageFormat = MessageFormats.Excel;
            //    else if (strFileExt == ".csv")
            //        objSourceMessage.SourceMessageFormat = MessageFormats.CSV;
            //}

            switch (objSourceMessage.SourceMessageFormat)
            {
                case MessageFormats.CSV:
                case MessageFormats.Excel:
                    //parsedMessage = new List<SourceMessage>();
                    //  SourceMessage msgLineObj = new SourceMessage(objSourceMessage);
                    parsedMessage = GenericDelimiterParser.Parse(objSourceMessage, objCustomerMessage.Message, objSourceMessage.IsHeaderPresent, ConstantUtilities.delimitersNew[0].ToCharArray()[0], ref isSuccess, ref errorDesc);
                    //string filePath = Path.Combine(objCustomerMessage.BatchFileSourceAddress, objCustomerMessage.BatchFileName);
                    //string messageVal =InteropExcelParser.ExcelParser(filePath); 
                    //string[] message = messageVal.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    //for (int i = 0; i < message.Length; i++)
                    //{
                    //    msgLineObj.OriginalLineContent = message[i];
                    //    parsedMessage.Add(msgLineObj);
                    //}
                    //isSuccess = true;
                    break;
                case MessageFormats.FixedFieldLength:
                    parsedMessage = FixedFieldLengthParser.Parse(objSourceMessage, objCustomerMessage.Message, ref isSuccess, ref errorDesc);
                    break;
                case MessageFormats.SemiColonDelimited:
                    parsedMessage = GenericDelimiterParser.Parse(objSourceMessage, objCustomerMessage.Message, objSourceMessage.IsHeaderPresent, ';', ref isSuccess, ref errorDesc);
                    break;
            }

            if (!isSuccess)
            {
                //log4net.GlobalContext.Properties["CustomerCode"] = objCustomerMessage.CustomerCode.ToString();
                //log4net.GlobalContext.Properties["TransactionType"] = objCustomerMessage.TransactionType.ToString();
                //log4net.GlobalContext.Properties["FileName"] = objCustomerMessage.BatchFileName.ToString();
                //GPTools.objILog.Error(errorDesc);
                return null;
            }
            return parsedMessage;
        }//-- End Parse()

        public static string ExtractDateElements(string targetFieldDateFormat, string sourceFieldDataFormat, string sourceFieldValue, out bool isSuccess, out string errorMessage)
        {
            // Init vars
            sourceFieldValue = sourceFieldValue.Replace(".", "/");
            sourceFieldValue = sourceFieldValue.Replace("-", "/");
            sourceFieldDataFormat = sourceFieldDataFormat.Replace(".", "/");
            sourceFieldDataFormat = sourceFieldDataFormat.Replace("-", "/");
            char[] delimiter = sourceFieldDataFormat.Substring(2, 1).ToCharArray();
            string[] date = sourceFieldValue.Split(delimiter);
            string returnDateTimeValue = string.Empty;
            errorMessage = string.Empty;
            isSuccess = true;
            if (sourceFieldDataFormat != null)
            {
                sourceFieldDataFormat = sourceFieldDataFormat.ToUpper();
            }

            try
            {
                // Check length of date field
                if (sourceFieldValue.Length < 6)
                {
                    throw new Exception(
                        string.Format("The date field value of {0} is too short.  A minimum of 6 numeric characters is expected.", sourceFieldValue)
                        );
                }
                // YYYY is expected if date format contains WW
                if (sourceFieldDataFormat.Contains("WW")
                    && !sourceFieldDataFormat.Contains("YYYY"))
                {
                    throw new Exception(
                        string.Format("YYYY is expected in the date format if it contains WW.", sourceFieldDataFormat)
                        );
                }

                // Preformat source field value and source field date format
                sourceFieldDataFormat = sourceFieldDataFormat.ToUpper().Trim();
                sourceFieldValue = sourceFieldValue.Trim();
                targetFieldDateFormat = targetFieldDateFormat.ToUpper();

                // Crop characters from the source field value if it is longer than the source field date format
                // i.e. MM-YYYY, WW-YYYY, DD-YYYY
                if (sourceFieldValue.Length > sourceFieldDataFormat.Length)
                    sourceFieldValue = sourceFieldValue.Substring(0, sourceFieldDataFormat.Length);
                // If the second character in the source field value is not numeric and the first portion of the date format
                // is not MMM, then add a leading zero
                if (!IsNumeric(sourceFieldValue[1].ToString()) && !sourceFieldDataFormat.StartsWith("MMM"))
                {
                    sourceFieldValue = sourceFieldValue.Insert(0, "0");
                }
                // If the format is MMM-DD, but the day part has only one digit, then insert a leading zero into the day part
                if (sourceFieldDataFormat.StartsWith("MMM") && !IsNumeric(sourceFieldValue[3].ToString()) && !IsNumeric(sourceFieldValue[5].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(4, "0");
                }
                // If the format is MM-DD but the day part has only one digit, then insert a leading zero into the day part
                if (sourceFieldDataFormat.StartsWith("MM") && !sourceFieldDataFormat.StartsWith("MMM") && !IsNumeric(sourceFieldValue[2].ToString()) && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is DD-MM but the month part has only one digit, then insert a leading zero into the month part
                if (sourceFieldDataFormat.StartsWith("DD") && sourceFieldDataFormat.Contains("MM") && !sourceFieldDataFormat.Contains("MMM") && !IsNumeric(sourceFieldValue[2].ToString()) && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YYYY-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[6].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }
                // If the format is YYYY-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[6].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }
                // If the format is YYYY-DD-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[7].ToString())
                    && sourceFieldValue.Length < 10
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(8, "0");
                }
                // If the format is YYYY-MM-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[5].ToString())
                    && sourceFieldValue.Length < 10
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(8, "0");
                }
                // If the format is YY-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YY-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YY-DD-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[5].ToString())
                    && sourceFieldValue.Length < 8
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(6, "0");
                }
                // If the format is YY-MM-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[7].ToString())
                    && sourceFieldValue.Length < 8
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(6, "0");
                }
                // if the format is YYYY-WW  but the WW part only has 1 digit, then insert a leading zero into WW Part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("WW")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && sourceFieldValue.Length < 6)
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }

                // Correct the casing of characters in source field data format to be valid during formatting
                sourceFieldDataFormat = CleanDateFormat(sourceFieldDataFormat);

                // Correct the casing of characters in target data format to be valid during formatting
                targetFieldDateFormat = CleanDateFormat(targetFieldDateFormat);

                //Convert to date time
                DateTime result;

                if (sourceFieldDataFormat.Contains("WW"))
                {
                    // Get location of WW and yyyy
                    int startLocationOfWWInString = sourceFieldDataFormat.IndexOf("WW");
                    int startLocationOfYYYYInString = sourceFieldDataFormat.IndexOf("yyyy");

                    // If index of search keys are non-zero (not found in Date Format string), then throw exception
                    if (startLocationOfWWInString < 0 || startLocationOfYYYYInString < 0)
                    {
                        throw new System.Exception(
                            string.Format("Expected WW and YYYY in the date format provided ({0}).", sourceFieldDataFormat)
                            );
                    }

                    // Get value of WW and YYYY
                    string wwString = sourceFieldValue.Substring(startLocationOfWWInString, 2);
                    string yyyyString = sourceFieldValue.Substring(startLocationOfYYYYInString, 4);

                    // Get monday of given week number of the year
                    result = GetFirstDayOfWeek(
                        int.Parse(yyyyString) // Year
                        , int.Parse(wwString) // Week number
                        , DayOfWeek.Monday
                        );
                }
                else
                {

                    result = DateTime.ParseExact(sourceFieldValue, sourceFieldDataFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal);
                }

                // Get the date according to desired format

                returnDateTimeValue = result.ToString(targetFieldDateFormat);
                // return returnDateTimeValue;
            }
            catch (Exception ex)
            {
                log.Error(" ExtractDateElements in SCDataVerificationManager.targetFieldDateFormat:" + targetFieldDateFormat + ", sourceFieldDataFormat:" + sourceFieldDataFormat + ",sourceFieldValue:" + sourceFieldValue + ",isSuccess:" + isSuccess + ",errorMessage:" + errorMessage + ". Exception : Message- " + ex.Message);
                isSuccess = false;
                errorMessage = string.Format(
                    "Error encountered while extracting date elements from source message field value '{0}' with date format '{1}'.  If the original file is in MS Excel format, the date fields are probabaly not date formatted. {2}"
                    , sourceFieldValue
                    , sourceFieldDataFormat
                    , ex.Message
                    );
                //throw ex;
            }
            return returnDateTimeValue;
        }

        public static bool IsNumeric(string number)
        {
            int tempInt;
            return int.TryParse(number, out tempInt);
        }

        public static bool IsDecimal(string number)
        {
            decimal tempDec;
            return decimal.TryParse(number, out tempDec);
        }

        /// <summary>
        /// Updates the date format so it complies with .NETs expected character casing for the date portion codes
        /// </summary>
        /// <param name="dateFormat"></param>
        /// <returns></returns>
        public static string CleanDateFormat(string dateFormat)
        {
            return dateFormat.ToUpper()
                 .Replace("YYYY", "yyyy")
                 .Replace("YY", "yy")
                 .Replace("DD", "dd");
        }

        public static DateTime GetFirstDayOfWeek(int year, int week, DayOfWeek dayOfTheWeek)
        {
            // If the week day of the first day of the year is greater than the given day of the week,
            // then subtract by week by 2 to get to the week of the previous year
            if (new DateTime(year, 1, 1).DayOfWeek > dayOfTheWeek)
            {
                week = week - 2;
            }
            else
            {
                week = week - 1;
            }

            return GetWeek1Day1(year, dayOfTheWeek).AddDays(7 * week);
        }

        public static DateTime GetWeek1Day1(int year, DayOfWeek firstDayOfWeek)
        {
            DateTime date = new DateTime(year, 1, 1);

            // Move towards firstDayOfWeek
            date = date.AddDays(firstDayOfWeek - date.DayOfWeek);

            // Either 1 or 52 or 53
            int weekOfYear = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFullWeek, firstDayOfWeek);

            // Move forwards 1 week if week is 52 or 53
            date = date.AddDays(7 * System.Math.Sign(weekOfYear - 1));

            return date;
        }

        public static string GPLookup(int SubsiId, string LookupName, string CodeValue, bool PerformReverseLookup, bool ignoreCustomerCode, string mapperName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string strLookupDecodeValue = null;
                int lookUpId = (from s in context.Ref_LookupNames
                                where s.LookupName == LookupName
                                select s.Id).FirstOrDefault();

                if (!ignoreCustomerCode)
                {
                    if (!PerformReverseLookup)
                    {


                        var lookVal = (from s in context.Trn_LookupValues
                                       orderby SubsiId descending
                                       where ((s.SubsiId == SubsiId) && (s.LookupNameId == lookUpId) && (s.LookupCode == CodeValue) && (s.MapperName == mapperName))
                                       select s.LookupDecodeValue).FirstOrDefault();
                        if (lookVal != null)
                        {
                            strLookupDecodeValue = lookVal;
                        }
                        else
                        {
                            strLookupDecodeValue = CodeValue;
                        }
                    }
                    else
                    {
                        var lookVal = (from s in context.Trn_LookupValues
                                       where ((s.SubsiId == SubsiId) && (s.LookupNameId == lookUpId) && (s.LookupDecodeValue == CodeValue) && (s.MapperName == mapperName))
                                       select s.LookupCode).FirstOrDefault();
                        if (lookVal != null)
                        {
                            strLookupDecodeValue = lookVal;
                        }
                        else
                        {
                            strLookupDecodeValue = CodeValue;
                        }
                    }
                }
                else
                {
                    if (!PerformReverseLookup)
                    {
                        var lookVal = (from s in context.Trn_LookupValues
                                       orderby s.SubsiId descending
                                       where ((s.LookupNameId == lookUpId) && (s.LookupCode == CodeValue) && (s.MapperName == mapperName))
                                       select s.LookupDecodeValue).FirstOrDefault();
                        if (lookVal != null)
                        {
                            strLookupDecodeValue = lookVal;
                        }
                        else
                        {
                            strLookupDecodeValue = CodeValue;
                        }

                    }
                    else
                    {

                        var lookVal = (from s in context.Trn_LookupValues
                                       where ((s.LookupNameId == lookUpId) && (s.LookupDecodeValue == CodeValue) && (s.MapperName == mapperName))
                                       select s.LookupCode).FirstOrDefault();
                        if (lookVal != null)
                        {
                            strLookupDecodeValue = lookVal;
                        }
                        else
                        {
                            strLookupDecodeValue = CodeValue;
                        }

                    }
                }

                return strLookupDecodeValue;
            }
        }

        public static string GPLookUpForSC(int SubsiId, string CodeValue, string mapperName)
        {
            string strLookupDecodeValue = "";
            using (var context = new GoodpackEDIEntities())
            {

                strLookupDecodeValue = (from s in context.Trn_LookupValues
                                        where ((s.LookupCode == CodeValue) && (s.MapperName == mapperName))
                                        select s.LookupDecodeValue).FirstOrDefault();
                if (strLookupDecodeValue == null)
                {
                    return CodeValue;
                }
                else
                {
                    return strLookupDecodeValue;
                }
            }


        }


        public static EntityMapperModel VerticalLookup_By_CustomerCode_Value(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField, long customerCode, string customerName)
        {
            string Vertical = string.Empty;
            EntityMapperModel mapperModel = new EntityMapperModel();
            using (var context = new GoodpackEDIEntities())
            {
                if ((customerName != null) && (customerCode != 0))
                {
                    mapperModel = (from s in context.Gen_EDIEntities
                                   orderby s.EntityCode descending
                                   where ((s.EntityName == customerName) &&
                                (s.SubsiId == (from p in context.Gen_EDIEntities
                                               where p.EntityCode == customerCode
                                               select p.SubsiId).FirstOrDefault()))
                                   select new EntityMapperModel
                                   {
                                       ContractNumber = s.ContractNumber,
                                       EntityCode = s.EntityCode,
                                       Vertical = s.Vertical
                                   }).FirstOrDefault();
                }
                else if (customerName != null)
                {
                    mapperModel = (from s in context.Gen_EDIEntities
                                   orderby s.EntityCode descending
                                   where s.EntityName == customerName
                                   select new EntityMapperModel
                                   {
                                       ContractNumber = s.ContractNumber,
                                       EntityCode = s.EntityCode,
                                       Vertical = s.Vertical
                                   }).FirstOrDefault();
                }
                else if (customerCode != 0)
                {
                    mapperModel = (from s in context.Gen_EDIEntities
                                   orderby s.EntityCode descending
                                   where s.EntityCode == customerCode
                                   select new EntityMapperModel
                                   {
                                       ContractNumber = s.ContractNumber,
                                       EntityCode = s.EntityCode,
                                       Vertical = s.Vertical
                                   }).FirstOrDefault();
                }
            }
            return mapperModel;
        }

        public static EntitiesModel GetCustomer(long customerId)
        {
            //string SrchColumn = "CustomerCode";
            //long SrchValue = customerId;
            EntitiesModel entitiesModel = new EntitiesModel();
            using (var context = new GoodpackEDIEntities())
            {
                entitiesModel = (from a in context.Gen_EDISubsies
                                 join e in context.Gen_EDIEntities
                                     on a.Id equals e.SubsiId
                                 where e.EntityCode == customerId
                                 select new EntitiesModel
                                 {
                                     //  ContractEnd =e.ContractEndDate,
                                     ContractNumber = e.ContractNumber,
                                     //  ContractStart=e.ContractStartDate,
                                     CustomerCode = e.EntityCode,
                                     //CustomerHeadCompany=e.SubsiId,
                                     CustomerName = e.EntityName,
                                     CustomerStakeholderEmailList = e.EmailIdList,
                                     FileSourcePath = e.SourcePath,
                                     GoodpackSubsidiaryEmailList = e.GoodpackSubsidaryEmailIdList,
                                     IsEnabled = e.IsEnabled,
                                     Subsidiary = e.GoodpackSubsidary,
                                     Vertical = e.Vertical
                                 }
                            ).FirstOrDefault();
                return entitiesModel;
            }

        }

        public static VesselTransitTimeModel GetVesselTransitTime(string podValue, string polValue)
        {
            VesselTransitTimeModel vesselTransitTimeValue = new VesselTransitTimeModel();
            using (var context = new GoodpackEDIEntities())
            {
                vesselTransitTimeValue = (from a in context.Ref_VesselTransitTime
                                          where ((a.POD == podValue) && (a.POL == polValue))
                                          select new VesselTransitTimeModel
                                          {

                                              ShippingLine = a.ShippingLiner,
                                              TransitTime = a.TransitTime,
                                              VoyageScheduleLookupVarianceInDay = a.VoyageScheduleLookupVarianceInDay ?? 0
                                          }).FirstOrDefault();
                return vesselTransitTimeValue;
            }
        }

        public static DateTime GetVoyageSchedule(string shippingLiner, string destinationPort, string etaDestination, string etaDestinationDate)
        {
            //SELECT ETAatDestinationPort FROM MSTR_VOYAGE_SCHEDULING WHERE ShippingLiner='{0}' AND DestinationPort='{1}' AND ETAatDestinationPort between '{2}' AND '{3}'
            DateTime etaDest = Convert.ToDateTime(etaDestination).Date;
            DateTime etaDestDate = Convert.ToDateTime(etaDestinationDate);
            using (var context = new GoodpackEDIEntities())
            {
                DateTime ETAatDestinationPort = (from s in context.Ref_VoyageScheduling
                                                 where ((s.ShippingLiner == shippingLiner) && (s.DestinationPort == destinationPort) && (s.ETAatDestinationPort >= etaDestDate) && (s.ETAatDestinationPort <= etaDest))
                                                 select s.ETAatDestinationPort).FirstOrDefault();
                return ETAatDestinationPort;
            }

        }

        public static string GPLookup(int SubsiId, string LookupName, string CodeValue, string mapperName)
        {
            return GPLookup(SubsiId, LookupName, CodeValue, false, false, mapperName);
        }

        public static string GPLookup(int SubsiId, string LookupName, string CodeValue, bool performReverseLookup, string mapperName)
        {
            return GPLookup(SubsiId, LookupName, CodeValue, performReverseLookup, false, mapperName);
        }

        public static int GenerateSequenceNum(string sequenceName)
        {

            BigInteger maxVal = 9999999999;
            using (var context = new GoodpackEDIEntities())
            {
                Ref_Sequence seq = (from s in context.Ref_Sequence
                                    select s).FirstOrDefault();

                if (seq == null)
                {
                    Ref_Sequence sequenceData = new Ref_Sequence();
                    sequenceData.Seed = 1;
                    sequenceData.SeqName = sequenceName;
                    context.Ref_Sequence.Add(sequenceData);
                    context.SaveChanges();
                }
                else
                {
                    Ref_Sequence sequence = context.Ref_Sequence.FirstOrDefault(x => x.SeqName == sequenceName);

                    // set @NewSeqVal = Seed = case when Seed = @MaxVal Then 1 else Seed+1 end
                    if (sequence != null)
                    {
                        if (sequence.Seed == maxVal)
                        {
                            sequence.Seed = 1;
                        }
                        else
                        {
                            sequence.Seed = sequence.Seed + 1;
                        }
                        context.SaveChanges();
                    }
                }

                string strSeqNum = (from s in context.Ref_Sequence
                                    where s.SeqName == sequenceName
                                    select s.Seed).FirstOrDefault().ToString();
                int intSeq = 0;
                int.TryParse(strSeqNum, out intSeq);
                return intSeq;
            }

        }
    }
}