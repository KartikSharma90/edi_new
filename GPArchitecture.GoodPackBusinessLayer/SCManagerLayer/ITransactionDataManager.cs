﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace GPArchitecture.GoodPackBusinessLayer.SCManagerLayer
{
    public interface ITransactionDataManager
    {
        bool fileUpload(IList<MultipartFileData> filedata, string root);
        IList<GenericItemModel> SOChangeCancelFileData(int subsiId, bool isFileType, string path);
        bool fileUpload(IList<MultipartFileData> filedata, string root,out string path);
        IList<string> mappedFiles(int subsiId, int transactionType);
        IList<GenericItemModel> SCFileData(string mapperName, int subsiId, bool isFileType,string path);
        IList<GenericItemModel> VerifySOChangeCancelData(string username, int subsiId, IList<GenericItemModel> itemModel);
        IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData);
        IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData, bool isWeb);
        IList<GenericItemModel> verifyDataNew(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData, bool isWeb);        
        IList<ResponseModel> SubmitData(IList<GenericItemModel> itemModel, string username, TRANSACTION_TYPES transType);
        IList<ResponseLineDataModel> ReSubmit(string transactionType, string username, int batchId, List<int> lineIds, int status, string mapperName);
        IList<string> RetrieveErrorData(int lineId, TRANSACTION_TYPES transactionType);
        IList<string> RetrieveSOErrorData(int lineId);
        bool DeleteRecords(string transactionType, int BatchId, string DeleteComment,string selectIds);
        void FileInfo(string path, string fileName, string message, string emailId);
        IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId,string custPOdate, IList<GenericItemModel> itemModel, bool isFileData);
        IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, bool isFileData, bool isWeb);
        IList<GenericItemModel> SOFileData(string mapperName, int subsiId, bool isFileType,string path);
        IList<ResponseLineDataModel> sscLineContentParsing(Trn_SSCTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName);
        IList<ResponseLineDataModel> scLineContentParsing(Trn_SCTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName);
        ResponseLineDataModel updateSSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i);
        ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i);
        ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i, Trn_SCTransLine lineDetail);
       // List<ResponseModel> serviceCallingSSC(List<int> lineIds, int i, Trn_SSCTransLine lines, string val, ref int successCount, ref int errorCount, string username, int batchId, bool isSuccess, string MvtType);
        List<ResponseModel> serviceCallingSC(List<int> lineIds, int i, Trn_SCTransLine lines, string val, ref int successCount, ref int errorCount, string username, int batchId, bool isSuccess);
        string[] getAllRecipients(string mapperFile);
       // IList<GenericItemModel> verifySSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData);
        IList<ResponseModel> SCSubmit(IList<GenericItemModel> itemModel, string username);
        IList<string> RetrieveLineContentDetails(int lineId, TRANSACTION_TYPES transactiontype);
        string getResponseSO(int batchId);
        string ErrorLinesDownload(int batchId,bool isDownloadError);
        string GetFileType(int batchId);
        IList<GenericItemModel> verifyASNData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData);
        IList<ResponseLineDataModel> ReSubmit(string transactionType, string username, int batchId, int status, bool SubmitActionType);
        BatchStatusDetail GetBatchStatus(int BatchId, int TransactionType);
    }
}
    
 

