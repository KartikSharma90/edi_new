using GPArchitecture.DataLayer.Models;
 using GPArchitecture.EnumsAndConstants;
 using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.SCManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer;
 using GPArchitecture.Models;
 using GPArchitecture.Utilities;
 using log4net;
 using System;
 using System.Collections.Generic;
 using System.ComponentModel;
 using System.Data;
 using System.Linq;
 using System.Text;
 
 namespace GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer
 {
     public class SSCManagerNew : ISSCManagerNew
     {
         private static readonly ILog log = LogManager.GetLogger(typeof(SSCManagerNew));
         private SCTransactionModel objGPCustomerMessage;
         private bool isTermTrip { get; set; }
         //      private Dictionary<int, Dictionary<string, string>> sapSubmitTransData;
         private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
         private string _UserName { get; set; }
    
         public IList<GenericItemModel> submitSSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, int fileType)
         {
             IList<GenericItemModel> returnVlaue = null; ;
             try
             {
                 _UserName = username;
                 objGPCustomerMessage = new SCTransactionModel();
                 List<string> dataValue;
                 List<string> fileName;
                 List<string> filePath;
                 List<string> message;       
                 dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                 fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                 filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                 message = itemModel.Select(x => x.Message).Distinct().ToList();
                 objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                 objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                 objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                 string sourceType="";
                 if (fileType==1)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;   
                     sourceType="FILE_SYSTEM";
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                         where s.Username == username
                                                         select s.EmailId).FirstOrDefault();
                     }
                 }
                 else if (fileType==2)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                     sourceType="EMAIL";
                 }
                 else if (fileType==3)
                 { 
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE; 
                     sourceType="FTP_SERVICE";
                 }
                 else if (fileType == 4)
                 {    
                     sourceType="FILE_SYSTEM";
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                 }
                 using (var context = new GoodpackEDIEntities())
                 {
                     int userId = (from s in context.Gen_User
                                   where s.Username == username
                                   select s.Id).FirstOrDefault();
                     objGPCustomerMessage.UserId = userId;
                     objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                      join e in context.Ref_TransactionTypes
                                                                                                                      on s.TransactionCodeId equals e.Id
                                                                                                                      where s.Filename == mapperFile
                                                                                                                      select e.TransactionCode).FirstOrDefault());
                 }
                 objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                 objGPCustomerMessage.DateCreated = DateTime.Now;
                 objGPCustomerMessage.Service = "GPMapper";
                 objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                 GPMapper mapper = new GPMapper();
                 MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                 if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                          && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 {
                     List<int> listInt = InsertSSCDatatoShipmentDetailTable(objGPCustomerMessage.mappingServiceValues, mapperFile, Convert.ToInt32(subsiId));
                     List<SScTOScConvertionModel> listSSC = CreateIntermediateData(listInt, Convert.ToInt32(subsiId), mapperFile);
                     if (fileType == 2)
                     {
                         int batchId = InsertSSCTransBatchWithoutValidation(listSSC, sourceType);
                         itemModel[0].BatchId = batchId.ToString();
                         InsertSSCTransLineWithoutValidation(listSSC, batchId, listInt);
                     }
                     else
                     {
                         int batchId = InsertSSCTransBatch(listSSC, sourceType);
                         itemModel[0].BatchId = batchId.ToString();
                         InsertSSCTransLine(listSSC, batchId, listInt);
                     }
                     returnVlaue = GetResponseData(itemModel, listSSC);
                 }
                 return returnVlaue;
             }
             catch (Exception e)
             {
                 log.Error("submitSSCData :-" + e);
                 IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                 GenericItemModel itemsModel = new GenericItemModel();
                 objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                 objGPCustomerMessage.ErrorDesc = e.Message;
                 itemsModel.Status = "false";
                 itemsModel.Message = "Unable to process your request.";
                 // Update TRANS_BATCH
                 genericModel.Add(itemsModel);
                 UpdateSSCBatchFileData(objGPCustomerMessage);
                 Dictionary<string, string> placeholder = new Dictionary<string, string>();
                 // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                 placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                 return genericModel;
             }
         }
 
         public IList<GenericItemModel> submitSSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData)
         {
             IList<GenericItemModel> returnVlaue = null; ;
             try
             {
                 _UserName = username;
                 objGPCustomerMessage = new SCTransactionModel();
                 List<string> dataValue;
                 List<string> fileName;
                 List<string> filePath;
                 List<string> message;
                 var defaultVal = "";
                 dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                 fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                 filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                 message = itemModel.Select(x => x.Message).Distinct().ToList();
                 objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                 objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                 objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                 if (isFileData)
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     //itemModel.RemoveAt(1);
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                         where s.Username == username
                                                         select s.EmailId).FirstOrDefault();
                     }
                 }
                 else
                 {
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                 }
                 using (var context = new GoodpackEDIEntities())
                 {
                     int userId = (from s in context.Gen_User
                                   where s.Username == username
                                   select s.Id).FirstOrDefault();
                     objGPCustomerMessage.UserId = userId;
                     objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                      join e in context.Ref_TransactionTypes
                                                                                                                      on s.TransactionCodeId equals e.Id
                                                                                                                      where s.Filename == mapperFile
                                                                                                                      select e.TransactionCode).FirstOrDefault());
                     var transactionType = objGPCustomerMessage.TransactionType.ToString();
                     var mappingSubsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                                           join m in context.Trn_MappingSubsi
                                           on s.Id equals m.SubsiToTransactionFileMapperId
                                           where s.Filename == mapperFile
                                           select m.Id).FirstOrDefault();
                     var sapFieldseq = context.Ref_SAPMessageFields.Where(i => i.SAPFieldName == "Material Number" && i.Ref_TransactionTypes.TransactionCode == transactionType).Select(i => i.FieldSequence).FirstOrDefault();
                     defaultVal = (from s in context.Trn_MappingConfiguration
                                   where s.MappingSubsiId == mappingSubsiId
                                   && s.SapFieldSeq == sapFieldseq
                                   select s.FixedValue).FirstOrDefault();
                 }
                 objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                 objGPCustomerMessage.DateCreated = DateTime.Now;
                 objGPCustomerMessage.Service = "GPMapper";
                 objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                 GPMapper mapper = new GPMapper();
                 MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                 objGPCustomerMessage.mappingServiceValues.ForEach(i =>
                 {
                     if (string.IsNullOrEmpty(i[SCSapMessageFields.MaterialNumber]))
                     {
                         i[SCSapMessageFields.MaterialNumber] = string.IsNullOrEmpty(defaultVal) ? "" : defaultVal;
                     }
                 });
                 if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                          && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                 {
                     List<int> listInt = InsertSSCDatatoShipmentDetailTable(objGPCustomerMessage.mappingServiceValues, mapperFile, Convert.ToInt32(subsiId));
                     List<SScTOScConvertionModel> listSSC = CreateIntermediateData(listInt, Convert.ToInt32(subsiId), mapperFile);
                     int batchId = InsertSSCTransBatch(listSSC, objGPCustomerMessage.BatchFileSourceType.ToString(), objGPCustomerMessage.BatchFileName);
                     itemModel[0].BatchId = batchId.ToString();
                     InsertSSCTransLine(listSSC, batchId, listInt);
                     returnVlaue = GetResponseData(itemModel, listSSC);
                 }
                 return returnVlaue;
             }
             catch (Exception e)
             {
                 log.Error("submitSSCData :-" + e);
                 IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                 GenericItemModel itemsModel = new GenericItemModel();
                 objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                 objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                 objGPCustomerMessage.ErrorDesc = e.Message;
                 itemsModel.Status = "false";
                 itemsModel.Message = "Unable to process your request.";
                 // Update TRANS_BATCH
                 genericModel.Add(itemsModel);
                 UpdateSSCBatchFileData(objGPCustomerMessage);
                 Dictionary<string, string> placeholder = new Dictionary<string, string>();
                 // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                 placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                 GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                 return genericModel;
             }
         }
         #region Private
             private IList<GenericItemModel> GetResponseData(IList<GenericItemModel> itemModel, List<SScTOScConvertionModel> data)
             {
                 GenericItemModel itemModelVal = itemModel[1];
                 IList<GenericItemModel> genericModel = null;
                 genericModel = SScTestResult(itemModel, data);
 
                 //if (genericModel.Count > 0)
                 //{
                 //    genericModel[0].sapResponseData = sapSubmitTransData;
                 //}
                 //for (int i = 0; i < genericModel.Count; i++)
                 //{
                 //    itemModelData.Add(genericModel[i]);
                 //    if (i == 0)
                 //    {
                 //        itemModelData.Add(itemModelVal);
                 //    }
                 //}
                 return genericModel;
             }
             private IList<GenericItemModel> SScTestResult(IList<GenericItemModel> itemModel, List<SScTOScConvertionModel> sscData)
             {
                 using (var context = new GoodpackEDIEntities())
                 {
                     IList<GenericItemModel> returnModel = new List<GenericItemModel>();
                     for (int i = 0; i < sscData.Count; i++)
                     {
                         int lineId = Convert.ToInt32(sscData[i].TransLineId);
                         Trn_SSCTransLineNew transLine = context.Trn_SSCTransLineNew.Where(x => x.Id == lineId).FirstOrDefault();
                         int statusId = transLine.StatusId;
                         string statusCode = (from s in context.Trn_LineStatus
                                              where s.Id == statusId
                                              select s.StatusCode).FirstOrDefault();
                         var errorMessage = String.Join(",", context.Trn_SSCTransLineContentNew.Where(x => x.LineId == lineId).Select(x => x.SapResponseMessage).ToArray());
                         int m = 0;
                         foreach (var data in itemModel)
                         {
                             if (Array.Exists(itemModel[m].Name.Split('|'), x => x == sscData[i].SInumber))
                             {
                                 itemModel[m].LineId = lineId;
                                 itemModel[m].Status = statusCode;
                                 itemModel[m].ErrorMessage = errorMessage;
                             }
                             m++;
                         }
                         //itemModel[i + 1].LineId = lineId;
                         //itemModel[i + 1].Status = statusCode;
                         //var test = Array.Exists(itemModel[i + 1].Name.Split('|'), x => x == "APLU 0602269/1");
                         //var test1 = Array.FindIndex(itemModel[i + 1].Name.Split('|'), x => x == "APLU 0602269/1");
                         //var test2 = Array.FindIndex(itemModel[i + 1].Name.Split('|'), x => x == transLine.SINumber);
                     }
                     return itemModel;
                 }
             }
             private List<int> InsertSSCDatatoShipmentDetailTable(List<Dictionary<string, string>> data, string mapper, int subsy)
             {
                 List<int> listInt = new List<int>();
                 try
                 {
                     foreach (var value in data)
                     {
                         using (var context = new GoodpackEDIEntities())
                         {
                             Trn_ScannedShipmentDetails scannedDetails = new Trn_ScannedShipmentDetails();
                             scannedDetails.Barcode = value[SCSapMessageFields.TwoDPin];
                             scannedDetails.BinType = value[SCSapMessageFields.MaterialNumber];
                             scannedDetails.ConsigneeId = value[SCSapMessageFields.ToLocation];
                             scannedDetails.CreatedDate = DateTime.Now;
                             scannedDetails.CustomerReferenceNumber = value[SCSapMessageFields.CustomerReferenceNumber];
                             scannedDetails.DeviceId = value[SCSapMessageFields.DeviceId];
                             scannedDetails.ETA = value[SCSapMessageFields.DTADate];
                             scannedDetails.ETD = value[SCSapMessageFields.ETDDate];
                             scannedDetails.FileSource = SOURCE_TYPES.FILE_SYSTEM.ToString();
                             scannedDetails.SINumber = value[SCSapMessageFields.CustomerReferenceNumber];
                             scannedDetails.CustomerCode = GetDecodeValue(value[SCSapMessageFields.CustomerNumber], "Customer Number", mapper, subsy);
                             scannedDetails.PackerCode = GetDecodeValue(value[SCSapMessageFields.FromLocation], "From Location", mapper, subsy);
                             scannedDetails.Subsy = subsy.ToString();
 
                             if (value.Keys.Contains(SCSapMessageFields.DRYAGE_SHIPMENT))
                                 scannedDetails.DRYAGE_SHIPMENT = value[SCSapMessageFields.DRYAGE_SHIPMENT];
                             if (value.Keys.Contains(SCSapMessageFields.PLANT))
                                 scannedDetails.PLANT = value[SCSapMessageFields.PLANT];
                             if (value.Keys.Contains(SCSapMessageFields.SKU))
                                 scannedDetails.SKU = value[SCSapMessageFields.SKU];
                             if (value.Keys.Contains(SCSapMessageFields.SKU))
                                 scannedDetails.SKU_TEXT = value[SCSapMessageFields.SKU];
                             if (value.Keys.Contains(SCSapMessageFields.SHIP_TO))
                                 scannedDetails.SHIP_TO = value[SCSapMessageFields.SHIP_TO];
                             if (value.Keys.Contains(SCSapMessageFields.ST_NAME))
                                 scannedDetails.ST_NAME = value[SCSapMessageFields.ST_NAME];
                             if (value.Keys.Contains(SCSapMessageFields.BATCH))
                                 scannedDetails.BATCH = value[SCSapMessageFields.BATCH];
                             if (value.Keys.Contains(SCSapMessageFields.ADDFIELD1))
                                 scannedDetails.ADDFIELD1 = value[SCSapMessageFields.ADDFIELD1];
                             if (value.Keys.Contains(SCSapMessageFields.ADDFIELD2))
                                 scannedDetails.ADDFIELD2 = value[SCSapMessageFields.ADDFIELD2];
                             if (value.Keys.Contains(SCSapMessageFields.ADDFIELD3))
                                 scannedDetails.ADDFIELD3 = value[SCSapMessageFields.ADDFIELD3];
                             if (value.Keys.Contains(SCSapMessageFields.ADDFIELD4))
                                 scannedDetails.ADDFIELD4 = value[SCSapMessageFields.ADDFIELD4];
                             if (value.Keys.Contains(SCSapMessageFields.ADDFIELD5))
                                 scannedDetails.ADDFIELD5 = value[SCSapMessageFields.ADDFIELD5];
 
                             context.Trn_ScannedShipmentDetails.Add(scannedDetails);
                             context.SaveChanges();
                             listInt.Add(scannedDetails.Id);
                         }
                     }
                 }
                 catch (Exception e)
                 {
                     log.Error("InsertSSCDatatoShipmentDetailTable " + e);
                     throw e;
                 }
                 return listInt;
             }
             private List<SScTOScConvertionModel> CreateIntermediateData(List<int> shipmentDetailsIds, int subsy, string mapper)
             {
                 List<SScTOScConvertionModel> ssctoScConvertionModel = new List<SScTOScConvertionModel>();
                 try
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
                         List<DataConvertionModel> data = (from queryResult in context.Trn_ScannedShipmentDetails
                                                           where shipmentDetailsIds.Contains(queryResult.Id)
                                                           group queryResult by queryResult.SINumber into rowGroup
                                                           select new DataConvertionModel
                                                           {
                                                               SINumber = rowGroup.Key,
                                                               Count = rowGroup.Count()
                                                           }).ToList();
 
                         foreach (var t in data)
                         {
                             var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                             where shipmentDetailsIds.Contains(s.Id)
                                             select s.DeviceId).FirstOrDefault();
                             if (deviceId == null)
                             {
                                 throw new FormatException("Device Id is not Valid");
                             }
                             //var packingPlantCustomerCode = GetPackerCodeFromDeviceId(deviceId);
                             //string packingPlantString = packingPlantCustomerCode.PackerCode.ToString();
                             //string customerCode = packingPlantCustomerCode.CustomerCode.ToString();
                             //var subsyVal = int.Parse(packingPlantCustomerCode.Subsy.ToString().Trim());
                             //int susyId = context.Gen_EDISubsies.Where(i => i.SubsiCode == subsyVal).Select(i => i.Id).SingleOrDefault();
                             SScTOScConvertionModel ssctoscModel = (from s in context.Trn_ScannedShipmentDetails
                                                                    where shipmentDetailsIds.Contains(s.Id) && s.SINumber == t.SINumber
                                                                    select new SScTOScConvertionModel
                                                                    {
                                                                        BinType = s.BinType,
                                                                        Consignee = s.ConsigneeId,
                                                                        CustomerName = s.CustomerCode,
                                                                        ETA = s.ETA,
                                                                        ETD = s.ETD,
                                                                        DeviceId = deviceId,
                                                                        PackingPlant = s.PackerCode,
                                                                        Quantity = t.Count,
                                                                        Remark = "",
                                                                        MapperName = mapper,
                                                                        LineId = s.Id,
                                                                        SubsiId = subsy,
                                                                        SInumber = s.SINumber,
                                                                        DRYAGE_SHIPMENT=s.DRYAGE_SHIPMENT,
                                                                        PLANT=s.PLANT,
                                                                        SKU = s.SKU,
                                                                        SKU_TEXT = s.SKU_TEXT,
                                                                        SHIP_TO = s.SHIP_TO,
                                                                        ST_NAME = s.ST_NAME,
                                                                        BATCH = s.BATCH,
                                                                        ADDFIELD1 = s.ADDFIELD1,
                                                                        ADDFIELD2 = s.ADDFIELD2,
                                                                        ADDFIELD3 = s.ADDFIELD3,
                                                                        ADDFIELD4 = s.ADDFIELD4,
                                                                        ADDFIELD5 = s.ADDFIELD5
                                                                    }).FirstOrDefault();
                             ssctoScConvertionModel.Add(ssctoscModel);
                         }
                         DataTable dataTable = ConvertToDatatable(ssctoScConvertionModel);
                         var batchData = WriteDataTable(dataTable);
                         ssctoScConvertionModel.ForEach(a => a.Data = batchData);
                     }
                 }
                 catch (Exception e)
                 {
                     log.Error("CreateIntermediateData " + e);
                     throw e;
                 }
                 return ssctoScConvertionModel;
             }
             private string GetDecodeValue(string data, string header, string mapperName, int subsiId)
             {
                 string decodeValue = null;
                 int lookupNameId = 0;
                 int transactionId;
                 int asnTransactionId=0;
                 string inputType = String.Empty;
                 IDecodeValueManager decodeValueManager = new DecodeValueManager();
                 using (var context = new GoodpackEDIEntities())
                 {
                     transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                      where s.Filename == mapperName
                                      select s.TransactionCodeId).FirstOrDefault();
 
                     inputType = (from s in context.Ref_SAPMessageFields
                                  where ((s.SAPFieldName == header) && (s.TransactionId == transactionId))
                                  select s.InputType).FirstOrDefault();
                     asnTransactionId=context.Ref_TransactionTypes.Where(i=>i.TransactionCode=="ASN").Select(i=>i.Id).FirstOrDefault();
                 }
                 switch (header.ToUpper())
                 {
                     case "CUSTOMER CODE":
                     case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                         break;
                     case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                         break;
                     case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                         break;
                     case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                         break;
                     case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                         break;
                     case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                         break;
                     case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                         break;
                     case "FRM LOCATION":
                     case "FROM LOCATION": if (data.StartsWith("3") && data.Length == 6)
                         {
                             lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                         }
                         else
                         {
                             if (transactionId == asnTransactionId)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                             }
                             else
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Packer);
                             }
                         }
                         break;
                     case "TO LOCATION": if (data.StartsWith("5") && data.Length == 6)
                         {
                             lookupNameId = GetLookupId(ConstantUtilities.Packer);
                         }
                         else
                         {
                             lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                         }
                         break;
                     case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                         break;
                 }
                 TargetMessageInputTypes inputSAPType = (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);
                 decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data, mapperName, subsiId, lookupNameId);
                 if (String.IsNullOrEmpty(decodeValue))
                     decodeValue = data;
                 return decodeValue;
             }
             private int GetLookupId(string LookupName)
             {
 
                 using (var context = new GoodpackEDIEntities())
                 {
                     return (from s in context.Ref_LookupNames
                             where s.LookupName == LookupName
                             select s.Id).FirstOrDefault();
 
                 }
 
             }
             private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
             {
                 IValidationTypeManger validator = new ValidationTypeManager();
                 Dictionary<string, string> validatorParams = new Dictionary<string, string>();
                 validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
                 validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
                 validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
                 validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
                 validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
                 switch (inputType)
                 {
                     case GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator:
                         return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);
 
                     default:
                         return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
                 }
 
 
             }
             private DeviceDetail GetPackerCodeFromDeviceId(string deviceId)
             {
                 using (var context = new GoodpackEDIEntities())
                 {
                     var packerCustomerCode = (from s in context.Trn_DeviceDetails
                                               where s.DeviceID == deviceId
                                               select new DeviceDetail { PackerCode = s.PackerCode, MapperName = s.MapperName, CustomerCode = s.CustomerCode, Subsy = s.SubsiId }).SingleOrDefault();
                     return packerCustomerCode;
                 }
 
             }
             private int InsertSSCTransBatch(List<SScTOScConvertionModel> listSSC, string sourceType)
             {
                 int batchId = 0;
                 int totalCount = listSSC.Count;
                 int errorcount = listSSC.Where(r => r.ErrorMessage != null).Count();
                 using (var context = new GoodpackEDIEntities())
                 {
                     foreach (var sscData in listSSC)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         try
                         {
                             if (sscData.CustomerName.StartsWith("3"))
                             {
                                 isTermTrip = termTrip.IsTirmTripORNot(sscData.CustomerName);
                             }
                         }
                         catch (Exception e)
                         {
                             isTermTrip = false;
                             log.Error("Term trip service error -" + e);
                         }                       
                         var userid = (from s in context.Gen_User
                                       where s.Username == _UserName
                                       select s.Id).SingleOrDefault();
                         Trn_SSCTransBatch scFileData = new Trn_SSCTransBatch();
                         scFileData.UserId = Convert.ToInt32(userid);
                         scFileData.SubsyId = sscData.SubsiId;
                         scFileData.TransactionId = 3;
                         scFileData.MapperFileName = sscData.MapperName;
                         scFileData.StatusId = 17;
                         scFileData.ErrorId = 28;
                         scFileData.IsTermTrip = isTermTrip;
                         //scFileData.FileName = ;
                         scFileData.FileSource = sourceType;
                         //scFileData.FileSourceAddress =;
                         scFileData.BatchContent = sscData.Data;
                         scFileData.ErrorDescription = "";
                         scFileData.DateCreated = DateTime.Now;
                         scFileData.DateUpdated = null;
                         scFileData.RecordCountTotal = totalCount;
                         scFileData.RecordCountSkipValFailure = errorcount;
                         scFileData.RecordCountFieldValFailure = 0;
                         scFileData.RecordCountSAPError = 0;
                         scFileData.RecordCountSuccess = 0;
                         scFileData.RecordCountSAPInProgress = 0;
                         scFileData.DateExportedToTargetSystem = null;
                         //scFileData.IsTestMode = true;
                         scFileData.EmailId = null;
                         context.Trn_SSCTransBatch.Add(scFileData);
                         context.SaveChanges();
                         batchId = scFileData.Id;
                         break;
                     }
                 }
                 return batchId;
             }
             private int InsertSSCTransBatch(List<SScTOScConvertionModel> listSSC, string transactionType, string fileName)
             {
                 int batchId = 0;
                 int totalCount = listSSC.Count;
                 int errorcount = listSSC.Where(r => r.ErrorMessage != null).Count();
                 using (var context = new GoodpackEDIEntities())
                 {
                     foreach (var sscData in listSSC)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         isTermTrip = termTrip.IsTirmTripORNot(sscData.CustomerName);
                         var userid = (from s in context.Gen_User
                                       where s.Username == _UserName
                                       select s.Id).SingleOrDefault();
                         Trn_SSCTransBatch scFileData = new Trn_SSCTransBatch();
                         scFileData.UserId = Convert.ToInt32(userid);
                         scFileData.SubsyId = sscData.SubsiId;
                         scFileData.TransactionId = 3;
                         scFileData.MapperFileName = sscData.MapperName;
                         scFileData.StatusId = 17;
                         scFileData.ErrorId = 28;
                         scFileData.IsTermTrip = isTermTrip;
                         scFileData.FileName = fileName;
                         scFileData.FileSource = transactionType;
                         //scFileData.FileSourceAddress =;
                         scFileData.BatchContent = sscData.Data;
                         scFileData.ErrorDescription = "";
                         scFileData.DateCreated = DateTime.Now;
                         scFileData.DateUpdated = null;
                         scFileData.RecordCountTotal = totalCount;
                         scFileData.RecordCountSkipValFailure = errorcount;
                         scFileData.RecordCountFieldValFailure = 0;
                         scFileData.RecordCountSAPError = 0;
                         scFileData.RecordCountSuccess = 0;
                         scFileData.RecordCountSAPInProgress = 0;
                         scFileData.DateExportedToTargetSystem = null;
                         //scFileData.IsTestMode = true;
                         scFileData.EmailId = null;
                         context.Trn_SSCTransBatch.Add(scFileData);
                         context.SaveChanges();
                         batchId = scFileData.Id;
                         break;
                     }
                 }
                 return batchId;
             }
             private int InsertSSCTransBatchWithoutValidation(List<SScTOScConvertionModel> listSSC, string sourceType)
             {
                 int batchId = 0;
                 int totalCount = listSSC.Count;
                 int errorcount = listSSC.Where(r => r.ErrorMessage != null).Count();
                 using (var context = new GoodpackEDIEntities())
                 {
                     foreach (var sscData in listSSC)
                     {
 
                         ITirmTripManager termTrip = new TirmTripManager();
                         try
                         {
                             if (sscData.CustomerName.StartsWith("3"))
                             {
                                 isTermTrip = termTrip.IsTirmTripORNot(sscData.CustomerName);
                             }
                         }
                         catch (Exception e)
                         {
                             isTermTrip = false;
                             log.Error("Term trip service error -" + e);
                         }
                         var userid = (from s in context.Gen_User
                                       where s.Username == _UserName
                                       select s.Id).SingleOrDefault();
                         Trn_SSCTransBatch scFileData = new Trn_SSCTransBatch();
                         scFileData.UserId = Convert.ToInt32(userid);
                         scFileData.SubsyId = sscData.SubsiId;
                         scFileData.TransactionId = 3;
                         scFileData.MapperFileName = sscData.MapperName;
                         scFileData.StatusId = 17;
                         scFileData.ErrorId = 28;
                         scFileData.IsTermTrip = isTermTrip;
                         //scFileData.FileName = ;
                         scFileData.FileSource = sourceType;
                         //scFileData.FileSourceAddress =;
                         scFileData.BatchContent = sscData.Data;
                         scFileData.ErrorDescription = "";
                         scFileData.DateCreated = DateTime.Now;
                         scFileData.DateUpdated = null;
                         scFileData.RecordCountTotal = totalCount;
                         scFileData.RecordCountSkipValFailure = errorcount;
                         scFileData.RecordCountFieldValFailure = 0;
                         scFileData.RecordCountSAPError = 0;
                         scFileData.RecordCountSuccess = 0;
                         scFileData.RecordCountSAPInProgress = 0;
                         scFileData.DateExportedToTargetSystem = null;
                         //scFileData.IsTestMode = true;
                         scFileData.EmailId = null;
                         context.Trn_SSCTransBatch.Add(scFileData);
                         context.SaveChanges();
                         batchId = scFileData.Id;
                         break;
                     }
                 }
                 return batchId;
             }
 
             private void InsertSSCTransLine(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids)
             {
                 try
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
                         foreach (var sscData in listSSC)
                         {
                             ITirmTripManager termtrip = new TirmTripManager();
                             bool termTrip = termtrip.IsTirmTripORNot(sscData.CustomerName);
                             Trn_SSCTransLineNew sscTrsansLine = new Trn_SSCTransLineNew();
                             sscTrsansLine.BatchId = batchId;
                             sscTrsansLine.BinType = sscData.BinType;
                             sscTrsansLine.CustomerCode = sscData.CustomerName;
                             sscTrsansLine.CustomerRefNumber = sscData.SInumber;
                             sscTrsansLine.ETA = sscData.ETA;
                             sscTrsansLine.ETD = sscData.ETD;
                             sscTrsansLine.FromLocation = sscData.PackingPlant;
                             sscTrsansLine.InTestMode = true;
                             sscTrsansLine.IsActive = true;
                             sscTrsansLine.ItemNumber = "10";
                             sscTrsansLine.ItrNumber = "10";
                             sscTrsansLine.ItrType = "SHCN";
                             sscTrsansLine.LastUpdated = DateTime.Now;
                             sscTrsansLine.LineContent = "";
                             sscTrsansLine.LineNumber = sscData.LineId;
                             sscTrsansLine.StatusId = 3;
                             sscTrsansLine.TermTripSAP = termTrip;
                             sscTrsansLine.Quantity = sscData.Quantity;
                             sscTrsansLine.SubsiId = sscData.ErrorMessage == null ? 6 : 1;
                             sscTrsansLine.ToLocation = sscData.Consignee;
                             context.Trn_SSCTransLineNew.Add(sscTrsansLine);
                             context.SaveChanges();
                             sscData.TransLineId = sscTrsansLine.Id;
                         }
                     }
                     SubmitToSAPandUpdateSSCTransBatch(listSSC, batchId, Ids, false);
                 }
                 catch (Exception e)
                 {
                     log.Error("InsertSSCTransLine :-" + e);
                 }
             }
 
             private void InsertSSCTransLineWithoutValidation(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids)
             {
                 try
                 {
                     int transactionId = 0;
                     int asnTransactionId = 0;
                     using (var context = new GoodpackEDIEntities())
                     {
                         foreach (var sscData in listSSC)
                         {
                             ITirmTripManager termtrip = new TirmTripManager();
                             bool termTrip = false;
                             if (sscData.CustomerName.StartsWith("3"))
                             {
                                 termtrip.IsTirmTripORNot(sscData.CustomerName);
                             }
                             Trn_SSCTransLineNew sscTrsansLine = new Trn_SSCTransLineNew();
                             sscTrsansLine.BatchId = batchId;
                             sscTrsansLine.BinType = sscData.BinType;
                             sscTrsansLine.CustomerCode = sscData.CustomerName;
                             sscTrsansLine.CustomerRefNumber = sscData.SInumber;
                             sscTrsansLine.ETA = sscData.ETA;
                             sscTrsansLine.ETD = sscData.ETD;
                             sscTrsansLine.FromLocation = sscData.PackingPlant;
                             sscTrsansLine.InTestMode = true;
                             sscTrsansLine.IsActive = true;
                             sscTrsansLine.ItemNumber = "10";
                             sscTrsansLine.ItrNumber = "10";
                             sscTrsansLine.ItrType = "SHCN";
                             sscTrsansLine.LastUpdated = DateTime.Now;
                             sscTrsansLine.LineContent = "";
                             sscTrsansLine.LineNumber = sscData.LineId;
                             sscTrsansLine.StatusId = 3;
                             sscTrsansLine.TermTripSAP = termTrip;
                             sscTrsansLine.Quantity = sscData.Quantity;
                             sscTrsansLine.SubsiId = sscData.ErrorMessage == null ? 6 : 1;
                             sscTrsansLine.ToLocation = sscData.Consignee;
                             context.Trn_SSCTransLineNew.Add(sscTrsansLine);
                             context.SaveChanges();
                             sscData.TransLineId = sscTrsansLine.Id;
                         }
                         var mapperName=listSSC[0].MapperName;
                         transactionId =(from s in  context.Trn_SubsiToTransactionFileMapper
                                         where s.Filename == mapperName
                                        select s.TransactionCodeId).FirstOrDefault();								 
                         asnTransactionId=context.Ref_TransactionTypes.Where(i=>i.TransactionCode=="ASN").Select(i=>i.Id).FirstOrDefault();                        
                     }
                     listSSC.ForEach(i =>
                     {                        
                         if (!i.CustomerName.StartsWith("3") || !(i.CustomerName.Length == 6))
                         {
                             i.ErrorMessage += "Decode value missing for Customer,";
                         };
                         if (transactionId == asnTransactionId)
                         {
                             if (!i.PackingPlant.StartsWith("6") || !(i.PackingPlant.Length == 6))
                             {
                                 i.ErrorMessage += "Decode value missing for Packer,";
                             }
                         }
                         else
                         {
                             if (!i.PackingPlant.StartsWith("5") || !(i.PackingPlant.Length == 6))
                             {
                                 i.ErrorMessage += "Decode value missing for Packer,";
                             }
                         };
                         if (!i.Consignee.StartsWith("6") || !(i.Consignee.Length == 6))
                         {
                             i.ErrorMessage += "Decode value missing for Consignee,";
                         }
                     });
 
                     SubmitToSAPandUpdateSSCTransBatchWithoutValidation(listSSC, batchId, Ids, false);
                 }
                 catch (Exception e)
                 {
                     log.Error("InsertSSCTransLine :-" + e);
                     throw e;
                 }
             }
             private void SubmitToSAPandUpdateSSCTransBatch(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids, bool isUpdate)
             {
                 try
                 {
                     int successCount = 0;
                     int errorCount = 0;
                     int totalFailedCount = 0;
                     List<ResponseModel> listResponse = null;
                     SCServiceManager serviceCaller = new SCServiceManager();
                     List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
                     List<SScTOScConvertionModel> failureList = listSSC.Where(i => i.ErrorMessage != null).ToList();
                     foreach (var errorData in failureList)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         isTermTrip = termTrip.IsTirmTripORNot(errorData.CustomerName);
                         List<ResponseModel> responseModel = new List<ResponseModel>();
                         ResponseModel response = new ResponseModel();
                         response.Success = false;
                         response.Message = errorData.ErrorMessage;
                         responseModel.Add(response);
                         if (isUpdate)
                         {
                             UpdateSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                         }
                         else
                         {
                             InsertSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                         }
                         using (var context = new GoodpackEDIEntities())
                         {
                             Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                             sscTransbatch.RecordCountSAPError = failureList.Count();
                             //sscTransbatch.RecordCountSuccess = successCount;
                             context.SaveChanges();
                         }
                         UpdateSSCTransLineStatus(errorData.LineId, responseModel);
                     }
                     foreach (var data in successList)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         isTermTrip = termTrip.IsTirmTripORNot(data.CustomerName);
                         var username = "";
                         int? userId = 0;
                         using (var context = new GoodpackEDIEntities())
                         {
                             userId = (from s in context.Trn_DeviceDetails
                                       where s.DeviceID == data.DeviceId
                                       select s.UserId).FirstOrDefault();
                             username = (from s in context.Gen_User
                                         where s.Id == userId
                                         select s.Username).FirstOrDefault();
                         }
                         try
                         {
                             listResponse = serviceCaller.scServiceCaller(CreateSCSAPDictionary(data), null, ref successCount, ref errorCount, username);
                         }
                         catch (Exception ex)
                         {
                             listResponse = new List<ResponseModel>();
                             log.Error("Sc SAP service error ;" + ex);
                             ResponseModel responseModel = new ResponseModel();
                             responseModel.Success = false;
                             responseModel.Message = "Error contacting SAP";
                             listResponse.Add(responseModel);
                         }
                         if (isUpdate)
                         {
                             totalFailedCount = UpdateSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                         }
                         else
                         {
                             totalFailedCount = InsertSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                             UpdateSSCTransLineStatus(data.LineId, listResponse);
                         }
                     }
                     totalFailedCount = totalFailedCount + failureList.Count();
                     using (var context = new GoodpackEDIEntities())
                     {
                         Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                         sscTransbatch.RecordCountSAPError = totalFailedCount;
                         sscTransbatch.RecordCountSuccess = sscTransbatch.RecordCountTotal - totalFailedCount;
                         context.SaveChanges();
                     }
                 }
                 catch (Exception e)
                 {
                     log.Error("SubmitToSAPandUpdateSSCTransBatch :-" + e);
                     throw e;
                 }
             }
             private void SubmitToSAPandUpdateSSCTransBatchWithoutValidation(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids, bool isUpdate)
             {
                 try
                 {
                     int successCount = 0;
                     int errorCount = 0;
                     int totalFailedCount = 0;
                     List<ResponseModel> listResponse = null;
                     SCServiceManager serviceCaller = new SCServiceManager();
                     List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
                     List<SScTOScConvertionModel> failureList = listSSC.Where(i => i.ErrorMessage != null).ToList();
                     foreach (var errorData in failureList)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         if (errorData.CustomerName.StartsWith("3"))
                         {
                             isTermTrip = termTrip.IsTirmTripORNot(errorData.CustomerName);
                         }
                         else
                         {
                             isTermTrip = false;
                         }
                         List<ResponseModel> responseModel = new List<ResponseModel>();
                         ResponseModel response = new ResponseModel();
                         response.Success = false;
                         response.Message = errorData.ErrorMessage;
                         responseModel.Add(response);
                         if (isUpdate)
                         {
                             UpdateSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                         }
                         else
                         {
                             InsertSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                         }
                         using (var context = new GoodpackEDIEntities())
                         {
                             Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                             sscTransbatch.RecordCountSAPError = failureList.Count();
                             //sscTransbatch.RecordCountSuccess = successCount;
                             context.SaveChanges();
                         }
                         UpdateSSCTransLineStatus(errorData.LineId, responseModel);
                     }
                     foreach (var data in successList)
                     {
                         ITirmTripManager termTrip = new TirmTripManager();
                         if (data.CustomerName.StartsWith("3"))
                         {
                             isTermTrip = termTrip.IsTirmTripORNot(data.CustomerName);
                         }
                         else
                         {
                             isTermTrip = false;
                         }
                         var username = "";
                         int? userId = 0;
                         using (var context = new GoodpackEDIEntities())
                         {
                             userId = (from s in context.Trn_DeviceDetails
                                       where s.DeviceID == data.DeviceId
                                       select s.UserId).FirstOrDefault();
                             username = (from s in context.Gen_User
                                         where s.Id == userId
                                         select s.Username).FirstOrDefault();
                         }
                         try
                         {
                             listResponse = serviceCaller.scServiceCaller(CreateSCSAPDictionary(data), null, ref successCount, ref errorCount, username);
                         }
                         catch (Exception ex)
                         {
                             log.Error("Sc SAP service error ;" + ex);
                             ResponseModel responseModel = new ResponseModel();
                             responseModel.Success = false;
                             responseModel.Message = "Error contacting SAP";
                             listResponse.Add(responseModel);
                         }
                         if (isUpdate)
                         {
                             totalFailedCount = UpdateSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                         }
                         else
                         {
                             totalFailedCount = InsertSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                             UpdateSSCTransLineStatus(data.LineId, listResponse);
                         }
                     }
                     totalFailedCount = totalFailedCount + failureList.Count();
                     using (var context = new GoodpackEDIEntities())
                     {
                         Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                         sscTransbatch.RecordCountSAPError = totalFailedCount;
                         sscTransbatch.RecordCountSuccess = sscTransbatch.RecordCountTotal - totalFailedCount;
                         context.SaveChanges();
                     }
                 }
                 catch (Exception e)
                 {
                     log.Error("SubmitToSAPandUpdateSSCTransBatch :-" + e);
                     throw e;
                 }
             }
             private int UpdateSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel, int Id, List<int> updateIds, bool isTermTrip)
             {
                 string lineStatus = null;
                 int failedCount = 0;
                 using (var context = new GoodpackEDIEntities())
                 {
                     for (int i = 0; i < responseModel.Count; i++)
                     {
                         Trn_SSCTransLineContentNew lineContent = context.Trn_SSCTransLineContentNew.FirstOrDefault(j => j.LineId == lineId && j.BatchID == batchId);
                         bool responseStatus=Convert.ToBoolean(responseModel[i].Success);
                         if (responseStatus)
                         {
                             lineStatus = LineStatus.SUCCESS.ToString();
                         }
                         else
                         {
                             failedCount++;
                             lineStatus = LineStatus.ERROR.ToString();
                         }
 
                         log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                         int lineStatusId = (from s in context.Trn_LineStatus
                                             where s.StatusCode == lineStatus
                                             select s.Id).FirstOrDefault();
                         lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                         ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                         var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                         scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; });
                         lineContent.StatusId = lineStatusId;
                         //lineContent.SapResponseMessage = responseModel[i].Message;
                         lineContent.SapResponseMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                     ? "Waiting for SAP response " : responseModel[i].Message;
                         context.SaveChanges();
                     }
                 }
                 return failedCount;
             }
             private int InsertSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel, int Id, List<int> updateIds, bool isTermTrip)
             {
                 string lineStatus = null;
                 int failedCount = 0;
                 using (var context = new GoodpackEDIEntities())
                 {
                     Trn_SSCTransLineContentNew lineContent = new Trn_SSCTransLineContentNew();
                     for (int i = 0; i < responseModel.Count; i++)
                     {
                         bool responseStatus = Convert.ToBoolean(responseModel[i].Success);
                         if (responseStatus)
                         {
                             lineStatus = LineStatus.SUCCESS.ToString();
                         }
                         else
                         {
                             failedCount++;
                             lineStatus = LineStatus.ERROR.ToString();
                         }
                         log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                         int lineStatusId = (from s in context.Trn_LineStatus
                                             where s.StatusCode == lineStatus
                                             select s.Id).FirstOrDefault();
                         lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                       ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                         //UpdateScannedShipMentDetails
                         var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                         scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; });
 
                         lineContent.BatchID = batchId;
                         lineContent.LineId = lineId;
                         lineContent.StatusId = lineStatusId;
                         //lineContent.SapResponseMessage = responseModel[i].Message;
                         lineContent.SapResponseMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                     ? "Waiting for SAP response " : responseModel[i].Message;
                         lineContent.ScannedDataLineId = Id; 
                         context.Trn_SSCTransLineContentNew.Add(lineContent);
                         context.SaveChanges();
                     }
                 }
                 return failedCount;
             }
             private Dictionary<string, string> CreateSCSAPDictionary(SScTOScConvertionModel list)
             {
                 Dictionary<string, string> scData = new Dictionary<string, string>();
                 scData.Add(SCSapMessageFields.DocumentType, "SHCN");
                 scData.Add(SCSapMessageFields.ITRNumber, "10");
                 scData.Add(SCSapMessageFields.ReferenceITRNumber, "");
                 scData.Add(SCSapMessageFields.FromLocation, list.PackingPlant);
                 scData.Add(SCSapMessageFields.ToLocation, list.Consignee);
                 scData.Add(SCSapMessageFields.CustomerReferenceNumber, list.SInumber);
                 scData.Add(SCSapMessageFields.ItemNo, "10");
                 scData.Add(SCSapMessageFields.MaterialNumber, list.BinType);
                 scData.Add(SCSapMessageFields.Quantity, list.Quantity.ToString());
                 scData.Add(SCSapMessageFields.ContainerNumber, "");
                 scData.Add(SCSapMessageFields.CustomerNumber, list.CustomerName);
                 scData.Add(SCSapMessageFields.SINumber, list.SInumber);
                 scData.Add(SCSapMessageFields.SalesDocument, "");
                 scData.Add(SCSapMessageFields.ETDDate, list.ETD);
                 scData.Add(SCSapMessageFields.DTADate, list.ETA);
                 scData.Add(SCSapMessageFields.Remarks, list.Remark);
 
                 scData.Add(SCSapMessageFields.DRYAGE_SHIPMENT, list.DRYAGE_SHIPMENT);
                 scData.Add(SCSapMessageFields.PLANT, list.PLANT);
                 scData.Add(SCSapMessageFields.SKU, list.SKU);
                 scData.Add(SCSapMessageFields.SKU_TEXT, list.SKU_TEXT);
                 scData.Add(SCSapMessageFields.SHIP_TO, list.SHIP_TO);
                 scData.Add(SCSapMessageFields.ST_NAME, list.ST_NAME);
                 scData.Add(SCSapMessageFields.BATCH, list.BATCH);
                 scData.Add(SCSapMessageFields.ADDFIELD1, list.ADDFIELD1);
                 scData.Add(SCSapMessageFields.ADDFIELD2, list.ADDFIELD2);
                 scData.Add(SCSapMessageFields.ADDFIELD3, list.ADDFIELD3);
                 scData.Add(SCSapMessageFields.ADDFIELD4, list.ADDFIELD4);
                 scData.Add(SCSapMessageFields.ADDFIELD5, list.ADDFIELD5);
 
                 return scData;
             }
             private void UpdateSSCTransLineStatus(int lineId, List<ResponseModel> responseModel)
             {
                 string lineStatus = null;
                 int lineStatusId = -1;
                 using (var context = new GoodpackEDIEntities())
                 {
                     var datatoUpdate = context.Trn_SSCTransLineNew.Where(i => i.LineNumber == lineId).ToList();
                     for (int i = 0; i < responseModel.Count; i++)
                     {
                         bool responseStatus = Convert.ToBoolean(responseModel[i].Success);
                         if (responseStatus)
                         {
                             lineStatus = LineStatus.SUCCESS.ToString();
                         }
                         else
                         {
 
                             lineStatus = LineStatus.ERROR.ToString();
                         }
                         log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                         lineStatusId = (from s in context.Trn_LineStatus
                                         where s.StatusCode == lineStatus
                                         select s.Id).FirstOrDefault();
                     }
                     datatoUpdate.ForEach(a => { a.StatusId = lineStatusId; });
                     context.SaveChanges();
                 }
             }
             private static string WriteDataTable(DataTable sourceTable)
             {
                 StringBuilder s = new StringBuilder();
                 List<string> headerValues = new List<string>();
                 foreach (DataColumn column in sourceTable.Columns)
                 {
                     headerValues.Add(QuoteValue(column.ColumnName));
                 }
                 s.Append(String.Join(",", headerValues.ToArray()) + Environment.NewLine);
                 string[] items = null;
                 foreach (DataRow row in sourceTable.Rows)
                 {
                     items = row.ItemArray.Select(o => QuoteValue(o.ToString())).ToArray();
                     s.Append(String.Join(",", items) + Environment.NewLine);
                 }
                 return s.ToString();
             }
             private static string QuoteValue(string value)
             {
                 return String.Concat("\"", value.Replace("\"", "\"\""), "\"");
             }
             private static DataTable ConvertToDatatable<T>(List<T> data)
             {
                 PropertyDescriptorCollection props =
                     TypeDescriptor.GetProperties(typeof(T));
                 DataTable table = new DataTable();
                 for (int i = 0; i < props.Count; i++)
                 {
                     PropertyDescriptor prop = props[i];
                     if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                         table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                     else
                         table.Columns.Add(prop.Name, prop.PropertyType);
                 }
                 object[] values = new object[props.Count];
                 foreach (T item in data)
                 {
                     for (int i = 0; i < values.Length; i++)
                     {
                         values[i] = props[i].GetValue(item);
                     }
                     table.Rows.Add(values);
                 }
                 return table;
             }
             private string[] getAllRecipients(string mapperFile)
             {
                 List<string> mailList = new List<string>();
                 //  mailList.Add(objGPCustomerMessage.EmailId);            
                 using (var context = new GoodpackEDIEntities())
                 {
                     int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                                    where s.Filename == mapperFile
                                    select s.EDISubsiId).FirstOrDefault();
                     IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                             where s.SubsiId == subsiId
                                             select s.UserId).ToList();
 
                     foreach (var item in userLists)
                     {
                         string mailId = (from s in context.Gen_User
                                          where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                          select s.EmailId).FirstOrDefault();
                         log.Info("Mail sending to " + mailId);
                         if (mailId != null)
                         {
                             mailList.Add(mailId);
                         }
 
 
                     }
                     string emailId = (from s in context.Gen_Configuration
                                       where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                       select s.Value).FirstOrDefault();
                     mailList.Add(emailId);
                     return mailList.ToArray();
                 }
 
 
             }
             private void UpdateSSCBatchFileData(SCTransactionModel transBtachDetails)
             {
                 using (var context = new GoodpackEDIEntities())
                 {
                     string statusCode = transBtachDetails.StatusCode.ToString();
                     string errorCode = transBtachDetails.ErrorCode.ToString();
                     int statusID = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == statusCode
                                     select s.Id).FirstOrDefault();
                     int errorCodeID = (from s in context.Ref_ErrorCodes
                                        where s.ErrorCode == errorCode
                                        select s.Id).FirstOrDefault();
 
                     Trn_SSCTransBatch scFileData = context.Trn_SSCTransBatch.FirstOrDefault(x => (x.Id == transBtachDetails.BatchID)); //&& x.StatusId != 10 && x.StatusId != 12 && x.StatusId != 13));//Give where condition in valuetype
                     scFileData.StatusId = statusID;
                     scFileData.ErrorId = errorCodeID;
                     scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                     scFileData.DateUpdated = DateTime.Now;
                     context.SaveChanges();
                 }
 
             }
         #endregion
        
     }
 }

 

