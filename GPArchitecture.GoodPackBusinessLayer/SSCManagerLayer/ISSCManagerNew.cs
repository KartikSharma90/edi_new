﻿using GPArchitecture.Models;
using System.Collections.Generic;

namespace GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer
{
    public interface ISSCManagerNew
    {
        IList<GenericItemModel> submitSSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, int fileType);
    }
}
