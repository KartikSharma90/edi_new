﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.MappingManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.SCManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer;
using GPArchitecture.Models;
using GPArchitecture.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Objects.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Transactions;
using System.Web.Configuration;
using System.Web.Script.Serialization;


namespace GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer
{
    public class DeviceManager : ISSCManager
    {        
        public string _UserName{ get; set; }
        public string _TransactionType { get; set; }
        public bool isTermTrip { get; set; }

        private static readonly ILog log = LogManager.GetLogger(typeof(DeviceManager));
        ResponseModel responseModel = new ResponseModel();

        #region Public

        public List<DeviceList> GetDeviceList()
        {
            log.Debug("Enter GetDeviceList");
            using (var context = new GoodpackEDIEntities())
            {
                var deviceDetails = (from c in context.Trn_DeviceDetails
                                     select new DeviceList
                                     {
                                         Id = c.Id,
                                         DeviceID = c.DeviceID,
                                         CustomerEmail = c.CustomerEmail,
                                         Details = c.Details,
                                         IsEnabled = c.IsEnabled,
                                         MapperName = c.MapperName,
                                         PackerCode = c.PackerCode,
                                         Vertical=c.Vertical,
                                         SKU=c.SKU,
                                         TransactionType=c.TransactionTypes.Trim(),
                                         CustomerCode = c.CustomerCode,
                                         SubsiId=c.SubsiId.Trim()
                                     }).ToList();
                return deviceDetails;
            }

        }

        public ResponseModel CreateDevice(DeviceList detail, string userName)
        {
            log.Debug("Enter CreateDevice");
            try
            {
                using (var context = new GoodpackEDIEntities())
                {

                    var deviceexist = (from s in context.Trn_DeviceDetails
                                       where s.DeviceID == detail.DeviceID
                                       select s).FirstOrDefault();
                    if (deviceexist != null)
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Device already Registred";
                        return responseModel;
                    }
                    Trn_DeviceDetails devicedetail = new Trn_DeviceDetails();
                    devicedetail.CustomerCode = detail.CustomerCode;
                    devicedetail.CustomerEmail = detail.CustomerEmail;
                    devicedetail.Details = detail.Details;
                    devicedetail.DeviceID = detail.DeviceID;
                    devicedetail.UserId = (from s in context.Gen_User
                                           where s.Username == userName
                                           select s.Id).FirstOrDefault();
                    devicedetail.IsEnabled = detail.IsEnabled;
                    devicedetail.Vertical = detail.Vertical;
                    devicedetail.MapperName = detail.MapperName;
                    devicedetail.PackerCode = detail.PackerCode;
                    devicedetail.SKU = detail.SKU;
                    devicedetail.SubsiId = detail.SubsiId;
                    devicedetail.TransactionTypes = detail.TransactionType;
                    context.Trn_DeviceDetails.Add(devicedetail);
                    context.SaveChanges();
                    responseModel.Success = true;
                }
                responseModel = CreateCustomMapper(detail, userName);
                return responseModel;
            }
            catch (Exception e)
            {
                responseModel.Success = false;
                log.Error("API exception :-CreateDevice, Exception Message -" + e.Message);
                return responseModel;
            }
        }

        public ResponseModel UpdateDevice(DeviceList detail, string userName)
        {
            log.Debug("Enter UpdateDevice");
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    Trn_DeviceDetails devicedetail = context.Trn_DeviceDetails.FirstOrDefault(i => i.Id == detail.Id);
                    devicedetail.CustomerCode = detail.CustomerCode;
                    devicedetail.CustomerEmail = detail.CustomerEmail;
                    devicedetail.Details = detail.Details;
                    devicedetail.DeviceID = detail.DeviceID;
                    devicedetail.IsEnabled = detail.IsEnabled;
                    devicedetail.SKU = detail.SKU;
                    devicedetail.Vertical = detail.Vertical;
                    devicedetail.SubsiId = detail.SubsiId;
                    devicedetail.TransactionTypes = detail.TransactionType;
                    //devicedetail.MapperName = detail.MapperName;
                    devicedetail.PackerCode = detail.PackerCode;
                    UpdateDeviceMapperSubsi(devicedetail.MapperName, detail.SubsiId);
                    context.SaveChanges();                   
                    responseModel.Success = true;
                }
            }
            catch (Exception e)
            {
                responseModel.Success = false;
                responseModel.Message = "Error while updating";
                log.Error("API exception :-UpdateDevice, Exception Message -" + e.Message);
            }
            return responseModel;
        }

        public ResponseModel DeleteDevice(int Id, string userName)
        {
            log.Debug("Enter DeleteDevice");
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    // Trn_DeviceDetails
                    var devicemapperDetail = (from s in context.Trn_DeviceDetails
                                              where s.Id == Id
                                              select s.MapperName).FirstOrDefault();

                    var subsiToTransactionFileMapperId = (from s in context.Trn_SubsiToTransactionFileMapper
                                                          where s.Filename == devicemapperDetail
                                                          select s.Id).FirstOrDefault();
                    if (subsiToTransactionFileMapperId != null)
                    {
                        var mappingSubsiId = (from s in context.Trn_MappingSubsi
                                              where s.SubsiToTransactionFileMapperId == subsiToTransactionFileMapperId
                                              select s.Id).FirstOrDefault();
                        //Trn_MapperFieldSpecs
                        //Trn_MappingConfiguration mappingConfig = context.Trn_MappingConfiguration.Find((from s in context.Trn_MappingConfiguration
                        //                                                                                where s.MappingSubsiId == mappingSubsiId
                        //                                                                                select s.Id).SingleOrDefault());
                        ICollection<Trn_MappingConfiguration> mappingConfigcollectn = (from s in context.Trn_MappingConfiguration
                                                                                       where s.MappingSubsiId == mappingSubsiId
                                                                                       select s).ToList();
                        foreach (var mappingConfig in mappingConfigcollectn)
                        {
                            context.Trn_MappingConfiguration.Remove(mappingConfig);
                            context.SaveChanges();
                        }
                        Trn_MappingSubsi mappingSubsi = context.Trn_MappingSubsi.Find((from s in context.Trn_MappingSubsi
                                                                                       where s.SubsiToTransactionFileMapperId == subsiToTransactionFileMapperId
                                                                                       select s.Id).SingleOrDefault());
                        if (mappingSubsi != null)
                        {
                            context.Trn_MappingSubsi.Remove(mappingSubsi);
                            context.SaveChanges();
                        }
                        Trn_SubsiToTransactionFileMapper subsitoTransaction = context.Trn_SubsiToTransactionFileMapper.Find(subsiToTransactionFileMapperId);
                        if (subsitoTransaction != null)
                        {
                            context.Trn_SubsiToTransactionFileMapper.Remove(subsitoTransaction);
                            context.SaveChanges();
                        }
                        
                        Trn_DeviceDetails devidedetail = context.Trn_DeviceDetails.Find(Id);
                        if (devidedetail!=null)
                        {
                            context.Trn_DeviceDetails.Remove(devidedetail);
                            context.SaveChanges();
                        }
                        responseModel.Success = true;
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Failed to delete record";
                    }
                }
            }
            catch (Exception e)
            {
                responseModel.Success = false;
                responseModel.Message = "Failed to delete record";
                log.Error("API exception :-DeleteDevice, Exception Message -" + e.Message);
            }
            return responseModel;
        }

        public bool DeviceRegistered(ShipmentData data)
        {
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    var deviceDetail = (from s in context.Trn_DeviceDetails
                                        where s.DeviceID == data.DeviceId && s.IsEnabled==true
                                        select s).FirstOrDefault();
                    if (deviceDetail == null)
                        return false;
                    else
                    {
                        if (deviceDetail.IsEnabled)
                            return true;
                        else
                            return false;
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("API exception :-DeviceRegistered, Exception Message -" + e.Message);
                throw e;
            }
        }

        public List<ShipmentResponse> ShipmentData(ShipmentData data)
        {
            List<ShipmentResponse> listResponse = new List<ShipmentResponse>();
            List<int> insertedId = new List<int>();
            var json = new JavaScriptSerializer().Serialize(data);
            log.Debug(" ShipmentData ,Data model -" + json);
            try
            {
                using (var scope = new TransactionScope())
                {
                    try
                    {
                        using (var context = new GoodpackEDIEntities())
                        {
                            List<string> customerReference = new List<string>();
                            var deviceDetail = (from s in context.Trn_DeviceDetails
                                                where s.DeviceID == data.DeviceId
                                                select s).FirstOrDefault();
                            if (deviceDetail != null)
                            {
                                foreach (ShipmentSync s in data.scandata)
                                {
                                    _TransactionType = deviceDetail.TransactionTypes.Trim();
                                    Trn_ScannedShipmentDetails scannedShipmentDetails = new Trn_ScannedShipmentDetails();
                                    scannedShipmentDetails.DeviceId = data.DeviceId.Trim();
                                    scannedShipmentDetails.Barcode = s.Barcode.Trim();
                                    scannedShipmentDetails.ConsigneeId = s.ConsigneeId.Trim();
                                    scannedShipmentDetails.CustomerReferenceNumber = s.CustomerReference.Trim();
                                    scannedShipmentDetails.ETA = s.ETA.Trim();
                                    scannedShipmentDetails.ETD = s.ETD.Trim();
                                    scannedShipmentDetails.SINumber = s.SINumber.Trim();
                                    scannedShipmentDetails.BinType = s.BinType.Trim();
                                    context.Trn_ScannedShipmentDetails.Add(scannedShipmentDetails);
                                    context.SaveChanges();
                                    insertedId.Add(scannedShipmentDetails.Id);
                                    if (!customerReference.Contains(s.CustomerReference))
                                    {
                                        ShipmentResponse response = new ShipmentResponse();
                                        response.Status = true;
                                        response.Message = "Success";
                                        response.CustomerReference = s.CustomerReference;
                                        customerReference.Add(s.CustomerReference);
                                        listResponse.Add(response);
                                    }
                                }
                                scope.Complete();
                                Thread thread = new Thread(delegate()
                                {
                                    DataValidatingandService(insertedId);
                                });
                                thread.Start();
                            }
                            else
                            {
                                ShipmentResponse response = new ShipmentResponse();
                                response.Status = false;
                                response.Message = "Failed";
                                listResponse.Add(response);
                                scope.Dispose();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ShipmentResponse response = new ShipmentResponse();
                        response.Status = false;
                        response.Message = "Failed";
                        listResponse.Add(response);
                        scope.Dispose();                        
                       // log.Error("Error Model :-" + json);
                        log.Error("API exception :-ShipmentData, Exception Message -" + ex+" , Inner Exception:- "+ex.InnerException.Message);
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("API exception :-ShipmentData, Exception Message -" + e.Message);
                throw e;
            }
            return listResponse;
        }
      
        public RegisterScannerResponse RegisterScanner(ShipmentData data)
        {
            RegisterScannerResponse registerScannerResponse = new RegisterScannerResponse();
            try
            {               
                registerScannerResponse = GetDeviceDetails(data.DeviceId);
                //List<SScTOScConvertionModel> listSSC = ValidateDeviceData(CreateIntermediateData(), data.DeviceId);
                //int batchId = InsertSSCTransBatch(listSSC);
                //InsertSSCTransLine(listSSC, batchId);
            }
            catch (Exception e)
            {
                registerScannerResponse.status = false;
                registerScannerResponse.message = "API Exception";
                log.Error("API exception :-RegisterScanner, Exception Message -" + e.Message);
            }
            return registerScannerResponse;
        }

        public void SendEmailToGoodPackAdmin(string DeviceId)
        {
            log.Debug("DeviceManager ,DeviceId -" + DeviceId);
            try
            {
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$DeviceId$$", DeviceId);
                string[] customerEmail = new string[] { "anoop.varghese@goodpack.com" };
                GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SSC_NEW_REGISTRATION", null);
            }
            catch (Exception e)
            {
                log.Error("API Exception  DeviceManager,Error -" + e.Message);
                throw e; 
            }
        }

        public List<VerticalModel> GetVerticalData()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Trn_Vertical
                        select new VerticalModel
                        {
                            DisplayText = s.Name,
                            Value = s.Name
                        }).ToList();
            }
        }

        public bool ReSubmitErrorData(int BatchId)
        {
            bool hasErrorRecords ;
            using (var context = new GoodpackEDIEntities())
            {
                string lineStatus= LineStatus.ERROR.ToString();
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == lineStatus
                                    select s.Id).FirstOrDefault();
                List<int> errorDataForBatchId = context.Trn_ScannedShipmentDetails.Where(i => i.BatchId == BatchId && i.Status == lineStatusId).Select(i => i.Id).ToList();
                string deviceId = context.Trn_ScannedShipmentDetails.Where(i => i.BatchId == BatchId && i.Status == lineStatusId).Select(i => i.DeviceId).Take(1).SingleOrDefault().Trim();
                _TransactionType = context.Trn_DeviceDetails.Where(x => x.DeviceID == deviceId).Select(x => x.TransactionTypes).SingleOrDefault().Trim();
                List<SScTOScConvertionModel> listSSC = ValidateDeviceData(CreateIntermediateData(errorDataForBatchId), errorDataForBatchId); 
                UpdateSSCTransLine(listSSC, BatchId, errorDataForBatchId);
                hasErrorRecords = context.Trn_SSCTransBatch.Where(i => i.Id == BatchId).Select(i => i.RecordCountSAPError).FirstOrDefault() > 0 ? true : false;
                return hasErrorRecords;
            }
        }

        public List<VerticalModel> GetDeviceTransactionType()
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<string> list=new List<string>();
                list=WebConfigurationManager.AppSettings["DeviceTrnType"].Split(',').ToList();
                return (from s in context.Ref_TransactionTypes
                        where list.Contains(s.TransactionCode)
                        select new VerticalModel
                        {
                            DisplayText = s.TransactionCode,
                            Value = s.TransactionCode
                        }).ToList();
            }
        }

        public List<VerticalModel> GetSubsyData()
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<string> list = new List<string>();
              
                return (from s in context.Gen_EDISubsies                     
                        select new VerticalModel
                        {
                            DisplayText = s.SubsiName,
                            Value =SqlFunctions.StringConvert((double)s.SubsiCode).Trim()
                        }).ToList();
            }
        }
        #endregion

        #region Private

        private void UpdateDeviceMapperSubsi(string mapperName,string subsiCode)
        {
            using(var context=new GoodpackEDIEntities())
            {
                int subsiCodeValue = Convert.ToInt32(subsiCode);
                int subsiIdValue = context.Gen_EDISubsies.Where(i => i.SubsiCode == subsiCodeValue).Select(i => i.Id).FirstOrDefault();
                Trn_SubsiToTransactionFileMapper mapperDetails = context.Trn_SubsiToTransactionFileMapper.Where(i => i.Filename == mapperName).FirstOrDefault();
                mapperDetails.EDISubsiId = subsiIdValue;
                List<Trn_LookupValues> lookUpValue = context.Trn_LookupValues.Where(i => i.MapperName == mapperName).ToList();
                lookUpValue.ForEach(i => { i.SubsiId = subsiIdValue; });
                context.SaveChanges();
            }
        }
        private void DataValidatingandService(List<int> Ids)
        {
            try
            {
                List<SScTOScConvertionModel> listSSC = ValidateDeviceData(CreateIntermediateData(Ids), Ids);
                int batchId = InsertSSCTransBatch(listSSC);
                InsertSSCTransLine(listSSC, batchId, Ids);
            }
            catch(Exception e)
            {
                log.Error("SSC Data update error:" + e);
            }
        }

        private ResponseModel CreateCustomMapper(DeviceList detail, string userName)
        {
            MappingManagementLayer.MapperManager mappingManager = new MappingManagementLayer.MapperManager();
            var details = GetCustomerDetails(detail);
            MappingModel mappingModel = new MappingModel();
            mappingModel.SubsiId = details;
            mappingModel.TransactionId = 3;
            mappingModel.FileTypeId = 3;
            mappingModel.FieldCountValue = "1";
            mappingModel.MappedFileModel = GetMappingFieldModel();
            mappingModel.FileHeaders = GetCutomFileHeaders();
            mappingModel.SAPFieldHeaders = GetSAPFieldHeaders();
            mappingModel.DateFormat = "DD-MM-YYYY";
            mappingModel.FileName = detail.MapperName;
            mappingModel.EnableEmail = false;
            mappingModel.EmailIds = detail.CustomerEmail;
            return mappingManager.SaveHeaderFieldsForSSCRegistration(mappingModel, userName, detail.MapperName);
            //mappingModel.FieldId   
            //mappingModel.SRNO
            //mappingModel.IsCustomizedInput
            //mappingModel.RowNumber
            //mappingModel.ColumnNumber 
        }

        private int GetCustomerDetails(DeviceList detail)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int subsiCode=Convert.ToInt32(detail.SubsiId.Trim());
                var details = (from c in context.Gen_EDISubsies
                               where c.SubsiCode == subsiCode
                               select c.Id).FirstOrDefault();
                return details;
            }
        }

        private List<FileHeaderModel> GetCutomFileHeaders()
        {
            List<FileHeaderModel> listfileheaderModel = new List<FileHeaderModel>();
            string[] headerArray = { "Customer Name", "Packing Plant", "Consignee", "Doc Ref No/BL number", "Bin Type", "Count(Barcode/RFID)", "Quantity", "ETD date", "ETA date", "Remark" };
            for (int i = 0; i < headerArray.Length; i++)
            {
                FileHeaderModel fileheaderModel = new FileHeaderModel();
                fileheaderModel.HeaderName = headerArray[i];
                fileheaderModel.SRNO = (i + 1).ToString();
                listfileheaderModel.Add(fileheaderModel);
            }
            return listfileheaderModel;
        }

        private List<SAPMappingModel> GetSAPFieldHeaders()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_SAPMessageFields
                        orderby s.FieldSequence
                        where (s.TransactionId == 3)
                        select new SAPMappingModel
                        {
                            Id = s.Id,
                            FieldName = s.SAPFieldName,
                            Note = s.Note,
                            InputType = s.InputType,
                            IsDisplay = s.IsDisplay,
                            IsMandatory = s.IsMandatory,
                            FieldSequence = s.FieldSequence
                        }).ToList();
            }
        }

        private List<MapperHeaderModel> GetMappingFieldModel()
        {
            List<MapperHeaderModel> listMapperHeader = new List<MapperHeaderModel>();

            MapperHeaderModel mapperheader0 = new MapperHeaderModel();
            mapperheader0.FieldName = "From Location";
            mapperheader0.FieldSequence = 4;
            mapperheader0.HeaderName = "Packing Plant";
            mapperheader0.Id = 66;
            mapperheader0.InputType = "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING";
            mapperheader0.MappedField = "From Location ---> Packing Plant";
            mapperheader0.SRNO = 2;
            listMapperHeader.Add(mapperheader0);

            MapperHeaderModel mapperheader1 = new MapperHeaderModel();
            mapperheader1.FieldName = "To Location";
            mapperheader1.FieldSequence = 5;
            mapperheader1.HeaderName = "Consignee";
            mapperheader1.Id = 67;
            mapperheader1.InputType = "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING";
            mapperheader1.MappedField = "To Location ---> Consignee";
            mapperheader1.SRNO = 3;
            listMapperHeader.Add(mapperheader1);

            MapperHeaderModel mapperheader2 = new MapperHeaderModel();
            mapperheader2.FieldName = "Customer Reference Number";
            mapperheader2.FieldSequence = 8;
            mapperheader2.HeaderName = "Doc Ref No/BL number";
            mapperheader2.Id = 70;
            mapperheader2.InputType = "MAP_FROM_SOURCE_WITH_SUBSTRING";
            mapperheader2.MappedField = "Customer Reference Number ---> Doc Ref No/BL number";
            mapperheader2.SRNO = 4;
            listMapperHeader.Add(mapperheader2);

            MapperHeaderModel mapperheader3 = new MapperHeaderModel();
            mapperheader3.FieldName = "Material Number";
            mapperheader3.FieldSequence = 24;
            mapperheader3.HeaderName = "Bin Type";
            mapperheader3.Id = 86;
            mapperheader3.InputType = "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED";
            mapperheader3.MappedField = "Material Number ---> Bin Type";
            mapperheader3.SRNO = 5;
            listMapperHeader.Add(mapperheader3);

            MapperHeaderModel mapperheader4 = new MapperHeaderModel();
            mapperheader4.FieldName = "Quantity";
            mapperheader4.FieldSequence = 25;
            mapperheader4.HeaderName = "Quantity";
            mapperheader4.Id = 87;
            mapperheader4.InputType = "MAP_FROM_SOURCE";
            mapperheader4.MappedField = "Quantity --->Count(Barcode/RFID)";
            mapperheader4.SRNO = 6;
            listMapperHeader.Add(mapperheader4);

            MapperHeaderModel mapperheader5 = new MapperHeaderModel();
            mapperheader5.FieldName = "Customer Number";
            mapperheader5.FieldSequence = 27;
            mapperheader5.HeaderName = "Customer Name";
            mapperheader5.Id = 89;
            mapperheader5.InputType = "OPERATION_ON_SOURCE_FIELDS2";
            mapperheader5.MappedField = "Customer Number ---> Customer Name";
            mapperheader5.SRNO = 1;
            listMapperHeader.Add(mapperheader5);

            MapperHeaderModel mapperheader6 = new MapperHeaderModel();
            mapperheader6.FieldName = "SI Number";
            mapperheader6.FieldSequence = 28;
            mapperheader6.HeaderName = "Doc Ref No/BL number";
            mapperheader6.Id = 90;
            mapperheader6.InputType = "MAP_FROM_SOURCE_WITH_SUBSTRING";
            mapperheader6.MappedField = "SI Number ---> Doc Ref No/BL number";
            mapperheader6.SRNO = 4;
            listMapperHeader.Add(mapperheader6);


            MapperHeaderModel mapperheader7 = new MapperHeaderModel();
            mapperheader7.FieldName = "ETD Date";
            mapperheader7.FieldSequence = 32;
            mapperheader7.HeaderName = "ETD date";
            mapperheader7.Id = 94;
            mapperheader7.InputType = "DATE";
            mapperheader7.MappedField = "ETD Date ---> ETD date";
            mapperheader7.SRNO = 8;
            listMapperHeader.Add(mapperheader7);

            MapperHeaderModel mapperheader8 = new MapperHeaderModel();
            mapperheader8.FieldName = "DTA Date";
            mapperheader8.FieldSequence = 33;
            mapperheader8.HeaderName = "ETA date";
            mapperheader8.Id = 95;
            mapperheader8.InputType = "DATE";
            mapperheader8.MappedField = "DTA Date ---> ETA date";
            mapperheader8.SRNO = 9;
            listMapperHeader.Add(mapperheader8);

            MapperHeaderModel mapperheader9 = new MapperHeaderModel();
            mapperheader9.FieldName = "Remarks";
            mapperheader9.FieldSequence = 34;
            mapperheader9.HeaderName = "Remark";
            mapperheader9.Id = 96;
            mapperheader9.InputType = "MAP_FROM_SOURCE_WITH_SUBSTRING";
            mapperheader9.MappedField = "Remarks ---> Remark";
            mapperheader9.SRNO = 10;
            listMapperHeader.Add(mapperheader9);

            return listMapperHeader;
        }

        private RegisterScannerResponse GetDeviceDetails(string deviceId)
        {
            RegisterScannerResponse regScanner=new RegisterScannerResponse();
            
            using (var context = new GoodpackEDIEntities())
            {
                var skuValue = (from s in context.Trn_DeviceDetails
                                         where s.DeviceID == deviceId
                                         select s.SKU).SingleOrDefault().Split(',');
                List<string> stringValue = skuValue.ToList();
                RegisterScanner registeredScanner = (from s in context.Trn_DeviceDetails
                                        where s.DeviceID == deviceId
                                        select new RegisterScanner
                                        {
                                            Vertical = s.Vertical,
                                            BinTypes = stringValue
                                        }).SingleOrDefault();
                regScanner.message = "Success";
                regScanner.status = true;
                regScanner.registerScanner = registeredScanner;
            }
            return regScanner;
        }

        private List<SScTOScConvertionModel> CreateIntermediateData(List<int> shipmentDetailsIds)
        {
            List<SScTOScConvertionModel> ssctoScConvertionModel = new List<SScTOScConvertionModel>();
            try
            {               
                using (var context = new GoodpackEDIEntities())
                {                  
                    List<DataConvertionModel> data = (from queryResult in context.Trn_ScannedShipmentDetails
                                                      where shipmentDetailsIds.Contains(queryResult.Id)
                                                      group queryResult by queryResult.SINumber into rowGroup
                                                      select new DataConvertionModel
                                                      {
                                                          SINumber = rowGroup.Key,
                                                          Count = rowGroup.Count()
                                                      }).ToList();

                    foreach (var t in data)
                    {
                        var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                        where shipmentDetailsIds.Contains(s.Id)
                                        select s.DeviceId).FirstOrDefault();
                        var packingPlantCustomerCode = GetPackerCodeFromDeviceId(deviceId);
                        string packingPlantString = packingPlantCustomerCode.PackerCode.ToString();
                        string customerCode = packingPlantCustomerCode.CustomerCode.ToString();
                        SScTOScConvertionModel ssctoscModel = (from s in context.Trn_ScannedShipmentDetails
                                                               where shipmentDetailsIds.Contains(s.Id) && s.SINumber==t.SINumber
                                                               select new SScTOScConvertionModel
                                                               {
                                                                   BinType = s.BinType,
                                                                   Consignee = s.ConsigneeId,
                                                                   CustomerName = customerCode,
                                                                   ETA = s.ETA,
                                                                   ETD = s.ETD,
                                                                   DeviceId = deviceId,
                                                                   PackingPlant = packingPlantString,
                                                                   Quantity = t.Count,
                                                                   Remark = "",
                                                                   MapperName = packingPlantCustomerCode.MapperName,
                                                                   LineId=s.Id,
                                                                   SInumber = s.SINumber,
                                                                   CustomerReferenceNumber=s.CustomerReferenceNumber
                                                               }).FirstOrDefault();
                        ssctoScConvertionModel.Add(ssctoscModel);
                    }                    
                }
            }
            catch(Exception e)
            {
                log.Error("CreateIntermediateData error:" + e);
            }
            return ssctoScConvertionModel;
        }

        private DeviceDetail GetPackerCodeFromDeviceId(string deviceId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                var packerCustomerCode = (from s in context.Trn_DeviceDetails
                                     where s.DeviceID == deviceId
                                  select new DeviceDetail { PackerCode = s.PackerCode, MapperName = s.MapperName,CustomerCode=s.CustomerCode }).SingleOrDefault();
                return packerCustomerCode;
            }
                              
        }

        private int InsertSSCTransBatch(List<SScTOScConvertionModel> listSSC)
        {
            DataTable dataTable = ConvertToDatatable(listSSC);
            var batchData = WriteDataTable(dataTable);
            int batchId=0;
            int totalCount = listSSC.Count;
            int errorcount = listSSC.Where(r => r.ErrorMessage != null).Count();
            using (var context = new GoodpackEDIEntities())
            {
                foreach (var sscData in listSSC)
                {
                   ITirmTripManager termTrip=new TirmTripManager();
                   if (_TransactionType != "ASN")
                       isTermTrip = termTrip.IsTirmTripORNot(sscData.CustomerName);
                   else
                       isTermTrip = false;                 
                    var userid = (from s in context.Trn_DeviceDetails
                                  where s.DeviceID == sscData.DeviceId
                                  select s.UserId).SingleOrDefault();
                    Trn_SSCTransBatch scFileData = new Trn_SSCTransBatch();
                    scFileData.UserId = Convert.ToInt32(userid);
                    scFileData.SubsyId = sscData.SubsiId;
                    scFileData.TransactionId =(_TransactionType == "ASN")?12:3;
                    scFileData.MapperFileName = sscData.MapperName;
                    scFileData.StatusId = 17;
                    scFileData.ErrorId = 28;
                    scFileData.IsTermTrip = isTermTrip;
                    //scFileData.FileName = ;                   
                    scFileData.FileSource = "Scanner";
                    //scFileData.FileSourceAddress =;
                    scFileData.BatchContent = batchData;
                    scFileData.ErrorDescription = "";
                    scFileData.DateCreated = DateTime.Now;
                    scFileData.DateUpdated = null;
                    scFileData.RecordCountTotal = totalCount;
                    scFileData.RecordCountSkipValFailure = errorcount;
                    scFileData.RecordCountFieldValFailure = 0;
                    scFileData.RecordCountSAPError = 0;
                    scFileData.RecordCountSuccess = 0;
                    scFileData.RecordCountSAPInProgress = 0;
                    scFileData.DateExportedToTargetSystem = null;
                    //scFileData.IsTestMode = true;
                    scFileData.EmailId = null;
                    context.Trn_SSCTransBatch.Add(scFileData);
                    context.SaveChanges();
                    batchId = scFileData.Id;
                    break;
                }
            }
            return batchId;
        }

        private void InsertSSCTransLine(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids)
        {
            try
            {
                //List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
                //List<SScTOScConvertionModel> failureList = listSSC.Where(i => i.ErrorMessage != null).ToList();
                using (var context = new GoodpackEDIEntities())
                {
                    foreach (var sscData in listSSC)
                    {
                        bool termTrip = false;
                        ITirmTripManager termtrip = new TirmTripManager();
                        if (_TransactionType != "ASN")
                            termtrip.IsTirmTripORNot(sscData.CustomerName);                        
                        Trn_SSCTransLineNew sscTrsansLine = new Trn_SSCTransLineNew();
                        sscTrsansLine.BatchId = batchId;
                        sscTrsansLine.BinType = sscData.BinType;
                        sscTrsansLine.SINumber = sscData.SInumber;
                        sscTrsansLine.CustomerCode = sscData.CustomerName;
                        sscTrsansLine.CustomerRefNumber = sscData.CustomerReferenceNumber;
                        sscTrsansLine.ETA = sscData.ETA;
                        sscTrsansLine.ETD = sscData.ETD;
                        sscTrsansLine.FromLocation = sscData.PackingPlant;
                        sscTrsansLine.InTestMode = true;
                        sscTrsansLine.IsActive = true;
                        sscTrsansLine.ItemNumber = "10";
                        sscTrsansLine.ItrNumber = "10";
                        sscTrsansLine.ItrType = "SHCN";
                        sscTrsansLine.LastUpdated = DateTime.Now;
                        sscTrsansLine.LineContent = "";
                        sscTrsansLine.LineNumber = sscData.LineId;
                        sscTrsansLine.StatusId = 3;
                        bool? boolValue = null;
                        if (termTrip)
                            boolValue = false;
                        sscTrsansLine.TermTripSAP = boolValue;
                        sscTrsansLine.Quantity = sscData.Quantity;
                        sscTrsansLine.SubsiId = sscData.ErrorMessage == null ? 6 : 1;
                        sscTrsansLine.ToLocation = sscData.Consignee;
                        context.Trn_SSCTransLineNew.Add(sscTrsansLine);
                        context.SaveChanges();
                        sscData.TransLineId = sscTrsansLine.Id;                        
                    }
                }
                SubmitToSAPandUpdateSSCTransBatch(listSSC, batchId,Ids,false);
                UpdateSSCTransBatchAndSendEmail(batchId);
            }
            catch (Exception e)
            {
                log.Error("InsertSSCTransLine error:" + e);
            }
        }

        private void SubmitToSAPandUpdateSSCTransBatch(List<SScTOScConvertionModel> listSSC, int batchId, List<int> Ids,bool isUpdate)
        {
            try
            {
                int successCount = 0;
                int errorCount = 0;
                int totalFailedCount=0;              
                List<ResponseModel> listResponse = null;
                SCServiceManager serviceCaller = new SCServiceManager();
                List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
                List<SScTOScConvertionModel> failureList = listSSC.Where(i => i.ErrorMessage != null).ToList();
                foreach (var errorData in failureList)
                {
                    ITirmTripManager termTrip = new TirmTripManager();
                    if (_TransactionType != "ASN")
                        isTermTrip = termTrip.IsTirmTripORNot(errorData.CustomerName);
                    else
                        isTermTrip = false;                    
                    List<ResponseModel> responseModel=new List<ResponseModel>();
                    ResponseModel response=new ResponseModel();
                    response.Success=false;
                    response.Message=errorData.ErrorMessage;
                    responseModel.Add(response);
                    if (isUpdate)
                    {
                        UpdateSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                    }
                    else
                    {
                        InsertSSCTransLineContent(errorData.TransLineId, batchId, responseModel, errorData.LineId, Ids, isTermTrip);
                    }
                    using (var context = new GoodpackEDIEntities())
                    {
                        Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                        sscTransbatch.RecordCountSAPError = failureList.Count();                       
                        //sscTransbatch.RecordCountSuccess = successCount;
                        context.SaveChanges();
                    }
                    UpdateSSCTransLineStatus(errorData.LineId, responseModel, isTermTrip);
                }
                foreach (var data in successList)
                {
                    ITirmTripManager termTrip = new TirmTripManager();
                    if (_TransactionType != "ASN")
                        isTermTrip = termTrip.IsTirmTripORNot(data.CustomerName);
                    else
                        isTermTrip = false;
                    var username = "";
                    int? userId = 0;                   
                    using (var context = new GoodpackEDIEntities())
                    {
                        userId = (from s in context.Trn_DeviceDetails
                                  where s.DeviceID == data.DeviceId
                                  select s.UserId).FirstOrDefault();
                        username = (from s in context.Gen_User
                                    where s.Id == userId
                                    select s.Username).FirstOrDefault();
                    }
                    try
                    {
                        listResponse = serviceCaller.scServiceCaller(CreateSCSAPDictionary(data), null, ref successCount, ref errorCount, username);
                    }
                    catch(Exception e)
                    {
                        log.Error("SAP Service Error :-" + e);
                        listResponse = new List<ResponseModel>();
                        ResponseModel response = new ResponseModel();
                        response.Message = "SAP Service Error";
                        response.Success = false;
                        listResponse.Add(response);
                    }
                    if (isUpdate)
                    {
                        totalFailedCount = UpdateSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                    }
                    else
                    {
                        totalFailedCount = InsertSSCTransLineContent(data.TransLineId, batchId, listResponse, data.LineId, Ids, isTermTrip) > 0 ? totalFailedCount + 1 : totalFailedCount;
                        UpdateSSCTransLineStatus(data.LineId, listResponse, isTermTrip);
                    }
                }
                totalFailedCount = totalFailedCount + failureList.Count();
                using (var context = new GoodpackEDIEntities())
                {
                    Trn_SSCTransBatch sscTransbatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                    sscTransbatch.RecordCountSAPError = totalFailedCount;
                    if (!isTermTrip)
                        sscTransbatch.RecordCountSuccess = sscTransbatch.RecordCountTotal - totalFailedCount;
                    context.SaveChanges();
                }
            }
            catch(Exception e)
            {
                log.Error("SubmitToSAPandUpdateSSCTransBatch error:" + e);
            }
        }

        private int InsertSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel,  int Id, List<int> updateIds,bool isTermTrip)
        {
            string lineStatus = null;          
            int failedCount = 0;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    Trn_SSCTransLineContentNew lineContent = new Trn_SSCTransLineContentNew();
                    for (int i = 0; i < responseModel.Count; i++)
                    {
                        bool response=Convert.ToBoolean(responseModel[i].Success);
                        if (response)
                        {
                            lineStatus = LineStatus.SUCCESS.ToString();
                        }
                        else
                        {
                            failedCount++;
                            lineStatus = LineStatus.ERROR.ToString();
                        }
                        log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                        int lineStatusId = (from s in context.Trn_LineStatus
                                            where s.StatusCode == lineStatus
                                            select s.Id).FirstOrDefault();
                        lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                      ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                        string sapMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                    ? "Waiting for SAP response " : responseModel[i].Message;
                        //UpdateScannedShipMentDetails
                        var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                        scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; a.SAPBinStatus = lineStatusId; a.SAPBinSyncMessage = sapMessage; });

                        lineContent.BatchID = batchId;
                        lineContent.LineId = lineId;
                        lineContent.StatusId = lineStatusId;
                        //lineContent.SapResponseMessage = responseModel[i].Message;
                        lineContent.SapResponseMessage = sapMessage;
                        lineContent.ScannedDataLineId = Id; //Ids.FirstOrDefault();
                        context.Trn_SSCTransLineContentNew.Add(lineContent);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("InsertSSCTransLineContent -" + e); 
            }
            return failedCount;
        }

        private int UpdateSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel, int Id, List<int> updateIds,bool isTermTrip)
        {
            string lineStatus = null;
            int failedCount = 0;
            try
            {                
                using (var context = new GoodpackEDIEntities())
                {
                    for (int i = 0; i < responseModel.Count; i++)
                    {
                        Trn_SSCTransLineContentNew lineContent = context.Trn_SSCTransLineContentNew.FirstOrDefault(j => j.LineId == lineId && j.BatchID == batchId);
                        bool responseStatus=Convert.ToBoolean(responseModel[i].Success);
                        if (responseStatus)
                        {
                            lineStatus = LineStatus.SUCCESS.ToString();
                        }
                        else
                        {
                            failedCount++;
                            lineStatus = LineStatus.ERROR.ToString();
                        }

                        log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                        int lineStatusId = (from s in context.Trn_LineStatus
                                            where s.StatusCode == lineStatus
                                            select s.Id).FirstOrDefault();
                        string sapMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                    ? "Waiting for SAP response " : responseModel[i].Message;
                        lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                        ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                        var scannedData = context.Trn_ScannedShipmentDetails.Where(j => updateIds.Contains(j.Id)).ToList();
                        scannedData.ForEach(a => { a.BatchId = batchId; a.Status = lineStatusId; a.SAPBinStatus = lineStatusId; a.SAPBinSyncMessage = sapMessage; });
                        lineContent.StatusId = lineStatusId;
                        //lineContent.SapResponseMessage = responseModel[i].Message;
                        lineContent.SapResponseMessage = sapMessage;
                        context.SaveChanges();
                    }
                }
            }
            catch(Exception e)
            {
                log.Error("UpdateSSCTransLineContent -" + e);
            }
            return failedCount;
        }

        private void UpdateSSCTransLine(List<SScTOScConvertionModel> listSSC, int BatchId, List<int> Ids)
        {
            List<SScTOScConvertionModel> successList = listSSC.Where(i => i.ErrorMessage == null).ToList();
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    listSSC.ForEach(i => i.TransLineId = (context.Trn_SSCTransLineNew.Where(j => j.LineNumber == i.LineId).Select(k => k.Id).FirstOrDefault()));
                    SubmitToSAPandUpdateSSCTransBatch(listSSC, BatchId, Ids, true);                   
                }
            }
            catch (Exception e)
            {
                log.Error("UpdateSSCTransLine error -" + e);
                
            }
        }

        private void UpdateSSCTransLineStatus(int lineId, List<ResponseModel> responseModel, bool isTermTrip)
        {
            try
            {
                string lineStatus = null;
                string itrNumber = null;
                int lineStatusId = -1;
                using (var context = new GoodpackEDIEntities())
                {
                    var datatoUpdate = context.Trn_SSCTransLineNew.Where(i => i.LineNumber == lineId).ToList();
                    for (int i = 0; i < responseModel.Count; i++)
                    {
                        bool responseStatus = Convert.ToBoolean(responseModel[i].Success);
                        if (responseStatus)
                        {                        
                            lineStatus = LineStatus.SUCCESS.ToString();
                        }
                        else
                        {

                            lineStatus = LineStatus.ERROR.ToString();
                        }
                        if (responseModel[i].ResponseItems != null)
                            itrNumber = responseModel[i].ResponseItems.ToString();
                        else
                            itrNumber = "10";
                        log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                        lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                        lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                      ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                    }
                    datatoUpdate.ForEach(a => { a.StatusId = lineStatusId; a.InTestMode = false; a.ItrNumber = itrNumber; });
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                log.Error("UpdateSSCTransLineStatus " + e);
            }
        }

        private List<SScTOScConvertionModel> ValidateDeviceData(List<SScTOScConvertionModel> listSSCToScModel, List<int> Ids)
        {
            int lookupNameId;
            int subsiId;
            DeviceDetail customerDetails;
            IDecodeValueManager decodeValueManager = new DecodeValueManager();
            IMapperManager mapperManager = new MapperManager();
            List<SScTOScConvertionModel> updatedSSCList = new List<SScTOScConvertionModel>();
            try
            {         
                //IList<EditMapperHeaderModel> mapperHeaderModel = mapperManager.GetMappedFileDetailsForEdit(3212);
                foreach (var data in listSSCToScModel)
                {
                    SScTOScConvertionModel newSSCModel = new SScTOScConvertionModel();
                    using (var context = new GoodpackEDIEntities())
                    {
                        customerDetails = (from s in context.Trn_DeviceDetails
                                           where s.DeviceID == data.DeviceId.Trim()
                                           select new DeviceDetail { MapperName = s.MapperName.Trim(), CustomerCode = s.CustomerCode, Subsy = s.SubsiId.Trim() }).FirstOrDefault();
                        int subsiVlaue = int.Parse(customerDetails.Subsy);
                        subsiId = (from s in context.Gen_EDISubsies
                                   where s.SubsiCode == subsiVlaue
                                   select s.Id).FirstOrDefault();
                    }

                    lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                    string customerValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data.CustomerName, customerDetails.MapperName, subsiId, lookupNameId);
                    if (customerValue != null)
                    {
                        if (customerValue != String.Empty)
                            newSSCModel.CustomerName = customerValue;
                        else
                            newSSCModel.CustomerName = data.CustomerName;
                    }
                    else
                    {
                        newSSCModel.CustomerName = data.CustomerName;
                        newSSCModel.ErrorMessage = newSSCModel.ErrorMessage + "Customer Name does not have decode value ,";
                    }

                    lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                    string consigneeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data.Consignee, customerDetails.MapperName, subsiId, lookupNameId);
                    if (consigneeValue != null)
                    {
                        if (consigneeValue != String.Empty)
                            newSSCModel.Consignee = consigneeValue;
                        else
                            newSSCModel.Consignee = data.Consignee;
                    }
                    else
                    {
                        newSSCModel.Consignee = data.Consignee;
                        newSSCModel.ErrorMessage = newSSCModel.ErrorMessage + " Consignee does not have decode value ,";
                    }
                    //For ASN Checking Consignee insted of packer
                    if (_TransactionType == "ASN")
                    {
                        lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                        string packerValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data.PackingPlant, customerDetails.MapperName, subsiId, lookupNameId);
                        if (packerValue != null)
                        {
                            if (packerValue != String.Empty)
                                newSSCModel.PackingPlant = packerValue;
                            else
                                newSSCModel.PackingPlant = data.PackingPlant;
                        }
                        else
                        {
                            newSSCModel.PackingPlant = data.PackingPlant;
                            newSSCModel.ErrorMessage = newSSCModel.ErrorMessage + " PackingPlant does not have decode value ,";
                        }
                    }
                    else
                    {
                        lookupNameId = GetLookupId(ConstantUtilities.Packer);
                        string packerValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(data.PackingPlant, customerDetails.MapperName, subsiId, lookupNameId);
                        if (packerValue != null)
                        {
                            if (packerValue != String.Empty)
                                newSSCModel.PackingPlant = packerValue;
                            else
                                newSSCModel.PackingPlant = data.PackingPlant;
                        }
                        else
                        {
                            newSSCModel.PackingPlant = data.PackingPlant;
                            newSSCModel.ErrorMessage = newSSCModel.ErrorMessage + " PackingPlant does not have decode value ,";
                        }
                    }
                    newSSCModel.SubsiId = subsiId;
                    newSSCModel.BinType = data.BinType;
                    newSSCModel.ETA = data.ETA;
                    newSSCModel.ETD = data.ETD;
                    newSSCModel.Quantity = data.Quantity;
                    newSSCModel.Remark = data.Remark;
                    newSSCModel.SInumber = data.SInumber;
                    newSSCModel.DeviceId = data.DeviceId;
                    newSSCModel.CustomerReferenceNumber = data.CustomerReferenceNumber;
                    newSSCModel.LineId = data.LineId;
                    newSSCModel.MapperName = customerDetails.MapperName;
                    updatedSSCList.Add(newSSCModel);
                }
            }
            catch(Exception e)
            {
                log.Error("ValidateDeviceData "+e);
            }
            return updatedSSCList;
        }       

        private int GetLookupId(string LookupName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames
                        where s.LookupName == LookupName
                        select s.Id).FirstOrDefault();

            }
        }

        private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
        {
            IValidationTypeManger validator = new ValidationTypeManager();
            Dictionary<string, string> validatorParams = new Dictionary<string, string>();
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
            validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, "3");
            switch (inputType)
            {
                case GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator:
                    return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);

                default:
                    return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
            }
        }

        private Dictionary<string, string> CreateSCSAPDictionary(SScTOScConvertionModel list)
        {
            Dictionary<string, string> scData = new Dictionary<string, string>();
            scData.Add(SCSapMessageFields.DocumentType,(_TransactionType == "ASN")? "ASN":"SHCN");
            scData.Add(SCSapMessageFields.ITRNumber, "10");
            scData.Add(SCSapMessageFields.ReferenceITRNumber, "");
            scData.Add(SCSapMessageFields.FromLocation, list.PackingPlant);
            scData.Add(SCSapMessageFields.ToLocation, list.Consignee);
            scData.Add(SCSapMessageFields.CustomerReferenceNumber, list.CustomerReferenceNumber);
            scData.Add(SCSapMessageFields.ItemNo, "10");
            scData.Add(SCSapMessageFields.MaterialNumber, list.BinType);
            scData.Add(SCSapMessageFields.Quantity, list.Quantity.ToString());
            scData.Add(SCSapMessageFields.ContainerNumber, "");
            scData.Add(SCSapMessageFields.CustomerNumber, list.CustomerName);
            scData.Add(SCSapMessageFields.SINumber, list.SInumber);
            scData.Add(SCSapMessageFields.SalesDocument, "");
            scData.Add(SCSapMessageFields.ETDDate, ParseDate(list.ETD));     
            scData.Add(SCSapMessageFields.DTADate,ParseDate(list.ETA));
            scData.Add(SCSapMessageFields.Remarks, list.Remark);
            return scData;
        }

        private void UpdateSSCTransBatchAndSendEmail(int batchId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SSCTransBatch transBatch = context.Trn_SSCTransBatch.FirstOrDefault(i => i.Id == batchId);
                if (transBatch.RecordCountTotal == transBatch.RecordCountSuccess)
                { //Email Sending Section
                    try
                    {
                        var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                        where s.BatchId == transBatch.Id
                                        select s.DeviceId).FirstOrDefault();
                        var data = (from s in context.Trn_DeviceDetails
                                    where s.DeviceID == deviceId
                                    select new { s.CustomerCode, s.CustomerEmail, s.SubsiId, s.MapperName, s.UserId }).FirstOrDefault();
                        var cusromerName = (from s in context.Gen_EDIEntities
                                            where s.EntityCode == data.CustomerCode
                                            select s.EntityName).FirstOrDefault();
                        var userName = (from s in context.Gen_User
                                        where s.Id == data.UserId
                                        select s.Username).FirstOrDefault();
                        string[] customerEmail = new string[] { data.CustomerEmail };
                        Dictionary<string, string> placeholders = new Dictionary<string, string>();
                        placeholders.Add("$$CustomerName$$", cusromerName.ToString());
                        placeholders.Add("$$BatchID$$", batchId.ToString());
                        placeholders.Add("$$UserName$$", userName.ToString());
                        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                        placeholders.Add("$$EmailID$$", data.CustomerEmail);
                        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(int.Parse(data.SubsiId)));

                        GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", null);
                        GPTools.SendEmail(placeholders, getAllRecipients(data.MapperName), "NOTIFY_SC_EMAIL", null);
                    }
                    catch (Exception e)
                    {
                        log.Error("UpdateSSCTransBatchAndSendEmail ,error email Sending " + e);
                    }
                }
                else
                {
                    try
                    {
                        var deviceId = (from s in context.Trn_ScannedShipmentDetails
                                        where s.BatchId == transBatch.Id
                                        select s.DeviceId).FirstOrDefault();
                        var data = (from s in context.Trn_DeviceDetails
                                    where s.DeviceID == deviceId
                                    select new { s.CustomerCode, s.CustomerEmail, s.SubsiId, s.MapperName, s.UserId }).FirstOrDefault();
                        var cusromerName = (from s in context.Gen_EDIEntities
                                            where s.EntityCode == data.CustomerCode
                                            select s.EntityName).FirstOrDefault();
                        var userName = (from s in context.Gen_User
                                        where s.Id == data.UserId
                                        select s.Username).FirstOrDefault();
                        string[] customerEmail = new string[] { data.CustomerEmail };
                        Dictionary<string, string> placeholders = new Dictionary<string, string>();
                        placeholders.Add("$$CustomerName$$", cusromerName.ToString());
                        placeholders.Add("$$BatchID$$", batchId.ToString());
                        placeholders.Add("$$UserName$$", userName.ToString());
                        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                        placeholders.Add("$$EmailID$$", data.CustomerEmail);
                        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(int.Parse(data.SubsiId)));

                        GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", null);
                        GPTools.SendEmail(placeholders, getAllRecipients(data.MapperName), "NOTIFY_SC_EMAIL", null);
                    }
                    catch (Exception e)
                    {
                        log.Error("UpdateSSCTransBatchAndSendEmail ,error email Sending " + e);
                    }
                }
                context.SaveChanges();
            }
        }
        private string ParseDate(string dateValues)
        {
            dateValues = dateValues.Replace('.', '-').Replace('/', '-');
            DateTime parsedDate;
            if (DateTime.TryParseExact(dateValues, "dd-MM-yyyy", null, DateTimeStyles.None, out parsedDate))
            {

            }
            else if (DateTime.TryParseExact(dateValues, "dd-MMM-yyyy", null, DateTimeStyles.None, out parsedDate))
            {

            }
            else
            {
                string[] date = dateValues.Split('-');
                parsedDate = new DateTime(int.Parse(date[2]), int.Parse(date[1]), int.Parse(date[0]));
            }
            return parsedDate.ToString("ddMMyyyy");
        }

        private string getSubsyNameFromId(int SubsiId)
        {
            string SubsiName;
            using (var context = new GoodpackEDIEntities())
            {
                SubsiName = (from s in context.Gen_EDISubsies
                             where s.SubsiCode == SubsiId
                             select s.SubsiName).FirstOrDefault();
            }
            return SubsiName;


        }
        private string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();
            //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }
                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }
        }
        private static string WriteDataTable(DataTable sourceTable)
        {
            StringBuilder s = new StringBuilder();
            List<string> headerValues = new List<string>();
            foreach (DataColumn column in sourceTable.Columns)
            {
                headerValues.Add(QuoteValue(column.ColumnName));
            }
            s.Append(String.Join(",", headerValues.ToArray()) + Environment.NewLine);
            string[] items = null;
            foreach (DataRow row in sourceTable.Rows)
            {
                items = row.ItemArray.Select(o => QuoteValue(o.ToString())).ToArray();
                s.Append(String.Join(",", items) + Environment.NewLine);
            }
            return s.ToString();
        }
        private static string QuoteValue(string value)
        {
            return String.Concat("\"", value.Replace("\"", "\"\""), "\"");
        }
        private static DataTable ConvertToDatatable<T>(List<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    table.Columns.Add(prop.Name, prop.PropertyType.GetGenericArguments()[0]);
                else
                    table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
       
        #endregion
    }
}