﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Tools
{
    public class FilePolling
    {
        private IList<string> FolderPaths;
        private string FolderPath;
        private IList<string> fileExtns;
        // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public FilePolling(IList<string> objFolderPaths, IList<string> fileExtensions)
        {
            FolderPaths = objFolderPaths;
            fileExtns = fileExtensions;
        }
        public FilePolling(string objFolderPath, IList<string> fileExtensions)
        {
            FolderPath = objFolderPath;
            fileExtns = fileExtensions;
        }

        public FileInfo[] GetFiles()
        {
            DirectoryInfo objDirectoryInfo;
            List<FileInfo> objlstFileInfo = new List<FileInfo>();
            foreach (string strFolderPath in FolderPaths)
            {
                objDirectoryInfo = new DirectoryInfo(strFolderPath);
                objDirectoryInfo.Refresh();
                if (objDirectoryInfo.Exists)
                {
                    foreach (string strFileExtention in fileExtns)
                    {
                        foreach (FileInfo objFile in objDirectoryInfo.GetFiles("*" + strFileExtention, SearchOption.AllDirectories))
                        {
                            //added below condition to avoid issue with .xls, .xlsx file formats
                            if (objFile.Name.ToLower().EndsWith(strFileExtention.ToLower()))
                                objlstFileInfo.Add(objFile);
                        }
                    }
                }
                else
                {
                    // log4net.GlobalContext.Properties["FileName"] = strFolderPath;
                    //   objILog.Warn("File poller cannot access path " + strFolderPath);
                }
            }
            return objlstFileInfo.ToArray();
        }



        public FileInfo[] GetFilesOfParentDirectoryOnly()
        {
            DirectoryInfo objDirectoryInfo;
            List<FileInfo> objlstFileInfo = new List<FileInfo>();
            //foreach (string strFolderPath in FolderPath)
            //{
            objDirectoryInfo = new DirectoryInfo(FolderPath);
            if (objDirectoryInfo.Exists)
            {
                foreach (string strFileExtention in fileExtns)
                {
                    foreach (FileInfo objFile in objDirectoryInfo.GetFiles("*" + strFileExtention, SearchOption.TopDirectoryOnly))
                    {
                        //added below condition to avoid issue with .xls, .xlsx file formats
                        if (objFile.Name.ToLower().EndsWith(strFileExtention.ToLower()))
                            objlstFileInfo.Add(objFile);
                    }
                }
            }
            else
            {
                //log4net.GlobalContext.Properties["FileName"] = strFolderPath;
                //objILog.Warn("File poller cannot access path " + strFolderPath);
            }
            //}
            return objlstFileInfo.ToArray();
        }


    }
}