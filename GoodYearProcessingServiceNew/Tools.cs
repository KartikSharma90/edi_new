﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GoodyearProcessingService
{
    public  class Tools
    {
        /// <summary>
        ///  Updates TRANS_PREPROCESSING table
        /// </summary>
        /// <param name="objGPCustomerMessage"></param>

       internal static void UpdatePreprocessingTransaction(GPCustomerMessage objGPCustomerMessage)
        {
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                   Trn_GoodyearPreprocessing Preprocessor = new Trn_GoodyearPreprocessing();
                   Preprocessor.BatchId = objGPCustomerMessage.BatchID;
                   Preprocessor.StatusCode = objGPCustomerMessage.StatusCode.ToString();
                   Preprocessor.ErrorCode = objGPCustomerMessage.ErrorCode.ToString();
                   Preprocessor.BatchErrorDescription = objGPCustomerMessage.ErrorDesc;
                   Preprocessor.TransactionType = objGPCustomerMessage.TransactionType.ToString();
                   Preprocessor.BatchFilename= objGPCustomerMessage.BatchFileName;
                   context.SaveChanges();
                }
                ProcessingService.log.Info("Entered in method UpdatepreprocessingTransaction().");
            }
            catch(Exception ex)
            {
                ProcessingService.log.Error("Error in method UpdatePreprocessingTransaction(). ", ex);
            }

        }
       /// <summary>
       ///  Updates TRANS_PREPROCESSING table
       /// </summary>
       /// <param name="sourceMsgLines"></param>

       internal static void InsertGoodyearRawFileData(IList<GPArchitecture.CommonTypes.SourceMessage> sourceMsgLines, string fileName)
       {
           try
           {
               using (var context = new GoodpackEDIEntities())
               {
                   Trn_GoodyearFileContent FileContent = new Trn_GoodyearFileContent();
                   for (int i = 0; i < sourceMsgLines.Count(); i++)
                   {
                       FileContent.CustPO = sourceMsgLines[i].Fields[0].Value;
                       FileContent.SONumber = sourceMsgLines[i].Fields[1].Value;
                       FileContent.SINumber = sourceMsgLines[i].Fields[2].Value;
                       FileContent.Ver = sourceMsgLines[i].Fields[3].Value;
                       FileContent.SIStatus = sourceMsgLines[i].Fields[4].Value;
                       FileContent.SIQtyinUnits = sourceMsgLines[i].Fields[5].Value;
                       FileContent.RubberGrade = sourceMsgLines[i].Fields[6].Value;
                       FileContent.Supplier = sourceMsgLines[i].Fields[7].Value;
                       FileContent.FactoryCode = sourceMsgLines[i].Fields[8].Value;
                       FileContent.PackagingType = sourceMsgLines[i].Fields[9].Value;
                       FileContent.LoadPort = sourceMsgLines[i].Fields[10].Value;
                       FileContent.LoadDate = sourceMsgLines[i].Fields[11].Value;
                       FileContent.ShippingLine = sourceMsgLines[i].Fields[12].Value;
                       FileContent.VesselName = sourceMsgLines[i].Fields[13].Value;
                       FileContent.VoyageNumber = sourceMsgLines[i].Fields[14].Value;
                       FileContent.BOLNumber = sourceMsgLines[i].Fields[15].Value;
                       FileContent.BOLQty = sourceMsgLines[i].Fields[16].Value;
                       FileContent.ContainerNumber = sourceMsgLines[i].Fields[17].Value;
                       FileContent.ContainerQty = sourceMsgLines[i].Fields[18].Value;
                       FileContent.OceanETD = sourceMsgLines[i].Fields[19].Value;
                       FileContent.ArrivalPort = sourceMsgLines[i].Fields[20].Value;
                       FileContent.ArrivalDate = sourceMsgLines[i].Fields[21].Value;
                       FileContent.CustomerCode = sourceMsgLines[i].Fields[22].Value;
                       FileContent.CustomerName = sourceMsgLines[i].Fields[23].Value;
                       FileContent.BookingNumber = sourceMsgLines[i].Fields[24].Value;
                       FileContent.EndOfRecord = sourceMsgLines[i].Fields[25].Value;
                       FileContent.FileName = fileName;
                       context.Trn_GoodyearFileContent.Add(FileContent);
                       context.SaveChanges();
                   }
               }
               ProcessingService.log.Info("Entered in method InsertGoodyearRawFileData().");
           }
           catch (Exception ex)
           {
               ProcessingService.log.Error("Error in method InsertGoodyearRawFileData(). ", ex);
           }

       }
        /// <summary>
        /// Updates TRANS_PREPROCESSING table
        /// </summary>
        ///<param name="objGPCustomerMessage"></param>
      
      internal static int InsertPreprocessingTransaction(GPCustomerMessage objGPCustomerMessage)
      {
          ProcessingService.log.Info("Entered in method InsertpreprocessingTransaction().");
          int processorId = 0;
          try{
            using (var context = new GoodpackEDIEntities())
                {
                  
                   Trn_GoodyearPreprocessing Preprocessor = new Trn_GoodyearPreprocessing();
                   Preprocessor.SubsiName= objGPCustomerMessage.SubsiName;
                   Preprocessor.TransactionType = objGPCustomerMessage.TransactionType.ToString();
                   Preprocessor.StatusCode = objGPCustomerMessage.StatusCode.ToString();
                   Preprocessor.ErrorCode = objGPCustomerMessage.ErrorCode.ToString();
                   Preprocessor.BatchErrorDescription= objGPCustomerMessage.ErrorDesc;                
                   Preprocessor.BatchFilename = objGPCustomerMessage.BatchFileName;
                   Preprocessor.BatchFileSource = objGPCustomerMessage.BatchFileSourceType.ToString();
                   Preprocessor.DateCreated = objGPCustomerMessage.DateCreated;
                   context.Trn_GoodyearPreprocessing.Add(Preprocessor);
                   context.SaveChanges();
                   processorId = Preprocessor.BatchId;
                }
           
              
            }
            catch(Exception ex)
            {
                ProcessingService.log.Error("Error in method InsertPreprocessingTransaction(). ", ex);
            }
          return processorId;
      }

    }
}
