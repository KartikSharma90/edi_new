﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Resources;
using System.Configuration;
using System.Data;
//using GPArchitecture.CommonTypes;
using GPArchitecture.Cache;
using System.Text.RegularExpressions;
using log4net;
using GPArchitecture.Utilities;
using GPArchitecture.EnumsAndConstants;


namespace GoodyearProcessingService
{
    public abstract class InboundEndPointPreProcessor
    {
        protected IList<GPCustomerMessage> objLstGPCustomerMessage;
        private static readonly ILog log = LogManager.GetLogger(typeof(InboundEndPointPreProcessor));
        // private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public virtual void Initialize()
        {
            //  DBConnection.InitializeSQLConn(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
        }

        protected abstract void Process();

        protected virtual bool Validate(GPCustomerMessage objGPCustomerMessage)
        {
            log.Debug("Entering Validate method");

            // Init vars
            bool blnValidFileType = true;

            // Perform validation
            blnValidFileType &= this.Validate_IsFileTypeSupported(objGPCustomerMessage);

            log.Debug("Leaving Validate method");
            return blnValidFileType;
        }

        protected virtual void GetMessageDetails(GPCustomerMessage objGPCustomerMessage)
        {
            log.Debug("Entering GetMessageDetails method");
            string[] strFileName = objGPCustomerMessage.BatchFileName.Split('.');
            if (strFileName.LongLength > 1 && strFileName[0].Length > 1)
            {
                int ver = 0;
                int.TryParse(strFileName[0].Substring(strFileName[0].Length - 2), out ver);
                objGPCustomerMessage.BatchFileVersion = ver;
            }
            log.Debug("Leaving GetMessageDetails method");
        }

        /// <summary>
        /// Validates whether the file extension of the file received from customer is valid or not.
        /// </summary>
        /// <param name="objGPCustomerMessage"></param>
        /// <returns></returns>
        private bool Validate_IsFileTypeSupported(GPCustomerMessage objGPCustomerMessage)
        {
            log.Debug("Entering Validate_IsFileTypeSupported method");
            bool blnValidFileType = false;
            try
            {
                FILE_TYPES fileType;
                blnValidFileType = SupportedFileTypes.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                if (blnValidFileType == false)
                {
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    log.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                }
            }
            catch (Exception exp)
            {
                log.Error("Error while Validating the file", exp);
                throw exp;
            }
            log.Debug("Leaving Validate_IsFileTypeSupported method");
            return blnValidFileType;
        }
    }
}
