﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.IO;
using System.Reflection;
using log4net;
using System.Timers;
using GPArchitecture.Cache;
//using GPArchitecture.CommonTypes;
using GPArchitecture.Parsers;
using GPArchitecture.Tools;
using GPArchitecture.Accessors;
using System.Threading;
using System.Text.RegularExpressions;
using GoodyearProcessingService.PreprocessingLogic;
using GPArchitecture.Utilities;
using GPArchitecture.EnumsAndConstants;

namespace GoodyearProcessingService
{
    public class FileReceiverEndPointPreProcessor : InboundEndPointPreProcessor
    {
        public bool isProcessing = false; // public get property
        string path;
        FilePolling objFilePolling;
        InboundDataAccessFactoryPreProcess objDataAccess;
        //public System.Timers.Timer processTimer;
        private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Constructor
        /// <summary>
        /// Used to initalize DBConenction with conenction string 
        /// </summary>
        public FileReceiverEndPointPreProcessor()
        {
           
        }
        #endregion

        #region Initialize()
        /// <summary>
        /// Used to get the path from Configuration File Source path table and polling
        /// </summary>
        public override void Initialize()
        {
            objILog.Debug("Entering Preprocessor Service Initialize method");
            //TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("RcvFilePollingInterval")));
            ////new TimeSpan(0, 0, 30);
            //this.processTimer.Interval = objTimeInterval.TotalMilliseconds;
            //this.processTimer.Elapsed += new System.Timers.ElapsedEventHandler(processTimer_Elapsed);
            //this.processTimer.Enabled = true;
            //this.processTimer.Start();
            StartEvent();
            isProcessing = false;
            objILog.Debug("Leaving Preprocessor Service Initialize method");
        }
        #endregion

        //void processTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    objILog.Debug("Entering processTimer_Elapsed event");
        //    objILog.Debug("Value of isProcessing :" + isProcessing);
        //    if (!isProcessing)
        //    {
        //        objILog.Debug("Entering method Process");
        //        Process();
        //    }
        //    objILog.Debug("Leaving processTimer_Elapsed event");
        //}
        void StartEvent()
        {
            objILog.Debug("Entering processTimer_Elapsed event");
            objILog.Debug("Value of isProcessing :" + isProcessing);
            if (!isProcessing)
            {
                objILog.Debug("Entering method Process");
                Process();
            }
            objILog.Debug("Leaving processTimer_Elapsed event");
        }


        override protected void Process()
        {
            objILog.Debug("Entered method Process");
            isProcessing = true;
            path = GPArchitecture.Cache.Config.getConfig("PREPROCESSING_IN");
            objILog.Debug("Path:" + path);
            GPCustomerMessage objGPCustomerMessage = new GPCustomerMessage();
            GPArchitecture.CommonTypes.SourceMessage objSourceMessage;
            try
            {
                objILog.Debug("Inside try");
                objFilePolling = new FilePolling(path, SupportedFileTypes.GetSupportedFileTypes());
                FileInfo[] objFileInfos = objFilePolling.GetFilesOfParentDirectoryOnly();
                //For each file, loop thro this
                bool isValidFile;
                bool isParseSuccess = false;
                bool foundPreprocessing = false;
                bool isPreprocessingSuccessful = false;
                string errorDesc = string.Empty;
                IList<GPArchitecture.CommonTypes.SourceMessage> sourceMessageLines;

                objILog.Debug("Inside try");
                foreach (FileInfo objFileinfo in objFileInfos)
                {
                    objILog.Debug("Inside foreach");
                    // Init vars
                    objSourceMessage = null;
                    objGPCustomerMessage = new GPCustomerMessage();
                    objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                    objGPCustomerMessage.ErrorDesc = null;
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    objGPCustomerMessage.BatchFileSourceAddress = objFileinfo.Directory.FullName;
                    objGPCustomerMessage.BatchFileName =objFileinfo.Name;
                    objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                    objGPCustomerMessage.DateCreated = DateTime.Now;


                    // Validate
                    isValidFile = this.Validate(objGPCustomerMessage);

                    // Get message details
                    this.GetMessageDetails(objGPCustomerMessage);
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromSubsiName(objGPCustomerMessage.SubsiName);

                    // Validate that customer head company is set
                    if (string.IsNullOrEmpty(objGPCustomerMessage.SubsiName))
                    {
                        objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;
                        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                        objGPCustomerMessage.ErrorDesc = "Cannot identify the source subsi Id of file " + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
                    }

                    // Save transaction into TRANS_PREPROCESSING table
                    objGPCustomerMessage.BatchID = Tools.InsertPreprocessingTransaction(objGPCustomerMessage);



                    try
                    {
                        // Get Source Message Specs
                        objSourceMessage = GPTools.LoadSourceMessageSpecs(objGPCustomerMessage.SubsiId, objGPCustomerMessage.TransactionType, "Pre-processing");

                        // Validate source message
                        if (objSourceMessage == null)
                        {
                            string errMessage = string.Format(
                                    "Cannot find source message specification for batch ID {0}, customer {1} and transaction type {2}",
                                    objGPCustomerMessage.BatchID.ToString(),
                                    objGPCustomerMessage.SubsiId,
                                    objGPCustomerMessage.TransactionType.ToString());

                            objILog.Warn(errMessage);

                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.V_SRC_SPEC;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorDesc = errMessage;

                            //Update TRANS_PREPROCESSING table
                            Tools.UpdatePreprocessingTransaction(objGPCustomerMessage);

                            // Continue to next file
                            continue;
                        }

                        // Attempt to parse the customer request
                        sourceMessageLines = GPTools.Parse(objGPCustomerMessage, objSourceMessage, ref isParseSuccess, ref errorDesc);

                        //Insert Goodyear File raw data in to DB
                        Tools.InsertGoodyearRawFileData(sourceMessageLines, objGPCustomerMessage.BatchFileName);
                        //

                        if (!isParseSuccess)
                        {
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.V_PARSE_REQ;
                            objGPCustomerMessage.ErrorDesc = errorDesc;
                            string errMessage = string.Format(
                                    "Parsing failed for batch ID (0), customer head company {1} and transaction type {2}. {3}",
                                    objGPCustomerMessage.BatchID.ToString(),
                                    objGPCustomerMessage.SubsiId,
                                    objGPCustomerMessage.TransactionType.ToString(),
                                    errorDesc
                                );
                            objILog.Warn(errMessage);

                            // Update TRANS_PREPROCESSING table
                            Tools.UpdatePreprocessingTransaction(objGPCustomerMessage);

                            // Continue to next file
                            continue;
                        } //-- End parsing

                        // Execute Pre-processing based on Subsi Name

                        if (GoodyearFieldModel.GoodyearSubsiName == objGPCustomerMessage.SubsiName)
                        {
                            foundPreprocessing = true;
                            GoodyearCorporation goodyearCorpPreprocessing = new GoodyearCorporation();
                            isPreprocessingSuccessful = goodyearCorpPreprocessing.PreprocessGoodyear(objGPCustomerMessage, sourceMessageLines);
                        }


                        // Check if pre-processing action was found for request message
                        if (!foundPreprocessing)
                        {
                            string errMessage = string.Format(
                                    "No pre-processing action found for batch ID (0), customer head company {1} and transaction type {2}. ",
                                    objGPCustomerMessage.BatchID.ToString(),
                                    objGPCustomerMessage.SubsiId,
                                    objGPCustomerMessage.TransactionType.ToString()
                                );

                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.V_NO_PREPROCESSING;
                            objGPCustomerMessage.ErrorDesc = errMessage;
                            objILog.Warn(errMessage);

                            // Update TRANS_PREPROCESSING table
                            Tools.UpdatePreprocessingTransaction(objGPCustomerMessage);

                            // Continue to next file
                            continue;
                        }

                        // Mark as successful
                        objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                        objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                        objGPCustomerMessage.ErrorDesc = string.Empty;

                        // Update TRANS_PREPROCESSING table
                        Tools.UpdatePreprocessingTransaction(objGPCustomerMessage);

                    }
                    catch (System.Exception ex)
                    {
                        objILog.Error("Error encountered during pre-processing", ex);
                        //  GPTools.ProcessErrorAction(ProcessingService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_GEN, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), ex.Message);
                        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                        objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                        objGPCustomerMessage.ErrorDesc = "Error encountered during pre-processing. " + ex.Message;

                        // Update TRANS_PREPROCESSING table
                        Tools.UpdatePreprocessingTransaction(objGPCustomerMessage);
                    }
                    finally
                    {
                        //copy file to Archive location before deleting
                        string preprocessingArchivepath;
                        preprocessingArchivepath = GPArchitecture.Cache.Config.getConfig("PREPRO_ARCHIVE");
                        string strSourcePath = objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
                        string destinationPath = preprocessingArchivepath + "\\" + objGPCustomerMessage.BatchFileName;
                        this.CopySourceFile(strSourcePath, destinationPath);

                        // Delete source file
                        this.ClearSourceFile(strSourcePath);

                    }//-- end try catch block                  

                }//-- End foreach file
            }
            catch (Exception exp)
            {
                // Set batch file name
                if (string.IsNullOrEmpty(objGPCustomerMessage.BatchFileName))
                    objGPCustomerMessage.BatchFileName = string.Empty;

                log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
                log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();

                // Send notification email
                //         GPTools.ProcessErrorAction(ProcessingService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_GEN, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, DateTime.Now.ToShortDateString(), "Error encountered in FileReceiverEndPoint of Goodpack Facade Service. " + exp.Message);

                objILog.Error("Error while processing FileReceiverEndPoint", exp);
            }
            finally
            {
                isProcessing = false;
            }
        }

        protected override void GetMessageDetails(GPCustomerMessage objGPCustomerMessage)
        {
            base.GetMessageDetails(objGPCustomerMessage);
            KeyValuePair<TRANSACTION_TYPES, string> objCustTran = new KeyValuePair<TRANSACTION_TYPES, string>(TRANSACTION_TYPES.SO, "GoodyearCorporation");


            if (objCustTran.Value != null)
            {
                objGPCustomerMessage.TransactionType = objCustTran.Key;
                objGPCustomerMessage.SubsiName = objCustTran.Value;
                objGPCustomerMessage.CustomerCode = 0;
                objGPCustomerMessage.CustomerName = string.Empty;
            }
            else
            {
                log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileSourceAddress;
                objILog.Debug("FileReceiver End point: Source Head Company is not identified ");
                objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;
                // GPTools.ProcessErrorAction(ProcessingService.Service, objGPCustomerMessage.BatchID, ErrorCodes.V_CUST, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, DateTime.Now.ToShortDateString(), "File received from Unknown Source");
            }
        }


        protected override bool Validate(GPCustomerMessage objGPCustomerMessage)
        {
            bool isValidFile = false;
            isValidFile = base.Validate(objGPCustomerMessage);
            bool isFileLock = false;
          
            if (isValidFile)
            {
                // Validate if file exists still
                FileInfo objFileInfo = new FileInfo(objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName);
                isFileLock = IsFileLocked(objFileInfo);
                if (!isFileLock)
                {
                    if (!objFileInfo.Exists)
                    {
                        isValidFile = false;
                        objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                        objGPCustomerMessage.ErrorDesc = "Cannot find source file " + objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
                    }
                    else
                    {
                        try
                        {
                            //Create the Message content separately for Excel files
                            if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                            {
                                objGPCustomerMessage.Message = GPArchitecture.Parsers.ExcelParser.ExcelToCSV(objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName);
                            }
                            else
                            {
                                using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                                {
                                    objGPCustomerMessage.Message = objStreamReader.ReadToEnd();
                                    objStreamReader.Close();
                                    objStreamReader.Dispose();
                                }
                            }
                        }
                        catch (Exception exp)
                        {
                            isValidFile = false;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objILog.Info("File is not in correct format", exp);
                        }
                    }
                }
                else
                {
                    Thread.Sleep(3000);
                    this.Validate(objGPCustomerMessage);
                }

            }
            return isValidFile;
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        protected void ClearSourceFile(string strSourcePath)
        {
           // string strSourcePath = objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
            try
            {
                File.Delete(strSourcePath);
            }
            catch (Exception exp)
            {
                objILog.Error("Error deleting customer file " + strSourcePath, exp);
            }
        }

        protected void CopySourceFile( string strSourcePath,string destinationPath)
        {
            // string preprocessingArchivepath;
            // preprocessingArchivepath = GPArchitecture.Cache.Config.getConfig("PREPRO_ARCHIVE");
            //string strSourcePath = objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
            //string destinationPath = archivePath + "\\" + objGPCustomerMessage.BatchFileName;
            try
            {
                //DirectoryInfo objDirectoryInfo = new DirectoryInfo(destinationPath);
                //if (!objDirectoryInfo.Exists)
                //    //need to gptools                
                //    GPTools.createFolder(objDirectoryInfo.FullName);
                if (File.Exists(destinationPath))
                    File.Delete(destinationPath);
                File.Copy(strSourcePath,destinationPath);
            }
            catch (Exception exp)
            {
                objILog.Error("Error copying customer file " + strSourcePath, exp);
            }
        }
    }
}
