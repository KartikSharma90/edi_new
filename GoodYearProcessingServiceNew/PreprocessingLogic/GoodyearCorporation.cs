﻿using GPArchitecture.CommonTypes;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using log4net;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using GPArchitecture.Cache;
using GPArchitecture.Parsers;
using GPArchitecture.Tools;
using GPArchitecture.Accessors;
using GoodyearProcessingService.MessageSpecs.GoodyearCorporation;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.Utilities;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
using GPArchitecture.Models;



namespace GoodyearProcessingService.PreprocessingLogic
{
    class GoodyearCorporation : FileReceiverEndPointPreProcessor
    {
        #region Private Members
        private string strProcessingFolder;
        private string recipient;
        private string strGoodyearNewSOFolder;
        private string strGoodyearChangeSOFolder;
        private string strGoodyearNewSCFolder;
        private string strGoodyearCancelSCFolder;
        private string strGoodyearCancelSOFolder;
        private string strGoodyearChangeSCFolder;
        private StringBuilder newSOBuilder;
        private StringBuilder newSCBuilder;
        private StringBuilder amendSOBuilder;
        private StringBuilder amendSOEmailBuilder;
        private StringBuilder amendSCBuilder;
        private StringBuilder cancelSOBuilder;
        private StringBuilder cancelSCBuilder;
        private static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        #region CTORs
        public GoodyearCorporation()
        {
            // archiveDAO = new ARCHIVE();
        }
        #endregion

        public bool PreprocessGoodyear(GPCustomerMessage objGPCustomerMessage, IList<GPArchitecture.CommonTypes.SourceMessage> sourceMessageLines)
        {
            // Initiate local vars
            bool isSuccess = false;
            bool hasNewSO = false;
            bool hasNewSC = false;
            bool hasAmendSO = false;
            bool hasAmendSC = false;
            bool hasCancelSO = false;
            bool hasCancelSC = false;


            // Initiate string builders
            this.InitializeGoodyearOutboundSOSpecStringBuilder(ref this.newSOBuilder);
            this.InitializeGoodyearOutboundSOSpecStringBuilder(ref this.amendSOBuilder);
            this.InitializeGoodyearStandardOutboundSpecStringBuilder(ref this.newSCBuilder);
            this.InitializeGoodyearStandardOutboundSpecStringBuilder(ref this.amendSOEmailBuilder);
            this.InitializeGoodyearStandardOutboundSpecStringBuilder(ref this.amendSCBuilder);
            this.InitializeGoodyearStandardOutboundSpecStringBuilder(ref this.cancelSOBuilder);
            this.InitializeGoodyearStandardOutboundSpecStringBuilder(ref this.cancelSCBuilder);

            try
            {
                // Look through each message line
                foreach (GPArchitecture.CommonTypes.SourceMessage sourceMessageLine in sourceMessageLines)
                {
                    // Build Goodyear Corp Inbound message line object from source message line
                    GoodyearCorporationInboundSpecs goodyearSourceMessageLine = new GoodyearCorporationInboundSpecs(sourceMessageLine);

                    // Check if new SO
                    if (string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber)
                        && goodyearSourceMessageLine.SIStatus == "0"
                        && goodyearSourceMessageLine.Ver == "00")
                    {
                        this.MapNewSO(goodyearSourceMessageLine, this.newSOBuilder);
                        hasNewSO = true;
                    }
                    // Check if new SC
                    else if (!string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber)
                        && goodyearSourceMessageLine.SIStatus == "0"
                        && goodyearSourceMessageLine.Ver == "00")
                    {
                        this.DirectMappingFixedFieldLengthToCSV(goodyearSourceMessageLine, this.newSCBuilder);
                        hasNewSC = true;
                    }
                    // Check if amended S0
                    else if (string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber)
                        && goodyearSourceMessageLine.SIStatus == "0"
                        && goodyearSourceMessageLine.Ver != "00")
                    {
                        this.DirectMappingFixedFieldLengthToCSV(goodyearSourceMessageLine, this.amendSOEmailBuilder);
                        this.MapNewSO(goodyearSourceMessageLine, this.amendSOBuilder);
                        hasAmendSO = true;
                    }
                    // Check if amended SC
                    else if (!string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber) 
                        && goodyearSourceMessageLine.SIStatus == "0"
                        && goodyearSourceMessageLine.Ver != "00")
                    {
                        this.DirectMappingFixedFieldLengthToCSV(goodyearSourceMessageLine, this.amendSCBuilder);
                        hasAmendSC = true;
                    }
                    // Check if cancelled S0
                    else if (string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber.Trim())
                        && goodyearSourceMessageLine.SIStatus == "1"
                        && goodyearSourceMessageLine.Ver == "00"
                        && ((goodyearSourceMessageLine.PackagingType.Contains("MB4") || goodyearSourceMessageLine.PackagingType.Contains("MB5"))))
                    {
                        this.DirectMappingFixedFieldLengthToCSV(goodyearSourceMessageLine, this.cancelSOBuilder);
                        hasCancelSO = true;
                    }
                    // Check if cancelled SC
                    else if (!string.IsNullOrEmpty(goodyearSourceMessageLine.BOLNumber)
                        && goodyearSourceMessageLine.SIStatus == "1"
                        && goodyearSourceMessageLine.Ver != "00")
                    {
                        this.DirectMappingFixedFieldLengthToCSV(goodyearSourceMessageLine, this.cancelSCBuilder);
                        hasCancelSC = true;
                    }

                }//-- End foreach loop

                #region HasNewSO processing
                // Generate ouput for new SO by dropping the file into any Goodyear subsidiary input folder
                if (hasNewSO)
                {
                    objILog.Info("New so " + hasNewSO + "for file:" + objGPCustomerMessage.BatchFileName);
                    // Get file path where SO file will be dropped
                    string outboundFilePath = string.Empty;
                    strGoodyearNewSOFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_NEW_SO_OUTPUT_FILE_PATH");
                    if (strGoodyearNewSOFolder != null)
                    {
                        outboundFilePath = strGoodyearNewSOFolder.ToString();
                    }

                    // If file path is not found, notify support
                    if (outboundFilePath == string.Empty)
                    {
                        // Notify support
                    }
                    else // drop file
                    {
                        // Add backslash at the end if it's not present
                        if (!outboundFilePath.EndsWith(@"\"))
                            outboundFilePath += @"\";

                        // Generate filename
                        outboundFilePath += objGPCustomerMessage.BatchID.ToString() + "_SO_New" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SO file in path                      
                        File.WriteAllText(outboundFilePath, newSOBuilder.ToString());
                        objILog.Info("New so file generated in path:" + outboundFilePath);

                    }
                    //transaction starts here
                    // Get mapper for goodyear sc
                    objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper);
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    objGPCustomerMessage.Username = EntityMaster.DeriveUsernameFromMapperName(objGPCustomerMessage.MapperName);

                    //getting emailid of user
                    using (var context = new GoodpackEDIEntities())
                    {
                        recipient = (from s in context.Gen_User
                                     where s.Username == objGPCustomerMessage.Username
                                     select s.EmailId).FirstOrDefault();
                    }

                    //getting File Info
                    FileInfo objFileInfo = new FileInfo(strGoodyearNewSOFolder + "\\" + objGPCustomerMessage.BatchID.ToString() + "_SO_New" + objGPCustomerMessage.BatchFileName + ".csv");


                    ITransactionDataManager scmanager = new TransactionDataManager();
                    string soData;


                    //read SO file created its a .csv format file
                    using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                    {
                        soData = objStreamReader.ReadToEnd();
                        objGPCustomerMessage.Message = soData.ToString();
                        objStreamReader.Close();
                        objStreamReader.Dispose();
                    }

                    //placing delimeters inside file content
                    objGPCustomerMessage.Message = objGPCustomerMessage.Message.Replace(',', '|');

                    //creating a genric model for message inside file
                    IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                    GenericItemModel itemModel;
                    string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    for (int i = 0; i < messageData.Length; i++)
                    {
                        itemModel = new GenericItemModel();
                        itemModel.Id = i;
                        itemModel.Name = messageData[i];
                        itemModel.DataValues = objGPCustomerMessage.Message;
                        itemModel.BatchFileSourceAddress = outboundFilePath;
                        itemModel.FileName = objGPCustomerMessage.BatchID.ToString() + "_SO_New" + objGPCustomerMessage.BatchFileName + ".csv";
                        itemModel.Message = objGPCustomerMessage.Message;
                        itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                        genericModel.Add(itemModel);
                    }

                    //verify SO data and processing
                    scmanager.FileInfo(strGoodyearNewSOFolder, objGPCustomerMessage.BatchID.ToString() + "_SO_New" + objGPCustomerMessage.BatchFileName + ".csv", objGPCustomerMessage.Message, recipient);
                    string custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                    objILog.Info("Retrive new so file info");
                    IList<GenericItemModel> model = scmanager.verifySOData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), custPOdate, genericModel, true);
                    objILog.Info("new so transaction completed and model :" + model);

                    //copy New SO file to Archive location before deleting
                    string newSOArchivepath;
                    newSOArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_NEW_SO");
                    newSOArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_SO_New" + objGPCustomerMessage.BatchFileName + ".csv";
                    this.CopySourceFile(outboundFilePath, newSOArchivepath);

                    // Delete source New SO file
                    this.ClearSourceFile(outboundFilePath);
                }//-- End New SO

                #endregion

                #region HasAmendSO processing
                // Generate ouput for amend SO by dropping the file into the configured head company's subsidiary input folder
                //and send mail
                if (hasAmendSO)
                {
                    objILog.Info("Change so " + hasAmendSO + "for file:" + objGPCustomerMessage.BatchFileName);
                    // Get file path where SO file will be dropped
                    string outboundFilePath = string.Empty;
                    strGoodyearChangeSOFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_CHANGE_SO_OUTPUT_FILE_PATH");


                    if (strGoodyearChangeSOFolder != null)
                    {
                        outboundFilePath = strGoodyearChangeSOFolder.ToString();
                    }

                    // If file path is not found, notify support
                    if (outboundFilePath == string.Empty)
                    {
                        // Notify support
                    }
                    else // drop file
                    {
                        // Add backslash at the end if it's not present
                        if (!outboundFilePath.EndsWith(@"\"))
                            outboundFilePath += @"\";

                        // Generate filename
                        outboundFilePath += objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SO file in path                       
                        File.WriteAllText(outboundFilePath, amendSOBuilder.ToString());
                        objILog.Info("Change so file generated in path:" + outboundFilePath);
                    }
                    //transaction starts here
                    // Get mapper for goodyear sc
                    objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper);
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    objGPCustomerMessage.Username = EntityMaster.DeriveUsernameFromMapperName(objGPCustomerMessage.MapperName);
                    //getting emailid of user
                    using (var context = new GoodpackEDIEntities())
                    {
                        recipient = (from s in context.Gen_User
                                     where s.Username == objGPCustomerMessage.Username
                                     select s.EmailId).FirstOrDefault();
                    }

                    //getting File Info
                    FileInfo objFileInfo = new FileInfo(strGoodyearChangeSOFolder + "\\" + objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv");

                    ITransactionDataManager scmanager = new TransactionDataManager();
                    string soData;

                    //read SO file created its a .csv format file
                    using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                    {
                        soData = objStreamReader.ReadToEnd();
                        objGPCustomerMessage.Message = soData.ToString();
                        objStreamReader.Close();
                        objStreamReader.Dispose();
                    }

                    //placing delimeters inside file content
                    objGPCustomerMessage.Message = objGPCustomerMessage.Message.Replace(',', '|');
                    IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                    GenericItemModel itemModel;

                    //creating a genric model for message inside file
                    string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    for (int i = 0; i < messageData.Length; i++)
                    {
                        itemModel = new GenericItemModel();
                        itemModel.Id = i;
                        itemModel.Name = messageData[i];
                        itemModel.DataValues = objGPCustomerMessage.Message;
                        itemModel.BatchFileSourceAddress = outboundFilePath;
                        itemModel.FileName = objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv";
                        itemModel.Message = objGPCustomerMessage.Message;
                        itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                        genericModel.Add(itemModel);
                    }

                    //verify SO data and processing                     
                    scmanager.FileInfo(strGoodyearChangeSOFolder, objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv", objGPCustomerMessage.Message, recipient);
                    string custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                    IList<GenericItemModel> model = scmanager.verifySOData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), custPOdate, genericModel, true);
                    objILog.Info("change so transaction completed and model :" + model);



                }
                #endregion

                #region HasNewSC Processing
                // Generate ouput for new SC by dropping the file into any Goodyear subsidiary input folder
                if (hasNewSC)
                {
                    objILog.Info("New SC " + hasNewSC + "for file:" + objGPCustomerMessage.BatchFileName);
                    // Get file path where SC file will be dropped
                    string outboundFilePath = string.Empty;
                    strGoodyearNewSCFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_NEW_SC_OUTPUT_FILE_PATH");

                    if (strGoodyearNewSCFolder != null)
                    {
                        outboundFilePath = strGoodyearNewSCFolder.ToString();
                    }

                    // If file path is not found, notify support
                    if (outboundFilePath == string.Empty)
                    {
                        // Notify support
                    }
                    else // drop file
                    {
                        // Add backslash at the end if it's not present
                        if (!outboundFilePath.EndsWith(@"\"))
                            outboundFilePath += @"\";

                        // Generate filename
                        outboundFilePath += objGPCustomerMessage.BatchID.ToString() + "_NEW_SC_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SO file in path
                        File.WriteAllText(outboundFilePath, newSCBuilder.ToString());
                        objILog.Info("New SC file generated in path:" + outboundFilePath);
                    }

                    // Get mapper for goodyear sc
                    objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper);
                    objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                    objGPCustomerMessage.Username = EntityMaster.DeriveUsernameFromMapperName(objGPCustomerMessage.MapperName);

                    //getting emailid of user
                    using (var context = new GoodpackEDIEntities())
                    {
                        recipient = (from s in context.Gen_User
                                     where s.Username == objGPCustomerMessage.Username
                                     select s.EmailId).FirstOrDefault();
                    }

                    //getting File Info
                    FileInfo objFileInfo = new FileInfo(strGoodyearNewSCFolder + "\\" + objGPCustomerMessage.BatchID.ToString() + "_NEW_SC_" + objGPCustomerMessage.BatchFileName + ".csv");

                    ITransactionDataManager scmanager = new TransactionDataManager();
                    string scData;
                    //read SC file created its a .csv format file
                    using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                    {
                        scData = objStreamReader.ReadToEnd();
                        objGPCustomerMessage.Message = scData;
                        objStreamReader.Close();
                        objStreamReader.Dispose();
                    }
                    //placing delimeters inside file content
                    objGPCustomerMessage.Message = objGPCustomerMessage.Message.Replace(',', '|');

                    //creating a genric model for message inside file
                    IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                    GenericItemModel itemModel;
                    string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    for (int i = 0; i < messageData.Length; i++)
                    {
                        itemModel = new GenericItemModel();
                        itemModel.Id = i;
                        itemModel.Name = messageData[i];
                        itemModel.DataValues = objGPCustomerMessage.Message;
                        itemModel.BatchFileSourceAddress = outboundFilePath;
                        itemModel.FileName = objGPCustomerMessage.BatchID.ToString() + "_NEW_SC_" + objGPCustomerMessage.BatchFileName + ".csv";
                        itemModel.Message = objGPCustomerMessage.Message;
                        
                        itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                        genericModel.Add(itemModel);
                    }

                    scmanager.FileInfo(strGoodyearNewSCFolder, objGPCustomerMessage.BatchID.ToString() + "_NEW_SC_" + objGPCustomerMessage.BatchFileName + ".csv", objGPCustomerMessage.Message, recipient);
                    IList<GenericItemModel> model = scmanager.verifyData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, true);
                    IList<ResponseModel> SCModel = scmanager.SCSubmit(model, objGPCustomerMessage.Username);
                    objILog.Info("New SC transaction completed and model :" + model);

                    //copy New SC file to Archive location before deleting
                    string newSCArchivepath;
                    newSCArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_NEW_SC");
                    newSCArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_NEW_SC_" + objGPCustomerMessage.BatchFileName + ".csv";
                    this.CopySourceFile(outboundFilePath, newSCArchivepath);

                    // Delete source New SC file
                    this.ClearSourceFile(outboundFilePath);
                }
                #endregion

                #region HasCancelledSO or AmendSO Processing
                //// Email Cancel SO and AmendSO file to support
                if (hasCancelSO || hasAmendSO)
                {
                    objILog.Info("cancel so" + hasCancelSO + "change so" + hasAmendSO);
                    List<Attachment> attachments = new List<Attachment>();

                    Dictionary<string, string> placeholders = new Dictionary<string, string>();

                    // Archive File and add to attachment list

                    // string archivePath = String.Format("{0}\\{1}", GPArchitecture.Cache.Config.getConfig("PREPRO_ARCHIVE"), GoodpackEDI.Utilities.ArchiveSources.PREPROCESSING_IN.ToString());
                    strGoodyearCancelSOFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_CANCEL_SO_OUTPUT_FILE_PATH");
                    strGoodyearChangeSOFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_CHANGE_SO_OUTPUT_FILE_PATH");

                    if (hasCancelSO)
                    {

                        if (!strGoodyearCancelSOFolder.EndsWith(@"\"))
                            strGoodyearCancelSOFolder += @"\";

                        // Generate filename
                        strGoodyearCancelSOFolder += objGPCustomerMessage.BatchID.ToString() + "_SO_Cancel_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SO file in path                       
                        File.WriteAllText(strGoodyearCancelSOFolder, cancelSOBuilder.ToString());

                        //Add attachment
                        if (!string.IsNullOrEmpty(strGoodyearCancelSOFolder))
                        {
                            Attachment attachment = new Attachment(strGoodyearCancelSOFolder);
                            attachment.Name = objGPCustomerMessage.BatchID.ToString() + "_SO_Cancel_" + objGPCustomerMessage.BatchFileName + ".csv";
                            attachments.Add(attachment);
                            objILog.Info("Attachment for email in path" + strGoodyearCancelSOFolder);
                        }
                        // Set Placeholders

                        placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                        placeholders.Add("$$SubsyName$$", objGPCustomerMessage.SubsiName);
                        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToShortDateString());
                        // Send email
                        objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper);
                        GPTools.SendEmail(placeholders, getAllRecipients(objGPCustomerMessage.MapperName), "NOTIFY_GOODYEAR_CANCEL_SO", attachments);
                        objILog.Info("Send Email to" + getAllRecipients(objGPCustomerMessage.MapperName));


                        //copy Cancel SO file to Archive location before deleting
                        string cancelSOArchivepath;
                        cancelSOArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_CANCEL_SO");
                        cancelSOArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_SO_Cancel_" + objGPCustomerMessage.BatchFileName + ".csv";
                        this.CopySourceFile(strGoodyearCancelSOFolder, cancelSOArchivepath);

                        // Delete source Cancel SO file
                        this.ClearSourceFile(strGoodyearCancelSOFolder);
                   
                    
                    
                    }
                    if (hasAmendSO)
                    {

                        if (!strGoodyearChangeSOFolder.EndsWith(@"\"))
                            strGoodyearChangeSOFolder += @"\";

                        // Generate filename

                        strGoodyearChangeSOFolder += objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SO file in path                       
                        //File.WriteAllText(strGoodyearChangeSOFolder, amendSOBuilder.ToString());

                        //Add attachment
                        //if (!string.IsNullOrEmpty(strGoodyearChangeSOFolder))
                        //{
                        //    Attachment attachment = new Attachment(strGoodyearChangeSOFolder);
                        //    attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        //    attachments.Add(attachment);
                        //    objILog.Info("Attachment for email in path" + strGoodyearChangeSOFolder);
                        //}
                    }

                    // Email to GP subsidiaries
                   

                    // Get recipient list
                    // recipientList = this.GetGoodyearNotificationRecipients();


                    //copy Change SO file to Archive location before deleting
                    string changeSOArchivepath;
                    changeSOArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_CHANGE_SO");
                    changeSOArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_SO_Change_" + objGPCustomerMessage.BatchFileName + ".csv";
                    this.CopySourceFile(strGoodyearChangeSOFolder, changeSOArchivepath);

                    // Delete source Change SO file
                    this.ClearSourceFile(strGoodyearChangeSOFolder);
                }//-- End hasCancelSO
                #endregion

                #region HasAmendSC or HasCancelledSC processing
                // Email Amend SC file to support
                if (hasAmendSC || hasCancelSC)
                {
                    objILog.Info("cancel sc" + hasCancelSC + "change sc" + hasAmendSC);
                    List<Attachment> attachments = new List<Attachment>();
                    Dictionary<string, string> placeholders = new Dictionary<string, string>();

                    // Archive File and add to attachment list
                    strGoodyearCancelSCFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_CANCEL_SC_OUTPUT_FILE_PATH");
                    strGoodyearChangeSCFolder = GPArchitecture.Cache.Config.getConfig("GOOD_YEAR_CHANGE_SC_OUTPUT_FILE_PATH");

                    //  GOOD_YEAR_CANCEL_SC_OUTPUT_FILE_PATH
                    // string archivePath = String.Format("{0}\\{1}", strGoodyearCancelSCFolder, GoodpackEDI.Utilities.ArchiveSources.PREPROCESSING_IN.ToString());
                    if (hasAmendSC)
                    {
                        if (!strGoodyearChangeSCFolder.EndsWith(@"\"))
                            strGoodyearChangeSCFolder += @"\";

                        // Generate filename
                        strGoodyearChangeSCFolder += objGPCustomerMessage.BatchID.ToString() + "_SC_Change_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SC file in path                       
                        File.WriteAllText(strGoodyearChangeSCFolder, amendSCBuilder.ToString());

                        //Add attachment
                        if (!string.IsNullOrEmpty(strGoodyearChangeSCFolder))
                        {
                            Attachment attachment = new Attachment(strGoodyearChangeSCFolder);
                            attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                            attachments.Add(attachment);
                            objILog.Info("Attachment for email in path" + strGoodyearChangeSCFolder);
                        }

                        #region Upload Change SC
                        try
                        {
                            string outboundFilePath = string.Empty;
                            if (strGoodyearChangeSCFolder != null)
                            {
                                outboundFilePath = strGoodyearChangeSCFolder.ToString();
                            }            
                            // Get mapper for goodyear sc
                            objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper);
                            objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(objGPCustomerMessage.MapperName);
                            objGPCustomerMessage.Username = EntityMaster.DeriveUsernameFromMapperName(objGPCustomerMessage.MapperName);

                            //getting emailid of user
                            using (var context = new GoodpackEDIEntities())
                            {
                                recipient = (from s in context.Gen_User
                                             where s.Username == objGPCustomerMessage.Username
                                             select s.EmailId).FirstOrDefault();
                            }

                            //getting File Info
                            FileInfo objFileInfo = new FileInfo(strGoodyearChangeSCFolder);

                            ITransactionDataManager scmanager = new TransactionDataManager();
                            string scData;
                            //read SC file created its a .csv format file
                            using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                            {
                                scData = objStreamReader.ReadToEnd();
                                objGPCustomerMessage.Message = scData;
                                objStreamReader.Close();
                                objStreamReader.Dispose();
                            }
                            //placing delimeters inside file content
                            objGPCustomerMessage.Message = objGPCustomerMessage.Message.Replace(',', '|');

                            //creating a genric model for message inside file
                            IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                            GenericItemModel itemModel;
                            string[] messageData = objGPCustomerMessage.Message.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                            for (int i = 0; i < messageData.Length; i++)
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Id = i;
                                itemModel.Name = messageData[i];
                                itemModel.DataValues = objGPCustomerMessage.Message;
                                itemModel.BatchFileSourceAddress = outboundFilePath;
                                itemModel.FileName = objGPCustomerMessage.BatchID.ToString() + "_Change_SC_" + objGPCustomerMessage.BatchFileName + ".csv";
                                itemModel.Message = objGPCustomerMessage.Message;
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                genericModel.Add(itemModel);
                            }
                            scmanager.FileInfo(strGoodyearNewSCFolder, objGPCustomerMessage.BatchID.ToString() + "_Change_SC_" + objGPCustomerMessage.BatchFileName + ".csv", objGPCustomerMessage.Message, recipient);
                            IList<GenericItemModel> model = scmanager.verifyData(objGPCustomerMessage.MapperName, objGPCustomerMessage.Username, objGPCustomerMessage.SubsiId.ToString(), genericModel, true);
                            IList<ResponseModel> SCModel = scmanager.SCSubmit(model, objGPCustomerMessage.Username);
                            objILog.Info("Change SC transaction completed and model :" + model);


                        }
                        catch(Exception e)
                        {
                            objILog.Error("Change_SC upload error -"+e);
                        }
#endregion

                        //copy Change SC file to Archive location before deleting
                        string changeSCArchivepath;
                        changeSCArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_CHANGE_SO");
                        changeSCArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_SC_Change_" + objGPCustomerMessage.BatchFileName + ".csv";
                        this.CopySourceFile(strGoodyearChangeSCFolder, changeSCArchivepath);

                        // Delete source Change SO file
                      //  this.ClearSourceFile(strGoodyearChangeSCFolder);


                    }
                    if (hasCancelSC)
                    {

                        if (!strGoodyearCancelSCFolder.EndsWith(@"\"))
                            strGoodyearCancelSCFolder += @"\";

                        // Generate filename
                        strGoodyearCancelSCFolder += objGPCustomerMessage.BatchID.ToString() + "_SC_Cancel_" + objGPCustomerMessage.BatchFileName + ".csv";

                        // Drop SC file in path                       
                        File.WriteAllText(strGoodyearCancelSCFolder, cancelSCBuilder.ToString());

                        //Add attachment
                        if (!string.IsNullOrEmpty(strGoodyearCancelSCFolder))
                        {
                            Attachment attachment = new Attachment(strGoodyearCancelSCFolder);
                            attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                            attachments.Add(attachment);
                            objILog.Info("Attachment for email in path" + strGoodyearCancelSCFolder);
                        }
                        //copy Cancel SC file to Archive location before deleting
                        string cancelSCArchivepath;
                        cancelSCArchivepath = GPArchitecture.Cache.Config.getConfig("ARCHIVE_CANCEL_SC");
                        cancelSCArchivepath += "\\" + objGPCustomerMessage.BatchID.ToString() + "_SC_Cancel_" + objGPCustomerMessage.BatchFileName + ".csv";
                        this.CopySourceFile(strGoodyearCancelSCFolder, cancelSCArchivepath);

                        // Delete source Cancel SC file
                        this.ClearSourceFile(strGoodyearCancelSCFolder);
                    }

                    // Set Placeholders
                    placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                    placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                    placeholders.Add("$$SubsyName$$", objGPCustomerMessage.SubsiName);
                    placeholders.Add("$$DateTime$$", System.DateTime.Now.ToShortDateString());

                    // Get recipient list
                    // recipientList = this.GetGoodyearNotificationRecipients();
                    objGPCustomerMessage.MapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper);
                    GPTools.SendEmail(placeholders, getAllRecipients(objGPCustomerMessage.MapperName), "NOTIFY_GOODYEAR_SC_CUSTOMER_ATTACHMENT", attachments);
                    this.ClearSourceFile(strGoodyearChangeSCFolder);
                    objILog.Info("Send Email to" + getAllRecipients(objGPCustomerMessage.MapperName));
                }
                #endregion

            }
            finally
            {


            }

            return isSuccess;
        }

        /// <summary>
        /// Maps to target SO message line in CSV format
        /// </summary>
        /// <param name="goodyearSourceMessageLine"></param>
        private void MapNewSO(GoodyearCorporationInboundSpecs goodyearSourceMessageLine, StringBuilder stringBuilder)
        {
            // Init vars
            int tempInt;
            DateTime tempDate;

            // Add line break
            stringBuilder.Append(Environment.NewLine);

            // #1 Map to Customer field.  Remove leading zeros from source field "Customer Code"
            if (int.TryParse(goodyearSourceMessageLine.CustomerCode, out tempInt))
            {
                stringBuilder.Append(tempInt);
            }
            else
            {
                stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.CustomerCode.Replace("\"", "\"\""))
                    .Append("\"");
            }

            // #2 Map to SI Number
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                .Append(goodyearSourceMessageLine.SINumber.Replace("\"", "\"\""))
                .Append("\"");

            // #3 Map to LoadDate
            stringBuilder.Append(",");
            if (DateTime.TryParseExact(goodyearSourceMessageLine.LoadDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate))
            {
                stringBuilder.Append(tempDate.ToString("dd/MM/yyyy"));
            }
            else
            {
                stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.LoadDate.Replace("\"", "\"\""))
                    .Append("\"");
            }

            // #4 Map to Packing Type
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.PackagingType.Replace("\"", "\"\""))
                    .Append("\"");

            // #5 Map to Factory Code
            stringBuilder.Append(",");
            if (goodyearSourceMessageLine.FactoryCode.ToUpper() == "RSS_WILSON"
                || goodyearSourceMessageLine.FactoryCode.ToUpper() == "SCQ"
                || goodyearSourceMessageLine.FactoryCode.ToUpper() == "SFS"
                || goodyearSourceMessageLine.FactoryCode.ToUpper() == "RSS_ABAD"
                )
            {
                stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.FactoryCode.Replace("\"", "\"\""))
                    .Append("-")
                    .Append(goodyearSourceMessageLine.LoadPort.Replace("\"", "\"\""))
                    .Append("\"");
            }
            else
            {
                stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.FactoryCode.Replace("\"", "\"\""))
                    .Append("\"");
            }

            // #6 Map to Customer Code
            // try to convert to int then back to string to remove leading zeros
            if (int.TryParse(goodyearSourceMessageLine.CustomerCode, out tempInt))
            {
                stringBuilder.Append(",");
                stringBuilder.Append("\"")
                        .Append(tempInt.ToString())
                        .Append("\"");
            }
            else
            {
                stringBuilder.Append(",");
                stringBuilder.Append("\"")
                        .Append(goodyearSourceMessageLine.CustomerCode.Replace("\"", "\"\""))
                        .Append("\"");
            }

            // #7 Map to SI QTY
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.SIQtyinNumberofUnits.Replace("\"", "\"\""))
                    .Append("\"");

            // #8 Map to SO Number
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.SONumber.Replace("\"", "\"\""))
                    .Append("\"");

            // #9 Map to Vessel Name
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.VesselName.Replace("\"", "\"\""))
                    .Append("\"");

            // #10 Map to Voyage Number
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.VoyageNumber.Replace("\"", "\"\""))
                    .Append("\"");

            // #11 Map to Load Port
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.LoadPort.Replace("\"", "\"\""))
                    .Append("\"");

            // #12 Map to Arrival Port
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.ArrivalPort.Replace("\"", "\"\""))
                    .Append("\"");

            // #13 Map to Customer Name
            stringBuilder.Append(",");
            stringBuilder.Append("\"")
                    .Append(goodyearSourceMessageLine.CustomerName.Replace("\"", "\"\""))
                    .Append("\"");


        }//-- End MapSO()

        private void DirectMappingFixedFieldLengthToCSV(GoodyearCorporationInboundSpecs goodyearSourceMessageLine, StringBuilder stringBuilder)
        {
            // Init vars
            int tempInt;

            // Add line break
            stringBuilder.Append(Environment.NewLine);

            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.CustPO);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.SONumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.SINumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.Ver);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.SIStatus);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.SIQtyinNumberofUnits);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.RubberGrade);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.Supplier);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.FactoryCode);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.PackagingType);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.LoadPort);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.LoadDate);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.ShippingLine);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.VesselName);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.VoyageNumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.BOLNumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.BOLQty);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.ContainerNumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.ContainerQty);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.OceanETD);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.ArrivalPort);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.ArrivalDate);

            stringBuilder.Append(",");
            if (int.TryParse(goodyearSourceMessageLine.CustomerCode, out tempInt))
            {
                this.EscapeCSVField(stringBuilder, tempInt.ToString());
            }
            else
            {
                this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.CustomerCode);
            }

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.CustomerName);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.BookingNumber);

            stringBuilder.Append(",");
            this.EscapeCSVField(stringBuilder, goodyearSourceMessageLine.EndofRecord);
        }//-- End MaAmendSO()


        /// <summary>
        /// Initiates the string builder for New SO message.  The format is in CSV.
        /// </summary>
        private void InitializeGoodyearOutboundSOSpecStringBuilder(ref StringBuilder stringBuilder)
        {
            stringBuilder = new StringBuilder();
            stringBuilder.Append("Customer")
                .Append(",SI Number")
                .Append(",Load Date")
                .Append(",Packing type")
                .Append(",Factory Code")
                .Append(",Customer Code")
                .Append(",SI QTY")
                .Append(",SO Number")
                .Append(",Vessel Name")
                .Append(",Voyage Number")
                .Append(",Load Port")
                .Append(",Arrival Port")
                .Append(",Customer Name");
        }

        /// <summary>
        /// Initiates the string builder for New SC message.  The format is in CSV.
        /// </summary>
        private void InitializeGoodyearStandardOutboundSpecStringBuilder(ref StringBuilder stringBuilder)
        {
            stringBuilder = new StringBuilder();
            stringBuilder.Append("Cust PO")
                .Append(",SO Number")
                .Append(",SI Number")
                .Append(",Ver")
                .Append(",SI Status")
                .Append(",SI Qty in Number of Units")
                .Append(",Rubber Grade")
                .Append(",Supplier")
                .Append(",Factory Code")
                .Append(",Packaging Type")
                .Append(",Load Port")
                .Append(",Load Date")
                .Append(",Shipping Line")
                .Append(",Vessel Name")
                .Append(",Voyage Number")
                .Append(",BOL Number")
                .Append(",BOL Qty")
                .Append(",Container Number")
                .Append(",Container Qty")
                .Append(",Ocean ETD")
                .Append(",Arrival Port")
                .Append(",Arrival Date")
                .Append(",Customer Code")
                .Append(",Customer Name")
                .Append(",Booking Number")
                .Append(",End of Record");
        }

        /// <summary>
        /// Escapes the field in CSV form and appends it into the string builder
        /// </summary>
        /// <param name="stringBuilder"></param>
        /// <param name="field"></param>
        private void EscapeCSVField(StringBuilder stringBuilder, string field)
        {
            stringBuilder.Append("\"")
                .Append(field.Trim().Replace("\"", "\"\""))
                .Append("\"");
        }

        /// <summary>
        /// Get list of all Goodpack subsidiary email addresses which are associated with Goodyear
        /// </summary>
        /// <returns></returns>
        //private List<string> GetGoodyearNotificationRecipients()
        //{
        //    // Set Receipients
        //    List<string> recipientList = new List<string>();
        //    //   DataTable subsidiaryList = SQLDbHandler.ExecuteQuery("conn", DBQueryResources.SELECT_GOODYEAR_SUBSI, null, DbCommandType.STRING_QUERY);

        //    string[] adminListCSV = Regex.Split("mothy.mp@attinadsoftware.com", ",");
        //    // string[] adminListCSV = Regex.Split(Config.getConfig("AdminEmail"), ",");


        //    // string adminCSV = "mothy.mp@attinadsoftware.com";
        //    //Regex.Split(Config.getConfig("AdminEmail"), ",");
        //    foreach (string emailAddr in adminListCSV)
        //    {
        //        if (!recipientList.Contains(emailAddr))
        //        {
        //            recipientList.Add(emailAddr);
        //        }
        //    }//--End Foreach

        //    //if (subsidiaryList != null && subsidiaryList.Rows.Count > 0)
        //    //{
        //    //    foreach (DataRow row in subsidiaryList.Rows)
        //    //    {
        //    // Parse list
        //    //  string[] subsidiaryListCSV = Regex.Split(row[0].ToString(), ",");
        //    //foreach (string emailAddr in subsidiaryListCSV)
        //    //{
        //    //if (!recipientList.Contains(emailAddr))
        //    //{
        //    //    recipientList.Add(emailAddr);
        //    //}
        //    //    }
        //    //}
        //    //  }//-- End if

        //    return recipientList;
        //}


        private string[] getAllRecipients(string mapperName)
        {
            List<string> mailList = new List<string>();
            // mailList.Add(objGPCustomerMessage.EmailId);
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperName
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                   
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
    }
}
