﻿using GoodyearProcessingService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace GoodYearProcessingServiceNew
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Starting preprocessing service");
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //   new ProcessingService() 
            //};
            //ServiceBase.Run(ServicesToRun);
#if DEBUG

            log.Info("Entered in Program.cs");
            ProcessingService service = new ProcessingService();
            service.debugger();


# else
            log.Info("Release Mode");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new ProcessingService() 
            };
            log.Info("Leaving ");
            ServiceBase.Run(ServicesToRun);
# endif
        }
    }
}
