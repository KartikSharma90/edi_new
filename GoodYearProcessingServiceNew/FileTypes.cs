﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodyearProcessingService
{
    class FileTypes
    {
        public int Id { get; set; }
        public string FileType { get; set; }
    }
}
