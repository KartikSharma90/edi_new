﻿using GPArchitecture.CommonTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using GPArchitecture.CommonTypes;

namespace GoodyearProcessingService.MessageSpecs.GoodyearCorporation
{
    class GoodyearCorporationInboundSpecs
    {
        // Fields
        public string CustPO;
        public string SONumber;
        public string SINumber;
        public string Ver;
        public string SIStatus;
        public string SIQtyinNumberofUnits;
        public string RubberGrade;
        public string Supplier;
        public string FactoryCode;
        public string PackagingType;
        public string LoadPort;
        public string LoadDate;
        public string ShippingLine;
        public string VesselName;
        public string VoyageNumber;
        public string BOLNumber;
        public string BOLQty;
        public string ContainerNumber;
        public string ContainerQty;
        public string OceanETD;
        public string ArrivalPort;
        public string ArrivalDate;
        public string CustomerCode;
        public string CustomerName;
        public string BookingNumber;
        public string EndofRecord;
        public string originalLine;

        public GoodyearCorporationInboundSpecs() { }

        public GoodyearCorporationInboundSpecs(SourceMessage sourceMessageLine)
        {
            if (sourceMessageLine.Fields.Count < 26)
            {
                throw new System.Exception("Field count in Goodyear Corporation message is less than the expected number of fields.");
            }
            
            this.CustPO = sourceMessageLine.Fields[0].Value.Trim();
            this.SONumber = sourceMessageLine.Fields[1].Value.Trim();
            this.SINumber = sourceMessageLine.Fields[2].Value.Trim();
            this.Ver = sourceMessageLine.Fields[3].Value.Trim();
            this.SIStatus = sourceMessageLine.Fields[4].Value.Trim();
            this.SIQtyinNumberofUnits = sourceMessageLine.Fields[5].Value.Trim();
            this.RubberGrade = sourceMessageLine.Fields[6].Value.Trim();
            this.Supplier = sourceMessageLine.Fields[7].Value.Trim();
            this.FactoryCode = sourceMessageLine.Fields[8].Value.Trim();
            this.PackagingType = sourceMessageLine.Fields[9].Value.Trim();
            this.LoadPort = sourceMessageLine.Fields[10].Value.Trim();
            this.LoadDate = sourceMessageLine.Fields[11].Value.Trim();
            this.ShippingLine = sourceMessageLine.Fields[12].Value.Trim();
            this.VesselName = sourceMessageLine.Fields[13].Value.Trim();
            this.VoyageNumber = sourceMessageLine.Fields[14].Value.Trim();
            this.BOLNumber = sourceMessageLine.Fields[15].Value.Trim();
            this.BOLQty = sourceMessageLine.Fields[16].Value.Trim();
            this.ContainerNumber = sourceMessageLine.Fields[17].Value.Trim();
            this.ContainerQty = sourceMessageLine.Fields[18].Value.Trim();
            this.OceanETD = sourceMessageLine.Fields[19].Value.Trim();
            this.ArrivalPort = sourceMessageLine.Fields[20].Value.Trim();
            this.ArrivalDate = sourceMessageLine.Fields[21].Value.Trim();
            this.CustomerCode = sourceMessageLine.Fields[22].Value.Trim();
            this.CustomerName = sourceMessageLine.Fields[23].Value.Trim();
            this.BookingNumber = sourceMessageLine.Fields[24].Value.Trim();
            this.EndofRecord = sourceMessageLine.Fields[25].Value.Trim();
            this.originalLine = sourceMessageLine.OriginalLineContent;
        }
    }
}
