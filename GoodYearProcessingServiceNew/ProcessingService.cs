﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using GoodyearProcessingService;

namespace GoodyearProcessingService
{
    public partial class ProcessingService : ServiceBase
    {
        public ProcessingService()
        {
            log.Info("Entering into constructor");
            InitializeComponent();
            log.Info("Leaving from constructor");
        }

      //  internal static ILog objILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        internal static ILog log = LogManager.GetLogger(typeof(ProcessingService));
        public const string Service = "Goodpack Preprocessor Service";
        FileReceiverEndPointPreProcessor   objPreProcessor = new FileReceiverEndPointPreProcessor();
        public System.Timers.Timer processTimer;
        private int NoofAttempts = 0;
        public void debugger()
        {
            log.Info("Entering into Start method");
            OnStart(null);
            log.Info("Leaving Start method");
        }
        protected override void OnStart(string[] args)
        {

            log.Info("Entering Response Processor OnStart");
         //   DBConnection.InitializeSQLConn(ConfigurationManager.ConnectionStrings["conn"].ConnectionString);
           // if (System.Configuration.ConfigurationManager.AppSettings["debug"].ToString() == "true")
           //     System.Diagnostics.Debugger.Launch();
            processTimer = new System.Timers.Timer();
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("RcvFilePollingInterval")));
            //new TimeSpan(0, 0, 30);
            this.processTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.processTimer.Elapsed += new System.Timers.ElapsedEventHandler(processTimer_Elapsed);
            this.processTimer.Enabled = true;
            this.processTimer.Start();
            //objPreProcessor.Initialize();
            log.Debug("Leaving Response Processor OnStart");
            //while (true) { }
            
        }
        void processTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            log.Debug("Entering processTimer_Elapsed event");

            if (!objPreProcessor.isProcessing)
            {
                objPreProcessor.Initialize();
            }
            log.Debug("Leaving processTimer_Elapsed event");
        }
        protected override void OnStop()
        {
            this.processTimer.Stop();
            while (objPreProcessor.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    log.Warn("Goodpack Preprocessor Service was stopped abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
