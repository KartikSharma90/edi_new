﻿using log4net;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.XWPF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class ExcelCreator
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ExcelCreator));
        public static void CreateExcel(string header,string fileName,string filePath)
        {
            using (FileStream stream = new FileStream(Path.Combine(filePath, fileName + ".xlsx"), FileMode.Create, FileAccess.Write))
            {
                IWorkbook wb = new XSSFWorkbook();
                ISheet sheet = wb.CreateSheet("Sheet1");
                ICreationHelper cH = wb.GetCreationHelper();
                var data = header.Split(',');
                for (int i = 0; i < 1; i++)
                {
                    IRow row = sheet.CreateRow(i);
                    for (int j = 0; j < data.Length; j++)
                    {
                        ICell cell = row.CreateCell(j);
                        cell.SetCellValue(cH.CreateRichTextString(data[j].ToString()));
                    }
                }
                wb.Write(stream);
            }

        }
        public static void CreateExcelWithData(string header, string data, string fileName, string filePath)
        {
            throw new NotImplementedException();
            using (FileStream stream = new FileStream(Path.Combine(filePath, fileName + ".xlsx"), FileMode.Create, FileAccess.Write))
            {
                IWorkbook wb = new XSSFWorkbook();
                ISheet sheet = wb.CreateSheet("Sheet1");
                ICreationHelper cH = wb.GetCreationHelper();
                var headerdata = header.Split(',');
                for (int i = 0; i < 1; i++)
                {
                    IRow row = sheet.CreateRow(i);
                    for (int j = 0; j < headerdata.Length; j++)
                    {
                        ICell cell = row.CreateCell(j);
                        cell.SetCellValue(cH.CreateRichTextString(headerdata[j].ToString()));
                    }
                }
                //var dataValue = data.Split(',');
                //for (int i = 1; i < 1; i++)
                //{
                //    IRow row = sheet.CreateRow(i);
                //    for (int j = 0; j < dataValue.Length; j++)
                //    {
                //        ICell cell = row.CreateCell(j);
                //        cell.SetCellValue(cH.CreateRichTextString(dataValue[j].ToString()));
                //    }
                //}
                wb.Write(stream);
            }
        }
    }
}