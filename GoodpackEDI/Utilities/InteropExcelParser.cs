﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Office.Interop.Excel;
using System.Text;

namespace GoodpackEDI.Utilities
{
    public class InteropExcelParser
    {
        public static string ExcelParser(string path)
        {
            Application xlApp = new Application();
            Workbook xlWorkbook = xlApp.Workbooks.Open(path);
            _Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Range xlRange = xlWorksheet.UsedRange;
            string val=null;
            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;
            StringBuilder excelData = new StringBuilder();
            for (int i = 1; i <= rowCount; i++)
            {               
                for (int j = 1; j <= colCount; j++)
                {
                    if (xlRange.Cells[i, j].Value != null)
                    {     
                        //if(xlRange.Cells[i,j]==xlRange.)


                        excelData.Append(xlRange.Cells[i, j].Value.ToString().Replace("12:00:00 AM", " "));
                       // Console.WriteLine("" + j + "." + xlRange.Cells[i, j].Value2.ToString() + ",");
                    }
                    else
                    {
                        excelData.Append("");
                    }

                    if (j != colCount) {
                        excelData.Append(",");
                    }
                }
                if (i == 1)
                {
                    excelData.Replace('\n', ' ');
                }
                excelData.Append("\r\n");
            }
            return excelData.ToString();        
        }
    }
}