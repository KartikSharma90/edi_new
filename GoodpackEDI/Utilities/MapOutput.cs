﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class MapOutput
    {

        int intLineCount = 0;
        int intLinesSuccessful = 0;
        int intLinesFailed = 0;
        bool blnIsSuccess = false;
        bool bValidationFailure = false;
        bool containsUpdateSupplement = false;
        bool containsCancelSupplement = false;
        string strErrorCode;
        string strErrorDesc;
        string strErrorDescFormatted;
        string strErrorLookupFailed;
        string skipTempFilePath;
        string strRemarks;
        IList<LineReferences> lstLineReferenceItems;
        long customerCode = -1;
        string customerName = string.Empty;

        public int LineCount
        {
            set { intLineCount = value; }
            get { return intLineCount; }
        }
        public int LinesSuccessful
        {
            set { intLinesSuccessful = value; }
            get { return intLinesSuccessful; }
        }
        public bool IsSuccess
        {
            set { blnIsSuccess = value; }
            get { return blnIsSuccess; }
        }
        public ErrorCodes ErrorCode;
        public string ErrorDesc
        {
            set { strErrorDesc = value; }
            get { return strErrorDesc; }
        }
        public string ErrorDescFormatted
        {
            set { strErrorDescFormatted = value; }
            get { return strErrorDescFormatted; }
        }
        public string ErrorLookupFailed
        {
            set { strErrorLookupFailed = value; }
            get { return strErrorLookupFailed; }

        }
        public StringBuilder OutputMessage;

        /// <summary>
        /// String builder containing the records which were skipped by the mapper, in same format as how the system
        /// received the record.  If the input is in Excel format, the content is automatically transformed to CSV format
        /// </summary>
        public StringBuilder SkippedRecordsFromCustomerMessageBuilder = new StringBuilder();

        public IList<LineReferences> LineReferenceItems
        {
            set { lstLineReferenceItems = value; }
            get { return lstLineReferenceItems; }
        }

        public long CustomerCode
        {
            set { this.customerCode = value; }
            get { return this.customerCode; }
        }

        public string CustomerName
        {
            set { this.customerName = value; }
            get { return this.customerName; }
        }

        public bool ContainsUpdateSupplement
        {
            get { return this.containsUpdateSupplement; }
            set { this.containsUpdateSupplement = value; }
        }

        public bool ContainsCancelSupplement
        {
            get { return this.containsCancelSupplement; }
            set { this.containsCancelSupplement = value; }
        }

        public string Remarks
        {
            get { return this.strRemarks; }
            set { this.strRemarks = value; }
        }

        public bool ValidationFailure
        {
            get { return this.bValidationFailure; }
            set { this.bValidationFailure = value; }
        }

        public string SkipTempFilePath
        {
            set { this.skipTempFilePath = value; }
            get { return this.skipTempFilePath; }
        }
    }
    public class LineReferences
    {
        string strSourceReferenceCode;
        string strLineNumber;
        string strEdiNumber;
        string strMaterialCode;
        string strCustomerCode;
        string strLineContentItems;
        public LineReferences(string sourceReferenceCode, string lineNumber, string materialCode)
        {
            this.strSourceReferenceCode = sourceReferenceCode;
            this.strLineNumber = lineNumber;
            this.strMaterialCode = materialCode;
        }

        public LineReferences(string sourceReferenceCode, string lineNumber, string materialCode, string ediNumber)
        {
            this.strSourceReferenceCode = sourceReferenceCode;
            this.strLineNumber = lineNumber;
            this.strMaterialCode = materialCode;
            this.strEdiNumber = ediNumber;
        }

        public LineReferences(string sourceReferenceCode, string lineNumber, string materialCode, string ediNumber, string customerCode, string lineContentItems)
        {
            this.strSourceReferenceCode = sourceReferenceCode;
            this.strLineNumber = lineNumber;
            this.strMaterialCode = materialCode;
            this.strEdiNumber = ediNumber;
            this.strCustomerCode = customerCode;
            this.strLineContentItems = lineContentItems;
        }

        public LineStatus StatusCode;
        public string BatchNumber{get;set;}

        public string SourceReferenceCode
        {
            get { return strSourceReferenceCode; }
        }
        public string LineNumber
        {
            get { return strLineNumber; }
        }
        public string EdiNumber
        {
            get { return strEdiNumber; }
        }
        public string MaterialCode
        {
            get { return this.strMaterialCode; }
        }
        public string CustomerCode
        {
            get { return this.strCustomerCode; }
        }

        public string LineContentItems
        {
            set { strLineContentItems = value; }
            get { return strLineContentItems; }
        }

    }
}