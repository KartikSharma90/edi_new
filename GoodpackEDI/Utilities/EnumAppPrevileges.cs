﻿
namespace GoodpackEDI.Utilities
{
    public enum EnumAppPrevileges
    {
        Administrator,        
        ShipmentConfirmation,
        SubsiMapping,
        AddLookup,
        SalesOrder,
        MappingConfiguration,
        DataLookUp,
        AddMapping,
        ScannedShipmentConfirmation,
        SalesOrderMassEdit,
        AdvancedShipmentNotification,
        SalesOrderCancel,
        ErrorEmailDetails,
     };
}