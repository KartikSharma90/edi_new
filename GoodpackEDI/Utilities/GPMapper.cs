﻿using GoodpackEDI.BusinessLayer.SCManagementLayer;
using GoodpackEDI.ViewModels;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using GoodpackEDI.BusinessLayer.SOManagementLayer;
using GoodpackEDI.BusinessLayer.SSCManagementLayer;
using GoodpackEDI.BusinessLayer.PSSCManagementLayer;
using GoodpackEDI.BusinessLayer.ASNManagementLayer;
using GoodpackEDI.Parsers;

namespace GoodpackEDI.Utilities
{
    public class GPMapper
    {
        ISCDataVerificationManager dataVerify = new SCDataVerificationManager();
        private IList<SourceMessage> sourceMessageLines;
        private Map mapper;
        private static readonly ILog log = LogManager.GetLogger(typeof(GPMapper));
        public MapOutput  ProcessMap(bool processOneLineOnly,string mapperFile,SCTransactionModel objCustomerMessage)
        {
            MapOutput mapOut = new MapOutput();
            mapOut.LineReferenceItems = new List<LineReferences>();
            mapOut.IsSuccess = true;
            int lineNum = 0;
            bool performFieldValidationAfterMapping = true;

            // Set Validation flag
            if (processOneLineOnly)
            {
                performFieldValidationAfterMapping = false;
            }

            //MapperSpecModel sourceMessage = null;
            SourceMessage sourceMessage = null;
            //lock (objLockObject)//A dummy object locked for synchronization and multi-threading.
            //{
            sourceMessage = dataVerify.mapperDetails(mapperFile);
            sourceMessage.TransactionType = objCustomerMessage.TransactionType;
            sourceMessage.Service = objCustomerMessage.Service;

            //if (mapperFile == "SumitomoSOMapper" || mapperFile == "SumitomoScMapper")
            //{
            //    sourceMessage.SubsiName = "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE";
            //}

            //}

            // If source message cannot be found, raise error and return control to caller
            if (sourceMessage == null)
            {
                mapOut.IsSuccess = false;
                mapOut.ErrorCode = ErrorCodes.V_SRC_SPEC;
                //objILog.Warn(
                //    string.Format(
                //        "Cannot find source message specification for batch ID {0}, customer {1} and transaction type {2}",
                //        gpCustomerMessage.BatchID.ToString(),
                //        gpCustomerMessage.CustomerName,
                //        gpCustomerMessage.TransactionType.ToString()
                //    )
                //);

                // Exit process
                return mapOut;
            }

            // Set line num value and header record
            if (sourceMessage.IsHeaderPresent)
            {
                lineNum = 1;
                mapOut.SkippedRecordsFromCustomerMessageBuilder.Append(sourceMessage.GetHeaderRow()).Append(Environment.NewLine);
            }

            //Parser
            bool blsIsParseSuccess = false;
            //MapOutput mapOutValue = new MapOutput();

            try
            {
                string errorDesc = string.Empty;

                if (objCustomerMessage.BatchFileSourceType == SOURCE_TYPES.WEB_SERVICE && objCustomerMessage.TransactionType == TRANSACTION_TYPES.PSSC)
                {
                    sourceMessageLines = SCDataVerificationManager.ParseFromWSObject(objCustomerMessage, sourceMessage, GPArchitecture.WSTypes.WSTypeAccessor.packerSSInfoArr, ref blsIsParseSuccess, ref errorDesc);
                }
                else if (mapperFile == GPArchitecture.Cache.Config.getConfig("YokohamaSOMapper") || GPArchitecture.Cache.Config.getConfigList("YokohamaSOMapper").Contains(mapperFile))
                {
                    sourceMessageLines = GenericDelimiterParser.YokohamaParse(sourceMessage, objCustomerMessage.Message, sourceMessage.IsHeaderPresent, ConstantUtilities.delimiters[0].ToCharArray()[0], ref blsIsParseSuccess, ref errorDesc);
                }
                else
                {
                    objCustomerMessage.Message = objCustomerMessage.Message.Replace("|", "| ");
                    sourceMessageLines =  SCDataVerificationManager.Parse(objCustomerMessage, sourceMessage, ref blsIsParseSuccess, ref errorDesc);
                    objCustomerMessage.Message = objCustomerMessage.Message.Replace("| ", "|");
                }
                //if (blsIsParseSuccess && sourceMessage.CustomerHeadCompany.ToUpper() == "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE")
                //{
                if ((blsIsParseSuccess && mapperFile == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder")) ||( blsIsParseSuccess && mapperFile == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation") )||( blsIsParseSuccess && GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(mapperFile)) || (blsIsParseSuccess && GPArchitecture.Cache.Config.getConfigList("SumitomoShipmentConfirmation").Contains(mapperFile)))
                {
                    //Adding integer suffix with duplicate SI on SUMITOMI customer
                    this.CreateSumitomoSuffix(sourceMessageLines);

                }

                if (!blsIsParseSuccess)
                {
                    mapOut.ErrorCode = ErrorCodes.V_PARSE_REQ;
                    mapOut.ErrorDesc = errorDesc;
                    mapOut.IsSuccess = false;
                    //objILog.Warn(
                    //    string.Format(
                    //        "Parsing failed for batch ID (0), customer {1} and transaction type {2}. {3}",
                    //        gpCustomerMessage.BatchID.ToString(),
                    //        gpCustomerMessage.CustomerName,
                    //        gpCustomerMessage.TransactionType.ToString(),
                    //        errorDesc
                    //    )
                    //);

                    // Exit process
                    return mapOut;
                }
            }
            catch (Exception exp)
            {
                 log.Error(" ProcessMap in GPMapper 1st try .processOneLineOnly:" + processOneLineOnly + ", mapperFile:" + mapperFile + ",objCustomerMessage:" + objCustomerMessage + ".  Exception : Message- " + exp.Message);
                 throw exp;
            }

            RefTransactionTypeModel objTargetMessageSpecs = null;
            objTargetMessageSpecs = GPSAPMessageSpec.GetTargetMessage(objCustomerMessage.TransactionType, TargetMessageFormat.TAB_DELIMITED);

            List<MsgSapSOLine> salesOrderMsg = new List<MsgSapSOLine>();
            List<MsgSapSCLine> shipmentConfMsg = new List<MsgSapSCLine>();
            List<MsgSapSSCLine> scannedSCMsg = new List<MsgSapSSCLine>();
            List<MsgSapASNLine> asnConfMsg = new List<MsgSapASNLine>();
            //IList<MsgSapFCLine> objMsgSapFCLine = new List<MsgSapFCLine>();
            //IList<MsgSapACILine> autoITRMsg = new List<MsgSapACILine>();
            //IList<MsgMapBPLine> binPairingMsg = new List<MsgMapBPLine>();
            List<MsgSapPSSCLine> scannedPSSCMsg = new List<MsgSapPSSCLine>();
            if (objCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                && objCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
            {
                mapper = GPSAPMessageSpec.MapperData(mapperFile);

                if (mapper == null)
                {
                    mapOut.IsSuccess = false;
                    mapOut.ErrorCode = ErrorCodes.E_MAP;
                    mapOut.ErrorDesc = string.Format(
                            "No mapping configuration found or enabled for batch ID {0}, customer {1} and transaction type {2}",
                            objCustomerMessage.BatchID.ToString(),
                            objCustomerMessage.CustomerName,
                            objCustomerMessage.TransactionType.ToString()
                        );

                    //objILog.Warn(mapOut.ErrorDesc);

                    // Exit process
                    return mapOut;
                }

                StringBuilder errorDescFormattedBuilder = new StringBuilder();

                errorDescFormattedBuilder.Append("<TABLE border='1' cellpadding=3><TR>")
                  .Append("<TD align='center'>Line No.</TD>")
                  .Append("<TD align='center'>Record</TD>")
                  .Append("<TD align='center'>Failed fields</TD>")
                  .Append("</TR>");

                StringBuilder errordescForLookupFailed = new StringBuilder();

                errordescForLookupFailed.Append("<TABLE border='1' cellpadding=3><TR>")
                  .Append("<TD align='center'>Line No.</TD>")
                  .Append("<TD align='center'>Failed fields</TD>")
                  .Append("<TD align='center'>Message</TD>")
                  .Append("</TR>");

                string fieldsWhichFailedValidation = "";
                int iRecordCountSkipValFailure = 0, iRecordCountFieldValFailure = 0, iTotal = 0;
                iTotal = sourceMessageLines.Count();

                //GPTools.UpdateBatchCounts(objCustomerMessage.BatchID, iTotal, 0, 0, 0, 0, 0, "Request");
                // Look through each line from the customer source message
                Dictionary<string, string> serviceValues;
                objCustomerMessage.mappingServiceValues = new List<Dictionary<string,string>>();
                foreach (SourceMessage objSourceMsg in sourceMessageLines)
                {
                    ErrorCodes errorCode = ErrorCodes.NONE;
                    string errorDesc = string.Empty;
                    bool isSuccess = false;
                    bool validationFailed = false;
                    string errordescLookup = string.Empty;
                    // Increment line num
                    lineNum++;
                    // iTotal++;

                    // IList<string> messageLineFields = this.Map(gpCustomerMessage, mapper, objTargetMessageSpecs, objSourceMsg, ref isSuccess, ref errorCode, ref errorDesc, ref fieldsWhichFailedValidation, performFieldValidationAfterMapping, ref validationFailed);
                    IList<string> messageLineFields = this.Map(objCustomerMessage, mapper, objTargetMessageSpecs, objSourceMsg, ref isSuccess, ref errorCode, ref errorDesc, ref fieldsWhichFailedValidation, performFieldValidationAfterMapping, ref validationFailed, ref errordescLookup, mapperFile);

                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) || mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                    {
                        serviceValues = new Dictionary<string, string>();
                        for (int i = 0; i < messageLineFields.Count; i++)
                        {
                            string serviceValueKey = objTargetMessageSpecs.Fields[i + 1].FieldName;
                            string serviceValueValue = messageLineFields[i];
                            if (serviceValueKey == ConstantUtilities.OrderQuantity || serviceValueKey == ConstantUtilities.SIQuantity)
                            {
                                StringBuilder orderQty = new StringBuilder(serviceValueValue);
                                for (int j = 0; j < orderQty.Length; j++)
                                {
                                    if (orderQty[j]=='0')
                                    {
                                      //  serviceValueValue.Remove(j, 1);
                                        serviceValueValue = serviceValueValue.TrimStart('0');
                                    }
                                }                              
                            }
                            
                            serviceValues.Add(serviceValueKey, serviceValueValue);
                        }

                        objCustomerMessage.mappingServiceValues.Add(serviceValues);
                    }
                    else
                    {
                        serviceValues = new Dictionary<string, string>();
                        for (int i = 0; i < messageLineFields.Count; i++)
                        {
                            string serviceValueKey = objTargetMessageSpecs.Fields[i + 1].FieldName;
                            string serviceValueValue = messageLineFields[i];
                            serviceValues.Add(serviceValueKey, serviceValueValue);
                        }

                        objCustomerMessage.mappingServiceValues.Add(serviceValues);
                    }
                    // Set map output success flag. Only set to true 
                    mapOut.IsSuccess = (mapOut.IsSuccess == false) ? false : isSuccess;

                    // Map validation failed
                    mapOut.ValidationFailure = (mapOut.ValidationFailure == true) ? true : validationFailed;

                    // So that the mapped record can be skipped.  V_FIELD overrides V_SKIPRECORDS
                    if (errorCode == ErrorCodes.V_SKIPRECORDS)
                    {
                        mapOut.ErrorCode = errorCode;
                        // Build table cells which will be inserted into the email notification body
                        errorDescFormattedBuilder.Append("<tr><td>" + lineNum + "</td><td>");
                        errorDescFormattedBuilder.Append(fieldsWhichFailedValidation + "</td><td>");
                        errorDescFormattedBuilder.Append(errorDesc + "</td></tr>");

                        // Append line which will be skipped 
                        mapOut.SkippedRecordsFromCustomerMessageBuilder.Append(objSourceMsg.OriginalLineContent).Append(Environment.NewLine);
                        iRecordCountSkipValFailure++;
                    }
                    if (errorCode == ErrorCodes.V_FAIL_LOOKUP_BP)
                    {
                        mapOut.ErrorCode = errorCode;
                        errordescForLookupFailed.Append("<tr><td>" + lineNum + "</td><td>");
                        errordescForLookupFailed.Append(errordescLookup + "</td><td>");
                        errordescForLookupFailed.Append("Lookup Failed" + "</td></tr>");
                    }
                    if (errorCode == ErrorCodes.V_FIELD)
                    {
                        iRecordCountFieldValFailure++;
                    }
                    // Mapping wasn’t successful, get error details
                    // and exit processing.
                    if (!mapOut.IsSuccess)
                    {
                        mapOut.ErrorCode = errorCode;
                        mapOut.ErrorDesc = errorDesc + " Line num: " + lineNum.ToString() + ". ";
                        objCustomerMessage.RecordCountTotal = iTotal;
                        objCustomerMessage.RecordCountFieldValFailure = iRecordCountFieldValFailure;
                        objCustomerMessage.RecordCountSkipValFailure = iRecordCountSkipValFailure;
                        //GPTools.UpdateBatchCounts(gpCustomerMessage.BatchID, gpCustomerMessage.RecordCountTotal, gpCustomerMessage.RecordCountSkipValFailure, gpCustomerMessage.RecordCountFieldValFailure,
                        //0, gpCustomerMessage.RecordCountSAPError, gpCustomerMessage.RecordCountSuccess, "Request");
                        return mapOut;
                    }
                    else if (validationFailed) // If validation failed, continue to next record
                    {
                        continue;
                    }

                    try
                    {
                        // Perform post map processing and generate final message
                        switch (objCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SO:
                                MsgSapSOLine mapSO = new MsgSapSOLine(messageLineFields);
                                mapSO.SOLineContent = objSourceMsg.OriginalLineContent;
                                salesOrderMsg.Add(mapSO);
                                break;
                            case TRANSACTION_TYPES.SC:
                                MsgSapSCLine mapSC = new MsgSapSCLine(messageLineFields);
                                mapSC.PSCLineContent = objSourceMsg.OriginalLineContent;
                                shipmentConfMsg.Add(mapSC);
                                break;
                            case TRANSACTION_TYPES.SSC:
                                MsgSapSSCLine mapSSC = new MsgSapSSCLine(messageLineFields);
                                mapSSC.PSSCLineContent = objSourceMsg.OriginalLineContent;
                                scannedSCMsg.Add(mapSSC);
                                break;
                            //case TRANSACTION_TYPES.PSC:
                            //    MsgSapSCLine mapPSC = new MsgSapSCLine(messageLineFields);
                            //    gpCustomerMessage.CustomerCode = mapPSC.getPackerCode();
                            //    mapPSC.PSCLineContent = objSourceMsg.OriginalLineContent;
                            //    shipmentConfMsg.Add(mapPSC);

                            //    break;
                            case TRANSACTION_TYPES.PSSC:
                                MsgSapPSSCLine mapPSSC = new MsgSapPSSCLine(messageLineFields);
                                objCustomerMessage.CustomerCode = mapPSSC.getPackerCode();
                                mapPSSC.PSSCLineContent = objSourceMsg.OriginalLineContent;
                                scannedPSSCMsg.Add(mapPSSC);
                                break;
                            case TRANSACTION_TYPES.ASN:
                                MsgSapASNLine mapASN = new MsgSapASNLine(messageLineFields);
                               // objCustomerMessage.CustomerCode = mapASN.getPackerCode();
                                mapASN.PSCLineContent = objSourceMsg.OriginalLineContent;
                                asnConfMsg.Add(mapASN);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error(" ProcessMap in GPMapper 2nd try .processOneLineOnly:" + processOneLineOnly + ", mapperFile:" + mapperFile + ",objCustomerMessage:" + objCustomerMessage + ".  Exception : Message- " + ex.Message);
                        mapOut.IsSuccess = false;
                        mapOut.ErrorCode = ErrorCodes.E_MAP;
                        mapOut.ErrorDesc = ex.Message + " Line num: " + lineNum.ToString() + ". ";
                        return mapOut;
                    }

                    // If there's a directive to process only the first line, then break process.
                    if (processOneLineOnly)
                    {
                        break;
                    }
                }//-- End foreach

                errordescForLookupFailed.Append("</table>");
                mapOut.ErrorLookupFailed = errordescForLookupFailed.ToString();
                errorDescFormattedBuilder.Append("</table>");
                mapOut.ErrorDescFormatted = errorDescFormattedBuilder.ToString();

                objCustomerMessage.RecordCountSkipValFailure = iRecordCountSkipValFailure;
                objCustomerMessage.RecordCountTotal = iTotal;
                objCustomerMessage.RecordCountFieldValFailure = iRecordCountFieldValFailure;
                objCustomerMessage.RecordCountSuccess = 0;
                //GPTools.UpdateBatchCounts(objCustomerMessage.BatchID, objCustomerMessage.RecordCountTotal, objCustomerMessage.RecordCountSkipValFailure, objCustomerMessage.RecordCountFieldValFailure,
                //   0, objCustomerMessage.RecordCountSAPError, objCustomerMessage.RecordCountSuccess, "Request");

                // If Map Out message is null or empty, then throw error
                switch (objCustomerMessage.TransactionType)
                {
                    case TRANSACTION_TYPES.SO:
                        if (salesOrderMsg.Count <= 0)
                        {
                            mapOut.IsSuccess = false;
                            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                            return mapOut;
                        }
                        break;
                    case TRANSACTION_TYPES.SC:
                        if (shipmentConfMsg.Count <= 0)
                        {
                            mapOut.IsSuccess = false;
                            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                            return mapOut;
                        }
                        break;
                    case TRANSACTION_TYPES.SSC:
                        if (scannedSCMsg.Count <= 0)
                        {
                            mapOut.IsSuccess = false;
                            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                            return mapOut;
                        }
                        break;
                    //case TRANSACTION_TYPES.PSC:
                    //    if (shipmentConfMsg.Count <= 0)
                    //    {
                    //        mapOut.IsSuccess = false;
                    //        mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                    //        mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                    //        return mapOut;
                    //    }
                    //    break;
                    case TRANSACTION_TYPES.PSSC:
                        if (scannedPSSCMsg.Count <= 0)
                        {
                            mapOut.IsSuccess = false;
                            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                            return mapOut;
                        }
                        break;
                    case TRANSACTION_TYPES.ASN:
                        if (asnConfMsg.Count <= 0)
                        {
                            mapOut.IsSuccess = false;
                            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
                            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";
                            return mapOut;
                        }
                        break;
                }
            }
            //else if (objCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
            //{
            //    //load FC mapping
            //    ErrorCodes errorCode = ErrorCodes.NONE;
            //    string errorDesc = string.Empty;
            //    bool isSuccess = false;

            //    if (sourceMessageLines != null && sourceMessageLines.Count > 0)
            //    {
            //        objMsgSapFCLine = Map_FC(objCustomerMessage, objTargetMessageSpecs, sourceMessageLines, processOneLineOnly, performFieldValidationAfterMapping, ref isSuccess, ref errorCode, ref errorDesc);

            //        // Update counters
            //        GPTools.UpdateBatchCounts(objCustomerMessage.BatchID
            //            , objCustomerMessage.RecordCountTotal
            //            , objCustomerMessage.RecordCountSkipValFailure
            //            , objCustomerMessage.RecordCountFieldValFailure
            //            , 0
            //            , objCustomerMessage.RecordCountSAPError
            //            , objCustomerMessage.RecordCountSuccess
            //            , "Request");

            //        // Check if skip on validation failure flag is set
            //        if (isSuccess && errorCode == ErrorCodes.V_SKIPRECORDS)
            //        {
            //            mapOut.ErrorCode = ErrorCodes.V_SKIPRECORDS;
            //            //    mapOut.ErrorDesc = errorDesc;
            //            mapOut.ErrorDescFormatted = errorDesc;
            //            mapOut.SkippedRecordsFromCustomerMessageBuilder = objCustomerMessage.SkippedRecordsFromFC;
            //        }

            //        if (objMsgSapFCLine.Count <= 0)
            //        {
            //            mapOut.IsSuccess = false;
            //            mapOut.ErrorCode = (mapOut.ErrorCode == ErrorCodes.NONE) ? ErrorCodes.E_MAP : mapOut.ErrorCode;
            //            mapOut.ErrorDesc += "Mapping resulted in empty output message.  Customer message may be empty or all records were skipped due to validation failure.";

            //            return mapOut;
            //        }
            //        else
            //        {
            //            mapOut.IsSuccess = isSuccess;
            //        }
            //    }
            //    else
            //    {
            //        mapOut.IsSuccess = false;
            //        mapOut.ErrorCode = ErrorCodes.E_MAP;
            //        mapOut.ErrorDesc = "Forecast message specification not found in database.";
            //    }



            //    if (!isSuccess)
            //    {
            //        mapOut.ErrorCode = ErrorCodes.E_MAP;
            //        mapOut.ErrorDesc = errorDesc;
            //        return mapOut;
            //    }

            //}
            //else if (objCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI)
            //{
            //    //load ACI mapping
            //    ErrorCodes errorCode = ErrorCodes.NONE;
            //    string errorDesc = string.Empty;
            //    string errorDescFormatted = string.Empty;
            //    bool isSuccess = false;

            //    if (sourceMessageLines != null && sourceMessageLines.Count > 0)
            //    {
            //        autoITRMsg = Map_ACI(objCustomerMessage, objTargetMessageSpecs, sourceMessageLines, processOneLineOnly,
            //            ref isSuccess, ref errorCode, ref errorDesc, ref errorDescFormatted);
            //        mapOut.IsSuccess = isSuccess;
            //    }
            //    else
            //    {
            //        mapOut.IsSuccess = false;
            //        mapOut.ErrorCode = ErrorCodes.E_MAP;
            //        mapOut.ErrorDesc = "Auto ITR message specification not found in database.";
            //    }

            //    if (!isSuccess)
            //    {
            //        mapOut.ErrorCode = errorCode;
            //        mapOut.ErrorDesc = errorDesc;
            //        mapOut.ErrorDescFormatted = errorDescFormatted;
            //        GPTools.UpdateBatchCounts(objCustomerMessage.BatchID, objCustomerMessage.RecordCountTotal, objCustomerMessage.RecordCountSkipValFailure, gpCustomerMessage.RecordCountFieldValFailure,
            //        0, objCustomerMessage.RecordCountSAPError, objCustomerMessage.RecordCountSuccess, "Request");
            //        return mapOut;
            //    }

            //}
            //else if (gpCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
            //{
            //    //load BP mapping
            //    ErrorCodes errorCode = ErrorCodes.NONE;
            //    string errorDesc = string.Empty;
            //    string errorDescFormatted = string.Empty;
            //    bool isSuccess = false;

            //    if (sourceMessageLines != null && sourceMessageLines.Count > 0)
            //    {
            //        binPairingMsg = Map_BP(objCustomerMessage, objTargetMessageSpecs, sourceMessageLines, processOneLineOnly,
            //            ref isSuccess, ref errorCode, ref  errorDesc);
            //        mapOut.IsSuccess = isSuccess;
            //    }
            //    else
            //    {
            //        mapOut.IsSuccess = false;
            //        mapOut.ErrorCode = ErrorCodes.E_MAP;
            //        mapOut.ErrorDesc = "Bin Pairing message specification not found in database.";
            //    }

            //    if (!isSuccess)
            //    {
            //        mapOut.ErrorCode = errorCode;
            //        mapOut.ErrorDesc = errorDesc;
            //        mapOut.ErrorDescFormatted = errorDescFormatted;
            //        GPTools.UpdateBatchCounts(objCustomerMessage.BatchID, objCustomerMessage.RecordCountTotal, objCustomerMessage.RecordCountSkipValFailure, objCustomerMessage.RecordCountFieldValFailure,
            //       0, objCustomerMessage.RecordCountSAPError, objCustomerMessage.RecordCountSuccess, "Request");
            //        return mapOut;
            //    }
            //}

            // Perform post processing
            switch (objCustomerMessage.TransactionType)
            {
                case TRANSACTION_TYPES.SO:
                    PostProcessingSO(objCustomerMessage,salesOrderMsg, mapOut);
                    break;
                case TRANSACTION_TYPES.SC:
                    PostProcessingSC(shipmentConfMsg, objCustomerMessage, mapOut);
                    break;
                //case TRANSACTION_TYPES.FC:
                //    PostProcessingFC(objMsgSapFCLine, mapOut);
                //    break;
                case TRANSACTION_TYPES.SSC:
                    PostProcessingSSC(scannedSCMsg, objCustomerMessage, mapOut);
                    break;
                //case TRANSACTION_TYPES.ACI:
                //    PostProcessingACI(autoITRMsg, mapOut);
                //    break;
                //case TRANSACTION_TYPES.BP:
                //    PostProcessingBP(binPairingMsg, objCustomerMessage, mapOut);
                //    break;
                //case TRANSACTION_TYPES.PSC:
                //    PostProcessingPSC(shipmentConfMsg, objCustomerMessage, mapOut);
                //    break;
                case TRANSACTION_TYPES.PSSC:
                    PostProcessingPSSC(scannedPSSCMsg, objCustomerMessage, mapOut);
                    break;
                case TRANSACTION_TYPES.ASN:
                    PostProcessingASN(asnConfMsg, objCustomerMessage, mapOut);
                    break;
            }
            return mapOut;
        }
        private void CreateSumitomoSuffix(IList<SourceMessage> sourceMessageLines)
        {
            try
            {
                string SIName = null;
                string SIValue = null;
                List<string> SIValueDetail = new List<string>();
                List<string> SIUniqueValue = new List<string>();
                List<string> duplicates = new List<string>();
                foreach (var msg in sourceMessageLines)
                {
                    for (int i = 0; i <= msg.Fields.Count - 1; i++)
                    {
                        SIName = msg.Fields[i].FieldName;
                        if (SIName.Contains("SI") && msg.Fields[i].Value != null)
                        {
                            SIValue = msg.Fields[i].Value;
                            SIValueDetail.Add(SIValue);
                        }
                    }
                }


                SIUniqueValue = SIValueDetail.Distinct().ToList();
                if (SIValueDetail.Count != SIUniqueValue.Count)
                {
                    duplicates = SIValueDetail.GroupBy(i => i)
                       .Where(g => g.Count() > 1)
                       .Select(g => g.Key).ToList();
                }
                foreach (var siSufixitem in duplicates)
                {
                    int j = 1;
                    foreach (var msg in sourceMessageLines)
                    {

                        for (int i = 0; i <= msg.Fields.Count - 1; i++)
                        {
                            if (msg.Fields[i].FieldName.Contains("SI") && msg.Fields[i].Value != null && msg.Fields[i].Value == siSufixitem)
                            {
                                msg.Fields[i].Value = msg.Fields[i].Value + "_" + j;
                                j++;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Error encountered while processing " + ex.Message);
                throw ex;
            }
        }

        private IList<string> Map(SCTransactionModel objGPCustomerMessage, Map objMap, RefTransactionTypeModel objTargetMessageSpecs, SourceMessage objSourceMessage, ref bool isSuccess, ref ErrorCodes errorCode, ref string errorDesc, ref string listOfFieldsSkippedDueToValidationFailure, bool performValidation, ref bool validationFailure, ref string errordescLookup,string mapperName)
        {
            string fieldName = string.Empty;
            isSuccess = true;
            validationFailure = false;
            StringBuilder errorDescStringBuilder = new StringBuilder();
            StringBuilder errordescLookupFailed = new StringBuilder();

            SAPMessageFieldsModel targetFieldSpec;
            listOfFieldsSkippedDueToValidationFailure = string.Empty;

            // List<> is list of message line fields.
            List<string> fieldList = new List<string>();

            //clear the FieldList string and create new one with source field values for SkipOnValidationFailure email
            for (int i = 0; i < objSourceMessage.Fields.Count; i++)
            {
                listOfFieldsSkippedDueToValidationFailure += objSourceMessage.Fields[i].Value + " , ";
            }
            try
            {
                foreach (MapItem mapItem in objMap.MapItems)
                {
                    // Init vars
                    string fieldValue = string.Empty;
                    bool isDateParsingSuccessful = true;
                    string dateParsingErrorMessage = string.Empty;
                    string validationMessage = string.Empty;
                    string bpFiledValue = string.Empty;
                    string sourceField1Value = null;
                    string sourceField2Value = null;
                    string sourceField3Value = null;

                    // Get source field values   
                    if (mapItem.SourceFieldSeq != 0)
                    {
                         sourceField1Value = (objSourceMessage.Fields.ContainsKey(mapItem.SourceFieldSeq-1)) ? objSourceMessage.Fields[mapItem.SourceFieldSeq - 1].Value : string.Empty;
                    }
                    else
                    {
                        sourceField1Value = string.Empty;
                    }
                    if (mapItem.SourceFieldSeq2 != 0)
                    {
                         sourceField2Value = (objSourceMessage.Fields.ContainsKey(mapItem.SourceFieldSeq2-1)) ? objSourceMessage.Fields[mapItem.SourceFieldSeq2 - 1].Value : string.Empty;
                    }
                    else
                    {
                        sourceField2Value = string.Empty;
                    }
                    //string sourceField2Value = (objSourceMessage.Fields.ContainsKey(mapItem.SourceFieldSeq2)) ? objSourceMessage.Fields[mapItem.SourceFieldSeq2-1].Value : string.Empty;
                    if (mapItem.SourceFieldSeq3 != 0)
                    {
                         sourceField3Value = (objSourceMessage.Fields.ContainsKey(mapItem.SourceFieldSeq3-1)) ? objSourceMessage.Fields[mapItem.SourceFieldSeq3 - 1].Value : string.Empty;
                    }
                    else
                    {
                        sourceField3Value = string.Empty;
                    }
                    // Get target field
                    targetFieldSpec = objTargetMessageSpecs.Fields[mapItem.TargetFieldSeq];
                    bool skipOnValidationFailure = targetFieldSpec.SkipOnValidationFailure;

                    // set field name
                    fieldName = targetFieldSpec.FieldName;

                    // Perform field mapping
                    switch (targetFieldSpec.InputType)
                    {
                        case TargetMessageInputTypes.FIXED:
                            fieldValue = targetFieldSpec.DefaultValue;
                            break;

                        case TargetMessageInputTypes.USER_FIXED:
                            fieldValue = mapItem.FixedValue;
                            break;

                        case TargetMessageInputTypes.MAP_FROM_SOURCE:
                            fieldValue = sourceField1Value;

                            break;

                        case TargetMessageInputTypes.DATE:
                            if (!string.IsNullOrEmpty(sourceField1Value))
                            {
                                if (mapItem.SourceDateFormat != "WW.YYYY")
                                {
                                    fieldValue = GPMapper.TryParseDate(objSourceMessage.SourceMessageFormat, targetFieldSpec.FieldType.ToString(), mapItem.SourceDateFormat, ref isDateParsingSuccessful, ref dateParsingErrorMessage, sourceField1Value);
                                }
                                else
                                {
                                    string arrivalDateString = GPMapper.TryParseDate(objSourceMessage.SourceMessageFormat, targetFieldSpec.FieldType.ToString(), mapItem.SourceDateFormat, ref isDateParsingSuccessful, ref dateParsingErrorMessage, sourceField1Value);
                                    sourceField1Value = (objSourceMessage.Fields.ContainsKey(mapItem.SourceFieldSeq3 - 1)) ? objSourceMessage.Fields[mapItem.SourceFieldSeq3 - 1].Value : string.Empty;

                                    fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, sourceField1Value, sourceField2Value, arrivalDateString, targetFieldSpec.FieldType.ToString(), mapperName);

                                   // fieldValue = GPMapper.TryParseDate(objSourceMessage.SourceMessageFormat, targetFieldSpec.FieldType.ToString(), mapItem.SourceDateFormat, ref isDateParsingSuccessful, ref dateParsingErrorMessage, sourceField1Value);
                                }
                               
                            }
                            break;

                        case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD:
                            if (!string.IsNullOrEmpty(sourceField1Value))
                            {
                                fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, sourceField1Value,mapperName);
                            }
                            break;

                        case TargetMessageInputTypes.MAP_FROM_SOURCE_WITH_SUBSTRING:

                            if  (mapItem.SourceFieldSeq1SubStringLength != 0 && mapItem.SourceFieldSeq1SubStringStartPosition !=0)
                            {
                                fieldValue = GetSubString(sourceField1Value, mapItem.SourceFieldSeq1SubStringStartPosition.Value, mapItem.SourceFieldSeq1SubStringLength.Value);

                            }
                            else
                            {
                                fieldValue = sourceField1Value;
                            }
                            break;

                        case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING:
                            // Perform Substring
                            if ( mapItem.SourceFieldSeq1SubStringLength != 0 && mapItem.SourceFieldSeq1SubStringStartPosition != 0 )
                            
                            {
                                fieldValue =GetSubString(sourceField1Value, mapItem.SourceFieldSeq1SubStringStartPosition.Value, mapItem.SourceFieldSeq1SubStringLength.Value);
                            }
                            else
                            {
                                fieldValue = sourceField1Value;
                            }
                            // run operation
                            fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, fieldValue,mapperName);
                            break;

                        case TargetMessageInputTypes.MAP_FROM_SOURCE_OR_USER_FIXED_INPUT:
                            if (!string.IsNullOrEmpty(sourceField1Value))
                            {
                                fieldValue = sourceField1Value;
                            }
                            else
                            {
                                fieldValue = mapItem.FixedValue;
                            }
                            break;

                        case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED:
                            if (!string.IsNullOrEmpty(sourceField1Value))
                            {
                                fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, sourceField1Value,mapperName);
                            }
                            else
                            {
                                fieldValue = mapItem.FixedValue;
                            }
                            break;

                        case TargetMessageInputTypes.OPERATION_ON_SOURCE_FIELDS2:
                            fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, sourceField2Value, mapperName);
                            break;

                        case TargetMessageInputTypes.MAP_ETD_FROM_SOURCE:
                            // set source fields
                            // If mapping type is direct, then perform direct date mapping
                            if (mapItem.MappingType != "2")
                            {
                                fieldValue = GPMapper.TryParseDate(objSourceMessage.SourceMessageFormat, targetFieldSpec.FieldType.ToString(), mapItem.SourceDateFormat, ref isDateParsingSuccessful, ref dateParsingErrorMessage, sourceField1Value);
                            }
                            else // Perform operation
                            {
                                // First, try to parse the arrival date.  Arrival date is supposedly stored in source field 3
                                string arrivalDateString = GPMapper.TryParseDate(objSourceMessage.SourceMessageFormat, targetFieldSpec.FieldType.ToString(), mapItem.SourceDateFormat, ref isDateParsingSuccessful, ref dateParsingErrorMessage, sourceField3Value);

                                fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam, sourceField1Value, sourceField2Value, arrivalDateString, targetFieldSpec.FieldType.ToString(), mapperName);
                            }
                            break;

                        case TargetMessageInputTypes.DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD:
                            // Perform Substring
                            if ((mapItem.SourceFieldSeq1SubStringLength != null && mapItem.SourceFieldSeq1SubStringStartPosition != null)&&(mapItem.SourceFieldSeq1SubStringLength!=0))
                            {
                                fieldValue = GetSubString(sourceField1Value, mapItem.SourceFieldSeq1SubStringStartPosition.Value, mapItem.SourceFieldSeq1SubStringLength.Value);
                            }
                            else
                            {
                                fieldValue = sourceField1Value;
                            }
                            // Perform Operation
                            if (!string.IsNullOrEmpty(mapItem.MappingType) && mapItem.MappingType.ToUpper() != "NO OPERATION")
                            {
                                // run operation by passing value of mapping type ie CalculateCheckDigitIn8DigitBarcode or as applicable to operation name
                                string calculateCheckDigitFieldValue = null;
                                string binPairingLookupFieldValue = null;
                                calculateCheckDigitFieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, mapItem.MappingType, targetFieldSpec.OperationParam, fieldValue, mapperName);
                                binPairingLookupFieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, mapItem.MappingType, targetFieldSpec.OperationParam, fieldValue, mapperName);
                                if (calculateCheckDigitFieldValue != null)
                                {
                                    fieldValue = calculateCheckDigitFieldValue;
                                }
                                else if (binPairingLookupFieldValue != null)
                                {
                                    fieldValue = binPairingLookupFieldValue;
                                }
                                if (fieldValue == string.Empty)
                                {
                                    bpFiledValue = sourceField1Value;
                                }
                            }
                            break;

                        case TargetMessageInputTypes.OPERATION_NO_INPUT:
                            fieldValue = OperationManager.ExecuteOperation(objGPCustomerMessage, targetFieldSpec.OperationName, targetFieldSpec.OperationParam);
                            break;

                        default:
                            fieldValue = String.Empty;
                            break;
                    }//-- END SWITCH

                    // Trim value
                    if (fieldValue != null)
                    {
                        fieldValue = fieldValue.Trim();
                    }
                    //Validation is specially done for SSC to collect the Lookup failed Barcodes
                    if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SSC && fieldValue == string.Empty && mapItem.MappingType == "BinPairingLookup")
                    {
                        //stringbuilder to collect lookup failed barcodes.                                         
                        errorCode = ErrorCodes.V_FAIL_LOOKUP_BP;
                        errordescLookupFailed.Append(bpFiledValue);
                        errordescLookup = errordescLookupFailed.ToString();
                    }

                    if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO && fieldValue == string.Empty && mapItem.MappingType == "TransitTime")
                    {
                        errorCode = ErrorCodes.E_MAP;
                       // isSuccess = false;
                        errorDescStringBuilder.Append("Error validating TransitTime ")
                                                        .Append(targetFieldSpec.FieldName);
                                                        
                        errordescLookupFailed.Append("Error validating TransitTime ");
                        errordescLookup = errordescLookupFailed.ToString();
                    }

                    // Validate field value.  Do not validate if isSuccess is
                    // already false;
                    //if (
                    //     (
                    //         !GPValidationAndBusinessRules.Validate(ref fieldValue, objGPCustomerMessage, targetFieldSpec, ref skipOnValidationFailure, ref validationMessage)
                    //         || !ValidateField(targetFieldSpec, ref fieldValue)
                    //         || !isDateParsingSuccessful
                    //         || !GPValidationAndBusinessRules.SSCValidate2DPinNo(fieldValue, objGPCustomerMessage, targetFieldSpec, mapItem.MappingType, ref skipOnValidationFailure, ref validationMessage)
                    //     ) // check if validation failed
                    //     && isSuccess
                    //     && performValidation
                    //     )
                    //{
                    //    validationFailure = true;

                    //    //set another error code if the field fails validation but allows skip
                    //    if (skipOnValidationFailure)
                    //    {
                    //        errorCode = ErrorCodes.V_SKIPRECORDS;
                    //        errorDescStringBuilder.Append(targetFieldSpec.FieldName);

                    //        // Check if validationMessage value is set
                    //        if (!string.IsNullOrEmpty(validationMessage))
                    //        {
                    //            errorDescStringBuilder.Append("(")
                    //                .Append(validationMessage)
                    //                .Append(")");
                    //        }

                    //        errorDescStringBuilder.Append(",");
                    //    }

                    //    else
                    //    {
                    //        isSuccess = false;
                    //        errorCode = ErrorCodes.V_FIELD;
                    //        errorDescStringBuilder.Append("Error validating target field ")
                    //            .Append(targetFieldSpec.FieldName)
                    //            .Append(" mapped with value '")
                    //            .Append(sourceField1Value)
                    //            .Append("'. ")
                    //            .Append(dateParsingErrorMessage)
                    //            .Append(targetFieldSpec.ValidationError);
                    //    }
                    //}

                    if (isSuccess)
                    {
                        fieldList.Add(fieldValue);
                    }
                    // If validation error encountered, continue validating.
                    else if (!isSuccess && errorCode != ErrorCodes.V_FIELD && errorCode != ErrorCodes.V_SKIPRECORDS)
                    {
                        // break loop
                        break;
                    }

                }//-- end foreach
            }
            catch (Exception ex)
            {
                log.Error(" Map in GPMapper.objGPCustomerMessage:" + objGPCustomerMessage + ", objMap:" + objMap + ",objTargetMessageSpecs:" + objTargetMessageSpecs + ", objSourceMessage:" + objSourceMessage + ", mapperName:" + mapperName + ".  Exception : Message- " + ex.Message);
                isSuccess = false;
                errorCode = ErrorCodes.E_MAP;
                errorDesc = string.Format("Error encountered while mapping field {0}. Details: {1}", fieldName, ex.Message);
                //objILog.Error(errorDesc);
            }

            // Set error desc
            errorDesc = errorDescStringBuilder.ToString();

            return fieldList;
        }

        public static string TryParseDate(string sourceMessageFormat, string targetFieldDateFormat, string sourceMessageDateFormat, ref bool isDateParsingSuccessful, ref string dateParsingErrorMessage, string dateStringToBeParsed)
        {
            string returnVal = string.Empty;

            if (!string.IsNullOrEmpty(dateStringToBeParsed))
            {
                // Default flag to false
                isDateParsingSuccessful = false;

                // Try to parse with dd-mm-yyyy for Excel, else use mapped date format
                if (sourceMessageFormat == MessageFormats.Excel)
                {
                    if (sourceMessageDateFormat == "WW.YYYY")
                    {
                        returnVal = SCDataVerificationManager.ExtractDateElements(targetFieldDateFormat, sourceMessageDateFormat, dateStringToBeParsed, out isDateParsingSuccessful, out dateParsingErrorMessage);
                    }
                    else
                    {
                        returnVal = SCDataVerificationManager.ExtractDateElements(targetFieldDateFormat, "dd-MM-yyyy", dateStringToBeParsed, out isDateParsingSuccessful, out dateParsingErrorMessage);
                    }
                    }
                /* if (sourceMessageFormat == "BFT")
                 {
                     returnVal = GPTools.ExtractDateElements(targetFieldDateFormat, "dd.MM.yyyy", dateStringToBeParsed, out isDateParsingSuccessful, out dateParsingErrorMessage);
                 }*/
                if (!isDateParsingSuccessful)
                {
                    returnVal = SCDataVerificationManager.ExtractDateElements(targetFieldDateFormat, sourceMessageDateFormat, dateStringToBeParsed, out isDateParsingSuccessful, out dateParsingErrorMessage);
                }

                // If source message format is not Excel or Excel date parsing failed, run date parse again using user provided date format

            }

            // set value of returnVal with date string so source field value shows up correctly

            return returnVal;
        }

        /// <summary>
        /// Get the SubString of the filedvalue using startposition and length.
        /// </summary>
        public static string GetSubString(string sourcefieldvalue, int startposition, int givenlength)
        {
            // Init vars
            string substring = string.Empty;

            // Check value of given length. If less than one, return empty string
            if (givenlength < 1 || startposition > sourcefieldvalue.Length)
            {
               // return substring;
                return sourcefieldvalue;
            }

            // Set value of startPosition to start at index 0
            if (startposition < 1)
            {
                startposition = 0;
            }
            else
            {
                startposition = startposition - 1;
            }

            // Set the bounds of substring to pull from the main string
            int substringPositionAndLength = startposition + givenlength;

            // Check if the bounds of the substring goes beyong the length of the main string.
            // If it does, reset the substring length so that the substring is within the
            // boundary of the main string
            if (sourcefieldvalue.Length < substringPositionAndLength)
            {
                givenlength = sourcefieldvalue.Length - startposition;
            }

            // Perform substring
            substring = sourcefieldvalue.Substring(startposition, givenlength);

            // return substring
            return substring;
        }//-- End GetSubString()


        private void PostProcessingSC(List<MsgSapSCLine> msg, SCTransactionModel gpCustomerMessage, MapOutput objMapOutput)
        {
            // <Source Reference, <ITR Number, line number>>
            Dictionary<string, KeyValuePair<string, int>> customerPOLineCounter = new Dictionary<string, KeyValuePair<string, int>>();
            bool usedITRNumberAsSourceRef;
            objMapOutput.OutputMessage = new StringBuilder();
            objMapOutput.LineReferenceItems = new List<LineReferences>();

            foreach (MsgSapSCLine objMsgSapSCLine in msg)
            {
                // Init vars
                String sourceRef;    

                if (objMsgSapSCLine.SINumber != string.Empty)
                {
                    sourceRef = objMsgSapSCLine.SINumber;
                    usedITRNumberAsSourceRef = false;
                }
                else
                {
                    sourceRef = SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000");
                    usedITRNumberAsSourceRef = true;
                }


                // If the source reference is used in a previous line, then
                // re-use the ITR number associated to that source reference
                if (customerPOLineCounter.ContainsKey(sourceRef))
                {
                    // increment line number, create new KeyValue pair
                    customerPOLineCounter[sourceRef] = new KeyValuePair<string, int>(customerPOLineCounter[sourceRef].Key, customerPOLineCounter[sourceRef].Value + 10);
                }
                else
                {
                    if (usedITRNumberAsSourceRef)
                    {
                        customerPOLineCounter.Add(
                            sourceRef,
                            new KeyValuePair<string, int>(
                                sourceRef,
                                10
                            )
                        );
                    }
                    else
                    {
                        customerPOLineCounter.Add(
                            sourceRef,
                            new KeyValuePair<string, int>(
                                SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000"),
                                10
                            )
                        );
                    }
                }

                // Set ITR Number and Item Number
                objMsgSapSCLine.ITRNumber = customerPOLineCounter[sourceRef].Key;
                objMsgSapSCLine.ItemNo = customerPOLineCounter[sourceRef].Value.ToString();

                // Get message and store in output.
                objMsgSapSCLine.GenerateSAPMessage();

                // If first run, set customer code and name. Otherwise, add new line to output message
                if (objMapOutput.OutputMessage.Length > 0)
                {
                    objMapOutput.OutputMessage.Append(Environment.NewLine);
                }
                else
                {
                    long customerCode;

                    // Convert the customer code in the SC message to long
                    if (long.TryParse(objMsgSapSCLine.CustomerNumber, out customerCode))
                    {
                        objMapOutput.CustomerCode = customerCode;
                    }

                    // Get the customer details to be able to pull the customer name
                    EntitiesModel customer = SCDataVerificationManager.GetCustomer(objMapOutput.CustomerCode);
                    if (customer != null)
                    {
                        objMapOutput.CustomerName = customer.CustomerName;
                    }
                }

                objMapOutput.OutputMessage.Append(objMsgSapSCLine.Message);

                // Add to reference code and line item container
                objMapOutput.LineReferenceItems.Add(new LineReferences(sourceRef, objMsgSapSCLine.ItemNo, objMsgSapSCLine.MaterialNumber, objMsgSapSCLine.ITRNumber, objMsgSapSCLine.CustomerNumber, objMsgSapSCLine.PSCLineContent));
            }
        }

        private void PostProcessingASN(List<MsgSapASNLine> msg, SCTransactionModel gpCustomerMessage, MapOutput objMapOutput)
        {
            // <Source Reference, <ITR Number, line number>>
            Dictionary<string, KeyValuePair<string, int>> customerPOLineCounter = new Dictionary<string, KeyValuePair<string, int>>();
            bool usedITRNumberAsSourceRef;
            objMapOutput.OutputMessage = new StringBuilder();
            objMapOutput.LineReferenceItems = new List<LineReferences>();

            foreach (MsgSapASNLine objMsgSapSCLine in msg)
            {
                // Init vars
                String sourceRef;

                if (objMsgSapSCLine.SINumber != string.Empty)
                {
                    sourceRef = objMsgSapSCLine.SINumber;
                    usedITRNumberAsSourceRef = false;
                }
                else
                {
                    sourceRef = SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000");
                    usedITRNumberAsSourceRef = true;
                }


                // If the source reference is used in a previous line, then
                // re-use the ITR number associated to that source reference
                if (customerPOLineCounter.ContainsKey(sourceRef))
                {
                    // increment line number, create new KeyValue pair
                    customerPOLineCounter[sourceRef] = new KeyValuePair<string, int>(customerPOLineCounter[sourceRef].Key, customerPOLineCounter[sourceRef].Value + 10);
                }
                else
                {
                    if (usedITRNumberAsSourceRef)
                    {
                        customerPOLineCounter.Add(
                            sourceRef,
                            new KeyValuePair<string, int>(
                                sourceRef,
                                10
                            )
                        );
                    }
                    else
                    {
                        customerPOLineCounter.Add(
                            sourceRef,
                            new KeyValuePair<string, int>(
                                SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000"),
                                10
                            )
                        );
                    }
                }

                // Set ITR Number and Item Number
                objMsgSapSCLine.ITRNumber = customerPOLineCounter[sourceRef].Key;
                objMsgSapSCLine.ItemNo = customerPOLineCounter[sourceRef].Value.ToString();

                // Get message and store in output.
                objMsgSapSCLine.GenerateSAPMessage();

                // If first run, set customer code and name. Otherwise, add new line to output message
                if (objMapOutput.OutputMessage.Length > 0)
                {
                    objMapOutput.OutputMessage.Append(Environment.NewLine);
                }
                else
                {
                    long customerCode;

                    // Convert the customer code in the SC message to long
                    if (long.TryParse(objMsgSapSCLine.CustomerNumber, out customerCode))
                    {
                        objMapOutput.CustomerCode = customerCode;
                    }

                    // Get the customer details to be able to pull the customer name
                    EntitiesModel customer = SCDataVerificationManager.GetCustomer(objMapOutput.CustomerCode);
                    if (customer != null)
                    {
                        objMapOutput.CustomerName = customer.CustomerName;
                    }
                }

                objMapOutput.OutputMessage.Append(objMsgSapSCLine.Message);

                // Add to reference code and line item container
                objMapOutput.LineReferenceItems.Add(new LineReferences(sourceRef, objMsgSapSCLine.ItemNo, objMsgSapSCLine.MaterialNumber, objMsgSapSCLine.ITRNumber, objMsgSapSCLine.CustomerNumber, objMsgSapSCLine.PSCLineContent));
            }
        }

        private void PostProcessingSO(SCTransactionModel gpCustomerMessage, List<MsgSapSOLine> msg, MapOutput objMapOutput)
        {
            Dictionary<string, int> customerPOLineCounter = new Dictionary<string, int>();
            objMapOutput.OutputMessage = new StringBuilder();
            objMapOutput.LineReferenceItems = new List<LineReferences>();
            foreach (MsgSapSOLine objMsgSapSOLine in msg)
            {
                // Set supplement flags
                if (objMsgSapSOLine.supplementFlag == "C" /*Constants.cancelSupplement*/)
                {
                    objMapOutput.ContainsCancelSupplement = true;
                    objMapOutput.Remarks = "One of the lines of this order is a cancel supplement.";
                }
                else if (objMsgSapSOLine.supplementFlag == "U"/*Constants.updateSupplement*/)
                {
                    objMapOutput.ContainsUpdateSupplement = true;
                    objMapOutput.Remarks = "One of the lines of this order is an update supplement.";
                }

                // If PO number exists, add to counter.
                // If not, add PO number with counter value set to 10
                if (customerPOLineCounter.ContainsKey(objMsgSapSOLine.CustPOnumber))
                {
                    customerPOLineCounter[objMsgSapSOLine.CustPOnumber] += 10;
                }
                else
                {
                    customerPOLineCounter.Add(objMsgSapSOLine.CustPOnumber, 10);
                }

                // Set the contract number.  Need to perform this post-processing in case the
                // customer request contain line requests for different subsidiaries.
                objMsgSapSOLine.ReferencedContractNumber = OperationManager.ExecuteOperation(gpCustomerMessage, "ContractNumberLookup_By_CustomerCode_Direct", objMsgSapSOLine.SoldToParty);


                // Update item number value
                objMsgSapSOLine.SOItemNumber = customerPOLineCounter[objMsgSapSOLine.CustPOnumber].ToString();

                // Get message and store in output.
                objMsgSapSOLine.GenerateSAPMessage();

                // If first run, set customer code and name. Otherwise, add new line to output message
                if (objMapOutput.OutputMessage.Length > 0)
                {
                    objMapOutput.OutputMessage.Append(Environment.NewLine);
                }
                else
                {
                    // Parse the SoldToParty field, which holds the customer code.
                    long customerCode;
                    if (long.TryParse(objMsgSapSOLine.SoldToParty, out customerCode))
                    {
                        objMapOutput.CustomerCode = customerCode;
                    }

                    // commented by vivek 
                    // Perform customer name lookup in the customer master data
                    /*    Customer customer = Accessors.EntityMaster.GetCustomer(objMapOutput.CustomerCode);
                        if (customer != null)
                        {
                            objMapOutput.CustomerName = customer.CustomerName;
                        }*/
                }
                objMapOutput.OutputMessage.Append(objMsgSapSOLine.Message);

                // Add to reference code and line item container
                objMapOutput.LineReferenceItems.Add(new LineReferences(objMsgSapSOLine.CustPOnumber, objMsgSapSOLine.SOItemNumber, objMsgSapSOLine.Material, null, objMsgSapSOLine.SoldToParty, objMsgSapSOLine.SOLineContent));
            }
        }
        private void PostProcessingSSC(List<MsgSapSSCLine> msg, SCTransactionModel gpCustomerMessage, MapOutput objMapOutput)
        {
            //<Source Reference, <ITR Number, line number>>
            objMapOutput.OutputMessage = new StringBuilder();
            objMapOutput.LineReferenceItems = new List<LineReferences>();
            Dictionary<string, string> batchITR = new Dictionary<string, string>();
            foreach (MsgSapSSCLine objMsgSapSSLine in msg)
            {
                // Set ITR Number and Item Number
                string ITRNumber;
                if (batchITR.TryGetValue(objMsgSapSSLine.BatchNumber, out ITRNumber))
                    objMsgSapSSLine.ITRNumber = ITRNumber;
                else
                {
                    objMsgSapSSLine.ITRNumber = SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000");
                    batchITR.Add(objMsgSapSSLine.BatchNumber, objMsgSapSSLine.ITRNumber);
                }

                // Get message and store in output.
                objMsgSapSSLine.GenerateSAPMessage();

                // If first run, set customer code and name. Otherwise, add new line to output message
                if (objMapOutput.OutputMessage.Length > 0)
                {
                    objMapOutput.OutputMessage.Append(Environment.NewLine);
                }
                else
                {
                    long customerCode;
                    // Convert the customer code in the SC message to long
                    if (long.TryParse(objMsgSapSSLine.CustomerCode, out customerCode))
                    {

                        objMapOutput.CustomerCode = customerCode;
                    }

                    // Get the customer details to be able to pull the customer name
                    EntitiesModel customer = SCDataVerificationManager.GetCustomer(objMapOutput.CustomerCode);
                    if (customer != null)
                    {
                        objMapOutput.CustomerName = customer.CustomerName;
                    }

                }

                objMapOutput.OutputMessage.Append(objMsgSapSSLine.Message);

                // Add to reference code and line item container
                objMapOutput.LineReferenceItems.Add(new LineReferences(objMsgSapSSLine.CustomerReferenceNumber, "10", objMsgSapSSLine.MaterialNumber,
                    objMsgSapSSLine.ITRNumber, objMsgSapSSLine.CustomerCode, objMsgSapSSLine.PSSCLineContent));
            }
        }

        private void PostProcessingPSSC(List<MsgSapPSSCLine> msg, SCTransactionModel gpCustomerMessage, MapOutput objMapOutput)
        {
            //<Source Reference, <ITR Number, line number>>
            objMapOutput.OutputMessage = new StringBuilder();
            objMapOutput.LineReferenceItems = new List<LineReferences>();
            Dictionary<string, string> batchITR = new Dictionary<string, string>();
            foreach (MsgSapPSSCLine objMsgSapSSLine in msg)
            {
                // Set ITR Number and Item Number
                string ITRNumber;
                if (batchITR.TryGetValue(objMsgSapSSLine.BatchNumber, out ITRNumber))
                    objMsgSapSSLine.ITRNumber = ITRNumber;
                else
                {
                    objMsgSapSSLine.ITRNumber = SCDataVerificationManager.GenerateSequenceNum("SC").ToString("0000000000");
                    batchITR.Add(objMsgSapSSLine.BatchNumber, objMsgSapSSLine.ITRNumber);
                }

                // Get message and store in output.
                objMsgSapSSLine.GenerateSAPMessage();

                // If first run, set customer code and name. Otherwise, add new line to output message
                if (objMapOutput.OutputMessage.Length > 0)
                {
                    objMapOutput.OutputMessage.Append(Environment.NewLine);
                }
                else
                {



                    //commented the below line for PSSC transaction by vivek , customer code= packer code


                    /*
   
                     long customerCode;
                                      // Convert the customer code in the SC message to long
                                      if (long.TryParse(objMsgSapSSLine.CustomerCode, out customerCode))
                                      {

                                          objMapOutput.CustomerCode = customerCode;
                                      }

                                      // Get the customer details to be able to pull the customer name
                                      Customer customer = Accessors.EntityMaster.GetCustomer(objMapOutput.CustomerCode);
                                      if (customer != null)
                                      {
                                          objMapOutput.CustomerName = customer.CustomerName;
                                      }

                  */

                    // assigning the values inorder to avoid null value vivek
                    if (objMapOutput.CustomerCode < 0) { objMapOutput.CustomerCode = gpCustomerMessage.CustomerCode; }
                    // if (objMapOutput.CustomerName == "") { objMapOutput.CustomerName = gpCustomerMessage.CustomerName;}
                }

                objMapOutput.OutputMessage.Append(objMsgSapSSLine.Message);

                // Add to reference code and line item container
                objMapOutput.LineReferenceItems.Add(new LineReferences(objMsgSapSSLine.BinNo, "10", objMsgSapSSLine.MaterialNumber,
                    objMsgSapSSLine.ITRNumber, gpCustomerMessage.CustomerCode.ToString(), objMsgSapSSLine.PSSCLineContent));
            }
        }
    }
}