﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

//namespace GoodpackEDI.Utilities
//{
//    public class SupportedFileTypes
//    {
//        private static List<string> objFileTypes = null;
//        public static List<string> GetSupportedFileTypes()
//        {
//            if (objFileTypes == null)
//            {
//                CacheDAO objCacheDAO;
//                objFileTypes = new List<string>();
//                objCacheDAO = new CacheDAO(DBConnection.SqlConnString);
//                DataTable objCFGtable = objCacheDAO.loadTable(CacheResource.MSTR_FILE_TYPES);
//                foreach (DataRow drCFG in objCFGtable.Rows)
//                {
//                    objFileTypes.Add(drCFG["FileType"].ToString());
//                }
//            }
//            return objFileTypes;
//        }
//        public static bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
//        {
//            bool blnValidFileType = false;
//            IList<string> lstFileExtns = GetSupportedFileTypes();
//            enumFileType = FILE_TYPES.CSV;
//            foreach (string strFileExt in lstFileExtns)
//            {
//                if (strFileName.EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
//                {
//                    if (strFileExt.ToLower() == ".csv")
//                        enumFileType = FILE_TYPES.CSV;
//                    else
//                        enumFileType = FILE_TYPES.TEXT;
//                    blnValidFileType = true;
//                    break;
//                }
//            }
//            return blnValidFileType;
//        }

//    }
//}