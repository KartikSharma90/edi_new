﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public static class TransactionTypes
    {
        public static string DF = "Demand Forecast";
        public static string SC = "Shipment Confirmation";
        public static string SO = "Sales Order";
        public static string SSC = "Scanned Shipment Confirmation";
        public static string BP = "Bin Pairing";
        public static string PSC = "Packer Shipment Confirmation";
        public static string PSSC = "Packer Scanned Shipment Confirmation";
        public static string BFT = "Brazil File Translation";
        public static string ACI = "Auto Collection ITR";
        public static string ASN = "Advanced Shipment Notification";
    }
}