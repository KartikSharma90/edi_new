﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class ConstantUtility
    {
        public static string HQUser = "HQ User";
        public static string SubsiUser = "Subsi User";
        public static string HQAdmin = "HQ Admin";
        public static string HQAdminUser = "HQ Admin User";
    }
}