﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class SOChangeCancelSAPFields
    {
        public const string Customer = "Customer";
        public const string CustomerPONumber = "CustomerPONumber";
        public const string Requestdeliverydate = "Requestdeliverydate";
        public const string Material = "Material";
        public const string Packer = "Packer";
        public const string OrderQuantity = "OrderQuantity";
        public const string ReasonforETDchange = "ReasonforETDchange";
        public const string ReasonforSKUchange = "ReasonforSKUchange";
        public const string ReasonforQuantityChange = "ReasonforQuantityChange";
        public const string ReasonforCancellation = "ReasonforCancellation";
       
    }
}