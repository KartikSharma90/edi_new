﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public static class SOSapMessageFields
    {
         public const string SalesDocumentType = "Sales Document Type";
         public const string SalesOrganization = "Sales Organization";
         public const string DistributionChannel = "Distribution Channel";
         public const string Division = "Division";
         public const string SalesOffice = "Sales Office";
         public const string CustPOnumber = "Cust PO number";
         public const string CustomerPOdate = "Customer PO date";
         public const string CustomerSOnumber = "Customer SO number";
         public const string Customer = "Customer";
         public const string Requestdeliverydate = "Request delivery date";
         public const string Orderdate  = "Order date ";
         public const string HeaderRemarks = "Header Remarks";
         public const string SOItemnumber = "SO Item number";
         public const string ReferencedContractnumber = "Referenced Contract number";
         public const string Material = "Material";
         public const string Packer = "Packer";
         public const string Consignee = "Consignee";
         public const string DeliveryDate = "Delivery Date";
         public const string Orderquantity = "Order quantity";
         public const string UOM = "UOM";
         public const string VesselETD = "Vessel ETD";
         public const string VesselETA = "VesselETA";
         public const string Vesselname = "Vessel name";
         public const string Voyagereference  = "Voyage reference";
         public const string Shippingline = "Shipping line";
         public const string POL = "POL";
         public const string POD = "POD";
         public const string ItemRemarks = "Item Remarks";
         public const string DeliveryBlockOrders = "Delivery Block Orders";
         public const string SupplementFlag = "Supplement Flag";
         public const string ReasonforETDchange = "Reason for ETD change";
         public const string ReasonforSKUchange = "Reason for SKU change";
         public const string ReasonforQuantityChange = "Reason for Quantity Change";
         public const string ReasonforCancellation = "Reason for Cancellation";
        
    }
}