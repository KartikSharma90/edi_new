﻿using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.Utilities
{
    public class Config
    {
        private static Dictionary<string, string> objCFG_MAIN;
        public static string getConfig(string Key)
        {
            string strValue;
            if (objCFG_MAIN == null)
            {
                objCFG_MAIN = new Dictionary<string, string>();
                using (var context = new GoodpackEDIEntities())
                {
                    List<Gen_Configuration> configuarion = context.Gen_Configuration.ToList();
                    for (int i=0;i<configuarion.Count;i++)
                    {
                        objCFG_MAIN.Add(configuarion[i].ConfigItem, configuarion[i].Value);
                    }               
                }
            }
            objCFG_MAIN.TryGetValue(Key, out strValue);
            return strValue;
        }
    }
}