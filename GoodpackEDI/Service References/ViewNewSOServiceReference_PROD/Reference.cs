﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GoodpackEDI.ViewNewSOServiceReference_PROD {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:sap-com:document:sap:soap:functions:mc-style", ConfigurationName="ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr")]
    public interface zws_cust_so_history_nrr {
        
        // CODEGEN: Generating message contract since the operation ZcustSoHistory is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1 ZcustSoHistory(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="", ReplyAction="*")]
        System.Threading.Tasks.Task<GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1> ZcustSoHistoryAsync(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18408")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:soap:functions:mc-style")]
    public partial class ZcustSoHistory : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string pFsietdField;
        
        private string pKunnrField;
        
        private string pTsietdField;
        
        private ZsdSoWsdl[] zsdSoWsdlField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string PFsietd {
            get {
                return this.pFsietdField;
            }
            set {
                this.pFsietdField = value;
                this.RaisePropertyChanged("PFsietd");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string PKunnr {
            get {
                return this.pKunnrField;
            }
            set {
                this.pKunnrField = value;
                this.RaisePropertyChanged("PKunnr");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string PTsietd {
            get {
                return this.pTsietdField;
            }
            set {
                this.pTsietdField = value;
                this.RaisePropertyChanged("PTsietd");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZsdSoWsdl[] ZsdSoWsdl {
            get {
                return this.zsdSoWsdlField;
            }
            set {
                this.zsdSoWsdlField = value;
                this.RaisePropertyChanged("ZsdSoWsdl");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18408")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:soap:functions:mc-style")]
    public partial class ZsdSoWsdl : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string land1Field;
        
        private string consField;
        
        private string consCField;
        
        private string regioField;
        
        private string packerField;
        
        private string packCField;
        
        private string matnrField;
        
        private string bstkdField;
        
        private string edatuField;
        
        private short leaddaysField;
        
        private string zcrdateField;
        
        private string plweekField;
        
        private decimal kwmengField;
        
        private decimal zcrqtyField;
        
        private string wkstrtField;
        
        private string vbelnField;
        
        private string vdatuField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string Land1 {
            get {
                return this.land1Field;
            }
            set {
                this.land1Field = value;
                this.RaisePropertyChanged("Land1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string Cons {
            get {
                return this.consField;
            }
            set {
                this.consField = value;
                this.RaisePropertyChanged("Cons");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string ConsC {
            get {
                return this.consCField;
            }
            set {
                this.consCField = value;
                this.RaisePropertyChanged("ConsC");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string Regio {
            get {
                return this.regioField;
            }
            set {
                this.regioField = value;
                this.RaisePropertyChanged("Regio");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string Packer {
            get {
                return this.packerField;
            }
            set {
                this.packerField = value;
                this.RaisePropertyChanged("Packer");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string PackC {
            get {
                return this.packCField;
            }
            set {
                this.packCField = value;
                this.RaisePropertyChanged("PackC");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string Matnr {
            get {
                return this.matnrField;
            }
            set {
                this.matnrField = value;
                this.RaisePropertyChanged("Matnr");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string Bstkd {
            get {
                return this.bstkdField;
            }
            set {
                this.bstkdField = value;
                this.RaisePropertyChanged("Bstkd");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string Edatu {
            get {
                return this.edatuField;
            }
            set {
                this.edatuField = value;
                this.RaisePropertyChanged("Edatu");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public short Leaddays {
            get {
                return this.leaddaysField;
            }
            set {
                this.leaddaysField = value;
                this.RaisePropertyChanged("Leaddays");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=10)]
        public string Zcrdate {
            get {
                return this.zcrdateField;
            }
            set {
                this.zcrdateField = value;
                this.RaisePropertyChanged("Zcrdate");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=11)]
        public string Plweek {
            get {
                return this.plweekField;
            }
            set {
                this.plweekField = value;
                this.RaisePropertyChanged("Plweek");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=12)]
        public decimal Kwmeng {
            get {
                return this.kwmengField;
            }
            set {
                this.kwmengField = value;
                this.RaisePropertyChanged("Kwmeng");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=13)]
        public decimal Zcrqty {
            get {
                return this.zcrqtyField;
            }
            set {
                this.zcrqtyField = value;
                this.RaisePropertyChanged("Zcrqty");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=14)]
        public string Wkstrt {
            get {
                return this.wkstrtField;
            }
            set {
                this.wkstrtField = value;
                this.RaisePropertyChanged("Wkstrt");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=15)]
        public string Vbeln {
            get {
                return this.vbelnField;
            }
            set {
                this.vbelnField = value;
                this.RaisePropertyChanged("Vbeln");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=16)]
        public string Vdatu {
            get {
                return this.vdatuField;
            }
            set {
                this.vdatuField = value;
                this.RaisePropertyChanged("Vdatu");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18408")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:soap:functions:mc-style")]
    public partial class Bapireturn : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string typeField;
        
        private string codeField;
        
        private string messageField;
        
        private string logNoField;
        
        private string logMsgNoField;
        
        private string messageV1Field;
        
        private string messageV2Field;
        
        private string messageV3Field;
        
        private string messageV4Field;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string Type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
                this.RaisePropertyChanged("Type");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string Code {
            get {
                return this.codeField;
            }
            set {
                this.codeField = value;
                this.RaisePropertyChanged("Code");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string Message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
                this.RaisePropertyChanged("Message");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string LogNo {
            get {
                return this.logNoField;
            }
            set {
                this.logNoField = value;
                this.RaisePropertyChanged("LogNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string LogMsgNo {
            get {
                return this.logMsgNoField;
            }
            set {
                this.logMsgNoField = value;
                this.RaisePropertyChanged("LogMsgNo");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string MessageV1 {
            get {
                return this.messageV1Field;
            }
            set {
                this.messageV1Field = value;
                this.RaisePropertyChanged("MessageV1");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string MessageV2 {
            get {
                return this.messageV2Field;
            }
            set {
                this.messageV2Field = value;
                this.RaisePropertyChanged("MessageV2");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string MessageV3 {
            get {
                return this.messageV3Field;
            }
            set {
                this.messageV3Field = value;
                this.RaisePropertyChanged("MessageV3");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string MessageV4 {
            get {
                return this.messageV4Field;
            }
            set {
                this.messageV4Field = value;
                this.RaisePropertyChanged("MessageV4");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18408")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:soap:functions:mc-style")]
    public partial class ZcustSoHistoryResponse : object, System.ComponentModel.INotifyPropertyChanged {
        
        private Bapireturn returnField;
        
        private ZsdSoWsdl[] zsdSoWsdlField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public Bapireturn Return {
            get {
                return this.returnField;
            }
            set {
                this.returnField = value;
                this.RaisePropertyChanged("Return");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZsdSoWsdl[] ZsdSoWsdl {
            get {
                return this.zsdSoWsdlField;
            }
            set {
                this.zsdSoWsdlField = value;
                this.RaisePropertyChanged("ZsdSoWsdl");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ZcustSoHistoryRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:sap-com:document:sap:soap:functions:mc-style", Order=0)]
        public GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory ZcustSoHistory;
        
        public ZcustSoHistoryRequest() {
        }
        
        public ZcustSoHistoryRequest(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory ZcustSoHistory) {
            this.ZcustSoHistory = ZcustSoHistory;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ZcustSoHistoryResponse1 {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:sap-com:document:sap:soap:functions:mc-style", Order=0)]
        public GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse ZcustSoHistoryResponse;
        
        public ZcustSoHistoryResponse1() {
        }
        
        public ZcustSoHistoryResponse1(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse ZcustSoHistoryResponse) {
            this.ZcustSoHistoryResponse = ZcustSoHistoryResponse;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface zws_cust_so_history_nrrChannel : GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class zws_cust_so_history_nrrClient : System.ServiceModel.ClientBase<GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr>, GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr {
        
        public zws_cust_so_history_nrrClient() {
        }
        
        public zws_cust_so_history_nrrClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public zws_cust_so_history_nrrClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public zws_cust_so_history_nrrClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public zws_cust_so_history_nrrClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1 GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr.ZcustSoHistory(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest request) {
            return base.Channel.ZcustSoHistory(request);
        }
        
        public GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse ZcustSoHistory(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory ZcustSoHistory1) {
            GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest inValue = new GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest();
            inValue.ZcustSoHistory = ZcustSoHistory1;
            GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1 retVal = ((GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr)(this)).ZcustSoHistory(inValue);
            return retVal.ZcustSoHistoryResponse;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1> GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr.ZcustSoHistoryAsync(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest request) {
            return base.Channel.ZcustSoHistoryAsync(request);
        }
        
        public System.Threading.Tasks.Task<GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryResponse1> ZcustSoHistoryAsync(GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistory ZcustSoHistory) {
            GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest inValue = new GoodpackEDI.ViewNewSOServiceReference_PROD.ZcustSoHistoryRequest();
            inValue.ZcustSoHistory = ZcustSoHistory;
            return ((GoodpackEDI.ViewNewSOServiceReference_PROD.zws_cust_so_history_nrr)(this)).ZcustSoHistoryAsync(inValue);
        }
    }
}
