﻿using GoodpackEDI.ViewModels;
using GPArchitecture.CommonTypes;
using System.Collections.Generic;

namespace GoodpackEDI.BusinessLayer.MappingManagementLayer
{
    interface IMapperManager
    {
        IList<GenericItemModel> TransactionTypes();
        IList<GenericItemModel> FileTypes();       
        IList<SAPMappingModel> SAPMessageFields(int transactionId);
        ResponseModel SaveHeaderFields(MappingModel mappingModel, string username, string mappingFilePath);
        IList<ViewMapperDataModel> GetAllMappingDetails(string username);
        IList<ViewSelectedMapperDataModel> GetSelectedMappingDetails(int susbsyToTransactionMapperId);
        ResponseModel EmailIdMapper(string emailIds, string transactionId);

        bool DeleteSelectedMappingDetails(int susbsyToTransactionMapperId);
        IList<EditMapperHeaderModel> GetMappedFileDetailsForEdit(int susbsyToTransactionMapperId);
        IList<EditHeaderModel> GetHeadersForEdit(int susbsyToTransactionMapperId);
        IList<EditMapperHeaderModel> GetFilDetailsForEdit(int susbsyToTransactionMapperId);
        ResponseModel EditHeaderFields(MappingModel mappingModel, string username);
        IList<MapperEmailModel> GetEmailIdsForEdit(int susbsyToTransactionMapperId);
        ResponseModel IsValidFtp(FtpModel model);
        string MapperFileDate(string mapperName);
    }
}
