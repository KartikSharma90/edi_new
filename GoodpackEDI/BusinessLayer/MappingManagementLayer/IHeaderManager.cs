﻿using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.MappingManagementLayer
{
    interface IHeaderManager
    {
        DataTable readHeaders(string mapperFilePath, MappingModel mappingModel);
    }
}