﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodpackEDI.ViewModels;
using GoodpackEDI.Utilities;
using System.IO;
using GPArchitecture.DataLayer.Models;
using log4net;

namespace GoodpackEDI.BusinessLayer.MappingManagementLayer
{
    public class MapperManager : IMapperManager
    {
        private Trn_MappingSubsi mappingSubsi;
        private Trn_MappingConfiguration mappingConfig;
        private static readonly ILog log = LogManager.GetLogger(typeof(MapperManager));
        public IList<GenericItemModel> TransactionTypes()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_TransactionTypes
                        where ((s.IsEnabled == true) && (s.IsDropdownDisplay == true))
                        orderby s.TransactionName
                        select new GenericItemModel
                        {
                            Id = s.Id,
                            Name = s.TransactionName
                        }).ToList();

            }
        }
        public IList<GenericItemModel> FileTypes()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_MessageFormat
                        where ((s.IsEnabled == true) && (s.IsDropdownDisplay == true))
                        orderby s.MessageFormat
                        select new GenericItemModel
                        {
                            Id = s.Id,
                            Name = s.MessageFormat
                        }).ToList();

            }
        }

        //public ResponseModel CheckMappingExists(MappingModel mappingModel, string username)
        //{
        //    using (var context = new GoodpackEDIEntities())
        //    {
        //        Gen_User user = (from s in context.Gen_User
        //                         where s.Username == username
        //                         select s).FirstOrDefault();
        //        Trn_SubsiToTransactionFileMapper fileMapper = (from s in context.Trn_SubsiToTransactionFileMapper
        //                                                       where ((s.EDISubsiId == mappingModel.SubsiId) && (s.TransactionCodeId == mappingModel.TransactionId) && (s.LastUpdatedBy == user.Id))
        //                                                       select s).FirstOrDefault();
        //        ResponseModel responseModel = new ResponseModel();

        //        if (fileMapper != null)
        //        {
        //            responseModel.Success = false;
        //            responseModel.Message = "Mapping already exists";
        //        }
        //        else
        //        {
        //            responseModel.Success = true;
        //            responseModel.Message = "Successfull";
        //        }
        //        return responseModel;
        //    }
        //}

        public IList<SAPMappingModel> SAPMessageFields(int transactionId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_SAPMessageFields
                        orderby s.FieldSequence
                        where (s.TransactionId == transactionId)
                        select new SAPMappingModel
                        {
                            Id = s.Id,
                            FieldName = s.SAPFieldName,
                            Note = s.Note,
                            InputType = s.InputType,
                            IsMandatory = s.IsDisplay,
                            FieldSequence = s.FieldSequence
                        }).ToList();
            }
        }

        public ResponseModel SaveHeaderFields(MappingModel mappingModel, string username, string mappingFilePath)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {

                using (var context = new GoodpackEDIEntities())
                {
                    Trn_SubsiToTransactionFileMapper uploadfileMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                         where s.Filename == mappingModel.FileName
                                                                         select s).FirstOrDefault();
                    if (uploadfileMapper != null)
                    {
                        responseModel.Success = false;
                        responseModel.Message = "File name already exists.Please input another file name";
                        return responseModel;
                    }
                    else
                    {

                        int userId = (from s in context.Gen_User
                                      where s.Username == username
                                      select s.Id).FirstOrDefault();
                        string filePath = MoveMapperFile(username, mappingFilePath);
                        Trn_SubsiToTransactionFileMapper fileMapper = new Trn_SubsiToTransactionFileMapper();
                        fileMapper.EDISubsiId = mappingModel.SubsiId;
                        fileMapper.IsEnabled = true;
                        fileMapper.IsHeaderPresent = true;
                        fileMapper.LastUpdatedBy = userId;
                        fileMapper.LastUpdatedDate = DateTime.Now;
                        fileMapper.MessageFormatId = mappingModel.FileTypeId;
                        fileMapper.MinFieldValueCount = Convert.ToInt32(mappingModel.FieldCountValue);
                        fileMapper.Service = ConstantUtilities.Service;
                        fileMapper.Filename = mappingModel.FileName;
                        fileMapper.FileServerPath = filePath;
                        fileMapper.TransactionCodeId = mappingModel.TransactionId;
                        context.Trn_SubsiToTransactionFileMapper.Add(fileMapper);
                        context.SaveChanges();

                        Trn_SubsiToTransactionFileMapper headerMapper = (from s in context.Trn_SubsiToTransactionFileMapper
                                                                         where (s.Filename == mappingModel.FileName)
                                                                         select s).FirstOrDefault();

                        if (mappingModel.EnableEmail)
                        {
                            string[] emailIds = mappingModel.EmailIds.Split(',');
                            foreach (var item in emailIds)
                            {
                                Trn_MapperNameToEmailIdMapper emailIdMapper = new Trn_MapperNameToEmailIdMapper();
                                emailIdMapper.EmailId = item;
                                emailIdMapper.MapperId = fileMapper.Id;
                                context.Trn_MapperNameToEmailIdMapper.Add(emailIdMapper);
                                context.SaveChanges();
                            }
                        }

                        // Inserting values to Trn_MapperFieldSpecs table
                        for (int i = 0; i < mappingModel.FileHeaders.Count; i++)
                        {

                            Trn_MapperFieldSpecs headerFields = new Trn_MapperFieldSpecs();
                            //Input file header
                            headerFields.FieldName = mappingModel.FileHeaders[i].HeaderName;
                            headerFields.FieldSequence = i + 1;
                            headerFields.Length = 0;
                            headerFields.SubsiToTransactionMapperId = headerMapper.Id;

                            context.Trn_MapperFieldSpecs.Add(headerFields);
                            context.SaveChanges();

                        }

                        // Inserting values to Trn_MappingSubsi table table

                        mappingSubsi = new Trn_MappingSubsi();
                        mappingSubsi.IsEnabled = true;
                        mappingSubsi.LastUpdatedBy = userId;
                        mappingSubsi.LastUpdatedDate = DateTime.Now;
                        mappingSubsi.SubsiToTransactionFileMapperId = headerMapper.Id;
                        mappingSubsi.SapMessageFieldId = mappingModel.TransactionId;
                        context.Trn_MappingSubsi.Add(mappingSubsi);
                        context.SaveChanges();
                        Trn_MappingSubsi subsi = (from s in context.Trn_MappingSubsi
                                                  where (s.SubsiToTransactionFileMapperId == headerMapper.Id) && (s.SapMessageFieldId == mappingModel.TransactionId) && (s.LastUpdatedBy == userId)
                                                  select s).FirstOrDefault();
                        // Inserting values to Trn_MappingConfiguration table

                        for (int item = 0; item < mappingModel.SAPFieldHeaders.Count; item++)
                        {

                            mappingConfig = new Trn_MappingConfiguration();
                            mappingConfig.MappingSubsiId = subsi.Id;
                            BindControlsValues(mappingModel, item);
                            context.Trn_MappingConfiguration.Add(mappingConfig);
                            context.SaveChanges();
                            // context.SaveChanges();

                        }
                        responseModel.Success = true;
                        responseModel.Message = "Header mapping done successfully ";
                        return responseModel;

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(" SaveHeaderFields in MapperManager .username:" + username + ", mappingFilePath:" + mappingFilePath + ",mappingModel:" + mappingModel + ". Api Exception : Message- " + ex.Message);
                responseModel.Success = false;
                responseModel.Message = "Unable to process your request.Please try again";
                return responseModel;
            }
        }
        #region - BindControls
        //<summary>
        //based on the type bind the controls in screen
        //</summary>
        //<param name="mappingModel"></param>
        //<param name="item"></param>
        private void BindControlsValues(MappingModel mappingModel, int item)
        {
            try
            {


                switch (mappingModel.SAPFieldHeaders[item].InputType.ToUpper())
                {
                    case "NONE":
                    case "OPERATION_NO_INPUT":
                    case "FIXED":

                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        break;
                    case "USER_FIXED":
                        mappingConfig.FixedValue = mappingModel.MappedFileModel[item].HeaderName;

                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        break;
                    case "OPERATION_ON_SOURCE_FIELD":
                    case "MAP_FROM_SOURCE":

                        if (mappingModel.MappedFileModel != null)
                        {
                            int sapId = mappingModel.SAPFieldHeaders[item].Id;
                            MapperHeaderModel result = mappingModel.MappedFileModel.Find(x => x.Id == sapId);

                            if (result != null)
                            {
                                mappingConfig.SourceFieldSequence = result.SRNO;
                            }
                            else
                            {
                                mappingConfig.SourceFieldSequence = null;
                            }


                            mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        }
                        break;
                    case "MAP_FROM_SOURCE_WITH_SUBSTRING":
                    case "OPERATION_ON_SOURCE_FIELD_WITH_SUBSTRING":


                        int sapItemId = mappingModel.SAPFieldHeaders[item].Id;
                        MapperHeaderModel mappingResult = mappingModel.MappedFileModel.Find(x => x.Id == sapItemId);

                        if (mappingResult != null)
                        {
                            mappingConfig.SourceFieldSequence = mappingResult.SRNO;
                        }
                        else
                        {
                            mappingConfig.SourceFieldSequence = 0;
                        }

                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        break;
                    case "DATE":
                        int sapHeaderId = mappingModel.SAPFieldHeaders[item].Id;
                        MapperHeaderModel header = mappingModel.MappedFileModel.Find(x => x.Id == sapHeaderId);

                        if (header != null)
                        {
                            mappingConfig.SourceFieldSequence = header.SRNO;
                            if (mappingModel.DateFormat != null)
                            {
                                mappingConfig.InputDateFormat = mappingModel.DateFormat.ToUpper();
                            }
                        }
                        else
                        {
                            mappingConfig.SourceFieldSequence = null;

                        }
                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        break;
                    case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                    case "MAP_FROM_SOURCE_OR_USER_FIXED_INPUT":
                        //TableRow tableRow = (TableRow)tableCell.Controls[0].Controls[0];
                        //TextBox tbFixedValue = (TextBox)tableRow.Cells[0].Controls[1];
                        //ddlSource = (DropDownList)tableRow.Cells[0].Controls[3].Controls[0];
                        //Label lblInputType = (Label)tableRow.Cells[0].Controls[4];

                        int sapMapId = mappingModel.SAPFieldHeaders[item].Id;
                        MapperHeaderModel mapHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapMapId);

                        if (mapHeader != null)
                        {
                            
                            if ((mapHeader.HeaderName != string.Empty) && (mapHeader.FieldSequence == 0))
                            {
                                mappingConfig.FixedValue = mapHeader.HeaderName.ToUpper();
                            }
                            else
                            {
                                mappingConfig.SourceFieldSequence = mapHeader.SRNO;
                            }
                        }
                        else
                        {
                            mappingConfig.SourceFieldSequence = null;
                        }


                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                        break;
                    case "OPERATION_ON_SOURCE_FIELDS2":

                        //ucSourceHeader2Fields ucOperationOnSourceFields2 = (ucSourceHeader2Fields)tableCell.Controls[0];
                        //if (ucOperationOnSourceFields2.SelectedValue != CommonConstants.DEFAULT_COMBOVALUE)
                        //    objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields2.SelectedValue);
                        //else
                        //    objMappingConfig.SourceFieldSeq = null;
                        //if (ucOperationOnSourceFields2.SelectedValue1 != CommonConstants.DEFAULT_COMBOVALUE)
                        //    objMappingConfig.SourceFieldSeq2 = Convert.ToInt32(ucOperationOnSourceFields2.SelectedValue1);
                        //else
                        //    objMappingConfig.SourceFieldSeq2 = null;

                        int sapOperationMapId = mappingModel.SAPFieldHeaders[item].Id;
                        MapperHeaderModel operationHeader = mappingModel.MappedFileModel.Find(x => x.Id == sapOperationMapId);

                        if (operationHeader != null)
                        {
                            mappingConfig.SourceFieldSeq2 = operationHeader.SRNO;
                            mappingConfig.SourceFieldSequence = null;
                        }
                        else
                        {
                            mappingConfig.SourceFieldSeq2 = null;
                            mappingConfig.SourceFieldSequence = null;
                        }

                        mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;

                        break;
                    //case "MAP_ETD_FROM_SOURCE":
                    //    ucSourceHeader3Fields ucOperationOnSourceFields3 = (ucSourceHeader3Fields)tableCell.Controls[0];
                    //    if (ucOperationOnSourceFields3.SelectedValueMappingType == "1")
                    //    {
                    //        if (ucOperationOnSourceFields3.SelectedValueDateField != CommonConstants.DEFAULT_COMBOVALUE)
                    //            objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValueDateField);
                    //        if (ucOperationOnSourceFields3.DateFormatDirectTextBox != null)
                    //        {
                    //            objMappingConfig.SourceDateFormat = ucOperationOnSourceFields3.DateFormatDirectTextBox.Text.ToUpper();
                    //        }
                    //    }
                    //    else if (ucOperationOnSourceFields3.SelectedValueMappingType == "2")
                    //    {
                    //        if (ucOperationOnSourceFields3.SelectedValuePOL != CommonConstants.DEFAULT_COMBOVALUE)
                    //            objMappingConfig.SourceFieldSeq = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValuePOL);
                    //        if (ucOperationOnSourceFields3.SelectedValuePOD != CommonConstants.DEFAULT_COMBOVALUE)
                    //            objMappingConfig.SourceFieldSeq2 = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValuePOD);
                    //        if (ucOperationOnSourceFields3.SelectedValueArrivalDate != CommonConstants.DEFAULT_COMBOVALUE)
                    //            objMappingConfig.SourceFieldSeq3 = Convert.ToInt32(ucOperationOnSourceFields3.SelectedValueArrivalDate);
                    //        if (ucOperationOnSourceFields3.DateFormatETDTextBox != null)
                    //            objMappingConfig.SourceDateFormat = ucOperationOnSourceFields3.DateFormatETDTextBox.Text.ToUpper();
                    //    }

                    //    objMappingConfig.MappingType = ucOperationOnSourceFields3.SelectedValueMappingType;



                    //    objMappingHead.TargetMessageFieldSpecs.ID = Convert.ToInt32(targetFieldId);
                    //    objMappingConfig.TargetFieldSeq = Convert.ToInt32(targetFieldSeq);

<<<<<<< local
                    //    break;


                    //            case "DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD":
                    //                {
                    //                    ucSingleSourceWithSubStringandOperation ucSingleSource_Operation_Substring = (ucSingleSourceWithSubStringandOperation)tableCell.Controls[0];
=======
                    //                break;
                    case "DYNAMIC_SINGLE_OPERATION_ON_SOURCE_FIELD":
                        {
                            //ucSingleSourceWithSubStringandOperation ucSingleSource_Operation_Substring = (ucSingleSourceWithSubStringandOperation)tableCell.Controls[0];
>>>>>>> other

                            //if (ucSingleSource_Operation_Substring.SourceSelectedValue != CommonConstants.DEFAULT_COMBOVALUE)
                            //    mappingConfig.SourceFieldSequence = Convert.ToInt32(ucSingleSource_Operation_Substring.SourceSelectedValue);


                            
                        int sapDynamicItemId = mappingModel.SAPFieldHeaders[item].Id;
                        MapperHeaderModel dynamicMappingResult = mappingModel.MappedFileModel.Find(x => x.Id == sapDynamicItemId);
                        if (dynamicMappingResult != null)
                        {
                            mappingConfig.SourceFieldSequence = dynamicMappingResult.SRNO;
                        }
                           //if (ucSingleSource_Operation_Substring.IsSubstringFunctionEnabled)
                           // {
                           //     if (ucSingleSource_Operation_Substring.SubStringStartPositionTextBox.Text != string.Empty)
                           //    {
                           //         mappingConfig.SourceFieldSeq1SubStringStartPosition = Convert.ToInt32(ucSingleSource_Operation_Substring.SubStringStartPositionTextBox.Text);
                           //     }
                           //     else
                           //     {
                           //         mappingConfig.SourceFieldSeq1SubStringStartPosition = null;
                           //     }

                           //     if (ucSingleSource_Operation_Substring.SubStringLengthTextBox.Text != string.Empty)
                           //     {
                           //         mappingConfig.SourceFieldSeq1SubStringLength = Convert.ToInt32(ucSingleSource_Operation_Substring.SubStringLengthTextBox.Text);
                           //     }
                           //     else
                           //     {
                           //         mappingConfig.SourceFieldSeq1SubStringLength = null;
                           //     }
                           // }

                           // objMappingHead.TargetMessageFieldSpecs.ID = Convert.ToInt32(targetFieldId);
                            mappingConfig.SapFieldSeq = mappingModel.SAPFieldHeaders[item].FieldSequence;
                            mappingConfig.MappingType = "NO OPERATION";
                            break;
                        }
                }//-- End Switch(inputType)
         }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        private string MoveMapperFile(string username, string sourceFile)
        {
            string filePath = HttpContext.Current.Server.MapPath("~/MappedFiles/" + username);
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string destinationFile = Path.Combine(filePath, Path.GetFileName(sourceFile));

            File.Move(sourceFile, destinationFile);
            return destinationFile;
        }

        public IList<ViewMapperDataModel> GetAllMappingDetails(string username)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == username
                                 select s).FirstOrDefault();

                string role = (from s in context.Ref_UserRoles
                               where s.Id == user.RoleId
                               select s.UserRole).FirstOrDefault();

                List<ViewMapperDataModel> ListViewMapperModel = new List<ViewMapperDataModel>();

                if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
                {
                    List<Trn_SubsiToTransactionFileMapper> ViewMapperDetails = (context.Trn_SubsiToTransactionFileMapper.Where(i => i.LastUpdatedBy == user.Id && i.IsEnabled == true).OrderByDescending(c => c.Id).ToList());
                    for (int i = 0; i < ViewMapperDetails.Count; i++)
                    {
                        ViewMapperDataModel ViewMapperModel = new ViewMapperDataModel();
                        ViewMapperModel.UniqueId = ViewMapperDetails[i].Id;
                        ViewMapperModel.AddedBy = ViewMapperDetails[i].Gen_User.Username;
                        ViewMapperModel.DateAdded = ViewMapperDetails[i].LastUpdatedDate;
                        ViewMapperModel.FileType = ViewMapperDetails[i].Ref_MessageFormat.MessageFormat;
                        ViewMapperModel.MapperName = ViewMapperDetails[i].Filename;
                        ViewMapperModel.SubsyName = ViewMapperDetails[i].Gen_EDISubsies.SubsiName;
                        ViewMapperModel.TransactionType = ViewMapperDetails[i].Ref_TransactionTypes.TransactionCode;
                        ListViewMapperModel.Add(ViewMapperModel);
                    }
                    return ListViewMapperModel;
                }
                else
                {
                    List<Trn_SubsiToTransactionFileMapper> ViewMapperDetails = (context.Trn_SubsiToTransactionFileMapper.Where(i => i.IsEnabled == true).OrderByDescending(c => c.Id).ToList());
                    for (int i = 0; i < ViewMapperDetails.Count; i++)
                    {
                        ViewMapperDataModel ViewMapperModel = new ViewMapperDataModel();
                        ViewMapperModel.UniqueId = ViewMapperDetails[i].Id;
                        ViewMapperModel.AddedBy = ViewMapperDetails[i].Gen_User.Username;
                        ViewMapperModel.DateAdded = ViewMapperDetails[i].LastUpdatedDate;
                        ViewMapperModel.FileType = ViewMapperDetails[i].Ref_MessageFormat.MessageFormat;
                        ViewMapperModel.MapperName = ViewMapperDetails[i].Filename;
                        ViewMapperModel.SubsyName = ViewMapperDetails[i].Gen_EDISubsies.SubsiName;
                        ViewMapperModel.TransactionType = ViewMapperDetails[i].Ref_TransactionTypes.TransactionCode;
                        ListViewMapperModel.Add(ViewMapperModel);
                    }
                    return ListViewMapperModel;
                }
            }

        }


        public IList<ViewSelectedMapperDataModel> GetSelectedMappingDetails(int susbsyToTransactionMapperId)
        {
            using (var context = new GoodpackEDIEntities())
            {

                List<ViewSelectedMapperDataModel> ListViewMapperModel = new List<ViewSelectedMapperDataModel>();

                int MapperFieldSpecId = context.Trn_MappingSubsi.Where(i => i.SubsiToTransactionFileMapperId == susbsyToTransactionMapperId).FirstOrDefault().Id;
                int TransactionId = context.Trn_MappingSubsi.Where(i => i.Id == MapperFieldSpecId).FirstOrDefault().SapMessageFieldId;
                List<Ref_SAPMessageFields> SAPMessageFields = context.Ref_SAPMessageFields.Where(i => i.TransactionId == TransactionId).ToList();


                for (int i = 0; i < SAPMessageFields.Count; i++)
                {
                    ViewSelectedMapperDataModel ViewMapperModel = new ViewSelectedMapperDataModel();
                    int FieldSequence = SAPMessageFields[i].FieldSequence;


                    switch (SAPMessageFields[i].InputType)
                    {
                        case "FIXED":
                            ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                            ViewMapperModel.Notes = SAPMessageFields[i].Note;
                            ViewMapperModel.InputField = SAPMessageFields[i].DefaultValue;
                            ListViewMapperModel.Add(ViewMapperModel);
                            break;

                        case "USER_FIXED":
                            ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                            ViewMapperModel.Notes = SAPMessageFields[i].Note;
                            ViewMapperModel.InputField = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                            ListViewMapperModel.Add(ViewMapperModel);
                            break;

                        case "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED":
                            ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                            ViewMapperModel.Notes = SAPMessageFields[i].Note;
                            string inputfield = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().FixedValue;
                            if (inputfield == null || inputfield == "")
                            {
                                int? SourceFieldSequenceId = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                                if (SourceFieldSequenceId == null || SourceFieldSequenceId == 0)
                                {
                                    ViewMapperModel.InputField = null;
                                }
                                else
                                {
                                    ViewMapperModel.InputField = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId).FirstOrDefault().FieldName;
                                }
                            }
                            else
                            {
                                ViewMapperModel.InputField = inputfield;
                            }
                            ListViewMapperModel.Add(ViewMapperModel);
                            break;

                        default:
                            ViewMapperModel.SAPField = SAPMessageFields[i].SAPFieldName;
                            ViewMapperModel.Notes = SAPMessageFields[i].Note;

                            int? SourceFieldSequenceId1 = context.Trn_MappingConfiguration.Where(j => j.MappingSubsiId == MapperFieldSpecId && j.SapFieldSeq == FieldSequence).FirstOrDefault().SourceFieldSequence;
                            if (SourceFieldSequenceId1 == null || SourceFieldSequenceId1 == 0)
                            {
                                ViewMapperModel.InputField = null;
                            }
                            else
                            {
                                ViewMapperModel.InputField = context.Trn_MapperFieldSpecs.Where(k => k.SubsiToTransactionMapperId == susbsyToTransactionMapperId && k.FieldSequence == SourceFieldSequenceId1).FirstOrDefault().FieldName;
                            }
                            ListViewMapperModel.Add(ViewMapperModel);

                            break;

                    }

                }

                return ListViewMapperModel;

            }

        }


        public ResponseModel EmailIdMapper(string emailIds)
        {
            ResponseModel responseModel=new ResponseModel();
            using (var context = new GoodpackEDIEntities())
            { 
                string[] emailIdValues=emailIds.Split(',');
                foreach (var item in emailIdValues)
                {

                    Trn_MapperNameToEmailIdMapper emailIdMapper = (from s in context.Trn_MapperNameToEmailIdMapper
                                                                   where s.EmailId == item
                                                                   select s).FirstOrDefault();
                    if (emailIdMapper != null)
                    {
                        responseModel.Success=false;
                        responseModel.Message="Email id " + item +" has mapping already.Please enter another email id";
                        return responseModel;
                    }                   
                }

                responseModel.Success = true;
                return responseModel;
            }
        }
    }
}