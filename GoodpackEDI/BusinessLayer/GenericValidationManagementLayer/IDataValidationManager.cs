﻿using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.GenericValidationManagementLayer
{
   public interface IDataValidationManager
    {
         string DataValidator(Dictionary<string,string> dataParams);
         string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel);        
    }
}
