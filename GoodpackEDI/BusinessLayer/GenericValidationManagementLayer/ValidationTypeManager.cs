﻿using GoodpackEDI.ConstantUtils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.GenericValidationManagementLayer
{
    public class ValidationTypeManager :IValidationTypeManger
    {
      public IDataValidationManager ValidationType(string validationType)
        {            
            switch (validationType)
            {
                case ConstantUtilities.DateValidator: return new DateValidationManager();
                default: return new CommonValidationManager();            
            }        
        }

       
    }
}