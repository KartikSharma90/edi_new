﻿using GoodpackEDI.ConstantUtils;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.GenericValidationManagementLayer
{
    public class ValueValidationManager : IDataValidationManager
    {
        public string DataValidator(Dictionary<string, string> dataParams)
        {
           string fileData = dataParams[ConstantUtilities.FileData];
           string fileHeader=dataParams[ConstantUtilities.SAPField];
           string transactionType=dataParams[ConstantUtilities.TransactionType];
           bool isNullValueAllowed = true;
           if (fileData.Length == 0)
           {
               using (var context = new GoodpackEDIEntities())
               {
                   isNullValueAllowed = (from s in context.Ref_SAPMessageFields
                                         join p in context.Ref_TransactionTypes
                                         on s.TransactionId equals p.Id
                                         where ((p.TransactionCode == transactionType)&&(s.SAPFieldName==fileHeader))
                                         select s.IsAllowNullValue).FirstOrDefault();
               }
               if (!isNullValueAllowed)
               {
                   return ConstantUtilities.ValueValidationMessage + fileHeader + ConstantUtilities.NewLine;
               }
               else {
                   return String.Empty;
               }
           }
           else
           {
               return String.Empty;
           }
        }


        public string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel)
        {
            string fileData = dataParams[ConstantUtilities.FileData];
            string fileHeader = dataParams[ConstantUtilities.SAPField];
            string transactionType = dataParams[ConstantUtilities.TransactionType];
            bool isNullValueAllowed = validationModel.BoolList.Where(i => i.SAPName == fileHeader).Select(i => i.IsNullAllowed).FirstOrDefault();
            if (fileData.Length == 0)
            {
                if (!isNullValueAllowed)
                {
                    return ConstantUtilities.ValueValidationMessage + fileHeader + ConstantUtilities.NewLine;
                }
                else
                {
                    return String.Empty;
                }
            }
            else
            {
                return String.Empty;
            }
        }


    }
}