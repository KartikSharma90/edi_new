﻿using GoodpackEDI.ConstantUtils;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.GenericValidationManagementLayer
{
    public class CharacterValidationManager : IDataValidationManager
    {
        public string DataValidator(Dictionary<string, string> dataParams)
        {
            string sapField =dataParams[ConstantUtilities.SAPField];
            string transaction = dataParams[ConstantUtilities.TransactionType];
            int character = 0;
            using (var context=new GoodpackEDIEntities())
            {
                character=(from s in context.Ref_TransactionTypes
                               join p in context.Ref_SAPMessageFields
                                   on s.Id equals p.TransactionId
                               where(p.SAPFieldName==sapField)&&(s.TransactionCode==transaction)
                               select p.MaxLength).FirstOrDefault()??0;
            }

            string fileData = dataParams[ConstantUtilities.FileData];
            if (fileData.Length <= character)
            {
                return String.Empty;
            }
            else
            {
                return ConstantUtilities.CharacterValidationMessage + character + " for " + fileData + ConstantUtilities.NewLine;
            }

        }

        public string DataValidator(Dictionary<string, string> dataParams, ValidationDataModel validationModel)
        {
            string sapField = dataParams[ConstantUtilities.SAPField];
            string transaction = dataParams[ConstantUtilities.TransactionType];
            int character = validationModel.CharList.Where(i => i.SAPName == sapField).Select(i => i.MaxLength).FirstOrDefault() ?? 0;
            string fileData = dataParams[ConstantUtilities.FileData];
            if (fileData.Length <= character)
            {
                return String.Empty;
            }
            else
            {
                return ConstantUtilities.CharacterValidationMessage + character + " for " + fileData + ConstantUtilities.NewLine;
            }

        }


    }
}