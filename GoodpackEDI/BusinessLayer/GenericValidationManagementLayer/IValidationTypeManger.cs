﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.GenericValidationManagementLayer
{
   public interface IValidationTypeManger
    {
       IDataValidationManager ValidationType(string validationType);
    }
}
