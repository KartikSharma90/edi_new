﻿using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.SubsiEntityManagementLayer
{
    interface ISubsiManager
    {
        IList<SubsiModel> GetAllSubsies(string username);
        UserToSubsiModel GetSubsiData(int userId,bool isMapping);
        ResponseModel MapUserToSubsi(List<SubsiModel> subsiModel, int userId);
    }
}
