﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodpackEDI.BusinessLayer.SOManagementLayer
{
    public class MsgSapSOLine
    {
        StringBuilder message;

        // Fields
        string SalesDocumentType;
        string SalesOrganization;
        string DistributionChannel;
        string Division;
        string SalesOffice;
        public string CustPOnumber;
        string CustomerPODate;
        string CustomerSONumber;
        string soldToParty;
        string RequestDeliveryDate;
        string OrderDate;
        string HeaderRemarks;
        public string SOItemNumber;
        public string ReferencedContractNumber;
        public string Material;
        string Packer;
        string Consignee;
        string DeliveryDate;
        string OrderQuantity;
        string UOM;
        string VesselETD;
        string VesselETA;
        string VesselName;
        string VoyageReference;
        string ShippingLine;
        string POL;
        string POD;
        string ItemRemarks;
        string DeliveryBlockOrders;
        public string supplementFlag;
        private string soLineContent;

        public MsgSapSOLine(IList<string> fieldListSrc)
        {
            if (fieldListSrc.Count < 30)
            {
                throw new Exception("The SAP Sales Order line does not have enough fields.");
            }
            SalesDocumentType = fieldListSrc[0];
            SalesOrganization = fieldListSrc[1];
            DistributionChannel = fieldListSrc[2];
            Division = fieldListSrc[3];
            SalesOffice = fieldListSrc[4];
            CustPOnumber = fieldListSrc[5];
            CustomerPODate = fieldListSrc[6];
            CustomerSONumber = fieldListSrc[7];
            soldToParty = fieldListSrc[8];
            RequestDeliveryDate = fieldListSrc[9];
            OrderDate = fieldListSrc[10];
            HeaderRemarks = fieldListSrc[11];
            SOItemNumber = fieldListSrc[12];
            ReferencedContractNumber = fieldListSrc[13];
            Material = fieldListSrc[14];
            Packer = fieldListSrc[15];
            Consignee = fieldListSrc[16];
            DeliveryDate = fieldListSrc[17];
            OrderQuantity = fieldListSrc[18];
            UOM = fieldListSrc[19];
            VesselETD = fieldListSrc[20];
            VesselETA = fieldListSrc[21];
            VesselName = fieldListSrc[22];
            VoyageReference = fieldListSrc[23];
            ShippingLine = fieldListSrc[24];
            POL = fieldListSrc[25];
            POD = fieldListSrc[26];
            ItemRemarks = fieldListSrc[27];
            DeliveryBlockOrders = fieldListSrc[28];
            supplementFlag = fieldListSrc[29];
        }

        public void GenerateSAPMessage()
        {
            message = new StringBuilder();
            message.Append(SalesDocumentType).Append("\t");
            message.Append(SalesOrganization).Append("\t");
            message.Append(DistributionChannel).Append("\t");
            message.Append(Division).Append("\t");
            message.Append(SalesOffice).Append("\t");
            message.Append(CustPOnumber).Append("\t");
            message.Append(CustomerPODate).Append("\t");
            message.Append(CustomerSONumber).Append("\t");
            message.Append(SoldToParty).Append("\t");
            message.Append(RequestDeliveryDate).Append("\t");
            message.Append(OrderDate).Append("\t");
            message.Append(HeaderRemarks).Append("\t");
            message.Append(SOItemNumber).Append("\t");
            message.Append(ReferencedContractNumber).Append("\t");
            message.Append(Material).Append("\t");
            message.Append(Packer).Append("\t");
            message.Append(Consignee).Append("\t");
            message.Append(DeliveryDate).Append("\t");
            message.Append(OrderQuantity).Append("\t");
            message.Append(UOM).Append("\t");
            message.Append(VesselETD).Append("\t");
            message.Append(VesselETA).Append("\t");
            message.Append(VesselName).Append("\t");
            message.Append(VoyageReference).Append("\t");
            message.Append(ShippingLine).Append("\t");
            message.Append(POL).Append("\t");
            message.Append(POD).Append("\t");
            message.Append(ItemRemarks).Append("\t");
            message.Append(DeliveryBlockOrders).Append("\t");
            message.Append(supplementFlag);
        }

        public string Message
        {
            get { return this.message.ToString(); }
        }

        public string SoldToParty
        {
            get { return this.soldToParty; }
        }

        public string SOLineContent
        {
            get { return this.soLineContent; }
            set { this.soLineContent = value; }
        }
    }
}