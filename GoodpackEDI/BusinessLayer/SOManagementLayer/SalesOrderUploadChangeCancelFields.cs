﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.SOManagementLayer
{
    public class SalesOrderUploadChangeCancelFields
    {
    
        public IList<ChangeCancelSOFieldsModel>  ChangeCacelSOData()
        {
            // Create the array to store the field specs.
            ChangeCancelSOFieldsModel[] fileModel = new ChangeCancelSOFieldsModel[10];

            // Populate the model array 
            for (int i = 0; i < 10; i++)
            {
                fileModel[i] = new ChangeCancelSOFieldsModel();
            }

            // Assign details.
            fileModel[0].FieldSeq = 1;
            fileModel[0].FieldName = "Customer Code";
        
            fileModel[1].FieldSeq = 2;
            fileModel[1].FieldName = "CustomerPONo /SINumber";
         
            fileModel[2].FieldSeq = 3;
            fileModel[2].FieldName = "Date Required";
      
            fileModel[3].FieldSeq = 4;
            fileModel[3].FieldName = "Bin Type";
       
            fileModel[4].FieldSeq = 5;
            fileModel[4].FieldName = "Packer Code";
       
            fileModel[5].FieldSeq = 6;
            fileModel[5].FieldName = "Order Quantity";
  
            fileModel[6].FieldSeq = 7;
            fileModel[6].FieldName = "Reason for ETD Change";
   
            fileModel[7].FieldSeq = 8;
            fileModel[7].FieldName = "Reason for SKU Change";
      
            fileModel[8].FieldSeq = 9;
            fileModel[8].FieldName = "Reason for Quantity Change";
      
            fileModel[9].FieldSeq = 10;
            fileModel[9].FieldName = "Reason for Cancellation";

            return fileModel;          

        }
        public string SAPHeadersForChangeCancelFileUpload()
        {
            string SAPHeaders;
            SAPHeaders = "Customer|Cust PO Number|Request delivery date|Material|Packer|Order Quantity|||||";

            return SAPHeaders;

        }
        
        

        //public SalesOrderUploadChangeCancelFields(SourceMessage sourceMessageLine)
        //{
        //    if (sourceMessageLine.Fields.Count < 26)
        //    {
        //        throw new System.Exception("Field count in Goodyear Corporation message is less than the expected number of fields.");
        //    }

        //    this.CustPO = sourceMessageLine.Fields[0].Value.Trim();
        //    this.SONumber = sourceMessageLine.Fields[1].Value.Trim();
        //    this.SINumber = sourceMessageLine.Fields[2].Value.Trim();
        //    this.Ver = sourceMessageLine.Fields[3].Value.Trim();
        //    this.SIStatus = sourceMessageLine.Fields[4].Value.Trim();
        //    this.SIQtyinNumberofUnits = sourceMessageLine.Fields[5].Value.Trim();
        //    this.RubberGrade = sourceMessageLine.Fields[6].Value.Trim();
        //    this.Supplier = sourceMessageLine.Fields[7].Value.Trim();
        //    this.FactoryCode = sourceMessageLine.Fields[8].Value.Trim();
        //    this.PackagingType = sourceMessageLine.Fields[9].Value.Trim();
        //    this.LoadPort = sourceMessageLine.Fields[10].Value.Trim();
        //    this.LoadDate = sourceMessageLine.Fields[11].Value.Trim();
        //    this.ShippingLine = sourceMessageLine.Fields[12].Value.Trim();
        //    this.VesselName = sourceMessageLine.Fields[13].Value.Trim();
        //    this.VoyageNumber = sourceMessageLine.Fields[14].Value.Trim();
        //    this.BOLNumber = sourceMessageLine.Fields[15].Value.Trim();
        //    this.BOLQty = sourceMessageLine.Fields[16].Value.Trim();
        //    this.ContainerNumber = sourceMessageLine.Fields[17].Value.Trim();
        //    this.ContainerQty = sourceMessageLine.Fields[18].Value.Trim();
        //    this.OceanETD = sourceMessageLine.Fields[19].Value.Trim();
        //    this.ArrivalPort = sourceMessageLine.Fields[20].Value.Trim();
        //    this.ArrivalDate = sourceMessageLine.Fields[21].Value.Trim();
        //    this.CustomerCode = sourceMessageLine.Fields[22].Value.Trim();
        //    this.CustomerName = sourceMessageLine.Fields[23].Value.Trim();
        //    this.BookingNumber = sourceMessageLine.Fields[24].Value.Trim();
        //    this.EndofRecord = sourceMessageLine.Fields[25].Value.Trim();
        //    this.originalLine = sourceMessageLine.OriginalLineContent;
        //}
    }
}