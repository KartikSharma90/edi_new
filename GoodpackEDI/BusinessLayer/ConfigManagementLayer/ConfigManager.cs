﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodpackEDI.ViewModels;
using GoodpackEDI.Utilities;
using GPArchitecture.DataLayer.Models;


namespace GoodpackEDI.BusinessLayer.ConfigManagementLayer
{
    public class ConfigManager : IConfigManager
    {
        private List<ConfigDetailsModel> configList = new List<ConfigDetailsModel>();

        /// <summary>
        /// Get configuration list 
        /// </summary>
        /// <returns></returns>
        public IList<ConfigDetailsModel> GetConfigDetails()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from c in context.Gen_Configuration
                        join u in context.Gen_User on c.LastUpdatedBy equals u.Id
                        select new ConfigDetailsModel
                        {
                            Id = c.Id,
                            ConfigItem = c.ConfigItem,
                            Value = c.Value,
                            Description = c.Description,
                            LastUpdatedBy = c.Gen_User.Username,
                            LastUpdatedDate = c.LastUpdatedDate
                        }).ToList();
            }
        }


        ///<summary>
        ///Add Configuration
        ///</summary>
        ///<param name="addconfigDetailsModel">ConfigDetailsModel</param>
        ///<param name="username">Username</param>
        ///<returns></returns>
        public ConfigDetailsModel AddConfigDetails(ConfigDetailsModel addConfigDetailsModel, string username)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int updatedBy = (from s in context.Gen_Configuration
                                 join user in context.Gen_User on s.LastUpdatedBy equals user.Id
                                 select user.Id).FirstOrDefault();
                System.DateTime updatedDate = System.DateTime.Now;

                Gen_User userTable = (from s in context.Gen_User
                                      where s.Username == username
                                      select s).FirstOrDefault();

                Gen_Configuration configDetails = new Gen_Configuration();
                configDetails.Id = addConfigDetailsModel.Id;
                configDetails.ConfigItem = addConfigDetailsModel.ConfigItem;
                configDetails.Value = addConfigDetailsModel.Value;
                configDetails.Description = addConfigDetailsModel.Description;
                configDetails.LastUpdatedBy = userTable.Id;
                configDetails.LastUpdatedDate = DateTime.Now;

                ConfigDetailsModel configModel = new ConfigDetailsModel();
                configModel.Id = addConfigDetailsModel.Id;
                configModel.ConfigItem = addConfigDetailsModel.ConfigItem;
                configModel.Value = addConfigDetailsModel.Value;
                configModel.Description = addConfigDetailsModel.Description;
                configModel.LastUpdatedBy = username;
                configModel.LastUpdatedDate = DateTime.Now;
                context.Gen_Configuration.Add(configDetails);
                configList.Add(configModel);
                context.SaveChanges();
                return configModel;
            }
        }

        ///<summary>
        ///Edit Config
        ///</summary>
        ///<param name="editconfigDetailsModel">ConfigDetailsModel</param>
        ///<param name="username">Username</param>
        ///<returns></returns>
        public bool UpdateConfigDetails(ConfigDetailsModel editConfigDetailsModel, string username)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User userTable = (from s in context.Gen_User
                                      where s.Username == username
                                      select s).FirstOrDefault();
                Gen_Configuration config = context.Gen_Configuration.FirstOrDefault(i => i.Id == editConfigDetailsModel.Id);
                config.ConfigItem = editConfigDetailsModel.ConfigItem;
                config.Value = editConfigDetailsModel.Value;
                config.Description = editConfigDetailsModel.Description;
                config.LastUpdatedDate = System.DateTime.Now;
                config.LastUpdatedBy = userTable.Id;
                context.SaveChanges();
                return true;
            }
        }

        ///  <summary>
        ///  Delete Config 
        ///  </summary>
        ///  <param name="configId">ConfigurationId</param>
        ///  <returns></returns>
        public bool DeleteConfigDetails(int configId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_Configuration configDetails = context.Gen_Configuration.Find(configId);
                context.Gen_Configuration.Remove(configDetails);
                context.SaveChanges();
                return true;
            }
        }

    }
}