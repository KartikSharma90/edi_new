﻿using System.Collections.Generic;
using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.ConfigManagementLayer
{
    interface IConfigManager
    {
        IList<ConfigDetailsModel> GetConfigDetails();
        ConfigDetailsModel AddConfigDetails(ConfigDetailsModel addConfigDetailsModel, string username);
        bool UpdateConfigDetails(ConfigDetailsModel editConfigDetailsModel, string username);
        bool DeleteConfigDetails(int configId);

    }
}