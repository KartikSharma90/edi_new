﻿using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.TermTripManagementLayer
{
    interface ITirmTripManager
    {
        bool CheckMultipleTripExist(List<Dictionary<string, string>> dataValue);
        bool CheckValidCustomer(List<Dictionary<string, string>> dataValue);
        bool IsTirmTripOrNot(Dictionary<string, string> dataValue);
        bool IsTirmTripORNot(string CustomerCode);
    }
}
