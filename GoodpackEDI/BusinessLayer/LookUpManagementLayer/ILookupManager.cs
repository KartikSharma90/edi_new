﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.LookUpManagementLayer
{
    interface ILookupManager
    {
        void AddLookup(ArrayList rowDetailsArr, string Username);
        void AddLookupForUpload(ArrayList rowDetailsArr, string Username);
        List<ViewLookupModel> GetLookupDetails(string username, int susbyId, int lookupNameId, string mapperName);

        void EditLookup(LookupModel LookupModelData, string Username);
        void EditLookupNew(LookUpModelUpdate LookupModelData, string Username);
        void DeleteSelectedLookupDetails(int lookupRowId, string Username);

    }
}
