﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodpackEDI.BusinessLayer.ASNManagementLayer
{
    public class MsgSapASNLine
    {
         // Field list
        StringBuilder message;

        // Fields
        string DocumentType;
        public string ITRNumber;
        string ReferenceITRNumber;
        string FromLocation;
        string ToLocation;
        string HeaderText;
        string OceanFreightIndicator;
        public string CustomerReferenceNumber;
        string FlightBookingCodes;
        string VesselETD;
        string VesselETA;
        string VesselATD;
        string VesselATA;
        string ShippingLine;
        string VesselName;
        string VoyageReference;
        string POL;
        string POD;
        string CustomDocumentReference;
        string TranshipmentPort;
        string TranshipmentETD;
        string TranshipmentETA;
        public string ItemNo;
        public string MaterialNumber;
        string Quantity;
        string ContainerNumber;
        public string CustomerNumber;
        public string SINumber;
        public string SalesDocument;
        public string SalesDocumentItem;
        string GoodsIssueDate;
        string ETDDate;
        string DTADate;
        string Remarks;
        string Damage;
        public string supplementFlag;
        private string pscLineContent;
        public MsgSapASNLine(IList<string> fieldListSrc)
        {
            if (fieldListSrc.Count < 35)
            {
                throw new Exception("The SAP Advanced Shipment Notification line does not have enough fields.");
            }

            DocumentType = fieldListSrc[0];
            ITRNumber = fieldListSrc[1];
            ReferenceITRNumber = fieldListSrc[2];
            FromLocation = fieldListSrc[3];
            ToLocation = fieldListSrc[4];
            HeaderText = fieldListSrc[5];
            OceanFreightIndicator = fieldListSrc[6];
            CustomerReferenceNumber = fieldListSrc[7];
            FlightBookingCodes = fieldListSrc[8];
            VesselETD = fieldListSrc[9];
            VesselETA = fieldListSrc[10];
            VesselATD = fieldListSrc[11];
            VesselATA = fieldListSrc[12];
            ShippingLine = fieldListSrc[13];
            VesselName = fieldListSrc[14];
            VoyageReference = fieldListSrc[15];
            POL = fieldListSrc[16];
            POD = fieldListSrc[17];
            CustomDocumentReference = fieldListSrc[18];
            TranshipmentPort = fieldListSrc[19];
            TranshipmentETD = fieldListSrc[20];
            TranshipmentETA = fieldListSrc[21];
            ItemNo = fieldListSrc[22];
            MaterialNumber = fieldListSrc[23];
            Quantity = fieldListSrc[24];
            ContainerNumber = fieldListSrc[25];
            CustomerNumber = fieldListSrc[26];
            SINumber = fieldListSrc[27];
            SalesDocument = fieldListSrc[28];
            SalesDocumentItem = fieldListSrc[29];
            GoodsIssueDate = fieldListSrc[30];
            ETDDate = fieldListSrc[31];
            DTADate = fieldListSrc[32];
            Remarks = fieldListSrc[33];
            Damage = fieldListSrc[34];
            supplementFlag = fieldListSrc[35];
        }

        public long getPackerCode()
        {
            if (FromLocation == "" || FromLocation == null)
            { return -1; }
            else
                return long.Parse(FromLocation);
        }

        public void GenerateSAPMessage()
        {
            message = new StringBuilder();
            message.Append(DocumentType).Append("\t");
            message.Append(ITRNumber).Append("\t");
            message.Append(ReferenceITRNumber).Append("\t");
            message.Append(FromLocation).Append("\t");
            message.Append(ToLocation).Append("\t");
            message.Append(HeaderText).Append("\t");
            message.Append(OceanFreightIndicator).Append("\t");
            message.Append(CustomerReferenceNumber).Append("\t");
            message.Append(FlightBookingCodes).Append("\t");
            message.Append(VesselETD).Append("\t");
            message.Append(VesselETA).Append("\t");
            message.Append(VesselATD).Append("\t");
            message.Append(VesselATA).Append("\t");
            message.Append(ShippingLine).Append("\t");
            message.Append(VesselName).Append("\t");
            message.Append(VoyageReference).Append("\t");
            message.Append(POL).Append("\t");
            message.Append(POD).Append("\t");
            message.Append(CustomDocumentReference).Append("\t");
            message.Append(TranshipmentPort).Append("\t");
            message.Append(TranshipmentETD).Append("\t");
            message.Append(TranshipmentETA).Append("\t");
            message.Append(ItemNo).Append("\t");
            message.Append(MaterialNumber).Append("\t");
            message.Append(Quantity).Append("\t");
            message.Append(ContainerNumber).Append("\t");
            message.Append(CustomerNumber).Append("\t");
            message.Append(SINumber).Append("\t");
            message.Append(SalesDocument).Append("\t");
            message.Append(SalesDocumentItem).Append("\t");
            message.Append(GoodsIssueDate).Append("\t");
            message.Append(ETDDate).Append("\t");
            message.Append(DTADate).Append("\t");
            message.Append(Remarks).Append("\t");
            message.Append(Damage).Append("\t");
            message.Append(supplementFlag);
        }


        public string Message
        {
            get { return this.message.ToString(); }
        }

        public string PSCLineContent
        {
            get { return this.pscLineContent; }
            set { this.pscLineContent = value; }
        }
    }
}