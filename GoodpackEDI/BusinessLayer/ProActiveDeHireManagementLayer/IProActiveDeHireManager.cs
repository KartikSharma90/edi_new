﻿using GoodpackEDI.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.ProActiveDeHireManagementLayer
{
    interface IProActiveDeHireManager
    {
        bool ProActiveDeHireUrlValidator(string consigneeCode, string startDate, string endDate, string binTypes,string key);
        ProActiveDehireReturnModel SubmitDehireData(JObject submitData, string Sdate, string Edate, string Consigneecode, string Bintype, string Key);
    }
}
