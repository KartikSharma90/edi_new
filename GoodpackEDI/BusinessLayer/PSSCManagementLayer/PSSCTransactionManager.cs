﻿using GoodpackEDI.BusinessLayer.SCManagementLayer;
using GoodpackEDI.BusinessLayer.SSCManagementLayer;
using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.PSSCManagementLayer
{
    public class PSSCTransactionManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PSSCTransactionManager));
        SCTransactionBatch scTransBatch = new SCTransactionBatch();
        int successCount = 0;
        int errorCount = 0;
        int trLineID=0;
        SCTransactionModel objGPCustomerMessage;
        IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
        List<List<ResponseModel>> serviceRespnseData = new List<List<ResponseModel>>();
        public IList<GenericItemModel> DataProcessing(MapOutput mappingOutput, SCTransactionModel objGPCustomerMessage, string mapperFile)
        {
            try
            {
                this.objGPCustomerMessage = objGPCustomerMessage;
                List<Dictionary<string, string>> sscData = new List<Dictionary<string, string>>();
                objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                int j = 0;
                // Update/Insert lines in TRANS_LINE table
                foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                {
                    objLineReferences.StatusCode = LineStatus.NEW;
                    string customerReference = objGPCustomerMessage.mappingServiceValues[j][SCSapMessageFields.CustomerReferenceNumber];
                    string itrNumber = objGPCustomerMessage.mappingServiceValues[j][SCSapMessageFields.ITRNumber];
                    objGPCustomerMessage.mappingServiceValues[j][SCSapMessageFields.EdiNumber] = objLineReferences.EdiNumber;
                    //int transID = 0;
                    //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);

                    objGPCustomerMessage.mappingServiceValues[j][SCSapMessageFields.LineId] = scTransBatch.InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber).ToString();

                    j++;
                }
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);

                    scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);


                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    Dictionary<int, Dictionary<string, string>> sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;

                    List<Dictionary<string, string>> data = new List<Dictionary<string, string>>();
                    //Cloning in of data to another list for iterating
                    objGPCustomerMessage.mappingServiceValues.ForEach((item) =>
                    {
                        data.Add(new Dictionary<string, string>(item));
                    });
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        sapSubmitTransData.Add(Convert.ToInt32(objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.LineId]), objGPCustomerMessage.mappingServiceValues[i]);
                    }
                    while (data.Count != 0)
                    {

                        string customerRefNumber = data[0][SCSapMessageFields.CustomerReferenceNumber];
                        sscData = data.FindAll(i => i[SCSapMessageFields.CustomerReferenceNumber] == customerRefNumber);
                        for (int i = 0; i < sscData.Count; i++)
                        {
                            int trLineID = Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                            log.Info("Updating TransLine table with excel data");
                            scTransBatch.UpdateSSCTransLineForData(trLineID, sscData[i]);
                        }

                        try
                        {
                            List<ResponseModel> responseModel = null;

                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SSC webservice");

                            responseModel = sscCaller.sscServiceCaller(sscData, null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId);

                            serviceRespnseData.Add(responseModel);
                            for (int i = 0; i < sscData.Count; i++)
                            {
                                serviceResponse.Add(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), responseModel);
                                bool isSuccess = false;
                                isSuccess = scTransBatch.InsertSSCTransLineContent(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), objGPCustomerMessage.BatchID, responseModel);
                                log.Info("Insertion of trans line content success status:" + isSuccess);
                                if (isSuccess)
                                {
                                    log.Info("Line status of trnLineId:" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]) + " is :" + LineStatus.SUCCESS.ToString());

                                    scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), false);
                                }
                                else
                                {
                                    log.Info("Line status of trnLineId:" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]) + " is :" + LineStatus.ERROR.ToString());
                                    scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), false);
                                }
                                //  sapSubmitTransData.Add(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), objGPCustomerMessage.mappingServiceValues[i]);
                                scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, false);
                                log.Info("Inserting into TransLineContent table of transLineId:-" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]));
                            }

                        }

                        catch (Exception e)
                        {
                            log.Error("Webservice in PSSCTransaction Manager DataProcessing method.mapperFile:" + mapperFile +", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + objGPCustomerMessage.mappingServiceValues + ". Api Exception : Message- " + e.Message);

                            // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            // model.Message = "Exception Message :" + e.Message;    
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            // GenericItemModel itemsModel = new GenericItemModel();
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;

                            for (int i = 0; i < sscData.Count; i++)
                            {
                                trLineID = Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                                SSCTransactionData(responseModel, false);
                                serviceResponse.Add(trLineID, responseModel);
                            }
                            scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, false);
                        }
                        data.RemoveAll(i => i[SCSapMessageFields.CustomerReferenceNumber] == customerRefNumber);
                    }
                }
                int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                GenericItemModel itemModel=null;
                IList<GenericItemModel> itemModelVal = new List<GenericItemModel>();
                for (int mapVal = 0; mapVal<objGPCustomerMessage.mappingServiceValues.Count; mapVal++)
                {
                    itemModel = new GenericItemModel();
                    itemModelVal.Add(itemModel);
                }
                for (int data = 0; data < serviceRespnseData.Count; data++)
                {
                    genericModel = scTransBatch.SScWebServiceResult(itemModelVal, objGPCustomerMessage.mappingServiceValues, serviceRespnseData[data]);
                    itemModelData.Add(genericModel[data]); 
                }
                //Gen_User userDetails;
                //using (var dataContext = new GoodpackEDIEntities())
                //{
                //    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                //}

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", "Goodpack User");
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
              //  string[] recipientList = new string[] { };
                // GPTools.SendEmail(placeholders, recipientList, "NOTIFY_CUSTOMER", null);
                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER", null);
                log.Info("Email sending completed");
                //for (int i = 0; i < genericModel.Count; i++)
                //{                   
                //        itemModelData.Add(genericModel[i]);                   
                //}
                log.Info("Leaving from Webservice PsscTransaction Manager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error("Webservice in PSSCTransaction Manager DataProcessing method.mapperFile:" + mapperFile + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + objGPCustomerMessage.mappingServiceValues + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;


            }

        }

        public void SSCTransactionData(List<ResponseModel> responseModel, bool isFileData)
        {
            scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
            scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);


        }

        public string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();          
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GoodpackEDI.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
    }
}