﻿using System.Linq;
using GoodpackEDI.ViewModels;
namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    public class PasswordManager : IPasswordManager
    {
        HashComputer m_hashComputer = new HashComputer();

        /// <summary>
        /// Password hash generator
        /// </summary>
        /// <param name="plainTextPassword"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public string GeneratePasswordHash(string plainTextPassword, out string salt)
        {
            salt = SaltGenerator.GetSaltString();

            string finalString = plainTextPassword + salt;

            return m_hashComputer.GetPasswordHashAndSalt(finalString);
        }
        /// <summary>
        /// Checks whether passowrd matches
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        public bool IsPasswordMatch(string password, string salt, string hash)
        {
            string finalString = password + salt;
            return hash == m_hashComputer.GetPasswordHashAndSalt(finalString);
        }



        /// <summary>
        /// Changes old password and creates new one
        /// </summary>
        /// <param name="accountParams"></param>

        //public ResponseModel ChangePassword(ChangePasswordModel accountParams, string username)
        //{
        //    ResponseModel responseModel = new ResponseModel();
        //    string salt = null;
        //    using (var context=new CustomerPortalEntities())
        //    {
        //        Gen_User user = (from s in context.Gen_User
        //                         where s.Username == username
        //                         select s).FirstOrDefault();

        //        if ((user != null) && (IsPasswordMatch(accountParams.OldPassword, user.PasswordSalt, user.Password)))
        //        {
        //            user.Password = GeneratePasswordHash(accountParams.NewPassword, out salt);
        //            user.PasswordSalt = salt;
        //            context.SaveChanges();
        //            responseModel.Success = true;
        //            responseModel.Message = "Password Changed Successfully.Please login again";
        //            return responseModel;
        //        }
        //        else
        //        {
        //            responseModel.Success = false;
        //            responseModel.Message = "Old password entered is incorrect.Please try again";
        //            return responseModel;
        //        }
        //    }
        //}
    }
}