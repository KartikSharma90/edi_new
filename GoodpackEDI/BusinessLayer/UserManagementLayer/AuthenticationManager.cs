﻿using System.Linq;
using GoodpackEDI.ViewModels;
using GoodpackEDI.BusinessLayer.UserManagementLayer;
using System;
using System.Web.Security;
using GPArchitecture.DataLayer.Models;
using log4net;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
   
    /// <summary>
    /// Handles the Authentication mechanism of the user
    /// </summary>
    /// 
    public class AuthenticationManager : IAuthenticationManager
    {
       
        private LoginResponseModel responseModel;
        private IPasswordManager pwdManager;
        private static readonly ILog log = LogManager.GetLogger(typeof(AuthenticationManager));
        private IPrevilegeManager privilegeManager = new PrevilegeManager();
     /// <summary>
     ///  Checks the type of authentication
     /// </summary>
     /// <param name="loginModel"></param>
     /// <returns></returns>

        public LoginResponseModel AuthenticateUser(UserLoginModel loginModel)
        {
            responseModel = new LoginResponseModel();
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = (from s in context.Gen_User
                                 where s.Username == loginModel.Username
                                 select s).FirstOrDefault();

                if (user != null)
                {
                    if (user.IsActive)
                    {
                        return CheckAuthentication(loginModel);
                    }

                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Your account is deleted/deactivated.Please contact administrator";
                        return responseModel;
                    }
                }

                else
                {
                    responseModel.Success = false;
                    responseModel.Message = "Please check your username/password and try again";
                    return responseModel;
                }
            }   
        }
        /// <summary>
        /// Authenticate External User
        /// </summary>
        /// <returns></returns>
        private LoginResponseModel CheckAuthentication(UserLoginModel loginModel)
        {
            pwdManager = new PasswordManager();
            using (var context = new GoodpackEDIEntities())
            {
                try
                {
                    UserLoginDataModel user = (
                             from s in context.Gen_User
                             where s.Username == loginModel.Username
                             select new UserLoginDataModel
                             {
                                 UserId = s.Id,                              
                                 PasswordSalt = s.PasswordSalt,
                                 Password = s.Password,
                                 Username = s.Username                                
                             }).FirstOrDefault<UserLoginDataModel>();

                    if (user != null)
                    {
                        
                            if (pwdManager.IsPasswordMatch(loginModel.Password.Trim(), user.PasswordSalt, user.Password))
                            {
                                FormsAuthentication.SetAuthCookie(loginModel.Username, false);
                                responseModel.Success = true;
                                responseModel.Message = "Login Successfull";
                                responseModel.AccessLinks = privilegeManager.LinkAccessPrivileges(loginModel.Username);
                                return responseModel;
                            }
                            else
                            {
                                responseModel.Success = false;
                                responseModel.Message = "The user name or password provided is incorrect.Please try again";
                                return responseModel;
                            }                                               
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "You are not registered.Please register into Customer portal";
                        return responseModel;
                    }
                }
                catch (Exception e)
                {
                    log.Error("External AuthenticateUser: Exception for user:" + loginModel.Username + ", method error " + e.Message);
                    responseModel.Success = false;
                    responseModel.Message = "The user name or password provided is incorrect.Please try again";
                    return responseModel;
                }
            }
        }
    }
}