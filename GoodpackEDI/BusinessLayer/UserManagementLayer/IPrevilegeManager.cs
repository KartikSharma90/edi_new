﻿using System.Collections.Generic;
using GoodpackEDI.ViewModels;
using GoodpackEDI.Utilities;


namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    public interface IPrevilegeManager
    {
        IList<PrivilegeModel> GetPrevileges(int roleId);
        IList<PrivilegeModel> MappedPrevilege(int role);
        bool CreatePrivilege(List<PrivilegeModel> privilegeModel, int role);
        bool IsPrevilegeToAccess(string username, EnumAppPrevileges typeOfPrevilege);
        PrivilegeAccessModel LinkAccessPrivileges(string username);
    }
}
