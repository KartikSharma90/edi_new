﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
//using log4net;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    /// <summary>
    /// RegistrationManager manages the customer registration
    /// </summary>
    public class RegistrationManager : IRegistrationManager
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(RegistrationManager));
        private PasswordManager pwdManager;
        private UserAccountManager accountManager ;
        private ResponseModel responseModel;        
        public RegistrationManager()
        {
            pwdManager = new PasswordManager();
            accountManager = new UserAccountManager();
            responseModel = new ResponseModel();
        }

        /// <summary>
        /// Registers new user
        /// </summary>
        /// <param name="registerParams"></param>

        public ResponseModel UserRegistration(Record registerParams, string username)
        {

            using (var context = new GoodpackEDIEntities())
            {
                var userExists = accountManager.CheckUserExists(registerParams.Username, registerParams.EmailId);                
                if (userExists == null)
                {
                  
                        string salt = null;
                        Gen_User registerUser = new Gen_User();
                        registerUser.Username = registerParams.Username;
                        registerUser.Company = registerParams.Company;
                        registerUser.DateCreated=DateTime.Now;
                        registerUser.DateLastActivity=DateTime.Now;
                        registerUser.DateLastLogin=DateTime.Now;
                        registerUser.DateLastPasswordChange=DateTime.Now;
                        registerUser.EmailId=registerParams.EmailId;
                        registerUser.FullName=registerParams.FullName;
                        registerUser.IsActive=registerParams.IsActive;
                        registerUser.IsInternalGoodpackUser=true;
                        registerUser.Password = pwdManager.GeneratePasswordHash(registerParams.Password.Trim(), out salt);
                        registerUser.PasswordSalt = salt;
                        registerUser.ReceiveAdminEmail=registerParams.ReceiveAdminEmail;
                        registerUser.ReceiveNotifications=registerParams.ReceiveNotifications;
                        registerUser.RoleId=registerParams.Value;
                        context.Gen_User.Add(registerUser);
                        context.SaveChanges();
                        responseModel.Success = true;                       
                }

                else
                {
                    responseModel.Success = false;
                    responseModel.Message = ConstantUtilities.RegUserAlreadyRegisteredMessage;                
                }
                return responseModel;
            }
        }
    }
}