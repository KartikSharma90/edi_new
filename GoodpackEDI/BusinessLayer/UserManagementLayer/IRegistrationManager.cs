﻿using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    interface IRegistrationManager
    {
        ResponseModel UserRegistration(Record registerParams, string username);
    }
}
