﻿
using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    interface IAuthenticationManager
    {        
        LoginResponseModel AuthenticateUser(UserLoginModel loginModel);
    }
}
