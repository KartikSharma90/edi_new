﻿using System;
using System.Collections.Generic;
using System.Linq;
using GoodpackEDI.ViewModels;
using GoodpackEDI.Utilities;
using GPArchitecture.DataLayer.Models;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    public class UserManager
    {
        /// <summary>
        /// Get user list 
        /// </summary>
        /// <param name="username">Username</param>
        /// <param name="id">User Type</param>
        /// <returns></returns>
        public IList<UserDetailsModel> GetUsers(string username)
        {
            using (var context = new GoodpackEDIEntities())
            {

                return (from s in context.Gen_User
                        join role in context.Ref_UserRoles on s.RoleId equals role.Id
                        orderby s.Username
                        select new UserDetailsModel
                        {
                            UserId = s.Id,
                            Username = s.Username,
                            FullName = s.FullName,
                            EmailId = s.EmailId,
                            Company = s.Company,
                            DateCreated = s.DateCreated ?? DateTime.MinValue,
                            ReceiveAdminEmail = s.ReceiveAdminEmail,
                            ReceiveNotifications = s.ReceiveNotifications,
                            Value = role.Id,
                            // DisplayText = role.UserRole, 
                            IsActive = s.IsActive
                        }).ToList();
            }
        }
        //  <summary>
        //  Delete user 
        //  </summary>
        //  <param name="id">User Id</param>
        //  <param name="username">Username</param>
        //  <returns></returns>
        public bool DeleteUser(int id)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = context.Gen_User.Find(id);
                context.Gen_User.Remove(user);
                context.SaveChanges();
                return true;
            }
        }

        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="userModel">User Model</param>
        /// <param name="username">Username</param>
        /// <returns></returns>
        public bool UpdateUser(Record userModel)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Gen_User user = context.Gen_User.FirstOrDefault(i => i.Id == userModel.UserId);

                user.FullName = userModel.FullName;
                user.IsActive = userModel.IsActive;
                user.Company = userModel.Company;
                
                PasswordManager pwdManager = new PasswordManager();
                string salt = "";
                user.Password = pwdManager.GeneratePasswordHash(userModel.Password.Trim(), out salt);
                user.PasswordSalt = salt;
                user.DateLastActivity = DateTime.Now;
                user.EmailId = userModel.EmailId;
                user.ReceiveAdminEmail = userModel.ReceiveAdminEmail;
                user.ReceiveNotifications = userModel.ReceiveNotifications;
                user.RoleId = userModel.Value;
                context.SaveChanges();
                return true;
            }
        }

        public IList<UserModel> GetAllUsers(int role)
        {
            IList<DropdownModel> query = new List<DropdownModel>();
            string roleType;
            switch (role)
            {
                case 0: roleType = ConstantUtility.HQUser;
                    break;
                case 1: roleType = ConstantUtility.SubsiUser;
                    break;
                case 2: roleType = ConstantUtility.HQAdminUser;
                    break;
                default: roleType = null;
                    break;
            }
            if (roleType != null)
            {
                using (var context = new GoodpackEDIEntities())
                {
                    Ref_UserRoles roles = (from r in context.Ref_UserRoles
                                           where r.UserRole == roleType
                                           select r).FirstOrDefault();
                    return (from s in context.Gen_User
                            where (s.IsActive == true) && (s.RoleId == roles.Id)
                            select new UserModel
                            {
                                UserId = s.Id,
                                Username = s.FullName
                            }).ToList();
                }
            }
            else {

                return new List<UserModel>();
            }
        }
        public bool ChangePassword(ChangePassowrdModel changeModel)
        {
            bool returnValue = false;
            try
            {
                using (var context = new GoodpackEDIEntities())
                {
                    string salt = "";
                    PasswordManager pwdManager = new PasswordManager();
                    Gen_User user = context.Gen_User.FirstOrDefault(i => i.Username == changeModel.UserName);
                    user.Password = pwdManager.GeneratePasswordHash(changeModel.Newpassword.Trim(), out salt);
                    user.PasswordSalt = salt;
                    context.SaveChanges();
                    returnValue = true;
                }
            }
            catch (Exception e)
            {
                returnValue = false;
            }
            return returnValue;
        }

    }
}

        //public string GetSapCodeOfUser (string username)
        //{
        //    using (var context=new CustomerPortalEntities())
        //    {
        //        return (from s in context.Gen_User
        //                         where (s.Username == username) && (s.IsActive == true)
        //                         select s.SAPCode).FirstOrDefault();
        //    }

        //}

    //}
       




   
      