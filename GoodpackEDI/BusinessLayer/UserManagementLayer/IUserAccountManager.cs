﻿using GoodpackEDI.ViewModels;

namespace GoodpackEDI.BusinessLayer.UserManagementLayer
{
    interface IUserAccountManager
    {       
        string CheckUserExists(string userName, string emailId);
        
    }
}
