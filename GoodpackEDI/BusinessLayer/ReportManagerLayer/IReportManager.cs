﻿using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.ReportManagerLayer
{
    interface IReportManager
    {
        IQueryable<ReportSOTransactionModel> GetSOTransactionReport(int trnasactionType,string userName);
        IQueryable<ReportSCTransactionModel> GetSCTransactionReport(int trnasactionType, string userName);
        IQueryable<ReportSSCTransactionModel> GetSSCTransactionReport(int trnasactionType, string userName);
    }
}
