﻿using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.ReportManagerLayer
{
    public class ReportManager : IReportManager
    {
        GoodpackEDIEntities context;
        public ReportManager()
        {
            context = new GoodpackEDIEntities();
        }
        public IQueryable<ReportSOTransactionModel> GetSOTransactionReport(int trnasactionType, string userName)
        {
            Gen_User user = (from s in context.Gen_User
                             where s.Username == userName
                             select s).FirstOrDefault();
            string role = (from s in context.Ref_UserRoles
                           where s.Id == user.RoleId
                           select s.UserRole).FirstOrDefault();

            if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
            {
                var subsiIds = (from s in context.Trn_UserToSubsiMapper
                                join p in context.Gen_EDISubsies on s.SubsiId equals p.Id
                                where s.UserId == user.Id && p.IsEnabled==true
                                orderby p.SubsiName
                                select p.Id).ToList();
                return (from transBatch in context.Trn_SCTransBatch
                        join transLine in context.Trn_SOTransLine
                             on transBatch.Id equals transLine.BatchId                       
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsiIds.Contains(transBatch.SubsyId)
                        select new ReportSOTransactionModel
                        {
                            FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerNumber = transLine.CustomerNumber,
                            CustomerPONumber = transLine.CustomerPONumber,
                            CustomerPODate = transLine.CustomerPODate,
                            MaterialNumber = transLine.MaterialNumber,
                            FromLocation = transLine.FromLocation,
                            Consignee = transLine.Consignee,
                            SOQuantity = transLine.SOQuantity,
                            POD = transLine.POD,
                            POL = transLine.POL,
                            ETA = transLine.ETA,
                            ETD = transLine.ETD,
                            ReqDeliveryDate = transLine.ReqDeliveryDate,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode//,
                            //LineContent = string.Join(",", transLine.Trn_SOTransLineContent.Where(i => i.LineID == transLine.Id).Select(i => i.SapResponseMessage).ToArray()),
                        }).AsQueryable();

                //return (from transBatch in context.Trn_SCTransBatch
                //        join transLine in context.Trn_SOTransLine
                //            on transBatch.Id equals transLine.BatchId
                //        join subsi in context.Gen_EDISubsies
                //            on subsi.Id equals transBatch.SubsyId
                //        where transBatch.Ref_TransactionTypes.Id == trnasactionType
                //        select new ReportSOTransactionModel
                //        {
                //            FileName = transBatch.FileName,
                //            UploadedDate = transBatch.DateUpdated,
                //            Batchid = transLine.BatchId,
                //            CustomerNumber = transLine.CustomerNumber,
                //            CustomerPONumber = transLine.CustomerPONumber,
                //            CustomerPODate = transLine.CustomerPODate,
                //            MaterialNumber = transLine.MaterialNumber,
                //            FromLocation = transLine.FromLocation,
                //            Consignee = transLine.Consignee,
                //            SOQuantity = transLine.SOQuantity,
                //            POD = transLine.POD,
                //            POL = transLine.POL,
                //            ETA = transLine.ETA,
                //            ETD = transLine.ETD,
                //            ReqDeliveryDate = transLine.ReqDeliveryDate,
                //            Mode = transLine.InTestMode ? "Verified" : "Processed",
                //            UniqueId = transLine.Id,
                //            Status = transLine.Trn_LineStatus.StatusCode//,
                //            //LineContent = string.Join(",", transLine.Trn_SOTransLineContent.Where(i => i.LineID == transLine.Id).Select(i => i.SapResponseMessage).ToArray()),
                //        }).AsQueryable();
            }
            else
            {
                return (from transBatch in context.Trn_SCTransBatch
                        join transLine in context.Trn_SOTransLine
                            on transBatch.Id equals transLine.BatchId
                        join subsi in context.Gen_EDISubsies
                            on transBatch.SubsyId equals subsi.Id
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsi.IsEnabled==true
                        select new ReportSOTransactionModel
                        {
                            FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerNumber = transLine.CustomerNumber,
                            CustomerPONumber = transLine.CustomerPONumber,
                            CustomerPODate = transLine.CustomerPODate,
                            MaterialNumber = transLine.MaterialNumber,
                            FromLocation = transLine.FromLocation,
                            Consignee = transLine.Consignee,
                            SOQuantity = transLine.SOQuantity,
                            POD = transLine.POD,
                            POL = transLine.POL,
                            ETA = transLine.ETA,
                            ETD = transLine.ETD,
                            ReqDeliveryDate = transLine.ReqDeliveryDate,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode//,
                            //LineContent = string.Join(",", transLine.Trn_SOTransLineContent.Where(i => i.LineID == transLine.Id).Select(i => i.SapResponseMessage).ToArray()),
                        }).AsQueryable();
            }
        }

        public IQueryable<ReportSCTransactionModel> GetSCTransactionReport(int trnasactionType, string userName)
        {          
            Gen_User user = (from s in context.Gen_User
                             where s.Username == userName
                             select s).FirstOrDefault();
            string role = (from s in context.Ref_UserRoles
                           where s.Id == user.RoleId
                           select s.UserRole).FirstOrDefault();

            if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
            {
                var subsiIds = (from s in context.Trn_UserToSubsiMapper
                                join p in context.Gen_EDISubsies on s.SubsiId equals p.Id
                                where s.UserId == user.Id && p.IsEnabled == true
                                orderby p.SubsiName
                                select p.Id).ToList();
                return (from transBatch in context.Trn_SCTransBatch
                        join transLine in context.Trn_SCTransLine
                            on transBatch.Id equals transLine.BatchId
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsiIds.Contains(transBatch.SubsyId)
                        select new ReportSCTransactionModel
                        {
                            FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerCode = transLine.CustomerCode,
                            ReferenceNumber = transLine.CustomerRefNumber,
                            packerLocation = transLine.FromLocation,
                            consigneeLocation = transLine.ToLocation,
                            BinType = transLine.BinType,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            Qty = SqlFunctions.StringConvert((decimal)transLine.Quantity).Trim(),
                            ETA = transLine.ETA,
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode,
                            ETD = transLine.ETD
                        }).AsQueryable();
            }
            else
            {
                return (from transBatch in context.Trn_SCTransBatch
                        join transLine in context.Trn_SCTransLine
                            on transBatch.Id equals transLine.BatchId
                        join subsi in context.Gen_EDISubsies
                           on transBatch.SubsyId equals subsi.Id
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsi.IsEnabled == true                       
                        select new ReportSCTransactionModel
                        {
                            FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerCode = transLine.CustomerCode,
                            ReferenceNumber = transLine.CustomerRefNumber,
                            packerLocation = transLine.FromLocation,
                            consigneeLocation = transLine.ToLocation,
                            BinType = transLine.BinType,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            Qty = SqlFunctions.StringConvert((decimal)transLine.Quantity).Trim(),
                            ETA = transLine.ETA,
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode,
                            ETD = transLine.ETD
                        }).AsQueryable();

            }
        }

        public IQueryable<ReportSSCTransactionModel> GetSSCTransactionReport(int trnasactionType, string userName)
        {
            Gen_User user = (from s in context.Gen_User
                             where s.Username == userName
                             select s).FirstOrDefault();
            string role = (from s in context.Ref_UserRoles
                           where s.Id == user.RoleId
                           select s.UserRole).FirstOrDefault();

            if ((role == ConstantUtility.SubsiUser) || (role == ConstantUtility.HQUser))
            {
                var subsiIds = (from s in context.Trn_UserToSubsiMapper
                                join p in context.Gen_EDISubsies on s.SubsiId equals p.Id
                                where s.UserId == user.Id && p.IsEnabled == true
                                orderby p.SubsiName
                                select p.Id).ToList();
                return (from transBatch in context.Trn_SSCTransBatch
                        join transLine in context.Trn_SSCTransLineNew
                            on transBatch.Id equals transLine.BatchId
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsiIds.Contains(transBatch.SubsyId)            
                        select new ReportSSCTransactionModel
                        {
                            // FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerCode = transLine.CustomerCode,
                            ReferenceNumber = transLine.CustomerRefNumber,
                            packerLocation = transLine.FromLocation,
                            consigneeLocation = transLine.ToLocation,
                            BinType = transLine.BinType,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            Qty = SqlFunctions.StringConvert((decimal)transLine.Quantity).Trim(),
                            ETA = transLine.ETA,
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode,
                            ETD = transLine.ETD
                        }).AsQueryable();
            }
            else
            {
                return (from transBatch in context.Trn_SSCTransBatch
                        join transLine in context.Trn_SSCTransLineNew
                            on transBatch.Id equals transLine.BatchId
                        join subsi in context.Gen_EDISubsies
                            on transBatch.SubsyId equals subsi.Id
                        where transBatch.Ref_TransactionTypes.Id == trnasactionType && subsi.IsEnabled == true  
                        select new ReportSSCTransactionModel
                        {
                            // FileName = transBatch.FileName,
                            UploadedDate = transBatch.DateUpdated,
                            Batchid = transLine.BatchId,
                            CustomerCode = transLine.CustomerCode,
                            ReferenceNumber = transLine.CustomerRefNumber,
                            packerLocation = transLine.FromLocation,
                            consigneeLocation = transLine.ToLocation,
                            BinType = transLine.BinType,
                            Mode = transLine.InTestMode ? "Verified" : "Processed",
                            Qty = SqlFunctions.StringConvert((decimal)transLine.Quantity).Trim(),
                            ETA = transLine.ETA,
                            UniqueId = transLine.Id,
                            Status = transLine.Trn_LineStatus.StatusCode,
                            ETD = transLine.ETD
                        }).AsQueryable();
            }
        }      
    }
}