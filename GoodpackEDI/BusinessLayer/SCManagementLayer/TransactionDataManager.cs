﻿using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using GoodpackEDI.BusinessLayer.DecodeValueManagementLayer;
using GoodpackEDI.BusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement;
using GoodpackEDI.Parsers;
using GPArchitecture.DataLayer.Models;
using log4net;
using GoodpackEDI.BusinessLayer.SSCManagementLayer;
using GoodpackEDI.BusinessLayer.SOManagementLayer;
using GoodpackEDI.BusinessLayer.GenericValidationManagementLayer;
using System.Net.Mail;
using System.Text.RegularExpressions;
using GoodpackEDI.BusinessLayer.TermTripManagementLayer;
using System.Web;


namespace GoodpackEDI.BusinessLayer.SCManagementLayer
{
    public class TransactionDataManager : ITransactionDataManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(TransactionDataManager));
        public string scPath { get; set; }
        private  SCTransactionModel objGPCustomerMessage;
        private Dictionary<int, Dictionary<string, string>> sapSubmitTransData;
       // private Dictionary<int, Dictionary<string, string>> sapSubmitTransBatchData;
        SCTransactionBatch scTransBatch = new SCTransactionBatch();
        private SCTransactionBatch transBatch = new SCTransactionBatch();
        private SCTransactionModel transactionModel = new SCTransactionModel();
        private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
        private StringBuilder errorDescription = new StringBuilder();
        public string FileName { get; set; }
        int trLineID = 0;
        private List<Dictionary<string, string>> sscData = new List<Dictionary<string, string>>();
        public bool fileUpload(IList<MultipartFileData> filedata, string root)
        {
            try
            {
                foreach (var file in filedata)
                {
                    string fileName = (file.Headers.ContentDisposition.FileName).Replace("\"", string.Empty);
                    string filePath = Path.Combine(root, fileName);                   
                    File.Delete(filePath);
                    File.Copy(file.LocalFileName, filePath, false);
                    string extension = Path.GetExtension(filePath);
                    string oldFileName = Path.GetFileNameWithoutExtension(filePath);
                    scPath = Path.Combine(root, oldFileName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture) + extension);
                    File.Move(filePath, scPath);
                    File.Delete(filePath);
                    File.Delete(file.LocalFileName);
                }
                return true;
            }
            catch (Exception e)
            {
                log.Error(" fileUpload in TransactionDataManager .FileData:" + filedata + ", File Root:" + root + ".  Exception : Message- " + e.Message);
                return false;
            }

        }
        public bool fileUpload(IList<MultipartFileData> filedata, string root,out string path)
        {
            path = "";
            try
            {
                foreach (var file in filedata)
                {
                    string fileName = (file.Headers.ContentDisposition.FileName).Replace("\"", string.Empty);
                    string filePath = Path.Combine(root, fileName);
                    File.Delete(filePath);
                    File.Copy(file.LocalFileName, filePath, false);
                    string extension = Path.GetExtension(filePath);
                    string oldFileName = Path.GetFileNameWithoutExtension(filePath);
                    scPath = Path.Combine(root, oldFileName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture) + extension);
                    path = scPath;
                    File.Move(filePath, scPath);
                    File.Delete(filePath);
                    File.Delete(file.LocalFileName);
                }
                return true;
            }
            catch (Exception e)
            {
                log.Error(" fileUpload in TransactionDataManager .FileData:" + filedata + ", File Root:" + root + ".  Exception : Message- " + e.Message);
                return false;
            }

        }
        public string ErrorLinesDownload(int batchId,bool isDownloadError)
        {
            try
            {
             using (var context = new GoodpackEDIEntities())
             {
                 int trnsType = (from s in context.Trn_SCTransBatch
                                 where s.Id == batchId
                                 select s.TransactionId).FirstOrDefault();
                 string trnName = (from s in context.Ref_TransactionTypes
                                   where s.Id == trnsType
                                   select s.TransactionCode).FirstOrDefault();

                 TRANSACTION_TYPES inputTransType =
                                 (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), trnName);
                if (isDownloadError)
                {
                     string responseFile = "errorFile";
                   
                        
                        switch (inputTransType)
                        {
                            case TRANSACTION_TYPES.SO: responseFile = getErrorSOFile(batchId);
                                break;
                            case TRANSACTION_TYPES.SC: responseFile = getErrorSCFile(batchId);
                                break;
                            case TRANSACTION_TYPES.SSC: responseFile = getErrorSSCFile(batchId);
                                break;
                            case TRANSACTION_TYPES.ASN: responseFile = getErrorASNFile(batchId);
                                break;
                        }

                        return responseFile;
                    }
                else
                {
                    string responseFile = "transactionFile";
                        switch (inputTransType)
                        {
                            case TRANSACTION_TYPES.SO: responseFile = getResponseSO(batchId);
                                break;
                            case TRANSACTION_TYPES.SC: responseFile = getResponseSC(batchId);
                                break;
                            case TRANSACTION_TYPES.ASN: responseFile = getResponseASN(batchId);
                                break;
                            case TRANSACTION_TYPES.SSC: responseFile = getResponseSSC(batchId);
                                break;
                        }

                        return responseFile;
                    
                }
            }

         }
            catch (Exception e)
            {
              //  log.Error(" fileUpload in TransactionDataManager .FileData:" + filedata + ", File Root:" + root + ".  Exception : Message- " + e.Message);
                return "no file";
            }

        }

        /// <summary>
        /// For Count File Lines
        /// </summary>

        public bool CountFileLines()
        {
            try
            {
                int counter = 0;
                string line;
                StreamReader filep = new StreamReader(scPath);
                while ((line = filep.ReadLine()) != null && (line = filep.ReadLine()) != "")
                {
                    counter++;
                }
                int lines = counter;
                if (lines > 51)
                {
                    return false;  
                }
                return true;
            }
            catch (Exception e)
            {
                log.Error(" CountFileLines in TransactionDataManager.  Exception : Message- " + e.Message);
                return false;  
            }

        }
        /// <summary>
        /// For EMail processing
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        public void FileInfo(string path, string fileName, string message, string emailId)
        {

            objGPCustomerMessage = new SCTransactionModel();
            objGPCustomerMessage.BatchFileName = fileName;
            objGPCustomerMessage.BatchFileSourceAddress = path;
            objGPCustomerMessage.Message = message;
            objGPCustomerMessage.EmailId = emailId;
        }

        public IList<string> RetrieveSOErrorData(int lineId)
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Trn_SOTransLineContent
                        where s.LineID == lineId
                        select s.SapResponseMessage).ToList();
            }
        }

        public IList<string> RetrieveLineContentDetails(int lineId, TRANSACTION_TYPES transactiontype)
        {
            using (var context = new GoodpackEDIEntities())
            {                
                var returnList=new List<string>();
                var tempList=new List<string>();
                switch (transactiontype)
                {
                    case TRANSACTION_TYPES.SC:
                        returnList.Add(string.Join("|", (from y in context.Trn_SubsiToTransactionFileMapper
                                                            join p in context.Trn_MapperFieldSpecs on y.Id equals p.SubsiToTransactionMapperId
                                                       where (y.Filename == (from s in context.Trn_SCTransLine
                                                                             join e in context.Trn_SCTransBatch
                                                                                  on s.BatchId equals e.Id
                                                                             where s.Id == lineId
                                                                             select e.MapperFileName).FirstOrDefault())
                                                            select p.FieldName).ToList().ToArray()));

                        returnList.AddRange((from s in context.Trn_SCTransLine
                                             where s.Id == lineId
                                             select s.LineContent).ToList());

                        return returnList;                        
                    case TRANSACTION_TYPES.SSC:
                        returnList.Add(string.Join("|", (from y in context.Trn_SubsiToTransactionFileMapper
                                                   join p in context.Trn_MapperFieldSpecs on y.Id equals p.SubsiToTransactionMapperId
                                                   where (y.Filename == (from s in context.Trn_SSCTransLine
                                                                         join e in context.Trn_SCTransBatch
                                                                              on s.BatchId equals e.Id
                                                                         where s.Id == lineId
                                                                         select e.MapperFileName).FirstOrDefault())
                                                   select p.FieldName).ToList().ToArray())); 
                        returnList.AddRange((from s in context.Trn_SSCTransLine
                                            where s.Id == lineId
                                            select s.LineContent).ToList());
                        return returnList; 
                    case TRANSACTION_TYPES.SO:
                        returnList.Add(string.Join("|", (from y in context.Trn_SubsiToTransactionFileMapper
                                                   join p in context.Trn_MapperFieldSpecs on y.Id equals p.SubsiToTransactionMapperId
                                                   where (y.Filename == (from s in context.Trn_SOTransLine
                                                                         join e in context.Trn_SCTransBatch
                                                                              on s.BatchId equals e.Id
                                                                         where s.Id == lineId
                                                                         select e.MapperFileName).FirstOrDefault())
                                                   select p.FieldName).ToList().ToArray())); 
                        returnList.AddRange( (from s in context.Trn_SOTransLine
                                where s.Id == lineId
                                select s.LineContent).ToList());
                        return returnList; 
                    case TRANSACTION_TYPES.ASN:
                        returnList.Add(string.Join("|", (from y in context.Trn_SubsiToTransactionFileMapper
                                                   join p in context.Trn_MapperFieldSpecs on y.Id equals p.SubsiToTransactionMapperId
                                                   where (y.Filename == (from s in context.Trn_ASNTransLine
                                                                         join e in context.Trn_SCTransBatch
                                                                              on s.BatchId equals e.Id
                                                                         where s.Id == lineId
                                                                         select e.MapperFileName).FirstOrDefault())
                                                   select p.FieldName).ToList().ToArray()));
                        returnList.AddRange((from s in context.Trn_ASNTransLine
                                                where s.Id == lineId
                                            select s.LineContent).ToList());
                        return returnList; 
                    default: return new List<string>();
                }
            }
        }


        public IList<string> mappedFiles(int subsiId, int transactionType)
        {
            string transType = null;
            using (var context = new GoodpackEDIEntities())
            {
                switch (transactionType)
                {
                    case 0: transType = TRANSACTION_TYPES.SO.ToString();
                        break;
                    case 1: transType = TRANSACTION_TYPES.SC.ToString();
                        break;
                    case 2: transType = TRANSACTION_TYPES.SSC.ToString();
                        break;
                    case 3: transType = TRANSACTION_TYPES.ASN.ToString();
                        break;
                }

                if (transType != null)
                {
                    int transactionId = (from s in context.Ref_TransactionTypes
                                         where s.TransactionCode == transType
                                         select s.Id).FirstOrDefault();

                    return (from s in context.Trn_SubsiToTransactionFileMapper
                            orderby s.Filename
                            where (s.EDISubsiId == subsiId) && (s.IsEnabled == true) && (s.TransactionCodeId == transactionId)
                            select s.Filename).ToList();
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        public IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData)
        {
            try
            {
                log.Info("VerifyData function start");
                GenericItemModel itemModelVal = itemModel[1];
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();               
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (isFileData)
                {
                    
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;

                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);

               // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                List<string> ediNumber = new List<string>();

                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                       && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                    itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                        string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                        ediNumber.Add(objLineReferences.EdiNumber);
                        int transID = 0;
                        //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: transID = scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                break;
                            case TRANSACTION_TYPES.SSC: transID = scTransBatch.InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
                                break;
                        }
                        trnsLineItems.Add(transID);
                        i++;
                    }
                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");


                // Archive message
                //no need to archive for bin pairing transaction
                //if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                //{
                //    try
                //    {
                //        string archiveFilename = (Guid.NewGuid().ToString()) + ".txt";
                //        string archivePath = String.Format("{0}\\{1}", Config.getConfig("ArchivePath"), ArchiveSources.REQ_OUT.ToString());
                //        GPTools.Archive(mappingOutput.OutputMessage.ToString(), ArchiveSources.REQ_OUT, objGPCustomerMessage.BatchID.ToString(), archivePath);

                //        //To copy the FC translated file to some FC archive folder
                //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
                //        {
                //            string fcArchiveFilename = System.IO.Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName);
                //            GPTools.ArchiveFC(mappingOutput.OutputMessage.ToString(), Config.getConfig("FCArchivePath"), fcArchiveFilename);
                //        }

                //    }
                //    catch (System.Exception ex)
                //    {
                //        objILog.Error("Error while archiving received file.", ex);
                //        GPTools.ProcessErrorAction(SAPClientService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_ARCHIVE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), ex.Message);
                //    }
                //}

                // Send to SAP
                //No need to send to SAP for Bin Pairing transaction
                //if (SendToSAP(objGPCustomerMessage, mappingOutput.OutputMessage))
                //{
                //objDataAccess = new BatchProcessorDAFactory(DBConnection.SqlConnString);
                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                        case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                    }

                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        trLineID = trnsLineItems[i];
                        log.Info("Updating TransLine table with excel data");
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                            case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                        }

                        try
                        {
                            List<ResponseModel> responseModel = null;
                            SCServiceManager serviceCaller = new SCServiceManager();
                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SC webservice");
                            if (isFileData)
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                        break;
                                    //case TRANSACTION_TYPES.SSC:
                                    //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
                                    //    break;
                                }
                            }
                            else
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                        break;
                                    //    case TRANSACTION_TYPES.SSC:
                                    //responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId, ediNumber[i]);
                                    //break;
                                }
                            }


                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                            bool isSuccess = false;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    isSuccess = scTransBatch.InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;

                                case TRANSACTION_TYPES.SSC:
                                    isSuccess = scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;
                            }
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                }
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());

                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                }
                            }
                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                            
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            // model.Message = "Exception Message :" + e.Message;    
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            // GenericItemModel itemsModel = new GenericItemModel();
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    SCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC:
                                    SSCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }

                            serviceResponse.Add(trLineID, responseModel);
                        }
                    }
                }
               
              //  int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                //below code commented by abina
                //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = scTransBatch.ScTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC:
                            genericModel = scTransBatch.ScTestResult(itemModel, trnsLineItems);
                            break;
                        // case TRANSACTION_TYPES.SSC:
                        //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
                        //break;
                    }
                }
               if(genericModel.Count>0)
               {
                   genericModel[0].sapResponseData = sapSubmitTransData;
               }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address
                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSC;
                    strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSC += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSC);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSC);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSC;
                        strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                        //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                        Attachment attachment = new Attachment(strGoodyearSC);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                    }
                    else 
                    {
                        //fileupload SO
                        //send email to users
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }
            //log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
            //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
            //log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
            //objILog.Error("Error while processing in ProcessBatch", exp);

        }
        public IList<GenericItemModel> verifyData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData,bool isWeb)
        {
            try
            {
                log.Info("Entering verifyData in SCManager");
                GenericItemModel itemModelVal = itemModel[1];
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (isFileData)
                {

                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;

                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);

                // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                log.Info("VerifyData ProcessMap start");
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                log.Info("VerifyData ProcessMap End");

                List<string> ediNumber = new List<string>();
                ITirmTripManager termTrip = new TirmTripManager();
                if (!termTrip.CheckValidCustomer(objGPCustomerMessage.mappingServiceValues))
                {
                    IList<GenericItemModel> genericModelReturn = new List<GenericItemModel>();
                    genericModelReturn = itemModel;
                    genericModelReturn[0].ErrorMessage = "Invalid Customer present";
                    genericModelReturn[0].Message = "Invalid Customer present";
                    genericModelReturn[0].Status = "false";
                    return genericModelReturn;
                }
                if (termTrip.CheckMultipleTripExist(objGPCustomerMessage.mappingServiceValues))
                {
                    IList<GenericItemModel> genericModelReturn = new List<GenericItemModel>();
                    genericModelReturn = itemModel;
                    genericModelReturn[0].ErrorMessage = "Cannot process Trip & Tirm Trip in a single batch";
                    genericModelReturn[0].Message = "Cannot process Trip & Tirm Trip in a single batch";
                    genericModelReturn[0].Status="false";
                    return genericModelReturn;
                }
                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                       && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                    itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                        string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                        ediNumber.Add(objLineReferences.EdiNumber);
                        int transID = 0;
                        //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: transID = scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                break;
                            case TRANSACTION_TYPES.SSC: transID = scTransBatch.InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
                                break;
                        }
                        trnsLineItems.Add(transID);
                        i++;
                    }
                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");


                // Archive message
                //no need to archive for bin pairing transaction
                //if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                //{
                //    try
                //    {
                //        string archiveFilename = (Guid.NewGuid().ToString()) + ".txt";
                //        string archivePath = String.Format("{0}\\{1}", Config.getConfig("ArchivePath"), ArchiveSources.REQ_OUT.ToString());
                //        GPTools.Archive(mappingOutput.OutputMessage.ToString(), ArchiveSources.REQ_OUT, objGPCustomerMessage.BatchID.ToString(), archivePath);

                //        //To copy the FC translated file to some FC archive folder
                //        if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC)
                //        {
                //            string fcArchiveFilename = System.IO.Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName);
                //            GPTools.ArchiveFC(mappingOutput.OutputMessage.ToString(), Config.getConfig("FCArchivePath"), fcArchiveFilename);
                //        }

                //    }
                //    catch (System.Exception ex)
                //    {
                //        objILog.Error("Error while archiving received file.", ex);
                //        GPTools.ProcessErrorAction(SAPClientService.Service, objGPCustomerMessage.BatchID, ErrorCodes.E_ARCHIVE, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerName, objGPCustomerMessage.CustomerCode, objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.TransactionType, objGPCustomerMessage.BatchFileName, objGPCustomerMessage.DateCreated.ToShortDateString(), ex.Message);
                //    }
                //}

                // Send to SAP
                //No need to send to SAP for Bin Pairing transaction
                //if (SendToSAP(objGPCustomerMessage, mappingOutput.OutputMessage))
                //{
                //objDataAccess = new BatchProcessorDAFactory(DBConnection.SqlConnString);
                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                        case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                    }

                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    log.Info("VerifyData data looping Start");
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        trLineID = trnsLineItems[i];
                        log.Info("Updating TransLine table with excel data");
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                            case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                        }

                        try
                        {
                            List<ResponseModel> responseModel = null;
                            SCServiceManager serviceCaller = new SCServiceManager();
                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SC webservice");
                            if (isFileData)
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = new List<ResponseModel>();
                                        //responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                       var returnMessage= CheckValidDecode(objGPCustomerMessage.mappingServiceValues[i]);
                                        ResponseModel response = new ResponseModel();
                                        if (string.IsNullOrEmpty(returnMessage))
                                        {
                                            response.Success = true;
                                            successCount++;
                                            responseModel.Add(response);
                                        }else
                                        {
                                            response.Success = false;
                                            response.Message = returnMessage;
                                            errorCount++;
                                            responseModel.Add(response);
                                        }
                                        break;
                                    //case TRANSACTION_TYPES.SSC:
                                    //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
                                    //    break;
                                }
                            }
                            else
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                        break;
                                    //    case TRANSACTION_TYPES.SSC:
                                    //responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId, ediNumber[i]);
                                    //break;
                                }
                            }


                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                            bool isSuccess = false;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    isSuccess = scTransBatch.InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;

                                case TRANSACTION_TYPES.SSC:
                                    isSuccess = scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;
                            }
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                }
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());

                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                }
                            }
                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);                   
                            switch (objGPCustomerMessage.TransactionType)
                            {

                                case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            // model.Message = "Exception Message :" + e.Message;    
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            // GenericItemModel itemsModel = new GenericItemModel();
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    SCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC:
                                    SSCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }

                            serviceResponse.Add(trLineID, responseModel);
                        }
                    }
                    log.Info("VerifyData data looping End");
                }

                //  int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                //below code commented by abina
                //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = scTransBatch.ScTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC:
                            genericModel = scTransBatch.ScTestResult(itemModel, trnsLineItems);
                            break;
                        // case TRANSACTION_TYPES.SSC:
                        //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
                        //break;
                    }
                }
                if (genericModel.Count > 0)
                {
                    genericModel[0].sapResponseData = sapSubmitTransData;
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address
                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSC;
                    strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSC += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSC);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSC);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSC;
                        strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                        if (isWeb)
                            strGoodyearSC = strGoodyearSC + "\\" + objGPCustomerMessage.BatchFileName;
                        //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                        Attachment attachment = new Attachment(strGoodyearSC);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                    }
                    else
                    {
                        //fileupload SO
                        //send email to users
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                if (exp.Message == "Quantity should be integer")
                    itemsModel.Message = "Unable to process your request."+exp.Message;
                else
                    itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }
            //log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
            //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
            //log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
            //objILog.Error("Error while processing in ProcessBatch", exp);

        }

        public IList<GenericItemModel> verifyDataNew(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData, bool isWeb)
        {
            try
            {
                log.Info("Entering verifyData in SCManager");
                GenericItemModel itemModelVal = itemModel[1];
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;

                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
               
                List<int> trnsLineItems = new List<int>();                
                GPMapper mapper = new GPMapper();
                log.Info("VerifyData ProcessMap start");
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                log.Info("VerifyData ProcessMap End");

                List<string> ediNumber = new List<string>();
                ITirmTripManager termTrip = new TirmTripManager();
                if (!termTrip.CheckValidCustomer(objGPCustomerMessage.mappingServiceValues))
                {
                    IList<GenericItemModel> genericModelReturn = new List<GenericItemModel>();
                    genericModelReturn = itemModel;
                    genericModelReturn[0].ErrorMessage = "Invalid Customer present";
                    genericModelReturn[0].Message = "Invalid Customer present";
                    genericModelReturn[0].Status = "false";
                    return genericModelReturn;
                }
                if (termTrip.CheckMultipleTripExist(objGPCustomerMessage.mappingServiceValues))
                {
                    IList<GenericItemModel> genericModelReturn = new List<GenericItemModel>();
                    genericModelReturn = itemModel;
                    genericModelReturn[0].ErrorMessage = "Cannot process Trip & Tirm Trip in a single batch";
                    genericModelReturn[0].Message = "Cannot process Trip & Tirm Trip in a single batch";
                    genericModelReturn[0].Status = "false";
                    return genericModelReturn;
                }
                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                       && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                    itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();                                    

                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
               
                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC: trnsLineItems = scTransBatch.InsertTransLineBulk(objGPCustomerMessage, mappingOutput.LineReferenceItems, null, objGPCustomerMessage.mappingServiceValues, objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                        //scTransBatch.UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                        case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                    }

                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    log.Info("VerifyData data looping Start");
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        trLineID = trnsLineItems[i];

                        try
                        {
                            List<ResponseModel> responseModel = null;
                            SCServiceManager serviceCaller = new SCServiceManager();
                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SC webservice");
                            if (isFileData)
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = new List<ResponseModel>();
                                        //responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                        var returnMessage = CheckValidDecode(objGPCustomerMessage.mappingServiceValues[i]);
                                        ResponseModel response = new ResponseModel();
                                        if (string.IsNullOrEmpty(returnMessage))
                                        {
                                            response.Success = true;
                                            successCount++;
                                            responseModel.Add(response);
                                        }
                                        else
                                        {
                                            response.Success = false;
                                            response.Message = returnMessage;
                                            errorCount++;
                                            responseModel.Add(response);
                                        }
                                        break;
       
                                }
                            }
                            else
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                        break;        
                                }
                            }
                            serviceResponse.Add(trLineID, responseModel);
                            log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                            bool isSuccess = false;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                   // isSuccess = scTransBatch.InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    scTransBatch.InsertTransLineContentAndUpdateResponseTransLineAndTransBatch(trLineID, objGPCustomerMessage.BatchID, responseModel, isFileData, successCount, errorCount);
                                    break;

                                case TRANSACTION_TYPES.SSC:
                                    isSuccess = scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;
                            }
                            log.Info("Insertion of trans line content success status:" + isSuccess);                           
                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);            
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;   
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    SCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC:
                                    SSCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }

                            serviceResponse.Add(trLineID, responseModel);
                        }
                    }
                    log.Info("VerifyData data looping End");
                }
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = scTransBatch.ScTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC:
                            genericModel = scTransBatch.ScTestResultNew(itemModel, trnsLineItems);
                            break;
                    }
                }
                if (genericModel.Count > 0)
                {
                    genericModel[0].sapResponseData = sapSubmitTransData;
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address
                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSC;
                    strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSC += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSC);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSC);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSC;
                        strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                        if (isWeb)
                            strGoodyearSC = strGoodyearSC + "\\" + objGPCustomerMessage.BatchFileName;
                        //strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                        Attachment attachment = new Attachment(strGoodyearSC);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                    }
                    else
                    {
                        //fileupload SO
                        //send email to users
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                if (exp.Message == "Quantity should be integer")
                    itemsModel.Message = "Unable to process your request." + exp.Message;
                else
                    itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;
            }
        }

        public IList<GenericItemModel> verifySSCData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData)
         {
            try
            {
                GenericItemModel itemModelVal = itemModel[1];
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
               // List<string> batchId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
               // batchId = itemModel.Select(x => x.BatchId).Distinct().ToList();                
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
               // objGPCustomerMessage.BatchID = batchId[0] == null ? 0 : int.Parse(batchId[0].ToString());
                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;

                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());

                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);

               // bool processOneLineOnly = false;

                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                List<string> ediNumber = new List<string>();

                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                       && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                    itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                        string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                        objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.EdiNumber] = objLineReferences.EdiNumber;
                        //int transID = 0;
                        //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);

                        objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.LineId] = scTransBatch.InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber).ToString();

                        i++;
                    }
                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);

                    scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);


                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;

                    List<Dictionary<string, string>> data = new List<Dictionary<string, string>>();
                    //Cloning in of data to another list for iterating
                    objGPCustomerMessage.mappingServiceValues.ForEach((item) =>
                    {
                        data.Add(new Dictionary<string, string>(item));
                    });
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        sapSubmitTransData.Add(Convert.ToInt32(objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.LineId]), objGPCustomerMessage.mappingServiceValues[i]);
                    }
                    while (data.Count != 0)
                    {

                        string customerRefNumber = data[0][SCSapMessageFields.CustomerReferenceNumber];
                        sscData = data.FindAll(i => i[SCSapMessageFields.CustomerReferenceNumber] == customerRefNumber);
                        for (int i = 0; i < sscData.Count; i++)
                        {
                            trLineID = Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                            log.Info("Updating TransLine table with excel data");
                            scTransBatch.UpdateSSCTransLineForData(trLineID, sscData[i]);
                        }

                        try
                        {
                            List<ResponseModel> responseModel = null;

                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SSC webservice");
                            if (isFileData)
                            {
                                responseModel = sscCaller.sscServiceCaller(sscData, ServiceModeTypes.TEST, ref successCount, ref errorCount, username);

                            }
                            else
                            {
                                responseModel = sscCaller.sscServiceCaller(sscData, null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId);
                            }

                            for (int i = 0; i < sscData.Count; i++)
                            {
                                serviceResponse.Add(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), responseModel);
                                bool isSuccess = false;
                                isSuccess = scTransBatch.InsertSSCTransLineContent(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), objGPCustomerMessage.BatchID, responseModel);
                                log.Info("Insertion of trans line content success status:" + isSuccess);
                                if (isSuccess)
                                {
                                    log.Info("Line status of trnLineId:" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]) + " is :" + LineStatus.SUCCESS.ToString());

                                    scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), isFileData);
                                }
                                else
                                {
                                    log.Info("Line status of trnLineId:" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]) + " is :" + LineStatus.ERROR.ToString());
                                    scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), isFileData);
                                }
                              //  sapSubmitTransData.Add(Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]), objGPCustomerMessage.mappingServiceValues[i]);

                                scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                log.Info("Inserting into TransLineContent table of transLineId:-" + Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]));
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            // model.Message = "Exception Message :" + e.Message;    
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            // GenericItemModel itemsModel = new GenericItemModel();
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            for (int i = 0; i < sscData.Count; i++)
                            {
                                trLineID =Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                                SSCTransactionData(responseModel, isFileData);
                                serviceResponse.Add(trLineID, responseModel);
                            }
                            scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);                           
                        }
                        data.RemoveAll(i => i[SCSapMessageFields.CustomerReferenceNumber] == customerRefNumber);
                    }
                }
                int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                genericModel = scTransBatch.SScTestResult(itemModel, objGPCustomerMessage.mappingServiceValues);

                if (genericModel.Count > 0)
                {
                    genericModel[0].sapResponseData = sapSubmitTransData;
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                string[] recipientList = new string[] { userDetails.EmailId };
                // GPTools.SendEmail(placeholders, recipientList, "NOTIFY_CUSTOMER", null);
                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER", null);
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }
            //log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
            //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
            //log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
            //objILog.Error("Error while processing in ProcessBatch", exp);

        }
        public IList<GenericItemModel> VerifySOChangeCancelData(string username, int subsiId, IList<GenericItemModel> itemModel)
        {
            try
            {
                var filePathLocal = "";              
                objGPCustomerMessage = new SCTransactionModel();
                GenericItemModel itemModelVal = itemModel[1];
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.Message = dataValue[0].ToString();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();            
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                itemModel.RemoveAt(1);
                using (var context = new GoodpackEDIEntities())
                {
                   objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();             
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;

                List<int> trnsLineItems = new List<int>();
                string mapperFile = GPArchitecture.Cache.Config.getConfig("ChangeCancelSOMapper");
                SalesOrderUploadChangeCancelFields soChangeClass = new SalesOrderUploadChangeCancelFields();
                IList<ChangeCancelSOFieldsModel> mappedData = soChangeClass.ChangeCacelSOData();
            

                filePathLocal = filePath[0].ToString() + "//" + fileName[0].ToString();
                string messageVal = ExcelParser.ExcelToCSV(filePathLocal, 0, 0);//InteropExcelParser.ExcelParser(filePath);
                messageVal = messageVal.Replace("\"", "");
                string[] messageData = messageVal.Split(new string[] { "\r\n" }, StringSplitOptions.None);

                objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                objGPCustomerMessage.BatchID = scTransBatch.AddSOBatchFileData(objGPCustomerMessage, mapperFile);

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());

                objGPCustomerMessage.RecordCountTotal = messageData.Count() - 2;

                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");

                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
               
                   
                    ISalesOrderManager soserviceManager = new SalesOrderManager();
                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();               
                    Dictionary<string, string> serviceValues;
                    objGPCustomerMessage.mappingServiceValues = new List<Dictionary<string, string>>();
                    serviceValues = new Dictionary<string, string>();
                    //messageVal = messageVal.Replace("\"", "");
                    //messageData = messageVal.Split(new string[] { "\r\n" }, StringSplitOptions.None);                 

                    ITransTypeDecodeValueManager decode = new SCDecodeValueManager();
                    for (int j = 1; j < messageData.Length-1; j++)
                    {
                        string[] filedata = messageData[j].Split('|');
                        
                        Dictionary<string, string> values = new Dictionary<string, string>();    
                        string customerValue=decode.FindDecodeValue(filedata[0],mapperFile,subsiId,GetLookupId(ConstantUtilities.CustomerCode));
                        if (customerValue==string.Empty)
                        {
                          values.Add("Customer",filedata[0]);
                        }                     
                        else{
                              values.Add("Customer",customerValue);
                        }                     
                        values.Add("CustomerPONumber", filedata[1]);
                        values.Add("Requestdeliverydate", filedata[2]);
                        values.Add("Material", filedata[3]);
                        string packerValue=decode.FindDecodeValue(filedata[4],mapperFile,subsiId,GetLookupId(ConstantUtilities.Packer));
                        if (packerValue==string.Empty)
                        {
                          values.Add("Packer",filedata[4]);
                        }                     
                        else{
                              values.Add("Packer",packerValue);
                        }                    
                        values.Add("OrderQuantity", filedata[5]);
                        values.Add("ReasonforETDchange", getReasoncodes(filedata[6].ToString()));
                        values.Add("ReasonforSKUchange", getReasoncodes(filedata[7].ToString()));
                        values.Add("ReasonforQuantityChange", getReasoncodes(filedata[8].ToString()));
                        values.Add("ReasonforCancellation", getReasoncodes(filedata[9].ToString()));
                        //bool isvalidSubsy = true;
                        //isvalidSubsy = subsyCheckForValid(int.Parse(filedata[0]),int.Parse(filedata[4]),subsiId);
                        objGPCustomerMessage.mappingServiceValues.Add(values);
                    }

                  
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        int successCount = 0;
                        int errorCount = 0;
                        int transLineID=0;
                        log.Info("Inserting and Updating SO TransLine table with excel data");
                        transLineID= scTransBatch.UpdateSOTransLineForSOChangeCancelData(objGPCustomerMessage.mappingServiceValues[i],objGPCustomerMessage,messageData);
                        trnsLineItems.Add(transLineID);
                        try
                        {

                            List<ResponseModel> responseModel;
                            log.Info("Calling SO webservice");

                            responseModel = soserviceManager.ChangeorCancelSOserviceCaller(objGPCustomerMessage.mappingServiceValues[i], ref successCount, ref errorCount, username, subsiId);

                            serviceResponse.Add(transLineID, responseModel);

                            log.Info("Inserting into SOTransLineContent table of transLineId:-" + transLineID);

                            bool isSuccess = scTransBatch.InsertSOTransLineContent(transLineID, objGPCustomerMessage.BatchID, responseModel);
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, transLineID, true);
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, transLineID, true);
                            }

                            
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                        }
                        catch (Exception e)
                        {

                            log.Error(" fileUpload in TransactionDataManager VerifySOChangeCancelData method, username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + " Api Exception : Message" + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper ";
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            scTransBatch.InsertSOTransLineContent(transLineID, objGPCustomerMessage.BatchID, responseModel);
                            scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                            serviceResponse.Add(transLineID, responseModel);
                        }
                    }

                //int iCountSentToSAP = 0;
                //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
               
                IList<GenericItemModel> genericModel = null;
               
                genericModel = scTransBatch.SoTestResult(itemModel, trnsLineItems);

              
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                              
                   
           //fileupload SO
          //send email to users
                       
            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);                      
             GPTools.SendEmail(placeholders, getAllRecipients(subsiId), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
                   
                
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from VerifySOChangeCancelData in TransactionDataManager");
                return itemModelData;

            }
            catch (Exception exp)
            {

                log.Error(" fileUpload in TransactionDataManager for SO Transaction VerifySOChangeCancelData method, username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(subsiId), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }

        }
        

        public string getReasoncodes(string reason)
        {
            using (var dataContext = new GoodpackEDIEntities())
            {
                string reasonCode;
                reasonCode = (from s in dataContext.Ref_SOEditReasonCodes
                               where s.Description== reason
                               select s.ReasonCode).FirstOrDefault();
                return reasonCode;
            }

        }

        public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId,string custPOdate, IList<GenericItemModel> itemModel, bool isFileData)
        {
            try
            {
                objGPCustomerMessage = new SCTransactionModel();
                GenericItemModel itemModelVal = itemModel[1];
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.Message = dataValue[0].ToString();
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }

                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
               // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSOBatchFileData(objGPCustomerMessage, mapperFile);
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        //string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];

                        int transID = scTransBatch.InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);

                        trnsLineItems.Add(transID);
                        i++;
                    }
                }


                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");




                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    scTransBatch.UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                    SOServiceManager serviceCaller = new SOServiceManager();
                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        int trLineID = trnsLineItems[i];
                        log.Info("Updating SO TransLine table with excel data");
                        scTransBatch.UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i],custPOdate);

                        try
                        {

                            List<ResponseModel> responseModel;
                            log.Info("Calling SO webservice");
                            if (isFileData)
                            {
                                responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId,custPOdate);
                            }
                            else
                            {
                                custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                                responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId,custPOdate);
                            }
                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

                            bool isSuccess = scTransBatch.InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
                            }

                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                        }
                        catch (Exception e)
                        {

                            log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;

                            scTransBatch.InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);

                            serviceResponse.Add(trLineID, responseModel);

                        }

                    }
                }


                int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = scTransBatch.SoTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    genericModel = scTransBatch.SoTestResult(itemModel, trnsLineItems);

                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
                placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
                placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address
                    
                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSO;
                    strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSO);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2= new Attachment(strFileSO);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSO;
                        strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
                       // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;

                        Attachment attachment = new Attachment(strGoodyearSO);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
                    }
                    else 
                    {
                        //fileupload SO
                        //send email to users
                      
                       // List<Attachment> attachments = new List<Attachment>();
                       // string responseFile;
                       // responseFile = objGPCustomerMessage.BatchFileSourceAddress;
                       //// responseFile += "\\" + objGPCustomerMessage.BatchFileName;
                       // responseFile += "\\" + "ResponseFileGenerated" + objGPCustomerMessage.BatchID;
                       // if (!File.Exists(responseFile))
                       // {
                       //     File.Create(responseFile).Close();
                       // }
                       // responseFile = getResponseSO(responseFile, objGPCustomerMessage.BatchID);                      
                       // Attachment attachment = new Attachment(responseFile);
                       // attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                       // attachments.Add(attachment);
                       log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                       // GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", attachments);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;

            }
            catch (Exception exp)
            {

                log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }

        }
        public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, bool isFileData,bool isWeb)
        {
            try
            {
                objGPCustomerMessage = new SCTransactionModel();
                GenericItemModel itemModelVal = itemModel[1];
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
                List<string> emailId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
                emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
                objGPCustomerMessage.Message = HttpRuntime.Cache[dataValue[1].ToString()].ToString();//dataValue[0].ToString()                
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
                objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                HttpRuntime.Cache.Remove(dataValue[1].ToString());
                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }

                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
                objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
                // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSOBatchFileData(objGPCustomerMessage, mapperFile);
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        //string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];

                        int transID = scTransBatch.InsertSOTransLine(objGPCustomerMessage, objLineReferences, null);

                        trnsLineItems.Add(transID);
                        i++;
                    }
                }


                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated  transbatch table for SO Transaction with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");




                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating SOtransLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    scTransBatch.UpdateSOTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                    SOServiceManager serviceCaller = new SOServiceManager();
                    SalesOrderManager soserviceManager = new SalesOrderManager();
                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        int trLineID = trnsLineItems[i];
                        log.Info("Updating SO TransLine table with excel data");
                        scTransBatch.UpdateSOTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i], custPOdate);

                        try
                        {

                            List<ResponseModel> responseModel;
                            log.Info("Calling SO webservice");
                           
                                if (isFileData)
                                {
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, username, subsiId, custPOdate);
                                   
                                }
                                else
                                {
                                    custPOdate = DateTime.Now.ToString(ConstantUtilities.DateFormat);
                                    responseModel = serviceCaller.soServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId, subsiId, custPOdate);
                                }
                         
                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into SOTransLineContent table of transLineId:-" + trLineID);

                            bool isSuccess = scTransBatch.InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, true);
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());
                                scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, true);
                            }

                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, true);
                        }
                        catch (Exception e)
                        {

                            log.Error(" fileUpload in SOManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;

                            scTransBatch.InsertSOTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                            scTransBatch.UpdateResponseSOTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                            scTransBatch.UpdateSOTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);

                            serviceResponse.Add(trLineID, responseModel);

                        }

                    }
                }


                int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper))
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    genericModel = scTransBatch.SoTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    genericModel = scTransBatch.SoTestResult(itemModel, trnsLineItems);

                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
                placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
                placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address

                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSO;
                    strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSO += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSO);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSO);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSO;
                        strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
                        // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;
                        if (isWeb)
                            strGoodyearSO = strGoodyearSO + "\\" + objGPCustomerMessage.BatchFileName;

                        Attachment attachment = new Attachment(strGoodyearSO);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
                    }
                    else
                    {
                        //fileupload SO
                        //send email to users

                        // List<Attachment> attachments = new List<Attachment>();
                        // string responseFile;
                        // responseFile = objGPCustomerMessage.BatchFileSourceAddress;
                        //// responseFile += "\\" + objGPCustomerMessage.BatchFileName;
                        // responseFile += "\\" + "ResponseFileGenerated" + objGPCustomerMessage.BatchID;
                        // if (!File.Exists(responseFile))
                        // {
                        //     File.Create(responseFile).Close();
                        // }
                        // responseFile = getResponseSO(responseFile, objGPCustomerMessage.BatchID);                      
                        // Attachment attachment = new Attachment(responseFile);
                        // attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        // attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        // GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", attachments);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");                
                return itemModelData;

            }
            catch (Exception exp)
            {
                
                log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }

        }
        public string GetFileType(int batchId)
        {
            string fileName;
            string FileType;
            using (var context = new GoodpackEDIEntities())
            {
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                int FileTypeId = (from s in context.Trn_SubsiToTransactionFileMapper
                                  where s.Filename == mapperFileName
                                  select s.MessageFormatId).FirstOrDefault();
                FileType = (from s in context.Ref_MessageFormat
                                   where s.Id == FileTypeId
                                   select s.MessageFormat).FirstOrDefault();
            
            }
            return FileType;
        }
       public string getErrorSOFile(int batchId)
        {
            string fileName;
            string errorFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                errorFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                               where s.Id == batchId
                               select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SOTransLine
                                where s.Id == batchId
                                select s.CustomerNumber).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                errorFile += batchId +"_"+customerCode+ "_ErrorFile";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                    errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    errorFile += fileName + "_ErrorFile";
                    File.Create(errorFile).Close();
                }
                else
                {
                    File.Create(errorFile).Close();
                }

                StreamWriter sw = File.AppendText(errorFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        if (item.FieldSequence == total)
                        {

                        }
                        else
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                    List<SOTransLineModel> ListTransLineModel = new List<SOTransLineModel>();
                    List<Trn_SOTransLine> SOTransLineDetails = (context.Trn_SOTransLine.Where(i => i.BatchId == batchId && i.IsActive == true && i.StatusId==1).ToList());

                    for (int i = 0; i < SOTransLineDetails.Count; i++)
                    {
                        int lineID = SOTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SOTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|'};
                       // char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len || j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

                return errorFile;
        
        }
        public string getErrorSCFile(int batchId)
        {
            string fileName;
            string errorFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                errorFile ="";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SCTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
          

                errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                errorFile += batchId + "_" + customerCode + "_ErrorFile";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                    errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    errorFile += batchId + "_" + customerCode + "_ErrorFile";
                    File.Create(errorFile).Close();
                }
                else
                {
                    File.Create(errorFile).Close();
                }

                StreamWriter sw = File.AppendText(errorFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        if (item.FieldSequence == total)
                        {

                        }
                        else
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                    List<SCTransLineModel> ListTransLineModel = new List<SCTransLineModel>();
                    List<Trn_SCTransLine> SCTransLineDetails = (context.Trn_SCTransLine.Where(i => i.BatchId == batchId && i.IsActive == true && i.StatusId==1).ToList());

                    for (int i = 0; i < SCTransLineDetails.Count; i++)
                    {
                        int lineID = SCTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SCTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|'};
                      //  char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len || j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return errorFile;

        }
        public string getErrorSSCFile(int batchId)
        {
            string fileName;
            string errorFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                errorFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SSCTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();


                errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                errorFile += batchId + "_" + customerCode + "_ErrorFile";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                    errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    errorFile += batchId + "_" + customerCode + "_ErrorFile";
                    File.Create(errorFile).Close();
                }
                else
                {
                    File.Create(errorFile).Close();
                }

                StreamWriter sw = File.AppendText(errorFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        if (item.FieldSequence == total)
                        {

                        }
                        else
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                    List<SSCTransLineModel> ListTransLineModel = new List<SSCTransLineModel>();
                    List<Trn_SSCTransLine> SSCTransLineDetails = (context.Trn_SSCTransLine.Where(i => i.BatchId == batchId && i.IsActive == true && i.StatusId == 1).ToList());

                    for (int i = 0; i < SSCTransLineDetails.Count; i++)
                    {
                        int lineID = SSCTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SSCTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|' };
                        //  char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len || j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return errorFile;

        }

        public string getErrorASNFile(int batchId)
        {
            string fileName;
            string errorFile;
            string customerCode;
            using (var context = new GoodpackEDIEntities())
            {
                errorFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_ASNTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();


                errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                errorFile += batchId + "_" + customerCode + "_ErrorFile";
                if (File.Exists(errorFile))
                {
                    File.Delete(errorFile);
                    errorFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    errorFile += batchId + "_" + customerCode + "_ErrorFile";
                    File.Create(errorFile).Close();
                }
                else
                {
                    File.Create(errorFile).Close();
                }

                StreamWriter sw = File.AppendText(errorFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        if (item.FieldSequence == total)
                        {

                        }
                        else
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                    List<SCTransLineModel> ListTransLineModel = new List<SCTransLineModel>();
                    List<Trn_ASNTransLine> SCTransLineDetails = (context.Trn_ASNTransLine.Where(i => i.BatchId == batchId && i.IsActive == true && i.StatusId == 1).ToList());

                    for (int i = 0; i < SCTransLineDetails.Count; i++)
                    {
                        int lineID = SCTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_ASNTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|' };
                        //  char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len || j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return errorFile;

        }
        public string getResponseSO(int batchId)
        {
            string fileName;
            string transactionFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                transactionFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SOTransLine
                                where s.BatchId == batchId
                                select s.CustomerNumber).FirstOrDefault();
                transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                if (File.Exists(transactionFile))
                {
                    File.Delete(transactionFile);
                    transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                    File.Create(transactionFile).Close();
                }
                else
                {
                    File.Create(transactionFile).Close();
                }
                StreamWriter sw = File.AppendText(transactionFile);
                {
                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data); 
                        sw.Write(",");

                    }
                    sw.Write("SAP Response");
                    sw.WriteLine();
                    List<SOTransLineModel> ListTransLineModel = new List<SOTransLineModel>();
                    List<Trn_SOTransLine> SOTransLineDetails = (context.Trn_SOTransLine.Where(i => i.BatchId == batchId && i.IsActive == true).ToList());

                    for (int i = 0; i < SOTransLineDetails.Count; i++)
                    {
                        int lineID = SOTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SOTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|'};
                       // char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if ( j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }
                        SOTransLineModel TransLineModel = new SOTransLineModel();
                        var responseList = new List<string>();
                        List<Trn_SOTransLineContent> SOTransLineContentDetails = (context.Trn_SOTransLineContent.Where(s => s.BatchID == batchId && s.LineID == lineID).ToList());
                        for (int k = 0; k < SOTransLineContentDetails.Count; k++)
                        {
                            string SAPResponse;
                            SAPResponse = SOTransLineContentDetails[k].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                        string SAPresponse;
                        SAPresponse = string.Join("|", responseList.ToArray());

                        sw.Write(SAPresponse);
                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return transactionFile;
        }
        public string getResponseSC(int batchId)
        {
            string fileName;
            string transactionFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                transactionFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SCTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                if (File.Exists(transactionFile))
                {
                    File.Delete(transactionFile);
                    transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                    File.Create(transactionFile).Close();
                }
                else
                {
                    File.Create(transactionFile).Close();
                }

                StreamWriter sw = File.AppendText(transactionFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        sw.Write(",");

                    }
                    sw.Write("SAP Response");
                    sw.WriteLine();
                    List<SCTransLineModel> ListTransLineModel = new List<SCTransLineModel>();
                    List<Trn_SCTransLine> SCTransLineDetails = (context.Trn_SCTransLine.Where(i => i.BatchId == batchId && i.IsActive == true).ToList());

                    for (int i = 0; i < SCTransLineDetails.Count; i++)
                    {
                        int lineID = SCTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SCTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|'};
                       // char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if ( j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        //sw.WriteLine();
                        SCTransLineModel TransLineModel = new SCTransLineModel();
                        var responseList = new List<string>();
                        List<Trn_SCTransLineContent> SCTransLineContentDetails = (context.Trn_SCTransLineContent.Where(s => s.BatchID == batchId && s.LineID == lineID).ToList());
                        for (int k = 0; k < SCTransLineContentDetails.Count; k++)
                        {
                            string SAPResponse;
                            SAPResponse = SCTransLineContentDetails[k].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                        string SAPresponse;
                        SAPresponse = string.Join("|", responseList.ToArray());

                        sw.Write(SAPresponse);
                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return transactionFile;
        }
        public string getResponseASN(int batchId)
        {
            string fileName;
            string transactionFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                transactionFile = "";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_ASNTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                if (File.Exists(transactionFile))
                {
                    File.Delete(transactionFile);
                    transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                    File.Create(transactionFile).Close();
                }
                else
                {
                    File.Create(transactionFile).Close();
                }

                StreamWriter sw = File.AppendText(transactionFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        sw.Write(",");

                    }
                    sw.Write("SAP Response");
                    sw.WriteLine();
                    List<ASNTransLineModel> ListTransLineModel = new List<ASNTransLineModel>();
                    List<Trn_ASNTransLine> ASNTransLineDetails = (context.Trn_ASNTransLine.Where(i => i.BatchId == batchId && i.IsActive == true).ToList());

                    for (int i = 0; i < ASNTransLineDetails.Count; i++)
                    {
                        int lineID = ASNTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_ASNTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|' };
                        // char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        //sw.WriteLine();
                        ASNTransLineModel TransLineModel = new ASNTransLineModel();
                        var responseList = new List<string>();
                        List<Trn_ASNTransLineContent> ASNTransLineContentDetails = (context.Trn_ASNTransLineContent.Where(s => s.BatchID == batchId && s.LineID == lineID).ToList());
                        for (int k = 0; k < ASNTransLineContentDetails.Count; k++)
                        {
                            string SAPResponse;
                            SAPResponse = ASNTransLineContentDetails[k].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                        string SAPresponse;
                        SAPresponse = string.Join("|", responseList.ToArray());

                        sw.Write(SAPresponse);
                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return transactionFile;
        }
        public string getResponseSSC(int batchId)
        {
            string fileName;
            string transactionFile;
            string customerCode;

            using (var context = new GoodpackEDIEntities())
            {
                transactionFile ="";
                fileName = (from s in context.Trn_SCTransBatch
                            where s.Id == batchId
                            select s.FileName).FirstOrDefault();
                customerCode = (from s in context.Trn_SSCTransLine
                                where s.BatchId == batchId
                                select s.CustomerCode).FirstOrDefault();
                string mapperFileName = (from s in context.Trn_SCTransBatch
                                         where s.Id == batchId
                                         select s.MapperFileName).FirstOrDefault();
                transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                if (File.Exists(transactionFile))
                {
                    File.Delete(transactionFile);
                    transactionFile = GPArchitecture.Cache.Config.getConfig("FileDownLoadPath");
                    transactionFile += batchId + "_" + customerCode + "_TransactionFile";
                    File.Create(transactionFile).Close();
                }
                else
                {
                    File.Create(transactionFile).Close();
                }

                StreamWriter sw = File.AppendText(transactionFile);
                {

                    IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperFileName);
                    int total = mappedData.Count;
                    foreach (var item in mappedData)
                    {
                        string data = item.FieldName.Replace("\r\n", "");
                        data = data.Replace(",", "");
                        sw.Write(data);
                        sw.Write(",");

                    }
                    sw.Write("SAP Response");
                    sw.WriteLine();
                    List<SSCTransLineModel> ListTransLineModel = new List<SSCTransLineModel>();
                    List<Trn_SSCTransLine> SSCTransLineDetails = (context.Trn_SSCTransLine.Where(i => i.BatchId == batchId && i.IsActive == true).ToList());

                    for (int i = 0; i < SSCTransLineDetails.Count; i++)
                    {
                        int lineID = SSCTransLineDetails[i].Id;
                        string lineData = (from s in context.Trn_SCTransLine
                                           where s.Id == lineID
                                           select s.LineContent).FirstOrDefault();
                        lineData = lineData.Replace(",", "");

                        char[] delimiterChars = { '|'};
                        //char[] delimiterChars = { '|', '\0' };
                        string[] words = lineData.Split(delimiterChars);
                        int len = words.Length;
                        int j = 0;
                        foreach (string match in words)
                        {
                            j++;
                            if (j == len - 1)
                            {
                                sw.Write("{0}", match);
                            }
                            else
                            {
                                sw.Write("{0},", match);
                            }
                        }

                        //sw.WriteLine();
                        SSCTransLineModel TransLineModel = new SSCTransLineModel();
                        var responseList = new List<string>();
                        List<Trn_SSCTransLineContent> SSCTransLineContentDetails = (context.Trn_SSCTransLineContent.Where(s => s.BatchID == batchId && s.LineId == lineID).ToList());
                        for (int k = 0; k < SSCTransLineContentDetails.Count; k++)
                        {
                            string SAPResponse;
                            SAPResponse = SSCTransLineContentDetails[k].SapResponseMessage;
                            responseList.Add(SAPResponse);
                        }
                        string SAPresponse;
                        SAPresponse = string.Join("|", responseList.ToArray());

                        sw.Write(SAPresponse);
                        sw.WriteLine();
                    }
                }
                sw.Close();
            }

            return transactionFile;
        }
        public string getSubsyNameFromId(int SubsiId)
        {
            string SubsiName;
                using (var context = new GoodpackEDIEntities())
                {
                    SubsiName = (from s in context.Gen_EDISubsies
                               where s.Id == SubsiId
                               select s.SubsiName).FirstOrDefault();
                }
                return SubsiName;
      

        }

        public void SCTransactionData(List<ResponseModel> responseModel, bool isFileData)
        {
            scTransBatch.InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
            scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);


        }
        public void SSCTransactionData(List<ResponseModel> responseModel, bool isFileData)
        {
            scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
            scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);


        }
        
        private int SSCLineCount(int batchId, int statusType)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = null;
                switch (statusType)
                {
                    case 0: statusCode = LineStatus.SUCCESS.ToString();
                        break;
                    case 1: statusCode = LineStatus.ERROR.ToString();
                        break;
                }
                int statusId = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                int Count = (from s in context.Trn_SSCTransLine
                             where ((s.StatusId == statusId) && (s.BatchId == batchId))
                             select s).ToList().Count;
                return Count;
            }

        }

        public IList<GenericItemModel> SOFileData(string mapperName, int subsiId, bool isFileType, string path)
        {
            log.Debug("Traansaction Manager -SOFileData Begin :-" + path + "Mapper Name :-" + mapperName);
            scPath = path;
            bool isValidFile = false;
            string message = "";

            FileInfo fileInfo = null;
            if (isFileType)
            {
                objGPCustomerMessage = new SCTransactionModel();

                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                objGPCustomerMessage.BatchFileName = fileInfo.Name;
            }
            else
            {
                scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
            }
            objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
            IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
            GenericItemModel itemModel;
            isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
            if (isValidFile)
            {
                string scData;
                bool isFirstVal = true;
                int i = 0;
                int transactionId;
                string[] data;
                log.Debug("Traansaction Manager -SOFileData data extract" );
                if (mapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(mapperName))
                {
                    log.Debug("Traansaction Manager -SOFileData SOSumiData");
                    data = SOSumiData(fileInfo);

                }
                else if (mapperName == GPArchitecture.Cache.Config.getConfig("YokohamaSOMapper") || GPArchitecture.Cache.Config.getConfigList("YokohamaSOMapper").Contains(mapperName))
                {
                    log.Debug("Traansaction Manager -SOFileData YokohamaSOMapper");
                    if (objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xls") || objGPCustomerMessage.BatchFileName.ToLower().EndsWith(".xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                    {

                        scData = ExcelParser.ExcelToCSV(scPath, 20, 0);
                        var line = "";
                        List<String> lines = new List<string>();
                        bool firstItem = true;
                        int indexValue = 0;
                        //if (scData.Contains(','))
                        //    throw new IOException("Data has special character , in it. Please remove it and upload");
                        if (scData != null)
                        {
                            string[] dataArray = scData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).Where(s => !string.IsNullOrEmpty(s)).ToArray();
                            int delimiterCount = dataArray[0].Count(x => x == ',');
                            log.Info("SCData CSV loop Start");
                            foreach (var linedata in dataArray)
                            {
                                if (firstItem)
                                {
                                    indexValue = linedata.Split(',').Where(s => !string.IsNullOrEmpty(s)).ToArray().Length;
                                    firstItem = false;
                                    lines.Add(linedata);
                                }
                                else
                                {
                                    string[] lineArray = linedata.Split(',');
                                    if (indexValue != lineArray.Length)
                                        throw new IOException("Data has special character , in it. Please remove it and upload");
                                    Array.Resize(ref lineArray, indexValue);
                                    lines.Add(string.Join(",", lineArray));
                                }
                            }
                            log.Info("SCData CSV loop End");
                        }
                        scData = scData.Replace("|", ",");
                        objGPCustomerMessage.Message = scData;
                        data = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                    }
                    else
                    {
                        using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                        {
                            scData = objStreamReader.ReadToEnd();
                            var line = "";
                            List<String> lines = new List<string>();
                            bool firstItem = true;
                            int indexValue = 0;
                            if (scData != null)
                            {
                                string[] dataArray = scData.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).Where(s => !string.IsNullOrEmpty(s)).ToArray();
                                int delimiterCount = dataArray[0].Count(x => x == ',');
                                log.Info("SCData CSV loop Start");
                                foreach (var linedata in dataArray)
                                {
                                    if (firstItem)
                                    {
                                        indexValue = linedata.Split(',').Where(s => !string.IsNullOrEmpty(s)).ToArray().Length;
                                        firstItem = false;
                                        lines.Add(linedata);
                                    }
                                    else
                                    {
                                        string[] lineArray = linedata.Split(',');
                                        if (indexValue != lineArray.Length)
                                            throw new IOException("Data has special character , in it. Please remove it and upload");
                                        Array.Resize(ref lineArray, indexValue);
                                        lines.Add(string.Join(",", lineArray));
                                    }
                                }
                                log.Info("SCData CSV loop End");
                            }
                            //if (scData.Contains(','))
                            //    throw new IOException("Data has special character , in it. Please remove it and upload");                           
//Commented due to performance improvement -Anoop S
                            //Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                            //scData = regData.Replace(scData, ",");
                            scData = scData.Replace(",", "|");
                            objGPCustomerMessage.Message = scData;
                            objStreamReader.Close();
                            objStreamReader.Dispose();

                        }
                    }
                    log.Debug("Traansaction Manager -SOFileData Yokohama Parser");
                    data = Yokohama_Custom.Parse(objGPCustomerMessage.Message, out message);
                }
                else
                {
                    
                    data = SCData(fileInfo, out message);
                }
                using (var context = new GoodpackEDIEntities())
                {                   
                    transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                     where s.Filename == mapperName
                                     select s.TransactionCodeId).FirstOrDefault();
                }
                log.Debug("Traansaction Manager -SOFileData End data extract");
                IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
                string soCacheName = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss") + "_SOData";
                log.Debug("Traansaction Manager -SOFileData begin validation");
                IList<Ref_LookupNames> lookUpList = GetLookupList();
                IList<Trn_LookupValues> lookUpValueList = GetLookupValues(mapperName);
                ValidationDataModel validationData = GetValidationDataForField(mapperName, transactionId);
//For formating data according to mapped data ***Anoop S *** Start               
                
                //List<string> dataResultModified = new List<string>();
                //var mapperDataStruct =new List<KeyValuePair<string,string>>();
                //var splitData = data.Select(x => x.Split('|'));
                //int itration = 0;
                //List<string> dataResultNew = null;
                //var arrayIndexex = new List<int>();
                //string mappedSAPHeader = null;
                //using (var context = new GoodpackEDIEntities())
                //{
                //    var query = from st in context.Trn_SubsiToTransactionFileMapper
                //                join ms in context.Trn_MappingSubsi on st.Id equals ms.SubsiToTransactionFileMapperId
                //                join mf in context.Trn_MapperFieldSpecs on st.Id equals mf.SubsiToTransactionMapperId
                //                from mc in context.Trn_MappingConfiguration
                //                where ms.Id == mc.MappingSubsiId && mf.FieldSequence == mc.SourceFieldSequence && mc.SourceFieldSequence > 0
                //                join sm in context.Ref_SAPMessageFields on mc.SapFieldSeq equals sm.FieldSequence
                //                where st.Filename == mapperName && sm.TransactionId == transactionId
                //                select new { mf.FieldName, sm.SAPFieldName };
                //    mapperDataStruct = query.ToList().Distinct().Select(t => new KeyValuePair<string, string>(t.FieldName, t.SAPFieldName)).ToList();
                //}
                //List<string> mappedSAPHeaderNew = null;
                //foreach (var dataResult in splitData)
                //{
                //    dataResultNew = dataResult.ToList();
                //    if (itration == 0)
                //    {
                //        var mappedDataHeader = mapperDataStruct.Select(x => x.Key).ToList();
                //        mappedSAPHeaderNew = mapperDataStruct.Select(x => x.Value).ToList();
                //        List<string> mappedSAPHeaderTemp = mapperDataStruct.Select(x => x.Value).ToList();
                //        var mappedDataHeaderTemp = mappedDataHeader;
                //        foreach (var dataResultTemp in dataResult)
                //        {
                //            var indexVal = mappedDataHeaderTemp.FindIndex(c => c == dataResultTemp);
                //            mappedDataHeaderTemp.RemoveAt(indexVal);
                //            mappedSAPHeaderTemp.RemoveAt(indexVal);
                //        }
                //        foreach (var dataResultTemp in mappedDataHeaderTemp)
                //        {
                //            arrayIndexex.Add(dataResult.ToList().FindIndex(x => x == mappedDataHeaderTemp.FirstOrDefault()));
                //        }
                //        arrayIndexex.ForEach(x => { mappedData.Add(mappedData[x]); });
                //        arrayIndexex.ForEach(x => { mappedSAPHeaderNew.RemoveAt(x);});
                //        mappedSAPHeaderTemp.ForEach(x => { mappedSAPHeaderNew.Add(x); });
                //    }
                //    else if (itration == 1)
                //    {

                //        mappedSAPHeader = string.Join("|", mappedSAPHeaderNew);
                //    }
                //    itration++;                    
                //    arrayIndexex.ForEach(x => { dataResultNew.Add(dataResultNew[x]); });                    
                //    dataResultModified.Add(string.Join("|", dataResultNew));
                //}
                //data = dataResultModified.ToArray();

//For formating data according to mapped data ***Anoop S *** End
   
                foreach (var item in data)
                {
                    if (item.Length != 0)
                    {
                        i++;
                        string val = item.Replace("\"", "").Replace("\"", "");
                        string[] stringData = val.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                        if (isFirstVal)
                        {
                            isFirstVal = false;
                            
                            if (stringData.Length == mappedData.Count)
                            {                               
                                
                                bool isMappedData = false;
//old implementation commented for performance improvement -Anoop S
                                for (int j = 0; j < mappedData.Count; j++)
                                {
                                    if (stringData[j].ToUpper() == mappedData[j].FieldName.ToUpper())
                                    {
                                        isMappedData = true;
                                        break;
                                    }
                                }
                               // var dataHeader = new string[] { };
                                //dataHeader = mappedData.Select(x => x.FieldName.ToUpper()).ToArray().Select(x => x.Replace(Environment.NewLine, "").Replace("\n", "")).ToArray();
                                //if (dataHeader.SequenceEqual(stringData))
                                //    isMappedData = true;
                                if (isMappedData)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.Name = val;
                                    if (!isFileType)
                                    {
                                        itemModel.DataValues = message;
                                    }
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    scDataModel.Add(itemModel);
                                    itemModel = new GenericItemModel();
                                    i++;
                                    itemModel.Id = i;                                    
//For formating data according to mapped data ***Anoop S ***
                                   // itemModel.Name = mappedSAPHeader;
                                    itemModel.Name = SAPHeaders(mapperName).Replace("\"", "").Replace("\r", "");
                                    scDataModel.Add(itemModel);
                                    ///////////////////////////
                                }
                                else
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Status = "false";
                                    if (!isFileType)
                                    {
                                        itemModel.DataValues = message;
                                    }
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    // itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                    itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file " + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                    scDataModel.Add(itemModel);
                                    return scDataModel;
                                }
                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false"; 
                                if (!isFileType)
                                {
                                    itemModel.DataValues = message;
                                }
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                // itemModel.Message = objGPCustomerMessage.Message;
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file" + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }
                        }
                        else
                        {
                            if (stringData.Length == mappedData.Count)
                            {
                                int counter = 0;
//old implementation commented for performance improvement -Anoop S
                                //for (int j = 0; j < stringData.Length; j++)
                                //{
                                //    if (stringData[j] == "")
                                //    {
                                //        counter++;
                                //    }
                                //}
                                counter = stringData.Count(x => x == "");
                                if (counter != stringData.Length)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.DataValues = soCacheName;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    itemModel.Name = item.Replace("\"", "");
                                    //SAP headers and items in the file
                                    //string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);
                                    //string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId, transactionId, lookUpList, lookUpValueList);
                                    string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId, transactionId, lookUpList, lookUpValueList, validationData);
                                  //  itemModel.CustomerPackerRelation = CheckCustomerPackerRelation(item.Replace("\"", ""), scDataModel[1].Name, dataWithDecodeValue);
                                    itemModel.DecodeValue = dataWithDecodeValue;
                                    scDataModel.Add(itemModel);
                                }
                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false";
                                if (!isFileType)
                                {
                                    itemModel.DataValues = message;
                                }
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file and Mapper";
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }
                        }
                    }
                }
                scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                if (isFileType)
                {
                    HttpContext.Current.Cache.Insert(soCacheName, message);
                }
                log.Debug("Traansaction Manager -SOFileData return");
                return scDataModel;
            }
            else
            {
                itemModel = new GenericItemModel();
                itemModel.Status = "false";
                itemModel.DataValues = message;
                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                itemModel.Message = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                itemModel.ErrorMessage = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                scDataModel.Add(itemModel);
                return scDataModel;
            }
        }
        public IList<GenericItemModel> SOChangeCancelFileData(int subsiId, bool isFileType, string path)
        {
            scPath = path;
            bool isValidFile = false;
            string message = "";
            string mapperName = GPArchitecture.Cache.Config.getConfig("ChangeCancelSOMapper");

            FileInfo fileInfo = null;
            if (isFileType)
            {
                objGPCustomerMessage = new SCTransactionModel();

                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                objGPCustomerMessage.BatchFileName = fileInfo.Name;
            }
            else
            {
                scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                fileInfo = new FileInfo(scPath);
                objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
            }

            //bool countLimit = CountFileLines();
            //if (countLimit)
            //{
            objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
            IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
            GenericItemModel itemModel;
            isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
            if (isValidFile)
            {
                bool isFirstVal = true;
                int i = 0;
                string[] data;

                data = SCData(fileInfo, out message);
                SalesOrderUploadChangeCancelFields soChangeClass = new SalesOrderUploadChangeCancelFields();
                IList<ChangeCancelSOFieldsModel> mappedData = soChangeClass.ChangeCacelSOData();


                foreach (var item in data)
                {
                    if (item.Length != 0)
                    {
                        i++;
                        string val = item.Replace("\"", "").Replace("\"", "");
                        string[] stringData = val.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                        // stringData=stringData.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                        if (isFirstVal)
                        {
                            isFirstVal = false;

                            if (stringData.Length == mappedData.Count)
                            {
                                bool isMappedData = false;
                                for (int j = 0; j < mappedData.Count; j++)
                                {
                                    if (stringData[j] == mappedData[j].FieldName)
                                    {
                                        isMappedData = true;
                                        break;
                                    }
                                }
                                if (isMappedData)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.Name = val;
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    scDataModel.Add(itemModel);
                                    itemModel = new GenericItemModel();
                                    i++;
                                    itemModel.Id = i;
                                    itemModel.Name = soChangeClass.SAPHeadersForChangeCancelFileUpload().Replace("\"", "").Replace("\r", "");
                                    scDataModel.Add(itemModel);
                                    ///////////////////////////
                                }
                                else
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Status = "false";
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    // itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                  
                                    scDataModel.Add(itemModel);
                                    return scDataModel;
                                }
                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false";
                                itemModel.DataValues = message;
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                // itemModel.Message = objGPCustomerMessage.Message;
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                               
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }
                        }
                        else
                        {
                            if (stringData.Length == mappedData.Count)
                            {
                                int counter = 0;
                                for (int j = 0; j < stringData.Length; j++)
                                {
                                    if (stringData[j] == "")
                                    {
                                        counter++;
                                    }
                                }
                                if (counter != stringData.Length)
                                {
                                    itemModel = new GenericItemModel();
                                    itemModel.Id = i;
                                    itemModel.DataValues = message;
                                    itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                    itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                    itemModel.Message = objGPCustomerMessage.Message;
                                    itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                    itemModel.Name = item.Replace("\"", "");
                                    //SAP headers and items in the file
                                    string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);
                                    // if (dataWithDecodeValue != null)
                                    // {
                                    itemModel.DecodeValue = dataWithDecodeValue;
                                   // itemModel.DecodeValue = "0";                             
                                    // if (dataWithDecodeValue != null)
                                    // {
                                    scDataModel.Add(itemModel);
                                    //  }
                                    //else
                                    //{
                                    //    scDataModel = new List<GenericItemModel>(); ;
                                    //    itemModel = new GenericItemModel();
                                    //    itemModel.Status = "false";
                                    //    itemModel.Message = "Decode value is missing.Please add decode values";
                                    //    scDataModel.Add(itemModel);
                                    //    return scDataModel;

                                    //}
                                }

                            }
                            else
                            {
                                itemModel = new GenericItemModel();
                                itemModel.Status = "false";
                                itemModel.DataValues = message;
                                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                // itemModel.Message = objGPCustomerMessage.Message;
                                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                               
                                scDataModel.Add(itemModel);
                                return scDataModel;
                            }

                        }

                    }

                }
                scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                return scDataModel;
            }
            else
            {
                itemModel = new GenericItemModel();
                itemModel.Status = "false";
                itemModel.DataValues = message;
                itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                itemModel.FileName = objGPCustomerMessage.BatchFileName;
                itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
             
                scDataModel.Add(itemModel);
                return scDataModel;
            }
            //}
            //else
            //{
            //    IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
            //    GenericItemModel itemModel;
            //    itemModel = new GenericItemModel();
            //    itemModel.Status = "false";
            //    itemModel.Message = "Please check the uploaded file...should contain less than 50 lines";
            //    scDataModel.Add(itemModel);
            //    return scDataModel;

            //}

        }
        public string[] getAllRecipients(string mapperFile)
        {
            List<string> mailList = new List<string>();            
              //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                               where s.Filename == mapperFile
                               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GoodpackEDI.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
        public string[] getAllRecipients(int subsiId)
        {
            List<string> mailList = new List<string>();            
              //  mailList.Add(objGPCustomerMessage.EmailId);            
            using (var context = new GoodpackEDIEntities())
            {
                //int subsiId = (from s in context.Trn_SubsiToTransactionFileMapper
                //               where s.Filename == mapperFile
                //               select s.EDISubsiId).FirstOrDefault();
                IList<int> userLists = (from s in context.Trn_UserToSubsiMapper
                                        where s.SubsiId == subsiId
                                        select s.UserId).ToList();

                foreach (var item in userLists)
                {
                    string mailId = (from s in context.Gen_User
                                     where s.Id == item && s.ReceiveNotifications == true && s.IsActive == true
                                     select s.EmailId).FirstOrDefault();
                    log.Info("Mail sending to " + mailId);
                    if (mailId != null)
                    {
                        mailList.Add(mailId);
                    }


                }
                string emailId = (from s in context.Gen_Configuration
                                  where s.ConfigItem == GoodpackEDI.ConstantUtils.ConstantUtilities.AdminForSc
                                  select s.Value).FirstOrDefault();
                mailList.Add(emailId);
                return mailList.ToArray();
            }


        }
        public IList<string> RetrieveErrorData(int lineId, TRANSACTION_TYPES transType)
        {
            using (var context = new GoodpackEDIEntities())
            {
                switch (transType)
                {
                    case TRANSACTION_TYPES.SC:
                        return (from s in context.Trn_SCTransLineContent
                                where s.LineID == lineId
                                select s.SapResponseMessage).ToList();

                    case TRANSACTION_TYPES.SSC:
                        return (from s in context.Trn_SSCTransLineContentNew
                                where s.LineId == lineId
                                select s.SapResponseMessage).ToList();

                    case TRANSACTION_TYPES.SO:
                        return RetrieveSOErrorData(lineId);


                    default: return new List<string>();
                }
            }
        }
      
        public IList<GenericItemModel> SCFileData(string mapperName, int subsiId, bool isFileType,string path) 
         {
             try
             {
                 log.Debug("SCFileData start");
                 scPath = path;
                 bool isValidFile = false;
                 FileInfo fileInfo = null;
                 string message = "";
                 if (isFileType)
                 {
                     objGPCustomerMessage = new SCTransactionModel();
                     fileInfo = new FileInfo(scPath);

                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                     objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                     objGPCustomerMessage.BatchFileName = fileInfo.Name;
                 }
                 else
                 {

                     scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                     fileInfo = new FileInfo(scPath);
                     objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                 }
                 //bool countLimit = CountFileLines();
                 //if (countLimit)
                 //{
                 IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
                 GenericItemModel itemModel;
                 isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
                 log.Debug("SCFileData data read start");
                 if (isValidFile)
                 {
                     bool isFirstVal = true;
                     int i = 0;
                     int transactionId;
                     string[] data;
                     //if (mapperName == "SumitomoShipmentConfirmation")
                     //{
                     if (mapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation"))
                     {
                         data = SCSumiData(fileInfo);
                     }
                     else
                     {
                         data = SCData(fileInfo, out message);
                     }

                     IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
                     using (var context = new GoodpackEDIEntities())
                     {
                         objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                          join p in context.Ref_TransactionTypes
                                                                                                                          on s.TransactionCodeId equals p.Id
                                                                                                                          where s.Filename == mapperName
                                                                                                                          select p.TransactionCode).FirstOrDefault());
                         transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                          where s.Filename == mapperName
                                          select s.TransactionCodeId).FirstOrDefault();
                     }
                     data = data.ToList().Where(s => !string.IsNullOrEmpty(s)).ToArray();
                     //For restresting file line count to 50 -included by Anoop S

                     //if (objGPCustomerMessage.TransactionType.ToString()=="SC" && data.Length > 51)
                     //{
                     //    itemModel = new GenericItemModel();
                     //    itemModel.Message = "Maximum line's allowed in a file is 50";
                     //    itemModel.Status = "false";
                     //    scDataModel.Add(itemModel);
                     //    return scDataModel;
                     //}
                     log.Debug("SCFileData data read end");
                     IList<Ref_LookupNames> lookUpList = GetLookupList();
                     IList<Trn_LookupValues> lookUpValueList = GetLookupValues(mapperName);
                     ValidationDataModel validationData = GetValidationDataForField(mapperName, transactionId);
                     log.Debug("SCFileData data config read start");
                     foreach (var item in data)
                     {
                         if (item.Length != 0)
                         {
                             i++;
                             string val = item.Replace("\"", "").Replace("\"", ""); 
                             string[] stringData = val.ToUpper().Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                             if (isFirstVal)
                             {
                                 isFirstVal = false;

                                 if (stringData.Length == mappedData.Count)
                                 {
                                     bool isMappedData = false;
                                     for (int j = 0; j < mappedData.Count; j++)
                                     {
                                         if (stringData[j].ToUpper() == mappedData[j].FieldName.ToUpper())
                                         {
                                             isMappedData = true;
                                             break;
                                         }
                                     }
                                     //var dataHeader = new string[] { };
                                     //dataHeader = mappedData.Select(x => x.FieldName.ToUpper()).ToArray().Select(x => x.Replace(Environment.NewLine, " ").Replace("\n", " ")).ToArray();
                                     //if (dataHeader.SequenceEqual(stringData))
                                     //    isMappedData = true;
                                     if (isMappedData)
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Id = i;
                                         itemModel.Name = val;
                                         //itemModel.DataValues = message;
                                         if (i == 1)
                                         {
                                             itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                             itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                             itemModel.Message = objGPCustomerMessage.Message;
                                         }
                                         scDataModel.Add(itemModel);
                                         itemModel = new GenericItemModel();
                                         i++;
                                         itemModel.Id = i;
                                         //if(objGPCustomerMessage.TransactionType==TRANSACTION_TYPES.ASN && )
                                         //{

                                         //}
                                         itemModel.Name = SAPHeaders(mapperName).Replace("\"", "").Replace("\r", "");

                                         scDataModel.Add(itemModel);
                                         ///////////////////////////
                                     }
                                     else
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Status = "false";
                                         //itemModel.DataValues = message;
                                         itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                         itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                         itemModel.Message = objGPCustomerMessage.Message;
                                         itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                         itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file " + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                         scDataModel.Add(itemModel);
                                         log.Debug("SCFileData End");
                                         return scDataModel;
                                     }
                                 }
                                 else
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Status = "false";
                                     // itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                     itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file" + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                     scDataModel.Add(itemModel);
                                     log.Debug("SCFileData End");
                                     return scDataModel;
                                 }
                             }
                             else
                             {
                                 if (stringData.Length == mappedData.Count)
                                 {
                                     int counter = 0;
                                     for (int j = 0; j < stringData.Length; j++)
                                     {
                                         if (stringData[j] == "")
                                         {
                                             counter++;
                                         }
                                     }
                                     if (counter != stringData.Length)
                                     {
                                         itemModel = new GenericItemModel();
                                         itemModel.Id = i;
                                         //itemModel.DataValues = message;
                                         if (i == 1)
                                         {
                                             itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                             itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                             itemModel.Message = objGPCustomerMessage.Message;
                                         }
                                         itemModel.Name = item.Replace("\"", "");
                                         //SAP headers and items in the file
                                         //string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);
                                         //string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId,transactionId, lookUpList, lookUpValueList);
                                         string dataWithDecodeValue = CheckForDecodeValueValidationNew(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId, transactionId, lookUpList, lookUpValueList, validationData);
                                        // itemModel.CustomerPackerRelation = CheckCustomerPackerRelation(item.Replace("\"", ""), scDataModel[1].Name, dataWithDecodeValue);
                                         // if (dataWithDecodeValue != null)
                                         // {
                                         itemModel.DecodeValue = dataWithDecodeValue;


                                         scDataModel.Add(itemModel);
                                         //  }
                                         //else
                                         //{
                                         //    scDataModel = new List<GenericItemModel>(); ;
                                         //    itemModel = new GenericItemModel();
                                         //    itemModel.Status = "false";
                                         //    itemModel.Message = "Decode value is missing.Please add decode values";
                                         //    scDataModel.Add(itemModel);
                                         //    return scDataModel;

                                         //}
                                     }

                                 }
                                 else
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Status = "false";
                                     //itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file and Mapper";
                                     itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                     scDataModel.Add(itemModel);
                                     log.Debug("SCFileData End");
                                     return scDataModel;
                                 }

                             }

                         }

                     }
                     log.Debug("SCFileData data config read end");
                     scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                     // HttpRuntime.Cache.Insert("mothy_SC", message);
                     log.Debug("SCFileData End");
                     return scDataModel;
                 }
                 else
                 {
                     itemModel = new GenericItemModel();
                     itemModel.Status = "false";
                     //   itemModel.DataValues = message;
                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                     itemModel.Message = objGPCustomerMessage.Message;
                     itemModel.ErrorMessage = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                     itemModel.Message = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                     scDataModel.Add(itemModel);
                     log.Debug("SCFileData End");
                     return scDataModel;
                 }
                 //}
                 //else
                 //{
                 //    IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
                 //    GenericItemModel itemModel;
                 //    itemModel = new GenericItemModel();
                 //    itemModel.Status = "false";
                 //    itemModel.Message = "Please check the uploaded file...should contain less than 50 lines";
                 //    scDataModel.Add(itemModel);
                 //    return scDataModel;

                 //}

                 log.Debug("SCFileData End");
             }
             catch (Exception e)
             {
                 log.Debug("SCFileData End");
                 //  log.Error("SCController-FileData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new List<GenericItemModel>();
             }
        }
        public int GetLookupId(string LookupName)
        {           
            using (var context = new GoodpackEDIEntities())
            {
             return   (from s in context.Ref_LookupNames
                 where s.LookupName==LookupName
                 select s.Id).FirstOrDefault();

            }
        }

        public IList<Ref_LookupNames> GetLookupList()
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames                        
                        select s).ToList();
            }
        }
        public int GetLookupId(string LookupName,IList<Ref_LookupNames> lookUpList)
        {           
             return (lookUpList.Where(i=>i.LookupName==LookupName).Select(i=>i.Id).FirstOrDefault());           
        }
        private string CheckForDecodeValueValidation(string fileData, string headers, string mapperName, int subsiId)
        {
            StringBuilder fileDecodedData = new StringBuilder();

            string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            IDecodeValueManager decodeValueManager = new DecodeValueManager();
            int transactionId=0;
            for (int i = 0; i < headerValues.Length; i++)
            {
                string sapFieldName = headerValues[i];
                string inputType = String.Empty;
                int lookupNameId = 0;
              
                if (sapFieldName != "")
                {
                    using (var context = new GoodpackEDIEntities())
                    {
                         transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                             where s.Filename == mapperName
                                             select s.TransactionCodeId).FirstOrDefault();

                        inputType = (from s in context.Ref_SAPMessageFields
                                     where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                     select s.InputType).FirstOrDefault();
                     //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                    }
                    switch (sapFieldName.ToUpper())
                    {
                        case "CUSTOMER CODE":
                        case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            break;
                        case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            break;
                        case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                            break;
                        case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                            break;
                        case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                            break;
                        case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                            break;
                        case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                            break;
                        case "FRM LOCATION":
                        case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length==6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                            }
                        else
                            {
                                if (transactionId == 12)
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                                }
                                else
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Packer);
                                }
                            }
                            break;
                        case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length==6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Packer);
                            }
                            else
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                            }
                            break;
                        case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                            break;
                    }
                    TargetMessageInputTypes inputSAPType =
                            (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);


                    if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                    {
                        string decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(fileValues[i], mapperName, subsiId,lookupNameId);
                        if (decodeValue != null)
                        {
                            if (decodeValue.Length != 0)
                                errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName));
                            else
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                            fileDecodedData.Append(decodeValue);

                        }
                        else
                        {
                            bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                            if (!lookupName || !string.IsNullOrEmpty(fileValues[i]) )
                            {
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));                                
                                //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
                                fileDecodedData.Append("false");
                            }
                        }
                    }
                    else
                    {
                        errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                    }

                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
                else
                {
                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
            }
            if (!headerValues.Contains("Consignee"))
            {
                using (var context = new GoodpackEDIEntities())
                {
                    var consigneeIndex = Convert.ToInt32((from s in context.Trn_SubsiToTransactionFileMapper
                                                          join p in context.Trn_MappingSubsi on s.Id equals p.SubsiToTransactionFileMapperId
                                                          join m in context.Trn_MappingConfiguration on p.Id equals m.MappingSubsiId
                                                          where s.Filename == mapperName && m.SapFieldSeq == (context.Ref_SAPMessageFields.Where(i => i.TransactionId == transactionId && i.SAPFieldName == "Consignee")
                                                                                                     .Select(i => i.FieldSequence).FirstOrDefault())
                                                          select m.SourceFieldSequence).FirstOrDefault());
                    var inputTypeConsignee = (from s in context.Ref_SAPMessageFields
                                              where ((s.SAPFieldName == "Consignee") && (s.TransactionId == transactionId))
                                              select s.InputType).FirstOrDefault();
                    if (consigneeIndex > 0)
                    {
                        if (!fileValues[consigneeIndex - 1].StartsWith("6") && !(fileValues[consigneeIndex - 1].Length == 6))
                        {
                            var lookupNameIdConsignee = GetLookupId(ConstantUtilities.Consignee);
                            string decodeValueConsignee = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(fileValues[consigneeIndex - 1], mapperName, subsiId, lookupNameIdConsignee);
                            if (decodeValueConsignee == null)
                            {
                                errorDescription.Append("Deocde value missing for Consignee (" + fileValues[consigneeIndex - 1] + ") ,");
                                fileDecodedData.Append("false" + ConstantUtilities.delimiters[0]);
                            }
                        }
                    }
                }
            }
            fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
            return fileDecodedData.ToString();
        }

        public bool GetLookupIsNull(string lookupName, int transactionId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_SAPMessageFields
                        where s.SAPFieldName == lookupName && s.TransactionId == transactionId
                        select s.IsAllowNullValue).FirstOrDefault();

            }

        }
        private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
        {
            IValidationTypeManger validator = new ValidationTypeManager();
            Dictionary<string, string> validatorParams = new Dictionary<string, string>();
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.InputType, inputType);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.MapperName, mapperName);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.FileData, fileValue);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.SAPField, sapField);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
            switch (inputType)
            {
                case GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);

                default:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
            }


        }
//New function for improving performance -Anoop S
        private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField, ValidationDataModel validationData)
        {
            IValidationTypeManger validator = new ValidationTypeManager();
            Dictionary<string, string> validatorParams = new Dictionary<string, string>();
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.InputType, inputType);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.MapperName, mapperName);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.FileData, fileValue);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.SAPField, sapField);
            validatorParams.Add(GoodpackEDI.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
            switch (inputType)
            {
                case GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams, validationData);

                default:
                    return validator.ValidationType(GoodpackEDI.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams, validationData);
            }


        }
        private string CheckBarcodeValidation(string mapperName, string fileValue)
        {
            Regex r = new Regex("^[a-zA-Z0-9]*$");
            string s = "";
            if (!r.IsMatch(fileValue)) 
                s = "Pin Number ( "+fileValue+" ) should not contain special character(s), ";
            return s;

        }
        private string CheckCustomerPackerRelation(string fileData, string header, string decodeValue)
        {
            string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            string[] headerValues = header.Split(ConstantUtilities.delimiters, StringSplitOptions.None).Select(i=>i.ToUpper()).ToArray();
            string[] decodeValues = decodeValue.Split(ConstantUtilities.delimiters, StringSplitOptions.None);

            int keyIndexCustomer = -1;
            int keyIndexPacker = -1;
            if (Array.IndexOf(headerValues, "CUSTOMER") > -1)
                keyIndexCustomer = Array.IndexOf(headerValues, "CUSTOMER");
            else if (Array.IndexOf(headerValues, "CUSTOMER CODE") > -1)
                keyIndexCustomer = Array.IndexOf(headerValues, "CUSTOMER CODE");
            else if (Array.IndexOf(headerValues, "CUSTOMER NUMBER") > -1)
                keyIndexCustomer = Array.IndexOf(headerValues, "CUSTOMER NUMBER");

            if (Array.IndexOf(headerValues, "PACKER") > -1)
                keyIndexPacker = Array.IndexOf(headerValues, "PACKER");
            else if (Array.IndexOf(headerValues, "FRM LOCATION") > -1)
                keyIndexPacker = Array.IndexOf(headerValues, "FRM LOCATION");
            else if (Array.IndexOf(headerValues, "FROM LOCATION") > -1)
                keyIndexPacker = Array.IndexOf(headerValues, "FROM LOCATION");
            int returnVal;
            if (!string.IsNullOrEmpty(decodeValues[keyIndexCustomer]))
                int.TryParse(decodeValues[keyIndexCustomer], out returnVal);
            else
                int.TryParse(fileValues[keyIndexCustomer], out returnVal);
            if (returnVal == 0)
                return "has invalid data";

            if (!string.IsNullOrEmpty(decodeValues[keyIndexPacker]))
                int.TryParse(decodeValues[keyIndexPacker], out returnVal);
            else
                int.TryParse(fileValues[keyIndexPacker], out returnVal);

            if (returnVal == 0)
                return "has invalid data";

            var customer =!string.IsNullOrEmpty(decodeValues[keyIndexCustomer])?int.Parse(decodeValues[keyIndexCustomer]): int.Parse(fileValues[keyIndexCustomer]);
            var packer = !string.IsNullOrEmpty(decodeValues[keyIndexPacker]) ? int.Parse(decodeValues[keyIndexPacker]) : int.Parse(fileValues[keyIndexPacker]);
            var packerRelation = new List<int>();
            using(var context =new GoodpackEDIEntities())
            {
                var packerList = (from s in context.Trn_CustomerToPackerMapper
                                 where s.CustomerCode == customer
                                 select s.PackerCode).ToList();
                packerRelation=packerList.Where(i => i == packer).ToList();
            }

            return packerRelation.Count() > 0 ? "" : packer.ToString();
        }
        private string CheckForDecodeValueValidationNew(string fileData, string headers,string mapperName, int subsiId, int transactionId, IList<Ref_LookupNames> lookUpList, IList<Trn_LookupValues> lookUpValueList,ValidationDataModel validationData)
        {
            StringBuilder fileDecodedData = new StringBuilder();

            string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
            IDecodeValueManager decodeValueManager = new DecodeValueManager();
            for (int i = 0; i < headerValues.Length; i++)
            {
                string sapFieldName = headerValues[i];
                string inputType = String.Empty;
                int lookupNameId = 0;
                string barcodeHeader = "";
                if (sapFieldName != "")
                {
                    using (var context = new GoodpackEDIEntities())
                    {                       

                        inputType = (from s in context.Ref_SAPMessageFields
                                     where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                     select s.InputType).FirstOrDefault();
                        //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                        barcodeHeader = (from s in context.Ref_SAPMessageFields
                                         where s.Ref_TransactionTypes.TransactionCode == "SSC" && s.FieldSequence == 37
                                         select s.SAPFieldName).First();
                    }
                    switch (sapFieldName.ToUpper())
                    {
                        case "CUSTOMER CODE":
                        case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                            break;
                        case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                            break;
                        case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                            break;
                        case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
                            break;
                        case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port, lookUpList);
                            break;
                        case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL, lookUpList);
                            break;
                        case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD, lookUpList);
                            break;
                        case "FRM LOCATION":
                        case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
                            }
                            else
                            {
                                if (transactionId == 12)
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                                }
                                else
                                {
                                    lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
                                }
                            }
                            break;
                        case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                            }
                            else
                            {
                                lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
                            }
                            break;
                        case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine, lookUpList);
                            break;
                    }
                    TargetMessageInputTypes inputSAPType =
                            (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);


                    if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                    {
                        string decodeValue = FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId, lookUpValueList);
                        if (decodeValue != null)
                        {
                            if (decodeValue.Length != 0)
                                errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName, validationData));
                            else
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData));
                            fileDecodedData.Append(decodeValue);

                        }
                        else
                        {
                            bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                            if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
                            {
                                errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData));
                                //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
                                fileDecodedData.Append("false");
                            }
                        }
                    }
                    else
                    {
                        if (sapFieldName == barcodeHeader)
                            errorDescription.Append(CheckBarcodeValidation(mapperName, fileValues[i]));
                        else
                            errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName, validationData));
                    }

                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
                else
                {
                    fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                }
            }
            fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
            return fileDecodedData.ToString();
        }
        //private string CheckForDecodeValueValidationNew(string fileData, string headers, string mapperName, int subsiId, int transactionId, IList<Ref_LookupNames> lookUpList, IList<Trn_LookupValues> lookUpValueList)
        //{
        //    StringBuilder fileDecodedData = new StringBuilder();

        //    string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
        //    string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
        //    IDecodeValueManager decodeValueManager = new DecodeValueManager();
        //    for (int i = 0; i < headerValues.Length; i++)
        //    {
        //        string sapFieldName = headerValues[i];
        //        string inputType = String.Empty;
        //        int lookupNameId = 0;
        //        string barcodeHeader = "";
        //        if (sapFieldName != "")
        //        {
        //            using (var context = new GoodpackEDIEntities())
        //            {

        //                inputType = (from s in context.Ref_SAPMessageFields
        //                             where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
        //                             select s.InputType).FirstOrDefault();
        //                //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
        //                barcodeHeader = (from s in context.Ref_SAPMessageFields
        //                                 where s.Ref_TransactionTypes.TransactionCode == "SSC" && s.FieldSequence == 37
        //                                 select s.SAPFieldName).First();
        //            }
        //            switch (sapFieldName.ToUpper())
        //            {
        //                case "CUSTOMER CODE":
        //                case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
        //                    break;
        //                case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
        //                    break;
        //                case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
        //                    break;
        //                case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
        //                    break;
        //                case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port, lookUpList);
        //                    break;
        //                case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL, lookUpList);
        //                    break;
        //                case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD, lookUpList);
        //                    break;
        //                case "FRM LOCATION":
        //                case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
        //                    {
        //                        lookupNameId = GetLookupId(ConstantUtilities.CustomerCode, lookUpList);
        //                    }
        //                    else
        //                    {
        //                        if (transactionId == 12)
        //                        {
        //                            lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
        //                        }
        //                        else
        //                        {
        //                            lookupNameId = GetLookupId(ConstantUtilities.Packer, lookUpList);
        //                        }
        //                    }
        //                    break;
        //                case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
        //                    {
        //                        lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
        //                    }
        //                    else
        //                    {
        //                        lookupNameId = GetLookupId(ConstantUtilities.Consignee, lookUpList);
        //                    }
        //                    break;
        //                case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine, lookUpList);
        //                    break;
        //            }
        //            TargetMessageInputTypes inputSAPType =
        //                    (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);


        //            if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
        //            {
        //                string decodeValue = FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId, lookUpValueList);
        //                if (decodeValue != null)
        //                {
        //                    if (decodeValue.Length != 0)
        //                        errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName));
        //                    else
        //                        errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
        //                    fileDecodedData.Append(decodeValue);

        //                }
        //                else
        //                {
        //                    bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
        //                    if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
        //                    {
        //                        errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
        //                        //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
        //                        fileDecodedData.Append("false");
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (sapFieldName == barcodeHeader)
        //                    errorDescription.Append(CheckBarcodeValidation(mapperName, fileValues[i]));
        //                else
        //                    errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
        //            }

        //            fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
        //        }
        //        else
        //        {
        //            fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
        //        }
        //    }
        //    fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
        //    return fileDecodedData.ToString();
        //}
        private string FindDecodeValue(string lookUpCode, string mapperFile, int subsiId, int lookupNameId, IList<Trn_LookupValues> lookUpValueList)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int codeLeng;
                bool isConsignee = false;
                bool isPacker = false;
                bool isCustomercode = false;
                codeLeng = lookUpCode.Length;
                string lookupName = GetLookupName(lookupNameId);
                switch (lookupName.ToUpper())
                {
                    case "CONSIGNEE": if (lookUpCode.StartsWith("6") && codeLeng == 6)
                        {
                            isConsignee = true;
                        }
                        break;
                    case "CUSTOMER CODE": if (lookUpCode.StartsWith("3") && codeLeng == 6)
                        {
                            isCustomercode = true;
                        }
                        break;
                    case "PACKER": if (lookUpCode.StartsWith("5") && codeLeng == 6)
                        {
                            isPacker = true;
                        }
                        break;
                }               
                if (isConsignee || isPacker || isCustomercode)
                {
                    return String.Empty;
                }
                else
                {
                    return lookUpValueList.Where(i => i.LookupCode.ToUpper().Trim() == lookUpCode.ToUpper().Trim() && i.SubsiId == subsiId && i.MapperName == mapperFile && i.LookupNameId == lookupNameId).Select(i => i.LookupDecodeValue).FirstOrDefault();
                    //return (from s in context.Trn_LookupValues
                    //        where ((s.LookupCode == lookUpCode) && (s.SubsiId == subsiId) && (s.MapperName == mapperFile) && (s.LookupNameId == lookupNameId))
                    //        select s.LookupDecodeValue).FirstOrDefault();
                }
            }
        }

        private IList<Trn_LookupValues> GetLookupValues(string mapperName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Trn_LookupValues
                        where s.MapperName==mapperName
                        select s).ToList();
            }
        }

        private ValidationDataModel GetValidationDataForField(string mapperName, int transactionType)
        {
            using (var context = new GoodpackEDIEntities())
            {
                ValidationDataModel dataModel = new ValidationDataModel();
                var dateField = (from s in context.Trn_SubsiToTransactionFileMapper
                              join p in context.Trn_MappingSubsi on s.Id equals p.SubsiToTransactionFileMapperId
                              join m in context.Trn_MappingConfiguration on p.Id equals m.MappingSubsiId
                              where (s.Filename == mapperName) && (m.InputDateFormat != null)
                              select m.InputDateFormat).FirstOrDefault();

                var characterList = (from s in context.Ref_TransactionTypes
                                     join p in context.Ref_SAPMessageFields
                                         on s.Id equals p.TransactionId
                                     where (s.Id == transactionType)
                                     select new CharacterList {MaxLength= p.MaxLength,SAPName= p.SAPFieldName }).ToList();

                var boolList = (from s in context.Ref_SAPMessageFields
                                join p in context.Ref_TransactionTypes
                                    on s.TransactionId equals p.Id
                                where ((p.Id == transactionType))
                                select new BoolList {IsNullAllowed= s.IsAllowNullValue,SAPName= s.SAPFieldName }).ToList();
                dataModel.DateString = dateField;
                dataModel.CharList =characterList;
                dataModel.BoolList = boolList;
                return dataModel;
            }
        }
        private string GetLookupName(int lookupNameId)
        {

            using (var context = new GoodpackEDIEntities())
            {
                return (from s in context.Ref_LookupNames
                        where s.Id == lookupNameId
                        select s.LookupName).FirstOrDefault();

            }

        }

        private string SAPHeaders(string filename)
        {
            StringBuilder sapFields = new StringBuilder();
            using (var context = new GoodpackEDIEntities())
            {
                int fileMapperId = (from s in context.Trn_SubsiToTransactionFileMapper
                                    where s.Filename == filename
                                    select s.Id).FirstOrDefault();

                //List<Trn_MappingConfiguration> mappingConfig = (from s in context.Trn_MappingConfiguration
                //                                                 where s.MappingSubsiId == fileMapperId
                //                                                 select s).ToList();

                List<Trn_MapperFieldSpecs> specs = (from s in context.Trn_MapperFieldSpecs
                                                    where s.SubsiToTransactionMapperId == fileMapperId
                                                    select s).ToList();

                //foreach (var item in specs)
                //{
                //    mappingConfig.Find(x => x.SapFieldSeq.Contains(item.
                //}
                Trn_MappingSubsi mapperData = (from s in context.Trn_MappingSubsi
                                               where s.SubsiToTransactionFileMapperId == fileMapperId
                                               select s).FirstOrDefault();

                foreach (var item in specs)
                {
                    int sapFieldSequenceId = (from s in context.Trn_MappingConfiguration
                                              where (((s.SourceFieldSequence == item.FieldSequence) || (s.SourceFieldSeq2 == item.FieldSequence)) && (s.MappingSubsiId == mapperData.Id))
                                              select s.SapFieldSeq).FirstOrDefault();
                    string sapField = (from s in context.Ref_SAPMessageFields
                                       where ((s.FieldSequence == sapFieldSequenceId) && (s.TransactionId == mapperData.SapMessageFieldId))
                                       select s.SAPFieldName).FirstOrDefault();
                    sapFields.Append(sapField);
                    sapFields.Append("" + ConstantUtilities.delimiters[0]);
                }
                return sapFields.Remove(sapFields.Length - 1, 1).ToString();

            }

        }

        public string[] SOSumiData(FileInfo fileInfo)
        {
            string scData;

            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                scData = ExcelParser.ExcelToCSV(scPath, 21, 0);
                scData = ExcelParser.GetSumitomoFileContent(scData);
                //scData = scData.Replace(",", "|");
                objGPCustomerMessage.Message =GetDataWithoutEmptyColumns(scData);
            }

            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    scData = objStreamReader.ReadToEnd();
                    objGPCustomerMessage.Message = scData;
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                }
            }
            //  return scData.Split('\r\n');   
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        }

        public string[] SCSumiData(FileInfo fileInfo)
        {
            string scData;

            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                scData = scData.Replace(",", "|");
                objGPCustomerMessage.Message =GetDataWithoutEmptyColumns(scData);
              
            }

            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    scData = objStreamReader.ReadToEnd();
                    scData = scData.Replace("\t", "|");
                    objGPCustomerMessage.Message = scData;
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                }
            }
            //  return scData.Split('\r\n');  
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);

        }

        private string[] SCData(FileInfo fileInfo,out string ObjMessage)
        {
            string scData;
            if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
            {
                log.Info("SCData xls read Start");
                scData = ExcelParser.ExcelToCSV(scPath, 0, 0);
                log.Info("SCData xls to csv  end");
                scData = GetDataWithoutEmptyColumns(scData);
                log.Info("SCData xls to csv  remove empty");
                objGPCustomerMessage.Message = scData;
                ObjMessage = scData;
                log.Info("SCData xls read End");
            }
            else
            {
                using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                {
                    var line = "";
                    List<String> lines = new List<string>();
                    bool firstItem = true;
                    int indexValue = 0;
                    //For removing blank column from CSV
                    log.Info("SCData CSV read Start");
 
//                    while ((line = objStreamReader.ReadLine()) != null)
//                    {
////Commented for performance improvement-Anoop S
//                        //if (Regex.IsMatch(line, "^[a-zA-Z0-9]"))
//                        //{
//                        //    if (firstItem)
//                        //    {
//                        //        if (line.LastIndexOf(',') + 1 == line.Length)
//                        //        {
//                        //            line = line.Remove(line.LastIndexOf(','));
//                        //        }
//                        //        indexValue = line.Split(',').Length;
//                        //        firstItem = false;
//                        //    }
//                        //    else
//                        //    {
//                        //        string[] values;
//                        //        if ((line.Split(',').Length) != indexValue)
//                        //        {
//                        //            values = line.Split(',');
//                        //            line = string.Join(",", values, 0, indexValue);
//                        //        }
//                        //    }
//                        //    lines.Add(line);
//                        //}

//                        var lineData = line.Split(',').Where(s => !string.IsNullOrEmpty(s)).ToArray();
//                        if (firstItem)
//                        {
//                            indexValue = lineData.Length;
//                            firstItem = false;
//                            lines.Add(string.Join(",", lineData));
//                        }
//                        else
//                        {
//                            Array.Resize(ref lineData, indexValue);
//                            lines.Add(string.Join(",", lineData));
//                        }
//                    }
                    var data = objStreamReader.ReadToEnd();
                    objStreamReader.Close();
                    objStreamReader.Dispose();
                    //if (data.Contains(','))
                    //    throw new IOException("Data has special character , in it. Please remove it and upload"); 
                    if (data != null)
                    {
                        string[] dataArray = data.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).Where(s => !string.IsNullOrEmpty(s)).ToArray();
                        int delimiterCount = dataArray[0].Count(i => i == ',');
                        log.Info("SCData CSV loop Start");
                        foreach (var linedata in dataArray)
                        {                            
                            if (firstItem)
                            {
                                indexValue = linedata.Split(',').Where(s => !string.IsNullOrEmpty(s)).ToArray().Length;
                                firstItem = false;
                                lines.Add(linedata);
                            }
                            else
                            {
                                string[] lineArray = linedata.Split(',');
                                if (indexValue != lineArray.Length)
                                    throw new IOException("Data has special character , in it. Please remove it and upload"); 
                                Array.Resize(ref lineArray, indexValue);
                                lines.Add(string.Join(",", lineArray));
                            }
                        }
                        log.Info("SCData CSV loop End");
                    }
                    log.Info("SCData CSV data read End");
                    scData = string.Join(Environment.NewLine, lines.ToArray());
                    Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",RegexOptions.Compiled | RegexOptions.IgnoreCase);
                    //scData = regData.Replace(scData, "|");
                    scData = scData.Replace(",", "|");
                    objGPCustomerMessage.Message = scData;
                    ObjMessage = scData;
                    log.Info("SCData CSV read END");
                }
            }      
            return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
        } 


        public IList<ResponseLineDataModel> ReSubmit(string transactionType, string username, int batchId, List<int> lineIds, int status, string mapperName)
        {
            string val = null;

            Dictionary<string, string> decodeValueStore = new Dictionary<string, string>();
            try
            {
                switch (status)
                {
                    case 0: val = null; break;
                    case 1: val = null; break;
                    case 2: val = ""; break;
                    default: return new List<ResponseLineDataModel>();

                }
                int successCount = 0;
                int errorCount = 0;
                ResponseLineDataModel lineData = null;
                IList<ResponseLineDataModel> allLineData = new List<ResponseLineDataModel>();
                using (var context = new GoodpackEDIEntities())
                {
                    bool isSuccess = true;
                    bool isTestMode = true;
                    //string transType = TRANSACTION_TYPES.SC.ToString();
                    int transId = (from s in context.Ref_TransactionTypes
                                   where s.TransactionName == transactionType
                                   select s.Id).FirstOrDefault();
                    IList<Ref_SAPMessageFields> sapFields = (from s in context.Ref_SAPMessageFields
                                                             where s.TransactionId == transId
                                                             select s).ToList();


                    //for (int i = 0; i < lineIds.Count; i++)
                    //{
                    //    int lineVal = lineIds[i];


                    if (transactionType == TransactionTypes.SC)
                    {

                            for (int i = 0; i < lineIds.Count; i++)
                            {
                                int lineVal = lineIds[i];
                                lineData = new ResponseLineDataModel();
                                //ResponseLineDataModel lineData = new ResponseLineDataModel();
                                Trn_SCTransLine lines = (from s in context.Trn_SCTransLine
                                                         where s.Id == lineVal
                                                         select s).FirstOrDefault();
                                IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                                responseLineModelData = scLineContentParsing(lines, sapFields, mapperName);
                                context.SaveChanges();
                                log.Info("-----------Resubmit data caller-------------");
                                log.Info("Service calling for Line Id:" + lines.Id);
                                List<ResponseModel> responseModelData = new List<ResponseModel>();
                                responseModelData = serviceCallingSC(lineIds, i, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess);
                                isSuccess = responseModelData[0].Success;
                                lineData = updateSCtransLine(val, isTestMode, isSuccess, status, batchId, lineIds, i);
                                allLineData.Add(lineData);
                            }
                    }
                    else if (transactionType == TransactionTypes.ASN)
                    {

                        for (int i = 0; i < lineIds.Count; i++)
                        {
                            int lineVal = lineIds[i];
                            lineData = new ResponseLineDataModel();
                            //ResponseLineDataModel lineData = new ResponseLineDataModel();
                            Trn_ASNTransLine lines = (from s in context.Trn_ASNTransLine
                                                     where s.Id == lineVal
                                                     select s).FirstOrDefault();
                            IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                            responseLineModelData = asnLineContentParsing(lines, sapFields, mapperName);
                            context.SaveChanges();
                            log.Info("-----------Resubmit data caller-------------");
                            log.Info("Service calling for Line Id:" + lines.Id);
                            List<ResponseModel> responseModelData = new List<ResponseModel>();
                            responseModelData = serviceCallingASN(lineIds, i, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess);
                            isSuccess = responseModelData[0].Success;
                            lineData = updateASNtransLine(val, isTestMode, isSuccess, status, batchId, lineIds, i);
                            allLineData.Add(lineData);
                        }
                    }

                    else if (transactionType == TransactionTypes.SSC)
                    {
                        List<int> lineValues;
                        List<int> lineNos;
                        lineValues = lineIds;
                        while (lineValues.Count != 0)
                        {
                            Dictionary<int, string> CustrefValues = new Dictionary<int, string>();
                            int line = lineValues[0];
                            for (int i = 0; i < lineValues.Count; i++)
                            {
                                int lineVal = lineValues[i];
                                Trn_SSCTransLine lines = (from s in context.Trn_SSCTransLine
                                                          where s.Id == lineVal
                                                          select s).FirstOrDefault();

                                CustrefValues.Add(lineValues[i], lines.CustomerReferenceNumber);

                            }
                            List<int> Custref = new List<int>();
                            List<string> CustrefValue = new List<string>();

                            foreach (KeyValuePair<int, string> pair in CustrefValues)
                            {
                                CustrefValue.Add(pair.Value);
                            }
                            //List<string> CustrefVal = new List<string>();
                            //CustrefVal = CustrefVal.FindAll(i => i ==CustrefValue[0]);
                            foreach (KeyValuePair<int, string> pair in CustrefValues)
                            {
                                if (pair.Value == CustrefValue[0])
                                {
                                    Custref.Add(pair.Key);
                                }
                            }

                            lineNos = Custref;
                            if (lineNos.Count > 0)
                            {
                                //for (int i = 0; i < lineNos.Count; i++)
                                //{
                                int lineVal = lineNos[0];
                                lineData = new ResponseLineDataModel();
                                //ResponseLineDataModel lineData = new ResponseLineDataModel();
                                Trn_SSCTransLine lines = (from s in context.Trn_SSCTransLine
                                                          where s.Id == lineVal
                                                          select s).FirstOrDefault();
                                IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                                responseLineModelData = sscLineContentParsing(lines, sapFields, mapperName);
                                context.SaveChanges();
                                log.Info("-----------Resubmit data caller-------------");
                                log.Info("Service calling for Line Id:" + lines.Id);
                                IList<ResponseModel> responseModelData = new List<ResponseModel>();
                                responseModelData = serviceCallingSSC(lineNos, 0, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess, lines.MvtType);
                                isSuccess = responseModelData[0].Success;
                                lineData = updateSSCtransLine(val, isTestMode, isSuccess, status, batchId, lineNos, 0);
                                allLineData.Add(lineData);

                                lineValues.RemoveAll(j => j == lineVal);
                                // }
                            }

                           }
                      
                          }               
                          
                                // }

                    if (transactionType == TransactionTypes.SC)
                    {
                        scTransBatch.UpdateTransBatch(batchId, successCount, errorCount, isTestMode);
                        scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));

                    }
                    else if (transactionType == TransactionTypes.SSC)
                    {
                        scTransBatch.UpdateSSCTransBatch(batchId, successCount, errorCount, isTestMode);
                        //  }
                        //}

                        //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                        //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                        scTransBatch.UpdateSCBatchCounts(batchId, SSCLineCount(batchId, 1), SSCLineCount(batchId, 0));

                    }
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }
                if (objGPCustomerMessage == null)
                {
                    objGPCustomerMessage = new SCTransactionModel();
                }
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", batchId.ToString());
                objGPCustomerMessage.EmailId = userDetails.EmailId;
                string[] recipientList = new string[] { userDetails.EmailId };
                GPTools.SendEmail(placeholders, getAllRecipients(mapperName), "NOTIFY_CUSTOMER", null);
                log.Info("Sending email to customer :" + userDetails.FullName.ToString());

                return allLineData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager ReSubmit method.mapperName:" + mapperName + ", username:" + username + ", batchId:" + batchId + ", lineIds:" + lineIds + ", status:" + status + ". Api Exception : Message- " + exp.Message);
                IList<ResponseLineDataModel> allLineData = new List<ResponseLineDataModel>();
                ResponseLineDataModel itemsModel = new ResponseLineDataModel();
                SCTransactionModel transModel = new SCTransactionModel();
                transModel.StatusCode = BatchStatus.ERROR;
                transModel.ErrorCode = ErrorCodes.E_GEN;
                transModel.ErrorDesc = exp.Message;
                transModel.BatchID = batchId;
                itemsModel.LineMessage = new List<string>();
                itemsModel.LineId = 0;
                itemsModel.Status = false;
                itemsModel.LineMessage.Add("Unable to process the Re-submit data.Please check decode values for the data");
                allLineData.Add(itemsModel);
                // Update TRANS_BATCH                
                scTransBatch.UpdateSCBatchFileData(transModel);
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", username);
                placeholders.Add("$$BatchID$$", batchId.ToString());
                // string[] recipientList = new string[] { userDetails.EmailId };
                GPTools.SendEmail(placeholders, getAllRecipients(mapperName), "NOTIFY_CUSTOMER", null);
                return allLineData;

            }

        }

        public ResponseLineDataModel updateSSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //  IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseSSCTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseSSCTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.SSC);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }
        public ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.SC);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }

        public ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i,Trn_SCTransLine lineDetail)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode, lineDetail);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode, lineDetail);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];            
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.SC);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }

        public ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i, Trn_SCTransLine lineDetail,bool isTermTrip)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode, lineDetail, isTermTrip);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode, lineDetail, isTermTrip);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.SC);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }
        public ResponseLineDataModel updateSCtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i, Trn_SCTransLine lineDetail, bool isTermTrip,string itrNumber,string message)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode, lineDetail, isTermTrip,itrNumber,message);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode, lineDetail, isTermTrip,itrNumber,message);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.SC);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }
        public ResponseLineDataModel updateASNtransLine(string val, bool isTestMode, bool isSuccess, int status, int batchId, List<int> lineIds, int i)
        {
            ResponseLineDataModel lineDataModel = new ResponseLineDataModel();
            //IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            if (val == null)
            {
                isTestMode = false;
            }
            else
            {
                isTestMode = true;
            }
            if (isSuccess)
            {

                log.Info("Updating TransLine");
                switch (status)
                {
                    //case 0:
                    //    scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineVal, responseModelData);
                    //    scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineVal,isTestMode);
                    //    break;

                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateASNResponseTransLine(batchId, LineStatus.SUCCESS, lineIds[i], isTestMode);
                        break;
                }

            }
            else
            {
                switch (status)
                {
                    case 0:
                    case 1:
                    case 2:
                        scTransBatch.UpdateASNResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                        break;
                    //    case 2: scTransBatch.UpdateResponseTransLine(batchId, LineStatus.ERROR, lineIds[i], isTestMode);
                    //break;
                }
            }
            lineDataModel = new ResponseLineDataModel();
            lineDataModel.Status = isSuccess;
            lineDataModel.LineMessage = new List<string>();
            lineDataModel.LineId = lineIds[i];
            lineDataModel.LineMessage = (List<string>)RetrieveErrorData(lineIds[i], TRANSACTION_TYPES.ASN);
            // lineData.Add(lineDataModel);
            return lineDataModel;
        }
        public List<ResponseModel> serviceCallingSSC(List<int> lineIds, int i, Trn_SSCTransLine lines, string val, ref int successCount, ref int errorCount, string username, int batchId, bool isSuccess, string MvtType)
        {
            List<ResponseModel> responseModelData = new List<ResponseModel>();
            SSCServiceManager serviceManager = new SSCServiceManager();
            using (var context = new GoodpackEDIEntities())
            {
                try
                {
                    Trn_SSCTransLine linesinfo = new Trn_SSCTransLine();
                    List<Trn_SSCTransLine> lineInfos = new List<Trn_SSCTransLine>();
                    for (int j = 0; j < lineIds.Count; j++)
                    {
                        int lineval = lineIds[j];
                        linesinfo = (from s in context.Trn_SSCTransLine
                                     where s.Id == lineval
                                     select s).FirstOrDefault();

                        lineInfos.Add(linesinfo);
                    }
                    responseModelData = serviceManager.sscResubmitCaller(lineInfos, val, ref successCount, ref errorCount, username, MvtType);
                }
                catch (Exception e)
                {

                    ResponseModel responseModelVal = new ResponseModel();
                    responseModelVal.Success = false;
                    responseModelVal.Message = "Decode value might be missing.Please check the decode values";
                    responseModelData.Add(responseModelVal);
                }

                bool isFirst = true;
                for (int j = 0; j < responseModelData.Count; j++)
                {
                    string lineStatus;

                    if (responseModelData[j].Success)
                    {

                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {

                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();

                    }
                    log.Info("Line Status of line Id:" + lineIds[i] + " is: " + lineStatus);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    int line = lineIds[i];
                    if (isFirst)
                    {
                        IList<Trn_SSCTransLineContent> lineContentData = context.Trn_SSCTransLineContent.Where(x => x.LineId == line).ToList();
                        foreach (var item in lineContentData)
                        {
                            if (item != null)
                            {
                                context.Trn_SSCTransLineContent.Remove(item);
                                context.SaveChanges();
                            }
                        }
                        isFirst = false;
                        log.Info("Removed all the content from Trn_SCTransLineContent with line Id:" + line);
                    }


                    Trn_SSCTransLineContent lineContent = new Trn_SSCTransLineContent();
                    lineContent.BatchID = batchId;
                    lineContent.LineId = lineIds[i];
                    lineContent.StatusId = lineStatusId;
                    lineContent.SapResponseMessage = responseModelData[j].Message;
                    context.Trn_SSCTransLineContent.Add(lineContent);
                    context.SaveChanges();
                    log.Info("Inserted new sap responses of line Id:" + lineIds[i] + "with status:" + lineStatus);
                }
            }
            return responseModelData;
        }
        public List<ResponseModel> serviceCallingSC(List<int> lineIds, int i, Trn_SCTransLine lines, string val, ref int successCount, ref int errorCount, string username, int batchId, bool isSuccess)
        {
            List<ResponseModel> responseModelData = new List<ResponseModel>();
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            SCServiceManager serviceManager = new SCServiceManager();
            using (var context = new GoodpackEDIEntities())
            {
                try
                {
                   
                    responseModelData = serviceManager.scResubmitCaller(lines, val, ref successCount, ref errorCount, username);
                }
                catch (Exception e)
                {

                    ResponseModel responseModelVal = new ResponseModel();
                    responseModelVal.Success = false;
                    responseModelVal.Message = "Decode value might be missing.Please check the decode values";
                    responseModelData.Add(responseModelVal);
                }
                bool isFirst = true;
                for (int j = 0; j < responseModelData.Count; j++)
                {
                    string lineStatus;

                    if (responseModelData[j].Success)
                    {

                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {

                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();

                    }
                    log.Info("Line Status of line Id:" + lineIds[i] + " is: " + lineStatus);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    int line = lineIds[i];
                    if (isFirst)
                    {
                        IList<Trn_SCTransLineContent> lineContentData = context.Trn_SCTransLineContent.Where(x => x.LineID == line).ToList();
                        foreach (var item in lineContentData)
                        {
                            if (item != null)
                            {
                                context.Trn_SCTransLineContent.Remove(item);
                                context.SaveChanges();
                            }
                        }
                        isFirst = false;
                        log.Info("Removed all the content from Trn_SCTransLineContent with line Id:" + line);
                    }


                    Trn_SCTransLineContent lineContent = new Trn_SCTransLineContent();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineIds[i];
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModelData[j].Message;
                    context.Trn_SCTransLineContent.Add(lineContent);
                    context.SaveChanges();
                    log.Info("Inserted new sap responses of line Id:" + lineIds[i] + "with status:" + lineStatus);
                }
            }
            return responseModelData;
        }

        public List<ResponseModel> serviceCallingASN(List<int> lineIds, int i, Trn_ASNTransLine lines, string val, ref int successCount, ref int errorCount, string username, int batchId, bool isSuccess)
        {
            List<ResponseModel> responseModelData = new List<ResponseModel>();
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            SCServiceManager serviceManager = new SCServiceManager();
            using (var context = new GoodpackEDIEntities())
            {
                try
                {

                    responseModelData = serviceManager.asnResubmitCaller(lines, val, ref successCount, ref errorCount, username);
                }
                catch (Exception e)
                {

                    ResponseModel responseModelVal = new ResponseModel();
                    responseModelVal.Success = false;
                    responseModelVal.Message = "Decode value might be missing.Please check the decode values";
                    responseModelData.Add(responseModelVal);
                }
                bool isFirst = true;
                for (int j = 0; j < responseModelData.Count; j++)
                {
                    string lineStatus;

                    if (responseModelData[j].Success)
                    {

                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {

                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();

                    }
                    log.Info("Line Status of line Id:" + lineIds[i] + " is: " + lineStatus);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    int line = lineIds[i];
                    if (isFirst)
                    {
                        IList<Trn_ASNTransLineContent> lineContentData = context.Trn_ASNTransLineContent.Where(x => x.LineID == line).ToList();
                        foreach (var item in lineContentData)
                        {
                            if (item != null)
                            {
                                context.Trn_ASNTransLineContent.Remove(item);
                                context.SaveChanges();
                            }
                        }
                        isFirst = false;
                        log.Info("Removed all the content from Trn_SCTransLineContent with line Id:" + line);
                    }


                    Trn_ASNTransLineContent lineContent = new Trn_ASNTransLineContent();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineIds[i];
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModelData[j].Message;
                    context.Trn_ASNTransLineContent.Add(lineContent);
                    context.SaveChanges();
                    log.Info("Inserted new sap responses of line Id:" + lineIds[i] + "with status:" + lineStatus);
                }
            }
            return responseModelData;
        }

        public IList<ResponseLineDataModel> sscLineContentParsing(Trn_SSCTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName)
        {
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            using (var context = new GoodpackEDIEntities())
            {
                foreach (var item in sapFields)
                {
                    LineContentParser lineContentParser = new LineContentParser();
                    //TargetMessageInputTypes inputSAPType =
                    //          (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), item.InputType);
                    switch (item.SAPFieldName)
                    {
                        case "FRM Location": lines.FromLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SSC, mapperName), mapperName, ConstantUtilities.Packer);
                            if (lines.FromLocation != null)
                            {
                                if (lines.FromLocation.Length != 10)
                                {
                                    lines.FromLocation = "0000" + lines.FromLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        case "TO Location": lines.ToLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SSC, mapperName), mapperName, ConstantUtilities.Consignee);
                            if (lines.ToLocation != null)
                            {
                                if (lines.ToLocation.Length != 10)
                                {
                                    lines.ToLocation = "0000" + lines.ToLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        default: break;
                    }
                }
                context.SaveChanges();
            }
            return lineData;
        }

        public IList<ResponseLineDataModel> scLineContentParsing(Trn_SCTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName)
        {
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            using (var context = new GoodpackEDIEntities())
            {
                LineContentParser lineContentParser = new LineContentParser();
                foreach (var item in sapFields)
                {
                    //TargetMessageInputTypes inputSAPType =
                    //          (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), item.InputType);
                    switch (item.SAPFieldName)
                    {

                        case "Customer Number": lines.CustomerCode = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.LookupName_CustomerCode);
                            if (lines.CustomerCode != null)
                            {
                                if (lines.CustomerCode.Length != 10)
                                {
                                    lines.CustomerCode = "0000" + lines.CustomerCode;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;

                        case "From Location": lines.FromLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Packer);
                            if (lines.FromLocation != null)
                            {
                                if (lines.FromLocation.Length != 10)
                                {
                                    lines.FromLocation = "0000" + lines.FromLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        case "To Location": lines.ToLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Consignee);
                            if (lines.ToLocation != null)
                            {
                                if (lines.ToLocation.Length != 10)
                                {
                                    lines.ToLocation = "0000" + lines.ToLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        default: break;
                    }
                }
                context.SaveChanges();
            }
            return lineData;

        }
        public Trn_SCTransLine scLineContentParsingNew(Trn_SCTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName)
        {
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            //using (var context = new GoodpackEDIEntities())
            //{
                LineContentParser lineContentParser = new LineContentParser();
                foreach (var item in sapFields)
                {
                    //TargetMessageInputTypes inputSAPType =
                    //          (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), item.InputType);
                    switch (item.SAPFieldName)
                    {

                        case "Customer Number": lines.CustomerCode = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.LookupName_CustomerCode);
                            if (lines.CustomerCode != null)
                            {
                                if (lines.CustomerCode.Length != 10)
                                {
                                    lines.CustomerCode = "0000" + lines.CustomerCode;
                                }
                            }
                            else
                            {
                               // return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;

                        case "From Location": lines.FromLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Packer);
                            if (lines.FromLocation != null)
                            {
                                if (lines.FromLocation.Length != 10)
                                {
                                    lines.FromLocation = "0000" + lines.FromLocation;
                                }
                            }
                            else
                            {
                               // return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        case "To Location": lines.ToLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Consignee);
                            if (lines.ToLocation != null)
                            {
                                if (lines.ToLocation.Length != 10)
                                {
                                    lines.ToLocation = "0000" + lines.ToLocation;
                                }
                            }
                            else
                            {
                                //return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        default: break;
                    }
                //}
                //context.SaveChanges();
            }
            return lines;

        }
        public IList<ResponseLineDataModel> asnLineContentParsing(Trn_ASNTransLine lines, IList<Ref_SAPMessageFields> sapFields, string mapperName)
        {
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            using (var context = new GoodpackEDIEntities())
            {
                LineContentParser lineContentParser = new LineContentParser();
                foreach (var item in sapFields)
                {
                    //TargetMessageInputTypes inputSAPType =
                    //          (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), item.InputType);
                    switch (item.SAPFieldName)
                    {

                        case "Customer Number": lines.CustomerCode = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.LookupName_CustomerCode);
                            if (lines.CustomerCode != null)
                            {
                                if (lines.CustomerCode.Length != 10)
                                {
                                    lines.CustomerCode = "0000" + lines.CustomerCode;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;

                        case "From Location": lines.FromLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Packer);
                            if (lines.FromLocation != null)
                            {
                                if (lines.FromLocation.Length != 10)
                                {
                                    lines.FromLocation = "0000" + lines.FromLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        case "To Location": lines.ToLocation = lineContentParser.DecodeValueFinder(lines.LineContent, lineContentParser.LinePositionFinder(item.SAPFieldName, TRANSACTION_TYPES.SC, mapperName), mapperName, ConstantUtilities.Consignee);
                            if (lines.ToLocation != null)
                            {
                                if (lines.ToLocation.Length != 10)
                                {
                                    lines.ToLocation = "0000" + lines.ToLocation;
                                }
                            }
                            else
                            {
                                return DecodeValueMissing(lineContentParser.CodeValue);
                            }
                            break;
                        default: break;
                    }
                }
                context.SaveChanges();
            }
            return lineData;

        }

        public IList<ResponseLineDataModel> DecodeValueMissing(string codeValue)
        {
            IList<ResponseLineDataModel> lineData = new List<ResponseLineDataModel>();
            ResponseLineDataModel itemsModel = new ResponseLineDataModel();
            itemsModel.LineMessage = new List<string>();
            itemsModel.LineId = 0;
            itemsModel.Status = false;
            itemsModel.LineMessage.Add("Decode value is missing.Please add decode value for " + codeValue);
            lineData.Add(itemsModel);
            // Update TRANS_BATCH                

            return lineData;

        }

        public bool DeleteRecords(string transactionType, int BatchId, string DeleteComment,string selectIds)
        {
            string status = LineStatus.ERROR.ToString();
            using (var context = new GoodpackEDIEntities())
            {
                int statusId = (from s in context.Trn_LineStatus
                                where s.StatusCode == status
                                select s.Id).FirstOrDefault();
                List<int> listnew=selectIds.Split(',').Select(int.Parse).ToList<int>();
                if (transactionType == TransactionTypes.SC)
                {
                    IList<Trn_SCTransLine> lineStatus = context.Trn_SCTransLine.Where(i => (i.BatchId == BatchId) && (i.StatusId == statusId) && (i.InTestMode == true) && (listnew.Contains(i.Id))).ToList();
                    foreach (var item in lineStatus)
                    {
                        item.IsActive = false;
                        item.DeleteComments = DeleteComment;
                        context.SaveChanges();
                    }
                    Trn_SCTransBatch transBatchDetails = context.Trn_SCTransBatch.Where(i => i.Id == BatchId).SingleOrDefault();
                    var count = context.Trn_SCTransLine.Where(i => i.BatchId == BatchId && i.IsActive == false).Count();
                    var totalCount = context.Trn_SCTransBatch.Where(i => i.Id == BatchId ).Select(i=>i.RecordCountTotal).SingleOrDefault();
                    transBatchDetails.DeletedRecordCount = count;
                    transBatchDetails.RecordCountSAPError = Convert.ToInt32(totalCount) - count;
                    context.SaveChanges();
                }
                else if (transactionType == TransactionTypes.SSC)
                {
                    IList<Trn_SSCTransLine> lineStatus = context.Trn_SSCTransLine.Where(i => (i.BatchId == BatchId) && (i.StatusId == statusId) && (i.InTestMode == true) && (listnew.Contains(i.Id))).ToList();
                    foreach (var item in lineStatus)
                    {
                        item.IsActive = false;
                        item.DeleteComments = DeleteComment;
                        context.SaveChanges();
                    }
                    Trn_SSCTransBatch transBatchDetails = context.Trn_SSCTransBatch.Where(i => i.Id == BatchId).SingleOrDefault();
                    var count = context.Trn_SSCTransLine.Where(i => i.BatchId == BatchId && i.IsActive == false).Count();
                    var totalCount = context.Trn_SCTransBatch.Where(i => i.Id == BatchId).Select(i => i.RecordCountTotal).SingleOrDefault();
                   // transBatchDetails.DeletedRecordCount = count;
                    transBatchDetails.RecordCountSAPError = Convert.ToInt32(totalCount) - count; 
                    context.SaveChanges();
                }
                return true;
            }
        }

        private int SCLineCount(int batchId, int statusType)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = null;
                switch (statusType)
                {
                    case 0: statusCode = LineStatus.SUCCESS.ToString();
                        break;
                    case 1: statusCode = LineStatus.ERROR.ToString();
                        break;
                }
                int statusId = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                int Count = (from s in context.Trn_SCTransLine
                             where ((s.StatusId == statusId) && (s.BatchId == batchId))
                             select s).ToList().Count;
                return Count;
            }

        }


        private bool Validate_IsFileTypeSupported(SCTransactionModel objGPCustomerMessage)
        {
            //objILog.Debug("Entering Validate_IsFileTypeSupported method");
            bool blnValidFileType = false;
            try
            {
                FILE_TYPES fileType;
                blnValidFileType = this.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                if (blnValidFileType == false)
                {
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    //objILog.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                }
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager Validate_IsFileTypeSupported method.objGPCustomerMessage:" + objGPCustomerMessage + ". Api Exception : Message- " + exp.Message);
                throw exp;
            }
            //objILog.Debug("Leaving Validate_IsFileTypeSupported method");
            return blnValidFileType;
        }

        public bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
        {
            bool blnValidFileType = false;
            //get value from db file types table
            IList<string> lstFileExtns = new List<string>();
            lstFileExtns.Add(".txt");
            lstFileExtns.Add(".csv");
            lstFileExtns.Add(".xls");
            lstFileExtns.Add(".xlsx");
            enumFileType = FILE_TYPES.CSV;
            foreach (string strFileExt in lstFileExtns)
            {
                if (strFileName.ToLower().EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
                {
                    if (strFileExt.ToLower() == ".csv")
                        enumFileType = FILE_TYPES.CSV;
                    else
                        enumFileType = FILE_TYPES.TEXT;
                    blnValidFileType = true;
                    break;
                }
            }
            return blnValidFileType;
        }

        public IList<ResponseModel> SubmitData(IList<GenericItemModel> itemModel, string username, TRANSACTION_TYPES transType)
        {
            IList<ResponseModel> responseData = new List<ResponseModel>();
            log.Info("Submit Data call");
            ITirmTripManager termtrip=new TirmTripManager();
            try
            {
                switch (transType)
                {
                    case TRANSACTION_TYPES.SC:                        
                        responseData = SCSubmit(itemModel, username);
                        break;
                    case TRANSACTION_TYPES.SSC:
                        responseData = SSCSubmit(itemModel, username);
                        break;
                    case TRANSACTION_TYPES.ASN:
                        responseData = ASNSubmit(itemModel, username);
                        break;
                }


              //  scTransBatch.UpdateSAPTransBatch(objGPCustomerMessage.BatchID, BatchStatus.SUCCESS, false);
            }
            catch (Exception e)
            {
                log.Error("Transaction Data manager Exception in SubmitData method.Transaction type :" + transType + ", Item Model:" + itemModel + ", Username:" + username);
                log.Error("Api Exception Message:" + e.Message + ",Inner Exception:" + e.InnerException);
            }

            return responseData; ;
        }

        public IList<Trn_MapperFieldSpecs> ValidateData(string fileName)
        {
            using (var context = new GoodpackEDIEntities())
            {
                IList<Trn_MapperFieldSpecs> mapperFields = (from s in context.Trn_SubsiToTransactionFileMapper
                                                            join p in context.Trn_MapperFieldSpecs on s.Id equals p.SubsiToTransactionMapperId
                                                            where (s.Filename == fileName)
                                                            select p).ToList();

                return mapperFields;
            }

        }

       public IList<ResponseModel> SCSubmit(IList<GenericItemModel> itemModel, string username)
        {
            IList<ResponseModel> responseData = new List<ResponseModel>();
            ResponseModel response;
            bool isTermTrip = false;
            int successCount = 0, errorCount = 0;
            sapSubmitTransData = itemModel[0].sapResponseData;
            //j needs to start from 2 to avoid the headers in the model
            for (int j = 2; j < itemModel.Count; j++)
            {
                if (itemModel[j].Status == "SUCCESS")
                {
                    int lineID = itemModel[j].LineId;
                    int batchId;
                    using (var dataContext = new GoodpackEDIEntities())
                    {
                        batchId = (from s in dataContext.Trn_SCTransLineContent
                                   where s.LineID == lineID
                                   select s.BatchID).FirstOrDefault();
                    }
                    SCServiceManager serviceCaller = new SCServiceManager();
                    Dictionary<string, string> sapData = sapSubmitTransData[lineID];
                    List<ResponseModel> responseModel = null;
                    try
                    {
                        ITirmTripManager terntrip = new TirmTripManager();
                        isTermTrip = terntrip.IsTirmTripOrNot(sapData);
                        sapData[SCSapMessageFields.ReferenceITRNumber] = isTermTrip ? "" : sapData[SCSapMessageFields.ReferenceITRNumber];
                        responseModel = serviceCaller.scServiceCaller(sapData, null, ref successCount, ref errorCount, username);
                        
                    }
                    catch (Exception e)
                    {
                        log.Error("SCSubmit Method" + e);
                        responseModel = new List<ResponseModel>();
                        ResponseModel model = new ResponseModel();
                        model.Success = false;
                        model.Message = "Error while contacting SAP";
                        responseModel.Add(model);
                    }
                    if (responseModel[0].Success)
                    {
                        log.Info("Response was successful for line Id:" + lineID);
                        scTransBatch.UpdateTransBatch(batchId, successCount, errorCount, true, isTermTrip);
                        scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.SUCCESS, lineID, responseModel, isTermTrip);
                        scTransBatch.UpdateSAPTransLine(LineStatus.SUCCESS, lineID, false, responseModel);
                        scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));
                    }
                    else
                    {
                        log.Info("Response was error for line Id:" + lineID);
                        scTransBatch.UpdateTransBatch(batchId, successCount, errorCount, true, isTermTrip);
                        scTransBatch.UpdateSAPTranLineContent(batchId, LineStatus.ERROR, lineID, responseModel, isTermTrip);
                        scTransBatch.UpdateSAPTransLine(LineStatus.ERROR, lineID, false, responseModel);
                        scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));

                    }
                    foreach (var item in responseModel)
                    {
                        response = new ResponseModel();
                        response.Success = item.Success;
                        response.Message = isTermTrip && (item.Success) ? "Need to wait for sometime & check main grid for status" : item.Message;
                        responseData.Add(response);
                    }
                }
            }
            return responseData;

        }

       public IList<ResponseModel> ASNSubmit(IList<GenericItemModel> itemModel, string username)
       {
           IList<ResponseModel> responseData = new List<ResponseModel>();
           ResponseModel response;
           int successCount = 0, errorCount = 0;
           sapSubmitTransData = itemModel[0].sapResponseData;
           //j needs to start from 2 to avoid the headers in the model
           for (int j = 2; j < itemModel.Count; j++)
           {
               if (itemModel[j].Status == "SUCCESS")
               {
                   int lineID = itemModel[j].LineId;
                   int batchId;
                   using (var dataContext = new GoodpackEDIEntities())
                   {
                       batchId = (from s in dataContext.Trn_ASNTransLineContent
                                  where s.LineID == lineID
                                  select s.BatchID).FirstOrDefault();
                   }
                   SCServiceManager serviceCaller = new SCServiceManager();
                   Dictionary<string, string> sapData = sapSubmitTransData[lineID];
                   List<ResponseModel> responseModel = null;
                   try
                   {
                       responseModel = serviceCaller.scServiceCaller(sapData, null, ref successCount, ref errorCount, username);

                   }
                   catch (Exception e)
                   {
                       log.Error("ASNSubmit Method" + e);
                       responseModel = new List<ResponseModel>();
                       ResponseModel model = new ResponseModel();
                       model.Success = false;
                       model.Message = "Decode value might be missing";
                       responseModel.Add(model);
                   }                   
                   if (responseModel[0].Success)
                   {
                       log.Info("Response was successful for line Id:" + lineID);
                       scTransBatch.UpdateASNTransBatch(batchId, successCount, errorCount, true);
                      // scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));
                       scTransBatch.UpdateSAPASNTranLineContent(batchId, LineStatus.SUCCESS, lineID, responseModel);
                       scTransBatch.UpdateSAPASNTransLine(LineStatus.SUCCESS, lineID, false);
                   }
                   else
                   {
                       log.Info("Response was error for line Id:" + lineID);
                       scTransBatch.UpdateASNTransBatch(batchId, successCount, errorCount, true);
                      // scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));
                       scTransBatch.UpdateSAPASNTranLineContent(batchId, LineStatus.ERROR, lineID, responseModel);
                       scTransBatch.UpdateSAPASNTransLine(LineStatus.ERROR, lineID, false);

                   }
                   foreach (var item in responseModel)
                   {
                       response = new ResponseModel();
                       response.Success = item.Success;
                       response.Message = item.Message;
                       responseData.Add(response);
                   }
               }
           }
           return responseData;

       }

        private IList<ResponseModel> SSCSubmit(IList<GenericItemModel> itemModel, string username)
        {

            IList<ResponseModel> responseData = new List<ResponseModel>();
            ResponseModel response;
            Dictionary<int, Dictionary<string, string>> successData = new Dictionary<int, Dictionary<string, string>>();
            int successCount = 0, errorCount = 0;
            SSCServiceManager sscServiceCaller = new SSCServiceManager();
            //j needs to start from 2 to avoid the headers in the model
            for (int j = 2; j < itemModel.Count; j++)
            {
                if (itemModel[j].Status == "SUCCESS")
                {
                    int lineID = itemModel[j].LineId;

                    successData.Add(lineID, sapSubmitTransData[lineID]);
                }
            }
            if (successData.Count != 0)
            {
                Dictionary<int, Dictionary<string, string>> data = new Dictionary<int, Dictionary<string, string>>();
                //Cloning data to another dictionary for iterating
                foreach (var item in successData)
                {
                    data.Add(item.Key, item.Value);
                }
                // data = successData.ToDictionary(entry => entry.Key, entry => entry.Value);
                string customerRefNumber = null;
                //sscData = data.Where(i => i.Value.Where(j=>j.Key[SCSapMessageFields.CustomerReferenceNumber] == customerRefNumber);
                bool isFirstVal = true;

                while (data.Count != 0)
                {
                    List<int> sscKeys = new List<int>();
                    List<Dictionary<string, string>> sscDataVal = new List<Dictionary<string, string>>();
                    foreach (var item in data)
                    {
                        if (isFirstVal)
                        {
                            customerRefNumber = item.Value[SCSapMessageFields.CustomerReferenceNumber];
                            isFirstVal = false;
                        }

                        if (customerRefNumber == item.Value[SCSapMessageFields.CustomerReferenceNumber])
                        {
                            int lineId = item.Key;
                            sscKeys.Add(item.Key);
                            using (var context = new GoodpackEDIEntities())
                            {
                                string ediReference = (from s in context.Trn_SSCTransLine
                                                       where s.Id == lineId
                                                       select s.EDIReference).FirstOrDefault();
                                if(!item.Value.ContainsKey(SCSapMessageFields.EdiReference))
                                item.Value.Add(SCSapMessageFields.EdiReference, ediReference);
                            }
                            sscDataVal.Add(item.Value);
                        }

                    }
                    List<ResponseModel> responseModel = null;
                    try
                    {
                        responseModel = sscServiceCaller.sscServiceCaller(sscDataVal, null, ref successCount, ref errorCount, username);
                    }
                    catch (Exception e)
                    {
                        log.Error("SSCSubmit Method" + e);
                        responseModel = new List<ResponseModel>();
                        ResponseModel model = new ResponseModel();
                        model.Success = false;
                        model.Message = "Decode value might be missing";
                        responseModel.Add(model);
                    }
                    foreach (var item in sscKeys)
                    {
                        if (responseModel[0].Success)
                        {
                            log.Info("Response was successful for line Id:" + item);
                            scTransBatch.UpdateSAPSSCTranLineContent(int.Parse(itemModel[0].BatchId), LineStatus.SUCCESS, item, responseModel);
                            scTransBatch.UpdateSAPSSCTransLine(LineStatus.SUCCESS, item, false);
                        }
                        else
                        {
                            log.Info("Response was error for line Id:" + item);
                            scTransBatch.UpdateSAPSSCTranLineContent(int.Parse(itemModel[0].BatchId), LineStatus.ERROR, item, responseModel);
                            scTransBatch.UpdateSAPSSCTransLine(LineStatus.ERROR, item, false);

                        }
                        foreach (var items in responseModel)
                        {
                            response = new ResponseModel();
                            response.Success = items.Success;
                            response.Message = items.Message;
                            responseData.Add(response);
                        }
                    }

                    foreach (var val in sscKeys)
                    {
                        data.Remove(val);
                    }
                    isFirstVal = true;
                }
            }
            return responseData;
        }

        public IList<GenericItemModel> verifyASNData(string mapperFile, string username, string subsiId, IList<GenericItemModel> itemModel, bool isFileData)
        {
            try
            {
                GenericItemModel itemModelVal = itemModel[1];
                objGPCustomerMessage = new SCTransactionModel();
                List<string> dataValue;
                List<string> fileName;
                List<string> filePath;
                List<string> message;
               // List<string> batchId;
                dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
                fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
                filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
                message = itemModel.Select(x => x.Message).Distinct().ToList();
               // batchId = itemModel.Select(x => x.BatchId).Distinct().ToList();           
                objGPCustomerMessage.BatchFileName = fileName[0].ToString();
                objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
                objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
               // objGPCustomerMessage.BatchID = batchId[0] == null ? 0 : int.Parse( batchId[0].ToString());

                if (isFileData)
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                    itemModel.RemoveAt(1);
                    using (var context = new GoodpackEDIEntities())
                    {
                        objGPCustomerMessage.EmailId = (from s in context.Gen_User
                                                        where s.Username == username
                                                        select s.EmailId).FirstOrDefault();
                    }
                }
                else
                {
                    objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
                }
                using (var context = new GoodpackEDIEntities())
                {
                    int userId = (from s in context.Gen_User
                                  where s.Username == username
                                  select s.Id).FirstOrDefault();
                    objGPCustomerMessage.UserId = userId;

                    objGPCustomerMessage.TransactionType = (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), (from s in context.Trn_SubsiToTransactionFileMapper
                                                                                                                     join e in context.Ref_TransactionTypes
                                                                                                                     on s.TransactionCodeId equals e.Id
                                                                                                                     where s.Filename == mapperFile
                                                                                                                     select e.TransactionCode).FirstOrDefault());
                }

                objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                objGPCustomerMessage.DateCreated = DateTime.Now;
                objGPCustomerMessage.Service = "GPMapper";
                objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);

                // bool processOneLineOnly = false;
                List<int> trnsLineItems = new List<int>();
                // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
                GPMapper mapper = new GPMapper();
                MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
                List<string> ediNumber = new List<string>();

                if (objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.FC && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.ACI
                       && objGPCustomerMessage.TransactionType != TRANSACTION_TYPES.BP)
                {
                    //Insert lines in Trn_TransBatch table                    
                    objGPCustomerMessage.StatusCode = BatchStatus.PROCSD;
                    objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(objGPCustomerMessage, mapperFile);
                    itemModel[0].BatchId = objGPCustomerMessage.BatchID.ToString();
                    int i = 0;
                    // Update/Insert lines in TRANS_LINE table
                    foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
                    {
                        objLineReferences.StatusCode = LineStatus.NEW;
                        string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                        string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                        ediNumber.Add(objLineReferences.EdiNumber);
                        int transID = 0;
                        //  transID=scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null,customerReference);
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: transID = scTransBatch.InsertTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                break;
                            case TRANSACTION_TYPES.SSC: transID = scTransBatch.InsertSSCTransLine(objGPCustomerMessage, objLineReferences, null, customerReference, itrNumber);
                                break;
                            case TRANSACTION_TYPES.ASN: transID = scTransBatch.InsertASNTransLine(objGPCustomerMessage, objLineReferences, null, customerReference);
                                break;
                        }
                        trnsLineItems.Add(transID);
                        i++;
                    }
                }

                // Update status of transaction to processed.
                objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                objGPCustomerMessage.ErrorCode = ErrorCodes.NONE;
                objGPCustomerMessage.ErrorDesc = string.Empty;
                log.Info("Updated SC transbatch table with status code:" + BatchStatus.IN_SAP.ToString());
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                  0, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");


                if (objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.FC || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.ACI
                    || objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.BP)
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.CLOSED;
                }
                else
                {
                    objGPCustomerMessage.StatusCode = BatchStatus.IN_SAP;
                    // Update TRANS_LINE status
                    log.Info("Updating transLine table with status code:" + BatchStatus.IN_SAP.ToString() + "of Batch Id:" + objGPCustomerMessage.BatchID);
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                        case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                        case TRANSACTION_TYPES.ASN: scTransBatch.UpdateASNTransLine(objGPCustomerMessage.BatchID, objGPCustomerMessage.SubsiId, LineStatus.SUBMITTED);
                            break;
                    }

                    Dictionary<int, List<ResponseModel>> serviceResponse = new Dictionary<int, List<ResponseModel>>();
                    sapSubmitTransData = new Dictionary<int, Dictionary<string, string>>();
                    int successCount = 0;
                    int errorCount = 0;
                    for (int i = 0; i < objGPCustomerMessage.mappingServiceValues.Count; i++)
                    {
                        trLineID = trnsLineItems[i];
                        log.Info("Updating TransLine table with excel data");
                        switch (objGPCustomerMessage.TransactionType)
                        {
                            case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                            case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                            case TRANSACTION_TYPES.ASN: scTransBatch.UpdateASNTransLineForData(trLineID, objGPCustomerMessage.mappingServiceValues[i]);
                                break;
                        }

                        try
                        {
                            List<ResponseModel> responseModel = null;
                            SCServiceManager serviceCaller = new SCServiceManager();
                            SSCServiceManager sscCaller = new SSCServiceManager();
                            log.Info("Calling SC webservice");
                            if (isFileData)
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.ASN:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username);
                                        break;
                                    //case TRANSACTION_TYPES.SSC:
                                    //    responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], ServiceModeTypes.TEST, ref successCount, ref errorCount, username, ediNumber[i]);
                                    //    break;
                                }
                            }
                            else
                            {
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.ASN:
                                        responseModel = serviceCaller.scServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref successCount, ref errorCount, objGPCustomerMessage.EmailId);
                                        break;
                                    //    case TRANSACTION_TYPES.SSC:
                                    //responseModel = sscCaller.sscServiceCaller(objGPCustomerMessage.mappingServiceValues[i], null, ref  successCount, ref errorCount, objGPCustomerMessage.EmailId, ediNumber[i]);
                                    //break;
                                }
                            }


                            serviceResponse.Add(trLineID, responseModel);

                            log.Info("Inserting into TransLineContent table of transLineId:-" + trLineID);
                            bool isSuccess = false;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    isSuccess = scTransBatch.InsertTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;

                                case TRANSACTION_TYPES.SSC:
                                    isSuccess = scTransBatch.InsertSSCTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;
                                case TRANSACTION_TYPES.ASN:
                                    isSuccess = scTransBatch.InsertASNTransLineContent(trLineID, objGPCustomerMessage.BatchID, responseModel);
                                    break;
                            }
                            log.Info("Insertion of trans line content success status:" + isSuccess);
                            if (isSuccess)
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.SUCCESS.ToString());
                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.ASN: scTransBatch.UpdateASNResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                }
                            }
                            else
                            {
                                log.Info("Line status of trnLineId:" + trLineID + " is :" + LineStatus.ERROR.ToString());

                                switch (objGPCustomerMessage.TransactionType)
                                {
                                    case TRANSACTION_TYPES.SC:
                                        scTransBatch.UpdateResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.ERROR, trLineID, isFileData);
                                        break;
                                    case TRANSACTION_TYPES.ASN: scTransBatch.UpdateASNResponseTransLine(objGPCustomerMessage.BatchID, LineStatus.SUCCESS, trLineID, isFileData);
                                        break;
                                }
                            }
                            sapSubmitTransData.Add(trLineID, objGPCustomerMessage.mappingServiceValues[i]);

                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC: scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC: scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.ASN: scTransBatch.UpdateASNTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }
                        }
                        catch (Exception e)
                        {
                            log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + e.Message);
                            // IList<GenericItemModel> genModel = new List<GenericItemModel>();
                            List<ResponseModel> responseModel = new List<ResponseModel>();
                            ResponseModel model = new ResponseModel();
                            model.Success = false;
                            // model.Message = "Exception Message :" + e.Message;    
                            model.Message = "Decode value might be missing.Please check the decode value for the mapper " + mapperFile;
                            responseModel.Add(model);
                            errorCount++;
                            // GenericItemModel itemsModel = new GenericItemModel();
                            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                            objGPCustomerMessage.ErrorDesc = e.Message;
                            switch (objGPCustomerMessage.TransactionType)
                            {
                                case TRANSACTION_TYPES.SC:
                                    SCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                case TRANSACTION_TYPES.SSC:
                                    SSCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateSSCTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                                     case TRANSACTION_TYPES.ASN:
                                    SSCTransactionData(responseModel, isFileData);
                                    scTransBatch.UpdateASNTransBatch(objGPCustomerMessage.BatchID, successCount, errorCount, isFileData);
                                    break;
                            }

                            serviceResponse.Add(trLineID, responseModel);
                        }
                    }
                }
                //  int iCountSentToSAP = 0;
                //if (objGPCustomerMessage.StatusCode == BatchStatus.IN_SAP)
                //    iCountSentToSAP = objGPCustomerMessage.RecordCountTotal - objGPCustomerMessage.RecordCountSkipValFailure - objGPCustomerMessage.RecordCountFieldValFailure;
                //below code commented by abina
                //scTransBatch.UpdateSCBatchCounts(objGPCustomerMessage.BatchID, objGPCustomerMessage.RecordCountTotal, objGPCustomerMessage.RecordCountSkipValFailure, objGPCustomerMessage.RecordCountFieldValFailure,
                //    iCountSentToSAP, objGPCustomerMessage.RecordCountSAPError, objGPCustomerMessage.RecordCountSuccess, "Request");
                IList<GenericItemModel> genericModel = null;
                if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper))
                {
                    genericModel = scTransBatch.ScTestResultGoodyear(itemModel, trnsLineItems);
                }
                else
                {
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SC:
                            genericModel = scTransBatch.ScTestResult(itemModel, trnsLineItems);
                            break;
                        // case TRANSACTION_TYPES.SSC:
                        //    genericModel= scTransBatch.SScTestResult(itemModel, trnsLineItems);
                        //break;
                       case TRANSACTION_TYPES.ASN: genericModel= scTransBatch.ASNTestResult(itemModel, trnsLineItems);
                        break;
                    }
                }

                if (genericModel.Count > 0)
                {
                    genericModel[0].sapResponseData = sapSubmitTransData;
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }

                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
                placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
                placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
                placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
                placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
                if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
                {
                    //send email to customer and internal users of goodpack
                    //emailid of customer get from batchfilesource address
                    string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
                    List<Attachment> attachments1 = new List<Attachment>();
                    string strFileSC;
                    strFileSC = objGPCustomerMessage.BatchFileSourceAddress;
                    strFileSC += "\\" + objGPCustomerMessage.BatchFileName;

                    Attachment attachment1 = new Attachment(strFileSC);
                    attachment1.Name = objGPCustomerMessage.BatchFileName;
                    attachments1.Add(attachment1);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);


                    GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SC_EMAIL", attachments1);
                    List<Attachment> attachments2 = new List<Attachment>();
                    Attachment attachment2 = new Attachment(strFileSC);
                    attachment2.Name = objGPCustomerMessage.BatchFileName;
                    attachments2.Add(attachment2);
                    log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                    GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SC_EMAIL", attachments2);
                }
                else
                {
                    if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSCMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
                    {
                        //goodyear notification
                        List<Attachment> attachments = new List<Attachment>();
                        string strGoodyearSC;
                        strGoodyearSC = objGPCustomerMessage.BatchFileSourceAddress;
                        strGoodyearSC += "\\" + objGPCustomerMessage.BatchFileName;
                        Attachment attachment = new Attachment(strGoodyearSC);
                        attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
                        attachments.Add(attachment);
                        log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SC_CUSTOMER", attachments);
                    }
                    else
                    {
                        //fileupload SO
                        //send email to users
                        GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SC_FILE_UPLOAD", null);
                    }
                }
                log.Info("Email send to customer" + userDetails.FullName.ToString());
                for (int i = 0; i < genericModel.Count; i++)
                {
                    itemModelData.Add(genericModel[i]);
                    if (i == 0)
                    {
                        itemModelData.Add(itemModelVal);
                    }
                }
                log.Info("Leaving from verifyData in SCManager");
                return itemModelData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp.Message);
                IList<GenericItemModel> genericModel = new List<GenericItemModel>();
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
                objGPCustomerMessage.ErrorDesc = exp.Message;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";
                // Update TRANS_BATCH
                genericModel.Add(itemsModel);
                scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
                Dictionary<string, string> placeholder = new Dictionary<string, string>();
                // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
                placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
                GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
                return genericModel;

            }
            //log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
            //log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
            //log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
            //objILog.Error("Error while processing in ProcessBatch", exp);

        }

        public string GetDataWithoutEmptyColumns(string scData)
        {
            bool firstItem = true;
            string returnValue = "";
            int indexValue = 0;
            List<String> lines = new List<string>();
            string[] lineArray = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            foreach (string linedata in lineArray)
            {
                if (linedata != "")
                {
                    if (firstItem)
                    {
                        if (linedata.LastIndexOf('|') + 1 == linedata.Length)
                        {
                            if (linedata.LastIndexOf("||") + 2 == linedata.Length)
                            {
                                string tempLine = linedata.Replace("||", "");
                                string lineValue = (tempLine.LastIndexOf('|') + 1 == tempLine.Length) ? tempLine.Remove(tempLine.LastIndexOf('|')) : tempLine;
                                returnValue = returnValue + lineValue.Trim() + Environment.NewLine;
                                indexValue = lineValue.Split('|').Length;

                            }
                            else
                            {
                                returnValue = returnValue + linedata.Remove(linedata.LastIndexOf('|')).Trim() + Environment.NewLine;
                                indexValue = linedata.Remove(linedata.LastIndexOf('|')).Split('|').Length;
                            }
                            firstItem = false;
                        }
                        else
                        {
                            returnValue = returnValue + linedata.ToString().Trim() + Environment.NewLine;
                            indexValue = linedata.Split('|').Length;
                            firstItem = false;
                        }
                    }
                    else
                    {
                        string[] linedetails;
                        linedetails = linedata.Split('|').Select(e => e.Replace(" \"", "\"")).ToArray();
                        returnValue = returnValue + string.Join("|", linedetails, 0, indexValue) + Environment.NewLine;
                    }
                }
            }
            return returnValue;
        }

        public IList<ResponseLineDataModel> ReSubmit(string transactionType, string username, int batchId, int status,bool SubmitActionType)
        {
            string val = null;
            string mapperName=null;
            bool isTermTrip = false;
            string statusValue =null;
            List<int> lineIds = new List<int>();  
            Dictionary<string, string> decodeValueStore = new Dictionary<string, string>();         
            try
            {               
                int successCount = 0;
                int errorCount = 0;
                ResponseLineDataModel lineData = null;
                statusValue = (status == 2 || status == 0) ? "ERROR" : "SUCCESS";
                switch (status)
                {
                    case 0: val = null; break;
                    case 1: val = null; break;
                    case 2: val = ""; break;
                    default: return new List<ResponseLineDataModel>();
                }
                IList<ResponseLineDataModel> allLineData = new List<ResponseLineDataModel>();
                using (var context = new GoodpackEDIEntities())
                {                  
                    bool isSuccess = true;
                    bool isTestMode = true;                                 
                    switch(transactionType)
                    {
                        case "SO":
                            lineIds=(from s in context.Trn_SOTransLine
                                     where s.BatchId == batchId && s.Trn_LineStatus.StatusCode == statusValue && s.InTestMode == SubmitActionType
                                     select s.Id).ToList();
                            break;
                        case "SC":
                              lineIds=(from s in context.Trn_SCTransLine
                                       where s.BatchId == batchId && s.Trn_LineStatus.StatusCode == statusValue && s.InTestMode == SubmitActionType
                                     select s.Id).ToList();
                            break;
                    }
                    transactionType = (transactionType == "SC") ? TransactionTypes.SC : (transactionType == "SO") ? TransactionTypes.SO : TransactionTypes.ASN;
                    mapperName=(from s in context.Trn_SCTransBatch
                            where s.Id==batchId 
                            select s.MapperFileName).FirstOrDefault();                   
                    int transId = (from s in context.Ref_TransactionTypes
                                   where s.TransactionName == transactionType
                                   select s.Id).FirstOrDefault();
                    IList<Ref_SAPMessageFields> sapFields = (from s in context.Ref_SAPMessageFields
                                                             where s.TransactionId == transId
                                                             select s).ToList();                   
                    if (transactionType == TransactionTypes.SC)
                    {

                        for (int i = 0; i < lineIds.Count; i++)
                        {
                            int lineVal = lineIds[i];
                            var itrNumber = "10";
                            lineData = new ResponseLineDataModel();                           
                            Trn_SCTransLine lines = (from s in context.Trn_SCTransLine
                                                     where s.Id == lineVal
                                                     select s).FirstOrDefault();
                            IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                            lines = scLineContentParsingNew(lines, sapFields, mapperName);
                            context.SaveChanges();
                            log.Info("-----------Resubmit data caller-------------");
                            log.Info("Service calling for Line Id:" + lines.Id);
                            List<ResponseModel> responseModelData = new List<ResponseModel>();                          
                            ITirmTripManager termTrip = new TirmTripManager();      
                            responseModelData = serviceCallingSC(lineIds, i, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess);
                            try
                            {
                                isTermTrip = termTrip.IsTirmTripORNot(lines.CustomerCode.Length > 6 ? lines.CustomerCode.Substring(4, 6) : lines.CustomerCode);
                            }
                            catch(Exception e)
                            {
                                log.Error("Resubmit data error -" + e);
                            }
                            isSuccess = responseModelData[0].Success;
                            if (responseModelData[0].ResponseItems != null)
                                itrNumber = responseModelData[0].ResponseItems.ToString();
                            lineData = updateSCtransLine(val, isTestMode, isSuccess, status, batchId, lineIds, i, lines, isTermTrip, itrNumber, responseModelData[0].Message);
                            if (isTermTrip && lineData.Status)
                            {
                                List<string> responseMessage=new List<string>();
                                responseMessage.Add("Need to wait for sometime & check main grid for status");
                                lineData.LineMessage = responseMessage;  
                            }
                            allLineData.Add(lineData);
                        }
                    }
                    else if (transactionType == TransactionTypes.ASN)
                    {

                        for (int i = 0; i < lineIds.Count; i++)
                        {
                            int lineVal = lineIds[i];
                            lineData = new ResponseLineDataModel();                         
                            Trn_ASNTransLine lines = (from s in context.Trn_ASNTransLine
                                                      where s.Id == lineVal
                                                      select s).FirstOrDefault();
                            IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                            responseLineModelData = asnLineContentParsing(lines, sapFields, mapperName);
                            context.SaveChanges();
                            log.Info("-----------Resubmit data caller-------------");
                            log.Info("Service calling for Line Id:" + lines.Id);
                            List<ResponseModel> responseModelData = new List<ResponseModel>();
                            responseModelData = serviceCallingASN(lineIds, i, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess);
                            isSuccess = responseModelData[0].Success;
                            lineData = updateASNtransLine(val, isTestMode, isSuccess, status, batchId, lineIds, i);
                            allLineData.Add(lineData);
                        }
                    }

                    else if (transactionType == TransactionTypes.SSC)
                    {
                        List<int> lineValues;
                        List<int> lineNos;
                        lineValues = lineIds;
                        while (lineValues.Count != 0)
                        {
                            Dictionary<int, string> CustrefValues = new Dictionary<int, string>();
                            int line = lineValues[0];
                            for (int i = 0; i < lineValues.Count; i++)
                            {
                                int lineVal = lineValues[i];
                                Trn_SSCTransLine lines = (from s in context.Trn_SSCTransLine
                                                          where s.Id == lineVal
                                                          select s).FirstOrDefault();

                                CustrefValues.Add(lineValues[i], lines.CustomerReferenceNumber);

                            }
                            List<int> Custref = new List<int>();
                            List<string> CustrefValue = new List<string>();

                            foreach (KeyValuePair<int, string> pair in CustrefValues)
                            {
                                CustrefValue.Add(pair.Value);
                            }
                            foreach (KeyValuePair<int, string> pair in CustrefValues)
                            {
                                if (pair.Value == CustrefValue[0])
                                {
                                    Custref.Add(pair.Key);
                                }
                            }

                            lineNos = Custref;
                            if (lineNos.Count > 0)
                            {                              
                                int lineVal = lineNos[0];
                                lineData = new ResponseLineDataModel();
                          
                                Trn_SSCTransLine lines = (from s in context.Trn_SSCTransLine
                                                          where s.Id == lineVal
                                                          select s).FirstOrDefault();
                                IList<ResponseLineDataModel> responseLineModelData = new List<ResponseLineDataModel>();
                                responseLineModelData = sscLineContentParsing(lines, sapFields, mapperName);
                                context.SaveChanges();
                                log.Info("-----------Resubmit data caller-------------");
                                log.Info("Service calling for Line Id:" + lines.Id);
                                IList<ResponseModel> responseModelData = new List<ResponseModel>();
                                responseModelData = serviceCallingSSC(lineNos, 0, lines, val, ref successCount, ref errorCount, username, batchId, isSuccess, lines.MvtType);
                                isSuccess = responseModelData[0].Success;
                                lineData = updateSSCtransLine(val, isTestMode, isSuccess, status, batchId, lineNos, 0);
                                allLineData.Add(lineData);
                                lineValues.RemoveAll(j => j == lineVal);                            
                            }
                        }
                    }
                    if (transactionType == TransactionTypes.SC)
                    {
                        //scTransBatch.UpdateTransBatch(batchId, successCount, errorCount, isTestMode);
                        scTransBatch.UpdateTransBatch(batchId, successCount, errorCount, isTestMode, isTermTrip);
                        scTransBatch.UpdateSCBatchCounts(batchId, SCLineCount(batchId, 1), SCLineCount(batchId, 0));
                    }
                    else if (transactionType == TransactionTypes.SSC)
                    {
                        scTransBatch.UpdateSSCTransBatch(batchId, successCount, errorCount, isTestMode);
                        scTransBatch.UpdateSCBatchCounts(batchId, SSCLineCount(batchId, 1), SSCLineCount(batchId, 0));
                    }
                }
                Gen_User userDetails;
                using (var dataContext = new GoodpackEDIEntities())
                {
                    userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
                }
                if (objGPCustomerMessage == null)
                {
                    objGPCustomerMessage = new SCTransactionModel();
                }
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
                placeholders.Add("$$BatchID$$", batchId.ToString());
                objGPCustomerMessage.EmailId = userDetails.EmailId;
                string[] recipientList = new string[] { userDetails.EmailId };
                GPTools.SendEmail(placeholders, getAllRecipients(mapperName), "NOTIFY_CUSTOMER", null);
                log.Info("Sending email to customer :" + userDetails.FullName.ToString());
                return allLineData;
            }
            catch (Exception exp)
            {
                log.Error(" fileUpload in SCManager ReSubmit method.mapperName:" + mapperName + ", username:" + username + ", batchId:" + batchId + ", lineIds:" + lineIds + ", status:" + status + ". Api Exception : Message- " + exp.Message);
                IList<ResponseLineDataModel> allLineData = new List<ResponseLineDataModel>();
                ResponseLineDataModel itemsModel = new ResponseLineDataModel();
                SCTransactionModel transModel = new SCTransactionModel();
                transModel.StatusCode = BatchStatus.ERROR;
                transModel.ErrorCode = ErrorCodes.E_GEN;
                transModel.ErrorDesc = exp.Message;
                transModel.BatchID = batchId;
                itemsModel.LineMessage = new List<string>();
                itemsModel.LineId = 0;
                itemsModel.Status = false;
                itemsModel.LineMessage.Add("Unable to process the Re-submit data.Please check decode values for the data");
                allLineData.Add(itemsModel);
            //Update TRANS_BATCH                
                scTransBatch.UpdateSCBatchFileData(transModel);
                Dictionary<string, string> placeholders = new Dictionary<string, string>();
                placeholders.Add("$$CustomerName$$", username);
                placeholders.Add("$$BatchID$$", batchId.ToString());             
                GPTools.SendEmail(placeholders, getAllRecipients(mapperName), "NOTIFY_CUSTOMER", null);
                return allLineData;
            }

        }

        public BatchStatusDetail GetBatchStatus(int BatchId, int TransactionType)
        {
            var SubmitSAPErrorData = false;
            var SubmitSuccessData = false;
            var SubmitErrorData = false;
            var DeleteLinedata = false;
            BatchStatusDetail batchDetail= new BatchStatusDetail();            
            using (var context = new GoodpackEDIEntities())
            { 
                switch (TransactionType)
                {
                    case 2:
                         //var dataValue = (from s in context.Trn_SOTransLine
                         //                where s.BatchId == BatchId
                         //                select s).ToList();
                        SubmitSAPErrorData = false;//dataValue.Where(i => i.Trn_LineStatus.StatusCode == "ERROR" && i.InTestMode == false).Count()>0 ? true : false;
                        SubmitSuccessData = false; //dataValue.Where(i => i.Trn_LineStatus.StatusCode == "SUCCESS" && i.InTestMode == true).Count() > 0 ? true : false;
                        SubmitErrorData = false; //dataValue.Where(i => i.Trn_LineStatus.StatusCode == "ERROR" && i.InTestMode == true).Count() > 0 ? true : false;
                        DeleteLinedata = false; //dataValue.Where(i => i.InTestMode == true).Count() > 0 ? true : false;
                        break;
                    case 3:
                        var dataValuenew = (from s in context.Trn_SCTransLine
                                         where s.BatchId == BatchId
                                         select s).ToList();
                        SubmitSAPErrorData = dataValuenew.Where(i => i.Trn_LineStatus.StatusCode == "ERROR" && i.InTestMode == false).Count() > 0 ? false : false;
                        SubmitSuccessData = dataValuenew.Where(i => i.Trn_LineStatus.StatusCode == "SUCCESS" && i.InTestMode == true).Count() > 0 ? true : false;
                        SubmitErrorData = dataValuenew.Where(i => i.Trn_LineStatus.StatusCode == "ERROR" && i.InTestMode == true).Count() > 0 ? false : false;
                        DeleteLinedata = dataValuenew.Where(i => i.InTestMode == true).Count() > 0 ? true : false;
                        break;
                }
                batchDetail.SubmitErrorData = SubmitErrorData;
                batchDetail.SubmitSAPErrorData = SubmitSAPErrorData;
                batchDetail.SubmitSuccessData = SubmitSuccessData;
            }
            return batchDetail; 
        }

        private string CheckValidDecode(Dictionary<string, string> dataVal)
        {
            StringBuilder returnVal=new StringBuilder(); 
            if(!dataVal[SCSapMessageFields.CustomerNumber].StartsWith("3") || (dataVal[SCSapMessageFields.CustomerNumber].Length!=6))
            {
                returnVal.Append("Customer does not have Decode value "+ Environment.NewLine);
            }
            if (!dataVal[SCSapMessageFields.FromLocation].StartsWith("5") || (dataVal[SCSapMessageFields.FromLocation].Length != 6))
            {
                returnVal.Append("Packing plant does not have Decode value " + Environment.NewLine);
            }
            if (!dataVal[SCSapMessageFields.ToLocation].StartsWith("6") || (dataVal[SCSapMessageFields.FromLocation].Length != 6))
            {
                returnVal.Append("Consignee does not have Decode value " + Environment.NewLine);
            }
            return returnVal.ToString();
        }
    }
}
