﻿//using GoodpackEDI.SCServiceReference1;  //QA pointing, 
//using GoodpackEDI.SCServiceReference1_PROD;
//using GoodpackEDI.SCWebReference;
using GoodpackEDI.SCWebReferenceNew;
using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace GoodpackEDI.BusinessLayer.SCManagementLayer
{
    public  class SCServiceManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SCServiceManager));

        public List<ResponseModel> scServiceCaller(Dictionary<string, string> scDataVal,string serviceTypes,ref int successCount,ref int errorCount,string username)
        {
            try
            {
                log.Info("SC Service calling. SC Data below");
                log.Info(scDataVal);
                string returnItrNo = null;
               // ZWS_SC_EDI_CREATE1Service client = new ZWS_SC_EDI_CREATE1Service();
                ZWS_EDI_SC_CREATE_3 client = new ZWS_EDI_SC_CREATE_3();
                client.Url = WebConfigurationManager.AppSettings["SCServiceReference1"];
                client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
                //client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["SCServiceReference1"]);
                //client.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
                //client.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;
                ZbapiScEdi scData = new ZbapiScEdi();
                scData.ZediScFormat = new ZediScFormat[1];
                scData.ZediScFormat[0] = new ZediScFormat();
                if (serviceTypes != null)
                    scData.Mode = serviceTypes;
                scData.ZediScFormat[0].Itrtype = scDataVal[SCSapMessageFields.DocumentType];
                scData.ZediScFormat[0].Itrno = scDataVal[SCSapMessageFields.ITRNumber];
                if ((scDataVal[SCSapMessageFields.ReferenceITRNumber] != ""))
                    scData.ZediScFormat[0].Refitrno = scDataVal[SCSapMessageFields.ReferenceITRNumber];
                if ((scDataVal[SCSapMessageFields.FromLocation] != ""))
                {
                    if (scDataVal[SCSapMessageFields.FromLocation].Length != 10)
                    {
                        scData.ZediScFormat[0].From = "0000" + scDataVal[SCSapMessageFields.FromLocation];
                    }
                    else
                    {
                        scData.ZediScFormat[0].From = scDataVal[SCSapMessageFields.FromLocation];
                    }
                }
                if ((scDataVal[SCSapMessageFields.ToLocation] != ""))
                {
                    if (scDataVal[SCSapMessageFields.ToLocation].Length != 10)
                    {
                        scData.ZediScFormat[0].To = "0000" + scDataVal[SCSapMessageFields.ToLocation];
                    }
                    else
                    {
                        scData.ZediScFormat[0].To = scDataVal[SCSapMessageFields.ToLocation];
                    }
                }
                //if ((scDataVal[SCSapMessageFields.Headertext] != ""))
                scData.ZediScFormat[0].Htext = username;

                if ((scDataVal[SCSapMessageFields.CustomerReferenceNumber] != ""))
                    scData.ZediScFormat[0].Custref = scDataVal[SCSapMessageFields.CustomerReferenceNumber];
                //if ((scDataVal[SCSapMessageFields.FlightBookingCodes] != ""))
                // scData.ZediScFormat[0].Booking=scDataVal[SCSapMessageFields.FlightBookingCodes];
                //if ((scDataVal[SCSapMessageFields.VesselETD] != ""))
                // scData.ZediScFormat[0].Vetd=scDataVal[SCSapMessageFields.VesselETD];
                //if ((scDataVal[SCSapMessageFields.VesselETA] != ""))
                // scData.ZediScFormat[0].Veta=scDataVal[SCSapMessageFields.VesselETA];
                //if ((scDataVal[SCSapMessageFields.VesselATD] != ""))
                // scData.ZediScFormat[0].Vatd=scDataVal[SCSapMessageFields.VesselATD];
                //if ((scDataVal[SCSapMessageFields.VesselATA] != ""))
                // scData.ZediScFormat[0].Vata=scDataVal[SCSapMessageFields.VesselATA];
                //if ((scDataVal[SCSapMessageFields.ShippingLine] != ""))
                // scData.ZediScFormat[0].Shipline=scDataVal[SCSapMessageFields.ShippingLine];
                //if ((scDataVal[SCSapMessageFields.VesselName] != ""))
                // scData.ZediScFormat[0].Vnam=scDataVal[SCSapMessageFields.VesselName];
                //if ((scDataVal[SCSapMessageFields.VoyageReference] != ""))
                // scData.ZediScFormat[0].Voyref=scDataVal[SCSapMessageFields.VoyageReference];
                //if ((scDataVal[SCSapMessageFields.POL] != ""))
                // scData.ZediScFormat[0].Pol=scDataVal[SCSapMessageFields.POL];
                //if ((scDataVal[SCSapMessageFields.POD] != ""))
                // scData.ZediScFormat[0].Pod=scDataVal[SCSapMessageFields.POD];    
                //if ((scDataVal[SCSapMessageFields.TranshipmentPort] != ""))
                // scData.ZediScFormat[0].Tport=scDataVal[SCSapMessageFields.TranshipmentPort];
                //if ((scDataVal[SCSapMessageFields.TranshipmentETD] != ""))
                // scData.ZediScFormat[0].Tetd=scDataVal[SCSapMessageFields.TranshipmentETD];
                //if ((scDataVal[SCSapMessageFields.TranshipmentETA] != ""))
                // scData.ZediScFormat[0].Teta=scDataVal[SCSapMessageFields.TranshipmentETA];
                if ((scDataVal[SCSapMessageFields.ItemNo] != ""))
                    scData.ZediScFormat[0].ImItem = scDataVal[SCSapMessageFields.ItemNo];
                if ((scDataVal[SCSapMessageFields.MaterialNumber] != ""))
                    scData.ZediScFormat[0].Matnr = scDataVal[SCSapMessageFields.MaterialNumber];
                if ((scDataVal[SCSapMessageFields.Quantity] != ""))
                    scData.ZediScFormat[0].Menge = scDataVal[SCSapMessageFields.Quantity];
                if ((scDataVal[SCSapMessageFields.ContainerNumber] != ""))
                    scData.ZediScFormat[0].ContNo = scDataVal[SCSapMessageFields.ContainerNumber];
                if ((scDataVal[SCSapMessageFields.CustomerNumber] != ""))
                {
                    if (scDataVal[SCSapMessageFields.CustomerNumber].Length != 10)
                    {
                        scData.ZediScFormat[0].CustNo = "0000" + scDataVal[SCSapMessageFields.CustomerNumber];
                    }
                    else
                    {
                        scData.ZediScFormat[0].CustNo = scDataVal[SCSapMessageFields.CustomerNumber];
                    }
                }
                if ((scDataVal[SCSapMessageFields.SINumber] != ""))
                    scData.ZediScFormat[0].CustPo = scDataVal[SCSapMessageFields.SINumber];
                if ((scDataVal[SCSapMessageFields.SalesDocument] != ""))
                    scData.ZediScFormat[0].Vbeln = scDataVal[SCSapMessageFields.SalesDocument];

                //If the sales document has value.Pass the default value as 10 - Mothy
                if ((scDataVal[SCSapMessageFields.SalesDocument] != ""))
                    scData.ZediScFormat[0].Posnr = ConstantUtilities.SalesDocumentItem;

                if ((scDataVal[SCSapMessageFields.ETDDate] != ""))
                {
                    string dateIssue = scDataVal[SCSapMessageFields.ETDDate];
                    if (dateIssue != null)
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        scData.ZediScFormat[0].ImEtddate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                        scData.ZediScFormat[0].Wadat = scData.ZediScFormat[0].ImEtddate;
                    }
                }
                if ((scDataVal[SCSapMessageFields.DTADate] != ""))
                {
                    string dateIssue = scDataVal[SCSapMessageFields.DTADate];
                    if (dateIssue != null)
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        scData.ZediScFormat[0].ImEtadate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                }
                if ((scDataVal[SCSapMessageFields.Remarks] != ""))
                    scData.ZediScFormat[0].Remarks = scDataVal[SCSapMessageFields.Remarks];
                //if ((scDataVal[SCSapMessageFields.Damage] != ""))
                // scData.ZediScFormat[0].Damagebins=scDataVal[SCSapMessageFields.Damage];
                //if ((scDataVal[SCSapMessageFields.SupplementFlag] != ""))
                // scData.ZediScFormat[0].Sendemail=scDataVal[SCSapMessageFields.SupplementFlag];
                // scData.ZediScFormat[0].BlNo=scDataVal[SCSapMessageFields];
                // scData.ZediScFormat[0].OldItrNumber=scDataVal[SCSapMessageFields];
                // scData.ZediScFormat[0].Status=scDataVal[SCSapMessageFields.];
                // scData.ZediScFormat[0].Migrate=scDataVal[SCSapMessageFields];
                //scData.ZediScFormat[0].Msg=;
                //scData.ZediScFormat[0].Hnote=;
                //scData.ZediScFormat[0].TruckNum=;
                scData.Result = new ZediScError[0];
                ZbapiScEdiResponse response = client.ZbapiScEdi(scData);
                int responseCount = response.Result.Count(); 
                List<ResponseModel> responseList = new List<ResponseModel>();
                ResponseModel responseModel;
                bool error = false;
                for (int i = 0; i < responseCount; i++)
                {
                    log.Info("From location :-" + scData.ZediScFormat[0].From + "\n ToLocation:-" + scData.ZediScFormat[0].To);
                    log.Info("Response from SC:-" + response.Result[i].Mtype);
                    log.Info("Message from SC:-" + response.Result[i].Mess);
                    returnItrNo = response.Result[i].Itrno;
                    if (response.Result[i].Mtype == "E")
                    {
                        if (!error)
                        {
                            errorCount++;
                            error = true;
                        }
                        responseModel = new ResponseModel();
                        responseModel.Success = false;
                        responseModel.Message = response.Result[i].Mess;
                        responseList.Add(responseModel);
                    }

                }

                if (responseList.Count == 0)
                {
                    if (!error)
                    {
                        successCount++;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = true;
                    if (serviceTypes != null)
                    {
                        responseModel.Message = "Successful verification of data in test mode";
                    }
                    else
                    {
                        responseModel.Message = "Successfully Created SC";
                    }
                    responseModel.ResponseItems = returnItrNo;
                    responseList.Add(responseModel);
                }
                return responseList;
            }
            catch(Exception e)
            {
                log.Error("SC service Error-" + e);
                throw e;
            }
        }      
        public List<ResponseModel> scResubmitCaller(Trn_SCTransLine scLine, string status, ref int successCount, ref int errorCount, string username)
        {
            log.Info("SC Resubmit service calling. SC Data below");
            log.Info(scLine);
            string returnItrNo = null;
          //  ZWS_SC_EDI_CREATE1Service client = new ZWS_SC_EDI_CREATE1Service();
            ZWS_EDI_SC_CREATE_3 client = new ZWS_EDI_SC_CREATE_3();
            client.Url = WebConfigurationManager.AppSettings["SCServiceReference1"];
            client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
            //client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["SCServiceReference1"]);
            //client.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
            //client.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;
            ZbapiScEdi scData = new ZbapiScEdi();
            scData.ZediScFormat = new ZediScFormat[1];
            scData.ZediScFormat[0] = new ZediScFormat();

            if (status != null)
            {
                scData.Mode = "X";
            }
            scData.ZediScFormat[0].Itrtype = scLine.ItrType;
            scData.ZediScFormat[0].Itrno = scLine.ItrNumber;
            scData.ZediScFormat[0].Refitrno = scLine.RefItrNumber;
            scData.ZediScFormat[0].From = scLine.FromLocation;
            scData.ZediScFormat[0].To = scLine.ToLocation;
            scData.ZediScFormat[0].Custref = scLine.CustomerRefNumber;
            scData.ZediScFormat[0].ImItem = scLine.ItemNumber;
            scData.ZediScFormat[0].Matnr = scLine.BinType;
            scData.ZediScFormat[0].Menge = scLine.Quantity.ToString();
            scData.ZediScFormat[0].CustNo = scLine.CustomerCode;
            scData.ZediScFormat[0].CustPo = scLine.SINumber;
            scData.ZediScFormat[0].ContNo = scLine.ContainerNumber;
            scData.ZediScFormat[0].Vbeln = scLine.SalesDocument;
            scData.ZediScFormat[0].Posnr = scLine.SalesDocumentItem;
            scData.ZediScFormat[0].Wadat = scLine.ETD;
            scData.ZediScFormat[0].ImEtddate = scLine.ETD;
            scData.ZediScFormat[0].ImEtadate = scLine.ETA;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
            scData.ZediScFormat[0].Remarks = scLine.Remarks;
            scData.ZediScFormat[0].Htext = username;

            scData.Result = new ZediScError[0];
            ZbapiScEdiResponse response = client.ZbapiScEdi(scData);
            int responseCount = response.Result.Count();
            List<ResponseModel> responseList = new List<ResponseModel>();
            ResponseModel responseModel;
            bool error = false;

            for (int i = 0; i < responseCount; i++)
            {
                log.Info("From location :-" + scData.ZediScFormat[0].From + "\n ToLocation:-" + scData.ZediScFormat[0].To);
                log.Info("Response from SC:-" + response.Result[i].Mtype);
                log.Info("Message from SC:-" + response.Result[i].Mess);
                returnItrNo = response.Result[i].Itrno;
                if (response.Result[i].Mtype == "E")
                {
                    if (!error)
                    {
                        errorCount++;
                        error = true;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = false;
                    responseModel.Message = response.Result[i].Mess;
                    responseList.Add(responseModel);
                }
            }
            if (responseList.Count == 0)
            {
                if (!error)
                {
                    successCount++;
                }
                responseModel = new ResponseModel();
                responseModel.Success = true;
                responseModel.ResponseItems = returnItrNo;
                if (status != null)
                {
                    responseModel.Message = "Successful verification of data in Test mode";
                }
                else
                {
                    responseModel.Message = "Successful verification of data in SAP mode";
                }
                responseList.Add(responseModel);
            }
            return responseList;
        }
        public List<ResponseModel> asnResubmitCaller(Trn_ASNTransLine scLine, string status, ref int successCount, ref int errorCount, string username)
        {
            log.Info("SC Resubmit service calling. SC Data below");
            log.Info(scLine);
           // ZWS_SC_EDI_CREATE1Service client = new ZWS_SC_EDI_CREATE1Service();
            ZWS_EDI_SC_CREATE_3 client = new ZWS_EDI_SC_CREATE_3();
            client.Url = WebConfigurationManager.AppSettings["SCServiceReference1"];
            client.Credentials = new System.Net.NetworkCredential(ConstantUtilities.SAPUsername, ConstantUtilities.SAPPassword);
            //ZWS_Sc_EDI_CREATE1Client client = new ZWS_Sc_EDI_CREATE1Client();
            //client.Endpoint.Address = new System.ServiceModel.EndpointAddress(WebConfigurationManager.AppSettings["SCServiceReference1"]);
            //client.ClientCredentials.UserName.UserName = ConstantUtilities.SAPUsername;
            //client.ClientCredentials.UserName.Password = ConstantUtilities.SAPPassword;
            ZbapiScEdi scData = new ZbapiScEdi();
            scData.ZediScFormat = new ZediScFormat[1];
            scData.ZediScFormat[0] = new ZediScFormat();
            if (status != null)
            {
                scData.Mode = "X";
            }

            scData.ZediScFormat[0].Itrtype = scLine.ItrType;
            scData.ZediScFormat[0].Itrno = scLine.ItrNumber;
            scData.ZediScFormat[0].Refitrno = scLine.RefItrNumber;
            scData.ZediScFormat[0].From = scLine.FromLocation;
            scData.ZediScFormat[0].To = scLine.ToLocation;
            scData.ZediScFormat[0].Custref = scLine.CustomerRefNumber;
            scData.ZediScFormat[0].ImItem = scLine.ItemNumber;
            scData.ZediScFormat[0].Matnr = scLine.BinType;
            scData.ZediScFormat[0].Menge = scLine.Quantity.ToString();
            scData.ZediScFormat[0].CustNo = scLine.CustomerCode;
            scData.ZediScFormat[0].CustPo = scLine.SINumber;
            scData.ZediScFormat[0].ContNo = scLine.ContainerNumber;
            scData.ZediScFormat[0].Vbeln = scLine.SalesDocument;
            scData.ZediScFormat[0].Posnr = scLine.SalesDocumentItem;
            scData.ZediScFormat[0].Wadat = scLine.ETD;
            scData.ZediScFormat[0].ImEtddate = scLine.ETD;
            scData.ZediScFormat[0].ImEtadate = scLine.ETA;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
            scData.ZediScFormat[0].Remarks = scLine.Remarks;

            scData.ZediScFormat[0].Htext = username;
            scData.Result = new ZediScError[0];
            ZbapiScEdiResponse response = client.ZbapiScEdi(scData);
            int responseCount = response.Result.Count();
            List<ResponseModel> responseList = new List<ResponseModel>();
            ResponseModel responseModel;
            bool error = false;

            for (int i = 0; i < responseCount; i++)
            {
                log.Info("From location :-" + scData.ZediScFormat[0].From + "\n ToLocation:-" + scData.ZediScFormat[0].To);
                log.Info("Response from SC:-" + response.Result[i].Mtype);
                log.Info("Message from SC:-" + response.Result[i].Mess);
                if (response.Result[i].Mtype == "E")
                {
                    if (!error)
                    {
                        errorCount++;
                        error = true;
                    }
                    responseModel = new ResponseModel();
                    responseModel.Success = false;
                    responseModel.Message = response.Result[i].Mess;
                    responseList.Add(responseModel);
                }

            }

            if (responseList.Count == 0)
            {
                if (!error)
                {
                    successCount++;
                }
                responseModel = new ResponseModel();
                responseModel.Success = true;
                if (status != null)
                {
                    responseModel.Message = "Successful verification of data in Test mode";
                }
                else
                {
                    responseModel.Message = "Successful verification of data in SAP mode";
                }
                responseList.Add(responseModel);
            }
            return responseList;
        }
    }
}