﻿using GoodpackEDI.Utilities;
using GoodpackEDI.ViewModels;
using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.SCManagementLayer
{
    public class GPSAPMessageSpec
    {
        private static List<RefTransactionTypeModel> objLstTargetMessage = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(GPSAPMessageSpec));
        public static void Initialize()
        {
            IList<SCTransMappingModel> mapperModel;
            try
            {
                if (objLstTargetMessage == null)
                {

                    using (var context = new GoodpackEDIEntities())
                    { 
                         
                         mapperModel =                           (from s in context.Ref_SAPMessageFields
                                                                  join p in context.Ref_TransactionTypes
                                                                  on s.TransactionId equals p.Id
                                                                  select new SCTransMappingModel { 
                                                                  DefaultValue=s.DefaultValue,
                                                                  FieldSequence=s.FieldSequence,
                                                                  FieldType=s.FieldType,
                                                                  Format=p.Format,
                                                                  Id=p.Id,
                                                                  InputType=s.InputType,
                                                                  IsMandatory=s.IsMandatory,
                                                                  MaxLength=s.MaxLength??0,
                                                                  Note=s.Note,
                                                                  Operation=s.Operation,
                                                                  OperationParam=s.OperationParam,
                                                                  SAPFieldName=s.SAPFieldName,
                                                                  SkipOnValidationFailure=s.SkipOnValidationFailure,
                                                                  TransactionCode=p.TransactionCode,
                                                                  TransactionId=s.TransactionId,
                                                                  TransactionName=p.TransactionName,
                                                                  ValidationMessage=s.ValidationMessage                                                                
                                                                  
                                                                  }).ToList();                                                                                            
                                                                                                       
                    
                    }
                   
                    objLstTargetMessage = new List<RefTransactionTypeModel>();
                   
                   
                    int intTargetMessageID = 0;
                    int intTargetMessageFieldID = 0;
                    SAPMessageFieldsModel objTargetMessageField;
                    RefTransactionTypeModel objTargetMessage = null;
                    bool blnTmp = false;
                    foreach (var drTargetMessage in mapperModel)
                    {

                        if (int.Parse(drTargetMessage.TransactionId.ToString()) != intTargetMessageID || objTargetMessage == null)
                        {
                            blnTmp = true;
                            objTargetMessage = new RefTransactionTypeModel();
                            objTargetMessage.ID = drTargetMessage.TransactionId;
                            objTargetMessage.TargetMessageName = drTargetMessage.TransactionName;
                            objTargetMessage.Format = drTargetMessage.Format;
                            objTargetMessage.TransactionType = EnumConverTo.TranType(drTargetMessage.TransactionCode);

                            // Set target message ID holder value
                            intTargetMessageID = objTargetMessage.ID;
                        }

                        // Get field details
                        objTargetMessageField = new SAPMessageFieldsModel();
                        intTargetMessageFieldID = drTargetMessage.Id;
                        objTargetMessageField.ID = intTargetMessageFieldID;
                        objTargetMessageField.FieldName = drTargetMessage.SAPFieldName;
                        objTargetMessageField.FieldSeq = drTargetMessage.FieldSequence;
                        objTargetMessageField.FieldType = (TargetMessageFieldTypes)EnumConverTo.GenericEnumConverter(typeof(TargetMessageFieldTypes), drTargetMessage.FieldType);
                        objTargetMessageField.InputType = EnumConverTo.TargetMessageInputs(drTargetMessage.InputType);
                        objTargetMessageField.DefaultValue = drTargetMessage.DefaultValue;
                        if (!string.IsNullOrEmpty(drTargetMessage.Operation))
                            objTargetMessageField.OperationName = drTargetMessage.Operation;
                        if (!string.IsNullOrEmpty(drTargetMessage.OperationParam))
                            objTargetMessageField.OperationParam = drTargetMessage.OperationParam;
                        if (!string.IsNullOrEmpty(drTargetMessage.MaxLength.ToString()))
                            objTargetMessageField.MaxLength = drTargetMessage.MaxLength;
                        objTargetMessageField.FieldSeq = drTargetMessage.FieldSequence;
                        if (!string.IsNullOrEmpty(drTargetMessage.Note))
                            objTargetMessageField.Note = drTargetMessage.Note;                       
                            objTargetMessageField.IsMandatory = drTargetMessage.IsMandatory;
                        if (drTargetMessage.ValidationMessage != null)
                            objTargetMessageField.ValidationError = drTargetMessage.ValidationMessage;
                        if (drTargetMessage.SkipOnValidationFailure != null)
                            objTargetMessageField.SkipOnValidationFailure = drTargetMessage.SkipOnValidationFailure;

                        // Add field to message spec
                        objTargetMessage.Fields.Add(objTargetMessageField.FieldSeq, objTargetMessageField);

                        // If message spec does not exist in spec list, add
                        if (!objLstTargetMessage.Contains(objTargetMessage))
                        {
                            objLstTargetMessage.Add(objTargetMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(" Initialize in GPSAPMessageSpec ." + "" + ". Api Exception : Message- " + ex.Message);
            }
        }

        public static RefTransactionTypeModel GetTargetMessage(TRANSACTION_TYPES objTransactionType, TargetMessageFormat objTargetMessageFormat)
        {
            if (objLstTargetMessage == null)
            {
                Initialize();
            }
            IList<RefTransactionTypeModel> objlstTargetMsgSpecs = null;
            lock (objLstTargetMessage)
            {
                var listHeader = from header in objLstTargetMessage
                                 where header.TransactionType == objTransactionType && header.Format == objTargetMessageFormat.ToString()
                                 select header;

                objlstTargetMsgSpecs = listHeader.ToList<RefTransactionTypeModel>();
            }
            return objlstTargetMsgSpecs != null ? objlstTargetMsgSpecs[0] : null;
        }

        public static Map MapperData(string mapperFile)
        {
            Map mapItem = new Map();
            
            using(var context=new GoodpackEDIEntities())
	            {
                    IList<MapperDataModel> mapperModel = (from s in context.Trn_MappingSubsi
                                                          join p in context.Trn_SubsiToTransactionFileMapper on s.SubsiToTransactionFileMapperId equals p.Id
                                                          join m in context.Trn_MappingConfiguration on s.Id equals m.MappingSubsiId
                                                          where (p.Filename == mapperFile) && (p.IsEnabled == true)
                                                          select new MapperDataModel
                                                          {
                                                              FixedValue = m.FixedValue,
                                                              Id = m.Id,
                                                              InputDateFormat = m.InputDateFormat,
                                                              MappingSubsiId = m.MappingSubsiId,
                                                              MappingType = m.MappingType,
                                                              SapFieldSeq = m.SapFieldSeq,
                                                              SourceFieldSeq1SubStringLength = m.SourceFieldSeq1SubStringLength ?? 0,
                                                              SourceFieldSeq1SubStringStartPosition = m.SourceFieldSeq1SubStringStartPosition ?? 0,
                                                              SourceFieldSeq2 = m.SourceFieldSeq2 ?? 0,
                                                              SourceFieldSeq3 = m.SourceFieldSeq3 ?? 0,
                                                              SourceFieldSequence = m.SourceFieldSequence ?? 0
                                                          }).ToList();
                    int i=0;
                    List<MapItem> mapperItem = new List<MapItem>();;
                    MapItem itemData;
                    int subsiId=0;
                    foreach (var item in mapperModel)
                    {
                       
                        itemData=new MapItem();
                        subsiId = item.MappingSubsiId;
                        itemData.FixedValue = item.FixedValue;
                        itemData.SourceFieldSeq = item.SourceFieldSequence;
                        itemData.SourceFieldSeq1SubStringLength = item.SourceFieldSeq1SubStringLength;
                        itemData.SourceFieldSeq1SubStringStartPosition = item.SourceFieldSeq1SubStringStartPosition;
                        itemData.SourceFieldSeq2 = item.SourceFieldSeq2;
                        itemData.SourceFieldSeq3 = item.SourceFieldSeq3;
                        itemData.SourceDateFormat = item.InputDateFormat;
                        itemData.MappingType = item.MappingType;
                        itemData.TargetFieldSeq = item.SapFieldSeq;
                        mapperItem.Add(itemData);                                             
                        i++;
                    }
                    mapItem.ID = subsiId;
                    mapItem.MapItems = mapperItem;  
                    return mapItem;
	            }
        }
          
 
    }
}