﻿using GoodpackEDI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement
{
   public interface ITransTypeDecodeValueManager
    {
         bool InputTypeFinder(TargetMessageInputTypes inputType);
         string FindDecodeValue(string lookUpCode, string mapperFile, int subsiId,int lookupNameId);       
        
    }
}
