﻿using GoodpackEDI.BusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement;
using GoodpackEDI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.BusinessLayer.DecodeValueManagementLayer
{

    public class DecodeValueManager : IDecodeValueManager
    {


        public ITransTypeDecodeValueManager DecodeType(TRANSACTION_TYPES types)
        {          
            switch (types)
            {
                case TRANSACTION_TYPES.SC: 
                    return new SCDecodeValueManager();                   
                   
                default: return new SCDecodeValueManager();
            } 
        }
    }
}