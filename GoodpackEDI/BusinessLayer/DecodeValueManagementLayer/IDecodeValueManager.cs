﻿using GoodpackEDI.BusinessLayer.DecodeValueManagementLayer.SCDecodeValueManagement;
using GoodpackEDI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodpackEDI.BusinessLayer.DecodeValueManagementLayer
{
   interface IDecodeValueManager
    {
     ITransTypeDecodeValueManager DecodeType(TRANSACTION_TYPES types);       
    }
}
