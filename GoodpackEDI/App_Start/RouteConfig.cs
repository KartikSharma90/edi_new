﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;
namespace GoodpackEDI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);
            routes.MapRoute("http://103.24.4.31/ediportal/GoodpackEDI/LSPCR", "{action}/{id}", new { controller = "GoodpackEDI", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "GoodpackEDI", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
            name: "DefaultNew",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "ProActiveDehire", action = "Index", id = UrlParameter.Optional }
            );
           

        }
    }
}




