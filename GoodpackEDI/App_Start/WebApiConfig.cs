﻿using GPArchitecture.Models;
using SessionState;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Http.WebHost;

namespace GoodpackEDI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            log4net.Config.XmlConfigurator.Configure();            
            var modelBuilder = new ODataConventionModelBuilder();
            modelBuilder.EntitySet<TransactionDataModel>("TransactionSSCOData");
            modelBuilder.EntitySet<TrabsactionLineDetailModel>("TransactionDetailsSSCOData");
            modelBuilder.EntitySet<TransactionDataModel>("TransactionOData");
            modelBuilder.EntitySet<SOTransLineModel>("TransactionDetailsSOOData");
            modelBuilder.EntitySet<SCTransLineModel>("TransactionDetailsSCOData");
            modelBuilder.EntitySet<ReportSOTransactionModel>("TransactionSOSearchOData");
            modelBuilder.EntitySet<ReportSCTransactionModel>("TransactionSCSearchOData");
            modelBuilder.EntitySet<ReportSSCTransactionModel>("TransactionSSCSearchOData");

            var model = modelBuilder.GetEdmModel();
            config.Routes.MapODataServiceRoute(
                routeName: "OData",
                routePrefix: "api",
                model: model
                );

            var httpControllerRouteHandler = typeof(HttpControllerRouteHandler).GetField("_instance",
              BindingFlags.Static |
              BindingFlags.NonPublic);

            if (httpControllerRouteHandler != null)
            {
                httpControllerRouteHandler.SetValue(null,
                    new Lazy<HttpControllerRouteHandler>(() => new SessionHttpControllerRouteHandler(), true));
            }

            config.Routes.MapHttpRoute("DefaultApiWithAction", "Api/{controller}/{action}");
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
