﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class FileHeaderModel
    {
        public string SRNO { get; set; }
        public string HeaderName { get; set; }
    }
}