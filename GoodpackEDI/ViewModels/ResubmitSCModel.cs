﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class ResubmitSCModel
    {
        public int UserId { get; set; }
        public int SubsiId { get; set; }
        public int TransactionId { get; set; }
        public string FileName { get; set; }
        public string FileSource { get; set; }
        public string FileSourceAddress { get; set; }
        public string BatchContent { get; set; }
        public string MapperFileName { get; set; }
    }
}