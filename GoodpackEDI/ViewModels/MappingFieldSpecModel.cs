﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class MappingFieldSpecModel
    {
        public int FieldSequence { get; set; }
        public string FieldName { get; set; }
        public int Length { get; set; }		
    }
}