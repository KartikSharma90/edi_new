﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class TransactionDataModel
    {
        public int UniqueId { get; set; }
        public string SubsyName { get; set; }
        public string TransactionName { get; set; }
        public string TransactionType { get; set; }
        public string FileName { get; set; }
        public int? TotalRecords { get; set; }
        public int SuccessCount { get; set; }
        public int ErrorCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MapperName { get; set; }
        public int? DeletedRecordCount { get; set; }
    }
    public class TrabsactionLineDetailModel
    {
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
        public string LineContent { get; set; }
        public int Batchid { get; set; }
        public int UniqueId { get; set; }
        public string Mode { get; set; }
        public string PackerLocation { get; set; }
        public string ConsigneeLocation { get; set; }
        public string BinType { get; set; }
        public string ETD { get; set; }
        public string Qty { get; set; }
        public string SAPStatus { get; set; }
        public string BinNumbers { get; set; }
    }

}