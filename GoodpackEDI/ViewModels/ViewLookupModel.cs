﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class ViewLookupModel
    {
        public int UniqueId { get; set; }
        public string SubsyName { get; set; }
        public string MapperName { get; set; }
        public string LookupName { get; set; }
        public string CodeValue { get; set; }
        public string DecodeValue { get; set; }

    }
}