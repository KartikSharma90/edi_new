﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SAPMappingModel
    {
        public int Id { get; set; }
        public string FieldName { get; set; }
        public string Note { get; set; }
        public string InputType { get; set; }
        public int FieldSequence { get; set; }
        public bool IsMandatory { get; set; }
        public bool IsDisplay { get; set; }
        public string FieldType { get; set; }
    }
}