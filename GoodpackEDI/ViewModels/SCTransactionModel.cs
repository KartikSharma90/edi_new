﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GoodpackEDI.Utilities;
using System.Text;

namespace GoodpackEDI.ViewModels
{
    public class SCTransactionModel
    {
        private string sourceAddress;
        private string filename;
        private int fileVersion;
        private int batchId;
        private string sourceReferenceId;
        private string message;
        private string customerName;
        private string customerHeadCompany;
        private string service;
        private long customerCode = -1;
        private string operatorNotes;
        private DateTime dateCreated;
        private DateTime lastUpdated;
        private string errorDesc;
        private string requestFileArchivePath;
        private string sourceMessageFormat;
        private string shippingLiner;
        private string strErrorDescFormatted;
        private int recordCountTotal;
        private int recordCountSkipValFailure;
        private int recordCountFieldValFailure;
        private int recordCountSAPError;
        private int recordCountSuccess;
        private int subsiId;
        public List<Dictionary<string, string>> mappingServiceValues = new List<Dictionary<string, string>>();
        public int UserId { get; set; }
        public string EmailId { get; set; }
        
        /// <summary>
        /// Represent unique Batch Id created for the file received.
        /// </summary>
        public int BatchID
        {
            set
            {
                batchId = value;
            }
            get
            {
                return batchId;
            }
        }

        /// <summary>
        /// Represent unique Subsi Id created for the file received.
        /// </summary>
        public int SubsiId
        {
            set
            {
                subsiId = value;
            }
            get
            {
                return subsiId;
            }
        }
        /// <summary>
        /// Represent unique CustomerCode of the Customer.
        /// </summary>
        public long CustomerCode
        {
            set
            {
                customerCode = value;
            }
            get
            {
                return customerCode;
            }
        }

        public string SourceMessageFormat
        {
            set { sourceMessageFormat = value; }
            get { return sourceMessageFormat; }
        }

        /// <summary>
        /// Represents the customer head company
        /// </summary>
        public string CustomerHeadCompany
        {
            set { this.customerHeadCompany = value; }
            get { return this.customerHeadCompany; }
        }

        /// <summary>
        /// Represents the Service
        /// </summary>
        public string Service
        {
            set { this.service = value; }
            get { return this.service; }
        }

        /// <summary>
        /// Represent Customer Name.
        /// </summary>
        public string CustomerName
        {
            set
            {
                customerName = value;
            }
            get
            {
                return customerName;
            }
        }

        /// <summary>
        /// Represents Transactions like Sales Order, 
        /// Shipment Confirmation, Forecast.
        /// </summary>
        public TRANSACTION_TYPES TransactionType;

        public BatchStatus StatusCode;

        public ErrorCodes ErrorCode;
        //To collect all skiprecords
        public StringBuilder SkippedRecordsFromFC = new StringBuilder();
        public string ErrorDescFormatted
        {
            set { strErrorDescFormatted = value; }
            get { return strErrorDescFormatted; }
        }
        /// <summary>
        /// name of file
        /// </summary>
        public string BatchFileName
        {
            set
            {
                filename = value;
            }
            get
            {
                return filename;
            }
        }

        public SOURCE_TYPES BatchFileSourceType;

        /// <summary>
        /// sender email address or source file path.
        /// </summary>
        public string BatchFileSourceAddress
        {
            set
            {
                sourceAddress = value;
            }
            get
            {
                return sourceAddress;
            }
        }


        /// <summary>
        /// Version of the File
        /// </summary>
        public int BatchFileVersion
        {
            set
            {
                fileVersion = value;
            }
            get
            {
                return fileVersion;
            }
        }


        /// <summary>
        /// Message – content of message
        /// </summary>
        public string Message
        {
            set
            {
                message = value;
            }
            get
            {
                return message;
            }
        }

        /// <summary>
        /// Notes from operator as indicated in TRANS_BATCH table
        /// </summary>
        public string OperatorNotes
        {
            set
            {
                operatorNotes = value;
            }
            get
            {
                return operatorNotes;
            }
        }

        public string ErrorDesc
        {
            set
            {
                errorDesc = value;
            }
            get
            {
                return errorDesc;
            }
        }
        public DateTime DateCreated
        {
            set
            {
                dateCreated = value;
            }
            get
            {
                return dateCreated;
            }
        }

        public DateTime LastUpdated
        {
            set
            {
                lastUpdated = value;
            }
            get
            {
                return lastUpdated;
            }
        }

        public string RequestFileArchivePath
        {
            set { this.requestFileArchivePath = value; }
            get { return this.requestFileArchivePath; }
        }

        public string ShippingLiner
        {
            set { this.shippingLiner = value; }
            get { return this.shippingLiner; }
        }

        public int RecordCountTotal
        {
            set { this.recordCountTotal = value; }
            get { return this.recordCountTotal; }
        }
        public int RecordCountSkipValFailure
        {
            set { this.recordCountSkipValFailure = value; }
            get { return this.recordCountSkipValFailure; }
        }
        public int RecordCountFieldValFailure
        {
            set { this.recordCountFieldValFailure = value; }
            get { return this.recordCountFieldValFailure; }
        }
        public int RecordCountSAPError
        {
            set { this.recordCountSAPError = value; }
            get { return this.recordCountSAPError; }
        }
        public int RecordCountSuccess
        {
            set { this.recordCountSuccess = value; }
            get { return this.recordCountSuccess; }
        }
       
    }
}