﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class MapperModel
    {
        public string MapperCode { get; set; }
        public string MapperName { get; set; }
    }
}