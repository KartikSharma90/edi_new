﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class VoyageSchedulingModel
    {      
        public string ShippingLineName { get; set; }
        public string VoyageNumber { get; set; }
        public string VesselName { get; set; }
        public string DestinationPort { get; set; }
        public DateTime ETAatDestinationPort { get; set; }
    }
}