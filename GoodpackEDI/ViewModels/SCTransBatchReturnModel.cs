﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SCTransBatchReturnModel
    {
        public List<SCTransBatchModel> TransBatchModel { get; set; }
        public int totalRecordCount { get; set; }
    }
}