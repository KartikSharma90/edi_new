﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class PrivilegeAccessModel
    {
        public bool IsAdminLink { get; set; }
        public bool IsSOLink { get; set; }
        public bool IsSCLink { get; set; }
        public bool IsSubsiLink { get; set; }
        public bool IsMappingConfigLink { get; set; }
        public bool IsDataLookUpLink { get; set; }
        public bool IsSSCLink { get; set; }
        public bool IsEditLink { get; set; }
        public bool IsASNLink { get; set; }
        public bool IsCancelSOLink { get; set; }
        public bool IsErrorMailDetailsLink { get; set; }
    }
}