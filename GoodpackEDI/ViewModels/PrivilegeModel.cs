﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class PrivilegeModel
    {
        public int PrivilegeId { get; set; }
        public string PrivilegeName { get; set; }
    }
}