﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class UserToSubsiModel
    {
        public IList<SubsiModel> SubsiList { get; set; }
        public IList<SubsiModel> MappedSubsi { get; set; }
    }
}