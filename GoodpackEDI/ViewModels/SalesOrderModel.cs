﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SalesOrderModel
    {
        public string cntryKey { get; set; }
        public string congName { get; set; }
        public string congCustNum { get; set; }
        public string reg { get; set; }
        public string pkrName { get; set; }
        public string packCustNum { get; set; }
        public string matNm { get; set; }
        public string custPrchOrdNo { get; set; }
        public string schLneDte { get; set; }
        public short leadDys { get; set; }
        public string itrCnfDte { get; set; }
        public string plndWk { get; set; }
        public decimal cuOrdQty { get; set; }
        public decimal itrCnfQty { get; set; }
        public string dte { get; set; }
        public string vbeln { get; set; }
        public string vdatu { get; set; }

    }
}