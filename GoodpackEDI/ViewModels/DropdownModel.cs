﻿
namespace GoodpackEDI.ViewModels
{
    public class DropdownModel
    {
        public int Value { get; set; }
        public string DisplayText { get; set; }
    }
}