﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class DeleteLineModel
    {
        public int TransactionId { get; set; }
        public string DeleteComment { get; set; }
        public string  DeleteIds { get; set; }
    }
}