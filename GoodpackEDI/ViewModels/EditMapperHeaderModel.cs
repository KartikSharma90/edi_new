﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
public class EditMapperHeaderModel
    {
        public int SRNO { get; set; }
        public string MappedField { get; set; }
        public int Id { get; set; }
        public string HeaderName { get; set; }
        public string FieldName { get; set; }
        public string InputType { get; set; }
        public int FieldSequence { get; set; }
        public string Note { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int MapperId { get; set; }

        public int SubsiId { get; set; }
        public int FileTypeId { get; set; }
        public int FieldCountValue { get; set; }
        public int TransactionId { get; set; }
        public string DateFormat { get; set; }

        public int FtpPort { get; set; }
        public bool FtpIsEnabledForMapper { get; set; }
        public bool IsFtpEnable { get; set; }
        public string FtpType { get; set; }
        public string FtpHost { get; set; }
        public string FtpLocation { get; set; }
        public string FtpUser { get; set; }
        public string FtpPassword { get; set; }
        public string FtpEncryptiontype { get; set; }
        public string FtpSslHostCertificateFingerprint { get; set; }

    }
}