﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class LookupModel
    {
        public string CodeValue { get; set; }
        public string DecodeValue { get; set; }
        public int UniqueId { get; set; }
    }

    public class LookUpModelUpdate
    {
        public string MapperName { get; set; }
        public string LookupName { get; set; }
        public string LookupCode { get; set; }
        public string LookupDecode { get; set; }
    }
}