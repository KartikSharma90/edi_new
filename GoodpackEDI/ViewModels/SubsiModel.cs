﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SubsiModel
    {
        public int SubsiId { get; set; }
        public string SubsiName { get;set; }
        public bool IsEnabled { get; set; }
    }
}