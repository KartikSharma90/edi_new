﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SCTransLineModel
    {
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
        public string LineContent { get; set; }
        public int Batchid { get; set; }
        public int UniqueId { get; set; }
        public string Mode { get; set; }
        public string packerLocation { get; set; }
        public string consigneeLocation { get; set; }
        public string BinType { get; set; }
        public string ETD { get; set; }
        public string Qty { get; set; }        
        public string SAPStatus { get; set; }
        public string CustomerCode { get; set; }
    }

    public class SSCTransLineModelNew
    {
        public string ReferenceNumber { get; set; }
        public string Status { get; set; }
        public string LineContent { get; set; }
        public int Batchid { get; set; }
        public int UniqueId { get; set; }
        public string Mode { get; set; }
        public string packerLocation { get; set; }
        public string consigneeLocation { get; set; }
        public string BinType { get; set; }
        public string ETD { get; set; }
        public string Qty { get; set; }
        public string SAPStatus { get; set; }
        public string BarcodeNumbers { get; set; }
        public string  CustomerCode { get; set; }
    }
}