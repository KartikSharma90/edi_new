﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class ValidationDataModel
    {
        public string DateString { get; set; }
        public List<CharacterList> CharList { get; set; }
        public List<BoolList> BoolList { get; set; }
        //public ValidationDataModel()
        //{
        //    CharList = new CharacterList();
        //    BoolList = new BoolList();
        //}

    }
    public class CharacterList
    {
        public int? MaxLength { get; set; }
        public string SAPName { get; set; }
    }
    public class BoolList
    {
        public bool IsNullAllowed { get; set; }
        public string SAPName { get; set; }
    }
}