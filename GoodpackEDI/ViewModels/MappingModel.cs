﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class MappingModel
    {
        public int SubsiId { get; set; }
        public int TransactionId { get; set; }
        public int FileTypeId { get; set; }
        public string FieldCountValue { get; set; }
        public bool IsCustomizedInput { get; set; }
        public int RowNumber { get; set; }       
        public int ColumnNumber { get; set; }
        public List<MapperHeaderModel> MappedFileModel{get;set;}
        public List<FileHeaderModel> FileHeaders { get; set; }
        public List<SAPMappingModel> SAPFieldHeaders { get; set; }
        public string DateFormat { get; set; }
        public string FileName { get; set; }
        public bool EnableEmail { get; set; }
        public string EmailIds { get; set; }

        public string FilePath { get; set; }
        public int MapperId { get; set; }
        public int FieldId { get; set; }
        public int SRNO { get; set; }

        public int FtpPort { get; set; }
        public bool FtpIsEnabledForMapper { get; set; }
        public bool IsFtpEnable { get; set; }
        public string FtpType { get; set; }
        public string FtpHost { get; set; }
        public string FtpLocation { get; set; }   
        public string FtpUser { get; set; }
        public string FtpPassword { get; set; }
        public string FtpEncryptiontype { get; set; }
        public string FtpSslHostCertificateFingerprint { get; set; }


    }
}