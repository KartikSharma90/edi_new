﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class MapperHeaderModel
    {
        public int SRNO { get; set; }
        public string MappedField { get; set; }
        public int Id { get; set; }
        public string HeaderName { get; set; }
        public string FieldName { get; set; }
        public string InputType { get; set; }
        public int FieldSequence { get; set; }
    }
}