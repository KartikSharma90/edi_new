﻿using GoodpackEDI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class GenericItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LineId { get; set; }
        public string Status { get; set; }
        public BatchStatus StatusCode { get; set; }
        public string Message { get; set; }
        public string DecodeValue { get; set; }
        public string ErrorMessage { get; set; }
        public string InputType { get; set; }
        public string  DataValues { get; set; }
        public string FileName { get; set; }
        public string BatchFileSourceAddress { get; set; }        
        public string BatchId{ get; set; }
        public string EmailId { get; set; }
        public string CustomerPackerRelation { get; set; }
        public Dictionary<int, Dictionary<string, string>> sapResponseData { get; set; }
        public List<string> ResponseMessage { get; set; }
        public GenericItemModel()
        {
            sapResponseData = new Dictionary<int, Dictionary<string, string>>();
        }

    }
}