﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}