﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class BatchStatusDetail
    {
        public bool SubmitSAPErrorData { get; set; }
        public bool SubmitSuccessData { get; set; }
        public bool SubmitErrorData { get; set; }
        public bool DeleteLinedata { get; set; }

    }
}