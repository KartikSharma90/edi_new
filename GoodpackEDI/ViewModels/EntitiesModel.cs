﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
      public class EntitiesModel
        {
            private long customerCode;
            private string customerName;
            private string customerStakeholderEmailList;
            private SubsiModel customerHeadCompany = null;
            private string lastUpdateBy;
            private DateTime lastUpdateDate;
            private string contractNumber;
            private DateTime contractStart;
            private DateTime contractEnd;
            private string subsidiary;
            private string goodpackSubsidiaryEmailList;
            private string vertical;
            private string fileSourcePath;
            private bool isEnabled;

            public long CustomerCode
            {
                set
                {
                    customerCode = value;
                }
                get
                {
                    return customerCode;
                }
            }
            public string CustomerName
            {
                set
                {
                    customerName = value;
                }
                get
                {
                    return customerName;
                }
            }
            public string CustomerStakeholderEmailList
            {
                set
                {
                    customerStakeholderEmailList = value;
                }
                get
                {
                    return customerStakeholderEmailList;
                }
            }
            public SubsiModel CustomerHeadCompany
            {
                set
                {   
                    this.customerHeadCompany = value;
                }
                get
                {
                    if (customerHeadCompany == null)
                    {
                        this.customerHeadCompany = new SubsiModel();
                    }
                     return customerHeadCompany;
                }
            }
            public string ContractNumber
            {
                set
                {
                    contractNumber = value;
                }
                get
                {
                    return contractNumber;
                }
            }

            public DateTime ContractStart
            {
                set
                {
                    contractStart = value;
                }
                get
                {
                    return contractStart;
                }
            }
            public DateTime ContractEnd
            {
                set
                {
                    contractEnd = value;
                }
                get
                {
                    return contractEnd;
                }
            }
            public string Subsidiary
            {
                set
                {
                    this.subsidiary = value;
                }
                get                
                {
                    return this.subsidiary;
                }
            }
            public string GoodpackSubsidiaryEmailList
            {
                set
                {
                    this.goodpackSubsidiaryEmailList = value;
                }
                get
                {
                    return this.goodpackSubsidiaryEmailList;
                }
            }
            public string Vertical
            {
                set
                {
                    vertical = value;
                }
                get
                {
                    return vertical;
                }
            }
            public string FileSourcePath
            {
                set
                {
                    fileSourcePath = value;
                }
                get
                {
                    return fileSourcePath;
                }
            }
            public string LastUpdatedBy
            {
                set
                {
                    lastUpdateBy = value;
                }
                get
                {
                    return lastUpdateBy;
                }
            }
            public DateTime LastUpdateDate
            {
                set
                {
                    lastUpdateDate = value;
                }
                get
                {
                    return lastUpdateDate;
                }
            }

            public bool IsEnabled
            {
                get { return this.isEnabled; }
                set { this.isEnabled = value; }
            }
        }        
  
}