﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class DeviceModel
    {

    }

    public class DeviceList
    {
        public int Id { get; set; }
        public string DeviceID { get; set; }
        public int PackerCode { get; set; }
        public int CustomerCode { get; set; }
        public string MapperName { get; set; }
        public string CustomerEmail { get; set; }
        public bool IsEnabled { get; set; }
        public string Vertical { get; set; }
        public string SKU { get; set; }
        public string Details { get; set; }
        public string  TransactionType { get; set; }
        public string SubsiId { get; set; }
    }
    public class ScanerDetail
    {
        public DeviceList record { get; set; }
    }
    
    public class VerticalModel   {
        public string DisplayText  { get; set; }
        public string Value { get; set; }
    }
    public class DataConvertionModel
    {
        public string SINumber { get; set; }
        public int Count { get; set; }
    }

    public class SScTOScConvertionModel
    {
        public string CustomerName { get; set; }
        public string PackingPlant { get; set; }
        public string Consignee { get; set; }
        public string SInumber { get; set; }
        public string BinType { get; set; }
        public int Quantity { get; set; }
        public string ETD { get; set; }
        public string ETA { get; set; }
        public int SubsiId { get; set; }
        public string DeviceId { get; set; }
        public string Remark { get; set; }
        public string ErrorMessage { get; set; }
        public string MapperName { get; set; }
        public int TransLineId { get; set; }
        public int LineId { get; set; }
        public string Data { get; set; }
        public string CustomerReferenceNumber { get; set; }
    }

    public class DeviceDetail
    {
        public string MapperName { get; set; }
        public int CustomerCode { get; set; }
        public int PackerCode { get; set; }
        public string Subsy { get; set; }
    }
}