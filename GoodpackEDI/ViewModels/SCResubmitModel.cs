﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoodpackEDI.ViewModels
{
    public class SCResubmitModel
    {
        public SCTransactionModel transactionModel { get; set; }
        public Dictionary<int, string> lineData { get; set; }
    }
}