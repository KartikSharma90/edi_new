﻿AppViewModel.addRow = function () {
    $('#AddConfig').show();
    $("#dialog-addconfig").dialog("open");
}
AppViewModel.editRow = function (configData) {
    AppViewModel.configData(configData);
    $("#dialog-editconfig").dialog("open");
}
AppViewModel.deleteRow = function (configData) {
    AppViewModel.configData(configData);
    $("#configDelete").dialog("open");
}

$(function () {   
    $("#dialog-addconfig").dialog({
        autoOpen: false,
        modal: true,
        width: 400, zIndex: 50000,
        height: 350,
        open: function (event, ui) {
            $('#AddConfig').show();
            $('#configform').validationEngine();
        },
        close: function (event, ui) {
            $('#configform').validationEngine('hideAll');
            $('#viewConfig').show();
        },
        show: {
            effect: "blind",
            duration: 100
        },
        hide: {
            effect: "blind",
            duration: 100
        },
        buttons: {
            "Save": function () {
                var configData = AppViewModel.configData();
                var configData = {
                    Id: AppViewModel.Id,
                    ConfigItem: AppViewModel.ConfigItem,
                    Value: AppViewModel.Value,
                    Description: AppViewModel.Description,
                    LastUpdatedBy: AppViewModel.LastUpdatedBy,
                    LastUpdatedDate: AppViewModel.LastUpdatedDate
                };
                if ($('#configform').validationEngine('validate')) {
                    $.ajax({
                        url: baseUrl + '/api/Config/AddConfig',
                        cache: false,
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        data: ko.toJSON(configData),
                        success: function (data) {
                            $.unblockUI();
                            //alert('added');
                            AppViewModel.configDatas.push(data);
                            AppViewModel.Id("");
                            AppViewModel.ConfigItem("");
                            AppViewModel.Value("");
                            AppViewModel.Description("");
                            AppViewModel.LastUpdatedBy("");
                            AppViewModel.LastUpdatedDate("");
                            loadConfigDetails();
                            AppViewModel.configData(null);
                        }
                    }).fail(
                    function (xhr, textStatus, err) {
                        alert(err);
                    });
                    $('#viewConfig').show();
                    $(this).dialog("close");
                }
            },

            Cancel: function () {
                AppViewModel.Id("");
                AppViewModel.ConfigItem("");
                AppViewModel.Value("");
                AppViewModel.Description("");
                AppViewModel.LastUpdatedBy("");
                AppViewModel.LastUpdatedDate("");
                AppViewModel.configData(null);
                $('#viewConfig').show();
                $(this).dialog("close");
            }
        }
    });
});

$(function () {
    $("#dialog-editconfig").dialog({
        autoOpen: false,
        modal: true, zIndex: 50000,
        width: 400,
        height: 350,
        open: function (event, ui) {
            $('#editconfig').validationEngine();
        },
        close: function (event, ui) {
            $('#editconfig').validationEngine('hideAll');
            AppViewModel.configData(null);
        },
        show: {
            effect: "blind",
            duration: 100
        },
        hide: {
            effect: "blind",
            duration: 100
        },
        buttons: {
            "Edit": function () {
                var configData = AppViewModel.configData();
                if ($('#editconfig').validationEngine('validate')) {
                    $.ajax({
                        url: baseUrl + '/api/Config/EditConfig',
                        cache: false,
                        type: 'PUT',
                        contentType: 'application/json; charset=utf-8',
                        data: ko.toJSON(configData),
                        success: function (data) {
                            $.unblockUI();
                            AppViewModel.configDatas.removeAll();
                            AppViewModel.configDatas(data);
                            AppViewModel.configData(null);
                            // alert("Record Updated Successfully");
                        }
                    }).fail(
                    function (xhr, textStatus, err) {
                        alert(err);
                    });

                    $(this).dialog("close");
                }
            },
            Cancel: function () {
                AppViewModel.configData(null);
                $(this).dialog("close");
            }
        }
    });
});

$(function () {
    $("#configDelete").dialog({
        autoOpen: false,
        title: "This record will be permanently deleted and cannot be recovered. Are you sure?",
        height: 200, zIndex: 50000,
        width: 400,
        modal: true,
        close: function (event, ui) {
            AppViewModel.configData(null);
        },
        show: {
            effect: "blind",
            duration: 100
        },
        hide: {
            effect: "blind",
            duration: 100
        },
        buttons: {
            "Delete": function () {
                var configData = AppViewModel.configData();
                var id = configData.Id;
                // alert(id);
                $.ajax({
                    url: baseUrl + '/api/Config/DeleteConfig?ConfigId=' + id,
                    cache: false,
                    type: 'DELETE',
                    contentType: 'application/json; charset=utf-8',
                    data: ko.toJSON(id),
                    success: function (data) {
                        AppViewModel.configDatas.remove(configData);
                        // alert("Config Record Deleted Successfully");
                        AppViewModel.Id("");
                        AppViewModel.ConfigItem("");
                        AppViewModel.Value("");
                        AppViewModel.Description("");
                        AppViewModel.LastUpdatedBy("");
                        AppViewModel.LastUpdatedDate("");
                        loadConfigDetails();
                        AppViewModel.configData(null);
                    }
                }).fail(
                     function (xhr, textStatus, err) {
                         alert(err);
                     });
                $(this).dialog("close");
            },
            Cancel: function () {
                AppViewModel.configData(null);
                $(this).dialog("close");
            }
        }
    });
});