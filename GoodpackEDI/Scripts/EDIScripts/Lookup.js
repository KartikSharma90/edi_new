﻿

this.onAddMoreLookup = function () {
    var tableRef = document.getElementById('LookupTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    var newRow = tableRef.insertRow(rowCount);

    // Insert a cell in the row at index 0
    var codecell = newRow.insertCell(0);
    codecell.style.border = '1px solid black';
    // codecell.style.width = '100%';
    // codecell.style.height = '100%';
    codecell.style.textAlign = "center";
    // Append a text node to the cell
    var codeText = document.createElement("input")
    codeText.style.width = '100%';
    codeText.style.height = '100%';
    codeText.id = "codeTB" + rowCount;
    codecell.appendChild(codeText);

    // Insert a cell in the row at index 1
    var decodecell = newRow.insertCell(1);
    decodecell.style.border = '1px solid black';
    // decodecell.style.width = '100%';
    // decodecell.style.height = '100%';
    decodecell.style.textAlign = "center";
    // Append a text node to the cell
    var decodeText = document.createElement("input")
    decodeText.style.width = '100%';
    decodeText.style.height = '100%';
    decodeText.id = "decodeTB" + rowCount;
    decodecell.appendChild(decodeText);
}
this.onSearchlookupClick = function () {

    if ($("#DDViewLookupsubsi :selected").text() == "Select Subsy" && $("#DDViewLookupmaper :selected").text() == "Select Mapper" && $("#DDViewLookupName :selected").text() == "Select Lookup Name") {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please select atleast one input field before search.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

        return;;
    }

    debugger
    var sortFlag = 0;
    var subsyId = 0;
    var MapperName = "";
    var LookupId = 0;

    if (!($("#DDViewLookupsubsi :selected").text() == "Select Subsy")) { subsyId = AppViewModel.selectedLookupSubsyValue()[0].SubsiId; }
    if (($("#DDViewLookupmaper :selected").text()) == "Select Mapper" || ($("#DDViewLookupmaper :selected").text()) == "") {
    }
    else { MapperName = AppViewModel.selectedMapperValue()[0].MapperCode; }
    if (!($("#DDViewLookupName :selected").text() == "Select Lookup Name")) { LookupId = AppViewModel.selectedViewLookupValue()[0].EntityCode; }

   // alert(MapperName);
    $('#ViewLookupGrid').jqGrid('GridUnload');
    $("#ViewLookupGrid").jqGrid({

        url: baseUrl + '/api/Lookup/ViewLookup?lookupNameId=' + LookupId + '&susbyId=' + subsyId + '&mapperName=' + MapperName,
        datatype: "json",
        myType: "GET",
        colNames: ['Subsy Name', 'Mapper Name', 'Lookup Name', 'Code Value', 'DecodeValue', 'Action'],
        colModel: [
        { name: 'SubsyName', index: 'SubsyName', align: 'center' },
        { name: 'MapperName', index: 'MapperName', align: 'center' },
        { name: 'LookupName', index: 'LookupName', align: 'center' },
        { name: 'CodeValue', index: 'CodeValue', align: 'center', editable: true },
        { name: 'DecodeValue', index: 'DecodeValue', align: 'center', editable: true },
        { name: 'act', index: 'act', align: 'center', sortable: false, width: 300, resizable: false }

        ],
        jsonReader: {
            root: 'LookupData',
            id: 'UniqueId',
            repeatitems: false
        },
        pager: $('#viewlookupPager'),
        excel: true,
        rowNum: 10,
        rowList: [10, 50, 200, 500, 1000],
        autowidth: true,
        shrinkToFit: true,
        viewrecords: true,
        loadonce: true,
        autoencode: false,
        height: 'auto',
        width: '100%',
        caption: "Lookup Details",

        loadComplete: function (data) {
            $("tr.jqgrow:odd").css("background", "#f1f1f1");
            debugger

            var ids = $("#ViewLookupGrid").jqGrid('getDataIDs');
            for (var i = 0; i < ids.length; i++) {
                var rowId = ids[i];
                //se = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Show Details' onclick=\"detailsButtonClick('" + rowId + "')\"  />";
                be = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='Edit' onclick=\"ViewLookUpEditButtonClick('" + rowId + "')\" />";
                ce = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='Cancel' onclick=\"ViewLookUpCancelButtonClick('" + rowId + "')\" />";
                se = "<input style='height:22px;width:60px;display:none;background-color:#004c97;color:#ffffff;border-radius:5px; ' type='button'  Id='" + rowId + "save" + "' value='Save' onclick=\"ViewLookUpSaveButtonClick('" + rowId + "')\" />"
                bd = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='Delete' onclick=\"ViewLookUpDeleteButtonClick('" + rowId + "')\" />";
                $("#ViewLookupGrid").jqGrid('setRowData', ids[i], { act: be + ce + se + bd })

                //  $("#TransactionGrid").jqGrid('setRowData', ids[i], { ShowDetails:be+ce+se});
            }


            if (sortFlag == 0) {

                if (!data.ModelStatus) {
                    //alert("No Data to Display or Network Error !");
                    var noData = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.S_errMsg,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                    $("#ViewLookupGrid").jqGrid('setGridState', 'hidden');
                }
                else {
                    var msg2 = noty({
                        layout: 'top',
                        type: 'success',
                        text: 'Lookup Details Generated.',
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }

                sortFlag = 1;
            }
        },


    });

    $('#ViewLookupGrid').jqGrid('navGrid', '#viewlookupPager', {
        edit: false, view: false, add: false, del: false, search: true,
        beforeRefresh: function () {
            //alert('In beforeRefresh');
            $('#ViewLookupGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
        }
    }, {}, {}, {}, { closeAfterSearch: true }, {});

}


function ViewLookUpEditButtonClick(rowId) {
    debugger
    //$('#SalesOrderGrid').editRow("" + rowId + "");
    $('#ViewLookupGrid').jqGrid('editRow', rowId, false);
    document.getElementById(rowId + "save").style.display = "inline";


}
function ViewLookUpCancelButtonClick(rowId) {
    debugger

    $('#ViewLookupGrid').restoreRow("" + rowId + "");
    document.getElementById(rowId + "save").style.display = "none";
}

function ViewLookUpSaveButtonClick(rowId) {
    debugger
    $('#ViewLookupGrid').saveRow("" + rowId + "");
    var selectedRowData = $("#ViewLookupGrid").getRowData(rowId);
    var LookupModel = {
        CodeValue: selectedRowData.CodeValue,
        DecodeValue: selectedRowData.DecodeValue,
        UniqueId:rowId
    }
    if (selectedRowData.CodeValue == '' && selectedRowData.DecodeValue == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please enter Code and Decode Values",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

    }
    else if (selectedRowData.CodeValue == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please enter Code Value",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

    }
    else if (selectedRowData.DecodeValue == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please enter Decode Value",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

    }
    else {
        editLookupData(LookupModel);
    }

}


function ViewLookUpDeleteButtonClick(rowId) {
    debugger;
    $("#viewLookupDelete").dialog({
        title: "Are you sure you want to delete the selected Lookup?",
        autoOpen: true,
        height: 150,
        width: 450, zIndex: 50000,
        modal: true,
        buttons: {
            "Confirm Delete": function () {
                debugger;
                $.ajax({
                    type: "DELETE",
                    url: baseUrl + '/api/Lookup/DeleteLookup?lookupRowId=' + rowId,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        $.unblockUI();
                        if (data) {
                            noty({
                                layout: 'top',
                                type: 'success',
                                text: "Successfully deleted the error records",
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                            $("#viewLookupDelete").dialog("close");
                            onSearchlookupClick();
                        }
                        else {
                            $.unblockUI();
                            noty({
                                layout: 'top',
                                type: 'error',
                                text: "Unable to delete error records.Please try again",
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                        $.unblockUI();
                    }
                })

            },
            Cancel: function () {
                $(this).dialog("close");
            }

        },
        close: function () {

            $(this).dialog("close");
        }
    });
}

function editLookupData(LookupModel) {
    uiBlocker();
    $.ajax({
        type: "PUT",
        url: baseUrl + '/api/Lookup/EditLookup',
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(LookupModel),
        success: function (data) {
            $.unblockUI();
            if (data.Success == true) {

                var LookupRespMessage = "<div style='color:green'>" + data.Message + "</div>";

                var msg = noty({
                    layout: 'top',
                    type: 'success',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
                onSearchlookupClick();

            }
            else {

                noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });
                onSearchlookupClick();
            }

            $.unblockUI();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

this.onDeleteLookup = function () {

    debugger
    var tableRef = document.getElementById('LookupTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    //var newRow = tableRef.insertRow(rowCount);

    if (rowCount == 0) {
        noty({
            layout: 'top',
            type: 'error',
            text: "No rows to delete.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

        return;
    }

    tableRef.deleteRow(rowCount - 1);

}


this.onAddlookupSaveClick = function () {
    debugger
    ////var bbb2 = $('#DDmaper').val();
    ////var fff5 = $('#DDmaper').has('option').length;

    // var e5 = document.getElementById("DDmaper");
    // var strUser5 = e5.options[e5.selectedIndex].value;

    // var e6 = document.getElementById("DDmaper");
    //  var strUsersadd = e6.options[e6.selectedIndex].text;
    //  var strUserxx = e6.options[e6.selectedIndex].value;

    var bbb5 = $("#DDmaper :selected").text()
    // var bbbss6 = $("#DDmaper :selected").val();


    if ($("#DDsubsi :selected").text() == "Select Subsy") {
        noty({
            layout: 'top',
            type: 'error',
            text: "Subsy Name is mandatory.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

        return;;
    }

    if ($("#DDmaper :selected").text() == "Select Mapper" || !($("#DDmaper :selected").text())) {
        noty({
            layout: 'top',
            type: 'error',
            text: "Mapper Name is mandatory.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

        return;;
    }

    //if (!$('#DDmaper').val()) {
    //    noty({
    //        layout: 'top',
    //        type: 'error',
    //        text: "Mapper Name is mandatory.",
    //        dismissQueue: true,
    //        animation: {
    //            open: { height: 'toggle' },
    //            close: { height: 'toggle' },
    //            easing: 'swing',
    //            speed: 500
    //        },
    //        timeout: 5000
    //    });

    //    return;;
    //}
    var table = document.getElementById('LookupTable');

    if (table.rows.length <= 1) {
        noty({
            layout: 'top',
            type: 'error',
            text: "No Lookup inputs",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });

        return;
    }

    var cellData = [];
    var subsyId = AppViewModel.selectedLookupSubsyValue()[0].SubsiId;
    var MapperName = AppViewModel.selectedMapperValue()[0].MapperCode;
    var LookupName = AppViewModel.selectedLookupValue().EntityCode;
    cellData[0] = new Array(subsyId, MapperName, LookupName);

    for (var r = 1, n = table.rows.length; r < n; r++) {
        var rowData = new Array();

        var mmm = table.rows[r].cells.length

        for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {
            if (c == 0) {
                codevalue = document.getElementById("codeTB" + (r - 1)).value;
                if (!codevalue) {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Code Value cannot be empty",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });

                    return;

                }
                else {
                    rowData[0] = codevalue;
                }
            }
            else {
                decodeValue = (document.getElementById("decodeTB" + (r - 1)).value);
                if (!decodeValue) {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Decode Value cannot be empty",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });

                    return;
                }
                else {

                    rowData[1] = decodeValue;
                    cellData.push(rowData);

                }
            }
        }
    }

    postLookupData(cellData);
    // alert("done");
}

this.onAddLookupClick = function () {

    debugger
    $('#scResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SO').hide();
    $('#soResult').hide();
    $('#scMapping').hide();
    $('#asnMapping').hide();
    $('#sscResult').hide();
    $('#roleList').hide();
    $('#AddnewSO').hide();
    $('#ViewLookup').hide();
    $('#scMapping').hide();
    $('#AddLookup').show();
    $('#sapMapping').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#kendoUISSC').hide();
    $('#scannerRegst').hide();
    $('#sscMapping').hide();
    AppViewModel.selectedMenu('Data Lookup >> Add look up');
    AppViewModel.selectedLookupSubsyValue('');
    AppViewModel.availableLookupSubsy('');
    AppViewModel.selectedMapperValue('');
    AppViewModel.mapperforSubsy([]);
    getAllSubsies();
    getAllLookupNames();
    //getAllEntitiesForSubsy();
    var tableRef = document.getElementById('LookupTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }

    var rowId = 0;
};

this.onViewLookupClick = function () {

    debugger
    $('#scResult').hide();
    $('#soResult').hide();
    $('#sapMapping').hide();
    $('#asnMapping').hide();
    $('#roleList').hide();
    $('#scMapping').hide();
    $('#sscResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SO').hide();
    $('#AddnewSO').hide();
    $('#AddLookup').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#ViewLookup').show();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#kendoUISSC').hide();
    $('#scannerRegst').hide();
    $('#sscMapping').hide();
    $('#ViewLookupGrid').jqGrid('GridUnload');
    AppViewModel.selectedMenu('Data Lookup >> View look up');
    AppViewModel.selectedLookupSubsyValue('');
    AppViewModel.availableLookupSubsy('');
    AppViewModel.selectedMapperValue('');
    AppViewModel.mapperforSubsy([]);
    getAllSubsies();
    getAllViewLookupNames();

};

function checkAddedLookupValue() {


}

function getAllSubsies() {

    debugger

    uiBlocker();
    $.ajax({
        type: "GET",
        //url: baseUrl + '/api/User/AllSubsies?previlegeCode=1',
        url: baseUrl + '/api/User/AllSubsies',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.availableLookupSubsy('');
                AppViewModel.availableLookupSubsy(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Subsi List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

};

function getAllLookupNames() {

    debugger

    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Lookup/GetAllLookupNames',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.selectedLookupValue('');
                AppViewModel.lookupNames([]);
                AppViewModel.lookupNames(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Lookup List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

};


function getAllViewLookupNames() {

    debugger

    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Lookup/GetAllLookupNames',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.ViewlookupNames([]);
                AppViewModel.ViewlookupNames(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Lookup List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

};





AppViewModel.selectedLookupSubsyValue.subscribe(function (newValue) {
    
    if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
    console.log(newValue.length);
    debugger
    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Lookup/AllMapperforSelectedSubsy?subsiCode=' + newValue[0].SubsiId,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.selectedMapperValue('');
                AppViewModel.mapperforSubsy([]);
                AppViewModel.mapperforSubsy(data);
            }
            else {
                AppViewModel.selectedMapperValue('');
                AppViewModel.mapperforSubsy([]);           
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "No Mapper for the selected subsi",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

});

AppViewModel.selectedMapperValue.subscribe(function (newValue) {
    if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Lookup/GetisMapperASN?mapperName=' + newValue[0].MapperCode,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.Status) {
            var list = UpdateArray(AppViewModel.lookupNames());
            console.log(list);
            AppViewModel.lookupNames([]);
            AppViewModel.lookupNames(list);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
});
function UpdateArray(oldArray) {    
    for(var i=0;i<oldArray.length;i++){
        if (oldArray[i].EntityName == "Consignee") {            
            oldArray[i].EntityName = "From Location(Consignee)";
        }
        if (oldArray[i].EntityName == "Packer") {
            oldArray[i].EntityName = "To Location(Consignee)";
        }
    }
    console.log(oldArray);
    return oldArray;
}

function postLookupData(postArr) {
    uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/Lookup/AddLookup',
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(postArr),
        success: function (data) {
            $.unblockUI();
            if (data.Success == true) {

                var LookupRespMessage = "<div style='color:green'>" + data.Message + "</div>";

                var postLookupAlt = noty({
                    layout: 'top',
                    type: alert,
                    text: LookupRespMessage,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });

                var tableRef = document.getElementById('LookupTable').getElementsByTagName('tbody')[0];
                var tableRows = tableRef.getElementsByTagName('tr');
                var rowCount = tableRows.length;
                for (var x = rowCount - 1; x >= 0; x--) {
                    tableRef.removeChild(tableRows[x]);
                }

            }
            else {

                var postSOErrAlt = noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });
            }

            $.unblockUI();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}