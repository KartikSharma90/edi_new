AppViewModel.selectedField.subscribe(function (newValue) {
     debugger;
     $("#dateRow").hide();
     $("#assignValue").hide();
     if (newValue != '') {
 
         AppViewModel.IsDateFormatEnabled(true);
         AppViewModel.IsAssignValueEnabled(true);
 
 
         if (AppViewModel.IsDateFormatEnabled()) {
             if (AppViewModel.selectedField()[0].InputType == "DATE") {
                 $("#dateRow").show();
             }
             else {
                 $("#dateRow").hide();
             }
         }
         //else {
         //    $("#dateRow").hide();
         //}
         if (AppViewModel.IsAssignValueEnabled()) {
             if (AppViewModel.selectedField()[0].InputType == "OPERATION_ON_SOURCE_FIELD_OR_USER_FIXED") {
                 $("#assignValue").show();
             }
             else {
                 $("#assignValue").hide();
             }
 
         }
         //else {
         //    $("#dateRow").hide();
         //    $("#assignValue").hide();
 
         //}
     }
 });
 
 AppViewModel.selectedFileType.subscribe(function (newValue) {
     if (newValue != null) {
         switch (newValue.Name) {
             case "CSV": $('#customInputFile').show();
                 $('#fieldCount').show();
                 $('#cb2').prop('checked', false);
                 $('#rowNum').hide();
                 $('#colNum').hide();
                 break;
             case "Excel":
                 $('#customInputFile').show();
                 $('#fieldCount').show();
                 $('#cb2').prop('checked', false);
                 $('#rowNum').hide();
                 $('#colNum').hide();
                 break;
             case "Fixed Field Length":
                 $('#customInputFile').hide();
                 $('#fieldCount').hide();
                 $('#cb2').prop('checked', false);
                 $('#rowNum').hide();
                 $('#colNum').hide();
                 break;
             case "SemiColon Delimited":
                 $('#customInputFile').hide();
                 $('#fieldCount').show();
                 $('#cb2').prop('checked', false);
                 $('#rowNum').hide();
                 $('#colNum').hide();
                 break;
 
         }
     }
 
 });
 
 this.onUploadMapperNextClick = function () {
 
     if (AppViewModel.isEnableEmail()) {
         if (AppViewModel.emailIds().length != 0) {
             var emailIdElement = AppViewModel.emailIds().split(',');
             var isValid = true;
             for (var i = 0; i < emailIdElement.length; i++) {
                 if (!emailValidation(emailIdElement[i])) {
                     isValid = false;
                     var emailAlt = noty({
                         layout: 'top',
                         type: 'error',
                         text: "Please Enter a Valid Email.",
                         dismissQueue: true,
                         animation: {
                             open: { height: 'toggle' },
                             close: { height: 'toggle' },
                             easing: 'swing',
                             speed: 500
                         },
                         timeout: 5000
                     });
                     break;
                 }
             }
             if (isValid) {                
                 UploadMapper();
             }
         }
         else {
             noty({
                 layout: 'top',
                 type: 'error',
                 text: "Email ids cannot be null.Please enter email ids",
                 dismissQueue: true,
                 animation: {
                     open: { height: 'toggle' },
                     close: { height: 'toggle' },
                     easing: 'swing',
                     speed: 500
                 },
                 timeout: 5000
             });
         }
     }
     else {
         if (AppViewModel.isEnableFTPForMapper()) {
             if (AppViewModel.FTPType() == "" || AppViewModel.FTPhost() == "" || AppViewModel.FTPport() == "" || AppViewModel.FTPuser() == "" || AppViewModel.FTPpassword() == "" || AppViewModel.Ftplocation() == "") {
                 noty({
                     layout: 'top',
                     type: 'error',
                     text: "FTP mandatory fields are empty.Please Add FTP Configuration details ",
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
                 return false;
             }
         }
         UploadMapper();
     }
 
   
 };
 
 function emailValidation(email) {
     var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     if (!filter.test(email)) {
         return false;
     }
     else {
         return true;
     }
 }
 
 function UploadMapper() {
     AppViewModel.mappedHeaders([]);
     AppViewModel.dateFormat('');
     AppViewModel.mapFileName('');
     var MappingModel = {
         SubsiId: AppViewModel.selectedSubsiValue().SubsiId,
         TransactionId: AppViewModel.selectedTransaction().Id,
         FileTypeId: AppViewModel.selectedFileType().Id,
         FieldCountValue: AppViewModel.selectedFieldCount(),
         IsCustomizedInput: AppViewModel.isCustomInput(),
         RowNumber: AppViewModel.rowNumber(),
         ColumnNumber: AppViewModel.columnNumber(),
         EnableEmail: AppViewModel.isEnableEmail(),
         EmailIds: AppViewModel.emailIds()
     }
 
     var ext = AppViewModel.uploadFileName().split('.').pop().toLowerCase();
     var isFileType = false;
     switch (AppViewModel.selectedFileType().Name) {
         case "Excel": if ((ext.toLowerCase() == "xls") || (ext.toLowerCase() == "xlsx")) {
             isFileType = true;
         }
         else {
 
             isFileType = false;
         }
             break;
         case "CSV": if (ext.toLowerCase() == "csv") {
             isFileType = true;
         }
         else {
             isFileType = false;
         } break;
         case "Fixed Field Length": if (ext.toLowerCase() == "txt") {
             isFileType = true;
         }
         else {
             isFileType = false;
         } break;
         case "SemiColon Delimited":
             if ((ext.toLowerCase() == "txt") || (ext.toLowerCase() == "csv")) {
                 isFileType = true;
             }
             else {
                 isFileType = false;
             } break;
         default: break;
 
     }
 
     if (isFileType == false) {
         noty({
             layout: 'top',
             type: 'error',
             text: "Please select a .csv, .txt, .xls or .xlsx file based on File Type selected",
             dismissQueue: true,
             animation: {
                 open: { height: 'toggle' },
                 close: { height: 'toggle' },
                 easing: 'swing',
                 speed: 500
             },
             timeout: 5000
         });
     }
     else {
 
 
       
         if (AppViewModel.uploadFileName() == "") {
             noty({
                 layout: 'top',
                 type: 'error',
                 text: "Please upload the sample file for mapping",
                 dismissQueue: true,
                 animation: {
                     open: { height: 'toggle' },
                     close: { height: 'toggle' },
                     easing: 'swing',
                     speed: 500
                 },
                 timeout: 5000
             });
             AppViewModel.subsies([]);
         }
         else {
             uiBlocker();
             if (AppViewModel.isEnableEmail()) {
                 $.ajax({
                     type: "GET",
                     url: baseUrl + '/api/Mapping/EmailMapper?emailIds=' + AppViewModel.emailIds() + '&transactionId=' + AppViewModel.selectedTransaction().Id,
                     contentType: 'application/json; charset=utf-8',
                     success: function (data) {
                         $.unblockUI();
                         if (data.Success) {
                             uploadFileData(MappingModel)
                         }
                         else {
                             noty({
                                 layout: 'top',
                                 type: 'error',
                                 text: "" + data.Message,
                                 dismissQueue: true,
                                 animation: {
                                     open: { height: 'toggle' },
                                     close: { height: 'toggle' },
                                     easing: 'swing',
                                     speed: 500
                                 },
                                 timeout: 5000
                             });
 
                         }
 
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                         $.unblockUI();
                     }
 
                 });
 
             }
             else {
                 uploadFileData(MappingModel)
             }
         }
     }
 }
 
 function uploadFileData(MappingModel) {
     debugger;
         var formData = new FormData($('#uploadform')[0]);
         $.ajax({
 
             type: 'POST',
             url: baseUrl + '/api/Mapping/PostFormData',  //server script to process data
             xhrFields: {
                 withCredentials: true
             },
 
             // Form data
             data: formData,
             //Options to tell JQuery not to process data or worry about content-type
             cache: false,
             contentType: false,
             processData: false,
             async: true,
             success: function (r) {
                 $.unblockUI();
                 $.ajax({
                     type: "GET",
                     url: baseUrl + '/api/Mapping/SAPFields?transactionId=' + AppViewModel.selectedTransaction().Id,
                     contentType: 'application/json; charset=utf-8',
                     success: function (data) {
                         if (data != null) {
                             AppViewModel.sapFields([]);
                             //if (data.val == "Customer PO date")
                             //{
                             //    data = data - "Customer PO date";
                             //}
                             //alert(data);
                             AppViewModel.sapFields(data);
                             AppViewModel.sapMappedFields(AppViewModel.sapFields().slice());
 
                             AppViewModel.sapFields([]);
                             for (var i = 0; i < data.length; i++) {
                                 if (data[i].IsDisplay == true) {
                                     debugger;
                                     if (data[i].FieldName == "Customer PO date") {
                                         continue;
                                     }
                                     else {
                                         AppViewModel.sapFields.push(data[i]);
                                     }
 
                                 }
                             }
                             $.each(AppViewModel.sapFields(), function (index, value) {
                                 var cur;
                                 cur = $('#sapFieldData option:eq(' + index + ')');
                                 cur.attr('title', value.Note);
                                 if (value.IsMandatory) {
                                     cur.css("color", "red");
 
                                 }
                                 else {
                                     cur.css("color", "black");
                                 }
                             });
                             //  $('#sapFieldData option[value=option2]').hide();
                         } else {
                             $.unblockUI();
                         }
                     
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                         $.unblockUI();
                     }
 
 
                 });
                 uiBlocker();
                 $.ajax({
                     type: "POST",
                     url: baseUrl + '/api/Mapping/HeaderFields',
                     data: ko.toJSON(MappingModel),
                     contentType: 'application/json; charset=utf-8',
                     success: function (data) {
                         $.unblockUI();
                         AppViewModel.headers([]);
                         AppViewModel.headers(data);
                         $('#welcomeScreen').hide();
                         $('#UploadMapper').hide();
                         $('#roleList').hide();
                         $('#userList').hide();
                         $('#UploadMapper').hide();
                         $('#scMapping').hide();
                         $('#viewMappingDetails').hide();
                         $('#sapMapping').show();
                         $('#emailIdRow').hide();
                         $('#backToMapper').hide();
                         $('#assignValue').hide();
                     },
                     error: function (jqXHR, textStatus, errorThrown) {
                         $.unblockUI();
                     }
                 });
             },
 
             error: function (r) {
                 $.unblockUI();
             }
         });
     }
 
 this.rightHeaderArrow = function () {
     if (AppViewModel.selectedHeader().length > 0) {
         AppViewModel.mappedHeaders.push({ SRNO: AppViewModel.selectedHeader()[0].SRNO, HeaderName: AppViewModel.selectedHeader()[0].HeaderName, Id: AppViewModel.selectedField()[0].Id, FieldName: AppViewModel.selectedField()[0].FieldName, InputType: AppViewModel.selectedField()[0].InputType, FieldSequence: AppViewModel.selectedField()[0].FieldSequence, IsMandatory: AppViewModel.selectedField()[0].IsMandatory, MappedField: AppViewModel.selectedField()[0].FieldName + " ---> " + AppViewModel.selectedHeader()[0].HeaderName });
         AppViewModel.sapFields.remove(AppViewModel.selectedField()[0]);
         AppViewModel.selectedHeader([]);
 
         $.each(AppViewModel.sapFields(), function (index, value) {
             var cur;
             cur = $('#sapFieldData option:eq(' + index + ')');
             cur.attr('title', value.Note);
             if (value.IsMandatory) {
                 cur.css("color", "red");
             }
             else {
                 cur.css("color", "black");
             }
         });
     }
     
 };
 
 this.leftHeaderArrow = function () {
     //var l = AppViewModel.selectedMappedHeaders().length;
     //alert(heart);
     if (AppViewModel.selectedMappedHeaders().length > 0) {
         //AppViewModel.sapFields.push({ Id: AppViewModel.selectedMappedHeaders()[0].Id, FieldName: AppViewModel.selectedMappedHeaders()[0].FieldName , Note: AppViewModel.selectedField()[0].Note, InputType: AppViewModel.selectedField()[0].InputType, FieldSequence: AppViewModel.selectedField()[0].FieldSequence });
         AppViewModel.sapFields.push({ Id: AppViewModel.selectedMappedHeaders()[0].Id, FieldName: AppViewModel.selectedMappedHeaders()[0].FieldName, Note: AppViewModel.selectedMappedHeaders()[0].Note, InputType: AppViewModel.selectedMappedHeaders()[0].InputType, FieldSequence: AppViewModel.selectedMappedHeaders()[0].FieldSequence, IsMandatory: AppViewModel.selectedMappedHeaders()[0].IsMandatory });
         AppViewModel.mappedHeaders.remove(AppViewModel.selectedMappedHeaders()[0]);
         AppViewModel.selectedMappedHeaders([]);
         $.each(AppViewModel.sapFields(), function (index, value) {
             var cur;
             cur = $('#sapFieldData option:eq(' + index + ')');
             cur.attr('title', value.Note);
             if (value.IsMandatory) {
                 cur.css("color", "red");
             }
             else {
                 cur.css("color", "black");
             }
         });
     }
 
 };
 
 this.assignValueClick = function () {
     AppViewModel.assignValue('');
     $("#assignValueForm").dialog("open");
 };
 
 this.mappingSaveClick = function () {
     debugger;
     var IsEmailValid = "true";
     AppViewModel.IsDateFormatEnabled(true);
     var anyElementPresent = false;
     $.each(AppViewModel.sapFields(), function (index, value) {
 
         if (value.IsMandatory) {
             anyElementPresent = true;
         }
     });
 
 
     if (AppViewModel.mappedHeaders().length != 0) {
 
         if (anyElementPresent) {
 
             noty({
                 layout: 'top',
                 type: 'error',
                 text: "Please map all mandatory fields.",
                 dismissQueue: true,
                 animation: {
                     open: { height: 'toggle' },
                     close: { height: 'toggle' }, 
                     easing: 'swing',
                     speed: 500
                 },
                 timeout: 5000
             });
         }
 
 
 
         else {
             if ((AppViewModel.dateFormat() == '') && (AppViewModel.IsDateFormatEnabled())) {
                 $("#dateRow").show();
                 noty({
                     layout: 'top',
                     type: 'error',
                     text: "Please enter the date format",
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
 
             else {
                 if (AppViewModel.emailIds() != '') {
                     var emailids = AppViewModel.emailIds();
                     var textEmailIds = $('textarea#multiemailids').val();
                     var emailid = new Array();
                     var emailid = textEmailIds.split(",");
                     var IsEmailValid = "true";
                     for (var i = 0; i < emailid.length; i++) {
                         var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                         if (!filter.test(emailid[i])) {
                             //  $('#emailIdRow').show();
                             IsEmailValid = "false";
                             noty({
                                 layout: 'top',
                                 type: 'error',
                                 text: "Please enter the valid email id",
                                 dismissQueue: true,
                                 animation: {
                                     open: { height: 'toggle' },
                                     close: { height: 'toggle' },
                                     easing: 'swing',
                                     speed: 500
                                 },
                                 timeout: 5000
                             });
                         }
                     }
                 }
                 if (AppViewModel.mapFileName() == '') {
                     //  AppViewModel.mapFileName('');
                     // alert("no mapfilename");
                     $("#mapFileSaveForm").dialog("open");
                 }
                 //   else {
                 if (AppViewModel.mapFileName() != '' && IsEmailValid == "true") {
                     //  alert("Edit Mapping");
                     EditMapping();
                 }
             }
         }
     }
     else {
         noty({
             layout: 'top',
             type: 'error',
             text: "Please map the headers",
             dismissQueue: true,
             animation: {
                 open: { height: 'toggle' },
                 close: { height: 'toggle' },
                 easing: 'swing',
                 speed: 500
             },
             timeout: 5000
         });
     }
 };
 
 function SaveMapping() {
     uiBlocker();
     var mappedModel = {
         MappedFileModel: AppViewModel.mappedHeaders(),
         SubsiId: AppViewModel.selectedSubsiValue().SubsiId,
         TransactionId: AppViewModel.selectedTransaction().Id,
         FileTypeId: AppViewModel.selectedFileType().Id,
         FieldCountValue: AppViewModel.selectedFieldCount(),
         IsCustomizedInput: AppViewModel.isCustomInput(),
         RowNumber: AppViewModel.rowNumber(),
         ColumnNumber: AppViewModel.columnNumber(),
         FileHeaders: AppViewModel.headers(),
         SAPFieldHeaders: AppViewModel.sapMappedFields(),
         DateFormat: AppViewModel.dateFormat(),
         FileName: AppViewModel.mapFileName(),
         EnableEmail: AppViewModel.isEnableEmail(),
         EmailIds: AppViewModel.emailIds(),
         FtpPort: AppViewModel.FTPport(),
         FtpIsEnabledForMapper: AppViewModel.isEnableFTPForMapper(),
         IsFtpEnable: AppViewModel.FTPisenabled(),
         FtpType: AppViewModel.FTPType(),
         FtpHost: AppViewModel.FTPhost(),
         FtpUser: AppViewModel.FTPuser(),
         FtpPassword: AppViewModel.FTPpassword(),
         FtpEncryptiontype: AppViewModel.FTPencryptiontype(),
         FtpSslHostCertificateFingerprint: AppViewModel.FTPsslhostcertificatefingerprint(),
         FtpLocation: AppViewModel.Ftplocation
     }
     $.ajax({
         type: "POST",
         url: baseUrl + '/api/Mapping/MappedData',
         contentType: 'application/json; charset=utf-8',
         data: ko.toJSON(mappedModel),
         success: function (data) {
             if (data.Success) {
                 $.unblockUI();
                 AppViewModel.isEnableEmail(false)
                 EmailIds: AppViewModel.emailIds('')
                 AppViewModel.mappedHeaders([]);
                 AppViewModel.selectedSubsiValue('');
                 AppViewModel.selectedTransaction('');
                 AppViewModel.selectedFileType('');
                 AppViewModel.selectedFieldCount('');
                 AppViewModel.isCustomInput(false);
                 AppViewModel.rowNumber('');
                 AppViewModel.columnNumber('');
                 AppViewModel.headers([]);
                 AppViewModel.sapMappedFields([]);
                 AppViewModel.dateFormat('');
                 AppViewModel.mapFileName('');
                 $('#login').hide();
                 $('#assignValueForm').dialog("close");
                 $('#mapFileSaveForm').dialog("close");
                 $('#UploadMapper').hide();
                 $('#roleList').hide();
                 $('#userList').hide();
                 $('#viewMapping').hide();
                 $('#UploadMapper').hide();
                 $('#scMapping').hide();
                 $('#sapMapping').hide();
                 $('#viewMappingDetails').hide();
                 $('#UploadMapper').hide();
                 $('#viewConfig').hide();
                 $('#configDelete').hide();
                 $('#home').show();
                 $('#welcomeScreen').show();
                 noty({
                     layout: 'top',
                     type: 'success',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
             else {
                 $.unblockUI();
                 noty({
                     layout: 'top',
                     type: 'error',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
         }
     });
 
 
 }
 
 function EditMapping() {
     uiBlocker();
     var mappedModel = {
         MappedFileModel: AppViewModel.mappedHeaders(),
         SubsiId: AppViewModel.SubsiId(),
         TransactionId: AppViewModel.TransactionId(),
         FileTypeId: AppViewModel.FileTypeId(),
         FieldCountValue: AppViewModel.FieldCountValue(),
         FileHeaders: AppViewModel.headers(),
         SAPFieldHeaders: AppViewModel.sapHeaderFields(),
         DateFormat: AppViewModel.dateFormat(),
         FileName: AppViewModel.mapFileName(),
         FilePath: AppViewModel.FilePath(),
         SRNO: AppViewModel.SRNO(),
         FieldId: AppViewModel.FieldId(),
         EmailIds: AppViewModel.emailIds(),
         MapperId: AppViewModel.mapperId(),
         FtpPort: AppViewModel.FTPport(),
         FtpIsEnabledForMapper: AppViewModel.isEnableFTPForMapper(),
         IsFtpEnable: AppViewModel.FTPisenabled(),
         FtpType: AppViewModel.FTPType(),
         FtpHost: AppViewModel.FTPhost(),
         FtpUser: AppViewModel.FTPuser(),
         FtpPassword: AppViewModel.FTPpassword(),
         FtpEncryptiontype: AppViewModel.FTPencryptiontype(),
         FtpSslHostCertificateFingerprint: AppViewModel.FTPsslhostcertificatefingerprint()
     }
 
 
     $.ajax({
         type: "POST",
         url: baseUrl + '/api/Mapping/EditMappedData',
         contentType: 'application/json; charset=utf-8',
         data: ko.toJSON(mappedModel),
         success: function (data) {
             if (data.Success) {
                 $.unblockUI();
                 AppViewModel.mappedHeaders([]);
                 AppViewModel.SubsiId(''),
                 AppViewModel.TransactionId(''),
                 AppViewModel.FileTypeId(''),
                 AppViewModel.FieldCountValue(''),
                  AppViewModel.headers([]),
                 AppViewModel.sapHeaderFields([]),
                 AppViewModel.dateFormat('');
                 AppViewModel.mapFileName('');
                 AppViewModel.FilePath('');
                 AppViewModel.emailIds([]);
                 AppViewModel.mapperId('');
                 $('#login').hide();
                 $('#assignValueForm').dialog("close");
                 $('#mapFileSaveForm').dialog("close");
                 $('#UploadMapper').hide();
                 $('#roleList').hide();
                 $('#userList').hide();
                 $('#viewMapping').hide();
                 $('#UploadMapper').hide();
                 $('#scMapping').hide();
                 $('#sapMapping').hide();
                 $('#viewMappingDetails').hide();
                 $('#deleteMapping').hide();
                 $('#UploadMapper').hide();
                 $('#viewConfig').hide();
                 $('#configDelete').hide();
                 $('#home').show();
                 $('#welcomeScreen').show();
                 noty({
                     layout: 'top',
                     type: 'success',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
             else {
                 $.unblockUI();
                 noty({
                     layout: 'top',
                     type: 'error',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
             var noData = noty({
                 layout: 'top',
                 type: 'error',
                 text: "Error fetching mapper details",
                 dismissQueue: true,
                 animation: {
                     open: { height: 'toggle' },
                     close: { height: 'toggle' },
                     easing: 'swing',
                     speed: 500
                 },
                 timeout: 5000
             });
         }
     });
 }
 
 function ViewMapperEditButtonClick(RowId) {
 
     debugger
 
     GetFileDetails(RowId);
 }
 
 function GetFileDetails(RowId) {
 
     debugger
 
     uiBlocker();
     $.ajax({
         type: "GET",
         url: baseUrl + '/api/Mapping/GetFilDetailsForEdit?susbsyToTransactionMapperId=' + RowId,
         contentType: 'application/json; charset=utf-8',
         success: function (data) {
             AppViewModel.mapFileName('');
             AppViewModel.SubsiId('');
             AppViewModel.TransactionId('');
             AppViewModel.FileTypeId('');
             AppViewModel.FieldCountValue('');
             AppViewModel.FilePath('');
             AppViewModel.dateFormat('');
             AppViewModel.mapperId('');
             AppViewModel.isEnableFTPForMapper(false);
             AppViewModel.FTPType('');
             AppViewModel.FTPhost('');
             AppViewModel.FTPport('');
             AppViewModel.FTPuser('');
             AppViewModel.Ftplocation('');
             AppViewModel.FTPpassword('');
             AppViewModel.FTPencryptiontype('');
             AppViewModel.FTPsslhostcertificatefingerprint('');
             AppViewModel.FTPisenabled(false);
 
             AppViewModel.mapFileName(data[0].FileName);
             AppViewModel.SubsiId(data[0].SubsiId);
             AppViewModel.TransactionId(data[0].TransactionId);
             AppViewModel.FileTypeId(data[0].FileTypeId);
             AppViewModel.FieldCountValue(data[0].FieldCountValue);
             AppViewModel.FilePath(data[0].FilePath);
             AppViewModel.dateFormat(data[0].DateFormat);
             AppViewModel.mapperId(data[0].MapperId);
             AppViewModel.isEnableFTPForMapper(data[0].FtpIsEnabledForMapper);
             AppViewModel.FTPType(data[0].FtpType);
             AppViewModel.FTPhost(data[0].FtpHost);
             AppViewModel.FTPport(data[0].FtpPort);
             AppViewModel.FTPuser(data[0].FtpUser);
             AppViewModel.Ftplocation(data[0].FtpLocation);
             AppViewModel.FTPpassword(data[0].FtpPassword);
             AppViewModel.FTPencryptiontype(data[0].FtpEncryptiontype);
             AppViewModel.FTPsslhostcertificatefingerprint(data[0].FtpSslHostCertificateFingerprint);
             AppViewModel.FTPisenabled(data[0].IsFtpEnable);
 
             $("#dateRow").show();
             $('#backToMapper').show();
             GetFileHeaders(RowId);
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
             var noData = noty({
                 layout: 'top',
                 type: 'error',
                 text: "Error fetching mapper details",
                 dismissQueue: true,
                 animation: {
                     open: { height: 'toggle' },
                     close: { height: 'toggle' },
                     easing: 'swing',
                     speed: 500
                 },
                 timeout: 5000
             });
         }
     });
 }
 function GetFileHeaders(RowId) {
     $.ajax({
         type: "GET",
         url: baseUrl + '/api/Mapping/GetFileHeaders?susbsyToTransactionMapperId=' + RowId,
         contentType: 'application/json; charset=utf-8',
         success: function (data) {
             AppViewModel.headers([]);
             AppViewModel.headers(data);
             GetSapFields(RowId);
 
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
         }
     });
 }
 function GetSapFields(RowId) {
     $.ajax({
         type: "GET",
         url: baseUrl + '/api/Mapping/SAPFields?TransactionId=' + AppViewModel.TransactionId(),
         contentType: 'application/json; charset=utf-8',
         success: function (data) {
             AppViewModel.sapHeaderFields([]);
             AppViewModel.sapHeaderFields(data);
             GetEmailIds(RowId);
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
         }
     });
 }
 function GetEmailIds(RowId) {
     $.ajax({
         type: "GET",
         url: baseUrl + '/api/Mapping/GetEmailDetailsForEdit?susbsyToTransactionMapperId=' + RowId,
         contentType: 'application/json; charset=utf-8',
         success: function (data) {
             AppViewModel.emailIds([]);
             if (data != null) {
                 for (var i = 0; i < data.length; i++) {
                     AppViewModel.emailIds.push(data[i].Emailid);
                 }
             }
             GetMappedDetails(RowId);
             $('#emailIdRow').show();
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
         }
     });
 
 }
 function GetMappedDetails(RowId) {
     $.ajax({
         url: baseUrl + '/api/Mapping/GetMappedDetails?susbsyToTransactionMapperId=' + RowId,
         type: 'GET',
         contentType: 'application/json; charset=utf-8',
         success: function (data) {
             $.unblockUI();
             AppViewModel.sapFields([]);
             AppViewModel.mappedHeaders([]);
             for (var i = 0; i < data.length; i++) {
                 AppViewModel.mappedHeaders.push({ SRNO: data[i].SRNO, Id: data[i].Id, HeaderName: data[i].HeaderName, FieldName: data[i].FieldName, InputType: data[i].InputType, FieldSequence: data[i].FieldSequence, Note: data[i].Note, SubsiId: data[i].SubsiId, FileTypeId: data[i].FileTypeId, FieldCountValue: data[i].FieldCountValue, TransactionId: data[i].TransactionId, DateFormat: data[i].DateFormat, MappedField: data[i].FieldName + " ---> " + data[i].HeaderName });
                 $('#login').hide();
                 $('#assignValueForm').dialog("close");
                 $('#mapFileSaveForm').dialog("close");
                 $('#UploadMapper').hide();
                 $('#roleList').hide();
                 $('#userList').hide();
                 $('#viewMapping').hide();
                 $('#UploadMapper').hide();
                 $('#scMapping').hide();
                 $('#sapMapping').hide();
                 $('#viewMappingDetails').hide();
                 $('#deleteMapping').hide();
                 $('#UploadMapper').hide();
                 $('#home').show();
                 $('#welcomeScreen').hide();
                 $('#assignValue').hide();
                 $('#viewConfig').hide();
                 $('#configDelete').hide();
                 $('#sapMapping').show();
             }
 
 
         }
     }).fail(function (xhr, textStatus, err) {
         alert(err);
     });
 
 }
 function onBackBtnToMappingClick() {
     $('#viewMapping').show();
     $('#sapMapping').hide();
 
 }
 
 function ViewMapperDeleteButtonClick(RowId) {
     deletemapping(RowId);
 }
 function deletemapping(RowId) {
     //  $("#dialog-deletemapping").dialog({
     $("#deleteMappingForm").dialog({
         title: "Are you sure you want to delete the selected Mapping?",
         autoOpen: true,
         height: 200,
         zIndex: 6000,
         modal: true,
         close: function (event, ui) {
             $('#viewMapping').show();
         },
         show: {
             effect: "blind",
             duration: 100
         },
         hide: {
             effect: "blind",
             duration: 100
         },
         buttons: {
             "Confirm Delete":
                 function () {
                     $.ajax({
                         url: baseUrl + '/api/Mapping/DeleteMapping?susbsyToTransactionMapperId=' + RowId,
                         cache: false,
                         type: 'POST',
                         contentType: 'application/json; charset=utf-8',
                         success: function (data) {
                             $.unblockUI();
                             loadViewMappingGrid();
                         }
                     }).fail(
                          function (xhr, textStatus, err) {
                              alert(err);
                          });
                     $(this).dialog("close");
                 },
             Cancel: function () {
                 $('#viewMapping').show();
                 $(this).dialog("close");
             }
         }
 
     });
 }
 function validateFTP()
 {
     var mappingModel = {
         Port: AppViewModel.FTPport(),       
         Name: AppViewModel.FTPType(),
         Host: AppViewModel.FTPhost(),
         User: AppViewModel.FTPuser(),
         Password: AppViewModel.FTPpassword(),
         EncryptionType: AppViewModel.FTPencryptiontype(),
         SslHostCertificateFingerprint: AppViewModel.FTPsslhostcertificatefingerprint(),
         Location: AppViewModel.Ftplocation
     }
     $.ajax({
         type: "POST",
         url: baseUrl + '/api/Mapping/ValidateFTP',
         contentType: 'application/json; charset=utf-8',
         data: ko.toJSON(mappingModel),
         async:false,
         success: function (data) {
             if (data.Success) {
                 $.unblockUI();               
                 noty({
                     layout: 'top',
                     type: 'success',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
             else {
                 $.unblockUI();
                 noty({
                     layout: 'top',
                     type: 'error',
                     text: "" + data.Message,
                     dismissQueue: true,
                     animation: {
                         open: { height: 'toggle' },
                         close: { height: 'toggle' },
                         easing: 'swing',
                         speed: 500
                     },
                     timeout: 5000
                 });
             }
         },
         error: function (jqXHR, textStatus, errorThrown) {
             $.unblockUI();
         }
     });
 
 }

GO 

