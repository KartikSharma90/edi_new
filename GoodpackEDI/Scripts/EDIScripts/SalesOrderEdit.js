﻿
function massEditReason() {
    debugger;
    AppViewModel.editSOQty('');
    //  document.getElementById("editFields").style.display = 'block';
    var currentDate = new Date();
    //$("#reqDate").datepicker({
    //    dateFormat: 'dd.mm.yy',
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    $('#divreqDate').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    //$("#reqDate").datepicker("setDate", currentDate);
    $("#SalesOrderEditGrid").trigger("reloadGrid");
    // AppViewModel.selectedBINValue('');

  
}
function refresh() {
    debugger;
    AppViewModel.selectedBIN([]);
    AppViewModel.selectedBINValue('');
    AppViewModel.selectedBIN([ { "name": "MB3", "value": "MB3" }, { "name": "MB4", "value": "MB4" }, { "name": "MB5", "value": "MB5" }, { "name": "MB7", "value": "MB7" }, { "name": "MB5H", "value": "MB5H" }, { "name": "MB5HE", "value": "MB5HE" }])
    AppViewModel.editSOQty('');
   
    //$("#reqDate").datepicker({
    //    dateFormat: 'dd.mm.yy',
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    //$("#reqDate").datepicker("setDate", "");
    $('#reqDate').val('');

}

function massEditSubmitClick() {
    var skipDailog = false;
    var newSOEditData = new Array();
    document.getElementById("r1").style.display = 'block';
    document.getElementById("r2").style.display = 'block';
    document.getElementById("r3").style.display = 'block';
   
    debugger;
    var selectedIds = $("#SalesOrderEditGrid").jqGrid('getGridParam', 'selarrrow');
    var isMatNoeditable = false;
    var isSOQtyeditable = false;
    var isReqDateditable = false;
    var isSOQtyzero = false;
    var ids = $("#SalesOrdereditGrid").jqGrid('getDataIDs');
    for (var i = 0; i < selectedIds.length; i++) {
        var rowId = selectedIds[i];
        var selectedRowData = $("#SalesOrderEditGrid").getRowData(rowId);
        mNo = selectedRowData.matNm;
        ddate = selectedRowData.vdatu;
        oqty = selectedRowData.cuOrdQty;
        if (AppViewModel.selectedBINValue() != "") {
            if (mNo != AppViewModel.selectedBINValue()[0].value && isMatNoeditable == false) {
                isMatNoeditable = true;
            }
        }
        if(oqty != AppViewModel.editSOQty() && isSOQtyeditable == false)
        {
            if (AppViewModel.editSOQty() != "")
                isSOQtyeditable = true;
            if (AppViewModel.editSOQty() == '0') {
                
                isSOQtyzero = true;
            }
        }
        if (REQdateFormatter(ddate) != $("#reqDate").val() && isReqDateditable == false)
        {
            if ($("#reqDate").val() != "")
                isReqDateditable = true;
        }
    }


    
    if (AppViewModel.selectedBINValue() == "" && AppViewModel.editSOQty() != '' && $("#reqDate").val() != '') {
        newSOEditData[0] = "";
        newSOEditData[1] = AppViewModel.editSOQty();
        newSOEditData[2] = $("#reqDate").val();

    }
    if (AppViewModel.selectedBINValue() == "" && AppViewModel.editSOQty() != '' && $("#reqDate").val() == '') {
        newSOEditData[0] = "";
        newSOEditData[1] = AppViewModel.editSOQty();
        newSOEditData[2] = "";

    }
    if (AppViewModel.selectedBINValue() == "" && AppViewModel.editSOQty() == '' && $("#reqDate").val() != '') {
        newSOEditData[0] = "";
        newSOEditData[1] = "";
        newSOEditData[2] = $("#reqDate").val();

    }
    if (AppViewModel.selectedBINValue() != "" && AppViewModel.editSOQty() == '' && $("#reqDate").val() == '') {
        newSOEditData[0] = AppViewModel.selectedBINValue()[0].value;
        newSOEditData[1] = "";
        newSOEditData[2] = "";
    }
    if (AppViewModel.selectedBINValue() != "" && AppViewModel.editSOQty() != '' && $("#reqDate").val() == '') {
        newSOEditData[0] = AppViewModel.selectedBINValue()[0].value;
        newSOEditData[1] = AppViewModel.editSOQty();
        newSOEditData[2] = "";
    }
    if (AppViewModel.selectedBINValue() != "" && AppViewModel.editSOQty() == '' && $("#reqDate").val() != '') {
        newSOEditData[0] = AppViewModel.selectedBINValue()[0].value;
        newSOEditData[1] = "";
        newSOEditData[2] = $("#reqDate").val();
    }
    if (AppViewModel.selectedBINValue() != "" && AppViewModel.editSOQty() != '' && $("#reqDate").val() != '') {
        newSOEditData[0] = AppViewModel.selectedBINValue()[0].value;
        newSOEditData[1] = AppViewModel.editSOQty();
        newSOEditData[2] = $("#reqDate").val();
    }
    if (AppViewModel.selectedBINValue() == "" && AppViewModel.editSOQty() == '' && $("#reqDate").val() == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Required values for any of these fields",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
         skipDailog = true;
    }   
    if (!isMatNoeditable) {
        document.getElementById("r1").style.display = 'none';
        $('#r1').hide();
    }
    if (!isSOQtyeditable) {
        document.getElementById("r2").style.display = 'none';
        $('#r2').hide();
    }
    if (!isReqDateditable) {
        document.getElementById("r3").style.display = 'none';
        $('#r3').hide();
    }
    if (!isReqDateditable && !isSOQtyeditable && !isMatNoeditable)
    {
        noty({
            layout: 'top',
            type: 'error',
            text: "No changes founded for any of these fields",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
        skipDailog = true;

    }
    if (isSOQtyzero) {
        noty({
            layout: 'top',
            type: 'error',
            text: "Quantity cannot be changed to 0, please proceed to Cancel Order if required.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
        skipDailog = true;

    }

  

    if (selectedIds == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please select atlest one row",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
        skipDailog = true;
    }
    else if( selectedIds==undefined)
    {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please do Search Orders then go for Edit Orders",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
        skipDailog = true;

    }
    else {
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SO/AllReasonsForEdit',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.length != 0) {
                    AppViewModel.availableReasonsMatNo([]);
                    AppViewModel.availableReasonsQty([]);
                    AppViewModel.availableReasonsReqDate([]);
                    var data1 = [];
                    var data2 = [];
                    var data3 = [];
                    var i = 0;
                    for (var m = 0; m < data.length; m++) {
                        if (data[m].ReasonType == "MaterialNumber") {
                            data1 = data[m];
                            AppViewModel.availableReasonsMatNo.push(data1);
                        }
                        if (data[m].ReasonType == "Quantity") {
                            data2 = data[m];
                            AppViewModel.availableReasonsQty.push(data2);
                        }
                        if (data[m].ReasonType == "ReqDate") {
                            data3 = data[m];
                            AppViewModel.availableReasonsReqDate.push(data3);
                        }
                    }
                }
                else {
                    $.unblockUI();
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Unable to retrieve Reasons",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
        if (skipDailog == false) {
            $("#somassEditReason").dialog({
                autoOpen: true,
                height: "auto",
                width: "auto", zIndex: 50000,
                modal: true,
                buttons: {
                    "OK": function () {
                        debugger;
                        uiBlocker();
                        var flag1 = AppViewModel.selectedReasonForEditMatNo() || "0";
                        var flag2 = AppViewModel.selectedReasonForEditQty() || "0";
                        var flag3 = AppViewModel.selectedReasonForEditReqDate() || "0";
                        if (flag1 != 0 && flag2 == 0 && flag3 == 0) {
                            var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                            var reason2 = "0";
                            var reason3 = "0";

                        }
                        if (flag1 != 0 && flag2 != 0 && flag3 == 0) {
                            var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                            var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                            var reason3 = "0";

                        }
                        if (flag1 != 0 && flag2 != 0 && flag3 != 0) {
                            var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                            var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                            var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                        }
                        if (flag1 == 0 && flag2 == 0 && flag3 != 0) {
                            var reason1 = "0";
                            var reason2 = "0";
                            var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                        }
                        if (flag1 == 0 && flag2 != 0 && flag3 == 0) {
                            var reason1 = "0";
                            var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                            var reason3 = "0";

                        }
                        if (flag1 == 0 && flag2 != 0 && flag3 != 0) {
                            var reason1 = "0";
                            var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                            var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                        }
                        if (flag1 != 0 && flag2 == 0 && flag3 != 0) {
                            var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                            var reason2 = "0";
                            var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                        }
                        AppViewModel.selectedLineContent([]);
                        AppViewModel.selectedDeliveryDates([]);
                        AppViewModel.selectedMaterialNos([]);
                        AppViewModel.selcetedorderQtys([]);
                        AppViewModel.selectedDelLocations([]);
                        var customerPNo;
                        var mNo;
                        var ddate;
                        var oqty;
                        var reason1Array=new Array();var reason2Array=new Array();var reason3Array=new Array();
                        for (var i = 0; i < selectedIds.length; i++) {
                            var rowId = selectedIds[i];
                            var selectedRowData = $("#SalesOrderEditGrid").getRowData(rowId);
                            customerPNo = selectedRowData.custPrchOrdNo;
                            mNo = selectedRowData.matNm;
                            ddate = selectedRowData.vdatu;
                            oqty = selectedRowData.cuOrdQty;
                            delLoc = selectedRowData.packCustNum;                          
                            if (mNo != (newSOEditData[0] == "" ? mNo : newSOEditData[0]) || oqty !=( newSOEditData[1] == "" ? oqty : newSOEditData[1]) || REQdateFormatter(ddate) != (newSOEditData[2] == "" ? REQdateFormatter(ddate) : newSOEditData[2]))
                           { //if (newSOEditData[0] != mNo || newSOEditData[1] != oqty || newSOEditData[2] != REQdateFormatter(ddate)) {                                
                                AppViewModel.selectedLineContent.push(customerPNo);
                                AppViewModel.selectedDelLocations.push(delLoc);
                                AppViewModel.selectedDeliveryDates.push(ddate);
                                AppViewModel.selectedMaterialNos.push(mNo);
                                AppViewModel.selcetedorderQtys.push(oqty);
                                if (newSOEditData[0] == mNo)
                                    reason1Array.push("0")
                                else
                                    reason1Array.push(reason1);
                                if (newSOEditData[1] == oqty)
                                    reason2Array.push("0");
                                else
                                    reason2Array.push(reason2);
                                if (newSOEditData[2] == REQdateFormatter(ddate))
                                    reason3Array.push("0");
                                else
                                    reason3Array.push(reason3);
                            }
                        }

                        var EditSOModel = {
                            customerCode:AppViewModel.edtitCustomerCode(),
                            customerPNos: AppViewModel.selectedLineContent(),
                            deliveryLocations:AppViewModel.selectedDelLocations(),
                            delDates: AppViewModel.selectedDeliveryDates(),
                            matNos: AppViewModel.selectedMaterialNos(),
                            orderQtys: AppViewModel.selcetedorderQtys(),
                            materialNo: newSOEditData[0],
                            orderQty: newSOEditData[1],
                            deliveryDt: newSOEditData[2],
                            reason1: reason1Array,
                            reason2: reason2Array,
                            reason3: reason3Array

                        }
                        $.ajax({
                            type: "POST",
                            url: baseUrl + '/api/SO/massEditSalesOrder',
                            data: ko.toJSON(EditSOModel),
                            contentType: 'application/json; charset=utf-8',
                            success: function (data) {
                                if (data.Success == true) {
                                    $('#SalesOrderEditGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                    var SORespMessage = "";
                                    for (var i = 0 ; i < data.ResponseItems.length ; i++) {
                                        if (data.ResponseItems[i].MessageType == "S") {
                                            SORespMessage = SORespMessage + "<div style='color:green'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";
                                        }
                                        else {
                                            SORespMessage = SORespMessage + "<div style='color:red'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";

                                        }
                                    }

                                    $("#createSOResponse").html(SORespMessage);
                                    $.unblockUI();
                                    $("#createSOResponse").dialog("open");
                                }
                                else {

                                    var postSOErrAlt = noty({
                                        layout: 'top',
                                        type: 'error',
                                        text: data.Message,
                                        dismissQueue: true,
                                        animation: {
                                            open: { height: 'toggle' },
                                            close: { height: 'toggle' },
                                            easing: 'swing',
                                            speed: 500
                                        },
                                        timeout: 10000
                                    });
                                    $.unblockUI();
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.unblockUI();
                            }
                        });

                        $(this).dialog("close");
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                    $(this).dialog("close");


                }
            });
        }
        else {

        }
    }

}

function onEditSOClick() {
    debugger;
    $("#AddnewSO").hide();
    $("#SOInputFileds").hide();
    $("#SOtable").hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#UploadSO').hide();
    $("#viewSO").hide();
    //$("#EditSO").hide();
    $("#CancelSO").hide();
    $("#SalesOrderEditGrid").hide();
    $("#EditSO").show();
    AppViewModel.selectedEntityValue('');
    getAllCustomersfortheUser();
    AppViewModel.selectedBIN([ { "name": "MB3", "value": "MB3" }, { "name": "MB4", "value": "MB4" }, { "name": "MB5", "value": "MB5" }, { "name": "MB7", "value": "MB7" }, { "name": "MB5H", "value": "MB5H" }, { "name": "MB5HE", "value": "MB5HE" }])
    AppViewModel.editSOQty('');
    AppViewModel.selectedBINValue('');
    var currentDate = new Date();
    //$("#reqDate").datepicker({
    //    dateFormat: 'dd.mm.yy',
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    //$("#reqDate").datepicker("setDate", currentDate);

    var currentDate = new Date();
    //$("#dtEdit1").datepicker({
    //    dateFormat: 'yy-mm-dd',
    //    maxDate: 0,
    //    changeYear: true,
    //    onSelect: function () {
    //        var editdt2 = $('#dtEdit2');
    //        var selDate = $(this).datepicker('getDate');
    //        editdt2.datepicker('option', 'minDate', selDate);
    //        //editdt2.datepicker('option', 'maxDate', 0);
    //    }
    //}).attr('readonly', 'readonly');

    //$("#dtEdit1").datepicker("setDate", currentDate);

    //$('#dtEdit2').datepicker({
    //    dateFormat: 'yy-mm-dd',
    //    // maxDate: 0,
    //    minDate: 0,
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    //$("#dtEdit2").datepicker("setDate", currentDate);
    $('#reqDate').datetimepicker({
        format: 'DD.MM.YYYY',
        defaultDate: currentDate
    });
    $('#dtEdit1').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });
    $('#dtEdit2').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });
    refresh();
};
//jQuery(document).ready(function () {
//    $("#salesBtnSearchEdit").click(function () {
//        //loadJQGrid();
//        debugger;

//       // document.getElementById("MassEditDiv").style.display = 'block';
//      //  document.getElementById("editFields").style.display = 'none';
//       // document.getElementById("massEdit").checked = false;
//        loadEditSOJQGrid();
//        //$("#salesOrderDiv2").show();
//       $("#SalesOrderEditGrid").show();

//    });
//});
function onSOEditViewOrderClick() {
  
    loadEditSOJQGrid();
    $("#SalesOrderEditGrid").show();


}

function loadEditSOJQGrid() {
    debugger;
    AppViewModel.edtitCustomerCode('');
    if (document.getElementById('DDEditcustomer').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the customer.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }

    else {
        var sortFlag = 0;
        //var custNo = AppViewModel.selectedEntityValue().EntityCode;
        custNo = AppViewModel.selectedEntityValue()[0].EntityCode;
        AppViewModel.edtitCustomerCode(custNo);
        var startd = $("#dtEdit1").val();
        var endd = $("#dtEdit2").val();
        if (endd == "") {
            var enddAlt = noty({
                layout: 'top',
                type: 'warning',
                text: 'Please input End date.',
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        else {
            $('#SalesOrderEditGrid').jqGrid('GridUnload');
            $("#SalesOrderEditGrid").jqGrid({

                url: baseUrl + '/api/SO/ViewNewSOOrder?startDate=' + startd + '&endDate=' + endd + '&custNo=' + custNo,
                datatype: "json",
                myType: "GET",

                colNames: ['Countrykey','SI Number', 'SO Number', 'Packer Name', 'Packer Code', 'SI ETD', 'Quantity', 'SKU', 'Planned week', 'ITR Cnf Qty'],
                colModel: [
                { name: 'cntryKey', index: 'cntryKey', align: 'center', hidden: true },
                //{ name: 'congName', index: 'congName', align: 'center' },
                //{ name: 'congCustNum', index: 'congCustNum', align: 'center' },
                //{ name: 'reg', index: 'reg', align: 'center' },
                { name: 'custPrchOrdNo', index: 'custPrchOrdNo', align: 'center' },
                { name: 'vbeln', index: 'vbeln', align: 'center' },
                { name: 'pkrName', index: 'pkrName', align: 'center' },
                { name: 'packCustNum', index: 'packCustNum', align: 'center' },
                { name: 'vdatu', index: 'vdatu', align: 'center', /*formatter: REQdateFormatter,*/ editable: true, editoptions: { dataInit: function (el) { setTimeout(function () { $(el).datepicker({ dateFormat: "yy-mm-dd" }); }, 200); } } },
                { name: 'cuOrdQty', index: 'cuOrdQty', align: 'center', formatter: roundInteger, editable: true, },
                { name: 'matNm', index: 'matNm', align: 'center', editable: true, edittype: "select", editoptions: { value: "MB3:MB3;MB4:MB4;MB5:MB5;MB7:MB7;MB5H:MB5H" } },
                { name: 'plndWk', index: 'plndWk', align: 'center' },
                 { name: 'itrCnfQty', index: 'itrCnfQty', align: 'center', hidden: true },
              // { name: 'act', index: 'act', width: 160, sortable: false, /*datatype: 'html'*/ }

                ],

               // colNames: [/*'Country Key', 'Consignee Name', 'Consignee Customer Number', 'Region', */'Packer Name', 'Packer Code', 'Material No.', 'Customer Purchase Order No.', /*'Schedule Line date', 'Lead Days', 'ITR Confirm Receipt Date', */'Planned Week', 'Quantity', 'SO Number', 'Req.Date', 'ITR Cnf Qty'],
               // colModel: [
               //// { name: 'cntryKey', index: 'cntryKey', align: 'center' },
               // //{ name: 'congName', index: 'congName', align: 'center' },
               // //{ name: 'congCustNum', index: 'congCustNum', align: 'center' },
               // //{ name: 'reg', index: 'reg', align: 'center' },
               // { name: 'pkrName', index: 'pkrName', align: 'center' },
               // { name: 'packCustNum', index: 'packCustNum', align: 'center' },
               // { name: 'matNm', index: 'matNm', align: 'center', editable: true, edittype: "select", editoptions: { value: "MB3:MB3;MB4:MB4;MB5:MB5;MB7:MB7;MB5H:MB5H" } },
               // { name: 'custPrchOrdNo', index: 'custPrchOrdNo', align: 'center' },
               //// { name: 'schLneDte', index: 'schLneDte', align: 'center', formatter: dateFormatter },
               //// { name: 'leadDys', index: 'leadDys', align: 'center' },
               //// { name: 'itrCnfDte', index: 'itrCnfDte', align: 'center', formatter: dateFormatter },
               // { name: 'plndWk', index: 'plndWk', align: 'center' },
               // { name: 'cuOrdQty', index: 'cuOrdQty', align: 'center', formatter: roundInteger, editable: true, },
               //// { name: 'dte', index: 'dte', align: 'center', formatter: dateFormatter },
               // { name: 'vbeln', index: 'vbeln', align: 'center' },
               // { name: 'vdatu', index: 'vdatu', align: 'center', /*formatter: REQdateFormatter,*/ editable: true, editoptions: { dataInit: function (el) { setTimeout(function () { $(el).datepicker({ dateFormat: "yy-mm-dd" }); }, 200); } } },
               // { name: 'itrCnfQty', index: 'itrCnfQty', align: 'center', hidden: true },
               ////{ name: 'act', index: 'act', width: 160, sortable: false, /*datatype: 'html'*/ }

               // ],
                onSelectRow: function (id, status) {
                    //Removes Row highlight
                    $('#SalesOrderEditGrid tr').removeClass("ui-state-highlight");

                    if (status) {

                        $('#SalesOrderEditGrid')[0].p.selarrrow = $('#SalesOrderEditGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                        .map(function () { return this.id; }) // convert to set of ids
                        .get();
                    }
                },
                jsonReader: {
                    root: 'sData',
                   // id: 'custPrchOrdNo',
                    id:'Countrykey',
                    repeatitems:true
                },
                pager: $('#salesPager2'),
                excel: true,
                rowNum: 10,
                rowList: [10, 50, 200, 500, 1000],
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                loadonce: true,
                autoencode: false,
                multiselect: true,
                height: 'auto',
                width: '100%',
                caption: "Sales Order Record",

                loadComplete: function () {
                    $("tr.jqgrow:odd").css("background", "#f1f1f1");
                    // $("tr.jqgrow:odd").hover(function () { $("tr.jqgrow:odd").css("background", "#d9e4f0"); });
                    debugger;
                    var mode;
                    //if (document.getElementById('massEdit').checked) {
                    //    mode = "Edit";
                    //}

                    //if (mode == "Edit") {
                    var ids = $("#SalesOrderEditGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        $("#SalesOrderEditGrid").jqGrid('showCol', 'cb');
                       
                    }
                   
                },
            });

            $('#SalesOrderEditGrid').jqGrid('navGrid', '#salesPager2', {
                edit: false, view: false, add: false, del: false, search: true,
                beforeRefresh: function () {
                    //alert('In beforeRefresh');
                    $('#SalesOrderEditGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                }
            }, {}, {}, {}, { closeAfterSearch: true }, {});
        }
    }
}
function onCancelSOClick() {
    debugger;
    $("#AddnewSO").hide();
    $("#SOInputFileds").hide();
    $("#SOtable").hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#UploadSO').hide();
    $("#viewSO").hide();
    $("#EditSO").hide();
    $("#CancelSO").show();
    AppViewModel.selectedEntityValue('');
    getAllCustomersfortheUser();
    var currentDate = new Date();
    //$("#dtCancel1").datepicker({
    //    dateFormat: 'yy-mm-dd',
    //    maxDate: 0,
    //    changeYear: true,
    //    onSelect: function () {
    //        var editdt2 = $('#dtCancel2');
    //        var selDate = $(this).datepicker('getDate');
    //        editdt2.datepicker('option', 'minDate', selDate);
    //        //editdt2.datepicker('option', 'maxDate', 0);
    //    }
    //}).attr('readonly', 'readonly');

    //$("#dtCancel1").datepicker("setDate", currentDate);

    //$('#dtCancel2').datepicker({
    //    dateFormat: 'yy-mm-dd',
    //    // maxDate: 0,
    //    minDate: 0,
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    //$("#dtCancel2").datepicker("setDate", currentDate);

    $('#dtCancel1').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });
    $('#dtCancel2').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });


}
function onShowDataSOClick() {
    loadCancelSOJQGrid();
    $("#SalesOrderCancelGrid").show();

}
function loadCancelSOJQGrid() {
    AppViewModel.edtitCustomerCode('');
    debugger;
    if (document.getElementById('DDCancelcustomer').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the customer.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }

    else {
        var sortFlag = 0;
        //var custNo = AppViewModel.selectedEntityValue().EntityCode;
        custNo = AppViewModel.selectedEntityValue()[0].EntityCode;
        AppViewModel.edtitCustomerCode(custNo);
        var startd = $("#dtCancel1").val();
        var endd = $("#dtCancel2").val();
        if (endd == "") {
            var enddAlt = noty({
                layout: 'top',
                type: 'warning',
                text: 'Please input End date.',
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        else {
            $('#SalesOrderCancelGrid').jqGrid('GridUnload');
            $("#SalesOrderCancelGrid").jqGrid({

                url: baseUrl + '/api/SO/ViewNewSOOrder?startDate=' + startd + '&endDate=' + endd + '&custNo=' + custNo,
                datatype: "json",
                myType: "GET",

                colNames: ['Countrykey','SI Number', 'SO Number', 'Packer Name', 'Packer Code', 'SI ETD', 'Quantity', 'SKU', 'Planned week', 'ITR Cnf Qty'],
                colModel: [
                 { name: 'cntryKey', index: 'cntryKey', align: 'center', hidden: true },
                //{ name: 'congName', index: 'congName', align: 'center' },
                //{ name: 'congCustNum', index: 'congCustNum', align: 'center' },
                //{ name: 'reg', index: 'reg', align: 'center' },
                { name: 'custPrchOrdNo', index: 'custPrchOrdNo', align: 'center' },
                { name: 'vbeln', index: 'vbeln', align: 'center' },
                { name: 'pkrName', index: 'pkrName', align: 'center' },
                { name: 'packCustNum', index: 'packCustNum', align: 'center' },
                { name: 'vdatu', index: 'vdatu', align: 'center', /*formatter: REQdateFormatter,*/ editable: true, editoptions: { dataInit: function (el) { setTimeout(function () { $(el).datepicker({ dateFormat: "yy-mm-dd" }); }, 200); } } },
                { name: 'cuOrdQty', index: 'cuOrdQty', align: 'center', formatter: roundInteger, editable: true, },
                { name: 'matNm', index: 'matNm', align: 'center', editable: true, edittype: "select", editoptions: { value: "MB3:MB3;MB4:MB4;MB5:MB5;MB7:MB7;MB5H:MB5H" } },
                { name: 'plndWk', index: 'plndWk', align: 'center' },
                 { name: 'itrCnfQty', index: 'itrCnfQty', align: 'center', hidden: true },
              // { name: 'act', index: 'act', width: 160, sortable: false, /*datatype: 'html'*/ }

                ],
              
                onSelectRow: function (id, status) {
                    debugger;
                    //Removes Row highlight
                   // $('#SalesOrderCancelGrid tr').removeClass("ui-state-highlight");
                    var selectedIds = $("#SalesOrderCancelGrid").jqGrid('getGridParam', 'selarrrow');
                    if (status == true && selectedIds.length > 0) {
                        document.getElementById("cancelSalesOrder").style.display = 'block';
                        $('#SalesOrderCancelGrid')[0].p.selarrrow = $('#SalesOrderCancelGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                        .map(function () { return this.id; }) // convert to set of ids
                        .get();
                    }
                    if (status == false && selectedIds.length == 0) {
                        document.getElementById("cancelSalesOrder").style.display = 'none';
                    }

                },
                onSelectAll: function (aRowids, status) {
                    debugger
                    $("#cb_my_grid").attr("checked", true);
                    //Use a ternary operator to choose zero if the header has just been unchecked, or the total items if it has just been checked.
                    $("totalSelected").val(status === false ? 0 : aRowids.length);
                    if (aRowids.length > 0 && status == true) {
                        document.getElementById("cancelSalesOrder").style.display = 'block';
                    }
                    else {
                        document.getElementById("cancelSalesOrder").style.display = 'none';
                    }
                },
                jsonReader: {
                    root: 'sData',
                   // id: 'custPrchOrdNo',
                    id: 'Countrykey',
                    repeatitems: true
                },
                pager: $('#salesPager3'),
                excel: true,
                rowNum: 10,
                rowList: [10, 50, 200, 500, 1000],
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                loadonce: true,
                autoencode: false,
                multiselect: true,
                height: 'auto',
                width: '100%',
                caption: "Sales Order Record",

                loadComplete: function () {

                    $("tr.jqgrow:odd").css("background", "#f1f1f1");
                    // $("tr.jqgrow:odd").hover(function () { $("tr.jqgrow:odd").css("background", "#d9e4f0"); });
                    debugger;
                    var mode;
                    //if (document.getElementById('massEdit').checked) {
                    //    mode = "Edit";
                    //}

                    //if (mode == "Edit") {
                    var ids = $("#SalesOrderCancelGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        $("#SalesOrderCancelGrid").jqGrid('showCol', 'cb');
                       
                    }
                   
                },
            });

            $('#SalesOrderCancelGrid').jqGrid('navGrid', '#salesPager3', {
                edit: false, view: false, add: false, del: false, search: true,
                beforeRefresh: function () {
                    //alert('In beforeRefresh');
                    $('#SalesOrderCancelGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                }
            }, {}, {}, {}, { closeAfterSearch: true }, {});
        }
    }
}
function onSOCancelOrderClick() {
    debugger;
    //newSOEditData[0] = AppViewModel.selectedBINValue()[0].value;
    //newSOEditData[1] = AppViewModel.editSOQty();
    //newSOEditData[2] = $("#reqDate").val();
    // alert(newSOEditData);
    var selectedIds = $("#SalesOrderCancelGrid").jqGrid('getGridParam', 'selarrrow');
    if (selectedIds == '') {
        noty({
            layout: 'top',
            type: 'error',
            text: "Please select atlest one row",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else {
        var ids = $("#SalesOrdereditGrid").jqGrid('getDataIDs');
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SO/AllReasonsForCancel',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.length != 0) {
                    AppViewModel.availableReasonsCancel([]);
                    //AppViewModel.availableReasonsQty([]);
                    //AppViewModel.availableReasonsReqDate([]);
                    var data1 = [];
                    //var data2 = [];
                    //var data3 = [];
                    var i = 0;
                    for (var m = 0; m < data.length; m++) {
                        if (data[m].ReasonType == "Cancel") {
                            data1 = data[m];
                            AppViewModel.availableReasonsCancel.push(data1);
                        }
                      
                    }
                }
                else {
                    $.unblockUI();
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Unable to retrieve Reasons",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
        $("#somassCancelReason").dialog({
            autoOpen: true,
            height: "auto",
            width: "auto", zIndex: 50000,
            modal: true,
            buttons: {
                "OK": function () {
                    debugger;
                    uiBlocker();

                    var reason1 = AppViewModel.selectedReasonForCancel()[0].ReasonCode;

                    // }
                    AppViewModel.selectedLineContent([]);
                    var customerPNo;
                    for (var i = 0; i < selectedIds.length; i++) {
                        var rowId = selectedIds[i];
                        var selectedRowData = $("#SalesOrderCancelGrid").getRowData(rowId);
                        customerPNo = selectedRowData.custPrchOrdNo;
                        materialNo = selectedRowData.matNm;
                        orderQty = selectedRowData.cuOrdQty;
                        deliveryDate = selectedRowData.vdatu;
                        deliveryLocation = selectedRowData.packCustNum;
                        AppViewModel.selectedLineContent.push({ customerPONos: customerPNo, materialNos: materialNo, orderQtys: orderQty, deliveryDates: deliveryDate, deliveryLocations: deliveryLocation });

                    }
                    var CancelSOModel = {
                        customerCode: AppViewModel.edtitCustomerCode(),
                        LineContent: AppViewModel.selectedLineContent(),
                        reason1: reason1,
                    }
                    $.ajax({
                        type: "POST",
                        url: baseUrl + '/api/SO/massCancelSalesOrder',
                        data: ko.toJSON(CancelSOModel),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            if (data.Success == true) {
                                $('#SalesOrderCancelGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                var SORespMessage = "";
                                for (var i = 0 ; i < data.ResponseItems.length ; i++) {
                                    if (data.ResponseItems[i].MessageType == "S") {
                                        SORespMessage = SORespMessage + "<div style='color:green'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";
                                    }
                                    else {
                                        SORespMessage = SORespMessage + "<div style='color:red'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";

                                    }
                                }

                                $("#createSOResponse").html(SORespMessage);
                                $.unblockUI();
                                $("#createSOResponse").dialog("open");
                            }
                            else {

                                var postSOErrAlt = noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: data.Message,
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 10000
                                });
                                $.unblockUI();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $.unblockUI();
                        }
                    });

                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                $(this).dialog("close");


            }
        });
    }

}
this.onChangeSOUploadClick=function ()
{
    uiBlocker();
    //AppViewModel.selectedSOSubsi('GP.Limited');
    debugger

    if ((AppViewModel.selectedSOSubsi() != '')  && (AppViewModel.uploadChangeSOFileName() != '')) {

        var ext = AppViewModel.uploadChangeSOFileName().split('.').pop();
        var isFileType = false;

        if ((ext.toLowerCase() == "xls") || (ext.toLowerCase() == "xlsx") || (ext == "csv") || (ext == "txt")) {
            var fileUploadData = new FormData($('#fileUploadSOform2')[0]);
            $.ajax({

                type: 'POST',
                url: baseUrl + '/api/SC/UploadFile?transactionType=SO',  //server script to process data
                xhrFields: {
                    withCredentials: true
                },

                // Form data
                data: fileUploadData,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false,
                async: true,
                success: function (data, textStatus, xhr) {
                    var path = data;
                    uiBlocker();
                    $.ajax({
                        type: "GET",
                        url: baseUrl + '/api/SC/SOChangeCancelUpload?subsiId=' + AppViewModel.selectedSOSubsi()[0].SubsiId + '&path=' + encodeURIComponent(path),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $.unblockUI();
                            $('#soVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });
                            $('#soChangeVerifyBtn').css({ 'visibility': 'visible', 'display': 'block' });                         
                            if (data.length != 0) {
                                var isSuccess = true;
                                var message = null;
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].Status == "false") {
                                        isSuccess = false;
                                        message = data[i].Message;
                                        break;
                                    }
                                    //if(data[i].ErrorMessage!=null)
                                    //{
                                    //    isSuccess = false;
                                    //}

                                }
                                if (!isSuccess) {
                                    $('#soVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });
                                    $('#soChangeVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });

                                    $("#soShowDataTable").hide();
                                    noty({
                                        layout: 'top',
                                        type: 'error',
                                        text: "" + message,
                                        dismissQueue: true,
                                        animation: {
                                            open: { height: 'toggle' },
                                            close: { height: 'toggle' },
                                            easing: 'swing',
                                            speed: 500
                                        },
                                        timeout: 5000
                                    });
                                }
                                else {

                                    soShowData(data);
                                    $("#soShowDataTable").show();
                                }
                            }
                            else {
                                noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: "Unable to parse the file.Please check your data in file",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 5000
                                });

                            }

                        }, error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            $.unblockUI();
                        }
                    });
                },
                error: function (r) {

                }
            });
        }
        else {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file based on Mapped file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
    }
    else {
        if (AppViewModel.uploadChangeSOFileName() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        if (AppViewModel.selectedMapSOSubsi() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select subsi for SO file upload",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
    }
}
this.verifyChangeSoData=function ()
{

    noty({
        layout: 'top',
        type: 'information',
        text: "SO data verification takes some time.Please wait...",
        dismissQueue: true,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        },
        timeout: 10000
    });

    uiBlocker();

    $.ajax({
        type: "POST",
        url: baseUrl + '/api/SC/VerifySOChangeCancelUpload?mapperFile=' + "ChangeCancelSOMapper" + "&subsiId=" + AppViewModel.selectedSOSubsi()[0].SubsiId + "&CustPODate=" + $("#dtCustPO").val(),
        data: ko.toJSON(AppViewModel.scTableData()),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$noty.close();
            $.unblockUI();
            if (data.length != 0) {
                if (data[0].Status == "false") {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "" + data[0].Message,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                else {

                    debugger
                    $('#scMapping').hide();
                    $('#SO').hide();
                    $('#soResult').show();
                    soResultData(data);
                }
            }
            else {
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to verify data.Please check your uploaded file",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

}

