﻿

AppViewModel.selectedMapSubsi.subscribe(function (newValue) {
    if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
    if (newValue != null && newValue != "") {
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SC/MappedFiles?subsiId=' + newValue[0].SubsiId + "&transactionType=" + AppViewModel.transactionType(),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                //AppViewModel.mappedFileData('');
                if (data.length != 0) {
                    AppViewModel.mappedFileData([]);
                    AppViewModel.mappedFileData(data);
                    AppViewModel.selectedFile(AppViewModel.mappedFileData()[0]);
                }
                else {
                    AppViewModel.selectedFile('');
                    $.unblockUI();
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "No Mapper for the selected subsi",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
    }

});






this.onShowData = function () {
    debugger;
    if ((AppViewModel.selectedMapSubsi() != '') && (AppViewModel.mappedFileData() != '') && (AppViewModel.uploadSCFileName()!='')) {

        var ext = AppViewModel.uploadSCFileName().split('.').pop();
        var isFileType = false;
        var fileUploadData = null;
        if ((ext.toLowerCase() == "xls") || (ext.toLowerCase() == "xlsx") || (ext.toLowerCase() == "csv") || (ext.toLowerCase() == "txt")) {
            if (AppViewModel.selectedTransType() == 'SC') {
                fileUploadData = new FormData($('#fileUploadform')[0]);
            }
            else if (AppViewModel.selectedTransType() == 'SSC') {
                fileUploadData = new FormData($('#fileUploadSSC')[0]);
            }
            else if (AppViewModel.selectedTransType() == 'ASN') {
                fileUploadData = new FormData($('#fileUploadASN')[0]);
            }
            uiBlocker();
            $.ajax({

                type: 'POST',
                url: baseUrl + '/api/SC/UploadFile?transactionType=' + AppViewModel.selectedTransType(),  //server script to process data
                xhrFields: {
                    withCredentials: true
                },

                // Form data
                data: fileUploadData,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false,
                async: true,
                success: function (data, textStatus, xhr) {
                  //  AppViewModel.filePath(data);
                    var path = data;                                      
                    $.ajax({
                        type: "GET",
                        url: baseUrl + '/api/SC/FileData?mapperName=' + AppViewModel.selectedFile() + '&subsiId=' + AppViewModel.selectedMapSubsi()[0].SubsiId + '&path=' + encodeURIComponent(path),
                      //  data: ko.toJSON(path),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $.unblockUI();
                            if (data.length != 0) {
                                var isSuccess = true;
                                var message=null;
                                for (var i = 0; i < data.length; i++)
                                {
                                    if (data[i].Status == "false")
                                    {
                                        isSuccess = false;
                                        message = data[i].Message;
                                        break;
                                    }

                                }
                                if (!isSuccess) {
                                    if (AppViewModel.selectedTransType() == 'SC') {
                                        $("#scTable").hide();
                                        $("#scVerifyBtn").hide();
                                        $("#sscVerifyBtn").hide();
                                        $("asnVerifyBtn").hide();
                                    }
                                    else {
                                        $("#sscTable").hide();
                                        $("#scVerifyBtn").hide();
                                        $("#sscVerifyBtn").hide();
                                        $("asnVerifyBtn").hide();
                                    }
                                    noty({
                                        layout: 'top',
                                        type: 'error',
                                        text: "" + message,
                                        dismissQueue: true,
                                        animation: {
                                            open: { height: 'toggle' },
                                            close: { height: 'toggle' },
                                            easing: 'swing',
                                            speed: 500
                                        },
                                        timeout: 5000
                                    });
                                }
                                else {                                   
                                    if (AppViewModel.selectedTransType() == 'SC') {
                                        scShowData(data);
                                        $("#scTable").show();
                                        $("#sscMapping").hide();
                                        $("#asnMapping").hide();
                                    }
                                    else if(AppViewModel.selectedTransType()=='SSC'){
                                        scShowData(data);
                                        $("#sscTable").show();
                                        $("#scMapping").hide();
                                        $("#asnMapping").hide();
                                    }
                                    else if (AppViewModel.selectedTransType() == 'ASN') {
                                        scShowData(data);
                                        $("#scMapping").hide();
                                        $("#sscMapping").hide();
                                        $("#asnMapping").show();
                                        $("#asnTable").show();

                                    }
                                }
                            }
                            else {
                                noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: "Unable to parse the file.Please check your data in file",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 5000
                                });

                            }

                        }, error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            $.unblockUI();
                        }
                    });
                },
                error: function (r) {
                    $.unblockUI();
                }
            });
        }
        else {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file based on Mapped file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
    }
    else {
        if (AppViewModel.uploadSCFileName() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        else if (AppViewModel.selectedMapSubsi() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select subsi for SC file upload",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
        else {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select mapping file for SC file upload",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
        }

}


function scShowData(data)
{    
    var isMissingSAPData = false;
    var tableRef = null;
    var htmlNew = " ";
    if (AppViewModel.selectedTransType() == 'SC') {
        $("#divSCTableLegend").show();
        tableRef = document.getElementById('scTable').getElementsByTagName('tbody')[0];
    }
    else if (AppViewModel.selectedTransType() == 'SSC') {
        $("#divSSCTableLegend").show();
        tableRef = document.getElementById('sscTable').getElementsByTagName('tbody')[0];
    }
    else if (AppViewModel.selectedTransType() == 'ASN') {
        tableRef = document.getElementById('asnTable').getElementsByTagName('tbody')[0];
    }

    var tableRows = tableRef.getElementsByTagName('tr');    
    var rowCount = tableRows.length;
    var isSAPFieldData = false;   
    AppViewModel.scTableData(data);
    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }
   

    /* var elmtTable = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
     var tableRows = elmtTable.getElementsByTagName('tr');
     var rowCount = tableRows.length;

     for (var x = rowCount - 1; x >= 0; x--) {
         elmtTable.removeChild(tableRows[x]);
     }*/

 

    for (var k = 0; k < data.length; k++) {
        // Insert a row in the table at row index 0
        var arr = data[k].Name.split('|');
        var decodeVal = null;
        if (k == 1)
            sapFields = arr;
        if (k > 1) {
            if (data[k].DecodeValue != null) {
                decodeVal = data[k].DecodeValue.split('|');
            }
            else {
                isSAPFieldData = true;
            }
        }
        
        $("#scVerifyBtn").show();
        $("#sscVerifyBtn").show();
        $("asnVerifyBtn").show();
        var newRow = tableRef.insertRow(tableRef.rows.length);
        if (k == 0) {
            newRow.style.backgroundColor = "#599CDE";
            //newRow.style.backgroundColor = "#6996c2";
        }
        else if (k == 1) {
            //newRow.style.backgroundColor = "rgb(205, 219, 219)";
            //newRow.style.backgroundColor = "#c2ddb6";
            newRow.style.backgroundColor = "rgb(126, 191, 117)";
        }
        else {
            if (isSAPFieldData) {
                newRow.style.backgroundColor = "rgb(245, 177, 177)";
            }
            else {
                newRow.style.backgroundColor = "#f1f1f1";
            }
        }

            for (var i = 0; i < arr.length; i++) {              
              
                var headerCodecell = newRow.insertCell(i);
                headerCodecell.style.border = '1px solid #ccc';
                headerCodecell.style.textAlign = "center";
                // Append a text node to the cell
                var newText = null;
                var newText1 = null;
                if (decodeVal != null) {
                    if ((decodeVal[i] != '')&&(decodeVal[i]!='false')) {
                        //newText = document.createTextNode(arr[i] + " (" + decodeVal[i] + ") ");
                        newText = document.createElement("span");
                        newText.innerHTML = arr[i];
                        newText.style.color = "#599CDE";
                        newText1 = document.createElement("span");
                        newText1.innerHTML = "(" + decodeVal[i] + ")";
                        newText1.style.color = "rgb(126, 191, 117)";
                    }
                    else {
                        newText = document.createTextNode(arr[i]);
                        if (decodeVal[i] == 'false')
                        {
                            isMissingSAPData = true;
                            headerCodecell.style.backgroundColor = "rgb(245, 177, 177)";
                            var x = document.createElement("a");
                            x.setAttribute("href", "#");
                            x.setAttribute("onclick", "addDecodeValueSC(" + i + ",this)");
                            x.appendChild(newText);
                            newText = x;
                        }
                    }
                }
                else {
                    newText = document.createTextNode(arr[i]);
                }
                if (newText1 == null)
                    headerCodecell.appendChild(newText);
                else {
                    var x = document.createElement("a");
                    x.setAttribute("href", "#");
                    x.setAttribute("onclick", "updateDecodeValueForSCData(" + i + ",this)");
                    x.appendChild(newText);
                    x.appendChild(newText1);
                    headerCodecell.appendChild(x);
                    // headerCodecell.appendChild(newText1);
                }
                //headerCodecell.appendChild(newText);
            }

            if (k == data.length - 1)
            {
                if (data[k].ErrorMessage.length != 0) {
                    isMissingSAPData = true;
                    htmlNew += '<p style="color:red">' + data[k].ErrorMessage + '</p>';
                    $("#errorMessage").html('<p style="color:red">' + data[k].ErrorMessage + '</p>');
                   // $("#errorMessage").dialog("open");
                    $('#scVerifyBtn').hide();
                    $('#sscVerifyBtn').hide();
                    $("asnVerifyBtn").hide();
                }
                
            }
    }
    if (isMissingSAPData) {
        if (AppViewModel.selectedTransType() == 'SC') {
            $('#scVerifyBtn').hide();
        }
        else if (AppViewModel.selectedTransType() == 'SSC') {
            $('#sscVerifyBtn').hide();
        }
        else if (AppViewModel.selectedTransType() == 'ASN') {
            $('#asnVerifyBtn').hide();
        }
        isMissingSAPData = false;
        //noty({
        //    layout: 'top',
        //    type: 'error',
        //    text: "There are some values which don’t have values/look up values.Please add values/decode values to proceed",
        //    dismissQueue: true,
        //    animation: {
        //        open: { height: 'toggle' },
        //        close: { height: 'toggle' },
        //        easing: 'swing',
        //        speed: 500
        //    },
        //    timeout: 10000
        //});
        //var html = '<table><tr><td>There are some values which don’t have look up values.Please add decode values to proceed.</td><td></td></tr>' +
        //            '<tr><td colspan=2><label style ="font-weight:normal !important">Added code value are shown in the color</lable> <div style="background:#599CDE;dispaly:block;width:15px;height:15px;"></div></td></tr>' +
        //            '<tr><td colspan=2><label style ="font-weight:normal !important">Added decode value are shown in the color</label> <div style="background:#7EBF75;dispaly:block;width:15px;height:15px;"></div></td></tr>' +
        //            '<tr><td>They can be added/edited by clicking the corresponding values in the table</td><td></td></tr>' +
        //            '</table>';
        var html = "<div style='overflow:auto !important;'><article><h4>Goodpack Code Missing</h4>" +
                   "<p>Some file customer fields do not have Goodpack codes </p>" +
                   "<p>click on Hyperlink to add Goodpack codes <span style='color:red;'> (Highlighted in RED)</span> </p>" +
                   "<p>Edit already added customer code by clicking corresponding Hyperlink(s)  </p>" +
                   "<p><div style='width:15px;height:15px;background-color:#599CDE;float:left;'></div>" +
                    "<div style='width:15px;height:15px;float:left;margin-left:10px;white-space: nowrap;'>Customer Data</div >" +
                    "</p></br>" +
                    "<p><div style='width:15px;height:15px;background-color:#7EBF75;float:left;'></div>" +
                    "<div style='width:15px;height:15px;float:left;margin-left:10px;white-space: nowrap;'>GoodPack Data</div ></p>" +
                    "</br><article><p><div style='float:left;'><h4>Error in Data</h4>" + htmlNew + "</div></p></article></div>";

        $("<div id='divDialog' style='overflow:auto !important;'></div>").html(html).dialog({
            title: "<img src='" + baseUrl + "/images/info.png' width='15px'/><span style='font-size: 16px;'>Warning</span>",
            resizable: false,
            zIndex: 50002,
            modal: true, width: 700, height: 400,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                    $('#divDialog').remove();
                }
            }
        });
    }
    else {
        $('#scVerifyBtn').show();
        $('#asnVerifyBtn').show();
        $('#sscVerifyBtn').show();
    }
}

function addDecodeValueSC(e, x) {
    debugger;
    console.log(x);
    //console.log(sapFields);
    console.log(sapFields[e]);
    // AppViewModel.selectedMapSOSubsi(AppViewModel.selectedMapSubsi());  

    $('#addLookupPage').removeClass('hide');
    $('#txtSubsi').val(AppViewModel.selectedMapSubsi()[0].SubsiName);
    $('#txtMapper').val(AppViewModel.selectedFile()[0]);
    console.log(sapFields[e]);
    var lookUpVal = (sapFields[e] == "From Location") ? "Packer" : (sapFields[e] == "To Location") ? "Consignee" : (sapFields[e] == "Customer" || sapFields[e] == "Customer Number") ? "Customer Code" : sapFields[e];
    console.log(lookUpVal);
    uiBlocker();
    if (lookUpVal == "Consignee") {
        $('#ddCustomerPackerDeCodeVal_chosen').hide();
        $('#txtDeCodeVal').show();
    }
    else {
        $('#txtDeCodeVal').hide();
        $('#ddCustomerPackerDeCodeVal_chosen').show();
        GetCustomerPackerDetails(lookUpVal);
    }
    $('#txtLookUp').val(lookUpVal);
    $('#txtCodeVal').val($(x).html());
    $('#txtDeCodeVal').val('');
    $('#divDialogHeader').html("Add Lookup");
    debugger;
    AppViewModel.selectedCustomerPackerDecodeValue('');
    $('#ddCustomerPackerDeCodeVal').val('');
    $('#ddCustomerPackerDeCodeVal').trigger("chosen:updated");
    $('#addLookupPage').dialog({
        height: 550,
        width: 650,        
        modal: true
    });
    setTimeout(function () { $('#txtLookUp').focus(); }, 200);
    $.unblockUI();
    tableIndex = e;
}

function updateDecodeValueForSCData(e, x) {
    $('#addLookupPage').removeClass('hide');
    $('#txtSubsi').val(AppViewModel.selectedMapSubsi()[0].SubsiName);
    $('#txtMapper').val(AppViewModel.selectedFile()[0]);
    console.log(sapFields[e]);
    var lookUpVal = (sapFields[e] == "From Location") ? "Packer" : (sapFields[e] == "To Location") ? "Consignee" : (sapFields[e] == "Customer" || sapFields[e] == "Customer Number") ? "Customer Code" : sapFields[e];
    console.log(lookUpVal);
    uiBlocker();
    if (lookUpVal == "Consignee") {
        $('#ddCustomerPackerDeCodeVal_chosen').hide();
        $('#txtDeCodeVal').show();
    }
    else {
        $('#txtDeCodeVal').hide();
        $('#ddCustomerPackerDeCodeVal_chosen').show();
        GetCustomerPackerDetails(lookUpVal);
    }
    $('#txtLookUp').val(lookUpVal);
    var data = $(x).html();
    var codeValue = null;
    var decodeValue = null;
    $.each($(data), function (key, value) {
        if (key == 0) {
            codeValue = $(value).html();
        }
        else if (key == 1) {
            var rowData = new Array();
            decodeValue = $(value).html();
            rowData.push({
                EntityCode: $(value).html().replace('(', '').replace(')', ''),
                EntityName: codeValue
            });
            debugger;
            if (lookUpVal == "Consignee") {
                var valu = decodeValue.replace('(', '').replace(')', '');
                $('#txtDeCodeVal').val(valu);
            } else {
                AppViewModel.selectedCustomerPackerDecodeValue = ko.observableArray(rowData);
                //$('#ddCustomerPackerDeCodeVal').val(codeValue + decodeValue);
                $('#ddCustomerPackerDeCodeVal option:contains(' + decodeValue.replace('(', '').replace(')', '') + ')').attr('selected', true);
                $('#ddCustomerPackerDeCodeVal').trigger("chosen:updated");
            }
        }
    });

    $('#txtCodeVal').val(codeValue);
    //$('#txtDeCodeVal').val('');
    $('#btnSaveLookup').hide();
    $('#btnUpdateLookup').show();
    $('#divDialogHeader').html("Update Lookup");
    $('#addLookupPage').dialog({
        height: 550,
        width: 650,      
        modal: true
    });
    setTimeout(function () { $('#txtLookUp').focus(); }, 200);
    $.unblockUI();
    tableIndex = e;
}
this.verifyClick = function () {
    var r = confirm("Please verify lookup decode value before Submiting. Do you want to continue?");
    if (r != true)
        return false;
    uiBlocker(); 
    noty({
        layout: 'top',
        type: 'information',
        text: "SC data verification takes some time.Please wait...",
        dismissQueue: true,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        },
        timeout: 10000
    });
    console.log('a');
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/SC/VerifyData?mapperFile=' + AppViewModel.selectedFile() + "&subsiId=" + AppViewModel.selectedMapSubsi()[0].SubsiId + "&transactionType=" + AppViewModel.transactionType(),
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(AppViewModel.scTableData()),
        success: function (data) {
            //$noty.close();
            $.unblockUI();         
            if (data.length != 0) {
                if (data[0].Status == "false") {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: ""+data[0].Message,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                else {
                    $('#scMapping').hide();
                    $('#sscMapping').hide();
                    $('#asnMapping').hide();
                    $('#scResult').show();
                    scResultData(data);
                }
            }
            else {
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to verify data.Please check your uploaded file",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

}

function scResultData(data) {

    debugger

    var tableRef = document.getElementById('scResultTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    var successCount = 0;
    AppViewModel.scResultTableData(data);
    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }

    for (var k = 0; k < data.length; k++) {
        // Insert a row in the table at row index 0
        var arr = data[k].Name.split('|');
        $("#scVerifyBtn").show();
        var newRow = tableRef.insertRow(tableRef.rows.length);
        if (k == 0) {
            newRow.style.backgroundColor = "#c2ddb6";
        }
        else if (k == 1)
        {
            //newRow.style.backgroundColor = "rgb(205, 219, 219)";
            newRow.style.backgroundColor = "#e4edf5)";
        }
        else {
            newRow.style.backgroundColor = "white";
        }
        var headerCodecell
        var cellCount = 0;
        for (var i = 0; i < arr.length + 2; i++) {
            
            if (i != arr.length) {
                
                headerCodecell = newRow.insertCell(cellCount);
                headerCodecell.style.border = '1px solid #ccc';
                headerCodecell.style.textAlign = "center";

                // Append a text node to the cell
                if (i < arr.length) {
                    var newText = document.createTextNode(arr[cellCount]);
                    headerCodecell.appendChild(newText);
                }

                if ((k == 0) && (i > arr.length)) {                  
                  
                    var newText2 = document.createTextNode("Error Details");
                    headerCodecell.appendChild(newText2);

                    headerCodecell = newRow.insertCell(cellCount);
                    headerCodecell.style.border = '1px solid #ccc';
                    headerCodecell.style.textAlign = "center";
                    // Append a text node to the cell
                  

                    // Append a text node to the cell
                    var newText1 = document.createTextNode("Status");
                    headerCodecell.appendChild(newText1);
                }
                else if ((k == 1) && (i > arr.length))
                {
                    var newText2 = document.createTextNode("");
                    headerCodecell.appendChild(newText2);

                    headerCodecell = newRow.insertCell(cellCount);
                    headerCodecell.style.border = '1px solid #ccc';
                    headerCodecell.style.textAlign = "center";
                    // Append a text node to the cell


                  

                }
                else if (i > arr.length) {

                    // Append a text node to the cell
                    if (data[k].Status == null)
                    {
                        var newText2 = document.createTextNode("");
                        headerCodecell.appendChild(newText2);
                    }

                    else if (data[k].Status == "ERROR") {

                        var btn = document.createElement("BUTTON");
                        var t = document.createTextNode("Details");
                        btn.style.backgroundColor = "#004c97";
                        btn.style.borderRadius = "5px";
                        btn.style.color = "white";
                        btn.id = k;
                        //var lineId = data[k].LineId;
                       // btn.onclick = function () { errorDetails(lineId); }
             
                        btn.onclick = (function (opt) {
                            return function () {
                                errorDetails(opt);
                            };
                        })(data[k].LineId);
                        btn.appendChild(t);
                        headerCodecell.appendChild(btn);
                        headerCodecell = newRow.insertCell(cellCount);
                        headerCodecell.style.border = '1px solid #ccc';
                        headerCodecell.style.textAlign = "center";
                    }

                    else {
                        if (data[k].Status == "SUCCESS") {
                            var newText2 = document.createTextNode("");
                            headerCodecell.appendChild(newText2);

                            headerCodecell = newRow.insertCell(cellCount);
                            headerCodecell.style.border = '1px solid #ccc';
                            headerCodecell.style.textAlign = "center";
                            successCount++
                        }
                    }

                    var newText3 = document.createTextNode(data[k].Status);
                    headerCodecell.appendChild(newText3);

                }
                cellCount++;
            }
        }
        cellCount = 0;
    }
    if (successCount == 0 || AppViewModel.selectedTransType() == 'SSC')
    {
        $("#scSubmit").hide();
    }
    else
    {
        $("#scSubmit").show();
    }
}

function lineContentDetails(rowId)
{
    debugger;
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SC/LineContentDetails?lineId=' + rowId + "&transactionType=" + AppViewModel.transactionType(),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                var messageVal = '';
                createTable(data,"box");
                for (var i = 0; i < data.length; i++) {
                    messageVal = messageVal + '<p style="color:red">' + "" + data[i] + '</p>';

                }
                $('#box').dialog({
                    width: 800, zIndex: 50000,
                    title: "Line Details",
                    height: 250,
                    modal: true
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

function createTable(data, elementId) {
    $("#" + elementId).html('');
    mytable = $('</br><table></table>').attr({ id: "basicTable" }).css({ "border-collapse": "collapse" });
    var rows = data.length;
    var cols = data[0].split('|').length;
    var tr = [];
    for (var i = 0; i < rows; i++) {
        if (data[i] != null) {
            var rowValue = data[i].split('|');
            var row = (i == 0) ? $('<tr></tr>').css({ "background-color": "#c2ddb6" }).css({ "height": "30px" }).appendTo(mytable) : $('<tr></tr>').css({ "height": "30px" }).appendTo(mytable);
            for (var j = 0; j < cols; j++) {
                $('<td></td>').text(rowValue[j]).css({ "border": "1px solid gray" }).css({ "white-space": "nowrap" })
                    .css({ "min-width": "100px" }).css({ "text-align": "center" }).appendTo(row);
            }
        }
    }
    mytable.appendTo("#" + elementId);
}

function errorDetails(opt) {
    console.log(opt);
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SC/ErrorDetails?LineId=' + opt + "&transactionType=" + AppViewModel.transactionType(),        
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                var messageVal = '';
                for (var i = 0; i < data.length; i++) {
                    messageVal = messageVal + '<p style="color:red">' + "" + data[i] + '</p>';

                }
                noty({
                    layout: 'top',
                    type: 'warning',
                    text: "" + messageVal,
                    buttons: [
                            {
                                addClass: 'btn btn-primary', text: 'Close', type: 'success', onClick: function ($noty) {
                                    $noty.close();
                                }
                            }

                    ]
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

function myfunction() {
    alert(this.id);
}

this.scSubmitSuccessClick = function ()
 {
    $('#scSubmit').prop('disabled', true);
    uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/SC/SubmitData?transactionType=' + AppViewModel.transactionType(),
        data: ko.toJSON(AppViewModel.scResultTableData()),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#scSubmit').prop('disabled', false);
            $.unblockUI();
            if (data.length != 0) {
                var messageVal = '';
                for (var i = 0; i < data.length; i++) {                  
                    if (!data[i].Success) {
                        messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Status in SAP: Not successfull </p><p  style="color:red"> Message: ' + '' + data[i].Message + '</p>';
                    }
                    else {
                        messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Status in SAP: Successfull </p><p  style="color:green"> Message: ' + '' + data[i].Message + '</p>';
                    }                    
                }
                $('#scMapping').hide();
                $('#scResult').hide();
                $("#lineError").html(messageVal);
                $.unblockUI();
                $("#lineError").dialog("open");
            }
            else{
             
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to submit data.Please try again",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            
            }
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#scSubmit').prop('disabled', false);
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

}