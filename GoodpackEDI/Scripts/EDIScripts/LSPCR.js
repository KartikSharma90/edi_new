﻿
function pickupDeliveryDateClick() {
    debugger  
    $("#PickUpDate").datepicker({
        dateFormat: 'dd-M-yy',
        changeYear: true,
        minDate: findminDate(),
        maxDate: findmaxDate(),
        beforeShowDay:$.datepicker.noWeekends
    }).attr('readonly', 'readonly');    
    $("#PickUpDate").datepicker('show');
}
function findminDate()
{
    debugger
    var table = document.getElementById('LSPCRTable').getElementsByTagName('tbody')[0];    
    var proposedD = table.rows[9].cells[1].innerHTML.split("-");
    var proposedDate = new Date(parseInt(proposedD[2]), returnMontIndex(proposedD[1]), parseInt(proposedD[0]));
    var formattedDate = proposedDate;
    formattedDate.setDate(formattedDate.getDate() - 7);
    return proposedDate;
}

var monthVal = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
function returnMontIndex(month) {
    return monthVal.indexOf(month);
    
}

function findmaxDate()
{
    debugger
    var table = document.getElementById('LSPCRTable').getElementsByTagName('tbody')[0];
    var proposedD = table.rows[9].cells[1].innerHTML.split("-");    
    var proposedDate = new Date(parseInt(proposedD[2]), returnMontIndex(proposedD[1]),parseInt(proposedD[0]));
    var formattedDate = proposedDate;
    formattedDate.setDate(formattedDate.getDate() + 7);
    return proposedDate;
}

function submitLSPCRClick()
{
    debugger   
    // alert("Data submitted successfully");
    var table = document.getElementById('LSPCRTable').getElementsByTagName('tbody')[0];
    var LSPCRModel={
        JobId: table.rows[0].cells[1].innerHTML,
        From: table.rows[3].cells[1].innerHTML,
        //FromAddress: table.rows[2].cells[1].childNodes[0].data,
        To: table.rows[5].cells[1].innerHTML,
        //ToAddress: table.rows[4].cells[1].childNodes[0].data,
        SKU: table.rows[7].cells[1].innerHTML,
        Quantity: table.rows[8].cells[1].innerHTML,
        ProposedPickUpDate: table.rows[9].cells[1].innerHTML,
        ConfirmedPickUpDate: document.getElementById('PickUpDate').value,
        Vendor: table.rows[1].cells[1].innerHTML,
        Remarks: table.rows[11].cells[1].innerHTML
    }
    
    postLSPCRData(LSPCRModel);

}

function postLSPCRData(LSPCRModel)
{   
    var pathname = location.pathname.substring(location.pathname.length - 1, location.pathname.length) == '/' ? location.pathname.substring(0, location.pathname.length - 1) : location.pathname
    var pathnames = pathname.split('/');
    pathname = '/' + pathnames[1];
   // uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + pathname+'/api/LSPCR/SubmitLSPCR',
        contentType: 'application/json; charset=utf-8',
        data:JSON.stringify(LSPCRModel),
        success: function (data) {
          //  $.unblockUI();
            if (data.Success == true) {

              //  $.unblockUI();
                alert(data.Message)

            }
            else {

                alert(data.Message)
               /* var postSOErrAlt = noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });*/
            }

        //    $.unblockUI();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            //  $.unblockUI();
            alert("Error happened");
        }
    });


}