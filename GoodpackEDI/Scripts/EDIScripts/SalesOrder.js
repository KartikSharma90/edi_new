﻿
function f(IsFirst) {
    if (IsFirst == true) {
        f.count = 0
        // alert("Call No " + f.count)
    }

    else {
        f.count = ++f.count || 0 // f.count is undefined at first   	 
        //  alert("Call No " + f.count)
    }
    return f.count;
}

function date(IsFirst) {
    if (IsFirst == true) {
        date.count = 0
        //   alert("Date " + date.count)
    }

    else {
        date.count = ++date.count || 0 // f.count is undefined at first   	 
        //   alert("Date " + date.count)
    }
    return date.count;
}


jQuery(document).ready(function () {

    $("#salesBtnSearch").click(function () {
        //loadJQGrid();
        debugger;
        loadNewSOJQGrid();
        $("#SalesOrderEditGrid").hide();
        $("#SalesOrderGrid").show();
    });



    //$("#dtCustPO").datepicker({
    //    dateFormat: 'dd.mm.yy',
    //    maxDate: 0,
    //    changeYear: true,
    //    onSelect: function () {
    //        var selDate = $(this).datepicker('getDate');
    //       // alert(selDate);
    //    }
    //}).attr('readonly', 'readonly');

    var currentDate=new Date();
    $('#dtCustPO').datetimepicker({
        format: 'DD.MM.YYYY',
        defaultDate: currentDate
    });
  //  $("#dtCustPO").datepicker("setDate", currentDate);


});


function CustomerPODateClick() {
    debugger;
    $("#dtCustPO").datepicker('show');
}



function loadJQGrid() {
    debugger;
    var sortFlag = 0;
    //var ab = user.SAPCode();
    //alert(ab);
    var custNo = AppViewModel.selectedEntityValue().EntityCode;

    var startd = $("#dt1").val();
    var endd = $("#dt2").val();
    if (endd == "") {
        var enddAlt = noty({
            layout: 'top',
            type: 'warning',
            text: 'Please input End date.',
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else {
        $('#SalesOrderGrid').jqGrid('GridUnload');
        $("#SalesOrderGrid").jqGrid({          
            url: baseUrl + '/api/SO/ViewOrder?startDate=' + startd + '&endDate=' + endd + '&custNo=' + custNo,
            datatype: "json",
            myType: "GET",
            //colNames: ['Actions', 'Country Key', 'Consignee Name', 'Consignee Customer No.', 'Region (State, Province, County)', 'Packer Name', 'Packer Customer No.', 'Material No.', 'Customer Purchase Order No.', 'Schedule Line date', 'Lead days', 'ITR Confirm Receipt Date', 'Planned Week', 'Cumulative Order Quantity in Sales Units', 'ITR Confirm Receipt Quantity', 'Date'],
            colNames: ['Country Key', /*'Consignee Name','Consignee Customer No.', 'Region (State, Province, County)', */'Packer Name', 'Packer Customer No.', 'Material No.', 'Customer Purchase Order No.', 'Delivery date',/* 'Lead days',*/ /*'ITR Confirm Receipt Date', */'Planned Week', 'Cumulative Order Quantity in Sales Units', /*'ITR Confirm Receipt Quantity',*/ 'Actions'/*, 'Date'*/],
            colModel: [
            { name: 'cntryKey', index: 'cntryKey', align: 'center' },
          ////  { name: 'congName', index: 'congName', align: 'center' },
          ////  { name: 'congCustNum', index: 'congCustNum', align: 'center' },
          ////  { name: 'reg', index: 'reg', align: 'center' },
            { name: 'pkrName', index: 'pkrName', align: 'center' },
            { name: 'packCustNum', index: 'packCustNum', align: 'center' },
            { name: 'matNm', index: 'matNm', align: 'center', editable: true, edittype: "select", editoptions: { value: "MB3:MB3;MB4:MB4;MB5:MB5;MB7:MB7;MB5H:MB5H" } },
            { name: 'custPrchOrdNo', index: 'custPrchOrdNo', align: 'center' },
            { name: 'schLneDte', index: 'schLneDte', align: 'center', formatter: dateFormatter },
         ////   { name: 'leadDys', index: 'leadDys', align: 'center' },
        ////    { name: 'itrCnfDte', index: 'itrCnfDte', align: 'center', formatter: dateFormatter },
            { name: 'plndWk', index: 'plndWk', align: 'center' },
            { name: 'cuOrdQty', index: 'cuOrdQty', align: 'center', formatter: roundInteger, editable: true, },
         ////   { name: 'itrCnfQty', index: 'itrCnfQty', align: 'center', formatter: roundInteger },
          ////  { name: 'dte', index: 'dte', align: 'center' }
           { name: 'act', index: 'act', width: 160, sortable: false, /*datatype: 'html'*/ }

            ],
            jsonReader: {
                root: 'sData',
                id: 'custPrchOrdNo',
                repeatitems: false
            },
            pager: $('#salesPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit: false,
            viewrecords: true,
            loadonce: true,
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Sales Order Record",
            //commented below line by vivek, as pagination doesnt need any message
            /*    beforeRequest: function () {
                    if (sortFlag == 0) {
                        //alert(noty({ text: 'Report generation will take some time, please continue using the website.You will get notified once report is generated.' }));
                        var msg1 = noty({
                            layout: 'top',
                            type: 'warning',
                            text: 'Report generation will take some time, please continue using the website.You will get notified once report is generated.',
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                        //alert("Report generation will take some time, please continue using the website.You will get notified once report is generated.");
                    }
                },*/
            //loadComplete: function (sData) {
            //    // $('#SalesOrderGrid').getGridParam('userData')
            //    //alert(Page);
            //    if (sData == null) {
            //        //alert("No Data to Display or Network Error !");
            //        var noData = noty({
            //            layout: 'top',
            //            type: 'error',
            //            text: 'No Data to Display or Network Error !',
            //            dismissQueue: true,
            //            animation: {
            //                open: { height: 'toggle' },
            //                close: { height: 'toggle' },
            //                easing: 'swing',
            //                speed: 500
            //            },
            //            timeout: 5000
            //        });
            //        $("#SalesOrderGrid").jqGrid('setGridState', 'hidden');
            //    }
            //    else if (sortFlag == 0) {




            //        //alert("Sales Order Report Generated.");
            //        var msg2 = noty({
            //            layout: 'top',
            //            type: 'success',
            //            text: 'Sales Order Report Generated.',
            //            dismissQueue: true,
            //            animation: {
            //                open: { height: 'toggle' },
            //                close: { height: 'toggle' },
            //                easing: 'swing',
            //                speed: 500
            //            },
            //            timeout: 5000
            //        });
            //        sortFlag = 1;
            //    }


            //},

            loadComplete: function () {
                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                debugger;

                var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i];

                    //commented by Abina as the itr logic is not needed any more 
                    //var qty = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'itrCnfQty');
                    //if (qty == 0) {
                        //$('#SalesOrderGrid').jqGrid('editRow', rowId, false);
                        be = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:35px;' type='button' Id='" + rowId + "edit" + "' value='Edit' onclick=\"editButtonClick('" + rowId + "')\"  />";
                        ce = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button' Id='" + rowId + "cancel" + "' value='Cancel' onclick=\"cancelButtonClick('" + rowId + "')\" />"
                        se = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button' Id='" + rowId + "submit" + "' value='Submit' onclick=\"submitButtonClick('" + rowId + "')\"  />";
                        $("#SalesOrderGrid").jqGrid('setRowData', ids[i], { act: be + ce + se });
                    //}
                    //else {
                    //}
                }
                //alert("completed");

                //var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');

                //for (var i = 0; i < ids.length; i++) {
                //    var cl = ids[i];
                //    //var ret = myGrid.jqGrid('getRowData', id);
                //    be = "<input style='height:22px;width:20px;' type='button' value='E' onclick=\"jQuery('#SalesOrderGrid').editRow('" + cl + "');\"  />";
                //    se = "<input style='height:22px;width:20px;' type='button' value='S' onclick=\"$('#SalesOrderGrid').saveRow('" + cl + "');\"  />";
                //    ce = "<input style='height:22px;width:20px;' type='button' value='C' onclick=\"$('#SalesOrderGrid').restoreRow('" + cl + "');\" />";
                //    $("#SalesOrderGrid").jqGrid('setRowData', ids[i], { act: be + se + ce });
                //}

                //var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');
                //for (var i = 0; i < ids.length; i++) {
                //    var rowId = ids[i];
                //    var qty = jQuery("#SalesOrderGrid").jqGrid('getCell', rowId, 'itrCnfQty');
                //    if (qty == 0) {
                //        jQuery('#SalesOrderGrid').jqGrid('editRow', rowId, false);
                //    }
                //    else {
                //    }
                //}


            },
        });

        $('#SalesOrderGrid').jqGrid('navGrid', '#salesPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#SalesOrderGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});

    }

}





function loadNewSOJQGrid() {
    debugger;
    if (document.getElementById('DDViewcustomer').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the customer.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }

    else {
        var sortFlag = 0;
        //var custNo = AppViewModel.selectedEntityValue().EntityCode;
        custNo = AppViewModel.selectedEntityValue()[0].EntityCode;

        var startd = $("#dt1").val();
        var endd = $("#dt2").val();
        if (endd == "") {
            var enddAlt = noty({
                layout: 'top',
                type: 'warning',
                text: 'Please input End date.',
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        else {
            $('#SalesOrderGrid').jqGrid('GridUnload');
            $("#SalesOrderGrid").jqGrid({

                url: baseUrl + '/api/SO/ViewNewSOOrder?startDate=' + startd + '&endDate=' + endd + '&custNo=' + custNo,
                datatype: "json",
                myType: "GET",

                colNames: ['SI Number', 'SO Number', 'Packer Name', 'Packer Code', 'SI ETD', 'Quantity', 'SKU', 'Planned week', 'ITR Cnf Qty'],
                colModel: [
                //{ name: 'cntryKey', index: 'cntryKey', align: 'center' },
                //{ name: 'congName', index: 'congName', align: 'center' },
                //{ name: 'congCustNum', index: 'congCustNum', align: 'center' },
                //{ name: 'reg', index: 'reg', align: 'center' },
                { name: 'custPrchOrdNo', index: 'custPrchOrdNo', align: 'center' },
                { name: 'vbeln', index: 'vbeln', align: 'center' },
                { name: 'pkrName', index: 'pkrName', align: 'center' },
                { name: 'packCustNum', index: 'packCustNum', align: 'center' },
                { name: 'vdatu', index: 'vdatu', align: 'center', /*formatter: REQdateFormatter,*/ editable: true, editoptions: { dataInit: function (el) { setTimeout(function () { $(el).datepicker({ dateFormat: "yy-mm-dd" }); }, 200); } } },
                { name: 'cuOrdQty', index: 'cuOrdQty', align: 'center', formatter: roundInteger, editable: true, },
                { name: 'matNm', index: 'matNm', align: 'center', editable: true, edittype: "select", editoptions: { value: "MB3:MB3;MB4:MB4;MB5:MB5;MB7:MB7;MB5H:MB5H" } },
                { name: 'plndWk', index: 'plndWk', align: 'center' },
                 { name: 'itrCnfQty', index: 'itrCnfQty', align: 'center', hidden: true },
              // { name: 'act', index: 'act', width: 160, sortable: false, /*datatype: 'html'*/ }

                ],
                //onSelectRow: function (id, status) {
                //    //Removes Row highlight
                //    $('#SalesOrderGrid tr').removeClass("ui-state-highlight");

                    //if (status) {

                    //    $('#SalesOrderGrid')[0].p.selarrrow = $('#SalesOrderGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    //    .map(function () { return this.id; }) // convert to set of ids
                    //    .get();
                    //}
                //},
                jsonReader: {
                    root: 'sData',
                    id: 'custPrchOrdNo',
                    repeatitems: false
                },
                pager: $('#salesPager'),
                excel: true,
                rowNum: 10,
                rowList: [10, 50, 200, 500, 1000],
                autowidth: true,
                shrinkToFit: true,
                viewrecords: true,
                loadonce: true,
                autoencode: false,
               // multiselect: true,   //checkbox needed here commented by Abina
                height: 'auto',
                width: '100%',
                caption: "Sales Order Record",

                loadComplete: function () {
                    $("tr.jqgrow:odd").css("background", "#f1f1f1");
                    // $("tr.jqgrow:odd").hover(function () { $("tr.jqgrow:odd").css("background", "#d9e4f0"); });
                    debugger;
                    var mode;

                        var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');
                        for (var i = 0; i < ids.length; i++) {
                            var rowId = ids[i];

                            //commented by Abina as the itr logic is not needed any more 

                           // $("#SalesOrderGrid").jqGrid('showCol', 'cb');
                            //var qty = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'itrCnfQty');
                            //var plandweek = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'plndWk');
                            //if (qty == 0 || plandweek != '') {
                                //$('#SalesOrderGrid').jqGrid('editRow', rowId, false);
                                //be = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:35px;' type='button'  Id='" + rowId + "edit" + "' value='Edit' onclick=\"editButtonClick('" + rowId + "')\"  />";
                                //ce = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button'  Id='" + rowId + "cancel" + "' value='Cancel' onclick=\"cancelButtonClick('" + rowId + "')\" />"
                                //se = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button' Id='" + rowId + "submit" + "' value='Submit' onclick=\"submitButtonClick('" + rowId + "')\"  />";
                                //$("#SalesOrderGrid").jqGrid('setRowData', ids[i], { act: be + ce + se });
                            //}
                            //else {
                            //}
                        }
                    //}
                    //else {
                    //    $("#SalesOrderGrid").jqGrid('hideCol', 'cb');
                    //    var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');
                    //    for (var i = 0; i < ids.length; i++) {
                    //        var rowId = ids[i];
                           
                    //        //var qty = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'itrCnfQty');
                    //        //var plandweek = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'plndWk');
                    //        //if (qty == 0 || plandweek != '') {
                    //            //$('#SalesOrderGrid').jqGrid('editRow', rowId, false);
                    //            //be = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:35px;' type='button'  Id='" + rowId + "edit" + "' value='Edit' onclick=\"editButtonClick('" + rowId + "')\"  />";
                    //            //ce = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button'  Id='" + rowId + "cancel" + "' value='Cancel' onclick=\"cancelButtonClick('" + rowId + "')\" />"
                    //            //se = "<input style='height:22px;background-color:#004c97;color:#ffffff;border-radius:5px; width:60px;display:none' type='button' Id='" + rowId + "submit" + "' value='Submit' onclick=\"submitButtonClick('" + rowId + "')\"  />";
                    //            //$("#SalesOrderGrid").jqGrid('setRowData', ids[i], { act: be + ce + se });
                    //        //}
                    //        //else {
                    //        //}
                    //    }

                   // }
                },
            });

            $('#SalesOrderGrid').jqGrid('navGrid', '#salesPager', {
                edit: false, view: false, add: false, del: false, search: true,
                beforeRefresh: function () {
                    //alert('In beforeRefresh');
                    $('#SalesOrderGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                }
            }, {}, {}, {}, { closeAfterSearch: true }, {});
        }
    }
}


function ceateButton() {

    //alert("Edit Clicked");
    var ids = $("#SalesOrderGrid").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var rowId = ids[i];
        debugger
        var qty = $("#SalesOrderGrid").jqGrid('getCell', rowId, 'itrCnfQty');
        if (i == 607) {
            alert(qty);
        }
        if (qty != "0") {
            return null;
        }
        else {
            return "<input style='height:22px;background-color:#004c97;' type='button' value='Edit' onclick=\"editButtonClick() \"  />" + "<input style='height:22px;' type='button' value='Save' onclick=\"saveButtonClick() \"  />"
        }

    }
    //return "<input style='height:22px;' type='button' value='Edit' onclick=\"editButtonClick() \"  />" + "<input style='height:22px;' type='button' value='Save' onclick=\"saveButtonClick() \"  />"
}

var salesOrderData = new Array();
function editButtonClick(rowId) {
    debugger
    var selectedRowData = $("#SalesOrderGrid").getRowData(rowId);
    var deliverydate = formatVDate(selectedRowData.vdatu);
    salesOrderData[0] = selectedRowData.matNm;
    salesOrderData[1] = selectedRowData.cuOrdQty;
    salesOrderData[2] = deliverydate;
  //  alert(salesOrderData);
    //$('#SalesOrderGrid').editRow("" + rowId + "");
    $('#SalesOrderGrid').jqGrid('editRow', rowId, false, requestPickdates);

    document.getElementById(rowId + "submit").style.display = "inline";
    document.getElementById(rowId + "edit").style.display = "none";
    document.getElementById(rowId + "cancel").style.display = "inline";
    
}

function requestPickdates(id) {
    debugger
    $("#" + id + "_vdatu", "#SalesOrderGrid").datepicker({
        dateFormat: "yy-mm-dd",
        changeYear: true
    }).attr('readonly', 'readonly');
    var selectedRowData = $("#SalesOrderGrid").getRowData(id);
    var currentDate = $("#" + id + "_vdatu").val();

    //var currentDate = new Date();
    $("#" + id + "_vdatu", "#SalesOrderGrid").datepicker("setDate", currentDate);
}

function formatVDate(input) {
    var datePart = input.match(/\d+/g),
    year = datePart[0]; // get only two digits
    month = datePart[1];
    day = datePart[2];
    return day + '.' + month + '.' + year;
}


function cancelButtonClick(rowId) {
    debugger

    $('#SalesOrderGrid').restoreRow("" + rowId + "");
    document.getElementById(rowId + "submit").style.display = "none";
    document.getElementById(rowId + "edit").style.display = "inline";
    document.getElementById(rowId + "cancel").style.display = "none";
}

var newSOData = new Array();
//below method SOEdit is created by Abina...its using Now
function SOEdit(rowId) {
   // uiBlocker();
    debugger;
    //document.getElementById("EditSO1").disabled = false;
    document.getElementById("r1").style.display = 'block';
    document.getElementById("r2").style.display = 'block';
    document.getElementById("r3").style.display = 'block';
    var isShow = Boolean(1);

    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SO/AllReasonsForEdit',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (isShow) {
                $("#soEditReason").dialog("open");
            }
            else {
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "No changes founded",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
               
            }
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.availableReasonsMatNo([]);
                AppViewModel.availableReasonsQty([]);
                AppViewModel.availableReasonsReqDate([]);
                var data1 = [];
                var data2 = [];
                var data3 = [];
                var i = 0;
                for (var m = 0; m < data.length; m++)
                {
                    if(data[m].ReasonType=="MaterialNumber")
                    {                        
                        data1 = data[m];
                        AppViewModel.availableReasonsMatNo.push(data1);
                    }
                    if (data[m].ReasonType=="Quantity") {
                        data2 = data[m];
                        AppViewModel.availableReasonsQty.push(data2);
                    }
                    if (data[m].ReasonType=="ReqDate") {
                        data3 = data[m];
                        AppViewModel.availableReasonsReqDate.push(data3);
                    }
                }
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Reasons",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

    $("#SalesOrderGrid").jqGrid('saveRow', rowId, false);
    var selectedRowData = $("#SalesOrderGrid").getRowData(rowId);
    var deliverydate = formatVDate(selectedRowData.vdatu);
    newSOData[0] = selectedRowData.matNm;
    newSOData[1] = selectedRowData.cuOrdQty;
    newSOData[2] = deliverydate;
    alert(newSOData);
    $('#SalesOrderGrid').restoreRow("" + rowId + "");

    if (salesOrderData[0] != newSOData[0] && salesOrderData[1] != newSOData[1] && salesOrderData[2] != newSOData[2]) {
        //document.getElementById("r1").style.visibility = 'visible';
        document.getElementById("r1").style.display = 'block';
        document.getElementById("r2").style.display = 'block';
        document.getElementById("r3").style.display = 'block';
        //.disabled =false;

    }
    if (salesOrderData[0] != newSOData[0] && salesOrderData[1] == newSOData[1] && salesOrderData[2] == newSOData[2]) {
        //document.getElementById("r2").style.visibility = 'hidden';
        // .disabled =true;
        document.getElementById("r2").style.display = 'none';
        document.getElementById("r3").style.display = 'none';

    }
    if (salesOrderData[0] == newSOData[0] && salesOrderData[1] != newSOData[1] && salesOrderData[2] == newSOData[2]) {
        document.getElementById("r1").style.display = 'none';
        document.getElementById("r3").style.display = 'none';

    }
    if (salesOrderData[0] == newSOData[0] && salesOrderData[1] == newSOData[1] && salesOrderData[2] != newSOData[2]) {

        document.getElementById("r1").style.display = 'none';
        document.getElementById("r2").style.display = 'none';

    }
    if (salesOrderData[0] != newSOData[0] && salesOrderData[1] != newSOData[1] && salesOrderData[2] == newSOData[2]) {

        document.getElementById("r3").style.display = 'none';

    }
    if (salesOrderData[0] != newSOData[0] && salesOrderData[1] == newSOData[1] && salesOrderData[2] != newSOData[2]) {

        document.getElementById("r2").style.display = 'none';

    }
    if (salesOrderData[0] == newSOData[0] && salesOrderData[1] != newSOData[1] && salesOrderData[2] != newSOData[2]) {

        document.getElementById("r1").style.display = 'none';
    }
    if (salesOrderData[0] == newSOData[0] && salesOrderData[1] == newSOData[1] && salesOrderData[2] == newSOData[2]) {

        isShow = Boolean(0);      
    }
   
    debugger;
    $("#soEditReason").dialog({
        autoOpen: false,
       height: "auto",
       width: "auto", zIndex: 50000,
        modal: true,
        buttons: {
            "OK": function () {
                debugger;
                uiBlocker();
                    var flag1 = AppViewModel.selectedReasonForEditMatNo() || "0";
                    var flag2 = AppViewModel.selectedReasonForEditQty() || "0";
                    var flag3 = AppViewModel.selectedReasonForEditReqDate() || "0";
                    if (flag1 != 0 && flag2 == 0 && flag3 == 0)
                    {
                        var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                        var reason2 = "0";
                        var reason3 = "0";

                    }
                    if (flag1 != 0 && flag2 != 0 && flag3 == 0) {
                        var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                        var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                        var reason3 = "0";

                    }
                    if (flag1 != 0 && flag2 != 0 && flag3 != 0) {
                        var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                        var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                        var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                    }
                    if (flag1 == 0 && flag2 == 0 && flag3 != 0)
                    {
                        var reason1 = "0";
                        var reason2 = "0";
                        var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                    }
                    if (flag1 == 0 && flag2 != 0 && flag3 == 0) {
                        var reason1 = "0";
                        var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                        var reason3 = "0";

                    }
                    if (flag1 == 0 && flag2 != 0 && flag3 != 0) {
                        var reason1 = "0";
                        var reason2 = AppViewModel.selectedReasonForEditQty()[0].ReasonCode;
                        var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                    }
                    if (flag1 != 0 && flag2 == 0 && flag3 != 0) {
                        var reason1 = AppViewModel.selectedReasonForEditMatNo()[0].ReasonCode;
                        var reason2 = "0";
                        var reason3 = AppViewModel.selectedReasonForEditReqDate()[0].ReasonCode;

                    }
                        var selectedRowData = $("#SalesOrderGrid").getRowData(rowId);
                        var cellData = [];
                        cellData.push(["false"]);
                        cellData.push([reason1]);
                        cellData.push([reason2]);
                        cellData.push([reason3]);
                        var rowData = new Array();

                        rowData[0] = AppViewModel.selectedEntityValue()[0].EntityCode;
                        rowData[1] = selectedRowData.packCustNum;
                        var deliverydate = formatVDate(selectedRowData.vdatu);

                        rowData[2] = deliverydate;
                        rowData[3] = selectedRowData.matNm;
                        rowData[4] = selectedRowData.cuOrdQty;
                        rowData[5] = selectedRowData.custPrchOrdNo;
                       // alert(rowData);
                        cellData.push(rowData);
                       // alert(cellData);
                        postSOData(cellData);
                    $(this).dialog("close");
            },
            Cancel: function () {                
                $(this).dialog("close");               
                $("#SalesOrderGrid").jqGrid('setCell', rowId, 'matNm', salesOrderData[0]);
                $("#SalesOrderGrid").jqGrid('setCell', rowId, 'cuOrdQty', salesOrderData[1]);
                $("#SalesOrderGrid").jqGrid('setCell', rowId, 'vdatu', salesOrderData[2]);
                $("#SalesOrderGrid").jqGrid('saveRow', rowId, false);
            }
        },
        close: function () {
            $(this).dialog("close");
            $("#SalesOrderGrid").jqGrid('setCell', rowId, 'matNm', salesOrderData[0]);
            $("#SalesOrderGrid").jqGrid('setCell', rowId, 'cuOrdQty', salesOrderData[1]);
            $("#SalesOrderGrid").jqGrid('setCell', rowId, 'vdatu', salesOrderData[2]);
            $("#SalesOrderGrid").jqGrid('saveRow', rowId, false);
        
        }
    });
  
}
function submitButtonClick(rowId) {
    debugger
   
   // $("#soEditReason").dialog("open");
    SOEdit(rowId);
  //  $('#SalesOrderGrid').saveRow("" + rowId + "");
  ////  var reason = AppViewModel.editSOReason();
  //  var selectedRowData = $("#SalesOrderGrid").getRowData(rowId);
  //  var cellData = [];
  //  cellData.push(["false"]);
  //  cellData.push([""]);
  //  var rowData = new Array();
  //  rowData[0] = AppViewModel.selectedEntityValue()[0].EntityCode
  //  rowData[1] = selectedRowData.packCustNum;
  //  var deliverydate = formatVDate(selectedRowData.vdatu);

  //  rowData[2] = deliverydate;
  //  rowData[3] = selectedRowData.matNm;
  //  rowData[4] = selectedRowData.cuOrdQty;
  //  rowData[5] = selectedRowData.custPrchOrdNo;

  //  cellData.push(rowData);

  //  postSOData(cellData);
    
}

function fromDateClick() {    
    $("#dtEdit1").datepicker('show');
}
function toDateClick() {
    $("#dtEdit2").datepicker('show');
}
function fromDateCancelClick() {
    $("#dtCancel1").datepicker('show');
}

function toDateCancelClick() {
    $("#dtCancel2").datepicker('show');
}

function fromDeliveryDateClick() {
    debugger
    // $("#dtDeliveryStart").datepicker('show');
    
}
function fromDateViewClick() {
    $("#dt1").datepicker('show');
}
function toDateViewClick() {
    $("#dt2").datepicker('show');
}


(function (exports) {

    exports.field = {};
    var _value = '';

    exports.field.get = function () {
        return _value;
    }

    exports.field.set = function (val) {
        _value = val;
    }

})(window);

function DeliveryDateBtnClick(button) {

    var seletedDataPickerId = button.id;
    field.set(seletedDataPickerId);

    $("#TBDate" + seletedDataPickerId).datepicker({
        dateFormat: 'dd.mm.yy',
        changeYear: true,
        onSelect: function (selected, evnt) {
            //updateAb(selected);

            var seletedDataPickerId = field.get();
            var seletedDate = (document.getElementById("TBDate" + seletedDataPickerId).value);
            var currentPOValue = (document.getElementById("TBPONum" + seletedDataPickerId).value);
            var startIndex = currentPOValue.indexOf("_");
            var endIndex = currentPOValue.lastIndexOf("_");
            var currentPODate = currentPOValue.substring(startIndex + 1, endIndex);
            var newPOValue = currentPOValue.replace(currentPODate, seletedDate);
            document.getElementById("TBPONum" + seletedDataPickerId).value = newPOValue;

        }
    }).attr('readonly', 'readonly');

    $("#TBDate" + seletedDataPickerId).datepicker('show');
}
function DeliveryDateBtnClickNew(button) {

    var seletedDataPickerId = button.id;
    field.set(seletedDataPickerId);

    $("#TBDate" + seletedDataPickerId).datetimepicker({
        dateFormat: 'dd.mm.yy',
        changeYear: true,
        onSelect: function (selected, evnt) {            //updateAb(selected);

            var seletedDataPickerId = field.get();
            var seletedDate = (document.getElementById("TBDate" + seletedDataPickerId).value);
            var currentPOValue = (document.getElementById("TBPONum" + seletedDataPickerId).value);
            var startIndex = currentPOValue.indexOf("_");
            var endIndex = currentPOValue.lastIndexOf("_");
            var currentPODate = currentPOValue.substring(startIndex + 1, endIndex);
            var newPOValue = currentPOValue.replace(currentPODate, seletedDate);
            document.getElementById("TBPONum" + seletedDataPickerId).value = newPOValue;
        }
    }).attr('readonly', 'readonly');

    $("#TBDate" + seletedDataPickerId).datetimepicker('show');
}


AppViewModel.selectedCustomerValue.subscribe(function (newValue) {   
    if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
    //alert(AppViewModel.selectedCustomerValue().EntityCode)
    //  var selectedCustomer = AppViewModel.selectedCustomerValue().EntityCode;
    //AppViewModel.selectedEntityCode(newValue[0].EntityCode);
    AppViewModel.selectedPackerValue('');
    debugger
    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SO/GetMappedPacker?customercode=' + newValue[0].EntityCode,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            debugger
            AppViewModel.selectedPacker([]);
            AppViewModel.selectedPacker(data);

            //AppViewModel.selectedPackerValue(AppViewModel.selectedPacker()[0].EntityCode);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });

});



AppViewModel.selectedMapSOSubsi.subscribe(function (newValue) {

    debugger

    if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
    if (newValue != null) {
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SC/MappedFiles?subsiId=' + newValue[0].SubsiId+ "&transactionType=" + AppViewModel.transactionType(),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                debugger
                if (data.length != 0) {
                    debugger
                    AppViewModel.selectedSOMapper([]);
                    AppViewModel.selectedSOMapper(data);
                    AppViewModel.selectedSOMaperValue(AppViewModel.mappedFileData()[0]);
                }
                else {
                    AppViewModel.selectedSOMapper('');
                    $.unblockUI();
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "No Mapper for the selected subsi",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
    }

});


//AppViewModel.selectedEntityValue.subscribe(function (newValue) {
//    if (!newValue) { return; }
//    debugger;
//    loadNewSOJQGrid();

//});



function onSOGenerateClick() {

    if (document.getElementById('DDcustomer').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the customer.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else if (document.getElementById('DDpacker').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the delivery location.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else if (document.getElementById('DDBIN').selectedIndex == 0) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the SKU type",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }

    else if (document.getElementById('dtDeliveryStart').value == "" || document.getElementById('dtDeliveryStart').value == null) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please select the From Date",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }



    else if (AppViewModel.totalQty() == 0 || AppViewModel.totalQty() == "" || AppViewModel.truckQty() == 0 || AppViewModel.truckQty() == "" || AppViewModel.qtyperTruck() == 0 || AppViewModel.qtyperTruck() == "") {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Please enter a valid number.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else {

        var customer = AppViewModel.selectedCustomerValue()[0].EntityCode;
        var packer = AppViewModel.selectedPackerValue()[0].EntityCode;
        //var packer = AppViewModel.selectedPackerValue().EntityCode;
        var BIN = AppViewModel.selectedBINValue()[0].value;
        var totalQty = AppViewModel.totalQty();
        var totalTrucks = AppViewModel.truckQty();
        var qtyperTruck = AppViewModel.qtyperTruck();

        if (totalQty % (totalTrucks * qtyperTruck) == 0) {
            // alert("OK");
            $("#SOtable").show();

            var totalNumberofDays = totalQty / (totalTrucks * qtyperTruck)
            var tableRef = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
            var tableRows = tableRef.getElementsByTagName('tr');
            var rowCount = tableRows.length;

            debugger

            for (var x = rowCount - 1; x >= 0; x--) {
                tableRef.removeChild(tableRows[x]);
            }

            /* var elmtTable = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
             var tableRows = elmtTable.getElementsByTagName('tr');
             var rowCount = tableRows.length;
     
             for (var x = rowCount - 1; x >= 0; x--) {
                 elmtTable.removeChild(tableRows[x]);
             }*/

            debugger

            var selectedDate = document.getElementById('dtDeliveryStart').value;

            var res = selectedDate.split(".");

            var someDate = new Date();

            someDate.setDate(res[0]);
            someDate.setMonth((res[1] - 1));
            someDate.setFullYear(res[2]);


            //// someDate.setDate(selectedDate);
            //// var numberOfDaysToAdd = k;
            ////  someDate.setDate(someDate.getDate() + numberOfDaysToAdd);


            var totalRowCount = f(true);
            date(true);

            for (var k = 0; k < totalNumberofDays; k++) {
                // Insert a row in the table at row index 0

                debugger

                if (k == 0) {
                    someDate.setDate(someDate.getDate());
                    if (someDate.getDay() == 6)
                    { someDate.setDate(someDate.getDate() + 2); }
                    if (someDate.getDay() == 0)
                    { someDate.setDate(someDate.getDate() + 1); }
                }
                else {
                    someDate.setDate(someDate.getDate() + 1);
                    if (someDate.getDay() == 6)
                    { someDate.setDate(someDate.getDate() + 2); }
                    if (someDate.getDay() == 0)
                    { someDate.setDate(someDate.getDate() + 1); }
                }




                if (k < totalNumberofDays && k != 0) {
                    date(false);
                }

                for (var j = 0; j < totalTrucks; j++) {


                    var newRow = tableRef.insertRow(tableRef.rows.length);
                    newRow.id = "ROW" + totalRowCount;

                    debugger

                    newRow.style.border = '1px solid #ccc';
                    //if (totalRowCount != 0 && totalRowCount % 2 != 0) {
                    //    newRow.style.backgroundColor = "white";
                    //}
                    //   for (var i = 0; i < 5; i++) {

                    // Insert a cell in the row at index 0
                    var customerCodecell = newRow.insertCell(0);
                    customerCodecell.style.border = '1px solid #ccc';
                    customerCodecell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(customer)
                    customerCodecell.appendChild(newText);


                    var packerCodecell = newRow.insertCell(1);
                    packerCodecell.style.border = '1px solid #ccc';
                    packerCodecell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(packer)
                    packerCodecell.appendChild(newText);

                    // to append zero before month number
                    var day = (someDate.getDate()).toString();
                    day = day.length < 2 ? '0' + day : day;

                    var month = (someDate.getMonth() + 1).toString();
                    month = month.length < 2 ? '0' + month : month;

                    var deliverydate = day + '.' + month + '.' + someDate.getFullYear();
                    var datecell = newRow.insertCell(2);
                    datecell.style.border = '1px solid #ccc';
                    datecell.style.textAlign = "center";
                    // Append a text node to the cell
                    //// var newText = document.createTextNode(deliverydate)
                    //// datecell.appendChild(newText);

                    var tdate = document.createElement("input");
                    tdate.id = "TBDateDT" + totalRowCount;
                    tdate.style.width = "90px";
                    tdate.readOnly = "true";
                    tdate.value = deliverydate;
                    //var newText = document.createTextNode(customer+'_'+deliverydate+'_'+(j+1))
                    datecell.appendChild(tdate);

                    var dateBTN = document.createElement("input");
                    dateBTN.type = "button";
                    dateBTN.style.width = "23px";
                    dateBTN.style.marginLeft = "2px";
                    dateBTN.style.marginTop = "1px";
                    // deleteBTN.style.height = "10px";
                    dateBTN.style.backgroundRepeat = "no-repeat";
                    var imgUrl = baseUrl + "/Images/dayPickerS.png";
                    dateBTN.style.backgroundImage = "url(" + imgUrl + ")";
                    dateBTN.id = "DT" + totalRowCount;
                    // var id = totalRowCount;
                    // deleteBTN.value = "Delete";
                    dateBTN.onclick = function () { DeliveryDateBtnClick(this) };
                    //var dateBTN = "<div class='input-group date' ><input type='text' class='form-control' id='" + "DT" + totalRowCount; +"' />" +
                    //            "<span class='input-group-addon'><span class='glyphicon glyphicon-calendar'></span></span></div>";
                            
                    datecell.appendChild(dateBTN);

                    var BINcell = newRow.insertCell(3);
                    BINcell.style.border = '1px solid #ccc';
                    BINcell.style.textAlign = "center";
                    //BINcell.id = "BINCELL" + totalRowCount;
                    // Append a text node to the cell
                    //  var newText = document.createTextNode(BIN)
                    var element2 = document.createElement("select");
                    element2.id = "DDBinType" + totalRowCount;

                    var option1 = document.createElement("option");
                    option1.innerHTML = "MB3";
                    option1.value = "MB3";
                    if (BIN == "MB3") {
                        option1.selected = true;
                    }
                    element2.add(option1, null);

                    var option2 = document.createElement("option");
                    option2.innerHTML = "MB4";
                    option2.value = "MB4";
                    if (BIN == "MB4") { option2.selected = true; }
                    element2.add(option2, null);

                    var option3 = document.createElement("option");
                    option3.innerHTML = "MB5";
                    option3.value = "MB5";
                    if (BIN == "MB5") { option3.selected = true; }
                    element2.add(option3, null);

                    var option5 = document.createElement("option");
                    option5.innerHTML = "MB5H";
                    option5.value = "MB5H";
                    if (BIN == "MB5H") { option5.selected = true; }
                    element2.add(option5, null);


                    var option4 = document.createElement("option");
                    option4.innerHTML = "MB7";
                    option4.value = "MB7";
                    if (BIN == "MB7") { option4.selected = true; }
                    element2.add(option4, null);
                    BINcell.appendChild(element2);
                    // BINcell.appendChild(newText);


                    var qtycell = newRow.insertCell(4);
                    qtycell.style.border = '1px solid #ccc';
                    qtycell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(qtyperTruck)
                    qtycell.appendChild(newText);


                    var POcell = newRow.insertCell(5);
                    POcell.style.border = '1px solid #ccc';
                    POcell.style.textAlign = "center";
                    // Append a text node to the cell
                    var t1 = document.createElement("input");
                    t1.id = "TBPONumDT" + totalRowCount;
                    t1.value = customer + '_' + deliverydate + '_' + (j + 1);
                    //var newText = document.createTextNode(customer+'_'+deliverydate+'_'+(j+1))
                    POcell.appendChild(t1);


                    debugger
                    // var deletecell = newRow.insertCell(6);
                    // //deletecell.innerHTML = '<input id="Button" type="button" style="background-image:url(/Images/deleteRow.png);background-repeat: no-repeat;" onclick:"deletebtnclick()" />';
                    // deletecell.style.border = '1px solid black';
                    // deletecell.style.textAlign = "center";
                    // // Append a text node to the cell
                    // var deleteBTN = document.createElement("input");
                    // deleteBTN.type = "button";
                    // //deleteBTN.style.width = "50px";
                    //// deleteBTN.style.height = "10px";
                    // //deleteBTN.style.backgroundRepeat = "no-repeat";
                    // //deleteBTN.style.backgroundImage = "url(/Images/deleteRow.png)";
                    // //t1.id = "TBPONum" + totalRowCount;
                    //// var id = totalRowCount;
                    // deleteBTN.value = "Delete";
                    // deleteBTN.id =totalRowCount;
                    // deleteBTN.onclick = function () { DeleteBtnClick(this) };
                    // deletecell.appendChild(deleteBTN);

                    // alert("row num : " + totalRowCount);

                    var totalRowCount = f(false);
                    //   alert(totalRowCount);

                }
            }


        } else {
            $("#SOtable").hide();
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Mismatch in quantities entered. Please check",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
    }
};


function DeleteBtnClick(button) {
    debugger
    //// alert(button.id);
    var x = button.id;
    // var y = 1;
    // z = Number(x) + Number(y);
    // //document.getElementById('SOCreateTable').deleteRow(Number(x));
    var deleteRow = document.getElementById("ROW" + x);
    // //elem.parentNode.removeChild(elem);
    var current = window.event.srcElement;
    // //here we will delete the line

    debugger

    var customer = "";
    var packer = "";
    var deletedDeliveryDate = "";
    var BIN = "";
    var qtyperTruck = "";
    var deletedCustomerPO = "";

    ////var table = document.getElementById('SOCreateTable');
    ////var rowData = new Array();


    ////var totalNumberofDays = totalQty / (totalTrucks * qtyperTruck)
    var table = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
    //// var tableRows = tableRef.getElementsByTagName('tr');
    ////var rowCount = tableRows.length;
    ////   var current = window.event.srcElement;


    debugger

    for (var c = 0, m = table.rows[0].cells.length; c < m; c++) {

        if (c == 3) {
            BIN = table.rows[0].cells[c].childNodes[0].value;
        }
        else if (c == 0) {
            customer = (table.rows[0].cells[c].innerHTML);
        }
        else if (c == 1) {
            var packer = (table.rows[0].cells[c].innerHTML);
        }
        else if (c == 4) {
            qtyperTruck = (table.rows[0].cells[c].innerHTML);
        }
    }

    while ((current = current.parentElement) && current.tagName != "TR");
    current.parentElement.removeChild(current);


    var tableRef = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];

    var lengthtee = tableRef.rows.length;
    // alert("row number after delete : " + lengthtee);




    var selectedDate = document.getElementById('dtDeliveryStart').value;
    var res = selectedDate.split(".");
    var someDate = new Date();
    someDate.setDate(res[0]);
    someDate.setMonth((res[1] - 1));
    someDate.setFullYear(res[2]);

    //var someDate = new Date();
    var numberOfDaysToAdd = date(false);
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd);



    var newRow = tableRef.insertRow(tableRef.rows.length);
    newRow.style.backgroundColor = "white";
    var tableRef = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
    var lengthtee = tableRef.rows.length;
    // alert("row number after recreate : " + lengthtee);


    var customerCodecell = newRow.insertCell(0);
    customerCodecell.style.border = '1px solid #ccc';
    customerCodecell.style.textAlign = "center";
    // Append a text node to the cell
    var newText = document.createTextNode(customer)
    customerCodecell.appendChild(newText);


    var packerCodecell = newRow.insertCell(1);
    packerCodecell.style.border = '1px solid #ccc';
    packerCodecell.style.textAlign = "center";
    // Append a text node to the cell
    var newText = document.createTextNode(packer)
    packerCodecell.appendChild(newText);

    var deliverydate = someDate.getDate() + '.' + (someDate.getMonth() + 1) + '.' + someDate.getFullYear();
    var datecell = newRow.insertCell(2);
    datecell.style.border = '1px solid #ccc';
    datecell.style.textAlign = "center";
    // Append a text node to the cell
    var newText = document.createTextNode(deliverydate)
    datecell.appendChild(newText);


    var BINcell = newRow.insertCell(3);
    BINcell.style.border = '1px solid #ccc';
    BINcell.style.textAlign = "center";
    //BINcell.id = "BINCELL" + totalRowCount;
    // Append a text node to the cell
    //  var newText = document.createTextNode(BIN)
    var element2 = document.createElement("select");
    element2.id = "DDBinType";

    var option1 = document.createElement("option");
    option1.innerHTML = "MB3";
    option1.value = "MB3";
    if (BIN == "MB3") {
        option1.selected = true;
    }
    element2.add(option1, null);

    var option2 = document.createElement("option");
    option2.innerHTML = "MB4";
    option2.value = "MB4";
    if (BIN == "MB4") { option2.selected = true; }
    element2.add(option2, null);

    var option3 = document.createElement("option");
    option3.innerHTML = "MB5";
    option3.value = "MB5";
    if (BIN == "MB5") { option3.selected = true; }
    element2.add(option3, null);

    var option5 = document.createElement("option");
    option5.innerHTML = "MB5H";
    option5.value = "MB5H";
    if (BIN == "MB5H") { option5.selected = true; }
    element2.add(option5, null);

    var option4 = document.createElement("option");
    option4.innerHTML = "MB7";
    option4.value = "MB7";
    if (BIN == "MB7") { option4.selected = true; }
    element2.add(option4, null);
    BINcell.appendChild(element2);
    // BINcell.appendChild(newText);


    var qtycell = newRow.insertCell(4);
    qtycell.style.border = '1px solid #ccc';
    qtycell.style.textAlign = "center";
    // Append a text node to the cell
    var newText = document.createTextNode(qtyperTruck)
    qtycell.appendChild(newText);


    var qtycell = newRow.insertCell(5);
    qtycell.style.border = '1px solid #ccc';
    qtycell.style.textAlign = "center";
    // Append a text node to the cell
    var t1 = document.createElement("input");
    //t1.id = "TBPONum" + totalRowCount;
    t1.value = customer + '_' + deliverydate + '_' + (1);
    //var newText = document.createTextNode(customer+'_'+deliverydate+'_'+(j+1))
    qtycell.appendChild(t1);


    debugger
    var deletecell = newRow.insertCell(6);
    //deletecell.innerHTML = '<input id="Button" type="button" style="background-image:url(/Images/deleteRow.png);background-repeat: no-repeat;" onclick:"deletebtnclick()" />';
    deletecell.style.border = '1px solid #ccc';
    deletecell.style.textAlign = "center";
    // Append a text node to the cell
    var deleteBTN = document.createElement("input");
    deleteBTN.type = "button";
    //deleteBTN.style.width = "50px";
    // deleteBTN.style.height = "10px";
    //deleteBTN.style.backgroundRepeat = "no-repeat";
    //deleteBTN.style.backgroundImage = "url(/Images/deleteRow.png)";
    //t1.id = "TBPONum" + totalRowCount;
    // var id = totalRowCount;
    deleteBTN.value = "Delete";
    //deleteBTN.id = totalRowCount;
    deleteBTN.onclick = function () { DeleteBtnClick(this) };
    deletecell.appendChild(deleteBTN);

}


function onNewOrderClick() {

    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SO/AllEntities',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.customers([]);
                AppViewModel.customers(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Customer List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
    AppViewModel.selectedBIN([{ "name": "MB3", "value": "MB3" }, { "name": "MB4", "value": "MB4" }, { "name": "MB5", "value": "MB5" }, { "name": "MB7", "value": "MB7" }, { "name": "MB5H", "value": "MB5H" }, { "name": "MB5HE", "value": "MB5HE" }])
    $("#AddnewSO").show();
    $("#SOInputFileds").show();
    $("#SOtable").hide();
    $("#viewSO").hide(); 
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#UploadSO').hide();
    $("#EditSO").hide();
    $("#CancelSO").hide();
   // $("#salesOrderDiv2").hide();
    var currentDate = new Date();
    //$("#dtDeliveryStart").datepicker({
    //    dateFormat: 'dd.mm.yy',
    //    changeYear: true
    //}).attr('readonly', 'readonly');

    
    //$("#dtDeliveryStart").datepicker("setDate", currentDate);

    
    /*  var addSO = document.getElementById('AddnewSO');
      addSO.style.visibility = true;
      var soInput = document.getElementById('SOInputFileds');
      soInput.style.visibility = true;*/
    /*var viewBtn = document.getElementById('newSOBTN');
    viewBtn.style.backgroundColor = 'blue';*/
    var currentDate = new Date();
    $('#dtDeliveryStart').datetimepicker({
        format: 'DD.MM.YYYY',
        defaultDate: currentDate
    });
};

function NewOrderDatePicker() {
    console.log('NewOrderDatePicker')
    var currentDate = new Date();
    $('#dtDeliveryStart').datetimepicker({
        format: 'DD.MM.YYYY',
        defaultDate: currentDate
    });
}

function onUploadSOClick() {
    $("#AddnewSO").hide();
    $("#SOInputFileds").hide();
    $("#SOtable").hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $("#viewSO").hide();
    $('#soVerifyBtn').hide();
    $('#UploadSO').show();
    $("#soShowDataTable").hide();
    $("#EditSO").hide();
    $("#CancelSO").hide();
   // $("#salesOrderDiv2").hide();
    $("#SalesOrderEditGrid").hide();
    $('#soVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#soChangeVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });
    AppViewModel.uploadSOFileName('')
    AppViewModel.selectedMapSOSubsi('');
    AppViewModel.selectedSOMaperValue('');
    AppViewModel.mappedSOSubsiData([]);
    AppViewModel.selectedSOMapper([]);
    AppViewModel.SOSubsiesData([]);
   
    uiBlocker();

    $.ajax({
        type: "GET",
        url: baseUrl + '/api/User/AllSubsies',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.mappedSOSubsiData([]);
                AppViewModel.mappedSOSubsiData(data);
                AppViewModel.SOSubsiesData(data);
            }

            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "No subsies are mapped",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

function onViewOrderClick() {
    debugger;
    $("#AddnewSO").hide();
    $("#SOInputFileds").hide();
    $("#SOtable").hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#UploadSO').hide();
    $("#viewSO").show();
    $("#EditSO").hide();
    $("#CancelSO").hide();
    $("#SalesOrderGrid").hide();
    $("#SalesOrderEditGrid").hide();
   // $("#salesOrderDiv2").hide();

   // document.getElementById("MassEditDiv").style.display = 'none';
   // document.getElementById("massEdit").checked = false;
 
    AppViewModel.selectedEntityValue('');
    getAllCustomersfortheUser();
    AppViewModel.selectedBIN([{ "name": "MB3", "value": "MB3" }, { "name": "MB4", "value": "MB4" }, { "name": "MB5", "value": "MB5" }, { "name": "MB7", "value": "MB7" }, { "name": "MB5H", "value": "MB5H" }, { "name": "MB5HE", "value": "MB5HE" }])

    var currentDate = new Date();
    //$("#dt1").datepicker({
    //    dateFormat: 'yy-mm-dd',
    //    maxDate: 0,
    //    changeYear: true,
    //    onSelect: function () {
    //        var dt2 = $('#dt2');
    //        var selDate = $(this).datepicker('getDate');
    //        dt2.datepicker('option', 'minDate', selDate);
    //      //  dt2.datepicker('option', 'maxDate', 0);
    //    }
    //}).attr('readonly', 'readonly');

    //$("#dt1").datepicker("setDate", currentDate);

    //$('#dt2').datepicker({
    //    dateFormat: 'yy-mm-dd',
    //   // maxDate: 0,
    //    minDate:0,
    //    changeYear: true
    //}).attr('readonly', 'readonly');
    //$("#dt2").datepicker("setDate", currentDate);
    $('#dt1').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });

    $('#dt2').datetimepicker({
        format: 'YYYY-MM-DD',
        defaultDate: currentDate
    });   


};


function onCreateOrderClick() {

    var allCells = $('#SOCreateTable tr td:nth-child(6) input');
    var cellValue = new Array();
    for (var v = 0; v < allCells.length; v++) {
        if (allCells[v].value == null || allCells[v].value == "") {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Customer PO Number cannot be empty",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        else
            cellValue[v] = allCells[v].value;
    }
    if (hasDuplicate(cellValue)) {
        var noData = noty({
            layout: 'top',
            type: 'error',
            text: "Found duplicate values for Customer PO Number. Please modify and then create the order.",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });
    }
    else {

        var cellData = [];
        cellData.push(["true"]);
        cellData.push([""]);
        cellData.push([""]);
        cellData.push([""]);
        var table = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
        for (var r = 0, n = table.rows.length; r < n; r++) {

            var rowData = new Array();

            for (var c = 0, m = table.rows[r].cells.length; c < m; c++) {

                if (c == 3) {
                    //  var value = document.getElementById("DDBinType" + (r - 1)).value;
                    // rowData[3] = (document.getElementById("DDBinType"+r).value);
                    rowData[3] = table.rows[r].cells[3].childNodes[0].value;
                }
                else if (c == 5) {

                    //var f = table.rows[r].cells[c].getElementsByTagName("TBPONum");
                    //rowData[5] = (document.getElementById("TBPONum"+r).value);
                    rowData[5] = table.rows[r].cells[5].childNodes[0].value;
                    cellData.push(rowData);
                }
                else if (c == 0) {
                    // alert(table.rows[r].cells[c].innerHTML);
                    rowData[0] = (table.rows[r].cells[c].innerHTML);
                }
                else if (c == 1) {
                    // alert(table.rows[r].cells[c].innerHTML);
                    var custCode = (table.rows[r].cells[c].innerHTML);
                    rowData[1] = "0000" + custCode;
                }
                else if (c == 2) {
                    // alert(table.rows[r].cells[c].innerHTML);
                    //rowData[2] = (table.rows[r].cells[c].innerHTML);
                    rowData[2] = table.rows[r].cells[2].childNodes[0].value;
                }
                else if (c == 4) {
                    // alert(table.rows[r].cells[c].innerHTML);
                    rowData[4] = (table.rows[r].cells[c].innerHTML);
                }

            }
        }        
        postSOData(cellData);

        // alert("SO Created Successfully.");
        $("#AddnewSO").show();
        $("#SOInputFileds").show();
        $("#SOtable").hide();
        $('#scMapping').hide();
        $('#viewMapping').hide();
        $('#viewMappingDetails').hide();



    }

};

function hasDuplicate(arr) {
    var x = {}, len = arr.length;
    for (var i = 0; i < len; i++) {
        if (x[arr[i]]) {
            return true;
        }
        x[arr[i]] = true;
    }
    return false;
};

function getAllCustomersfortheUser() {

    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/SO/AllCustomers',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.availableEntity([]);
                AppViewModel.availableEntity(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Customer List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
};


//Function to round Quantity in Reports
function roundInteger(cellValue) {
    rndValue = Math.round(cellValue);
    return rndValue;
};

//Function to change Date format in Report
function dateFormatter(cellValue) {
    var trmDate = cellValue.replace(/[ +\-]/g, '');
    var yy = trmDate.slice(0, 4);
    var mm = trmDate.slice(4, 6);
    var dd = trmDate.slice(6, 8);
    var vmm = chngToValidDte(mm);
    return dd + "-" + vmm + "-" + yy;

};


//Function to change Date format in Report
function REQdateFormatter(cellValue) {
    debugger;
    var trmDate = cellValue.replace(/[ +\-]/g, '');
    var yy = trmDate.slice(0, 4);
    var mm = trmDate.slice(4, 6);
    var dd = trmDate.slice(6, 8);
    //var vmm = chngToValidDte(mm);
    return dd + "." + mm + "." + yy;

};


//Function to change Date format in Report
function chngToValidDte(date) {
    if (date == "01") { return "Jan" }
    else if (date == "02") { return "Feb" }
    else if (date == "03") { return "Mar" }
    else if (date == "04") { return "Apr" }
    else if (date == "05") { return "May" }
    else if (date == "06") { return "Jun" }
    else if (date == "07") { return "Jul" }
    else if (date == "08") { return "Aug" }
    else if (date == "09") { return "Sep" }
    else if (date == "10") { return "Oct" }
    else if (date == "11") { return "Nov" }
    else if (date == "12") { return "Dec" }
    else { return date }
};


function postSOData(postArr) {
    debugger;
    uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/SO/createSO',
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(postArr),
        success: function (data) {
            $.unblockUI();
            if (data.Success == true) {
                $('#SalesOrderGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                var SORespMessage = "";
                    for (var i = 0 ; i < data.ResponseItems.length ; i++) {
                        if (data.ResponseItems[i].MessageType == "S") {
                            SORespMessage = SORespMessage + "<div style='color:green'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";
                        }
                        else {
                            SORespMessage = SORespMessage + "<div style='color:red'>" + data.ResponseItems[i].PONumber + " : " + data.ResponseItems[i].MessageContent + "</div>";

                        }
                }

                $("#createSOResponse").html(SORespMessage);
                $.unblockUI();
                $("#createSOResponse").dialog("open");


                //        var postSOAlt = noty({
                //            layout: 'top',
                //            type: alert,
                //            text: SORespMessage,
                //            buttons: [
                //{
                //    addClass: 'btn btn-primary', text: 'OK', type: 'success', onClick: function ($noty) {
                //        $noty.close();
                //    }
                //}

                //            ]
                //            //dismissQueue: true,
                //            //animation: {
                //            //    open: { height: 'toggle' },
                //            //    close: { height: 'toggle' },
                //            //    easing: 'swing',
                //            //    speed: 500
                //            //},
                //            //timeout: 10000
                //        });
            }
            else {

                var postSOErrAlt = noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });
            }

            $.unblockUI();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

this.onSOUploadClick = function () {

    debugger
   

    if ((AppViewModel.selectedMapSOSubsi() != '') && (AppViewModel.selectedSOMapper() != '') && (AppViewModel.uploadSOFileName() != '')) {

        var ext = AppViewModel.uploadSOFileName().split('.').pop();
        var isFileType = false;

        if ((ext.toLowerCase() == "xls") || (ext.toLowerCase() == "xlsx") || (ext.toLowerCase() == "csv") || (ext.toLowerCase() == "txt")) {
            var fileUploadData = new FormData($('#fileUploadSOform')[0]);
            $.ajax({

                type: 'POST',
                url: baseUrl + '/api/SC/UploadFile?transactionType=SO',  //server script to process data
                xhrFields: {
                    withCredentials: true
                },

                // Form data
                data: fileUploadData,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false,
                async: true,
                success: function (data, textStatus, xhr) {
                    var path = data;                  
                   // alert(encodeURIComponent(path));
                    uiBlocker();
                    $.ajax({
                        type: "GET",
                        url: baseUrl + '/api/SC/SOFileData?mapperName=' + AppViewModel.selectedSOMaperValue() + '&subsiId=' + AppViewModel.selectedMapSOSubsi()[0].SubsiId + '&path=' + encodeURIComponent(path),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $.unblockUI();
                            $('#soChangeVerifyBtn').css({ 'visibility': 'hidden', 'display': 'none' });
                            $('#soVerifyBtn').css({ 'visibility': 'visible', 'display': 'block' });
                            console.log(data.length);
                            if (data.length != 0) {
                                var isSuccess = true;
                                var message = null;
                                
                                for (var i = 0; i < data.length; i++) {
                                    if (data[i].Status == "false") {                                        
                                        isSuccess = false;
                                        message = data[i].Message;
                                        break;
                                    }

                                }
                                if (!isSuccess) {
                                    $("#soVerifyBtn").hide();
                                    $("#soShowDataTable").hide();
                                    noty({
                                        layout: 'top',
                                        type: 'error',
                                        text: "" + message,
                                        dismissQueue: true,
                                        animation: {
                                            open: { height: 'toggle' },
                                            close: { height: 'toggle' },
                                            easing: 'swing',
                                            speed: 500
                                        },
                                        timeout: 5000
                                    });
                                }
                                else {

                                    soShowData(data);
                                    $("#soShowDataTable").show();
                                }
                            }
                            else {
                                noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: "Unable to parse the file.Please check your data in file",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 5000
                                });

                            }

                        }, error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            $.unblockUI();
                        }
                    });
                },
                error: function (r) {

                }
            });
        }
        else {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file based on Mapped file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
    }
    else {
        if (AppViewModel.uploadSOFileName() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select a .csv, .txt, .xls or .xlsx file",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
        if (AppViewModel.selectedMapSOSubsi() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select subsi for SO file upload",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
        if (AppViewModel.selectedSOMapper() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select mapping file for SO file upload",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
       if ($("#dtCustPO").val() == '') {
            noty({
                layout: 'top',
                type: 'error',
                text: "Please select Customer PO Date",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

        }
    }

}
var sapFields = "";
function soShowData(data) {
    var isMissingSAPData = false;
    var htmlNew = " ";
    var tableRef = document.getElementById('soShowDataTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    var isSAPFieldData = false;
    var customerPacker = [];
    AppViewModel.scTableData(data);
    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }


    /* var elmtTable = document.getElementById('SOCreateTable').getElementsByTagName('tbody')[0];
     var tableRows = elmtTable.getElementsByTagName('tr');
     var rowCount = tableRows.length;

     for (var x = rowCount - 1; x >= 0; x--) {
         elmtTable.removeChild(tableRows[x]);
     }*/



    for (var k = 0; k < data.length; k++) {
        // Insert a row in the table at row index 0
        var arr = data[k].Name.split('|');
       // customerPacker.push(data[k].CustomerPackerRelation);
        if (k == 1)
            sapFields = arr;
        var decodeVal = null;
        if (k > 1) {
            if (data[k].DecodeValue != null && data[k].DecodeValue !="0") {
                decodeVal = data[k].DecodeValue.split('|');
            }
            else if(data[k].DecodeValue == "0") {
                isSAPFieldData = false;
            }
            else {
                isSAPFieldData = true;
            }
        }

        debugger
        $('#divTableLegend').show();
        $("#soVerifyBtn").show();
        $('#soChangeVerifyBtn').show();
        //$("#soVerifyBtn").text("Hello");
        var newRow = tableRef.insertRow(tableRef.rows.length);
        if (k == 0) {
            newRow.style.backgroundColor = "#599CDE";
        }
        else if (k == 1) {
           // newRow.style.backgroundColor = "rgb(205, 219, 219)";
            newRow.style.backgroundColor = "rgb(126, 191, 117)";
        }
        else {
            if (isSAPFieldData) {
                //newRow.style.backgroundColor = "rgb(245, 177, 177)";
                newRow.style.backgroundColor = "#c2ddb6";
            }
            else {
                newRow.style.backgroundColor = "#f1f1f1";
            }
        }
        for (var i = 0; i < arr.length; i++) {
            var headerCodecell = newRow.insertCell(i);
           // headerCodecell.style.border = '1px solid #ccc';
            headerCodecell.style.textAlign = "center";
            // Append a text node to the cell
            var newText = null;
            var newText1 = null;
            if (decodeVal != null) {
                if ((decodeVal[i] != '') && (decodeVal[i] != 'false')) {
                    //newText = document.createTextNode(arr[i] + " (" + decodeVal[i] + ") ");
                    newText = document.createElement("span");
                    newText.innerHTML = arr[i];
                    newText.style.color = "#599CDE";                   
                    newText1 = document.createElement("span");
                    newText1.innerHTML = "("+decodeVal[i]+")";
                    newText1.style.color = "rgb(126, 191, 117)";
                }
                else {
                    newText = document.createTextNode(arr[i]);
                    if (decodeVal[i] == 'false') {
                        isMissingSAPData = true;
                        headerCodecell.style.backgroundColor = "rgb(245, 177, 177)";
        //for decodevalue dialog Add
                        var x = document.createElement("a");                            
                        x.setAttribute("href", "#");
                        x.setAttribute("onclick", "addDecodeValue(" + i + ",this)");
                            x.appendChild(newText);
                            newText = x;
                    }
                }
            }
            else {
                newText = document.createTextNode(arr[i]);
            }
            if (newText1 == null)
                headerCodecell.appendChild(newText);
            else {
                var x = document.createElement("a");
                x.setAttribute("href", "#");
                x.setAttribute("onclick", "updateDecodeValueForData(" + i + ",this)");
                x.appendChild(newText);
                x.appendChild(newText1);
                headerCodecell.appendChild(x);
               // headerCodecell.appendChild(newText1);
            }
        }
        if (k == data.length - 1) {
            //alert(data[k].ErrorMessage.length);
            if (data[k].ErrorMessage.length != 0) {
                isMissingSAPData = true;
                htmlNew += '<p style="color:red">' + data[k].ErrorMessage + '</p>';
                $("#errorMessage").html('<p style="color:red">' + data[k].ErrorMessage + '</p>');
               // $("#errorMessage").dialog("open");
                $('#soVerifyBtn').hide();                
                $('#soChangeVerifyBtn').hide();
            }
        }
    }
//var hasCustomerPackerRelation = true;
//for (var i = 0; i < customerPacker.length; i++) {
//    if (customerPacker[i] != null && customerPacker[i] != "") {
//        hasCustomerPackerRelation = false;
//        break;
//    }
//}
    if (isMissingSAPData) {
        $('#soVerifyBtn').hide();
        $('#soChangeVerifyBtn').hide();
        isMissingSAPData = false;
        //noty({
        //    layout: 'top',
        //    type: 'error',
        //    text: "There are some values which don’t have look up values.Please add decode values to proceed",
        //    dismissQueue: true,
        //    animation: {
        //        open: { height: 'toggle' },
        //        close: { height: 'toggle' },
        //        easing: 'swing',
        //        speed: 500
        //    },
        //    timeout: 10000
        //});

        //var html = '<table><tr><td>There are some values which don’t have look up values.Please add decode values to proceed.</td><td></td></tr>' +
        //            '<tr><td colspan=2><label style ="font-weight:normal !important">Added code value are shown in the color</lable> <div style="background:#599CDE;dispaly:block;width:15px;height:15px;"></div></td></tr>' +
        //            '<tr><td colspan=2><label style ="font-weight:normal !important">Added decode value are shown in the color</label> <div style="background:#7EBF75;dispaly:block;width:15px;height:15px;"></div></td></tr>' +
        //            '<tr><td>They can be added/edited by clicking the corresponding values in the table</td><td></td></tr>' +
        //            '</table>';
        
        var html = "<div style='overflow:auto !important;'><article><h4>Goodpack Code Missing</h4>" +
                   "<p>Some file customer fields do not have Goodpack codes </p>" +
                   "<p>click on Hyperlink to add Goodpack codes <span style='color:red;'> (Highlighted in RED)</span> </p>" +
                   "<p>Edit already added customer code by clicking corresponding Hyperlink(s)  </p>" +
                   "<p><div style='width:15px;height:15px;background-color:#599CDE;float:left;'></div>" +
                    "<div style='width:15px;height:15px;float:left;margin-left:10px;white-space: nowrap;'>Customer Data</div >" +
                    "</p></br>" +
                    "<p><div style='width:15px;height:15px;background-color:#7EBF75;float:left;'></div>" +
                    "<div style='width:15px;height:15px;float:left;margin-left:10px;white-space: nowrap;'>GoodPack Data</div ></p>" +
                    "</br><article><p><div style='float:left;'><h4>Error in Data</h4>" + htmlNew + "</div></p></article></div>";

        $("<div id='divDialog' style='overflow:auto !important;'></div>").html(html).dialog({
            title: "<img src='"+baseUrl +"/images/info.png' width='15px'/><span style='font-size: 16px;'>Warning</span>",
            resizable: false,
            zIndex: 50002,
            modal: true, width: 700, height: 400,
            buttons: {
                "Ok": function () {
                    $(this).dialog("close");
                    $('#divDialog').remove();
                }
            }
        });
    }
    else {
        debugger
        $("#soVerifyBtn").show();
        $('#soChangeVerifyBtn').show();
        // $("#soVerifyBtn").text("Hello");
    }
    //if (!hasCustomerPackerRelation) {
    //    $("<div id='divDialog'></div>").html("Some of the customer/packer relation are invalid.").dialog({
    //        title: "<img src='images/Warning_notification.png' width='15px'/><span>Warning</span>",
    //        resizable: false,
    //        zIndex: 50000,
    //        modal: true,
    //        buttons: {
    //            "Ok": function () {
    //                $(this).dialog("close");
    //                $('#divDialog').remove();
    //            }
    //        }
    //    });
    //    // alert("Some of the customer/packer relation are invalid .")
    //}
}
function addDecodeValue(e,x)
{
    debugger;
    console.log(x);
    //console.log(sapFields);
    console.log(sapFields[e]);
    // AppViewModel.selectedMapSOSubsi(AppViewModel.selectedMapSubsi());  
   
    $('#addLookupPage').removeClass('hide');
    if ($('#collapseThree').hasClass('in')) {
        $('#txtSubsi').val(AppViewModel.selectedMapSOSubsi()[0].SubsiName);
        $('#txtMapper').val(AppViewModel.selectedSOMaperValue()[0]);
    }
    else {
        $('#txtSubsi').val(AppViewModel.SOSubsiesData()[0].SubsiName);
        $('#txtMapper').val('ChangeCancelSOMapper');
    }
    

    
    console.log(sapFields[e]);
    var lookUpVal = (sapFields[e] == "From Location") ? "Packer" : (sapFields[e] == "To Location") ? "Consignee" : (sapFields[e] == "Customer" || sapFields[e] == "Customer Number") ? "Customer Code" : sapFields[e];
    console.log(lookUpVal);
    uiBlocker();
    if (lookUpVal == "Consignee") {
        $('#ddCustomerPackerDeCodeVal_chosen').hide();
        $('#txtDeCodeVal').show();
    }
    else {
        $('#txtDeCodeVal').hide();
        $('#ddCustomerPackerDeCodeVal_chosen').show();
        GetCustomerPackerDetails(lookUpVal);
    }
    $('#txtLookUp').val(lookUpVal);    
    $('#txtCodeVal').val($(x).html());
    $('#txtDeCodeVal').val('');
    $('#btnSaveLookup').show();
    $('#btnUpdateLookup').hide();
    $('#divDialogHeader').html("Add Lookup");
    debugger;
     AppViewModel.selectedCustomerPackerDecodeValue = ko.observable('');
    //AppViewModel.selectedCustomerPackerDecodeValue('');
    //$('#ddCustomerPackerDeCodeVal').val('');
    //$('#ddCustomerPackerDeCodeVal').trigger("chosen:updated");
    $('#ddCustomerPackerDeCodeVal option:first-child').removeAttr('disabled')
    $("#ddCustomerPackerDeCodeVal").val("Select Customer/Packer")
    $("#ddCustomerPackerDeCodeVal").trigger("chosen:updated");
    
    $('#addLookupPage').dialog({        
        height: 550,
        width: 650,
        zIndex: 50000,        
        modal: true
    });
    setTimeout(function () { $('#txtLookUp').focus(); }, 200);
    $.unblockUI();
    tableIndex = e;   
}

function updateDecodeValueForData(e, x) {  
    //console.log(x);
    //console.log(sapFields);
    //console.log(sapFields[e]);
    // AppViewModel.selectedMapSOSubsi(AppViewModel.selectedMapSubsi());
    $('#addLookupPage').removeClass('hide');
    if ($('#collapseThree').hasClass('in')) {
        $('#txtSubsi').val(AppViewModel.selectedMapSOSubsi()[0].SubsiName);
        $('#txtMapper').val(AppViewModel.selectedSOMaperValue()[0]);
    }
    else {
        $('#txtSubsi').val(AppViewModel.SOSubsiesData()[0].SubsiName);
        $('#txtMapper').val('ChangeCancelSOMapper');
    }    
    var lookUpVal = (sapFields[e] == "From Location") ? "Packer" : (sapFields[e] == "To Location") ? "Consignee" : (sapFields[e] == "Customer" || sapFields[e] == "Customer Number") ? "Customer Code" : sapFields[e];
    uiBlocker();
    if (lookUpVal == "Consignee") {
        $('#ddCustomerPackerDeCodeVal_chosen').hide();
        $('#txtDeCodeVal').show();
    }
    else {
        $('#txtDeCodeVal').hide();
        $('#ddCustomerPackerDeCodeVal_chosen').show();
        GetCustomerPackerDetails(lookUpVal);
    }
    $('#txtLookUp').val(lookUpVal);
    var data = $(x).html();
    var codeValue = null;
    var decodeValue = null;   
    $.each($(data), function (key, value) {
        if (key == 0) {
            codeValue = $(value).html();
        }
        else if(key==1)
        {
            var rowData = new Array();
            decodeValue = $(value).html();
            rowData.push({
                EntityCode: $(value).html().replace('(', '').replace(')', ''),
                EntityName: codeValue
            });            
            debugger;
            if (lookUpVal == "Consignee") {
                $('#txtDeCodeVal').val(decodeValue.replace('(', '').replace(')', ''));
            } else {
                AppViewModel.selectedCustomerPackerDecodeValue = ko.observableArray(rowData);
                //$('#ddCustomerPackerDeCodeVal').val(codeValue + decodeValue);
                $('#ddCustomerPackerDeCodeVal option:contains(' + decodeValue.replace('(', '').replace(')', '') + ')').attr('selected', true);
                $('#ddCustomerPackerDeCodeVal').trigger("chosen:updated");
            }
        }
    });

    $('#txtCodeVal').val(codeValue);
  //  $('#txtDeCodeVal').val('');
    $('#btnSaveLookup').hide();
    $('#btnUpdateLookup').show();
    $('#divDialogHeader').html("Update Lookup");
    $('#addLookupPage').dialog({
        height: 550,
        width: 650,
        zIndex: 50000,        
        modal: true
    });
    setTimeout(function () { $('#txtLookUp').focus(); }, 200);
    $.unblockUI();
    tableIndex = e;
}
function GetCustomerPackerDetails(lookUpVal) {
    $.ajax({
        type: "GET",
        async:false,
        url: baseUrl + '/api/SO/GetCustomerPackerDetails?lookupName=' + lookUpVal,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                AppViewModel.CustomerPackers([]);
                AppViewModel.CustomerPackers(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Customer List",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}
this.SOverifyClick = function () {
    var r = confirm("Please verify lookup decode value before Submiting. Do you want to continue?");
    if (r != true)
        return false;
    //uiBlocker();   
    noty({
        layout: 'top',
        type: 'information',
        text: "SO data verification takes some time.Please wait...",
        dismissQueue: true,
        animation: {
            open: { height: 'toggle' },
            close: { height: 'toggle' },
            easing: 'swing',
            speed: 500
        },
        timeout: 10000
    });

    uiBlocker();
    $('#soVerifyBtn').prop('disabled', true);
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/SC/VerifySOData?mapperFile=' + AppViewModel.selectedSOMaperValue() + "&subsiId=" + AppViewModel.selectedMapSOSubsi()[0].SubsiId + "&CustPODate=" + $("#dtCustPO").val(),
        data: ko.toJSON(AppViewModel.scTableData()),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$noty.close();
            $('#soVerifyBtn').prop('disabled', false);
            $.unblockUI();
            if (data.length != 0) {
                if (data[0].Status == "false") {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "" + data[0].Message,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                else {

                    debugger
                    $('#scMapping').hide();
                    $('#SO').hide();
                    $('#soResult').show();
                    soResultData(data);
                }
            }
            else {
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to verify data.Please check your uploaded file",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#soVerifyBtn').prop('disabled', false);
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

}
var tableIndex = null;
function soResultData(data) {
    debugger
    var tableRef = document.getElementById('soResultTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;
    var successCount = 0;
    AppViewModel.scResultTableData(data);

    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }
    for (var k = 0; k < data.length; k++) {
        // Insert a row in the table at row index 0
        var arr = data[k].Name.split('|');
        //$("#scVerifyBtn").show();
        var newRow = tableRef.insertRow(tableRef.rows.length);
        if (k == 0) {
            newRow.style.backgroundColor = "#599CDE";
        }
        else if (k == 1) {
            newRow.style.backgroundColor = "#7EBF75";
        }
        else {
            newRow.style.backgroundColor = "white";
        }
        var headerCodecell
        var cellCount = 0;
        for (var i = 0; i < arr.length + 2; i++) {
            if (i != arr.length) {
                headerCodecell = newRow.insertCell(cellCount);
                headerCodecell.style.border = '1px solid #ccc';
                headerCodecell.style.textAlign = "center";

                // Append a text node to the cell
                if (i < arr.length) {
                    var newText = document.createTextNode(arr[cellCount]);
                    headerCodecell.appendChild(newText);
                }
                if ((k == 0) && (i > arr.length)) {
                    var newText2 = document.createTextNode("Error Details");
                    headerCodecell.appendChild(newText2);

                    headerCodecell = newRow.insertCell(cellCount);
                    headerCodecell.style.border = '1px solid #ccc';
                    headerCodecell.style.textAlign = "center";
                    // Append a text node to the cell
                    
                    // Append a text node to the cell
                    var newText1 = document.createTextNode("Status");
                    headerCodecell.appendChild(newText1);
                }
                else if ((k == 1) && (i > arr.length)) {
                    var newText2 = document.createTextNode("");
                    headerCodecell.appendChild(newText2);
                    headerCodecell = newRow.insertCell(cellCount);
                    headerCodecell.style.border = '1px solid #ccc';
                    headerCodecell.style.textAlign = "center";
                    // Append a text node to the cell
                }
                else if (i > arr.length) {

                    // Append a text node to the cell
                    if (data[k].Status == null) {
                        var newText2 = document.createTextNode("");
                        headerCodecell.appendChild(newText2);
                    }

                    //else if (data[k].Status == "ERROR")
                    //{

                        var btn = document.createElement("BUTTON");
                        var t = document.createTextNode("Details");
                        btn.style.backgroundColor = "#004c97";
                        btn.style.borderRadius = "5px";
                        btn.style.color= "white";
                        btn.id = k;
                        btn.onclick = (function (opt) {
                            return function () {
                                $.ajax({
                                    type: "POST",
                                    url: baseUrl + '/api/SC/SOErrorDetails?LineId=' + opt,
                                    data: ko.toJSON(AppViewModel.scTableData()),
                                    contentType: 'application/json; charset=utf-8',
                                    success: function (data) {
                                        $.unblockUI();
                                        if (data.length != 0) {
                                            var messageVal = '';
                                            for (var i = 0; i < data.length; i++) {
                                                messageVal = messageVal + '<p style="color:red">' + "" + data[i] + '</p>';

                                            }
                                            noty({
                                                layout: 'top',
                                                type: 'warning',
                                                text: "" + messageVal,
                                                buttons: [
                                                        {
                                                            addClass: 'btn btn-primary', text: 'Close', type: 'success', onClick: function ($noty) {
                                                                $noty.close();
                                                            }
                                                        }

                                                ]
                                            });
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        console.log(textStatus, errorThrown);
                                        $.unblockUI();
                                    }
                                });
                            };
                        })(data[k].LineId);
                        btn.appendChild(t);
                        headerCodecell.appendChild(btn);
                        headerCodecell = newRow.insertCell(cellCount);
                        headerCodecell.style.border = '1px solid #ccc';
                        headerCodecell.style.textAlign = "center";                  

                    var newText3 = document.createTextNode(data[k].Status);
                    headerCodecell.appendChild(newText3);

                }
                cellCount++;
            }
        }
        cellCount = 0;
    }
}
var transactionType = null;
this.onAddlookupDialogSaveClick = function () {
    var cellData = [];
    var subsiIdVal = $('#SO').css('display') == 'block' ? (AppViewModel.selectedMapSOSubsi()[0] != undefined ? AppViewModel.selectedMapSOSubsi()[0].SubsiId : AppViewModel.SOSubsiesData()[0].SubsiId) : $('#scMapping').css('display') == 'block' ? AppViewModel.selectedMapSubsi()[0].SubsiId : AppViewModel.selectedMapSubsi()[0].SubsiId;
    transactionType = $('#SO').css('display') == 'block' ? "SO" : $('#scMapping').css('display') == 'block' ? "SC" : "SSC";
    console.log(subsiIdVal);
    var subsyId = subsiIdVal;
    var MapperName = $('#txtMapper').val();
    var LookupName = $('#txtLookUp').val();
    if (LookupName == "Packer" || LookupName == "Customer Code")
        $('#txtDeCodeVal').val(AppViewModel.selectedCustomerPackerDecodeValue()[0].EntityCode);
    var isValidated = true;
    console.log($('#txtDeCodeVal').val());
    if (LookupName == "Packer") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0,1)==5) ){//!/^5.....$/.test($('#txtDeCodeVal').val())) {          
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Packer format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });           
        }
    }
    else if (LookupName == "Customer Code") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0,1)==3) ){//!/^3.....$/.test($('#txtDeCodeVal').val())) {         
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Customer format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });            
        }
    }
    else if (LookupName == "Consignee") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0,1)==6) ){//!/^6.....$/.test($('#txtDeCodeVal').val())) {            
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Consignee format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });            
        }
    }
    if (!isValidated)
        return;
    cellData[0] = new Array(subsyId, MapperName, LookupName);
    var rowData = new Array();
    rowData[0] = $('#txtCodeVal').val();
    rowData[1] = (LookupName == "Consignee") ? $('#txtDeCodeVal').val() : AppViewModel.selectedCustomerPackerDecodeValue()[0].EntityCode;
    cellData.push(rowData);
    postLookupDataDialog(cellData);
}

this.onEditlookupDialogSaveClick = function () {
    var r = confirm("Are you sure you want to edit the Lookup Value ?");
    if (r != true)       
        return false;
    var cellData = [];
    var subsiIdVal = $('#SO').css('display') == 'block' ? (AppViewModel.selectedMapSOSubsi()[0] != undefined ? AppViewModel.selectedMapSOSubsi()[0].SubsiId : AppViewModel.SOSubsiesData()[0].SubsiId) : $('#scMapping').css('display') == 'block' ? AppViewModel.selectedMapSubsi()[0].SubsiId : AppViewModel.selectedMapSubsi()[0].SubsiId;
    transactionType = $('#SO').css('display') == 'block' ? "SO" : $('#scMapping').css('display') == 'block' ? "SC" : "SSC";
    console.log(subsiIdVal);
    var subsyId = subsiIdVal;
    var MapperName = $('#txtMapper').val();
    var LookupName = $('#txtLookUp').val();
    if (LookupName == "Packer" || LookupName == "Customer Code")
        $('#txtDeCodeVal').val(AppViewModel.selectedCustomerPackerDecodeValue()[0].EntityCode);
    var isValidated = true;
    console.log($('#txtDeCodeVal').val());
    if (LookupName == "Packer") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0, 1) == 5)) {//!/^5.....$/.test($('#txtDeCodeVal').val())) {          
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Packer format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
    }
    else if (LookupName == "Customer Code") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0, 1) == 3)) {//!/^3.....$/.test($('#txtDeCodeVal').val())) {         
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Customer format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
    }
    else if (LookupName == "Consignee") {
        if ($('#txtDeCodeVal').val().length != 6 || !($('#txtDeCodeVal').val().substring(0, 1) == 6)) {//!/^6.....$/.test($('#txtDeCodeVal').val())) {            
            isValidated = false;
            noty({
                layout: 'top',
                type: 'error',
                text: "Invalid Consignee format",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
    }
    if (!isValidated)
        return;
    cellData[0] = new Array(subsyId, MapperName, LookupName);
    
    //rowData[0] = $('#txtCodeVal').val();
    //rowData[1] = (LookupName == "Consignee") ? $('#txtDeCodeVal').val() : AppViewModel.selectedCustomerPackerDecodeValue()[0].EntityCode;
    //rowData[2] = $('#txtLookUp').val();
    debugger;  
    var LookUpModelUpdate = {
        MapperName: MapperName,
        LookupName: LookupName,
        LookupCode: $('#txtCodeVal').val(),
        LookupDecode: (LookupName == "Consignee") ? $('#txtDeCodeVal').val() : AppViewModel.selectedCustomerPackerDecodeValue()[0].EntityCode
    };
    //cellData.push(rowData);
    //console.log(cellData);
    postLookupDataDialogUpdate(LookUpModelUpdate);
}
this.onCancellookupDialogSaveClick = function () {

};
function postLookupDataDialogUpdate(postArr) {
    uiBlocker();
    $.ajax({
        type: "PUT",
        url: baseUrl + '/api/Lookup/EditLookupNew',
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(postArr),
        success: function (data) {
            $.unblockUI();
            if (data.Success == true) {
                var LookupRespMessage = "<div style='color:green'>" + data.Message + "</div>";
                var postLookupAlt = noty({
                    layout: 'top',
                    type: alert,
                    text: LookupRespMessage,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
                $('#addLookupPage').dialog('close');
                updateTable($('#txtCodeVal').val(), $('#txtDeCodeVal').val());
                $('#ddCustomerPackerDeCodeVal option:first-child').attr('disabled', '');
            }
            else {
                var postSOErrAlt = noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });
            }
            $.unblockUI();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

function postLookupDataDialog(postArr) {
    uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/Lookup/AddLookupDialog',
        contentType: 'application/json; charset=utf-8',
        data: ko.toJSON(postArr),
        success: function (data) {
            $.unblockUI();
            if (data.Success == true) {
                var LookupRespMessage = "<div style='color:green'>" + data.Message + "</div>";
                var postLookupAlt = noty({
                    layout: 'top',
                    type: alert,
                    text: LookupRespMessage,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
                $('#addLookupPage').dialog('close');
                updateTable($('#txtCodeVal').val(), $('#txtDeCodeVal').val());
                $('#ddCustomerPackerDeCodeVal option:first-child').attr('disabled', '');
            }
            else {
                var postSOErrAlt = noty({
                    layout: 'top',
                    type: 'error',
                    text: data.Message,
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 10000
                });
            }
            $.unblockUI();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
}

function updateTable(codeValue, decodeValue) {
    var index = 0;
    if (transactionType == 'SO') {
        debugger;
        $('#soShowDataTable tr ').each(function () {
            debugger;
            var this_row = $(this);
            var row = this_row.find('td:eq(' + tableIndex + ')');
            //console.log(($(row).text() == codeValue));
            // if ($(row).text() == codeValue && index > 1) {
            if ($(row).text().indexOf(codeValue) > -1 && index > 1) {
                 $(row).css('background-color', '');
                //$(row).find('a').text($(row).find('a').text() + '(' + decodeValue + ')');
                //$(row).text($(row).find('a').text() + '(' + decodeValue + ')')                 
                 console.log(decodeValue);
                 $(row).html('<a onclick="updateDecodeValueForData(' + tableIndex + ',this)" href="#"><span style="color: rgb(89, 156, 222);">' + codeValue + '</span><span style="color: rgb(126, 191, 117);">(' + decodeValue + ')</span></a>')
                updateDecodeValue(index, decodeValue);
                
            }
            index++;
        });
    }
    else if(transactionType=="SC") {
        $('#scTable tr ').each(function () {
            var this_row = $(this);
            var row = this_row.find('td:eq(' + tableIndex + ')');
            //console.log(($(row).text() == codeValue));
            //if ($(row).text() == codeValue && index > 1) {
            if ($(row).text().indexOf(codeValue) > -1 && index > 1) {
                $(row).css('background-color', '');                
                // $(row).text($(row).find('a').text() + '(' + decodeValue + ')')
                $(row).html('<a onclick="updateDecodeValueForData(' + tableIndex + ',this)" href="#"><span style="color: rgb(89, 156, 222);">' + codeValue + '</span><span style="color: rgb(126, 191, 117);">(' + decodeValue + ')</span></a>')
                updateDecodeValue(index, decodeValue);
            }
            index++;
        });
    } else if (transactionType == "SSC") {        
        $('#sscTable tr ').each(function () {
            var this_row = $(this);
            var row = this_row.find('td:eq(' + tableIndex + ')');
            //console.log(($(row).text() == codeValue));
            if ($(row).text().indexOf(codeValue) > -1 && index > 1) {
                $(row).css('background-color', '');                
                // $(row).text($(row).find('a').text() + '(' + decodeValue + ')')
                $(row).html('<a onclick="updateDecodeValueForData(' + tableIndex + ',this)" href="#"><span style="color: rgb(89, 156, 222);">' + codeValue + '</span><span style="color: rgb(126, 191, 117);">(' + decodeValue + ')</span></a>')
                updateDecodeValue(index, decodeValue);
            }
            index++;
        });
    }
    checkIfDecodeValidated();
}
function checkIfDecodeValidated() {
    var returnValue = false;
    if (transactionType == 'SO') {
        $('#soShowDataTable tr ').each(function () {
            var this_row = $(this);
            this_row.find('td').each(function () {
                if ($(this).css('background-color') == 'rgb(245, 177, 177)') {
                    returnValue = true;//console.log( $(this).css('background-color'));                
                }
            });
        });
    }
    else if (transactionType == 'SC') {
        $('#scTable tr ').each(function () {
            var this_row = $(this);
            this_row.find('td').each(function () {
                if ($(this).css('background-color') == 'rgb(245, 177, 177)') {
                    returnValue = true;//console.log( $(this).css('background-color'));                
                }
            });
        });
    }
    else if (transactionType == "SSC") {
        $('#sscTable tr ').each(function () {
            var this_row = $(this);
            this_row.find('td').each(function () {
                if ($(this).css('background-color') == 'rgb(245, 177, 177)') {
                    returnValue = true;//console.log( $(this).css('background-color'));                
                }
            });
        });
    }
    if (!returnValue) {
        if (transactionType == 'SO') {
            $('#soVerifyBtn').show();
            $('#soChangeVerifyBtn').show();
        }
        else if (transactionType == 'SC')
            $('#scVerifyBtn').show();
        else if (transactionType == 'SSC')
            $('#sscVerifyBtn').show();
    }
}
function updateDecodeValue(index,decodeValue) {
    var dataValue = AppViewModel.scTableData();   
    $.each(AppViewModel.scTableData(), function (key, value) {
        if (key == index) {     // console.log(value.DecodeValue + ":" + key);
            var data = value.DecodeValue.split('|');
            data[tableIndex] = decodeValue;
            data.push(key-1);
            value.DecodeValue = data.join('|');                    
        }
        console.log(AppViewModel.scTableData());
    });
}