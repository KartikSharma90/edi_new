﻿//$(function () {
//    $("#selectable").selectable();
//    $("#previlegeList").selectable();
//});


this.hqUserClick = function () {
    $('#hqUser').css({ 'background-color': '#004c97' });
    $('#subsiUser').css({ 'background-color': '#bababa' });
    $('#hqAdminUser').css({ 'background-color': '#bababa' });
    uiBlocker();
    AppViewModel.selectedRole(0)
    previlegeCaller();
   
}

function previlegeCaller() {
    uiBlocker();
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/User/AllPrevileges?roleId=' + AppViewModel.selectedRole(),
        data: ko.toJSON(AppViewModel),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.privileges([]);
            AppViewModel.privileges(data)

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
    $.ajax({
        type: "POST",
        url: baseUrl + '/api/User/MappedPrevilege?roleId=' + AppViewModel.selectedRole(),
        data: ko.toJSON(AppViewModel),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.mappedPrevileges([]);
            AppViewModel.mappedPrevileges(data)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
}
this.subsiUserClick = function () {
    $('#hqUser').css({ 'background-color': '#bababa' });
    $('#subsiUser').css({ 'background-color': '#004c97' });
    $('#hqAdminUser').css({ 'background-color': '#bababa' });
    AppViewModel.selectedRole(1)
    uiBlocker();
    previlegeCaller();

}
this.hqAdminClick = function () {
    $('#hqUser').css({ 'background-color': '#bababa' });
    $('#subsiUser').css({ 'background-color': '#bababa' });
    $('#hqAdminUser').css({ 'background-color': '#004c97' });
    AppViewModel.selectedRole(2)
    uiBlocker();
    previlegeCaller();

}

this.rightArrow = function () {
    if (AppViewModel.selectedPrivilege().length > 0) {
        AppViewModel.mappedPrevileges.push({ PrivilegeId: AppViewModel.selectedPrivilege()[0].PrivilegeId, PrivilegeName: AppViewModel.selectedPrivilege()[0].PrivilegeName });
        AppViewModel.privileges.remove(AppViewModel.selectedPrivilege()[0]);
        AppViewModel.selectedPrivilege([]);
    }
   
};



this.leftArrow = function () {
    if (AppViewModel.mappedPrivilege().length > 0) {
        AppViewModel.privileges.push({ PrivilegeId: AppViewModel.mappedPrivilege()[0].PrivilegeId, PrivilegeName: AppViewModel.mappedPrivilege()[0].PrivilegeName });
        AppViewModel.mappedPrevileges.remove(AppViewModel.mappedPrivilege()[0]);
        AppViewModel.mappedPrivilege([]);
    }
};

this.roleSaveClick = function () {
    if (AppViewModel.mappedPrevileges().length > 0) {
        uiBlocker();
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/User/SavePrivilege?roleId=' + AppViewModel.selectedRole(),
            data: ko.toJSON(AppViewModel.mappedPrevileges()),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.Success) {
                    noty({
                        layout: 'top',
                        type: 'success',
                        text: "Privilege mapped successfully",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                else {
                    var loginAlert = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.Message,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });
    }
}