﻿

$(document).ready(function () {   
    $("#homeimg").attr("src", baseUrl + "/Images/Home Selected.png");
    $('.menu-toggle').click(function () {
        $('body').toggleClass('menu-mm');
       
        //$('body').toggle(function () {
        //    onArrow2Click();
        //}, function () {
        //    onArrow1Click();
        //});
    });

    $('#changePasswordDiv').on('click', function () {
        $('#changePasswordNew').val('');
        $('#changePasswordConfirm').val('');
        $("#changePasswordDialog").dialog({
            title: "Reset Password",
            height: 200,
            width: 250,
            modal: true,
            zIndex:50000,
            buttons: {
                "Save": function () {
                    if ($('#changePasswordNew').val() == $('#changePasswordConfirm').val()) {
                        $(this).dialog("close");
                        uiBlocker();
                        var changePasswordModel = {
                            UserName: AppViewModel.Username(),
                            Newpassword: $('#changePasswordNew').val()
                        };
                        $.ajax({
                            type: "POST",
                            url: baseUrl + '/api/User/ChangePassowrd',
                            data: ko.toJSON(changePasswordModel),
                            contentType: 'application/json; charset=utf-8',
                            success: function (data) {
                                $.unblockUI();
                                if (data) {
                                    noty({
                                        layout: 'top',
                                        type: 'success',
                                        text: "Passwords changed successfully",
                                        dismissQueue: true,
                                        animation: {
                                            open: { height: 'toggle' },
                                            close: { height: 'toggle' },
                                            easing: 'swing',
                                            speed: 500
                                        },
                                        timeout: 3000
                                    });
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $.unblockUI();
                                noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: "Error updating password",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 3000
                                });
                            }
                        });
                    }
                    else {
                        //$(this).dialog("close");
                        noty({
                            layout: 'top',
                            type: 'error',
                            text: "Passwords donot match",
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 3000
                        });
                    }

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            },
            close: function () {
                //  allFields.val("").removeClass("ui-state-error");
                $(this).dialog("close");
            }
        });
    });
    console.log(AppViewModel);
    ko.applyBindings(AppViewModel);
    $("#uploadTrigger").click(function () {
        $("#uploadFile").click();
    });

    $('#menu-v2 > ul > li > a').click(function () {
        debugger;
        $('#menu-v2 li').removeClass('active');
        $(this).closest('li').addClass('active');
        var checkElement = $(this).next();
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            checkElement.slideUp('normal');
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#menu-v2 ul ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }
        if ($(this).closest('li').find('ul').children().length == 0) {
            $('#menu-v2 ul ul:visible').slideUp('normal');
            return true;
        } else {
           
            return false;
        }
    }); 
    $("#homeDiv").click(function () {
        $("#homeimg").attr("src", baseUrl + "/Images/Home Selected.png");
        $("#homeimg").attr("height", "30px");
        $("#homeimg").attr("width", "45px");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");
        $('#Transaction').removeClass('in');
        $('#Master').removeClass('in');
        $('#Settings').removeClass('in');
        $('#Administrator').removeClass('in');
        $('#Templates').removeClass('in');
    });
    $("#trnDiv").click(function () {
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction selected.png");
        $("#trnimg").attr("height", "30px");
        $("#trnimg").attr("width", "45px");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");


    });
    $("#subsiDiv").click(function () {
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi Selected.png");
        $("#subsimage").attr("height", "30px");
        $("#subsimage").attr("width", "45px");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");
        $('#Transaction').removeClass('in');
        $('#Master').removeClass('in');
        $('#Settings').removeClass('in');
        $('#Administrator').removeClass('in');
        $('#Templates').removeClass('in');
    });
    $("#mapDiv").click(function () {
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping selected.png");
        $("#mapimage").attr("height", "30px");
        $("#mapimage").attr("width", "45px");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");


    });
    $("#dlookDiv").click(function () {
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup selected.png");
        $("#lookupimg").attr("height", "30px");
        $("#lookupimg").attr("width", "45px");
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");


    });
    $("#adminDiv").click(function () {
        $("#adminimg").attr("src", baseUrl + "/Images/Admin selected.png");
        $("#adminimg").attr("height", "30px");
        $("#adminimg").attr("width", "45px");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");


    });
    $("#downloadDiv").click(function () {
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#adminimg").attr("height", "30px");
        $("#adminimg").attr("width", "45px");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download selected.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");

    });
    $("#errormailDiv").click(function () {
        $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
        $("#adminimg").attr("height", "30px");
        $("#adminimg").attr("width", "45px");
        $("#homeimg").attr("src", baseUrl + "/Images/Home.png");
        $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
        $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
        $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
        $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
        $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
        $("#errorimage").attr("src", baseUrl + "/Images/error mail selected.png");
        $('#Transaction').removeClass('in');
        $('#Master').removeClass('in');
        $('#Settings').removeClass('in');
        $('#Administrator').removeClass('in');
        $('#Templates').removeClass('in');
    });
  //  $.noConflict();   
    $('#login').show();
    $('#home').hide();
    $('#userList').hide();
    $('#subsiMapping').hide();    
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#roleList').hide();
    $('#sapMapping').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#viewMapping').hide();
    $('#scMapping').hide();
    $('#viewMappingDetails').hide();
    $('#assignValueForm').hide();
    $("#mapFileSaveForm").hide();
    $("#soEditReason").hide();
    $("#soEditReason").hide();
    $("#lineError").hide();
    $("#errorMessage").hide();
    $("#lineDeleteForm").hide();
    $("#sscMapping").hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#somassEditReason').hide();
    $('#somassCancelReason').hide();
    AppViewModel.transactionType(-1);
    $("#asnTable").hide();
    $('#asnMapping').hide();
    $('#errorEmail').hide();
    $('#kendoUISSC').hide();
    //$('#scannerRegst').hide();
    $('#AddConfig').hide();
    $('#reportDetails').hide();
    //$('#EditConfig').hide();
    ///////////////////////////////////////////////////////
  
   

////////////////////////////////////////////////
    $("#assignValueForm").dialog({
        autoOpen: false,
        height: 300,
        zIndex: 50000,
        width: 250,
        modal: true,
        buttons: {
            "OK": function () {

                if (AppViewModel.selectedField().length > 0) {
                    AppViewModel.mappedHeaders.push({ SRNO: "1", HeaderName: AppViewModel.assignValue(), Id: AppViewModel.selectedField()[0].Id, FieldName: AppViewModel.selectedField()[0].FieldName, MappedField: AppViewModel.selectedField()[0].FieldName + " ---> " + AppViewModel.assignValue() });
                    AppViewModel.sapFields.remove(AppViewModel.selectedField()[0]);
                    AppViewModel.selectedHeader([]);
                    $(this).dialog("close");
                }

            },
            Cancel: function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });
    $("#mapFileSaveForm").dialog({
        autoOpen: false, zIndex: 50000,
        height: 250,
        width: 350,
        modal: true,
        zIndex:6000,
        buttons: {
            "OK": function () {

                if (AppViewModel.mapFileName() == '') {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Filename cannot be empty",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                else {
                    SaveMapping();
                    AppViewModel.mapFileName('');
                }

            },
            Cancel: function () {
                AppViewModel.mapFileName('');
                $(this).dialog("close");

            }
        },
        close: function () {
            AppViewModel.mapFileName('');
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });

    $("#lineError").dialog({
        title:"Status of records",
        autoOpen: false,
        height: 450,
        zIndex: 50000,
        width: 650,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
                //$('#TransactionGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                if ($('#grid').data().kendoGrid != undefined) {
                    $("#grid").data('kendoGrid').dataSource.read();
                }
                $('#welcomeScreen').show();
                $('#LineContent').hide();
            }
            
        },
        close: function () {
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });


    $("#errorMessage").dialog({
        title: "Summary of error's in uploaded File",
        autoOpen: false,
        height: 450,
        zIndex: 50000,
        width: 700,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("close");            

            }

        },
        close: function () {
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });
    $("#createSOResponse").dialog({
        title: "SO Response",
        autoOpen: false,
        height: 450,
        width: 650,
        zIndex: 50000,
        modal: true,
        buttons: {
            "OK": function () {
                $(this).dialog("close");
            }
        },
        close: function () {
            $(this).dialog("close");
        }
    });

    $("#lineDeleteForm").dialog({
        title: "Are you sure you want to delete all the error records of this transaction?",
        autoOpen: false,
        height: 150, zIndex: 50000,
        width: 450,
        modal: true,
        buttons: {
            "Yes": function () {
                $(this).dialog("close");
                AppViewModel.lineDeleteComment('');
                $("#lineCommentForm").dialog("open");
              
            },
                Cancel: function () {
                    $(this).dialog("close");
                }

        },
        close: function () {
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });

    $("#lineCommentForm").dialog({
        title: "Please enter your comment before deletion",
        autoOpen: false,
        height: 150, zIndex: 50000,
        width: 350,
        modal: true,
        buttons: {
            "Confirm Delete": function () {
                if (AppViewModel.lineDeleteComment() != '') {
                    $(this).dialog("close");
                    //uiBlocker();
                    if ($('#kendocheckAll').is(':checked')) {
                        var uniqueIds = null;
                        var entityGrid = $("#gridLineContent").data("kendoGrid");
                        var data = entityGrid.dataSource.data();
                        var totalNumber = data.length;
                        for (var i = 0; i < totalNumber; i++) {
                            var currentDataItem = data[i];
                            uniqueIds == null ? uniqueIds = currentDataItem.UniqueId : uniqueIds=(uniqueIds + ',' + currentDataItem.UniqueId);
                        }
                    } else {
                        uniqueIds = selectedIdsGrid.join(',');
                    }                
                    var deleteModel = {
                        TransactionId: AppViewModel.transactionIdVal(),
                        DeleteComment: AppViewModel.lineDeleteComment(),
                        DeleteIds: uniqueIds,
                    }                
                    $.ajax({
                        type: "DELETE",
                        url: baseUrl + '/api/SC/DeleteRecords?transactionType=' + AppViewModel.selectedTransactionValue()[0].EntityName,
                        data:ko.toJSON(deleteModel),
                        contentType: 'application/json; charset=utf-8',
                        success: function (data) {
                            $.unblockUI();
                            if (data) {                               
                                noty({
                                    layout: 'top',
                                    type: 'success',
                                    text: "Successfully deleted the error records",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 5000
                                });
                                $.unblockUI();
                                $('#TransactionGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                                $('#welcomeScreen').show();
                                $('#LineContent').hide();
                            }
                            else {
                                $.unblockUI();
                                noty({
                                    layout: 'top',
                                    type: 'error',
                                    text: "Unable to delete error records.Please try again",
                                    dismissQueue: true,
                                    animation: {
                                        open: { height: 'toggle' },
                                        close: { height: 'toggle' },
                                        easing: 'swing',
                                        speed: 500
                                    },
                                    timeout: 5000
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus, errorThrown);
                            $.unblockUI();
                        }
                    });
                }
                else {
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Comments cannot be empty",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });

                }

            },
            Cancel: function () {
                $(this).dialog("close");
            }

        },
        close: function () {
            //  allFields.val("").removeClass("ui-state-error");
            $(this).dialog("close");
        }
    });

    //var tooltips = $("[title]").tooltip();
    //ko.bindingHandlers.tooltip.defaultTooltipOptions = { hide: { event: 'unfocus' }, style: { classes: 'ui-tooltip-red' } }
});

var AppViewModel = {
    Username: ko.observable(''),
    Password: ko.observable(''),
    availableSubsy: ko.observableArray([]),
    mappedSubsiData: ko.observableArray([]),
    selectedSubsiValue: ko.observable(''),
    availableLookupSubsy: ko.observable(''),
    selectedLookupSubsyValue: ko.observable(''),
    availableEntity: ko.observableArray([]),
    selectedEntityValue: ko.observable(''),
    entityforSubsy: ko.observableArray([]),
    selectedEntityforSubsyValue: ko.observable(''),
    mapperforSubsy: ko.observableArray([]),
    selectedMapperValue: ko.observable(''),
    lookupNames: ko.observableArray([]),
    selectedLookupValue: ko.observable(''),
    availableTransactions: ko.observableArray([]),
    selectedTransactionValue: ko.observable(''),
    selectedTransactionValueReport: ko.observable(''),
    ViewlookupNames: ko.observableArray([]),
    selectedViewLookupValue: ko.observable(''),
    privileges: ko.observableArray([]),
    mappedPrevileges: ko.observableArray([]),
    selectedPrivilege: ko.observable(''),
    mappedPrivilege: ko.observable(''),
    selectedRole: ko.observable(0),
    subsies: ko.observableArray([]),
    selectedSubsies: ko.observable(''),
    users: ko.observableArray([]),
    selectedUser: ko.observable(''),
    customers: ko.observableArray([]),
    CustomerPackers: ko.observableArray([]),

    mappedSOSubsiData: ko.observableArray([]),
    selectedMapSOSubsi: ko.observable(''),
    selectedSOMapper: ko.observableArray([]),
    selectedSOMaperValue: ko.observable(''),
    uploadSOFileName: ko.observable(''),
    transactionType:ko.observable(-1),
    selectedCustomerValue: ko.observable(''),    
    selectedCustomerPackerDecodeValue: ko.observable(''),
    selectedMapSubsi:ko.observable(''),
    selectedPacker: ko.observableArray([]),
    selectedPackerValue: ko.observable(''),
    selectedBIN: ko.observableArray([]),
    selectedBINValue: ko.observable(''),
    availableTransactionSubsy: ko.observableArray([]),
    selectedTransactionSubsyValue: ko.observable(''),
    totalQty: ko.observable(''),
    truckQty: ko.observable(''),
    qtyperTruck: ko.observable(''),
    subsiType: ko.observable(0),
    mappedSubsies: ko.observableArray([]),
    selectedMappedSubsies: ko.observable(''),
    selectedUserId:ko.observable(0),
    IsAdminLink: ko.observable(false),
    IsSOLink: ko.observable(false),
    IsSCLink: ko.observable(false),
    IsSSCLink: ko.observable(false),
    IsASNLink: ko.observable(false),
    IsSubsiLink: ko.observable(false),
    IsEditLink: ko.observable(false),
    IsCancelSOLink:ko.observable(false),
    IsMappingConfigLink: ko.observable(false),
    IsDataLookUpLink: ko.observable(false),
    transactions: ko.observableArray([]),
    selectedTransaction: ko.observable(''),
    fileTypes: ko.observableArray([]),
    selectedFileType: ko.observable(''),
    isCustomInput: ko.observable(false),
    isEnableEmail: ko.observable(false),   
    columnNumber: ko.observable(''),
    rowNumber: ko.observable(''),
    selectedFieldCount: ko.observable(''),
    sapFields: ko.observableArray([]),
    sapMappedFields: ko.observableArray([]),
    selectedField: ko.observable(''),
    welcomeUser:ko.observable(''),
    headers: ko.observableArray([]),
    selectedHeader: ko.observable(''),
    mappedHeaders: ko.observableArray([]),
    selectedMappedHeaders: ko.observable(''),
    assignValue: ko.observable(''),
    dateFormat: ko.observable(''),
    uploadFileName: ko.observable(''),
    uploadSCFileName:ko.observable(''),
    mapFileName: ko.observable(''),
    mappedFileData: ko.observable(''),
    selectedFile: ko.observable(''),
    scTableData: ko.observableArray([]),
    scResultTableData: ko.observableArray([]),
    selectedLineContent: ko.observableArray([]),
    CustomerReferenceNumber: ko.observableArray([]),
    lineDeleteComment: ko.observable(''),
    transactionIdVal: ko.observable(0),
    IsDateFormatEnabled: ko.observable(false),
    selectedMapperFileName:ko.observable(''),
    emailIds: ko.observable(''),
    selectedTransType: ko.observable(''),
    editSOReason: ko.observable(''),


    SubsiId: ko.observable(''),
    FileName: ko.observable(''),
    TransactionId: ko.observable(''),
    FilePath: ko.observable(''),
    FileTypeId: ko.observable(''),
    FieldCountValue: ko.observable(''),
    mapperId: ko.observable(''),
    sapHeaderFields: ko.observableArray([]),
    SRNO: ko.observable(''),
    FieldId: ko.observable(''),
    emailIds: ko.observableArray([]),
    emailId: ko.observableArray([]),
    IsAssignValueEnabled: ko.observable(false),

    configDatas: ko.observableArray([]),
    configData: ko.observable(''),
    ConfigItem: ko.observable(''),
    Id: ko.observable(''),
    Value: ko.observable(''),
    Description: ko.observable(''),
    LastUpdatedDate: ko.observable(''),
    LastUpdatedBy: ko.observable(''),
    SubsiId: ko.observable(''),
    TransactionId: ko.observable(''),
    availableReasonsMatNo: ko.observableArray([]),
    availableReasonsQty: ko.observableArray([]),
    availableReasonsReqDate: ko.observableArray([]),
    selectedReasonForEditMatNo: ko.observable(),
    selectedReasonForEditQty: ko.observable(),
    selectedReasonForEditReqDate: ko.observable(),
    selectedBatchId: ko.observable(),
    selectedMenu: ko.observable(),
    selectedViewMapperName: ko.observable(),
    editSOQty: ko.observable(),
    availableReasonsCancel: ko.observableArray([]),
    selectedReasonForCancel: ko.observable(),
    selectedDeliveryDates: ko.observableArray([]),
    selectedMaterialNos: ko.observableArray([]),
    selcetedorderQtys: ko.observableArray([]),
    selectedDelLocations: ko.observableArray([]),
    filePath: ko.observable(),
    uploadChangeSOFileName: ko.observable(''),
    SOSubsiesData: ko.observableArray([]),
    selectedSOSubsi: ko.observable(''),
    IsErrorMailDetailsLink: ko.observable(false),
    edtitCustomerCode:ko.observable(),
    
    isEnableFTPForMapper: ko.observable(false),
    FTPType: ko.observable(''),
    FTPhost: ko.observable(''),
    FTPport: ko.observable(''),
    FTPuser: ko.observable(''),
    Ftplocation: ko.observable(''),
    FTPpassword: ko.observable(''),
    FTPencryptiontype: ko.observable(''),
    FTPsslhostcertificatefingerprint: ko.observable(''),
    FTPisenabled: ko.observable(false),
};
ko.bindingHandlers.chosen = {
    init: function (element) {
        ko.bindingHandlers.options.init(element);
        $(element).chosen({ disable_search_threshold: 10 });
    },
    update: function (element, valueAccessor, allBindings) {
        ko.bindingHandlers.options.update(element, valueAccessor, allBindings);
        $(element).trigger('chosen:updated');
    }
};

this.btnLoginClick = function () {
    loginCall();

};

function dynamicHeaderPopulation() {
    $('#menu-v2 ul ul:visible').slideUp('normal');
    $('#htransaction').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hSalesOrder').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hShipmentConfirmation').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hScannedShipmentConfirmation').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hAdvancedShipmentNotification').css({ 'visibility': 'hidden', 'display': 'none' });

    $('#subsili').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#mappingli').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#lookupli').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#adminli').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hmappingConfig').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hAddNewMapping').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hViewMapping').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hdataLookUp').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hAddLookUp').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hUploadLookUp').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hAdmin').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hUsers').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hRoles').css({ 'visibility': 'hidden', 'display': 'none' });    
    $('#hConfiguration').css({ 'visibility': 'hidden' });
    $('#editSOBTN').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#cancelSOBTN').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#errorEmailmenu').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#hScannerRegistration').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#Administratormenu').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#SubsiMenuH').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#Mapping').css({ 'visibility': 'hidden', 'display': 'none' });
    $('#ErrorEmailmenu').css({ 'visibility': 'hidden', 'display': 'none' });
    
    if (AppViewModel.IsAdminLink()) {
        $('#adminli').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hAdmin').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hUsers').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hRoles').css({ 'visibility': 'visible', 'display': 'block' });
        $('#htransaction').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hConfiguration').css({ 'visibility': 'visible' });
        $('#hScannerRegistration').css({ 'visibility': 'visible', 'display': 'block' });
        $('#Administratormenu').css({ 'visibility': 'visible', 'display': 'block' });
        $('#SubsiMenuH').css({ 'visibility': 'visible', 'display': 'block' });
        $('#Mapping').css({ 'visibility': 'visible', 'display': 'block' });
        $('#ErrorEmailmenu').css({ 'visibility': 'visible', 'display': 'block' });
        
    }
    if (AppViewModel.IsDataLookUpLink()) {
        $('#lookupli').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hdataLookUp').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hAddLookUp').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hUploadLookUp').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsMappingConfigLink()) {
        $('#Mapping').css({ 'visibility': 'visible', 'display': 'block' });
        $('#mappingli').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hmappingConfig').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hAddNewMapping').css({ 'visibility': 'visible', 'display': 'block' });
        $('#hViewMapping').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsSCLink()) {
        $('#hShipmentConfirmation').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsSSCLink()) {
 
        $('#hScannedShipmentConfirmation').css({ 'visibility': 'visible', 'display': 'block' });   
    }
    if (AppViewModel.IsSOLink()) {

        $('#hSalesOrder').css({ 'visibility': 'visible', 'display': 'block' });
       
    }
    if (AppViewModel.IsSubsiLink()) {
        $('#SubsiMenuH').css({ 'visibility': 'visible', 'display': 'block' });
        $('#subsili').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsEditLink()) {
        $('#editSOBTN').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsCancelSOLink()) {
        $('#cancelSOBTN').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsASNLink()) {
        $('#hAdvancedShipmentNotification').css({ 'visibility': 'visible', 'display': 'block' });
    }
    if (AppViewModel.IsErrorMailDetailsLink()) {
        $('#ErrorEmailmenu').css({ 'visibility': 'visible', 'display': 'block' });
        $('#errorEmailmenu').css({ 'visibility': 'visible', 'display': 'block' });
    }
   
}

//function searchOdr()
//{
//    if (document.getElementById('collapseTwo').className == "panel-collapse collapse in") {
//        $('#collapseTwo').toggleClass('panel-collapse collapse');
//    }

//}
function loginCall() {
    // var val = "@Url.Content("~")";
    $("#homeimg").attr("src", baseUrl + "/Images/Home Selected.png");
    if ((AppViewModel.Username() != '') && (AppViewModel.Password() != '')) {
        debugger;
        uiBlocker();
        if ($('#gridLineContent').data().kendoGrid != undefined) {
            $('#gridLineContent').data().kendoGrid.destroy();
            $('#gridLineContent').empty();
        }
        if ($('#grid').data().kendoGrid != undefined) {
            $('#grid').data().kendoGrid.destroy();
            $('#grid').empty();
        }

        $.ajax({
            type: "POST",
            url: baseUrl + '/api/User/Login',
            data: ko.toJSON(AppViewModel),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.Success) {                    
                    AppViewModel.selectedMenu('Home');
                    AppViewModel.welcomeUser(AppViewModel.Username());
                    AppViewModel.IsAdminLink(data.AccessLinks.IsAdminLink);
                    AppViewModel.IsDataLookUpLink(data.AccessLinks.IsDataLookUpLink);
                    AppViewModel.IsMappingConfigLink(data.AccessLinks.IsMappingConfigLink);
                    AppViewModel.IsSCLink(data.AccessLinks.IsSCLink);
                    AppViewModel.IsSOLink(data.AccessLinks.IsSOLink);
                    AppViewModel.IsSubsiLink(data.AccessLinks.IsSubsiLink);
                    AppViewModel.IsSSCLink(data.AccessLinks.IsSSCLink);
                    AppViewModel.IsEditLink(data.AccessLinks.IsEditLink);
                    AppViewModel.IsASNLink(data.AccessLinks.IsASNLink);
                    AppViewModel.IsCancelSOLink(data.AccessLinks.IsCancelSOLink);
                    AppViewModel.IsErrorMailDetailsLink(data.AccessLinks.IsErrorMailDetailsLink);                                      
                    dynamicHeaderPopulation();                                               
                    //AppViewModel.availableLookupSubsy([]),
                    AppViewModel.selectedTransactionSubsyValue('');
                    getAllSubsies();
                    getAllTransactions();
                    $('#login').hide();
                    $('html').removeClass('login');
                    $('#menuControle').removeClass('hide');
                    $('#landingContainer').css('visibility','visible');
                    $('#olmenu').removeClass('hide');
                    $('#home').show();
                    $('#menuDiv').show();
                    $('#headerContainer').show();
                    $('#headerContainer').show();
                    $('#welcomeScreen').show();
                    $('#scannerRegst').hide();
                    $('#KendoUIDiv').hide();
                }
                else {
                    var loginAlert = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.Message,
                        dismissQueue: true, 
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });
    }
    else {
       
        var userAlert = noty({
            layout: 'top',
            type: 'error',
            text: "Username/Password cannot be empty",
            dismissQueue: true,
            animation: {
                open: { height: 'toggle' },
                close: { height: 'toggle' },
                easing: 'swing',
                speed: 500
            },
            timeout: 5000
        });       
    }
};
function uiBlocker() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '10px',
            width: '520px',
            paddingRight:'120px',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 0.6,
            color: '#ffffff'
        }
    });
};

this.onLogOutClick = function () {
    uiBlocker();
    $("#homeimg").attr("src", baseUrl + "/Images/Home Selected.png");
    $("#trnimg").attr("src", baseUrl + "/Images/Transaction.png");
    $("#subsimage").attr("src", baseUrl + "/Images/Subsi.png");
    $("#mapimage").attr("src", baseUrl + "/Images/Mapping.png");
    $("#lookupimg").attr("src", baseUrl + "/Images/Datalookup.png");
    $("#adminimg").attr("src", baseUrl + "/Images/Admin.png");
    $("#downloadimg").attr("src", baseUrl + "/Images/download.png");
    $("#errorimage").attr("src", baseUrl + "/Images/error mail.png");

    $('#menu-v2 li').removeClass('active');
    $(this).closest('li').addClass('active');
    var checkElement = $(this).next();
    if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
        $(this).closest('li').removeClass('active');
        checkElement.slideUp('normal');
    }
    if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
        $('#menu-v2 ul ul:visible').slideUp('normal');
        checkElement.slideDown('normal');
    }
    if ($(this).closest('li').find('ul').children().length == 0) {
        $('#menu-v2 ul ul:visible').slideUp('normal');
    }
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/User/Logoff',
        contentType: 'application/json; charset=utf-8',
        success: function () {           
            $('#login').hide();
            $('html').addClass('loading');
            //$('html').addClass('login');
            $('#menuControle').addClass('hide');
            $('#olmenu').addClass('hide');
            $('#home').hide();
            $('#menuDiv').hide();
            $('#headerContainer').hide();
            $('#headerContainer').hide();
            $('#welcomeScreen').hide();
            location.reload(true);
           // $('#login').show();
            $('#home').hide();
            $('#userList').hide();
            $('#subsiMapping').hide();
            $('#welcomeScreen').hide();
            $('#LineContent').hide();
            $('#UploadMapper').hide();
            $('#scMapping').hide();
            $('#viewMapping').hide();
            $('#roleList').hide();
            $('#sapMapping').hide();
            $('#SO').hide();
            $("#sscMapping").hide();
            $('#asnMapping').hide();
            $('#AddLookup').hide();
            $('#ViewLookup').hide();
            $('#scResult').hide();
            $('#soResult').hide();
            $('#viewMappingDetails').hide();
            $('#viewConfig').hide();
            $('#configDelete').hide();
            $('#errorEmail').hide();
            $('#kendoUISSC').hide();
            $('#reportDetails').hide();
            $('#scannerRegst').hide();
            $('#TransactionGrid').jqGrid('GridUnload');
            $('#SalesOrderGrid').jqGrid('GridUnload');
            AppViewModel.Username(''),
            AppViewModel.Password(''),
            AppViewModel.availableSubsy([]),
            AppViewModel.customers([]),
            AppViewModel.CustomerPackers([]),
            AppViewModel.transactionType(-1),
            AppViewModel.selectedCustomerValue('');
            AppViewModel.selectedCustomerPackerDecodeValue('');
            AppViewModel.selectedPacker([]),
            AppViewModel.selectedPackerValue(''),
            AppViewModel.availableLookupSubsy([]),
            AppViewModel.selectedLookupSubsyValue(''),
            AppViewModel.ViewlookupNames([]),
            AppViewModel.selectedViewLookupValue(''),
            AppViewModel.selectedBIN([]),
            AppViewModel.selectedBINValue(''),
            AppViewModel.selectedSubsiValue(''),
            AppViewModel.availableTransactionSubsy([]),
            AppViewModel.selectedTransactionSubsyValue(''),
            AppViewModel.availableEntity([]),
            AppViewModel.selectedEntityValue(''),
            AppViewModel.entityforSubsy([]),
            AppViewModel.selectedEntityforSubsyValue(''),
            AppViewModel.selectedEntityforSubsyValue(''),
            AppViewModel.availableTransactions([]),
            AppViewModel.selectedTransactionValue(''),
            AppViewModel.lookupNames([]),
            AppViewModel.selectedLookupValue(''),
            AppViewModel.mappedSOSubsiData([]),
            AppViewModel.selectedMapSOSubsi(''),
            AppViewModel.selectedSOMapper([]),
            AppViewModel.selectedSOMaperValue(''),
            AppViewModel.uploadSOFileName(''),
            AppViewModel.mapperforSubsy([]),
            AppViewModel.selectedMapperValue('');
            AppViewModel.privileges([]),
            AppViewModel.mappedPrevileges([]),
            AppViewModel.selectedPrivilege(''),
            AppViewModel.mappedPrivilege(''),
            AppViewModel.selectedRole(0),
            AppViewModel.subsies([]),
            AppViewModel.selectedSubsies(''),
            AppViewModel.users([]),
            AppViewModel.selectedUser(''),
            AppViewModel.subsiType(0),
            AppViewModel.mappedSubsies([]),
            AppViewModel.selectedMappedSubsies(''),
            AppViewModel.selectedUserId(0),
            AppViewModel.IsAdminLink(false),
            AppViewModel.IsSOLink(false),
            AppViewModel.IsSCLink(false),
            AppViewModel.IsSubsiLink(false),
            AppViewModel.IsMappingConfigLink(false),
            AppViewModel.IsDataLookUpLink(false),
            AppViewModel.transactions([]),
            AppViewModel.selectedTransaction(''),
            AppViewModel.fileTypes([]),
            AppViewModel.selectedFileType(''),
            AppViewModel.isCustomInput(false),
            AppViewModel.isEnableEmail(false),            
            AppViewModel.columnNumber(''),
            AppViewModel.rowNumber(''),
            AppViewModel.selectedFieldCount(''),
            AppViewModel.sapFields([]),
            AppViewModel.selectedField(''),
            AppViewModel.welcomeUser(''),
            AppViewModel.selectedTransType(''),

            AppViewModel.SubsiId(''),
            AppViewModel.FileName(''),
            AppViewModel.TransactionId(''),
            AppViewModel.FilePath(''),
            AppViewModel.FileTypeId(''),
            AppViewModel.FieldCountValue(''),
            AppViewModel.mapperId(''),
            AppViewModel.sapHeaderFields([]),
            AppViewModel.SRNO(''),
            AppViewModel.FieldId(''),
            AppViewModel.emailIds([]),
            AppViewModel.emailId([]),
            AppViewModel.IsAssignValueEnabled(false),

            AppViewModel.configDatas([]),
             AppViewModel.configData(''),
             AppViewModel.ConfigItem(''),
            AppViewModel.Id(''),
            AppViewModel.Value(''),
             AppViewModel.Description(''),
             AppViewModel.LastUpdatedDate(''),
             AppViewModel.LastUpdatedBy(''),
             AppViewModel.SubsiId(''),
             AppViewModel.TransactionId(''),
            AppViewModel.mappedSubsiData([]),
            AppViewModel.mappedFileData([]),
            AppViewModel.selectedMapSubsi(''),
            AppViewModel.selectedFile(''),
            AppViewModel.uploadSCFileName(''),
            AppViewModel.uploadChangeSOFileName(''),
            AppViewModel.edtitCustomerCode(''),
            AppViewModel.FTPType(''),
            AppViewModel.FTPhost(''),
            AppViewModel.FTPport(''),
            AppViewModel.FTPuser(''),
            AppViewModel.FTPpassword(''),
            AppViewModel.Ftplocation(''),
            AppViewModel.FTPencryptiontype(''),
            AppViewModel.FTPsslhostcertificatefingerprint(''),
            AppViewModel.FTPisenabled(false),
            AppViewModel.isEnableFTPForMapper(false)
           
            $.unblockUI();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
};
