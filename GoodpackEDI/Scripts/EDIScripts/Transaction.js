﻿var gridLineContent = null;
kendo.ui.Tooltip.fn._show = function (show) {
    return function (target) {
        var e = {
            sender: this,
            target: target,
            preventDefault: function () {
                this.isDefaultPrevented = true;
            }
        };

        if (typeof this.options.beforeShow === "function") {
            this.options.beforeShow.call(this, e);
        }
        if (!e.isDefaultPrevented) {
            // only show the tooltip if preventDefault() wasn't called..
            show.call(this, target);
        }
    };
}(kendo.ui.Tooltip.fn._show);

    function getAllTransactions() {
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/Transaction/GetAllTransaction',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.length != 0) {
                    debugger                  
                    AppViewModel.availableTransactions([]);
                    AppViewModel.availableTransactions(data);
                }
                else {
                    $.unblockUI();
                    noty({
                        layout: 'top',
                        type: 'error',
                        text: "Unable to retrieve Transaction Types",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
    };

    this.onSubmitVerify = function () {
        submitSuccessData();
    }
    this.onVerifiedErrorLineSubmitClick = function () {
        submitErrorData();
    }


    function onBackBTNClick() {
        $('#LineContent').hide();
        $('#welcomeScreen').show();
    }

    function onLineSubmitClick() {
        if (document.getElementById('testRB').checked) {
            testModeRecordSelection();
        }
        else {
            sapModeRecordSelection();
        }
        debugger
    }

    this.submitProcessedClick = function () {
    submitSAPErrorData();
}
    function submitSAPErrorData() {
       // $('#deleteLineBTN').hide();
        var LineContentData = [];
        var LineIdArray = new Array();
        var BatchId = "";
        var LineId = "";
        var flag = "0";
        AppViewModel.CustomerReferenceNumber([]);
        AppViewModel.selectedLineContent([]);
      //  var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
        var ids = new Array();//= $("#TransactionLineContentGrid").jqGrid('getDataIDs');
        var idToDataIndex = $("#TransactionLineContentGrid").jqGrid('getGridParam', '_index');
        var size = 0;
        for (key in idToDataIndex) {
            if (idToDataIndex.hasOwnProperty(key)) {
                ids[size] = key;
                size++;
            }
        }
        for (var i = 0; i < ids.length; i++) {
            var rowId = ids[i];
            var gridData = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'data');
            var linestatus;            // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            var linemode;              // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            var selectedRowData;
            for (var x = 0 ; x < gridData.length; x++) {
                var gridObj = gridData[x];
                if (gridObj.UniqueId == rowId) {
                    selectedRowData = gridObj;
                    linestatus = gridObj.Status;
                    linemode = gridObj.Mode;
                }
            }
            //  var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
           // var linemode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            if (linestatus == "ERROR" && linemode == "Processed") {
                //   if (linemode == "Processed") {
                debugger
              //  var selectedRowData = $("#TransactionLineContentGrid").getRowData(rowId);
                BatchId = selectedRowData.Batchid;
                LineId = selectedRowData.UniqueId;
                AppViewModel.CustomerReferenceNumber.push(selectedRowData.ReferenceNumber);
                AppViewModel.selectedLineContent.push(LineId);
                flag = "1";
            }
        }
        var LineModel = {
            BatchId: BatchId,
            LineIds: AppViewModel.selectedLineContent()
        }
        uiBlocker();
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/SC/ReSubmit?status=0&mapperName=' + AppViewModel.selectedMapperFileName() + ' &transactionType=' + AppViewModel.selectedTransactionValue()[0].EntityName,
            data: ko.toJSON(LineModel),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var messageVal = '';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].LineId != 0) {
                        if (!data[i].Status) {
                            for (var j = 0; j < data[i].LineMessage.length; j++) {
                                messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:red"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                            }
                        }
                        else {
                            for (var j = 0; j < data[i].LineMessage.length; j++) {
                                messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:green"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                            }
                        }
                    }
                    else {
                        messageVal = '';
                        messageVal = messageVal + '<p style="color:red">' + data[0].LineMessage[0] + '</p>';
                    }
                }
                $("#lineError").html(messageVal);
                $.unblockUI();
                $("#lineError").dialog("open");


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });
    }


    function testModeRecordSelection() {
        if (document.getElementById('totalRecordsRB').checked) {
            submitSuccessData();
        }
        else {
            submitErrorData();
        }
    }

    function submitSuccessData() {
        debugger;
        var LineContentData = [];
        var LineIdArray = new Array();
        var BatchId = "";
        var LineId = "";
        var flag = "0";
        AppViewModel.CustomerReferenceNumber([]);
        AppViewModel.selectedLineContent([]);
        var ids = new Array();//= $("#TransactionLineContentGrid").jqGrid('getDataIDs');
        var idToDataIndex = $("#TransactionLineContentGrid").jqGrid('getGridParam', '_index');    
        var size=0;
        for (key in idToDataIndex) {
            if (idToDataIndex.hasOwnProperty(key)) {
                ids[size] = key;
                size++;                
            }
        }     
        for (var i = 0; i < ids.length; i++) {
            var rowId = ids[i];
            var gridData = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'data');
            var linestatus;            // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            var linemode;              // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            var selectedRowData;
            for (var x = 0 ; x < gridData.length; x++) {
                var gridObj = gridData[x];
                if (gridObj.UniqueId == rowId) {
                    selectedRowData = gridObj;
                    linestatus = gridObj.Status;
                    linemode = gridObj.Mode;
                }
            }
            //var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            //var linemode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            if (linestatus == "SUCCESS" && linemode == "Verified") {
                debugger
                //var selectedRowData = $("#TransactionLineContentGrid").getRowData(rowId);
                BatchId = selectedRowData.Batchid;
                LineId = selectedRowData.UniqueId;
                AppViewModel.CustomerReferenceNumber.push(selectedRowData.ReferenceNumber);
                AppViewModel.selectedLineContent.push(LineId);
                flag = "1";
            }
        }       
        var LineModel = {
            BatchId: BatchId,
            LineIds: AppViewModel.selectedLineContent()
        }
        uiBlocker();
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/SC/ReSubmit?status=1&mapperName=' + AppViewModel.selectedMapperFileName() + ' &transactionType=' + AppViewModel.selectedTransactionValue()[0].EntityName,
            data: ko.toJSON(LineModel),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                var messageVal = '';
                for (var i = 0; i < data.length; i++) {
                    if (data[i].LineId != 0) {
                        if (!data[i].Status) {
                            for (var j = 0; j < data[i].LineMessage.length; j++) {
                                messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:red"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                            }
                        }
                        else {
                            for (var j = 0; j < data[i].LineMessage.length; j++) {
                                messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:green"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                            }
                        }
                    }
                    else {
                        messageVal = '';
                        messageVal = messageVal + '<p style="color:red">' + data[0].LineMessage[0] + '</p>';
                    }
                }
                $("#lineError").html(messageVal);
                $.unblockUI();
                $("#lineError").dialog("open");


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });



    }

    function submitErrorData() {       
        var LineContentData = [];
        var LineIdArray = new Array();
        var BatchId = "";
        var LineId = "";
        var flag = "0";
        var selectedIds ="";
        AppViewModel.CustomerReferenceNumber([]);
        AppViewModel.selectedLineContent([]);
        if (selectallrecords)
        {  var ids = new Array();//= $("#TransactionLineContentGrid").jqGrid('getDataIDs');
            var idToDataIndex = $("#TransactionLineContentGrid").jqGrid('getGridParam', '_index');    
            var size=0;
            for (key in idToDataIndex) {
                if (idToDataIndex.hasOwnProperty(key)) {
                    ids[size] = key;
                    size++;                
                }
            }
            selectedIds = ids;
        }            
        else
            selectedIds = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'selarrrow');
       // var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
        for (var i = 0; i < selectedIds.length; i++) {
            var rowId = selectedIds[i];
            //  var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'cb');
            //  var ch = jQuery(this).find('#' + rowId + ' input[type=checkbox]').prop('checked');
            //   var val = $("#TransactionLineContentGrid").find('#jqg_TransactionLineContentGrid_' + rowId).prop('checked');
            var gridData = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'data');
            var linestatus;            // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            var linemode;              // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            var selectedRowData;
            for (var x = 0 ; x < gridData.length; x++) {
                var gridObj = gridData[x];
                if (gridObj.UniqueId == rowId) {
                    selectedRowData = gridObj;
                    linestatus = gridObj.Status;
                    linemode = gridObj.Mode;
                }
            }
            //var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            //var linemode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');           
            if (linestatus == "ERROR" && linemode == "Verified") {            
                //var selectedRowData = $("#TransactionLineContentGrid").getRowData(rowId);
                BatchId = selectedRowData.Batchid;
                LineId = selectedRowData.UniqueId;
                AppViewModel.CustomerReferenceNumber.push(selectedRowData.ReferenceNumber);
                AppViewModel.selectedLineContent.push(LineId);
                flag = "1";
            }
        }
        if (flag == "0") {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Please select atleast one line",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        else {
            debugger
            var LineModel = {
                BatchId: BatchId,
                LineIds: AppViewModel.selectedLineContent()
            }
            uiBlocker();
            $.ajax({
                type: "POST",
                url: baseUrl + '/api/SC/ReSubmit?status=2&mapperName=' + AppViewModel.selectedMapperFileName() + ' &transactionType=' + AppViewModel.selectedTransactionValue()[0].EntityName,
                data: ko.toJSON(LineModel),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var messageVal = '';
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].LineId != 0) {
                            if (!data[i].Status) {
                                for (var j = 0; j < data[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:red"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                                }
                            }
                            else {
                                for (var j = 0; j < data[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:green"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                                }
                            }
                        }
                        else {
                            messageVal = '';
                            messageVal = messageVal + '<p style="color:red">' + data[0].LineMessage[0] + '</p>';
                        }
                    }
                    $("#lineError").html(messageVal);
                    $.unblockUI();
                    $("#lineError").dialog("open");


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.unblockUI();
                }
            });

        }
    }

    function onDeleteLineClick() {
        $("#lineDeleteForm").dialog("open");
    }

    var gridrownumber = 15;
    function showSSCDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $('#grid').data().kendoGrid.destroy();
        $('#grid').empty();
        $('#KendoUIBackNew').removeClass("hide");
        $('#KendoUIBack').removeClass("hide");
        $(function () {
            var dataSource = new kendo.data.DataSource({
                type: "odata",
                transport: {
                    read: {
                        url: baseUrl + "/api/TransactionDetailsSSCOData?transactionId=" + dataItem.UniqueId,
                        dataType: "json"
                    },
                },
                schema: {
                    data: function (data) {
                        return data["value"];
                    },
                    total: function (data) {
                        return data["odata.count"];
                    },
                    model: {
                        fields: {
                            Batchid: { type: "string" },
                            BinType: { type: "string" },
                            ConsigneeLocation: { type: "string" },
                            ETD: { type: "string" },
                            Mode: { type: "string" },
                            PackerLocation: { type: "number" },
                            Qty: { type: "string" },
                            ReferenceNumber: { type: "string" },
                            SAPStatus: { type: "string" },
                            Status: { type: "string" },
                            BinNumbers: { type: "string" },
                            UniqueId: { type: "number" }
                        }
                    }
                },
                pageSize: gridPageSize,
                reorderable: true,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                sort: {
                    field: 'UniqueId',
                    dir: 'desc'
                },
                filterable: {
                    mode: "row"
                }
            });
            var grid = $("#grid").kendoGrid({
                dataSource: dataSource,
                dataBound: onDataBound,
                filterable: true,
                sortable: true,
                pageable: true,
                columnMenu: true,
                resizable: true,
                pageable: {
                    pageSizes: [10, 25, 50, 100], input: true, buttonCount: 5, change: function (e) {
                        gridPageSize = $("#grid").data("kendoGrid").grid.dataSource.page()
                    }
                },
                groupable: true,
                height: 430,
                columns: [{
                    field: "UniqueId", width: "190px",
                    title: "TransactionDetails ID"
                }, {
                    field: "BinType", width: "140px",
                }, {
                    field: "ConsigneeLocation", width: "140px",
                    title: "Consignee Location"
                }, {
                    field: "BinNumbers", hidden: true
                }, {
                    field: "ETD", width: "110px",
                    title: "ETD"
                }, {
                    field: "PackerLocation", width: "140px",
                    title: "Packer Location"
                }, {
                    field: "Qty", width: "110px",
                    title: "Qty"
                }, {
                    field: "ReferenceNumber", width: "160px",
                    title: "Customer ReferenceNumber"
                }, {
                    field: "SAPStatus", width: "140px",
                    title: "SAPStatus", hidden: true
                }, {
                    field: "Status", width: "140px",
                    title: "Status"
                }, {
                    command: { text: "Bin Details", click: showBinDetails },
                    title: " ", width: "140px"
                }, {
                    command: { text: "Error Details", click: showErrorDetails },
                    title: " ", width: "140px"
                }]
            });
            wnd = $("#details")
                    .kendoWindow({
                        title: "Error Details",
                        modal: true,
                        visible: false,
                        resizable: false,
                        width: 700,
                        height: 500,
                    }).data("kendoWindow");
            wnd1 = $("#binDetails")
                    .kendoWindow({
                        title: "Bin Details",
                        modal: true,
                        visible: false,
                        resizable: false,
                        width: 700,
                        height: 500,
                    }).data("kendoWindow");
        });
        //$("#grid").kendoTooltip({
        //    filter: "td",
        //    width: 100,
        //    content: function (e) {
        //        var target = e.target; // element for which the tooltip is shown                
        //        return $(target).context.innerHTML;
        //    }
        //}).data("kendoTooltip");
    }
    function onDataBound(arg) {
        var grid = $("#grid").data("kendoGrid");
        var gridData = grid.dataSource.view();
        for (var i = 0; i < gridData.length; i++) {
            var currentUid = gridData[i].uid;
            console.log(gridData[i].Status);
            if (gridData[i].Status == "SUCCESS") {
                var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                var createUserButton = $(currentRow).find(".k-grid-ErrorDetails");
                createUserButton.hide();
                console.log(gridData[i].Status);
            }
        }
    }
    function showErrorDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));        
        wnd.content((dataItem.SAPStatus));
        wnd.center().open();
    }
    var gridPageSize = 10;
    function submitDataKendoUI(e) {//*** e- {1 -> Submit Verified Error ,2 -> Submit Verified Records ,3 -> Submit Processed Error }        
        var status = null;
        var transactionType = null;
        var batchId = null;
        var actionType = null;
        var entityGrid = $("#gridLineContent").data("kendoGrid");
        var data = entityGrid.dataSource.data();
        var totalNumber = data.length;
        status = (e == 1) ? 2: (e == 2) ? 1 : 0;
        actionType = (e == 1) ? true : (e == 2) ? true : false;
        for (var i = 0; i < totalNumber; i++) {
            var currentDataItem = data[i];
            batchId = currentDataItem.Batchid;
            transactionType = AppViewModel.selectedTransactionValue()[0].EntityCode;
            if (e == 3) {
                if (currentDataItem.Status == "ERROR" && currentDataItem.Mode == "Processed") {
                    AppViewModel.CustomerReferenceNumber.push(currentDataItem.ReferenceNumber);
                }
            }
            else if (e == 2) {
                if (currentDataItem.Status == "SUCCESS" && currentDataItem.Mode == "Verified") {
                    AppViewModel.CustomerReferenceNumber.push(currentDataItem.ReferenceNumber);
                }
            }
        }
        if (e == 1) {
            submitErrorDataForKendoUI();
        }
        //else if (e == 2) {
        //    onDeleteLineClick();
        //}
        //if (e == 1||e==2)
        //    return false; 
        if (e == 1)
            return false;
        uiBlocker();
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SC/ReSubmitData?Status=' + status + '&TransactionType=' + transactionType + '&BatchId=' + batchId + '&ActionType=' + actionType,
            success: function (response, status, xhr) {                              
                    var messageVal = '';
                    for (var i = 0; i < response.length; i++) {
                        if (response[i].LineId != 0) {
                            if (!response[i].Status) {
                                for (var j = 0; j < response[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:red"> Message: ' + '' + response[i].LineMessage[j] + '</p>';
                                }
                            }
                            else {
                                for (var j = 0; j < response[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:green"> Message: ' + '' + response[i].LineMessage[j] + '</p>';
                                }
                            }
                        }
                        else {
                            messageVal = '';
                            messageVal = messageVal + '<p style="color:red">' + response[0].LineMessage[0] + '</p>';
                        }
                    }
                    $("#lineError").html(messageVal);
                    $.unblockUI();
                    $("#lineError").dialog("open");              
                $.unblockUI();
            },
            contentType: 'application/json; charset=utf-8',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
    }
    function submitErrorDataForKendoUI() {
        var LineContentData = [];
        var LineIdArray = new Array();
        var BatchId = "";
        var LineId = "";
        var flag = "0";
        var selectedIds = [];
        AppViewModel.CustomerReferenceNumber([]);
        AppViewModel.selectedLineContent([]);
        if ($('#totalRecordsRB').prop('checked')){
            var entityGrid = $("#gridLineContent").data("kendoGrid");
            var data = entityGrid.dataSource.data();
            var totalNumber = data.length;
            for (var i = 0; i < totalNumber; i++) {
                var currentDataItem = data[i];
                batchId = currentDataItem.Batchid;
                selectedIds.push(currentDataItem.UniqueId);
            }            
        }else {
            selectedIds = selectedIdsGrid;
        }       
        for (var i = 0; i < selectedIds.length; i++) {
            var rowId = selectedIds[i];
            var gridData = $("#gridLineContent").data("kendoGrid").dataSource.data();
            var linestatus;            // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
            var linemode;              // = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
            var selectedRowData;
            for (var x = 0 ; x < gridData.length; x++) {
                var gridObj = gridData[x];
                if (gridObj.UniqueId == rowId) {
                    selectedRowData = gridObj;
                    linestatus = gridObj.Status;
                    linemode = gridObj.Mode;
                    break;
                }
            }
            if (linestatus == "ERROR" && linemode == "Verified") {
                //var selectedRowData = $("#TransactionLineContentGrid").getRowData(rowId);
                BatchId = selectedRowData.Batchid;
                LineId = selectedRowData.UniqueId;
                AppViewModel.CustomerReferenceNumber.push(selectedRowData.ReferenceNumber);
                AppViewModel.selectedLineContent.push(LineId);
                flag = "1";
            }
        }
        if (flag == "0") {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Please select atleast one line",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        else {
            debugger
            var LineModel = {
                BatchId: BatchId,
                LineIds: AppViewModel.selectedLineContent()
            }
            console.log(LineModel);
            uiBlocker();
            $.ajax({
                type: "POST",
                url: baseUrl + '/api/SC/ReSubmit?status=2&mapperName=' + AppViewModel.selectedMapperFileName() + ' &transactionType=' + AppViewModel.selectedTransactionValue()[0].EntityName,
                data: ko.toJSON(LineModel),
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    var messageVal = '';
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].LineId != 0) {
                            if (!data[i].Status) {
                                for (var j = 0; j < data[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:red">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:red"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                                }
                            }
                            else {
                                for (var j = 0; j < data[i].LineMessage.length; j++) {
                                    messageVal = messageVal + '<p style="color:green">' + '' + (i + 1) + '. Customer Reference Number : ' + AppViewModel.CustomerReferenceNumber()[i] + '</p><p  style="color:green"> Message: ' + '' + data[i].LineMessage[j] + '</p>';
                                }
                            }
                        }
                        else {
                            messageVal = '';
                            messageVal = messageVal + '<p style="color:red">' + data[0].LineMessage[0] + '</p>';
                        }
                    }
                    $("#lineError").html(messageVal);
                    $.unblockUI();
                    $("#lineError").dialog("open");


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.unblockUI();
                }
            });

        }
    }
    function showBinDetails(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var dataArray = dataItem.BinNumbers.split(',');
        var content = "<table class='kendoModelPadding' style='border-collapse:collapse;' border='1'><tr style='background-color: rgb(194, 221, 182); height: 30px;'><td>Barcode</td><td>Status</td><td>SAP Message</td></tr>"
        for (var i = 0; i < dataArray.length; i++)
        {
            var dataColumn = dataArray[i].split('|');
            content += '<tr><td>' + dataColumn[0] + '</td><td>' + dataColumn[1] + '</td><td>' + dataColumn[2] + '</td></tr>';
        }
        content += "</table>"
        wnd1.content((content));
        wnd1.center().open();
    }
    function displayLoading(target) {
        var element = $(target);
        kendo.ui.progress(element, true);         
    }
    function hideLoading(target) {
        var element = $(target);
        kendo.ui.progress(element, false);
    }
    function reSubmitErrorRecords(e) {
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if (dataItem.ErrorCount > 0) {
            // displayLoading('#grid');
            uiBlocker();
            $.ajax({
                type: "GET",
                url: baseUrl + '/api/Device/ResubmitErrorData?BatchId=' + dataItem.UniqueId,
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                     $.unblockUI();
                    //hideLoading('#grid');
                    if (data.length != 0) {
                        if (data == true) {
                           // $.unblockUI();
                            noty({
                                layout: 'top',
                                type: 'error',
                                text: "Resubmit records still has error",
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                        }
                        else
                        {
                            noty({
                                layout: 'top',
                                type: 'success',
                                text: "Resubmited records successfully submitted",
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                        }
                    }
                    else {

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                    $.unblockUI();
                }
            });
        }
        else {
            wnd = $("#details")
                     .kendoWindow({
                         title: "ReSubmit Status",
                         modal: true,
                         visible: false,
                         resizable: false,
                         width: 300
                     }).data("kendoWindow");
            wnd.content(("No Error record to ReSubmit"));
            wnd.center().open();
        }
    }
    function TransactionDetailsClick() {        
        debugger
        var grid=null;
        if (document.getElementById('DDTrnsubsi').selectedIndex == 0) {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Please select the Subsy.",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });

            return;
        }
        if (document.getElementById('DDTransactionName').selectedIndex == 0) {

            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Please select the Transaction Type.",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        var transactionID = AppViewModel.selectedTransactionValue()[0].EntityCode;
        var transactionName = AppViewModel.selectedTransactionValue()[0].EntityName;
        var transactionSubsiId = AppViewModel.selectedTransactionSubsyValue()[0].SubsiId;
        console.log(transactionName);
        if (transactionName == "Scanned Shipment Confirmation" || transactionName == "Advanced Shipment Notification") {
            $('#TransactionPagerDiv').hide();
            if ($('#grid').data().kendoGrid != undefined) {
                $('#grid').data().kendoGrid.destroy();
                $('#grid').empty();
            }
            $('#KendoUIDiv').show();
            $(function () {
                var dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: baseUrl + "/api/TransactionSSCOData?SubsyCode=" + transactionSubsiId + "&transactionCode=" + transactionID,
                            dataType: "json"
                        },
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            fields: {
                                FileName: { type: "string" },
                                CreatedDate: { type: "date" },
                                MapperName: { type: "string" },
                                SubsyName: { type: "string" },
                                SuccessCount: { type: "number" },
                                TotalRecords: { type: "number" },
                                TransactionName: { type: "string" },
                                TransactionType: { type: "string" },
                                UniqueId: { type: "number" },
                                ErrorCount: { type: "number" }
                            }
                        }
                    },
                    pageSize: gridPageSize,
                    reorderable: true,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    sort: {
                        field: 'UniqueId',
                        dir: 'desc'
                    },
                    filterable: {
                        mode: "row"
                    }
                });  
                grid = $("#grid").kendoGrid({
                    dataSource: dataSource,
                    dataBound: onDataBound1,
                    filterable: { extra: false,
                                    operators: {
                                        string: {
                                            startswith: "Starts with",
                                            eq: "Is equal to",
                                            neq: "Is not equal to",
                                            contains: "Contains",
                                            endswith: "Endswith"
                                        }
                                    }
                                },
                    columnMenu: true,
                    sortable: true,
                    resizable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100], input: true, buttonCount: 5, change: function (e) {
                            gridPageSize = $("#grid").data("kendoGrid").grid.dataSource.page()
                        }
                    },
                    groupable: true,
                    height: 450,
                    columns: [{
                        field: "UniqueId", width: "150px",
                        title: "Transaction ID", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "MapperName", title: "Mapper Name", width: "150px"
                    }, {
                        field: "SubsyName",
                        title: "Subsy Name",hidden:true
                    }, {
                        field: "TransactionName", width: "150px",
                        title: "Transaction Name",hidden:true
                    }, {
                        field: "TransactionType", width: "150px",
                        title: "Transaction Type", hidden: true
                    }, {
                        field: "FileName", width: "150px",
                        title: "File Name"
                    }, {
                        field: "TotalRecords", width: "150px",
                        title: "Total Records", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "SuccessCount", width: "150px",
                        title: "Success Count", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "ErrorCount", width: "150px",
                        title: "Error Count", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "CreatedDate", width: "150px",
                        title: "Created Date", format: "{0:dd/MMM/yy hh:mm tt}",
                        filterable: {
                            ui: "datetimepicker"
                        }
                    }, {
                        command: { text: "Show Details", click: showSSCDetails },
                        title: " ", width: "150px"
                    }, {
                        command: { text: "ReSubmit Error", click: reSubmitErrorRecords },
                        title: " ", width: "150px"
                    }]
                }).data("kendoGrid");      
            });
            $("#grid").kendoTooltip({
                filter: "td",
                width: 150,
                beforeShow: function (f) {
                    if ($(f.target).data("name") == null) {
                        f.preventDefault();
                    }
                },
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown
                    return $(target).context.innerHTML;
                }
            }).data("kendoTooltip");
            return false;
        }
        else if (transactionName == "Shipment Confirmation" || transactionName == "Sales Order" )
        {
            $('#TransactionPagerDiv').hide();
            if ($('#grid').data().kendoGrid != undefined) {
                $('#grid').data().kendoGrid.destroy();
                $('#grid').empty();
            }
            $('#KendoUIDiv').show();
            $(function () {
                var dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: baseUrl + "/api/TransactionOData?SubsyCode=" + transactionSubsiId + "&transactionCode=" + transactionID,
                            dataType: "json"
                        },
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            fields: {
                                FileName: { type: "string" },
                                CreatedDate: { type: "date" },
                                MapperName: { type: "string" },
                                SubsyName: { type: "string" },
                                SuccessCount: { type: "number" },
                                TotalRecords: { type: "number" },
                                TransactionName: { type: "string" },
                                TransactionType: { type: "string" },
                                UniqueId: { type: "number" },
                                DeletedRecordCount: { type: "number" },
                                ErrorCount: { type: "number" }
                            }
                        }
                    },
                    pageSize: gridPageSize,
                    reorderable: true,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true,
                    sort: {
                        field: 'UniqueId',
                        dir: 'desc'
                    },
                    filterable: {
                        mode: "row"
                    }
                });
                function onDataBound(arg) {
                    //var grid = this;
                    //grid.tbody.find('>tr').each(function () {
                    //    var dataItem = grid.dataItem(this);
                    //    if (dataItem.ErrorCount>0) {

                    //    }
                    //})                   
                }
                grid = $("#grid").kendoGrid({
                    dataSource: dataSource,
                    dataBound: onDataBound,
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to",
                                contains: "Contains",
                                endswith: "Endswith"
                            }
                        }
                    },
                    columnMenu: true,
                    sortable: true,
                    resizable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100], input: true, buttonCount: 5, change: function (e) {
                            gridPageSize = $("#grid").data("kendoGrid").grid.dataSource.page()
                        }
                    },
                    groupable: true,
                    height: 450,
                    columns: [{
                        field: "UniqueId", width: "150px",
                        title: "Transaction ID", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "MapperName", title: "Mapper Name", width: "150px"
                    }, {
                        field: "SubsyName", 
                        title: "Subsy Name", hidden: true
                    }, {
                        field: "FileName",
                        title: "File Name", width: "150px"
                    }, {
                        field: "TransactionName",
                        title: "Transaction Name", hidden: true
                    }, {
                        field: "TransactionType",
                        title: "Transaction Type", width: "130px"
                    }, {
                        field: "TotalRecords", width: "150px",
                        title: "Total Records", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "SuccessCount", width: "160px",
                        title: "Success Count", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "ErrorCount", width: "150px",
                        title: "Error Count", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "DeletedRecordCount", width: "150px",
                        title: "Deleted Count", filterable: {
                            ui: function (element) {
                                element.kendoNumericTextBox({
                                    format: "n0"
                                });
                            }
                        }
                    }, {
                        field: "CreatedDate", width: "150px",
                        title: "Created Date", format: "{0:dd/MMM/yy hh:mm tt}",
                        filterable: {
                            ui: "datetimepicker"
                        }
                    }, {
                        command: { text: "Show Details", click: showSCandSODetails },
                        title: " ", width: "140px"
                    }]
                }).data("kendoGrid");
            });
            $("#grid").kendoTooltip({
                filter: "td",
                width: 150,
                beforeShow: function (f) {
                    if ($(f.target).data("name") == null) {
                        f.preventDefault();
                    }
                },
                content: function (e) {
                    var target = e.target; // element for which the tooltip is shown
                    return $(target).context.innerHTML;
                }
            }).data("kendoTooltip");
            return false;
        }
        else
        {
            $('#KendoUIDiv').hide();
            $('#TransactionPagerDiv').show();
        }
        if (transactionName != "Shipment Confirmation" && transactionName != "Scanned Shipment Confirmation" && transactionName != "Sales Order" && transactionName != "Advanced Shipment Notification") {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Transaction details for " + AppViewModel.selectedTransactionValue()[0].EntityName + " currently unavailable.",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        switch (AppViewModel.selectedTransactionValue()[0].EntityName) {
       // switch (AppViewModel.selectedTransactionValue()) {
            case "Sales Order": AppViewModel.transactionType(0); break;
            case "Scanned Shipment Confirmation": AppViewModel.transactionType(2); break;
            case "Shipment Confirmation": AppViewModel.transactionType(1); break;
            case "Advanced Shipment Notification": AppViewModel.transactionType(3); break;
            default: AppViewModel.transactionType(-1); break;
        }
        var sortFlag = 0;
       var selectedSubsyId = AppViewModel.selectedTransactionSubsyValue()[0].SubsiId;       
        $('#TransactionGrid').jqGrid('GridUnload');
        $("#TransactionGrid").jqGrid({
            url: baseUrl + '/api/Transaction/SCTransactionDetails?subsyCode=' + selectedSubsyId + '&transactionCode=' + transactionID,
            datatype: "json",
            myType: "GET",
            colNames: ['Transaction ID', 'Subsy Name', 'Transaction', 'Transaction Type', 'Mapper Name', 'File Name', 'Date Created', 'Total Records','Success Count','Error Count', 'Action'],
            colModel: [
            { name: 'UniqueId', index: 'UniqueId', align: 'center' },
            { name: 'SubsyName', index: 'SubsyName', align: 'center' },
            { name: 'TransactionName', index: 'TransactionName', align: 'center' },
            { name: 'TransactionType', index: 'TransactionType', align: 'center' },
            { name: 'MapperName', index: 'FileName', align: 'center', width: '200px' },
            { name: 'FileName', index: 'FileName', align: 'center', width: '200px' },
            { name: 'CreatedDate', index: 'CreatedDate', align: 'center' },
            { name: 'TotalRecords', index: 'TotalRecords', align: 'center' },
            { name: 'SuccessCount', index: 'SuccessCount', align: 'center' },
            { name: 'ErrorCount', index: 'ErrorCount', align: 'center' },
            { name: 'act', index: 'act', align: 'center', sortable: false }
            ],
            jsonReader: {
                root: 'TransactionData',
                id: 'UniqueId',
                total: 'totalpages',
                records: 'totalrecords',
                repeatitems: true,
            },
            pager: $('#transactionPager'),
            excel: true,
            rowNum: gridrownumber == 0?15:gridrownumber,
            rowList: [15, 50, 100, 200, 5000],
            autowidth: true,
            shrinkToFit: true,
            sortorder: "desc",
            viewrecords: true,
            ////commented for servier side pagination  loadonce: true,
            autoencode: false,
            height: '98%',
            width: '100%',
            caption: "Transaction Details",
            loadComplete: function (data) {
                debugger
                $("tr.jqgrow:odd").css("background", "#f1f1f1");

                var ids = $("#TransactionGrid").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i];
                    se = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px; 'type='button' Id='" + rowId + "submit" + "' value='Show Details' onclick=\"detailsButtonClick('" + rowId + "')\"  />";
                    $("#TransactionGrid").jqGrid('setRowData', ids[i], { act: se })
                }
                if (sortFlag == 0) {

                    if (!data.ModelStatus) { 
                        var noData = noty({
                            layout: 'top',
                            type: 'error',
                            text: data.S_errMsg,
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                        $("#TransactionGrid").jqGrid('setGridState', 'hidden');
                    }
                    else {
                        var msg2 = noty({
                            layout: 'top',
                            type: 'success',
                            text: 'Transaction Details Generated.',
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                    }
                    sortFlag = 1;
                }
                gridrownumber = $("#TransactionGrid").getGridParam("reccount");
            },
        });
        $('#TransactionGrid').jqGrid('navGrid', '#transactionPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#TransactionGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});
    };
    function onDataBound1(arg) {
        var grid = $("#grid").data("kendoGrid");
        var gridData = grid.dataSource.view();
        for (var i = 0; i < gridData.length; i++) {
            var currentUid = gridData[i].uid;
            console.log(gridData[i].ErrorCount);
            if (gridData[i].ErrorCount == 0) {
                var currentRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                var createUserButton = $(currentRow).find(".k-grid-ReSubmitError");
                createUserButton.hide();
                console.log(gridData[i].Status);
            }
        }
    }
    function showSCandSODetails(e)
    {
        e.preventDefault();
        $('#LineContent').show();
        $('#welcomeScreen').hide();
        $('#TransactionLineContentPagerDiv').hide();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var transactionCode = dataItem.TransactionName == "SO" ? 2 : 3;    
        AppViewModel.transactionIdVal(dataItem.UniqueId);
        AppViewModel.selectedMapperFileName('');
        AppViewModel.selectedMapperFileName(dataItem.MapperName);
        AppViewModel.selectedBatchId(dataItem.UniqueId);
        //$('#grid').data().kendoGrid.destroy();
        //$('#grid').empty();
        GetBatchStatus(dataItem.UniqueId,transactionCode);
        if (dataItem.TransactionName == "SO") {
            $('#backtoTransBatch').hide();
            $('#deleteLine').hide();
            $("#submitVerifiedErrorLineBTN").hide();
            $("#submitProcessedErrorLineBTN").hide();
            $("#submitVerifiedBTN").hide();
            $('#KendoUIBack').removeClass("hide");
            if ($('#gridLineContent').data().kendoGrid != undefined) {
                $('#gridLineContent').data().kendoGrid.destroy();
                $('#gridLineContent').empty();
            }
            AppViewModel.selectedMenu("HOME >> " + AppViewModel.selectedTransactionValue()[0].EntityName + " >> BatchId : " + dataItem.UniqueId);
            $(function () {
                var dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: baseUrl + "/api/TransactionDetailsSOOData?transactionId=" + dataItem.UniqueId,
                            dataType: "json"
                        },
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            fields: {
                                Batchid: { type: "number" },
                                CustomerNumber: { type: "string" },
                                CustomerPODate: { type: "string" },
                                CustomerPONumber: { type: "string" },
                                ETA: { type: "date" },
                                ETD: { type: "date" },
                                FromLocation: { type: "string" },
                                LineContent: { type: "string" },
                                MaterialNumber: { type: "string" },
                                Mode: { type: "string" },
                                POD: { type: "string" },
                                POL: { type: "string" },
                                ReqDeliveryDate: { type: "string" },
                                SAPStatus: { type: "string" },
                                SOQuantity: { type: "string" },
                                Status: { type: "string" },
                                Consignee: { type: "string" },
                                UniqueId: { type: "string" }
                            }
                        }
                    },
                    pageSize: gridPageSize,
                    reorderable: true,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    sort: {
                        field: 'UniqueId',
                        dir: 'desc'
                    },
                    filterable: {
                        mode: "row"
                    }
                });
                gridLineContent = $("#gridLineContent").kendoGrid({
                    dataSource: dataSource,
                    filterable: true,
                    sortable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100], input: true, buttonCount: 5, change: function (e) {
                            gridPageSize = $("#gridLineContent").data("kendoGrid").grid.dataSource.page()
                        }
                    },
                    columnMenu: true,
                    resizable: true,
                   // groupable: true,
                    height: 430,
                    columns: [
                    //    {
                    //    width: "35px",
                    //    title: "<input id='kendocheckAll', type='checkbox', class='check-box'  onclick='CheckAll();'/>", hidden: true,
                    //    template: "<input type=\"checkbox\" class='check-box' onclick='checkboxEvent();'/>"
                    //},
                        {
                            field: "UniqueId", title: "",
                            hidden: true
                        }, {
                            field: "Batchid", hidden:true,
                        }, {
                            field: "CustomerNumber",width:"180px",
                            title: "Customer Number"
                        }, {
                            field: "CustomerPONumber", width: "200px",
                            title: "CustomerPO Number"
                        }, {
                            field: "CustomerPODate", title: "CustomerPO Date",width:"190px"
                        }, {
                            field: "MaterialNumber", width: "170px",
                            title: "Material Number"
                        }, {
                            field: "FromLocation", width: "150px",
                            title: "Packer Code", filterable: {
                                ui: function (element) {
                                    element.kendoNumericTextBox({
                                        format: "n0"
                                    });
                                }
                            }
                        }, {
                            field: "Consignee", width: "140px",
                            title: "Consignee"
                        },
                        {
                            field: "SOQuantity", width: "130px",
                            title: "Quantity", filterable: {
                                ui: function (element) {
                                    element.kendoNumericTextBox({
                                        format: "n0"
                                    });
                                }
                            }
                        }, {
                            field: "ETD", width: "110px",
                            title: "ETD"
                        }, {
                            field: "ETA", width: "110px",
                            title: "ETA"
                        }, {
                            field: "POD", width: "110px",
                            title: "POD"
                        }, {
                            field: "POL", width: "110px",
                            title: "POL"
                        }, {
                            field: "ReqDeliveryDate", width: "180px",
                            title: "ReqDelivery Date", format: "{0:dd/MMM/yy hh:mm tt}",
                        }, {
                            field: "Mode", width: "120px",
                            title: "Mode"
                        }, {
                            field: "SAPStatus", width: "150px",
                            title: "SAP Status"
                        },  {
                            field: "Status", width: "120px",
                            title: "Status"
                        }, {
                            field: "LineContent", width: "150px",
                            title: "LineContent", template: "#=Getvalue(Status,Mode,UniqueId)#"
                        }]
                }).data("kendoGrid");;
                //wnd = $("#details")
                //        .kendoWindow({
                //            title: "Error Details",
                //            modal: true,
                //            visible: false,
                //            resizable: false,
                //            width: 300
                //        }).data("kendoWindow");
            });
            //grid.table.on("click", ".checkbox", selectRow);
        }
        else {
            $('#backtoTransBatch').hide();
            $('#deleteLine').hide();
            $("#submitVerifiedErrorLineBTN").hide();
            $("#submitProcessedErrorLineBTN").hide();
            $("#submitVerifiedBTN").hide();
            $('#KendoUIBack').removeClass("hide");
            if ($('#gridLineContent').data().kendoGrid != undefined) {
                $('#gridLineContent').data().kendoGrid.destroy();
                $('#gridLineContent').empty();
            }
            AppViewModel.selectedMenu("HOME >> " + AppViewModel.selectedTransactionValue()[0].EntityName + " >> BatchId : " + dataItem.UniqueId);          

            $(function () {
                var dataSource = new kendo.data.DataSource({
                    type: "odata",
                    transport: {
                        read: {
                            url: baseUrl + "/api/TransactionDetailsSCOData?transactionId=" + dataItem.UniqueId,
                            dataType: "json"
                        },
                    },
                    schema: {
                        data: function (data) {
                            return data["value"];
                        },
                        total: function (data) {
                            return data["odata.count"];
                        },
                        model: {
                            fields: {
                                Batchid: { type: "string" },
                                BinType: { type: "string" },
                                consigneeLocation: { type: "string" },
                                LineContent: { type: "string" },
                                Mode: { type: "string" },
                                packerLocation: { type: "number" },
                                Qty: { type: "string" },
                                ReferenceNumber: { type: "string" },
                                SAPStatus: { type: "string" },
                                Status: { type: "string" },
                                CustomerCode: { type: "string" },
                                UniqueId: { type: "number" }
                            }
                        }
                    },
                    pageSize: gridPageSize,
                    reorderable: true,
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                    sort: {
                        field: 'UniqueId',
                        dir: 'desc'
                    },
                    filterable: {
                        mode: "row"
                    }
                });
                gridLineContent = $("#gridLineContent").kendoGrid({
                    dataSource: dataSource,
                    filterable: true,
                    dataBound: onDataSCLineBound,
                    sortable: true,
                    pageable: {
                        pageSizes: [10, 25, 50, 100], input: true, buttonCount: 5, change: function (e) {
                            gridPageSize = $("#gridLineContent").data("kendoGrid").grid.dataSource.page()
                        }
                    },
                    columnMenu: true,
                    resizable: true,
                    // groupable: true,
                    height: 430,
                    columns: [{
                        width: 35,
                        title: "<input id='kendocheckAll', type='checkbox', class='check-box' onclick='CheckAll();' />",
                        template: "<input type='checkbox' class='check-box'  onclick='checkboxEvent();'/>",
                        hidden: true
                        }, {
                            field: "UniqueId",  hidden: true
                        }, {
                            field: "LineContent", hidden: true
                        }, {
                            field: "Batchid", hidden: true,
                            title: "Batchid"
                        }, {
                            field: "CustomerCode", width: "140px",
                            title: "Customer"
                        }, {
                            field: "ReferenceNumber", width: "250px",
                            title: "Customer Reference Number"
                        }, {
                            field: "packerLocation", width: "130px",
                            title: "Packer", filterable: {
                                ui: function (element) {
                                    element.kendoNumericTextBox({
                                        format: "n0"
                                    });
                                }
                            }
                        }, {
                            field: "consigneeLocation", width: "140px",
                            title: "Consignee", filterable: {
                                ui: function (element) {
                                    element.kendoNumericTextBox({
                                        format: "n0"
                                    });
                                }
                            }
                        }, {
                            field: "BinType", width: "130px",
                        }, {
                            field: "Mode", width: "120px",
                            title: "Mode"
                        }, {
                            field: "Qty", width: "120px",
                            title: "Qty", filterable: {
                                ui: function (element) {
                                    element.kendoNumericTextBox({
                                        format: "n0"
                                    });
                                }
                            }
                        },  {
                            field: "SAPStatus", width: "120px",
                            title: "SAPStatus"
                        }, {
                            field: "Status", width: "120px",
                            title: "Status"
                        }, {
                            field: "LineContent", width: "130px",
                            title: "LineContent", template: "#=Getvalue(Status,Mode,UniqueId)#"
                        }]
                }).data("kendoGrid");;
                //wnd = $("#details")
                //        .kendoWindow({
                //            title: "Error Details",
                //            modal: true,
                //            visible: false,
                //            resizable: false,
                //            width: 300
                //        }).data("kendoWindow");   
            });
        }  
        $('#errorRecordsRB').is(':checked') ? gridLineContent.showColumn(0) : gridLineContent.hideColumn(0);
        $('#totalRecordsRB').prop("checked", true);
        //$("#gridLineContent").kendoTooltip({
        //    filter: "td",
        //    width: 150,
        //    beforeShow: function (f,e) {
        //        console.log(f)
        //        if (f.target[0].innerHTML == "") {
        //            f.preventDefault();                }
        //    },
        //    content: function (e) {
        //        var target = e.target; // element for which the tooltip is shown
        //        var returnValue = null;
        //        if (!$(target).hasClass('k-button k-button-icontext k-grid-ShowDetails'))
        //            returnValue = $(target).context.innerHTML;                
        //        return returnValue;
        //    }
        //}).data("kendoTooltip");
    }
    function CheckAll() {
        if ($('#kendocheckAll').attr('checked')) {
            console.log('checked');
            $('.check-box').attr('checked', 'checked');
        } else {
            console.log('unchecked');
            $('.check-box').removeAttr('checked');
        }
    }
    var selectedIdsGrid = [];
    function checkboxEvent() {      
        var numChkBoxes = $('#gridLineContent input[type=checkbox][id!=kendocheckAll]').length;
        var numChkBoxesChecked = $('#gridLineContent input:checkbox:checked[id!=kendocheckAll]').length;
        var row = $(this).closest("tr"),
        grid = $("#gridLineContent").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        selectedIdsGrid = [];
        $('#gridLineContent input:checkbox:checked[id!=kendocheckAll]').each(function (e) {            
            selectedIdsGrid.push(grid.dataItem($(this).closest("tr")).UniqueId);
        });      
        if (numChkBoxes == numChkBoxesChecked && numChkBoxes > 0) {          
            $('#kendocheckAll').prop('checked', true);
            $('#kendocheckAll').prop('checked', true);
        }
        else {
           // $('#kendocheckAll').prop('checked', false);
        }
    }

    function onDataSCLineBound(e)
    {      
        //var row = e.row;
        //var dataItem = e.dataItem;    
        //var $filter = new Array();
        //selectedIdsGrid = [];
        //$('#kendocheckAll').prop('checked', false);    
    } 
    function Getvalue(status,Mode,Id) {
        if (status == "ERROR")
            return "<input  type='button' style='background-color:#004c97;color:#ffffff;border-radius:5px;' Id='" + Id + "submit" + "' value='Line Details' onclick='lineContentDetails(" + Id + ")'/>";
        else if (status == "ERROR")
            return "<input  type='button' style='border-radius:5px;' Id='" + Id + "submit" + "' value=' Details' onclick='lineContentDetails(" + Id + ")'/>";
        else
            return "";
    }
    function clickFormatter(cellValue) {
        return '<span style="font-weight:bold;text-decoration: underline">' + cellValue + '</span>';
    }

    function detailsButtonClick(rowId) {
        debugger;
        //alert(rowId);
        $('#LineContent').show();
        $('#welcomeScreen').hide();
        AppViewModel.transactionIdVal(rowId);
        var transactionRowData = $("#TransactionGrid").getRowData(rowId);
        AppViewModel.selectedMapperFileName('');
        AppViewModel.selectedMapperFileName(transactionRowData.MapperName);
        var transactionName = AppViewModel.selectedTransactionValue()[0].EntityName;
        if (transactionName == "Shipment Confirmation") {
            getTransLineContent(rowId);
        }
        else if (transactionName == "Scanned Shipment Confirmation") {
            getSSCTransLineContent(rowId);
        }
        else if (transactionName == "Advanced Shipment Notification") {
            getASNTransLineContent(rowId);
        }
        else if (transactionName == "Sales Order") {
            $('#backtoTransBatch').hide();
            $('#deleteLine').hide();
            $("#submitVerifiedErrorLineBTN").hide();
            $("#submitProcessedErrorLineBTN").hide();
            $("#submitVerifiedBTN").hide()
            getSOTransLineContent(rowId);
        }
    }

    function buttonDisp() {
        return '<input type="button" value="Submit">'
    }

    function RBClick(e) {
        var $filter = new Array();
        var grid = $("#gridLineContent").data("kendoGrid");
        $filter.push({ field: "Status", operator: "contains", value: "ERROR" });
        //$("#TransactionLineContentGrid").trigger("reloadGrid");     
        $('#gridLineContent').data('kendoGrid').dataSource.read();
        var grid = $("#gridLineContent").data("kendoGrid");        
        if (e.id == "errorRecordsRB") {
            gridLineContent.showColumn(0);
            grid.dataSource.filter($filter);
            $.each(grid._data, function (key, value) {
                var linestatus = value.Status;
                var lineMode = value.Mode;
                if (linestatus == "ERROR" && lineMode == "Verified") {
                    $('#KendoUISubmitVerifiedErrorbtn').removeClass('hide');
                    $('#KendoUIdeleteLineBTN').removeClass('hide');
                }
                if (linestatus == "ERROR" && lineMode == "Processed") {
                    $('#KendoUISubmitProcessedErrorLineBTN').removeClass('hide');
                }
            });            
        }
        else {
            grid.dataSource.filter([]);
            gridLineContent.hideColumn(0);
            $('#KendoUISubmitProcessedErrorLineBTN').addClass('hide');
            $('#KendoUISubmitVerifiedErrorbtn').addClass('hide');
            $('#KendoUIdeleteLineBTN').addClass('hide');
        }
    }

    AppViewModel.selectedTransactionValue.subscribe(function (newValue) {
        if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
        debugger;
        TransactionDetailsClick();
    });
    AppViewModel.selectedTransactionValueReport.subscribe(function (newValue) {
        if (!newValue || newValue[0] == null || newValue[0] == "") { return; }
        ReportTransactionKendo();
    });
    var selectallrecords = false;
function getTransLineContent(rowId) {
        debugger
        AppViewModel.selectedBatchId(rowId);
        var sortFlag = 0;
        document.getElementById("totalRecordsRB").checked = true;
        $('#TransactionLineContentGrid').jqGrid('GridUnload');
        $("#TransactionLineContentGrid").jqGrid({

            url: baseUrl + '/api/Transaction/SCTransactionLineDetails?transactionId=' + rowId,
            datatype: "json",
            myType: "GET",
            colNames: ['Unique Id', 'Transaction Id', 'Customer Reference Number', 'Packer Location', 'Consignee Location', 'Bin Type', 'Quantity','ETD', 'Status', 'Mode', 'SAP Response', 'Actions'],
            colModel: [
        { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', hidden:true,visible:false },
        { name: 'Batchid', index: 'Batchid', align: 'center', width: '0px', hidden: true ,visible:false},
            { name: 'ReferenceNumber', index: 'ReferenceNumber', align: 'center' },
            { name: 'packerLocation', index: 'packerLocation', align: 'center' },
            { name: 'consigneeLocation', index: 'consigneeLocation', align: 'center' },
            { name: 'BinType', index: 'BinType', align: 'center' },
            { name: 'Qty', index: 'Qty', align: 'center' },
            { name: 'ETD', index: 'ETD', align: 'center' },
            { name: 'Status', index: 'Status', align: 'center' },
            { name: 'Mode', index: 'Mode', align: 'center' },
            { name: 'SAPStatus', index: 'SAPStatus', align: 'center', width: '500px' },
           // { name: 'LineContent', index: 'LineContent', align: 'center',width:'500px' },
            { name: 'Actions', index: 'Actions', align: 'center' },
            ],
            onSelectRow: function (id, status) {
                //Removes Row highlight
                $('#TransactionGrid tr').removeClass("ui-state-highlight");
                if (status) {

                    $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    .map(function () { return this.id; }) // convert to set of ids
                    .get();
                }
            },
            jsonReader: {
                root: 'TranLineData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#transactionLineContentPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit:true,
            viewrecords: true,
            loadonce: true,
            multiselect: true,
            onSelectAll: function (aRowids, status) {
                status == true ? selectallrecords = true : selectallrecords = false;
            },
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Transaction Details",

            loadComplete: function (data) {
                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                debugger

                var mode;
                if (document.getElementById('totalRecordsRB').checked) {
                    mode = "TR";
                }

                if (mode == "TR") {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();                
                    debugger              
                    $("#TransactionLineContentGrid").hideCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus == "SUCCESS" && lineMode == "Verified") {                        
                            $("#submitVerifiedBTN").show();
                        }

                        if (linestatus == "ERROR") {
                         var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                    }
                    $("#TransactionLineContentGrid").jqGrid('hideCol', 'cb');
                    $('#backtoTransBatch').show();
                }
                else {

                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();

                    $("#TransactionLineContentGrid").showCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus != "ERROR") {
                            // $("#rowId", "#TransactionLineContentGrid").css({ display: "none" });
                            var successBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions:successBtn })
                            $("#" + rowId).hide();
                        }
                        else {
                            if (linestatus == "ERROR") {
                                var lineContentBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                                // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                                $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                            }
                            if (lineMode == "Verified") {
                                $("#submitVerifiedErrorLineBTN").show();
                                $('#deleteLine').show();
                                $("#TransactionLineContentGrid").jqGrid('showCol', 'cb');
                            }
                            else if (lineMode == "Processed") {
                                $("#jqg_TransactionLineContentGrid_" + rowId).remove();
                                //$("#jqg_"+rowId,'#TransactionLineContentGrid').remove();
                                $("#submitProcessedErrorLineBTN").show();
                            }
                        }
                    }
                    $('#backtoTransBatch').show();
                }

                if (sortFlag == 0) {
                    if (!data.ModelStatus) {
                        //alert("No Data to Display or Network Error !");
                        var noData = noty({
                            layout: 'top',
                            type: 'error',
                            text: data.S_errMsg,
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                        $("#TransactionLineContentGrid").jqGrid('setGridState', 'hidden');
                    }
                    else {
                        var msg2 = noty({
                            layout: 'top',
                            type: 'success',
                            text: 'Transaction Details Generated.',
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                    }

                    sortFlag = 1;
                }
            },
        });


        $('#TransactionLineContentGrid').jqGrid('navGrid', '#transactionLineContentPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});

    }
    function getASNTransLineContent(rowId) {
        debugger
        AppViewModel.selectedBatchId(rowId);
        var sortFlag = 0;
        document.getElementById("totalRecordsRB").checked = true;
        $('#TransactionLineContentGrid').jqGrid('GridUnload');
        $("#TransactionLineContentGrid").jqGrid({

            url: baseUrl + '/api/Transaction/ASNTransactionLineDetails?transactionId=' + rowId,
            datatype: "json",
            myType: "GET",
            colNames: ['Unique Id', 'Transaction Id', 'Customer Reference Number', 'Packer Location', 'Consignee Location', 'Bin Type', 'ETD', 'Status', 'Mode', 'SAP Response', 'Actions'],
            colModel: [
        { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', hidden: true, visible: false },
        { name: 'Batchid', index: 'Batchid', align: 'center', width: '0px', hidden: true, visible: false },
            { name: 'ReferenceNumber', index: 'ReferenceNumber', align: 'center' },
            { name: 'packerLocation', index: 'packerLocation', align: 'center' },
            { name: 'consigneeLocation', index: 'consigneeLocation', align: 'center' },
            { name: 'BinType', index: 'BinType', align: 'center' },
            { name: 'ETD', index: 'ETD', align: 'center' },
            { name: 'Status', index: 'Status', align: 'center' },
            { name: 'Mode', index: 'Mode', align: 'center' },
            { name: 'SAPStatus', index: 'SAPStatus', align: 'center', width: '500px' },          
            { name: 'Actions', index: 'Actions', align: 'center' },
            ],
            onSelectRow: function (id, status) {
                //Removes Row highlight
                $('#TransactionGrid tr').removeClass("ui-state-highlight");

                if (status) {

                    $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    .map(function () { return this.id; }) // convert to set of ids
                    .get();
                }
            },
            jsonReader: {
                root: 'TranASNLineData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#transactionLineContentPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit: true,
            viewrecords: true,
            loadonce: true,
            multiselect: true,
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Transaction Details",

            loadComplete: function (data) {
                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                debugger

                var mode;
                if (document.getElementById('totalRecordsRB').checked) {
                    mode = "TR";
                }

                if (mode == "TR") {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();
                    //  $("#errorFileButton").hide();
                    debugger
                    //alert("TR");
                    $("#TransactionLineContentGrid").hideCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus == "SUCCESS" && lineMode == "Verified") {
                            // $("#" + rowId).show();

                            $("#submitVerifiedBTN").show();

                        }
                        if (linestatus == "ERROR") {
                            // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                    }
                    $("#TransactionLineContentGrid").jqGrid('hideCol', 'cb');
                    $('#backtoTransBatch').show();
                }
                else {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();

                    $("#TransactionLineContentGrid").showCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus != "ERROR") {
                            // $("#rowId", "#TransactionLineContentGrid").css({ display: "none" });
                            var successBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: successBtn })
                            $("#" + rowId).hide();
                        }
                        else {
                            if (linestatus == "ERROR") {
                                var lineContentBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                                // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                                $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                            }
                            if (lineMode == "Verified") {
                                $("#submitVerifiedErrorLineBTN").show();
                                $('#deleteLine').show();
                                $("#TransactionLineContentGrid").jqGrid('showCol', 'cb');
                            }
                            else if (lineMode == "Processed") {
                                $("#jqg_TransactionLineContentGrid_" + rowId).remove();
                                //$("#jqg_"+rowId,'#TransactionLineContentGrid').remove();          
                                $("#submitProcessedErrorLineBTN").show();
                            }
                        }
                    }
                    $('#backtoTransBatch').show();
                }
                if (sortFlag == 0) {

                    if (!data.ModelStatus) {
                        //alert("No Data to Display or Network Error !");
                        var noData = noty({
                            layout: 'top',
                            type: 'error',
                            text: data.S_errMsg,
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                        $("#TransactionLineContentGrid").jqGrid('setGridState', 'hidden');
                    }
                    else {
                        var msg2 = noty({
                            layout: 'top',
                            type: 'success',
                            text: 'Transaction Details Generated.',
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                    }

                    sortFlag = 1;
                }
            },
        });


        $('#TransactionLineContentGrid').jqGrid('navGrid', '#transactionLineContentPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});

    }
    function getSSCTransLineContent(rowId) {
        debugger
        var sortFlag = 0;
         var checkSelected = true;
        document.getElementById("totalRecordsRB").checked = true;
        $('#TransactionLineContentGrid').jqGrid('GridUnload');
        $("#TransactionLineContentGrid").jqGrid({

            url: baseUrl + '/api/Transaction/SSCTransactionLineDetails?transactionId=' + rowId,
            datatype: "json",
            myType: "GET",
            colNames: ['Unique Id', 'Transaction Id', 'Customer Reference Number', 'Packer Location', 'Consignee Location', 'Bin Type', 'ETD', 'ETA', 'ScanDate', 'Status', 'Mode', 'SAP Response','Actions'],
            colModel: [
        { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', width: '0px', hidden: true, visible: false },
        { name: 'Batchid', index: 'Batchid', align: 'center', width: '0px', width: '0px', hidden: true, visible: false },
            { name: 'ReferenceNumber', index: 'ReferenceNumber', align: 'center' },
            { name: 'packerLocation', index: 'packerLocation', align: 'center' },
            { name: 'consigneeLocation', index: 'consigneeLocation', align: 'center' },
            { name: 'ETD', index: 'ETD', align: 'center' },
            { name: 'ETA', index: 'ETA', align: 'center' },
            { name: 'ScanDate', index: 'ScanDate', align: 'center' },
            { name: 'BinType', index: 'BinType', align: 'center' },
            { name: 'Status', index: 'Status', align: 'center' },
            { name: 'Mode', index: 'Mode', align: 'center' },     
            { name: 'SAPStatus', index: 'LineContent', align: 'center', width: '500px' },
            { name: 'Actions', index: 'Actions', align: 'center' }
            ],
            onSelectRow: function (id, status) {
                debugger;
                if (status == false) {
                    uncelValue = $("#TransactionLineContentGrid").jqGrid('getCell', id, 'ReferenceNumber');
                    isUnChecked = true;
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var CustomerRefNo = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'ReferenceNumber');
                        if (CustomerRefNo == uncelValue) {
                            $('#TransactionLineContentGrid').jqGrid('resetSelection', rowId);
                        }
                    }

                }
                //Removes Row highlight
                $('#TransactionGrid tr').removeClass("ui-state-highlight");
            var isChecked = false;
            if (status == true && checkSelected == true) {

                $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                .map(function () { return this.id; }) // convert to set of ids
                .get();
                //}
                selFirstRowId = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'selrow');
                isChecked = true;

            }
            if (status == true && checkSelected == false) {

                    $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    .map(function () { return this.id; }) // convert to set of ids
                    .get();
                    //}
                    selFirstRowId = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'selrow');
                    isChecked = true;

                }
                if (status == true && checkSelected == false) {

                    $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    .map(function () { return this.id; }) // convert to set of ids
                    .get();
                    //}
                    selRowId = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'selrow');
                    isChecked = true;
                }
                if (isChecked == true) {

                    selRowId = $("#TransactionLineContentGrid").jqGrid('getGridParam', 'selrow');
                    celValue = $("#TransactionLineContentGrid").jqGrid('getCell', selRowId, 'ReferenceNumber');
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    if (checkSelected == true) {
                        checkSelected = false;
                        for (var i = 0; i < ids.length; i++) {
                            var rowId = ids[i];
                            var CustomerRefNo = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'ReferenceNumber');
                            if (CustomerRefNo == celValue && selFirstRowId != rowId) {
                                $('#TransactionLineContentGrid').jqGrid('setSelection', rowId);
                            }
                        }
                        checkSelected = true;
                    }
                }
            },
            jsonReader: {
                root: 'TranSSCLineData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#transactionLineContentPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit: true,
            viewrecords: true,
            loadonce: true,
            multiselect: true,
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Transaction Details",
            loadComplete: function (data) {
                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                debugger
                var mode;
                if (document.getElementById('totalRecordsRB').checked) {
                    mode = "TR";
                }
                if (mode == "TR") {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();
                    debugger
                    //alert("TR");
                    $("#TransactionLineContentGrid").hideCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus == "SUCCESS" && lineMode == "Verified") {
                            // $("#" + rowId).show();
                            $("#submitVerifiedBTN").show();

                        }
                        if (linestatus == "ERROR") {
                          //  var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                    }   
                    $("#TransactionLineContentGrid").jqGrid('hideCol', 'cb');
                    $('#backtoTransBatch').show();
                }
                else {

                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();

                    $("#TransactionLineContentGrid").showCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus != "ERROR") {
                            // $("#rowId", "#TransactionLineContentGrid").css({ display: "none" });
                            $("#" + rowId).hide();
                        }
                        else {
                            if (linestatus == "ERROR") {
                               // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                                var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                                $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                            }
                            if (lineMode == "Verified") {
                                $("#submitVerifiedErrorLineBTN").show();
                                $('#deleteLine').show();
                                $("#TransactionLineContentGrid").jqGrid('showCol', 'cb');
                            }
                            else if (lineMode == "Processed") {
                                $("#jqg_TransactionLineContentGrid_" + rowId).remove();
                                //$("#jqg_"+rowId,'#TransactionLineContentGrid').remove();
                                $("#submitProcessedErrorLineBTN").show();
                        }
                    }
                }
                $('#backtoTransBatch').show();
            }
            if (sortFlag == 0) {
                if (!data.ModelStatus) {
                    //alert("No Data to Display or Network Error !");
                    var noData = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.S_errMsg,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                    $("#TransactionLineContentGrid").jqGrid('setGridState', 'hidden');
                }
                else {
                    var msg2 = noty({
                        layout: 'top',
                        type: 'success',
                        text: 'Transaction Details Generated.',
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }
                sortFlag = 1;
            }
        },
    });


    $('#TransactionLineContentGrid').jqGrid('navGrid', '#transactionLineContentPager', {
        edit: false, view: false, add: false, del: false, search: true,refreshstate: "current",
        beforeRefresh: function () {
            //alert('In beforeRefresh');
            $('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
        }
    }, {}, {}, {}, { closeAfterSearch: true }, {});

}

    function getSOTransLineContent(rowId) {
        debugger
        //$('#ErrorLineContentGrid').hide();

        AppViewModel.selectedBatchId(rowId);
        var sortFlag = 0;

        document.getElementById("totalRecordsRB").checked = true;
        $('#TransactionLineContentGrid').jqGrid('GridUnload');
        $("#TransactionLineContentGrid").jqGrid({

            url: baseUrl + '/api/Transaction/SOTransactionLineDetails?transactionId=' + rowId,
            datatype: "json",
            myType: "GET",
            colNames: ['Unique Id', 'Transaction Id', 'Customer PO Number','PO Date', 'Customer', 'Req Delivery Date', 'Material', 'From Location', 'Quantity', 'Status', 'Mode', 'SAP Response','Actions'],
            colModel: [
            { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', width: '0px', hidden: true, visible: false },
            { name: 'Batchid', index: 'Batchid', align: 'center', width: '0px', width: '0px', hidden: true, visible: false },
            { name: 'CustomerPONumber', index: 'CustomerPONumber', align: 'center' },
            { name: 'CustomerPODate', index: 'CustomerPODate', align: 'center' },
            { name: 'CustomerNumber', index: 'CustomerNumber', align: 'center' },
            { name: 'ReqDeliveryDate', index: 'ReqDeliveryDate', align: 'center' },
            { name: 'MaterialNumber', index: 'MaterialNumber', align: 'center' },
            { name: 'FromLocation', index: 'FromLocation', align: 'center' },
            { name: 'SOQuantity', index: 'SOQuantity', align: 'center' },
            { name: 'Status', index: 'Status', align: 'center' },
            { name: 'Mode', index: 'Mode', align: 'center' },
            //{ name: 'LineContent', index: 'LineContent', align: 'center', width: '500px' },
            { name: 'SAPStatus', index: 'SAPStatus', align: 'center', width: '200px' },
             { name: 'Actions', index: 'Actions', align: 'center' },

            ],
            //jsonReader: {
            //    repeatitems: false,
            //    root: 'TranSOLineData',
            //    page: function (obj) { return $("#TransactionLineContentGrid").jqGrid('getGridParam', 'page'); },
            //    total: function (obj) { return Math.ceil(obj.length / $("#TransactionLineContentGrid").jqGrid('getGridParam', 'rowNum')); },
            //    records: function (obj) { return obj.length; }
            //},
            jsonReader: {
                root: 'TranSOLineData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#transactionLineContentPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit: true,
            viewrecords: true,
            loadonce: true,
            multiselect: true,
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Transaction Details",

            loadComplete: function (data) {
               $("tr.jqgrow:odd").css("background", "#f1f1f1");
                //$("tr.jqgrow:odd").hover(function () { $("tr.jqgrow:odd").css("background", "#d9e4f0"); });
                debugger

                var mode;
                if (document.getElementById('totalRecordsRB').checked) {
                    mode = "TR";
                }

                if (mode == "TR") {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();
                    $("#TransactionLineContentGrid").jqGrid('hideCol', 'cb');
                    $('#backtoTransBatch').show();
                  //  $("#errorFileButton").hide();


                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        if (linestatus == "ERROR") {
                          //  var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                        if (linestatus == "SUCCESS") {
                            //var sucBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submitsuccess" + "' value='Success Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            //$("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: sucBtn })
                            var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                    }
             
                }
         
                else {
                    debugger;
                   // var grid = $("#TransactionLineContentGrid");
                   // var postdata = grid.jqGrid('getGridParam', 'postData');
                   // jquery.extend(postdata,
                   //                {
                   //                    filters: '',
                   //                    searchField: 'Status',
                   //                    searchOper: 'eq',
                   //                    searchString: 'ERROR'
                   //                });
                   //// grid.jqGrid('setGridParam', { search: text.length > 0, postData: postdata });

                   // $("#TransactionLineContentGrid").jqGrid('setGridParam', { search: true, postData: postdata });
                   // $("#TransactionLineContentGrid").trigger("reloadGrid", [{ page: 1 }]);
                    //$('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
               

                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();

                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus != "ERROR") {

                        //    // $("#"+rowId, "#TransactionLineContentGrid").css({ display: "none" });
                       
                            $("#" + rowId, "#TransactionLineContentGrid").hide();
                       
                        
                        
                        }
                        if (linestatus == "SUCCESS" && lineMode == "Verified") {
                            // $("#" + rowId).show();
                            $("#submitVerifiedBTN").show();

                        }
                        else
                        {
                            if (linestatus == "ERROR") {
                                var data = $("#TransactionLineContentGrid").jqGrid('getDataIDs');


                               // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                                var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                                $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                                }
                            }
                        }
                        $('#backtoTransBatch').show();

                    }

                    if (sortFlag == 0) {

                        if (!data.ModelStatus) {
                            //alert("No Data to Display or Network Error !");
                            var noData = noty({
                                layout: 'top',
                                type: 'error',
                                text: data.S_errMsg,
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                            $("#TransactionLineContentGrid").jqGrid('setGridState', 'hidden');
                        }
                        else {
                            var msg2 = noty({
                                layout: 'top',
                                type: 'success',
                                text: 'Transaction Details Generated.',
                                dismissQueue: true,
                                animation: {
                                    open: { height: 'toggle' },
                                    close: { height: 'toggle' },
                                    easing: 'swing',
                                    speed: 500
                                },
                                timeout: 5000
                            });
                        }

                        sortFlag = 1;
                    }
                },
            });
            $('#TransactionLineContentGrid').jqGrid('navGrid', '#transactionLineContentPager', {
                edit: false, view: false, add: false, del: false, search: true,
                beforeRefresh: function () {
                    //alert('In beforeRefresh');
                    $('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
                }
            }, {}, {}, {}, { closeAfterSearch: true }, {});

    }

    function GetBatchStatus(BatchId,transactionType) {
        //uiBlocker();
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/SC/GetBatchStatus?BatchId=' + BatchId + '&TransactionType=' + transactionType,
            success: function (response, status, xhr) {                
                response.SubmitSAPErrorData == true ? $('#KendoUISubmitProcessedErrorLineBTN').removeClass('hide') : $('#KendoUISubmitProcessedErrorLineBTN').addClass('hide');
                response.SubmitSuccessData == true ? $('#KendoUISubmitVerifiedbtn').removeClass('hide') : $('#KendoUISubmitVerifiedbtn').addClass('hide');
                response.SubmitErrorData == true ? $('#KendoUISubmitVerifiedErrorbtn').removeClass('hide') : $('#KendoUISubmitVerifiedErrorbtn').addClass('hide');
                response.DeleteLinedata == true ? $('#KendoUIdeleteLineBTN').removeClass('hide') : $('#KendoUIdeleteLineBTN').addClass('hide');
                $.unblockUI();
            },
            contentType: 'application/json; charset=utf-8',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                $.unblockUI();
            }
        });
    }
    function onDownloadTransactionClick()
    {
        debugger;
        var BatchId = AppViewModel.selectedBatchId();
        var isDownloadError;
        isDownloadError = Boolean(0);
        var actionUrl = '@Url.Action("GenerateExcelReport", "ADC")';
        var getUrl = '@Url.Action("DownloadExcelReport", "ADC")';
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/SC/DownloadErrorFile?batchId=' + BatchId + '&isDownloadError=' + isDownloadError,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "text/csv");
            },
            contentType: 'application/json; charset=utf-8',
      
            success: function (response, status, xhr) {
                // check for a filename
                var filename = [response].filename;
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }

                var type = xhr.getResponseHeader('Content-Type');

                var blob = new Blob([response], { type: type });

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
            }
        });


    }

    function onDownloadErrorFileClick() {
        debugger;
        var BatchId = AppViewModel.selectedBatchId();
        var isDownloadError;
        // alert(BatchId)  
        isDownloadError = Boolean(1);
        // alert(isDownloadError);
        var actionUrl = '@Url.Action("GenerateExcelReport", "ADC")';
        var getUrl = '@Url.Action("DownloadExcelReport", "ADC")';
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/SC/DownloadErrorFile?batchId=' + BatchId + '&isDownloadError=' + isDownloadError,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "text/csv");
            },
            success: function (response, status, xhr) {
                // check for a filename
                var filename = [response].filename;
                var disposition = xhr.getResponseHeader('Content-Disposition');
                if (disposition && disposition.indexOf('attachment') !== -1) {
                    var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    var matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
                }
                var type = xhr.getResponseHeader('Content-Type');
                var blob = new Blob([response], { type: type });

                if (typeof window.navigator.msSaveBlob !== 'undefined') {
                    // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                    window.navigator.msSaveBlob(blob, filename);
                } else {
                    var URL = window.URL || window.webkitURL;
                    var downloadUrl = URL.createObjectURL(blob);

                    if (filename) {
                        // use HTML5 a[download] attribute to specify filename
                        var a = document.createElement("a");
                        // safari doesn't support this yet
                        if (typeof a.download === 'undefined') {
                            window.location = downloadUrl;
                        } else {
                            a.href = downloadUrl;
                            a.download = filename;
                            document.body.appendChild(a);
                            a.click();
                        }
                    } else {
                        window.location = downloadUrl;
                    }

                    setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
                }
            }
        });
    }

    function getScannerTransLineContent(rowId) {
        debugger

        AppViewModel.selectedBatchId(rowId);
        var sortFlag = 0;

        document.getElementById("totalRecordsRB").checked = true;
        $('#TransactionLineContentGrid').jqGrid('GridUnload');
        $("#TransactionLineContentGrid").jqGrid({

            url: baseUrl + '/api/Transaction/ScannedTransactionLineDetails?transactionId=' + rowId,
            datatype: "json",
            myType: "GET",
            colNames: ['Unique Id', 'Transaction Id', 'Customer Reference Number', 'Packer Location', 'Consignee Location', 'Bin Type', 'Quantity', 'ETD', 'Status', 'Mode', 'SAP Response', 'Actions'],
            colModel: [
        { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', hidden: true, visible: false },
        { name: 'Batchid', index: 'Batchid', align: 'center', width: '0px', hidden: true, visible: false },
            { name: 'ReferenceNumber', index: 'ReferenceNumber', align: 'center' },
            { name: 'packerLocation', index: 'packerLocation', align: 'center' },
            { name: 'consigneeLocation', index: 'consigneeLocation', align: 'center' },
            { name: 'BinType', index: 'BinType', align: 'center' },
            { name: 'Qty', index: 'Qty', align: 'center' },
            { name: 'ETD', index: 'ETD', align: 'center' },
            { name: 'Status', index: 'Status', align: 'center' },
            { name: 'Mode', index: 'Mode', align: 'center' },
            { name: 'SAPStatus', index: 'SAPStatus', align: 'center', width: '500px' },
           // { name: 'LineContent', index: 'LineContent', align: 'center',width:'500px' },
            { name: 'Actions', index: 'Actions', align: 'center' },
            //{
            //    name: 'act', index: 'act', sortable: false, editable: true, align: 'center',
            //    edittype: 'checkbox', editoptions: { value: "True:False" },
            //    formatter: "checkbox", formatoptions: { disabled: false }
            //}

            ],
            onSelectRow: function (id, status) {
                //Removes Row highlight
                $('#TransactionGrid tr').removeClass("ui-state-highlight");

                if (status) {

                    $('#TransactionLineContentGrid')[0].p.selarrrow = $('#TransactionLineContentGrid').find("tr.jqgrow:has(td > input.cbox:checked)")
                    .map(function () { return this.id; }) // convert to set of ids
                    .get();
                }
            },
            jsonReader: {
                root: 'TranLineData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#transactionLineContentPager'),
            excel: true,
            rowNum: 10,
            rowList: [10, 50, 200, 500, 1000],
            autowidth: true,
            shrinkToFit: true,
            viewrecords: true,
            loadonce: true,
            multiselect: true,
            onSelectAll: function (aRowids, status) {
                status == true ? selectallrecords = true : selectallrecords = false;
                //alert(selectallrecords)
                //Use a ternary operator to choose zero if the header has just been unchecked, or the total items if it has just been checked.
                //$("totalSelected").val(status === false ? 0 : aRowids.length);
            },
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Transaction Details",

            loadComplete: function (data) {
                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                debugger

                var mode;
                if (document.getElementById('totalRecordsRB').checked) {
                    mode = "TR";
                }

                if (mode == "TR") {
                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();
                    //  $("#errorFileButton").hide();
                    debugger
                    //alert("TR");
                    $("#TransactionLineContentGrid").hideCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus == "SUCCESS" && lineMode == "Verified") {
                            // $("#" + rowId).show();

                            $("#submitVerifiedBTN").show();

                        }

                        if (linestatus == "ERROR") {
                            // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            var lineContentBtn = "<input style='height:22px;width:100px;background-color:#004c97;color:#ffffff;border-radius:5px;  'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                        }
                    }
                    //var cbs = $("tr.jqgrow > td > input.cbox:disabled", $('#TransactionLineContentGrid')[0]);
                    //cbs.removeAttr("checked");
                    //$("#TransactionLineContentGrid").jqGrid('multiselect', false);
                    //$("#TransactionLineContentGrid").jqGrid('setGridParam', { multiselect: false });
                    $("#TransactionLineContentGrid").jqGrid('hideCol', 'cb');
                    $('#backtoTransBatch').show();
                }
                else {

                    $('#backtoTransBatch').hide();
                    $('#deleteLine').hide();
                    $("#submitVerifiedErrorLineBTN").hide();
                    $("#submitProcessedErrorLineBTN").hide();
                    $("#submitVerifiedBTN").hide();

                    $("#TransactionLineContentGrid").showCol("act")
                    var ids = $("#TransactionLineContentGrid").jqGrid('getDataIDs');
                    for (var i = 0; i < ids.length; i++) {
                        var rowId = ids[i];
                        var linestatus = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Status');
                        var lineMode = $("#TransactionLineContentGrid").jqGrid('getCell', rowId, 'Mode');
                        if (linestatus != "ERROR") {
                            // $("#rowId", "#TransactionLineContentGrid").css({ display: "none" });
                            var successBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                            $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: successBtn })
                            $("#" + rowId).hide();
                        }
                        else {
                            if (linestatus == "ERROR") {
                                var lineContentBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Line Details' onclick=\"lineContentDetails('" + rowId + "')\"  />";
                                // var errorBtn = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Error Details' onclick=\"errorDetails('" + rowId + "')\"  />";
                                $("#TransactionLineContentGrid").jqGrid('setRowData', ids[i], { Actions: lineContentBtn })
                            }
                            if (lineMode == "Verified") {
                                $("#submitVerifiedErrorLineBTN").show();
                                $('#deleteLine').show();
                                $("#TransactionLineContentGrid").jqGrid('showCol', 'cb');
                            }
                            else if (lineMode == "Processed") {

                                //var cbs = $("tr.jqgrow > td > input.cbox:disabled", $('#TransactionLineContentGrid')[0]);
                                //cbs.removeAttr("checked");

                                $("#jqg_TransactionLineContentGrid_" + rowId).remove();
                                //$("#jqg_"+rowId,'#TransactionLineContentGrid').remove();
                                //$("#jqg_" + rowId, '#TransactionLineContentGrid').attr("disabled", true);
                                //$("#jqg_" + rowId, '#TransactionLineContentGrid').remove();
                                //$("#jqg_" + rowId, '#TransactionLineContentGrid').hide();
                                //$("#jqg_" + rowId, '#TransactionLineContentGrid').css("visibility", "hidden");
                                //$("#jqg_" + rowId, '#TransactionLineContentGrid').css({ display: "none" });

                                $("#submitProcessedErrorLineBTN").show();
                            }

                        }
                    }
                    $('#backtoTransBatch').show();
                }

                if (sortFlag == 0) {

                    if (!data.ModelStatus) {
                        //alert("No Data to Display or Network Error !");
                        var noData = noty({
                            layout: 'top',
                            type: 'error',
                            text: data.S_errMsg,
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                        $("#TransactionLineContentGrid").jqGrid('setGridState', 'hidden');
                    }
                    else {
                        var msg2 = noty({
                            layout: 'top',
                            type: 'success',
                            text: 'Transaction Details Generated.',
                            dismissQueue: true,
                            animation: {
                                open: { height: 'toggle' },
                                close: { height: 'toggle' },
                                easing: 'swing',
                                speed: 500
                            },
                            timeout: 5000
                        });
                    }

                    sortFlag = 1;
                }
            },
        });


        $('#TransactionLineContentGrid').jqGrid('navGrid', '#transactionLineContentPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#TransactionLineContentGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});

    }
    $(document).on('click', '#aSelectTrn', function () {
        $('#transactionSelectionDiv').hasClass('in') ? $('#transactionSelectionDiv').removeClass('in') : $('#transactionSelectionDiv').addClass('in')
    });
    function backToTransaction(target) {            
        if (target == 1) {
            $('#totalRecordsRB').trigger('click');
            $('#KendoUIBack').addClass("hide");
            $('#KendoUIBackNew').addClass("hide");
            $('#LineContent').hide();
            $('#welcomeScreen').show();
            $('#TransactionLineContentPagerDiv').show();
        } else {          
            $('#KendoUIBack').addClass("hide");
            $('#KendoUIBackNew').addClass("hide");
            debugger;
            TransactionDetailsClick();
        }
        AppViewModel.selectedMenu("HOME");
    }


    var gridKendoDynamic = null;
    var dataSourceNew = null;

    function ReportTransactionKendo() {
        var url;   
        var transaction=AppViewModel.selectedTransactionValueReport()[0].EntityName;
        if (transaction == "Sales Order")
            url = baseUrl + "/api/TransactionSOSearchOData?transactionId=2";
        else if (transaction == "Shipment Confirmation")
            url = baseUrl + "/api/TransactionSCSearchOData?transactionId=3";
        else if (transaction == "Scanned Shipment Confirmation")
            url = baseUrl + "/api/TransactionSSCSearchOData?transactionId=3";
        else {
            var noData = noty({
                layout: 'top',
                type: 'error',
                text: "Under Development",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
            return;
        }
        generalKendoGridLoading(url, getModelData(transaction), getColumnData(transaction));
    } 
    function generalKendoGridLoading(dataSourceUrl, modelValue, columnData) {
        if ($('#divKendoReport').data().kendoGrid != undefined) {
            $('#divKendoReport').data().kendoGrid.destroy();
            $('#divKendoReport').empty();
        }
        dataSourceNew = new kendo.data.DataSource({
            type: "odata",
            reorderable: true,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            pageSize: 10,
            sort: {
                field: 'UniqueId',
                dir: 'desc'
            },
            filterable: {
                mode: "row"
            },
            schema: {
                data: function (data) {
                    return data["value"];
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: modelValue
            },
            transport: {
                read: {
                    url: dataSourceUrl,// + transactionID,
                    dataType: "json"
                }
            }
        });
        gridKendoDynamic = $("#divKendoReport").kendoGrid({
            dataSource: dataSourceNew,           
            sortable: true,
            columnMenu: true,
            groupable: true,
            resizable: true,
            height: 450,
            columns: columnData,
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        contains: "Contains",
                        endswith: "Endswith"
                    }
                }
            },
            pageable: {
                pageSizes: [10, 100, 500, 1000], input: true, buttonCount: 5, change: function (e) {
                    gridPageSize = $("#grid").data("kendoGrid").grid.dataSource.page()
                }
            }
        });
    }
    function getModelData(transactionType) {
        if (transactionType == "Sales Order") {
            return {
                fields: {
                    FileName: { type: "string" },
                    UploadedDate: { type: "date" },
                    Batchid: { type: "number" },
                    CustomerNumber: { type: "string" },
                    CustomerPONumber: { type: "string" },
                    CustomerPODate: { type: "date" },
                    MaterialNumber: { type: "string" },
                    FromLocation: { type: "string" },
                    Consignee: { type: "string" },
                    SOQuantity: { type: "number" },
                    POD: { type: "string" },
                    POL: { type: "string" },
                    ETA: { type: "date" },
                    ETD: { type: "date" },
                    ReqDeliveryDate: { type: "date" },
                    Mode: { type: "string" },
                    Status: { type: "string" },
                    UniqueId: { type: "number" }
                }
            };
        }
        else if(transactionType ==  "Shipment Confirmation"){
            return {
                fields: {
                    UniqueId: { type: "number" },
                    Batchid: { type: "string" },
                    UploadedDate: { type: "date" },                    
                    ETA: { type: "date" },
                    ETD: { type: "date" },
                    Mode: { type: "string" },
                    Status: { type: "string" },
                    ReferenceNumber: { type: "string" },
                    packerLocation: { type: "string" },
                    consigneeLocation: { type: "string" },
                    BinType: { type: "string" },
                    Qty: { type: "number" },
                    CustomerCode: { type: "string" },
                    FileName: { type: "string" }
                }
            };
        }
        else if (transactionType == "Scanned Shipment Confirmation") {
            return {
                fields: {
                    UniqueId: { type: "number" },
                    Batchid: { type: "string" },
                    UploadedDate: { type: "date" },
                    ETA: { type: "date" },
                    ETD: { type: "date" },
                    Mode: { type: "string" },
                    Status: { type: "string" },
                    ReferenceNumber: { type: "string" },
                    packerLocation: { type: "string" },
                    consigneeLocation: { type: "string" },
                    BinType: { type: "string" },
                    Qty: { type: "number" },
                    CustomerCode: { type: "string" }
                }
            };
        }
    }
    function getColumnData(transactionType)
    {
        if (transactionType == "Sales Order") {
            return [{
                field: "UniqueId", width: "150px", title: "Transaction ID", hidden: true
            }, {
                field: "Batchid", width: "150px", title: "Batch id"
            }, {
                field: "FileName", title: "FileName", width: "150px"
            }, {
                field: "CustomerNumber", width: "150px", title: "CustomerNumber"
            }, {
                field: "CustomerPONumber", title: "Customer PONumber", width: "150px"
            }, {
                field: "CustomerPODate", width: "150px", title: "CustomerPODate", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "MaterialNumber", width: "150px", title: "MaterialNumber"
            }, {
                field: "FromLocation", width: "150px", title: "FromLocation"
            }, {
                field: "Consignee", width: "150px", title: "Consignee"
            }, {
                field: "SOQuantity", width: "150px", title: "SOQuantity"
            }, {
                field: "POD", width: "120px", title: "POD"
            }, {
                field: "POL", width: "120px", title: "POL"
            }, {
                field: "ETA", width: "120px", title: "ETA", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "ETD", width: "120px", title: "ETD", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "ReqDeliveryDate", width: "120px", title: "ReqDeliveryDate", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "Mode", width: "120px", title: "Mode"
            }, {
                field: "Status", width: "120px", title: "Status"
            }
            ];
        } else if (transactionType == "Shipment Confirmation") {
            return [{
                field: "UniqueId", width: "150px", title: "Transaction ID", hidden: true
            }, {
                field: "Batchid", width: "150px", title: "Batch id"
            }, {
                field: "FileName", title: "FileName", width: "150px"
            }, {
                field: "CustomerCode", width: "150px", title: "CustomerNumber"
            }, {
                field: "ReferenceNumber", title: "Customer ReferenceNumber", width: "150px"
            }, {
                field: "UploadedDate", width: "150px", title: "UploadedDate", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "BinType", width: "150px", title: "BinType"
            }, {
                field: "packerLocation", width: "150px", title: "packerLocation"
            }, {
                field: "consigneeLocation", width: "150px", title: "consigneeLocation"
            }, {
                field: "Qty", width: "150px", title: "Quantity"
            }, {
                field: "ETA", width: "120px", title: "ETA", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "ETD", width: "120px", title: "ETD", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "Mode", width: "120px", title: "Mode"
            }, {
                field: "Status", width: "120px", title: "Status"
            }
            ];
        } else if (transactionType == "Scanned Shipment Confirmation") {
            return [{
                field: "UniqueId", width: "150px", title: "Transaction ID", hidden: true
            }, {
                field: "Batchid", width: "150px", title: "Batch id"
            }, {
                field: "CustomerCode", width: "150px", title: "CustomerNumber"
            }, {
                field: "ReferenceNumber", title: "Customer ReferenceNumber", width: "150px"
            }, {
                field: "UploadedDate", width: "150px", title: "UploadedDate", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "BinType", width: "150px", title: "BinType"
            }, {
                field: "packerLocation", width: "150px", title: "packerLocation"
            }, {
                field: "consigneeLocation", width: "150px", title: "consigneeLocation"
            }, {
                field: "Qty", width: "150px", title: "Quantity"
            }, {
                field: "ETA", width: "120px", title: "ETA", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "ETD", width: "120px", title: "ETD", format: "{0:dd/MMM/yy hh:mm tt}", filterable: { ui: "datetimepicker" }
            }, {
                field: "Mode", width: "120px", title: "Mode"
            }, {
                field: "Status", width: "120px", title: "Status"
            }
            ];
        }
    }
    function generateModel(gridData) {
        var model = {};
        model.id = "ID";
        var fields = {};
        for (var property in gridData) {
            var propType = typeof gridData[property];

            if (propType == "number") {
                fields[property] = {
                    type: "number",
                    validation: {
                        required: true
                    }
                };
            } else if (propType == "boolean") {
                fields[property] = {
                    type: "boolean",
                    validation: {
                        required: true
                    }
                };
            } else if (propType == "string") {
                var parsedDate = kendo.parseDate(gridData[property]);
                if (parsedDate) {
                    fields[property] = {
                        type: "date",
                        validation: {
                            required: true
                        }
                    };
                    dateFields.push(property);
                } else {
                    fields[property] = {
                        validation: {
                            required: true
                        }
                    };
                }
            } else {
                fields[property] = {
                    validation: {
                        required: true
                    }
                };
            }
        }
        model.fields = fields;
        return model;
    }
