﻿function loadViewMappingGrid() {

    var sortFlag = 0;

    $('#ViewMappingGrid').jqGrid('GridUnload');
    $("#ViewMappingGrid").jqGrid({

             url: baseUrl + '/api/Mapping/GetAllMappingDetails',
            datatype: "json",
            myType: "GET",
            colNames: ['Id','Subsy Name', 'Transaction', 'File Type', 'Date','Added By','Mapper Name', 'Actions'],
           
            colModel: [
            { name: 'UniqueId', index: 'UniqueId', align: 'center', width: '0px', hidden: true, visible: false },
            { name: 'SubsyName', index: 'SubsyName', align: 'center' },
            { name: 'TransactionType', index: 'TransactionType', align: 'center' },
            { name: 'FileType', index: 'FileType', align: 'center' },
            { name: 'DateAdded', index: 'DateAdded', align: 'center', formatter: dateFormatter },
            { name: 'AddedBy', index: 'AddedBy', align: 'center' },
            { name: 'MapperName', index: 'MapperName', align: 'center' },
           { name: 'act', index: 'act', width: '200px', sortable: false}

            ],
            jsonReader: {
                root: 'ViewMappingData',
                id: 'UniqueId',
                repeatitems: false
            },
            pager: $('#viewmappingPager'),
            excel: true,
            rowNum: 25,
            rowList: [25, 50, 100, 500, 1000],
            autowidth: true,
            shrinkToFit: true,
            viewrecords: true,
            loadonce: true,
            autoencode: false,
            height: 'auto',
            width: '100%',
            caption: "Mapping Details",

            loadComplete: function () {

                $("tr.jqgrow:odd").css("background", "#f1f1f1");
                var ids = $("#ViewMappingGrid").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var rowId = ids[i];
                    be = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='View' onclick=\"ViewMapperViewButtonClick('" + rowId + "')\"  />";
                    ce = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='Edit' onclick=\"ViewMapperEditButtonClick('" + rowId + "')\" />"
                    se = "<input style='height:22px;width:60px;background-color:#004c97;color:#ffffff;border-radius:5px;' type='button' value='Delete' onclick=\"ViewMapperDeleteButtonClick('" + rowId + "')\" />";
                        $("#ViewMappingGrid").jqGrid('setRowData', ids[i], { act: be + ce + se });

                }
            },
        });

    $('#ViewMappingGrid').jqGrid('navGrid', '#viewmappingPager', {
            edit: false, view: false, add: false, del: false, search: true,
            beforeRefresh: function () {
                //alert('In beforeRefresh');
                $('#ViewMappingGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
            }
        }, {}, {}, {}, { closeAfterSearch: true }, {});
}

function ViewMapperViewButtonClick(RowId)
{
   // alert(RowId);
    debugger;
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#roleList').hide();
    $('#SO').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#userList').hide();
    $('#subsiMapping').hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#UploadMapper').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').show();
    var mapperName = $("#ViewMappingGrid").jqGrid('getCell', RowId, 'MapperName');
   // alert(mapperName);
    AppViewModel.selectedViewMapperName(mapperName);
    //getViewMappingDetails(RowId)
    showMapperTable(RowId);
}


function showMapperTable(RowId)
{


    debugger

    var tableDetails = new Array();

    var tableRef = document.getElementById('MapperCreateTable').getElementsByTagName('tbody')[0];
    var tableRows = tableRef.getElementsByTagName('tr');
    var rowCount = tableRows.length;

    debugger

    for (var x = rowCount - 1; x >= 0; x--) {
        tableRef.removeChild(tableRows[x]);
    }

    $("#Mappertable").show();

    uiBlocker();
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Mapping/GetSelectedMappingDetails?susbsyToTransactionMapperId='+ RowId,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();

            debugger

            if (data.ModelStatus == true)
            {
                tableDetails = data.ViewSelectedMappingData;

                debugger

                var rowCount = tableDetails.length;
              //  alert(rowCount);

                for (var k = 0; k < tableDetails.length; k++) {
                    // Insert a row in the table at row index 0

                    var newRow = tableRef.insertRow(tableRef.rows.length);
                    newRow.style.border = '1px solid #ccc';

                    var sapFieldCell = newRow.insertCell(0);
                    sapFieldCell.style.border = '1px solid #ccc';
                    sapFieldCell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(tableDetails[k].SAPField)
                    sapFieldCell.appendChild(newText);


                    var inputFieldcell = newRow.insertCell(1);
                    inputFieldcell.style.border = '1px solid #ccc';
                    inputFieldcell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(tableDetails[k].InputField)
                    inputFieldcell.appendChild(newText);

                    var notecell = newRow.insertCell(2);
                    notecell.style.border = '1px solid #ccc';
                    notecell.style.textAlign = "center";
                    // Append a text node to the cell
                    var newText = document.createTextNode(tableDetails[k].Notes)
                    notecell.appendChild(newText);
                }                  

            }
            else
            {
                var noData = noty({
                    layout: 'top',
                    type: 'error',
                    text: "Error fetching mapper details",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });

                return;
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });

}

function onBackBTNToMappingClick() {
    $('#viewMapping').show();
    $('#viewMappingDetails').hide();
}

function getViewMappingDetails(RowId)
{
    var sortFlag = 0;

    $('#ViewMappingDetailsGrid').jqGrid('GridUnload');
    $("#ViewMappingDetailsGrid").jqGrid({

        url: baseUrl + '/api/Mapping/GetSelectedMappingDetails?susbsyToTransactionMapperId='+RowId ,
        datatype: "json",
        myType: "GET",
        colNames: ['SAP Field Name', 'Input Field', 'Note'],
        colModel: [
        { name: 'SAPField', index: 'SAPField', align: 'center' },
        { name: 'InputField', index: 'InputField', align: 'center' },
        { name: 'Notes', index: 'Notes', align: 'center' },

        ],
        jsonReader: {
            root: 'ViewSelectedMappingData',
            id: 'SAPField',
            repeatitems: false
        },
        pager: $('#viewmappingDetailsPager'),
        excel: true,
        rowNum: 10,
        rowList: [10, 50, 200, 500, 1000],
        autowidth: true,
        shrinkToFit: true,
        viewrecords: true,
        loadonce: true,
        autoencode: false,
        height: 'auto',
        width: '100%',
        caption: "Mapping Details",

        loadComplete: function (data) {

            debugger

            //var ids = $("#ViewMappingDetailsGrid").jqGrid('getDataIDs');
            //for (var i = 0; i < ids.length; i++) {
            //    var rowId = ids[i];
            //    se = "<input style='height:22px;width:100px; 'type='button' Id='" + rowId + "submit" + "' value='Show Details' onclick=\"detailsButtonClick('" + rowId + "')\"  />";
            //    $("#ViewMappingDetailsGrid").jqGrid('setRowData', ids[i], { act: se })

            //    //  $("#TransactionGrid").jqGrid('setRowData', ids[i], { ShowDetails:be+ce+se});
            //}


            if (sortFlag == 0) {

                if (!data.ModelStatus) {
                    //alert("No Data to Display or Network Error !");
                    var noData = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.S_errMsg,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                    $("#ViewMappingDetailsGrid").jqGrid('setGridState', 'hidden');
                }
                else {
                    var msg2 = noty({
                        layout: 'top',
                        type: 'success',
                        text: 'Lookup Details Generated.',
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }

                sortFlag = 1;
            }
        },


    });

    $('#ViewMappingDetailsGrid').jqGrid('navGrid', '#viewmappingDetailsPager', {
        edit: false, view: false, add: false, del: false, search: true,
        beforeRefresh: function () {
            //alert('In beforeRefresh');
            $('#ViewMappingDetailsGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
        }
    }, {}, {}, {}, { closeAfterSearch: true }, {});

}