﻿
this.onSOClick = function () {
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#UploadMapper').hide();
    $('#sscResult').hide();
    $('#subsiMapping').hide();
    $('#roleList').hide();
    $('#SO').show();
    $('#AddnewSO').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#UploadSO').hide();
    $('#scMapping').hide(); 
    $('#viewSO').hide();
    $("#sscMapping").hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#sapMapping').hide();
    $('#LineContent').hide(); 
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#asnVerifyBtn').hide();
    $('#asnMapping').hide();
    $('#configDelete').hide();
    $("#EditSO").hide();
    $("#CancelSO").hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#divTableLegend').hide();
    $('#divSCTableLegend').hide();
    $('#divSSCTableLegend').hide();
    $('#mappingDownloadList').hide();
    //$('#SalesOrderEditGrid').hide();
    AppViewModel.transactionType(0);
    AppViewModel.selectedTransType('SO');
    AppViewModel.selectedMenu('Transactions >> SalesOrder');
};

this.onSCClick = function () {
    debugger;
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#sscResult').hide();
    $('#roleList').hide();
    $('#UploadMapper').hide();
    $('#AddLookup').hide();
    $('#sapMapping').hide();
    $('#scMapping').show(); 
    $('#subsiMapping').hide();
    $('#SO').hide();
    $('#scTable').hide();
    $("#asnTable").hide();
    $('#sscMapping').hide();
    $('#ViewLookup').hide();
    $('#scVerifyBtn').hide();
    $('#asnVerifyBtn').hide()
    $('#scResult').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#asnMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#divTableLegend').hide();
    $('#divSCTableLegend').hide();
    $('#divSSCTableLegend').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.uploadSCFileName('');
    AppViewModel.selectedMapSubsi('');
    AppViewModel.selectedFile('');
    AppViewModel.mappedSubsiData([]);
    AppViewModel.mappedFileData([]);
    mappedSubies();
    
    AppViewModel.transactionType(1);
    AppViewModel.selectedTransType('SC');
    AppViewModel.selectedMenu('Transactions >> Shipment Confirmation');
};
this.onASNClick = function () {
    debugger;
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#sscResult').hide();
    $('#roleList').hide();
    $('#UploadMapper').hide();
    $('#AddLookup').hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#asnMapping').show();
    $('#subsiMapping').hide();
    $('#SO').hide();
    $('#scTable').hide();
    $("#asnTable").hide();
    $('#sscMapping').hide();
    $('#ViewLookup').hide();
    $('#scVerifyBtn').hide();
    $('#asnVerifyBtn').hide()
    $('#scResult').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.uploadSCFileName('');
    AppViewModel.selectedMapSubsi('');
    AppViewModel.selectedFile('');
    AppViewModel.mappedSubsiData([]);
    AppViewModel.mappedFileData([]);
    mappedSubies();

    AppViewModel.transactionType(3);
    AppViewModel.selectedTransType('ASN');
    AppViewModel.selectedMenu('Transactions >> Advanced Shipment Notification');
};
function mappedSubies() {
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/User/AllSubsies',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.mappedSubsiData('');
            if (data.length != 0) {
                AppViewModel.mappedSubsiData([]);
                AppViewModel.mappedSubsiData(data);
                // AppViewModel.selectedMapSubsi([AppViewModel.mappedSubsiData()[0]]);

            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "No subsies are mapped",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });

}

this.onConfigClick = function () {

    debugger

    $('#viewConfig').show();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#UploadMapper').hide();
    $('#roleList').hide();
    $('#sapMapping').hide();
    $('#SO').hide();
    $('#asnMapping').hide();
    $('#UploadMapper').hide();
    $('#scMapping').hide();
    $('#subsiMapping').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#userList').hide();
    $('#scResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#deleteMapping').hide();    
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Administrator >> Configuration');
    loadConfigDetails();
};

function loadConfigDetails() {
    var configData = {
        Id: AppViewModel.Id,
        ConfigItem: AppViewModel.ConfigItem,
        Value: AppViewModel.Value,
        Description: AppViewModel.Description,
        LastUpdatedBy: AppViewModel.LastUpdatedBy,
        LastUpdatedDate: AppViewModel.LastUpdatedDate
    };
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Config/ConfigDetails',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.configDatas([]);
            AppViewModel.configDatas(data);
        }
    });
};

this.onViewMappingClick = function ()
{
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#roleList').hide();
    $('#SO').hide();
    $('#sscResult').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#userList').hide();
    $('#subsiMapping').hide();
    $("#sscMapping").hide();
    $("#asnMapping").hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#UploadMapper').hide();
    $('#viewMappingDetails').hide();
    $('#viewMapping').show();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Mapping >> View Mapping');
    loadViewMappingGrid();
}

this.onAddMapperClick = function () {
    //function onAddMapperClick(){
    $('#welcomeScreen').hide();
    $("#dateRow").hide();
    $('#LineContent').hide();
    $('#roleList').hide();
    $('#SO').hide();
    $('#sscResult').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#userList').hide();
    $("#sscMapping").hide();
    $('#subsiMapping').hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#UploadMapper').show();
    $('#viewMappingDetails').hide();
    $('#viewMapping').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#ASNDiv').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.isEnableEmail(false);
    AppViewModel.selectedMenu('Mapping >> Add New Mapping');
    AppViewModel.selectedSubsiValue('');
    AppViewModel.selectedTransaction('');
    AppViewModel.selectedFileType('');
    AppViewModel.emailIds('');
    AppViewModel.uploadFileName('');
    $('#email').hide();
    uiBlocker();
    $.ajax({
        type: "GET",
        // url: baseUrl + '/api/User/AllSubsies?previlegeCode=101',
        url: baseUrl + '/api/User/AllSubsies',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
                        
            if (data.length != 0) {
                //user.availableLocations('');
                //user.availableLocations(data.Records);
                AppViewModel.availableSubsy('');
                AppViewModel.availableSubsy(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "No subsies are mapped",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Mapping/TransactionTypes',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                //user.availableLocations('');
                //user.availableLocations(data.Records);
                AppViewModel.transactions('');
                AppViewModel.transactions(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Transaction Types",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/Mapping/FileTypes',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            if (data.length != 0) {
                //user.availableLocations('');
                //user.availableLocations(data.Records);
                AppViewModel.fileTypes('');
                AppViewModel.fileTypes(data);
            }
            else {
                $.unblockUI();
                noty({
                    layout: 'top',
                    type: 'error',
                    text: "Unable to retrieve Transaction Types",
                    dismissQueue: true,
                    animation: {
                        open: { height: 'toggle' },
                        close: { height: 'toggle' },
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 5000
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            $.unblockUI();
        }
    });
};

this.onUserClick = function () {
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#UploadMapper').hide();
    $('#roleList').hide();
    $('#sscResult').hide();
    $('#sapMapping').hide();
    $('#SO').hide();
    $('#UploadMapper').hide();
    $('#scMapping').hide();
    $("#sscMapping").hide();
    $('#subsiMapping').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#userList').show();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#asnMapping').hide();
    $('#configDelete').hide();
    $('#viewMappingDetails').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Administrator >> Users');
    $('#UserTableContainer').jtable({
        title: 'EDI User Lists',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: false, //Enable sorting   
        scroll: true,
        openChildAsAccordion: true,
        columnResizable: true, //Actually, no need to set true since it's default
        columnSelectable: true, //Actually, no need to set true since it's default
        saveUserPreferences: true,
        defaultSorting: 'FullName ASC', //Set default sorting        
        actions: {
            listAction: baseUrl + '/api/User/UserList',
            createAction: baseUrl + '/api/User/Create',
            updateAction: baseUrl + '/api/User/Update',
            deleteAction: baseUrl + '/api/User/Delete'
        },
        fields: {
            UserId: {
                title: 'User Id',
                key: true,
                create: false,
                edit: false,
                list: false
            },
            FullName: {
                title: 'Full Name',
                create: true
            },
            Username: {
                title: 'Username',
                edit: false,
                create: true

            },
            Password: {
                title: 'Password',
                type: 'password',
                edit: true,
                list: false,
                create: true
            },
            IsActive: {
                title: 'IsActive',
                type: 'checkbox',
                values: { 'false': 'Passive', 'true': 'Active' },
                defaultValue: 'true',
                list: true,
                create: true,
                edit: true
            },
            EmailId: {
                title: 'Email Id',
                edit: true,
                create: true
            },

            Company: {
                title: 'Company',
                edit: true,
                create: true
            },
            Value: {
                title: 'Role',
                options: baseUrl + '/api/User/AllRoles',
                create: true,
                edit: true,
                list: true
            },

            ReceiveAdminEmail: {
                title: 'Recieve Admin Email',
                type: 'checkbox',
                values: { 'false': 'No', 'true': 'Yes' },
                create: true
            },
            ReceiveNotifications: {
                title: 'Recieve Notifications',
                type: 'checkbox',
                values: { 'false': 'No', 'true': 'Yes' },
                create: true
            }
        },
        //Initialize validation logic when a form is created
        formCreated: function (event, data) {
            data.form.find('input[name="FullName"]').addClass('validate[required]');
            data.form.find('input[name="Username"]').addClass('validate[required]');
            data.form.find('input[name="Password"]').addClass('validate[required]');
            data.form.find('input[name="EmailId"]').addClass('validate[required,custom[email]]');
            data.form.find('input[name="Company"]').addClass('validate[required]');
            data.form.validationEngine();
            //data.form.closest('.ui-widget-content').css("cssText", "z-index: 100000 !important;");
            data.form.closest('.ui-dialog').css("cssText", "z-index: 100000 !important;" + data.form.closest('.ui-dialog').attr('style'));
            data.form.closest('.ui-dialog').find('*').css("cssText", "z-index: 100000 !important;");
           
            
        },
        //Validate form when it is being submitted
        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        //Dispose validation logic when form is closed
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
            
        }
    });
    $('#UserTableContainer').jtable('load');
    $('#subsiMapping').hide();
    $('#SO').hide();
};


this.onRoleClick = function () {
    $('#scResult').hide();
    $('#soResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#sscResult').hide();
    $('#userList').hide();
    $('#roleList').show();
    $('#sapMapping').hide(); 
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SalesOrder').hide();
    $('#scMapping').hide();
    $("#sscMapping").hide();
    $('#asnMapping').hide();
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Administartor >> Roles');
    $('#hqUser').css({ 'background-color': '#004c97' });
    $('#subsiUser').css({ 'background-color': '#bababa' });
    $('#hqAdminUser').css({ 'background-color': '#bababa' });
    uiBlocker();
    AppViewModel.selectedRole(0)
    previlegeCaller();
};

this.onHomeClick = function ()
{
    $('#soResult').hide();
    $('#userList').hide();
    $('#roleList').hide();
    $('#UploadMapper').hide();
    $('#sscResult').hide();
    $('#sscTable').hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#subsiMapping').hide();
    $('#asnMapping').hide();
    $('#UploadMapper').hide();
    $('#SO').hide();
    $("#sscMapping").hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#LineContent').hide();
    $('#scResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#welcomeScreen').show();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Home');

}

this.onSubsiClick = function () {
    $('#scResult').hide();
    $('#soResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#roleList').hide();
    $('#UploadMapper').hide();
    $('#sapMapping').hide();
    $('#sscResult').hide();
    $('#scMapping').hide(); 
    $('#subsiMapping').show();
    $("#sscMapping").hide();
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#AddLookup').hide();
    $('#viewMapping').hide();
    $('#scResult').hide();
    $('#soResult').hide();
    $('#sscTable').hide();
    $('#asnMapping').hide();
    $('#ViewLookup').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Subsi');
    $('#hqUserSubsi').css({ 'background-color': '#004c97' });
    $('#hqAdminUserSubsi').css({ 'background-color': '#bababa' });
    $('#subsiUserSubsi').css({ 'background-color': '#bababa' });
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/User/AllUsers?roleId=' + AppViewModel.subsiType(),
        data: ko.toJSON(AppViewModel.subsiType()),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.users([]);
            AppViewModel.users(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
    //$.ajax({
    //    type: "GET",
    //    url: baseUrl + '/api/User/AllSubsies',
    //    data: ko.toJSON(AppViewModel),
    //    contentType: 'application/json; charset=utf-8',
    //    success: function (data) {
    //         $.unblockUI();
    //         AppViewModel.subsies([]);
    //         AppViewModel.subsies(data)

    //    },
    //    error: function (jqXHR, textStatus, errorThrown) {
    //        $.unblockUI();
    //    }
    //});
};


this.onSSCClick = function () {
    $('#sscVerifyBtn').hide();
    $('#asnVerifyBtn').hide();
    $('#scResult').hide();
    $('#sscResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#roleList').hide();
    $('#sapMapping').hide();
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SalesOrder').hide();
    $('#scMapping').hide();
    $("#sscMapping").show();
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#asnMapping').hide();    
    $('#sscTable').hide();
    $("#asnTable").hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();   
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#reportDetails').hide();
    $('#divTableLegend').hide();
    $('#divSCTableLegend').hide();
    $('#divSSCTableLegend').hide();   
    AppViewModel.uploadSCFileName('');
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Transactions >> Scanned Shipment Confirmation');
    AppViewModel.transactionType(2);
    AppViewModel.selectedTransType('SSC')
    AppViewModel.selectedMapSubsi('');
    AppViewModel.selectedFile('');
    AppViewModel.mappedSubsiData([]);
    AppViewModel.mappedFileData([]);
    mappedSubies();
}

this.onArrow2Click = function () {
    //document.getElementById("arrow1Div").style.display = 'block';
    //document.getElementById("arrow2Div").style.display = 'none';
    document.getElementById("menu-v2").style.display = 'none';
    document.getElementById("menu-v").style.display = 'block';
    document.getElementById('welcomeHeader').style.width = 'auto';
   // document.getElementById('welcomeHeader').style.marginLeft='-80px'
    document.getElementById('trnPageDiv').style.width = 'auto';
    document.getElementById('trnPageDiv').style.marginLeft = '-10px';
    document.getElementById('TransactionPagerDiv').style.width = 'auto';
    document.getElementById('TransactionGrid').style.width = 'auto';
    document.getElementById("menuSelected2").style.display = 'block';
    $('#TransactionGrid').setGridWidth($('#welcomeScreen').width() - 100);
    $('#kendoUIGridDiv').css("  margin-left", "75px");
}
this.onArrow1Click = function () {
    //document.getElementById("arrow2Div").style.display = 'block';
    //document.getElementById("arrow1Div").style.display = 'none';
    document.getElementById("menu-v2").style.display = 'block';
    document.getElementById("menu-v").style.display = 'none';
    //document.getElementById('welcomeHeader').style.marginLeft = '0px';
    //document.getElementById('trnPageDiv').style.marginLeft = '-10px'
    document.getElementById('TransactionPagerDiv').style.width = 'auto';
    document.getElementById('welcomeHeader').style.width = 'auto';
    document.getElementById('trnPageDiv').style.width = 'auto';
    document.getElementById('TransactionGrid').style.width = 'auto';
    document.getElementById("menuSelected2").style.display = 'none';
    $('#TransactionGrid').setGridWidth($('#welcomeScreen').width() - 180);
    $('#kendoUIGridDiv').css("  margin-left", "155px");
    
}
this.onScannRegClick = function () {
    $('#scannerRegst').show();
    $('#sscVerifyBtn').hide();
    $('#asnVerifyBtn').hide();
    $('#scResult').hide();
    $('#sscResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#roleList').hide();
    $('#sapMapping').hide();
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SalesOrder').hide();
    $('#scMapping').hide();
    $("#sscMapping").hide(); //   
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#sscTable').hide();
    $("#asnTable").hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#reportDetails').hide();
    $('#mappingDownloadList').hide();
    AppViewModel.selectedMenu('Administrator >> Scanner Registration');

    $('#ScannerTableContainer').jtable({
        title: 'EDI Scanner List',
        paging: true, //Enable paging
        pageSize: 10, //Set page size (default: 10)
        sorting: false, //Enable sorting   
        scroll: true,
        selecting: true, //Enable selecting
        openChildAsAccordion: true,
        columnResizable: true, //Actually, no need to set true since it's default
        columnSelectable: true, //Actually, no need to set true since it's default
        saveUserPreferences: true,
        defaultSorting: 'DeviceID ASC', //Set default sorting        
        actions: {
            listAction  : baseUrl + '/api/Device/GetDeviceDetails',
            createAction: baseUrl + '/api/Device/Create',
            updateAction: baseUrl + '/api/Device/Update',
            deleteAction: baseUrl + '/api/Device/Delete'
        },
        fields: {
            Id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            DeviceID: {
                title: 'Device ID',
                key: false,
                create: true,
                width: '15%'
            },
            Vertical: {
                title: 'Vertical',
                key: false,
                create: true,
                options: baseUrl + '/api/Device/GetVerticalData',
                width: '10%'
            },
            SubsiId: {
                title: 'Subsy',
                key: false,
                create: true,
                edit: true,
                options: baseUrl + '/api/Device/GetSubsyData',
                width: '10%'
            },
            PackerCode: {
                title: 'Packer code',
                width: '15%',
                create: true,
                edit: true
            },
            TransactionType: {
                title: 'Transaction Type',
                width: '15%',
                options: baseUrl + '/api/Device/GetDeviceTransactionType',
                create: true,
                list: true,
                edit: true
            },
            CustomerCode: {
                title: 'Customer Code',                
                width: '10%',
                edit: true,
                create: true

            },
            MapperName: {
                title: 'Mapper name',               
                edit: false,
                width: '10%',               
                create: true
            },
            SKU: {
                title: 'SKU',                
                width: '10%',
                edit: true,
                create: true
            },
            CustomerEmail: {
                title: 'Customer Email',
                edit: true,
                width: '10%',
                create: true
            },

            Details: {
                title: 'Details',
                edit: true,
                width: '10%',
                create: true
            },
            IsEnabled: {
                title: 'Is Enabled',
               // options: baseUrl + '/api/User/AllRoles',
                create: true,
                width: '10%',
                edit: true,                
                type: 'checkbox',
                values: { 'false': 'Disabled', 'true': 'Enabled' },
                defaultValue: 'true'
            }
        },
        //Initialize validation logic when a form is created
        formCreated: function (event, data) {
            //var availableTags = ['MB3', 'MB4', 'MB5', 'MB7', 'MB5H', 'MB5HE'];
            //data.form.find('input[name="SKU"]').autocomplete({
            //    source: availableTags
            //});
            data.form.find('input[name="DeviceID"]').addClass('validate[required]');
            data.form.find('input[name="PackerCode"]').addClass('validate[required]');
            data.form.find('input[name="CustomerCode"]').addClass('validate[required]');
            data.form.find('input[name="MapperName"]').addClass('validate[required]');   
            data.form.validationEngine();

            $(document).on('focus', '#Edit-SKU', function () {
                var availableTags = ['MB3', 'MB4', 'MB5', 'MB7', 'MB5H', 'MB5HE'];
                console.log('aaaa');
                $('#divDialogSKU').html();
                var tableString = '<table id="tableSKU">';
                for (var i = 0; i < availableTags.length; i++) {
                    tableString = tableString + '<tr><td width="20px" style="border:1px solid ;margin: 0 auto;"><input type="checkbox" value="' + availableTags[i] + '"/></td><td width="100px" style="border:1px solid ">' + availableTags[i] + '</td></tr>';
                }
                tableString = tableString + '</table>';
                $('#divDialogSKU').html(tableString);
                $('#divDialogSKU').dialog({
                    height: 250, zIndex: 50000,
                    width: 300,
                    modal: true,
                    buttons: {
                        "Add SKU": function() {
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function () {
                        var values='';
                        $('#tableSKU').find('input[type="checkbox"]:checked').each(function () {
                            if (values == '')
                                values = values + $(this).val();
                            else
                                values = values + ',' + $(this).val();
                        });
                        if (values != '')
                            $('#Edit-SKU').val(values);

                    }
                });
            });
        },
        //Validate form when it is being submitted
        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        //Dispose validation logic when form is closed
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    });
    
    $('#ScannerTableContainer').jtable('load');
};

var dataTableId = null;
this.onDownloadMapperTemplateClick = function () {
    $('#scannerRegst').hide();
    $('#sscVerifyBtn').hide();
    $('#asnVerifyBtn').hide();
    $('#scResult').hide();
    $('#sscResult').hide();
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#roleList').hide();
    $('#sapMapping').hide();
    $('#UploadMapper').hide();
    $('#subsiMapping').hide();
    $('#SalesOrder').hide();
    $('#scMapping').hide();
    $("#sscMapping").hide(); //   
    $('#UploadMapper').hide();
    $('#SO').hide();
    $('#sscTable').hide();
    $("#asnTable").hide();
    $('#AddLookup').hide();
    $('#ViewLookup').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#reportDetails').hide();    
    $('#tblMappingList').html('');
    $('#mappingDownloadList').show();
    AppViewModel.selectedMenu('Templates >> Download Mapper Template');
    uiBlocker();
    if (dataTableId != null)
        dataTableId.destroy();
    $.get(baseUrl + '/api/Mapping/GetAllMappingDetailsForUsers', function (data, status) {
        if (status == "success") {
            var headers = [""], rows = {}
            headers = ["SubsyName", "TransactionType", "FileType", "MapperName", "DateAdded", "AddedBy", "Download"];            
            var rowHtml = '<thead><tr style="background:#599CDE !important;" ><th style="display:none">UniqueId</th><th>' + headers.join('</th><th>') + '</th></tr></thead>';
            $.each(data, function (clientIdx, item) {
                rowHtml += '<tr>';
                rowHtml += '<td style="display:none">' + item.UniqueId + '</td>'
                rowHtml += '<td>' + item.SubsyName + '</td>'
                rowHtml += '<td>' + item.TransactionType + '</td>'
                rowHtml += '<td>' + item.FileType + '</td>'
                rowHtml += '<td>' + item.MapperName + '</td>'
                rowHtml += '<td>' + item.DateAdded + '</td>'
                rowHtml += '<td>' + item.AddedBy + '</td>'
                var url = baseUrl + "/api/Mapping/DownloadMapperData?mapperName=" + item.MapperName;
                rowHtml += '<td><a class="btn btn-primary" value="Download" href="' + url + '" >Download</td>'
                rowHtml += '</tr>';
            });
            debugger;
            $('#tblMappingList').html(rowHtml);
            dataTableId = $('#tblMappingList').DataTable({
                "columns": [null, null,
                                { "width": "125px" },
                               null,
                               null,
                               null,
                               null,
                               null
                        ],
                "pageLength": 10,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50,200, "All"]]
            });
            // $('#dynatable-search-tblMappingList').html($('#dynatable-query-search-tblMappingList'));
            $.unblockUI();
        }
        else {
            $.unblockUI();
            noty({
                layout: 'top',
                type: 'error',
                text: "Error contacting server",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }
    });   
};

this.onTransactionReportClick = function () {
    $('#welcomeScreen').hide();
    $('#LineContent').hide();
    $('#userList').hide();
    $('#sscResult').hide();
    $('#roleList').hide();
    $('#UploadMapper').hide();
    $('#AddLookup').hide();
    $('#sapMapping').hide();
    $('#scMapping').hide();
    $('#asnMapping').hide();
    $('#subsiMapping').hide();
    $('#SO').hide();
    $('#scTable').hide();
    $("#asnTable").hide();
    $('#sscMapping').hide();
    $('#ViewLookup').hide();
    $('#scVerifyBtn').hide();
    $('#asnVerifyBtn').hide()
    $('#scResult').hide();
    $('#soResult').hide();
    $('#viewMapping').hide();
    $('#viewMappingDetails').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#viewConfig').hide();
    $('#configDelete').hide();
    $('#errorEmail').hide();
    $('#scannerRegst').hide();
    $('#kendoUISSC').hide();
    $('#reportDetails').show();
    $('#mappingDownloadList').hide();
    AppViewModel.uploadSCFileName('');
    AppViewModel.selectedMapSubsi('');
    AppViewModel.selectedFile('');
    AppViewModel.mappedSubsiData([]);
    AppViewModel.mappedFileData([]);
    
    
    //AppViewModel.transactionType(3);
    //AppViewModel.selectedTransType('ASN');
    AppViewModel.selectedMenu('Report');
};

var selectedData = '';

