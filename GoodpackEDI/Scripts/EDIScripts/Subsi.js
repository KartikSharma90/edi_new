﻿this.hqUserSubsiClick = function () {
    AppViewModel.users([]);
    AppViewModel.selectedUserId(0);
    AppViewModel.subsies([]);   
    AppViewModel.mappedSubsies([]);
    $('#hqUserSubsi').css({ 'background-color': '#004c97' });
    $('#hqAdminUserSubsi').css({ 'background-color': '#bababa' });
    $('#subsiUserSubsi').css({ 'background-color': '#bababa' });
    AppViewModel.subsiType(0);
    UserClick();
};

this.subsiUserSubsiClick = function () {
    AppViewModel.users([]);
    AppViewModel.selectedUserId(0)
    AppViewModel.subsies([]);
    AppViewModel.mappedSubsies([]);
    $('#hqUserSubsi').css({ 'background-color': '#bababa' });
    $('#hqAdminUserSubsi').css({ 'background-color': '#bababa' });
    $('#subsiUserSubsi').css({ 'background-color': '#004c97' });
    AppViewModel.subsiType(1);
    UserClick();
};
this.hqAdminUserClick = function () {
    AppViewModel.users([]);
    AppViewModel.selectedUserId(0)
    AppViewModel.subsies([]);
    AppViewModel.mappedSubsies([]);
    $('#hqUserSubsi').css({ 'background-color': '#bababa' });
    $('#subsiUserSubsi').css({ 'background-color': '#bababa' });
    $('#hqAdminUserSubsi').css({ 'background-color': '#004c97' });
    AppViewModel.subsiType(2);
    UserClick();
};
AppViewModel.selectedUser.subscribe(function (newValue) {
    if (newValue != "") {
        AppViewModel.selectedUserId(newValue[0].UserId);
        $.ajax({
            type: "GET",
            url: baseUrl + '/api/User/Subsies?userId=' + newValue[0].UserId + '&isMapping=false',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                AppViewModel.subsies([]);
                AppViewModel.subsies(data.SubsiList);
                AppViewModel.mappedSubsies([]);
                AppViewModel.mappedSubsies(data.MappedSubsi);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });
    }
});

function UserClick()
{
    $.ajax({
        type: "GET",
        url: baseUrl + '/api/User/AllUsers?roleId='+AppViewModel.subsiType(),
        data: ko.toJSON(AppViewModel.subsiType()),
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $.unblockUI();
            AppViewModel.users([]);
            AppViewModel.users(data)
            AppViewModel.selectedUserId(0)
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
}

this.rightSubsiArrow = function () {

    if (AppViewModel.selectedSubsies().length > 0) {
        if(((AppViewModel.subsiType()==1)&&(AppViewModel.mappedSubsies().length==0)) || (AppViewModel.subsiType()==0) || (AppViewModel.subsiType()==2))
        {
            AppViewModel.mappedSubsies.push({ SubsiId: AppViewModel.selectedSubsies()[0].SubsiId, SubsiName: AppViewModel.selectedSubsies()[0].SubsiName });
            AppViewModel.subsies.remove(AppViewModel.selectedSubsies()[0]);
            AppViewModel.selectedSubsies([]);
        }
        else
        {
            var loginAlert = noty({
                layout: 'top',
                type: 'error',
                text: "More than one mapping to Subsi user is restricted",
                dismissQueue: true,
                animation: {
                    open: { height: 'toggle' },
                    close: { height: 'toggle' },
                    easing: 'swing',
                    speed: 500
                },
                timeout: 5000
            });
        }        
    }

};

this.leftSubsiArrow = function () {
    if (AppViewModel.selectedMappedSubsies().length > 0) {
        AppViewModel.subsies.push({ SubsiId: AppViewModel.selectedMappedSubsies()[0].SubsiId, SubsiName: AppViewModel.selectedMappedSubsies()[0].SubsiName });
        AppViewModel.mappedSubsies.remove(AppViewModel.selectedMappedSubsies()[0]);
        AppViewModel.selectedMappedSubsies([]);
    }
};


this.subsiSaveClick = function () {
    if (AppViewModel.mappedSubsies().length > 0) {
        uiBlocker();
        $.ajax({
            type: "POST",
            url: baseUrl + '/api/User/SaveSubsi?userId=' + AppViewModel.selectedUserId(),
            data: ko.toJSON(AppViewModel.mappedSubsies()),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $.unblockUI();
                if (data.Success) {
                    var mappingAlert = noty({
                        layout: 'top',
                        type: 'success',
                        text: "Subsi mapping successfull",
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                    AppViewModel.subsies([]);
                    AppViewModel.mappedSubsies([]);
                }
                else {
                    var subsiAlert = noty({
                        layout: 'top',
                        type: 'error',
                        text: data.Message,
                        dismissQueue: true,
                        animation: {
                            open: { height: 'toggle' },
                            close: { height: 'toggle' },
                            easing: 'swing',
                            speed: 500
                        },
                        timeout: 5000
                    });
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $.unblockUI();
            }
        });
    }
}


