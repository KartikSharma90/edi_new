this.onErrorEmailClick = function () {
     $('#welcomeScreen').hide();
     $('#LineContent').hide();
     $('#userList').hide();
     $('#UploadMapper').hide();
     $('#sscResult').hide();
     $('#subsiMapping').hide();
     $('#roleList').hide();
     $('#SO').hide();
     $('#AddnewSO').hide();
     $('#AddLookup').hide();
     $('#ViewLookup').hide();
     $('#UploadSO').hide();
     $('#scMapping').hide();
     $('#viewSO').hide();
     $("#sscMapping").hide();
     $('#scResult').hide();
     $('#soResult').hide();
     $('#sapMapping').hide();
     $('#LineContent').hide();
     $('#viewMapping').hide();
     $('#viewMappingDetails').hide();
     $('#viewConfig').hide();
     $('#configDelete').hide();
     $('#viewConfig').hide();
     $('#asnVerifyBtn').hide();
     $('#asnMapping').hide();
     $('#configDelete').hide();
     $("#EditSO").hide();
     $("#CancelSO").hide();
     $('#errorEmail').show();
     $('#kendoUISSC').hide();
     $('#scannerRegst').hide();
     $('#reportDetails').hide();
     AppViewModel.selectedMenu('Error Emails');
     $('#EmailErrorGrid').jqGrid('GridUnload');
     $("#EmailErrorGrid").jqGrid({
 
         url: baseUrl + '/api/Transaction/ErrorEmailDetails',
         datatype: "json",
         myType: "GET",
         colNames: ['Tracker Id','Subsy Name', 'Email Id', 'Transaction', 'File Name', 'File Content', 'Mapper Name' ,'Email Received DateTime','Error Message','Actions'],
         colModel: [
         { name: 'UniqueId', index: 'UniqueId', align: 'center' },
         { name: 'SubsyName', index: 'SubsyId', align: 'center' },
         { name: 'EmailId', index: 'EmailId', align: 'center' },
         { name: 'TransactionName', index: 'TransactionName', align: 'center' },
         { name: 'FileName', index: 'FileName', align: 'center' },
         { name: 'FileContent', index: 'FileContent', align: 'center', hidden: true },
         { name: 'MapperName', index: 'FileName', align: 'center' },
         { name: 'CreatedDate', index: 'CreatedDate', align: 'center' },
         { name: 'ErrorMessage', index: 'ErrorMessage', align: 'center', hidden: true },
          { name: 'act', index: 'act', align: 'center', sortable: false, width: '200px' }
         ],
         jsonReader: {
             root: 'ErrorEmailData',
             id: 'UniqueId',
             total: 'totalpages',
             records: 'totalrecords',
             repeatitems: true,
         },
         pager: $('#EmailerrorPager'),
         excel: true,
         rowNum: 15,
         rowList: [15, 50, 100, 200, 5000],
         autowidth: true,
         shrinkToFit: true,
         sortorder: "desc",
         viewrecords: true,
         ////commented for servier side pagination  loadonce: true,
         autoencode: false,
         height: 'auto',
         width: '100%',
         caption: "Error Emails Details",
         loadComplete: function (data) {
 
             debugger
             $("tr.jqgrow:odd").css("background", "#f1f1f1");
             var ids = $("#EmailErrorGrid").jqGrid('getDataIDs');
             for (var i = 0; i < ids.length; i++) {
                 var rowId = ids[i];
                 se = "<input style='height:22px;width:80px;background-color:#004c97;color:#ffffff;border-radius:5px; 'type='button' Id='" + rowId + "error" + "' value='Error Details' onclick=\"ErrordetailsButtonClick('" + rowId + "')\"  />";
                 ce= "<input style='height:22px;width:80px;background-color:#004c97;color:#ffffff;border-radius:5px; 'type='button' Id='" + rowId + "content" + "' value='File Content' onclick=\"FileContentClick('" + rowId + "')\"  />";
                 $("#EmailErrorGrid").jqGrid('setRowData', ids[i], { act: se + ce} )
             }
                  
                     var msg2 = noty({
                         layout: 'top',
                         type: 'success',
                         text: 'Transaction Details Generated.',
                         dismissQueue: true,
                         animation: {
                             open: { height: 'toggle' },
                             close: { height: 'toggle' },
                             easing: 'swing',
                             speed: 500
                         },
                         timeout: 5000
                     });
 
         
             
         },
 
 
     });
 
     $('#EmailErrorGrid').jqGrid('navGrid', '#EmailerrorPager', {
         edit: false, view: false, add: false, del: false, search: true,
         beforeRefresh: function () {
             //alert('In beforeRefresh');
             $('#EmailErrorGrid').jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid');
         }
     }, {}, {}, {}, { closeAfterSearch: true }, {});
 
 
 }
 function ErrordetailsButtonClick(rowId)
 {
     debugger;
     var selectedRowData = $("#EmailErrorGrid").getRowData(rowId);
    
         noty({
             layout: 'top',
             type: 'warning',
             text: "" + selectedRowData.ErrorMessage,
             buttons: [
                     {
                         addClass: 'btn btn-primary', text: 'Close', type: 'success', onClick: function ($noty) {
                             $noty.close();
                         }
                     }
             ]
         });
    
 
     //var errorDetails = noty({
     //    layout: 'top',
     //    type: 'success',
     //    text: selectedRowData.ErrorMessage,
     //    dismissQueue: true,
     //    animation: {
     //        open: { height: 'toggle' },
     //        close: { height: 'toggle' },
     //        easing: 'swing',
     //        speed: 500
     //    },
     //    timeout: 5000
     //});
 
 }
 
 function FileContentClick(rowId) {   
     var selectedRowData = $("#EmailErrorGrid").getRowData(rowId);
     $('#dataTable').html('');
     function rowBuilder(e) {
         var rowContent = e.split("\n");
         var tableString = "<table style='border-collapse:collapse;'>";
         for (var i = 0; i < rowContent.length-1; i++)
         {
             var columnContent = rowContent[i].split('|');
             if (i == 0)
                 tableString = tableString + '<tr style="height:30px;background-color:#c2ddb6;">';
             else
                 tableString = tableString + '<tr style="height:30px;">';
             for(var j=0;j<columnContent.length;j++)
             {
                 if (i == 0)
                     tableString = tableString + '<td style="border:1px solid gray;white-space: nowrap;min-width:100px;">' + columnContent[j] + '</td>';
                 else
                     tableString = tableString + '<td style="border:1px solid #d4d4d4;white-space: nowrap;min-width:100px;">' + columnContent[j] + '</td>';
             }
             tableString = tableString + '</tr>'
         }
         tableString = tableString + "</table>";       
         return tableString;
     }
     $('#dataTable').append(rowBuilder(selectedRowData.FileContent));
     $('#errorfileContentDiv').dialog({
         title: "File Content",
         height: 550, zIndex: 50000,
         width: 800,
         modal: true
     });
 }

GO 

