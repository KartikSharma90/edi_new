﻿
namespace GoodpackEDI.ConstantUtils
{
    public enum EnumAppPrevileges
    {
        Administrator,        
        ShipmentConfirmation,
        SubsiMapping,
        SalesOrder,
        MappingConfiguration,
        DataLookUp       
     };
}