﻿using GoodpackEDI.BusinessLayer.SCManagementLayer;
using GoodpackEDI.Utilities;
using log4net;
using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace GoodpackEDI.Parsers
{
    public class GenericDelimiterParser
    {
        ///// <summary>
        ///// Performs CSV parsing on the message parameter based
        ///// on the provided message specifications.
        ///// </summary>
        ///// <param name="msgSpecs">Message specification</param>
        ///// <param name="message">Message to parse</param>
        ///// <param name="isSuccess"></param>
        ///// <param name="errorDesc"></param>
        private static readonly ILog log = LogManager.GetLogger(typeof(GenericDelimiterParser));

        public static List<SourceMessage> Parse(SourceMessage msgSpecs, string message, bool hasHeaders, char delimiter, ref bool isSuccess, ref string errorDesc)
        {

            // Init vars
            isSuccess = true;
            int lineCount = 0;
            int ctr;
            List<SourceMessage> parsedResult = new List<SourceMessage>();


            try
            {
                // Initialize csv reader object
                //CsvReader csvReader = new CsvReader(
                //    new StringReader(message)
                //    ,hasHeaders,delimiter);
                CsvReader csvReader = new CsvReader(
                    new StringReader(message)
                    , false, delimiter);


                // Iterate through each lines in customer message
                while (csvReader.ReadNextRecord())
                {
                    string msgLine = "";
                    // If isSuccess is false, then end processing
                    if (!isSuccess)
                        break;

                    // Verify that all fields in the CSV line have values.  If not, then skip.
                    //bool csvContainsFieldValue=false;
                    int iContainsFieldCount = 0;
                    lineCount++;

                    for (ctr = 0; ctr < csvReader.FieldCount; ctr++)
                    {
                        //Modified to count number of non-empty fields
                        if (!string.IsNullOrEmpty(csvReader[ctr]))
                        {
                            //csvContainsFieldValue = true;
                            iContainsFieldCount++;
                            //break;
                        }
                    }


                    //if (!csvContainsFieldValue)
                    //    continue;

                    //If number of non-empty fields is less than a minimum configured number, ignore the record
                    if (iContainsFieldCount < msgSpecs.MinFieldValCount)
                        continue;

                    // Increment line count


                    // Verify the number of fields derived from a message line
                    // against the expected number of fields
                    if (csvReader.FieldCount < msgSpecs.Fields.Count)
                    {
                        isSuccess = false;
                        errorDesc = string.Format(ResourceMessages.FIELD_COUNT_TOO_LOW, lineCount);
                        break;
                    }
                    // Check for parsing error
                    if (csvReader.ParseErrorFlag)
                    {
                        isSuccess = false;
                        errorDesc = string.Format(
                            ResourceMessages.CSV_PARSING_ERROR,
                            csvReader.CurrentRecordIndex.ToString()
                            );
                        break;
                    }

                    // Init vars
                    SourceMessage msgLineObj = new SourceMessage(msgSpecs);

                    if (hasHeaders == true && lineCount == 1)
                    {
                        continue;
                    }
                    ctr = 0;

                    // Iterate through each field in the source message line and assign value
                    foreach (KeyValuePair<int, SourceMessageField> lineFieldItem in msgLineObj.Fields)
                    {
                        // Check if a field in the input CSV file is missing
                        if (csvReader.MissingFieldFlag)
                        {
                            isSuccess = false;
                            errorDesc = ResourceMessages.CSV_FILE_INCONSISTENT;
                            break;
                        }

                        SourceMessageField lineField = lineFieldItem.Value;
                        lineField.Value = csvReader[ctr];
                        msgLine += lineField.Value + "" + delimiter;
                        // increment counter
                        ctr++;

                    }
                    msgLineObj.OriginalLineContent = msgLine;

                    // Add message line object to list
                    parsedResult.Add(msgLineObj);
                }
            }
            catch (LumenWorks.Framework.IO.Csv.MalformedCsvException ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ".  CSVException : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ".  Exception : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = "An error was encountered with the GenericDelimiter parser.  " + ex.Message;
            }


            return parsedResult;
        }

        /// <summary>
        /// Performs CSV parsing on the message parameter based
        /// on the provided message specifications.
        /// </summary>
        /// <param name="msgSpecs">Message specification</param>
        /// <param name="message">Message to parse</param>
        /// <param name="isSuccess"></param>
        /// <param name="errorDesc"></param>
        public static List<SourceMessage> Parse(SourceMessage msgSpecs,string path, string message, bool hasHeaders, char delimiter,string fileName, ref bool isSuccess, ref string errorDesc)
        {

            // Init vars
            isSuccess = true;
            int lineCount = 0;
            int ctr;
            List<SourceMessage> parsedResult = new List<SourceMessage>();


            try
            {
                // Initialize csv reader object
                //CsvReader csvReader = new CsvReader(
                //    new StringReader(message)
                //    ,hasHeaders,delimiter);
                
                string filePath = Path.Combine(path, fileName);
                string messageVal = ExcelParser.ExcelToCSV(filePath,0,0);//InteropExcelParser.ExcelParser(filePath);
                string[] messageData = messageVal.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                
                SourceMessage msgLineObj = new SourceMessage(msgSpecs);
                for (int i = 1; i < messageData.Length; i++)
                {
                    msgLineObj.OriginalLineContent = messageData[i];
                    parsedResult.Add(msgLineObj);
                }
                isSuccess = true;


                //// Iterate through each lines in customer message
                //while (csvReader.ReadNextRecord())
                //{
                //    string msgLine = "";
                //    // If isSuccess is false, then end processing
                //    if (!isSuccess)
                //        break;

                //    // Verify that all fields in the CSV line have values.  If not, then skip.
                //    //bool csvContainsFieldValue=false;
                //    int iContainsFieldCount = 0;
                //    lineCount++;

                //    for (ctr = 0; ctr < csvReader.FieldCount; ctr++)
                //    {
                //        //Modified to count number of non-empty fields
                //        if (!string.IsNullOrEmpty(csvReader[ctr]))
                //        {
                //            //csvContainsFieldValue = true;
                //            iContainsFieldCount++;
                //            //break;
                //        }
                //    }


                //    //if (!csvContainsFieldValue)
                //    //    continue;

                //    //If number of non-empty fields is less than a minimum configured number, ignore the record
                //    if (iContainsFieldCount < msgSpecs.MinFieldValCount)
                //        continue;

                //    // Increment line count


                //    // Verify the number of fields derived from a message line
                //    // against the expected number of fields
                //    if (csvReader.FieldCount < msgSpecs.Fields.Count)
                //    {
                //        isSuccess = false;
                //        errorDesc = string.Format(ResourceMessages.FIELD_COUNT_TOO_LOW, lineCount);
                //        break;
                //    }
                //    // Check for parsing error
                //    if (csvReader.ParseErrorFlag)
                //    {
                //        isSuccess = false;
                //        errorDesc = string.Format(
                //            ResourceMessages.CSV_PARSING_ERROR,
                //            csvReader.CurrentRecordIndex.ToString()
                //            );
                //        break;
                //    }

                //    // Init vars
                //    SourceMessage msgLineObj = new SourceMessage(msgSpecs);

                //    if (hasHeaders == true && lineCount == 1)
                //    {
                //        continue;
                //    }
                //    ctr = 0;

                //    // Iterate through each field in the source message line and assign value
                //    foreach (KeyValuePair<int, SourceMessageField> lineFieldItem in msgLineObj.Fields)
                //    {
                //        // Check if a field in the input CSV file is missing
                //        if (csvReader.MissingFieldFlag)
                //        {
                //            isSuccess = false;
                //            errorDesc = ResourceMessages.CSV_FILE_INCONSISTENT;
                //            break;
                //        }

                //        SourceMessageField lineField = lineFieldItem.Value;
                //        lineField.Value = csvReader[ctr];
                //        msgLine += lineField.Value + "" + delimiter;
                //        // increment counter
                //        ctr++;

                //    }
                //    msgLineObj.OriginalLineContent = msgLine;

                //    // Add message line object to list
                //    parsedResult.Add(msgLineObj);
                //}
            }
            catch (LumenWorks.Framework.IO.Csv.MalformedCsvException ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ", path:" + path + ", filename:" + fileName + ".  CSVException : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ", path:" + path + ", filename:" + fileName + ".  Exception : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = "An error was encountered with the GenericDelimiter parser.  " + ex.Message;
            }


            return parsedResult;
        }
        public static DataTable Parse(string csvString, bool hasHeaders, bool trimFieldValues, char delimiter, ref bool isSuccess, ref string errorDesc)
        {
            // Init vars
            DataTable returnVal = new DataTable();
            isSuccess = true;
            int lineCount = 0;

            try
            {
                // Initialize csv reader object
                CsvReader csvReader = new CsvReader(
                    new StringReader(csvString),
                    hasHeaders, delimiter);

                // Iterate through each lines in customer message
                while (csvReader.ReadNextRecord())
                {
                    // If isSuccess is false, then end processing
                    if (!isSuccess)
                        break;

                    lineCount++;

                    // Check for parsing error
                    if (csvReader.ParseErrorFlag)
                    {
                        isSuccess = false;
                        errorDesc = string.Format(
                            ResourceMessages.CSV_PARSING_ERROR,
                            csvReader.CurrentRecordIndex.ToString()
                            );
                        break;
                    }

                    // Set header name of data table. If hasHeaders is true, header names will be found on line 1
                    if (hasHeaders && lineCount == 1)
                    {
                        string[] fieldHeaders = csvReader.GetFieldHeaders();
                        foreach (string fieldHeader in fieldHeaders)
                        {
                            returnVal.Columns.Add(fieldHeader);
                        }
                    }


                    // Get fields in CSV record and store into array
                    string[] fieldList = new string[csvReader.FieldCount];
                    for (int ctr = 0; ctr < csvReader.FieldCount; ctr++)
                    {
                        if (trimFieldValues)
                        {
                            fieldList[ctr] = csvReader[ctr].Trim();
                        }
                        else
                        {
                            fieldList[ctr] = csvReader[ctr];
                        }
                    }

                    // Apply CSV record into Datatable
                    returnVal.Rows.Add(fieldList);
                }


            }
            catch (LumenWorks.Framework.IO.Csv.MalformedCsvException ex)
            {
                log.Error(" Parse in GenericDelimiterParser .csvString:" + csvString + ", errorDesc:" + errorDesc + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ", trimFieldValues:" + trimFieldValues + ".  CSVException : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                log.Error(" Parse in GenericDelimiterParser .csvString:" + csvString + ", errorDesc:" + errorDesc + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ", trimFieldValues:" + trimFieldValues + ".  Exception : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = "An error was encountered with the GenericDelimiter parser.  " + ex.Message;
            }

            return returnVal;
        }

        public static List<SourceMessage> YokohamaParse(SourceMessage msgSpecs, string message, bool hasHeaders, char delimiter, ref bool isSuccess, ref string errorDesc)
        {

            // Init vars
            log.Debug("Begin Yokohama_Custom Parse");
            bool headerReached = false;
            bool isheader = false;
            int ctr;
            isSuccess = true;
            StringBuilder batchMessage = new StringBuilder();
            StringBuilder batchMessagereturn = new StringBuilder();
            List<SourceMessage> parsedResult = new List<SourceMessage>();
            Dictionary<string, string> SINumberKeys = new Dictionary<string, string>();
            Dictionary<string, string> PlantKeys = new Dictionary<string, string>();
            Dictionary<string, string> FillerKeys = new Dictionary<string, string>();
           // IList<YokohamaGenValue> returnValue;


            try
            {
                // Initialize csv reader object
                //CsvReader csvReader = new CsvReader(
                //    new StringReader(message)
                //    ,hasHeaders,delimiter);
                CsvReader csvReader = new CsvReader(
                    new StringReader(message)
                    , false, ',');


                // Iterate through each lines in customer message
                while (csvReader.ReadNextRecord())
                {
                    string msgLine = "";
                    // If isSuccess is false, then end processing
                    if (!isSuccess)
                        break;

                    int iContainsFieldCount = 0;
                    for (ctr = 0; ctr < csvReader.FieldCount; ctr++)
                    {
                        //Modified to count number of non-empty fields
                        if (!string.IsNullOrEmpty(csvReader[ctr]))
                        {
                            //csvContainsFieldValue = true;
                            iContainsFieldCount++;
                            //break;
                        }
                    }

                    //If number of non-empty fields is less than a minimum configured number, ignore the record
                    // If field count is greater than 6 ,then check if it's the field header row
                    if (iContainsFieldCount < 10)
                    {
                        continue;
                    }
                    else if (headerReached == false)
                    {
                        if ((!string.IsNullOrEmpty(csvReader[0]) && csvReader[0].Trim().ToUpper() == "NEW CONT NO. - YRS")
                            && (!string.IsNullOrEmpty(csvReader[1]) && csvReader[1].Trim().ToUpper() == "SEQ.NO.")
                            && (!string.IsNullOrEmpty(csvReader[2]) && csvReader[2].Trim().ToUpper() == "CONT.NO.")
                            && (!string.IsNullOrEmpty(csvReader[3]) && csvReader[3].Trim().ToUpper() == "SEQ.NO.")
                            && (!string.IsNullOrEmpty(csvReader[4]) && csvReader[4].Trim().ToUpper() == "SHIPMENT")
                            && (!string.IsNullOrEmpty(csvReader[5]) && csvReader[5].Trim().ToUpper() == "MONTH")
                            && (!string.IsNullOrEmpty(csvReader[6]) && csvReader[6].Trim().ToUpper() == "GRADE")
                            )
                        {
                            headerReached = true; isheader = true;

                        }
                    }

                    // Check for parsing error
                    if (csvReader.ParseErrorFlag)
                    {
                        isSuccess = false;
                        break;
                    }
                   
                        // Init vars
                        SourceMessage msgLineObj = new SourceMessage();
                        msgLineObj.Fields = new Dictionary<int, SourceMessageField>();
                        // Init fields
                        msgLineObj.Fields.Add(0, new SourceMessageField("NEW CONT NO. - YRS", 100));
                        msgLineObj.Fields.Add(1, new SourceMessageField("SEQ.NO.", 100));
                        msgLineObj.Fields.Add(2, new SourceMessageField("CONT.NO.", 100));
                        msgLineObj.Fields.Add(3, new SourceMessageField("SEQ.NO.", 100));
                        msgLineObj.Fields.Add(4, new SourceMessageField("SHIPMENT", 100));
                        msgLineObj.Fields.Add(5, new SourceMessageField("MONTH", 100));
                        msgLineObj.Fields.Add(6, new SourceMessageField("GRADE", 100));
                        msgLineObj.Fields.Add(7, new SourceMessageField("CODE NO.", 100));
                        msgLineObj.Fields.Add(8, new SourceMessageField("SUPPLIER", 100));
                        msgLineObj.Fields.Add(9, new SourceMessageField("FACTORY", 100));
                        msgLineObj.Fields.Add(10, new SourceMessageField("PACKING", 100));
                        msgLineObj.Fields.Add(11, new SourceMessageField("QTY (KG)", 100));
                        msgLineObj.Fields.Add(12, new SourceMessageField("MB units", 100));
                        msgLineObj.Fields.Add(13, new SourceMessageField("FOB", 100));
                        msgLineObj.Fields.Add(14, new SourceMessageField("CARRIER", 100));
                        msgLineObj.Fields.Add(15, new SourceMessageField("1ST VSSL.", 100));
                        msgLineObj.Fields.Add(16, new SourceMessageField("ETD", 100));
                        msgLineObj.Fields.Add(17, new SourceMessageField("Destn", 100));
                        msgLineObj.Fields.Add(18, new SourceMessageField("ETA", 100));
                        msgLineObj.Fields.Add(19, new SourceMessageField("remark", 100));
                        msgLineObj.Fields.Add(20, new SourceMessageField("SI Number", 100));
                        msgLineObj.Fields.Add(21, new SourceMessageField("Plant", 100));
                        msgLineObj.Fields.Add(22, new SourceMessageField("Filler", 100));

                        // Map field values
                        msgLineObj.Fields[0].Value = (string.IsNullOrEmpty(csvReader[0])) ? string.Empty : csvReader[0];
                        msgLineObj.Fields[1].Value = (string.IsNullOrEmpty(csvReader[1])) ? string.Empty : csvReader[1];
                        msgLineObj.Fields[2].Value = (string.IsNullOrEmpty(csvReader[2])) ? string.Empty : csvReader[2];
                        msgLineObj.Fields[3].Value = (string.IsNullOrEmpty(csvReader[3])) ? string.Empty : csvReader[3];
                        msgLineObj.Fields[4].Value = (string.IsNullOrEmpty(csvReader[4])) ? string.Empty : csvReader[4];
                        msgLineObj.Fields[5].Value = (string.IsNullOrEmpty(csvReader[5])) ? string.Empty : csvReader[5];
                        msgLineObj.Fields[6].Value = (string.IsNullOrEmpty(csvReader[6])) ? string.Empty : csvReader[6];
                        msgLineObj.Fields[7].Value = (string.IsNullOrEmpty(csvReader[7])) ? string.Empty : csvReader[7];
                        msgLineObj.Fields[8].Value = (string.IsNullOrEmpty(csvReader[8])) ? string.Empty : csvReader[8];
                        msgLineObj.Fields[9].Value = (string.IsNullOrEmpty(csvReader[9])) ? string.Empty : csvReader[9];
                        msgLineObj.Fields[10].Value = (string.IsNullOrEmpty(csvReader[10])) ? string.Empty : csvReader[10];
                        msgLineObj.Fields[11].Value = (string.IsNullOrEmpty(csvReader[11])) ? string.Empty : csvReader[11];
                        msgLineObj.Fields[12].Value = (string.IsNullOrEmpty(csvReader[12])) ? string.Empty : csvReader[12];
                        msgLineObj.Fields[13].Value = (string.IsNullOrEmpty(csvReader[13])) ? string.Empty : csvReader[13];
                        msgLineObj.Fields[14].Value = (string.IsNullOrEmpty(csvReader[14])) ? string.Empty : csvReader[14];
                        msgLineObj.Fields[15].Value = (string.IsNullOrEmpty(csvReader[15])) ? string.Empty : csvReader[15];
                        msgLineObj.Fields[16].Value = (string.IsNullOrEmpty(csvReader[16])) ? string.Empty : csvReader[16];
                        msgLineObj.Fields[17].Value = (string.IsNullOrEmpty(csvReader[17])) ? string.Empty : csvReader[17];
                        msgLineObj.Fields[18].Value = (string.IsNullOrEmpty(csvReader[18])) ? string.Empty : csvReader[18];
                        msgLineObj.Fields[19].Value = (string.IsNullOrEmpty(csvReader[19])) ? string.Empty : csvReader[19];

                        // Map SI Number
                        if (msgLineObj.Fields[2].Value.Trim().ToUpper() == "CONT.NO.")
                        {
                            msgLineObj.Fields[19].Value = "SI#"; // Column name
                        }
                        if (string.IsNullOrEmpty(msgLineObj.Fields[1].Value))
                        {
                            msgLineObj.Fields[20].Value = msgLineObj.Fields[0].Value;
                        }
                        else if (msgLineObj.Fields[1].Value.ToUpper().StartsWith("IPO")
                            || msgLineObj.Fields[1].Value.ToUpper().StartsWith("IP0")) // if starts with IPO, trim IPO part
                        {
                            msgLineObj.Fields[20].Value = msgLineObj.Fields[0].Value + "-" + msgLineObj.Fields[1].Value.Substring(3, msgLineObj.Fields[1].Value.Length - 3);
                        }
                        else
                        {
                            msgLineObj.Fields[20].Value = msgLineObj.Fields[0].Value + "-" + msgLineObj.Fields[1].Value;
                        }

                        // Map Plant
                        if (msgLineObj.Fields[2].Value.Trim().ToUpper() == "CONT.NO.")
                        {
                            msgLineObj.Fields[21].Value = "PLANT";
                        }

                        else
                        {
                            foreach (KeyValuePair<string, string> keyvalue in PlantKeys)
                            {
                                if (msgLineObj.Fields[2].Value.ToUpper().StartsWith(keyvalue.Key))
                                {
                                    msgLineObj.Fields[21].Value = keyvalue.Value;
                                    break;
                                }
                                else
                                {
                                    msgLineObj.Fields[21].Value = string.Empty;
                                }
                            }
                        }

                        // Map empty string to filler field
                        if (msgLineObj.Fields[2].Value.Trim().ToUpper() == "CONT.NO.")
                        {
                            msgLineObj.Fields[22].Value = "FILLER";
                        }
                        else
                        {
                            msgLineObj.Fields[22].Value = string.Empty;
                        }
                        if (!isheader)
                        {                            
                            
                            foreach (KeyValuePair<int, SourceMessageField> lineFieldItem in msgLineObj.Fields)
                            {
                                // Check if a field in the input CSV file is missing
                                if (csvReader.MissingFieldFlag)
                                {
                                    isSuccess = false;
                                    errorDesc = ResourceMessages.CSV_FILE_INCONSISTENT;
                                    break;
                                }

                                SourceMessageField lineField = lineFieldItem.Value;                              
                                msgLine += lineField.Value + "" + delimiter;
                                // increment counter
                                ctr++;

                            }
                            msgLineObj.OriginalLineContent = msgLine;
                            // Add message line object to list
                            parsedResult.Add(msgLineObj);
                        }
                        isheader = false;
                    }
                }            
            catch (LumenWorks.Framework.IO.Csv.MalformedCsvException ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ".  CSVException : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = ResourceMessages.GENERAL_PARSING_ISSUE;
            }
            catch (System.Exception ex)
            {
                log.Error(" Parse in GenericDelimiterParser .msgSpecs:" + msgSpecs + ", message:" + message + ",isSuccess:" + isSuccess + ",errorDesc:" + errorDesc + ", hasHeaders:" + hasHeaders + ", delimiter:" + delimiter + ".  Exception : Message- " + ex.Message);
                isSuccess = false;
                errorDesc = "An error was encountered with the GenericDelimiter parser.  " + ex.Message;
            }


            return parsedResult;
        }
    }
}