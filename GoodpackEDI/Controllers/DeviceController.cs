﻿using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.DeviceManagerLayer;
using GPArchitecture.GoodPackBusinessLayer.UserManagementLayer;
using GPArchitecture.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GoodpackEDI.Controllers
{
    public class DeviceController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(DeviceController));
        private IPrevilegeManager previlegeManager = new PrevilegeManager();
        IDeviceManager deviceManager = new DeviceManager();
        private ResponseModel responseModel = new ResponseModel();

        [HttpPost]
        [Authorize]
        public object GetDeviceDetails(int jtStartIndex, int jtPageSize)
        {
            try
            {
                List<DeviceList> deviceList = deviceManager.GetDeviceList();
                return new { Result = "OK", Records = deviceList, TotalRecordCount = deviceList.Count };
            }
            catch (Exception e)
            {
                return new { Result = "ERROR", Message = "Not Authorized to do this action" };
            }
        }

        [HttpPost]
        [Authorize]
        public object Create(ScanerDetail jtableModel)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    if (ModelState.IsValid)
                    {
                        responseModel = deviceManager.CreateDevice(jtableModel.record, User.Identity.Name);
                        bool isTrue=responseModel.Success.HasValue ? (bool)responseModel.Success :false;
                        if (isTrue)
                        {
                            return new { Result = "OK", Record = jtableModel };
                        }
                        else
                        {
                            return new { Result = "ERROR", Message = responseModel.Message };

                        }
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Registration is incomplete.All fields are mandatory";
                    }
                    return responseModel;
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception e)
            {
                log.Error("Register Api Exception:-Message of Create method in DeviceController:" + e.Message + "jtableModel:" + jtableModel);
                responseModel.Success = false;
                responseModel.Message = "Creation Incomplete.Unable to process your request.";
                return responseModel;
            }
        }

        [HttpPost]
        [Authorize]
        public object Update(ScanerDetail jtableModel)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    if (ModelState.IsValid)
                    {
                        responseModel = deviceManager.UpdateDevice(jtableModel.record, User.Identity.Name);
                        bool isTrue = responseModel.Success.HasValue ? (bool)responseModel.Success : false;
                        if (isTrue)
                        {
                            return new { Result = "OK", Record = jtableModel };
                        }
                        else
                        {
                            return new { Result = "ERROR", Message = responseModel.Message };

                        }
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Update is incomplete.";
                    }
                    return responseModel;
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception e)
            {
                log.Error("Register Api Exception:-Message of Update method in DeviceController:" + e.Message + "jtableModel:" + jtableModel);
                responseModel.Success = false;
                responseModel.Message = "Update Incomplete.Unable to process your request.";
                return responseModel;
            }
        }

        [HttpPost]
        [Authorize]
        public object Delete(DeviceList detail)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    if (ModelState.IsValid)
                    {
                        responseModel = deviceManager.DeleteDevice(detail.Id, User.Identity.Name);
                        bool isTrue = responseModel.Success.HasValue ? (bool)responseModel.Success : false;
                        if (isTrue)
                        {
                            return new { Result = "OK", Record = "Success" };
                        }
                        else
                        {
                            return new { Result = "ERROR", Message = responseModel.Message };

                        }
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Delete is incomplete.";
                    }
                    return responseModel;
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception e)
            {
                log.Error("Register Api Exception:-Message of Delete method in DeviceController:" + e.Message + "jtableModel Id-:" + detail.Id);
                responseModel.Success = false;
                responseModel.Message = "Delete Incomplete.Unable to process your request.";
                return responseModel;
            }

        }

        [HttpPost]        
        public List<ShipmentResponse> ShipmentData(ShipmentData shipmentdata)
        {
            List<ShipmentResponse> response = new List<ShipmentResponse>();
            ShipmentResponse responseValue = new ShipmentResponse();
            try
            {
                if (deviceManager.DeviceRegistered(shipmentdata))
                {
                    response = deviceManager.ShipmentData(shipmentdata);
                }
                else
                {                    
                    responseValue.CustomerReference = null;
                    responseValue.Status = false;
                    response.Add(responseValue);
                }
            }
            catch(Exception e)
            {
                log.Error("Register Api Exception:-Message of ShipmentData method in DeviceController:" + e.Message + "ShipmentData:" + shipmentdata);
                responseValue.CustomerReference = null;
                responseValue.Status = false;
                response.Add(responseValue);
            }
            return response;
        }

        [HttpPost]        
        public RegisterScannerResponse ScannerRegisteration(ShipmentData shipmentdata)
        {
            RegisterScannerResponse scannerResponse = new RegisterScannerResponse();
            try
            {
                if (deviceManager.DeviceRegistered(shipmentdata))
                {
                    return deviceManager.RegisterScanner(shipmentdata);
                }
                else
                {
                    deviceManager.SendEmailToGoodPackAdmin(shipmentdata.DeviceId);
                    scannerResponse.status = false;
                    scannerResponse.message = "Please contact support@goodpack.com ";
                    return scannerResponse;
                }
            }
            catch (Exception e)
            {
                scannerResponse.status = false;
                scannerResponse.message = "Api Excetion ,ScannerRegisteration";
                log.Error("Register Api Exception:-Message of ScannerRegisteration method in DeviceController:" + e.Message + "ShipmentData:" + shipmentdata);
                return scannerResponse;
            }
        } 
        
        [HttpPost]
        [Authorize]
        public object GetVerticalData()
        {
            return new { Result = "OK", Options = deviceManager.GetVerticalData() }; 
        }

        [HttpGet]
        [Authorize]
        public bool ResubmitErrorData(int BatchId)
        {
            return deviceManager.ReSubmitErrorData(BatchId);
        }

        [HttpPost]
        [Authorize]
        public object GetDeviceTransactionType()
        {
            return new { Result = "OK", Options = deviceManager.GetDeviceTransactionType()}; 
        }

        [HttpPost]
        [Authorize]
        public object GetSubsyData()
        {
            return new { Result = "OK", Options = deviceManager.GetSubsyData() };
        }
    }
}
