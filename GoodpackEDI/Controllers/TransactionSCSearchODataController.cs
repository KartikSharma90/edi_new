﻿using GPArchitecture.GoodPackBusinessLayer.ReportManagerLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionSCSearchODataController : ODataController
    {
         private IReportManager reportManager = new ReportManager();
         [EnableQuery]
         public IQueryable<ReportSCTransactionModel> Get(int transactionId)
         {
             var responseData = reportManager.GetSCTransactionReport(transactionId, User.Identity.Name);
             return (from n in responseData
                     select new ReportSCTransactionModel
                     {
                         FileName = n.FileName,
                         UploadedDate = n.UploadedDate,
                         Batchid = n.Batchid,
                         CustomerCode = n.CustomerCode,
                         ReferenceNumber = n.ReferenceNumber,
                         packerLocation = n.packerLocation,
                         consigneeLocation = n.consigneeLocation,
                         BinType = n.BinType,
                         Mode = n.Mode,                         
                         Qty = n.Qty,                         
                         ETA = n.ETA,
                         UniqueId=n.UniqueId,
                         Status=n.Status,
                         ETD = n.ETD
                     }).AsQueryable();
         }
    }
}
