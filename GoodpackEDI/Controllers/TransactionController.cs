﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;

namespace GoodpackEDI.Controllers
{
    public class TransactionController : ApiController
    {

        private ITransactionManager transactionManager = new TransactionManager();
        private static readonly ILog log = LogManager.GetLogger(typeof(TransactionController));
        [HttpGet]
        [Authorize]
        public JQGridModel SCTransactionDetails(int subsyCode, int transactionCode, int page, int rows, string sidx, string sord, bool _search)
        {
            try
            {              
                SCTransBatchReturnModel responseData = transactionManager.GetSCTransBatchDetails(User.Identity.Name, subsyCode, transactionCode, page, rows);
                int totalPages = (int)Math.Ceiling((float)responseData.totalRecordCount / (float)rows);
                //if (_search)
                //    responseData = transactionManager.GetSearchField(User.Identity.Name, subsyCode, transactionCode, page, rows);

               
                return new JQGridModel
                {
                    TransactionData = responseData.TransBatchModel,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus=true,
                    totalrecords=responseData.totalRecordCount,
                    totalpages = totalPages,
                    S_errMsg = "Success"

                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SCTransactionDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TransactionData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg="Unable to process your request.Please try again"
                };

            }
        }
        [HttpGet]
        [Authorize]
        public JQGridModel ErrorEmailDetails(int page, int rows, string sidx, string sord, bool _search)
        {
            try
            {
                ErrorEmailReturnModel responseData = transactionManager.GetEmailErrorDetails(User.Identity.Name, page, rows);
                int totalPages = (int)Math.Ceiling((float)responseData.totalRecordCount / (float)rows);   

                return new JQGridModel
                {
                    ErrorEmailData = responseData.EmailBatchModel,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    totalrecords = responseData.totalRecordCount,
                    totalpages = totalPages,
                    S_errMsg = "Success"

                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-ErrorEmailDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    ErrorEmailData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
       
        [HttpGet]
        [Authorize]
        public JQGridModel SSCTransactionLineDetails(int transactionId, int page, int rows, string sidx, string sord)
        {
            try
            {
                List<SSCTransLineModel> responseData = transactionManager.GetSSCTransLineDetails(transactionId);

                return new JQGridModel
                {
                    TranSSCLineData = responseData,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    S_errMsg = "Success"
                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SSCTransactionLineDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TranSSCLineData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
        [HttpGet]
        [Authorize]
        public JQGridModel SCTransactionLineDetails(int transactionId, int page, int rows, string sidx, string sord)
        {
            try
            {
                List<SCTransLineModel> responseData = transactionManager.GetSCTransLineDetails(transactionId);

                return new JQGridModel
                {
                    TranLineData = responseData,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    S_errMsg = "Success"
                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SCTransactionLineDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TranLineData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
        [HttpGet]
        [Authorize]
        public JQGridModel ASNTransactionLineDetails(int transactionId, int page, int rows, string sidx, string sord)
        {
            try
            {
                List<ASNTransLineModel> responseData = transactionManager.GetASNTransLineDetails(transactionId);

                return new JQGridModel
                {
                    TranASNLineData = responseData,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    S_errMsg = "Success"
                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SCTransactionLineDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TranLineData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
        [HttpGet]
        [Authorize]
        public JQGridModel SOTransactionLineDetails(int transactionId, int page, int rows, string sidx, string sord)
        {
            try
            {
                List<SOTransLineModel> responseData = transactionManager.GetSOTransLineDetails(transactionId);

                return new JQGridModel
                {
                    TranSOLineData = responseData,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    S_errMsg = "Success"
                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SCTransactionLineDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TranLineData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
        [HttpGet]
        [Authorize]
        public object GetAllTransaction()
        {
            try
            {
                return transactionManager.GetAllTransaction();
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-GetAllTransaction of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

        [HttpGet]
        [Authorize]
        public JQGridModel ScannedTransactionLineDetails(int transactionId, int page, int rows, string sidx, string sord)
        {
            try
            {
                List<SCTransLineModel> responseData = transactionManager.GetScannedTransLineDetails(transactionId);

                return new JQGridModel
                {
                    TranLineData = responseData,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = true,
                    S_errMsg = "Success"
                };
            }
            catch (Exception ex)
            {
                log.Error("TransactionController-SCTransactionLineDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new JQGridModel
                {
                    TranLineData = null,
                    Page = page,
                    PageSize = 1,
                    SortColumn = sidx,
                    SortOrder = sord,
                    ModelStatus = false,
                    S_errMsg = "Unable to process your request.Please try again"
                };

            }
        }
    }
}
