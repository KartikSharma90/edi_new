﻿using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionDetailsSOODataController : ODataController
    {
        private ITransactionManager transactionManager = new TransactionManager();
        [EnableQuery]
        public IQueryable<SOTransLineModel> Get(int transactionId)
        {
            var responseData = transactionManager.GetSOTransLineDetailsNew(transactionId);
            return (from n in responseData
                    select new SOTransLineModel
                    {
                        Batchid = n.Batchid,
                        CustomerNumber=n.CustomerNumber,
                        CustomerPODate=n.CustomerPODate,
                        CustomerPONumber=n.CustomerPONumber,
                        ETA=n.ETA,
                        ETD = n.ETD,
                        FromLocation=n.FromLocation,
                        LineContent=n.LineContent,
                        MaterialNumber=n.MaterialNumber,
                        Mode=n.Mode,
                        POD=n.POD,
                        POL=n.POL,
                        ReqDeliveryDate=n.ReqDeliveryDate,
                        SAPStatus=n.SAPStatus,
                        SOQuantity=n.SOQuantity,
                        Status=n.Status,
                        UniqueId=n.UniqueId,
                        Consignee=n.Consignee
                    }).OrderByDescending(i => i.UniqueId).AsQueryable();
        }
    }
}
