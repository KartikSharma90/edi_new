﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;
using System.Web;
using System.IO;
using System.Net.Http.Headers;
using GPArchitecture.Models;
using GPArchitecture.Utilities;
using GPArchitecture.SAPService;

namespace GoodpackEDI.Controllers
{
    public class SOController : ApiController
    {
        private IEntityManager entityManager = new EntityManager();
        private ISalesOrderManager salesOrderManager = new SalesOrderManager();
        private static readonly ILog log = LogManager.GetLogger(typeof(SOController));
        ResponseModel responseModel = new ResponseModel();

        // get all the entities mapped to the subsi of the user
        [HttpGet]
        [Authorize]
        public object AllEntities()
        {
            try
            {
                return entityManager.GetAllEntitiesforUser(User.Identity.Name);
            }
            catch (Exception ex)
            {
                log.Error("SOController-AllEntities of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }



        // get all customers mapped to the subsi of the user
        [HttpGet]
        [Authorize]
        public object AllCustomers()
        {
            try
            {
                return entityManager.GetAllCustomersforUser(User.Identity.Name);
            }
            catch (Exception ex)
            {
                log.Error("SOController-AllCustomers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        //[HttpGet]
        //[Authorize]
        //public object IsMassEditPrevilege()
        //{
        //    try
        //    {
        //        return entityManager.GetPrivilegeforEdit(User.Identity.Name);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("SOController-GetPrevilageforEdit of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
        //        return new { Result = "Error", Message = "Unable to process your request.Please try again" };
        //    }
        //}
        // get all reasons for edit order
        [HttpGet]
        [Authorize]
        public object AllReasonsForEdit()
        {
            try
            {
                return entityManager.GetAllReasonsForEdit();
            }
            catch (Exception ex)
            {
                log.Error("SOController-AllCustomers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        [HttpGet]
        [Authorize]
        public object AllReasonsForCancel()
        {
            try
            {
                return entityManager.GetAllReasonsForCancel();
            }
            catch (Exception ex)
            {
                log.Error("SOController-AllCustomers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        [HttpGet]
        [Authorize]
        public object GetMappedPacker(int customercode)
        {
            try
            {
                return entityManager.GetAllPackerforCustomer(customercode);               
            }
            catch (Exception ex)
            {
                log.Error("SOController-GetMappedPacker of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

        [Authorize]
        [HttpGet]
        //
        public JQGridModel ViewOrder(string startDate, string endDate, int page, int rows, string sidx, string sord, string custNo)
        {

            try
            {
                List<SalesOrderModel> responseData = salesOrderManager.SalesOrderData(startDate, endDate, "0000" + custNo);
                var jqGridDataNew = new JQGridModel()
                {
                    sData = responseData,
                    Page = page,
                    PageSize = 3,
                    SortColumn = sidx,
                    SortOrder = sord
                }; 
                return jqGridDataNew;
            }

            catch(Exception ex)
            {
                log.Error("SOController-ViewOrder of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                var jqGridDataNew = new JQGridModel()
                {
                    sData = null,
                    Page = page,
                    PageSize = 3,
                    SortColumn = sidx,
                    SortOrder = sord
                };
                return jqGridDataNew;           
            }

        }


        [Authorize]
        [HttpGet]
        //
        public JQGridModel ViewNewSOOrder(string startDate, string endDate, int page, int rows, string sidx, string sord, string custNo)
        {

            try
            {
                List<SalesOrderModel> responseData = salesOrderManager.NewSalesOrderData(startDate, endDate, "0000" + custNo);
                var jqGridDataNew = new JQGridModel()
                {
                    sData = responseData,
                    Page = page,
                    PageSize = 3,
                    SortColumn = sidx,
                    SortOrder = sord
                };
                return jqGridDataNew;
            }

            catch (Exception ex)
            {
                log.Error("SOController-ViewNewSOOrder of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                var jqGridDataNew = new JQGridModel()
                {
                    sData = null,
                    Page = page,
                    PageSize = 3,
                    SortColumn = sidx,
                    SortOrder = sord
                };
                return jqGridDataNew;
            }

        }



        [HttpPost]
        [Authorize]
        public object createSO(ArrayList rowDetails) 
        {
            try
            {
                    responseModel.Success = true;
                    responseModel.Message = "";
                    responseModel.ResponseItems = salesOrderManager.postSO(User.Identity.Name,rowDetails);
            }
            catch (Exception ex)
            {
                log.Error("SOController-CreateSO of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                responseModel.Success = false;
                responseModel.Message = "Unable to process your request.Please try again";
            }
            return responseModel;
        }
        [HttpPost]
        [Authorize]
        public object massEditSalesOrder(EditSOModel SOModel)
        {
            try
            {
                responseModel.Success = true;
                responseModel.Message = "";
                responseModel.ResponseItems = salesOrderManager.massEditSO(User.Identity.Name,SOModel.customerCode, SOModel.customerPNos,SOModel.deliveryLocations, SOModel.delDates, SOModel.matNos, SOModel.orderQtys, SOModel.materialNo, SOModel.orderQty, SOModel.deliveryDt, SOModel.reason1, SOModel.reason2, SOModel.reason3);
            }
            catch (Exception ex)
            {
                log.Error("SOController-CreateSO of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                responseModel.Success = false;
                responseModel.Message = "Unable to process your request.Please try again";
            }
            return responseModel;
        }
        [HttpPost]
        [Authorize]
        public object massCancelSalesOrder(CancelSOModel SOModel)
        {
            try
            {
                responseModel.Success = true;
                responseModel.Message = "";
                responseModel.ResponseItems = salesOrderManager.massCancelSO(User.Identity.Name,SOModel);
            }
            catch (Exception ex)
            {
                log.Error("SOController-CreateSO of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                responseModel.Success = false;
                responseModel.Message = "Unable to process your request.Please try again";
            }
            return responseModel;
        }
        [HttpGet]
        [Authorize]
        public HttpResponseMessage DownloadEditMapperFile()
        {
           
            var localFilePath = HttpContext.Current.Server.MapPath("~/Download/EDI SO Change Template.xlsx");
            try
            {
                FileInfo file = new FileInfo(localFilePath);
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(localFilePath, FileMode.Open);
                message.Content = new StreamContent(stream);
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("Application/x-msexcel");
                message.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                message.Content.Headers.ContentDisposition.FileName = file.Name;
                
                return message;
            }
            catch (Exception ex)
            {
                HttpResponseMessage result = null;
                log.Error("SOController-AllCustomers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return result = Request.CreateResponse(HttpStatusCode.Gone);
            }
        }
        [HttpGet]        
        public HttpResponseMessage DownloadMapperFile()
        {
            
            var localFilePath = HttpContext.Current.Server.MapPath("~/Download/SO Template.xlsx");
            
            try
            {
                FileInfo file = new FileInfo(localFilePath);
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(localFilePath, FileMode.Open);
                message.Content = new StreamContent(stream);
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("Application/x-msexcel");
                message.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                message.Content.Headers.ContentDisposition.FileName = file.Name;
               
    
                return message;
            }
            catch (Exception ex)
            {
                HttpResponseMessage result = null;
                log.Error("SOController-AllCustomers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return result = Request.CreateResponse(HttpStatusCode.Gone);
            }
        }

        [HttpGet]
        [Authorize]
        public object GetCustomerPackerDetails(string lookupName)
        {
            try
            {
                return entityManager.GetCustomerPackerEntitiesforUser(lookupName,User.Identity.Name);
            }
            catch (Exception ex)
            {
                log.Error("SOController-GetCustomerPackerDetails of Username" + User.Identity.Name + ",lookupName "+lookupName+" ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
    }
}
