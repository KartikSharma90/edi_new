﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GPArchitecture.GoodPackBusinessLayer.ConfigManagementLayer;
using GPArchitecture.Models;

namespace GoodpackEDI.Controllers
{
    public class ConfigController : ApiController
    {
        IConfigManager manageConfig = new ConfigManager();
        [HttpGet]
        public object ConfigDetails()
        {
            try
            {
                string username = User.Identity.Name;
                return manageConfig.GetConfigDetails();
            }
            catch (Exception ex)
            {
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        [HttpPost]
        public object AddConfig(ConfigDetailsModel addConfigDetailsModel)
        {
            try
            {
                string username = User.Identity.Name;
                return manageConfig.AddConfigDetails(addConfigDetailsModel, username);

            }
            catch (Exception ex)
            {
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        [HttpPut]
        public object EditConfig(ConfigDetailsModel editConfigDetailsModel)
        {
            try
            {
                string username = User.Identity.Name;
                bool editStatus = manageConfig.UpdateConfigDetails(editConfigDetailsModel, username);
                if (editStatus == true)
                {
                    return manageConfig.GetConfigDetails();
                }
                return null;

            }
            catch (Exception ex)
            {
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        [HttpDelete]
        public object DeleteConfig(int ConfigId)
        {
            try
            {
                bool deleteStatus = manageConfig.DeleteConfigDetails(ConfigId);
                if (deleteStatus == true)
                {
                    return true;
                }
                return null;

            }
            catch (Exception ex)
            {
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
    }
}