﻿using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionDetailsSCODataController :  ODataController
    {
        private ITransactionManager transactionManager = new TransactionManager();
        [EnableQuery]
        public IQueryable<SCTransLineModel> Get(int transactionId)
        {          
                var responseData = transactionManager.GetSCTransLineDetails(transactionId);
                return (from n in responseData
                        select new SCTransLineModel
                        {
                            Batchid = n.Batchid,
                            BinType = n.BinType,
                            consigneeLocation = n.consigneeLocation,
                            ETD = n.ETD,
                            LineContent = n.LineContent,
                            Mode = n.Mode,
                            packerLocation = n.packerLocation,
                            Qty = n.Qty,
                            ReferenceNumber = n.ReferenceNumber,
                            SAPStatus = n.SAPStatus,
                            Status = n.Status,
                            CustomerCode = n.CustomerCode,
                            UniqueId = n.UniqueId
                        }).OrderByDescending(i => i.UniqueId).AsQueryable();          
         
        }
    }
}
