﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using GPArchitecture.Models.ViewModels;
using GPArchitecture.Models;
using GPArchitecture.SAPService.LSPCRManagementLayer;

namespace GoodpackEDI.Controllers
{
    public class LSPCRController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LSPCRController));

        [HttpPost]
        public object SubmitLSPCR(LSPCRModel modelDetails)
        {
            ResponseModel responseModel = new ResponseModel();

            try
            {

                LSPCRServiceManager serviceManager = new LSPCRServiceManager();
                responseModel = serviceManager.SubmitLSPData(modelDetails);

            }
            catch (Exception ex)
            {
                log.Error("LSPCRController-SubmitLSPCR of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                responseModel.Success = false;
                responseModel.Message = "Unable to process your request.Please try again";
            }

            return responseModel;
        }


    }
}
