﻿using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionDetailsSSCODataController : ODataController
    {
        private ITransactionManager transactionManager = new TransactionManager();
        [EnableQuery]
        public IQueryable<TrabsactionLineDetailModel> Get(int transactionId)
        {
            var responseData = transactionManager.GetScannedTransLineDetailsNew(transactionId);
            return (from n in responseData
                    select new TrabsactionLineDetailModel
                    {
                        Batchid = n.Batchid,
                        BinType = n.BinType,
                        ConsigneeLocation = n.consigneeLocation,
                        ETD = n.ETD,
                        LineContent = n.LineContent,
                        Mode = n.Mode,
                        PackerLocation = n.packerLocation,
                        Qty = n.Qty,
                        ReferenceNumber = n.ReferenceNumber,
                        SAPStatus = n.SAPStatus,
                        Status = n.Status,
                        BinNumbers = n.BarcodeNumbers,
                        UniqueId = n.UniqueId
                    }).OrderByDescending(i => i.UniqueId).AsQueryable();
        }
    }
}
