 using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Web;
 using System.Web.Mvc;
 
 using System.Dynamic;
 using GPArchitecture.SAPService;
 using log4net;
 using GPArchitecture.SAPService.LSPCRManagementLayer;
 using GPArchitecture.Models.ViewModels;
 using GPArchitecture.GoodPackBusinessLayer.ProActiveDeHireManagementLayer;
 
 namespace GoodpackEDI.Controllers
 {
     public class GoodpackEDIController : Controller
     {
         //
         // GET: /GoodpackEDI/
         private static readonly ILog log = LogManager.GetLogger(typeof(GoodpackEDIController));
         public ActionResult Index()
         {
             return View();
         }
 
         public ActionResult LSPCR(string jobid,string code)
         {
             LSPCRServiceManager serviceManager = new LSPCRServiceManager();
             LSPCRValidateService validateService = new LSPCRValidateService();
             Dictionary<string, string> validData = validateService.LSPCRValidate(jobid, code);
             LSPCRModel model = new LSPCRModel();
             try
             {
                 log.Info("LSPCR validate data-" + validData);
             }
             catch (Exception e) { }
             if (Convert.ToBoolean(validData["IsValid"]))
             {
                 model.From = validData["From"];
                 model.To = validData["To"];
                 model.IsValid = Convert.ToBoolean(validData["IsValid"]);
                 model.Message = validData["Message"];
                 model.JobId = validData["JobId"];
                 model.FromAddress = validData["FromAddress"];
                 model.ToAddress = validData["ToAddress"];
                 model.SKU = validData["SKU"];
                 model.Quantity = validData["Quantity"];
                 model.ItrQuantity = validData["ItrQuantity"];
                 model.ProposedPickUpDate = validData["ProposedPickUpDate"];
                 model.ConfirmedPickUpDate = validData["ConfirmedPickUpDate"];
                 model.Vendor = validData["Vendor"];
                 model.Remarks = validData["Remarks"];
                 model.Name = validData["lspname"];
             }
             else
             {              
                 model.IsValid = Convert.ToBoolean(validData["IsValid"]);
                 model.Message = validData["Message"];    
 
             }
             if (model.IsValid)
             {
                 return View("~/Views/Shared/_LSPCRPartial.cshtml", model);
             }
             else
             {
                 return View("~/Views/Shared/_LSPCRErrorPartial.cshtml", model);
             }
         }
 
         [HttpGet]
         public ActionResult ProActiveDeHire(string Sdate, string Edate, string Consigneecode, string Bintype, string Key)
         {
             IProActiveDeHireManager dehireManager = new ProActiveDeHireManager();
             if (string.IsNullOrEmpty(Sdate) || string.IsNullOrEmpty(Edate) || string.IsNullOrEmpty(Consigneecode) || string.IsNullOrEmpty(Bintype) || string.IsNullOrEmpty(Key))
                 return View("ProActiveDeHireError", new ProActiveDehireReturnModel { Status = "false", Message = "Some parameters are empty" });           
            
             DateTime startDate = Convert.ToDateTime(Sdate);
             DateTime endDate = Convert.ToDateTime(Edate);
 
             bool isValidKey=dehireManager.ProActiveDeHireUrlValidator(Consigneecode, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), Bintype, Key);
 isValidKey = true;
             if(!isValidKey)
                 return View("ProActiveDeHireError", new ProActiveDehireReturnModel { Status = "false", Message = "Code is invalid." });
 
             int binRowCount = Bintype.Count(i => i == ',') + 1;
             List<string> binType = Bintype.Split(',').ToList<string>();
             var dates = new List<DateTime>();
             for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
             {
                 dates.Add(dt);
             }
             DehireViewModel viewModel = new DehireViewModel(); 
             ProActiveDehireSubmitModel getModel=new ProActiveDehireSubmitModel();
             ProActiveDehireModel dehireVal = new ProActiveDehireModel();
             dehireVal.BinList = binType;
             dehireVal.ConsigneeCode = Consigneecode;
             dehireVal.Dates = dates;
             viewModel.dehire = dehireVal;
             
             return View(viewModel);
         }
     }
 }

 

