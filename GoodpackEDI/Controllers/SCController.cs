﻿using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
using GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer;
using GPArchitecture.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace GoodpackEDI.Controllers
{
    public class SCController : ApiController
    {
        private ITransactionDataManager scManager = new TransactionDataManager();
        private static readonly ILog log = LogManager.GetLogger(typeof(SCController));
        
        public async Task<HttpResponseMessage> UploadFile(string transactionType)
        {
            //if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.MappingConfiguration))
            //   {
            log.Debug("SCController-UploadFile");
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            string root = null;

            TRANSACTION_TYPES inputTransType =
                            (TRANSACTION_TYPES)Enum.Parse(typeof(TRANSACTION_TYPES), transactionType);
            switch (inputTransType)
            {
                case TRANSACTION_TYPES.SO: root = HttpContext.Current.Server.MapPath("~/SOUploadFiles");
                    break;
                case TRANSACTION_TYPES.SC: root = HttpContext.Current.Server.MapPath("~/ScUploadFiles");
                    break;
                case TRANSACTION_TYPES.SSC: root = HttpContext.Current.Server.MapPath("~/SScUploadFiles");
                    break;
                case TRANSACTION_TYPES.ASN: root = HttpContext.Current.Server.MapPath("~/ASNUploadFiles");
                    break;
            }           
           

            
            var provider = new MultipartFormDataStreamProvider(root);
            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body
                await Request.Content.ReadAsMultipartAsync(provider);          
     
                // This illustrates how to get the file names for uploaded files.
                ITransactionDataManager scManager = new TransactionDataManager();
                string path="";
                scManager.fileUpload(provider.FileData, root,out path);
                return new HttpResponseMessage()
                {
                    StatusCode=HttpStatusCode.OK,
                    Content = new StringContent(path)
                                       
                };               

            }
            catch (System.Exception e)
            {
                log.Error("SCController-UploadFile of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

        public  HttpResponseMessage DownloadErrorFile(int batchId,bool isDownloadError)
        {
           
            try
            {             
                // This illustrates how to get the file names for uploaded files.
                ITransactionDataManager scManager = new TransactionDataManager();
                string responseFile=scManager.ErrorLinesDownload(batchId,isDownloadError);
                FileInfo file = new FileInfo(responseFile);
                HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
                var stream = new FileStream(responseFile, FileMode.Open);
                message.Content = new StreamContent(stream);
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                message.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                message.Content.Headers.ContentDisposition.FileName = file.Name+".csv";
                //file.Delete();
                return message;             

            }
            catch (System.Exception e)
            {
                log.Error("SCController-DownloadFile of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }             

        }
      

        [HttpGet]
        [Authorize]
        public IList<string> MappedFiles(int subsiId,int transactionType)
        {
            try
            {
                return scManager.mappedFiles(subsiId, transactionType);
            }
            catch (Exception e)
            {
                log.Error("SCController-MappedFiles of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<string>();
            }
        }

       
        [HttpGet]
        [Authorize]
        public IList<GenericItemModel> FileData(string mapperName, int subsiId, string path)
        {
            try
            {
                //GoodpackEDI.BusinessLayer.SCManagementLayer.ITransactionDataManager transTest = new GoodpackEDI.BusinessLayer.SCManagementLayer.TransactionDataManager();
                //transTest.SCFileData(mapperName, subsiId, true, path);
                return scManager.SCFileData(mapperName, subsiId, true,path);
            }
            catch (Exception e)
            {
                log.Error("SCController-FileData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<GenericItemModel>();
            }
        }
      
        [HttpGet]
        [Authorize]
        public IList<GenericItemModel> SOFileData(string mapperName, int subsiId,string path)
        {
            try
            {
                log.Debug("SOController-SOFileData");
                return scManager.SOFileData(mapperName, subsiId, true, path);
            }
            catch (Exception e)
            {
                log.Error("SOController-SOFileData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                GenericItemModel item = new GenericItemModel();
                item.Message = e.Message == null ? "Unable to parse the file.Please check your data in file" : e.Message;
                item.Status = "false";
                List<GenericItemModel> returnValue = new List<GenericItemModel>();
                if (e.Message != null)
                    returnValue.Add(item);
                return returnValue;
            }
        }
        [HttpGet]
        [Authorize]
        public IList<GenericItemModel> SOChangeCancelUpload(int subsiId, string path)
        {
            try
            {
                return scManager.SOChangeCancelFileData(subsiId, true, path);
            }
            catch (Exception e)
            {
                log.Error("SOController-SOFileData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<GenericItemModel>();
            }
        }

        [HttpPost]
        [Authorize]
        public IList<GenericItemModel> VerifySOData(string mapperFile, string subsiId,string custPOdate, IList<GenericItemModel> itemModel)
        {
            try
            {
                return scManager.verifySOData(mapperFile, User.Identity.Name, subsiId,custPOdate, itemModel, true,true);
            }
            catch (Exception e)
            {
                log.Error("SCController-VerifyData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<GenericItemModel>();
            }
        }
        [HttpPost]
        [Authorize]
        public IList<GenericItemModel> VerifySOChangeCancelUpload(int subsiId, IList<GenericItemModel> itemModel)
        {
            try
            {
                return scManager.VerifySOChangeCancelData(User.Identity.Name, subsiId,itemModel);
            }
            catch (Exception e)
            {
                log.Error("SCController-VerifyData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<GenericItemModel>();
            }
        }
        [HttpPost]
        [Authorize]
        public IList<GenericItemModel> VerifyData(string mapperFile, string subsiId, IList<GenericItemModel> itemModel, int transactionType)
        {
            SSCManagerNew sscManager = new SSCManagerNew();
            try
            {
                
                switch (transactionType)
                {

                    case 1: return scManager.verifyDataNew(mapperFile, User.Identity.Name, subsiId, itemModel, true, true);
                         //scManager.verifyData(mapperFile, User.Identity.Name, subsiId, itemModel, true, true);
                    case 2: return sscManager.submitSSCData(mapperFile, User.Identity.Name, subsiId, itemModel, true);
                      //  return scManager.verifySSCData(mapperFile, User.Identity.Name, subsiId, itemModel, true);
                    case 3: return scManager.verifyASNData(mapperFile, User.Identity.Name, subsiId, itemModel, true);
                    default: return new List<GenericItemModel>();
                  
                }
                
            }
            catch (Exception e)
            {
                log.Error("SCController-VerifyData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<GenericItemModel>();
            }
        }
        [HttpPost]
        [Authorize]
        public IList<ResponseModel> SubmitData(IList<GenericItemModel> itemModel,int transactionType)
        {
            try
            {
                 TRANSACTION_TYPES transType=TRANSACTION_TYPES.SC;
                switch (transactionType)
                {   
                    case 0: transType = TRANSACTION_TYPES.SO;
                        break;
                    case 1: transType = TRANSACTION_TYPES.SC;
                        break;
                    case 2: transType = TRANSACTION_TYPES.SSC;
                        break;
                    case 3: transType = TRANSACTION_TYPES.ASN;
                        break;
                    //default: transType = null;
                    //    break;
                }
                return scManager.SubmitData(itemModel,User.Identity.Name,transType);
            }
            catch (Exception e)
            {
                log.Error("SCController-SubmitData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<ResponseModel>();
            }
        }

        [HttpPost]
        [Authorize]
        public IList<ResponseLineDataModel> ReSubmit(ResbumitSCLineModel LineModel, int status, string mapperName, string transactionType)
        {
            try
            {
                return scManager.ReSubmit(transactionType, User.Identity.Name, LineModel.BatchId, LineModel.LineIds, status, mapperName);
            }
            catch (Exception e)
            {
                log.Error("SCController-Resubmit of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<ResponseLineDataModel>();
            }
        }

        [HttpGet]
        [Authorize]
        public IList<string> ErrorDetails(int lineId, int transactionType)
        {
            try
            {
                TRANSACTION_TYPES transType = TRANSACTION_TYPES.SC;
                switch (transactionType)
                {
                    case 0: transType = TRANSACTION_TYPES.SO;
                        break;
                    case 1: transType = TRANSACTION_TYPES.SC;
                        break;
                    case 2: transType = TRANSACTION_TYPES.SSC;
                        break;
                    //default: transType = null;
                    //    break;
                }
                return scManager.RetrieveErrorData(lineId,transType);
            }
            catch (Exception e)
            {
                log.Error("SCController-ErrorDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<string>();
            }
        }

        public IList<string> SOErrorDetails(int lineId)
        {
            try
            {
                return scManager.RetrieveSOErrorData(lineId);
            }
            catch (Exception e)
            {
                log.Error("SCController-SOErrorDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<string>();
            }
        }
        [HttpGet]
        [Authorize]
        public IList<string> LineContentDetails(int lineId, int transactionType)
        {
            try
            {
                TRANSACTION_TYPES transType = TRANSACTION_TYPES.SC;
                switch (transactionType)
                {
                    case 0: transType = TRANSACTION_TYPES.SO;
                        break;
                    case 1: transType = TRANSACTION_TYPES.SC;
                        break;
                    case 2: transType = TRANSACTION_TYPES.SSC;
                        break;
                    case 3: transType = TRANSACTION_TYPES.ASN;
                        break;
                    //default: transType = null;
                    //    break;
                }
                return scManager.RetrieveLineContentDetails(lineId, transType);
            }
            catch (Exception e)
            {
                log.Error("SCController-SOErrorDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return new List<string>();
            }
        }


        [HttpDelete]
        [Authorize]
        public bool DeleteRecords(DeleteLineModel deleteModel, string transactionType)
        {
            try
            {
                return scManager.DeleteRecords(transactionType, deleteModel.TransactionId, deleteModel.DeleteComment,deleteModel.DeleteIds);
            }
            catch (Exception e)
            {
                log.Error("SCController-DeleteRecords of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                return false;
            }
        }

        [HttpGet]
        public IList<ResponseLineDataModel> ReSubmitData(int Status, int TransactionType, int BatchId, bool ActionType)
        {
            string transTypevalue = null;
            switch (TransactionType)
            {
                case 2: transTypevalue = "SO";
                    break;
                case 3: transTypevalue = "SC";
                    break;             
            }
           return scManager.ReSubmit(transTypevalue, User.Identity.Name, BatchId, Status, ActionType);
        }

        [HttpGet]
        public BatchStatusDetail GetBatchStatus(int BatchId, int TransactionType)
        {
            return scManager.GetBatchStatus(BatchId, TransactionType);
        }
    }
}
