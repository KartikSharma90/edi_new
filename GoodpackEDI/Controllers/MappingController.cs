using GPArchitecture.CommonTypes;
 using GPArchitecture.GoodPackBusinessLayer.MappingManagementLayer;
 using GPArchitecture.Models;
 using log4net;
 using System;
 using System.Collections.Generic;
 using System.Data;
 using System.Globalization;
 using System.IO;
 using System.Net;
 using System.Net.Http;
 using System.Net.Http.Headers;
 using System.Text;
 using System.Threading.Tasks;
 using System.Web;
 using System.Web.Http;
 
 namespace GoodpackEDI.Controllers
 {
     public class MappingController : ApiController
     {
         //
         // GET: /Mapping/
         private IMapperManager mapperManager = new MapperManager();
         private IHeaderManager headerManager = new HeaderReaderManager();
         private static string mappingFilePath;
         private static readonly ILog log = LogManager.GetLogger(typeof(MappingController));
 
         [HttpGet]
         public IList<GenericItemModel> TransactionTypes()
         {
             try
             {
                 return mapperManager.TransactionTypes();
             }
             catch (Exception e)
             {
               log.Error("MappingController-TransactionTypes of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
               return  new List<GenericItemModel>();
             }
         }
 
         [HttpGet]
         public IList<GenericItemModel> FileTypes() 
         {
             try
             {
                 return mapperManager.FileTypes();
             }
             catch (Exception e)
             {
                 log.Error("MappingController-FileTypes of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new List<GenericItemModel>();
             }
         }
 
         //[HttpPost]
         //public ResponseModel FileChecker(MappingModel mappingModel)
         //{
         // return mapperManager.CheckMappingExists(mappingModel, User.Identity.Name);
         //}
 
         [HttpGet]
         public IList<SAPMappingModel> SAPFields(int transactionId)
         {
             try
             {
                 return mapperManager.SAPMessageFields(transactionId);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-SAPFields of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new List<SAPMappingModel>();
             }
         }
         [HttpPost]
         public DataTable HeaderFields(MappingModel mappingModel)
         {
             try
             {
                 return headerManager.readHeaders(mappingFilePath, mappingModel);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-HeaderFields of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new DataTable();
             }
         }
 
         [HttpPost]
         public ResponseModel MappedData(MappingModel mappingModel)
         {
             try
             {
                 return mapperManager.SaveHeaderFields(mappingModel, User.Identity.Name, mappingFilePath);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-MappedData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new ResponseModel();
             }
         }
         [HttpPost]
         public ResponseModel ValidateFTP(FtpModel mappingModel)
         {
             try
             {
                 return mapperManager.IsValidFtp(mappingModel);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-MappedData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new ResponseModel();
             }
         }
         [HttpGet]
         public ResponseModel EmailMapper(string emailIds, string transactionId)
         {
             try
             {
                 return mapperManager.EmailIdMapper(emailIds,transactionId);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-EmailMapper of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new ResponseModel();
             }
         }
         
         [HttpPost]
         [Authorize]
         public async Task<HttpResponseMessage> PostFormData()
         {
             //if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.MappingConfiguration))
             //   {
             if (!Request.Content.IsMimeMultipartContent())
             {
                 throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
             }
 
             string root = HttpContext.Current.Server.MapPath("~/MapperUploadFile");
             var provider = new MultipartFormDataStreamProvider(root);
 
             try
             {
 
                 StringBuilder sb = new StringBuilder(); // Holds the response body
                 await Request.Content.ReadAsMultipartAsync(provider);
 
 
                 // This illustrates how to get the file names for uploaded files.
                 foreach (var file in provider.FileData)
                 {
                     string fileName = (file.Headers.ContentDisposition.FileName).Replace("\"", string.Empty);
                     string filePath = Path.Combine(root, fileName);
 
                     File.Copy(file.LocalFileName, filePath, false);
 
                     string extension = Path.GetExtension(filePath);
                     string oldFileName = Path.GetFileNameWithoutExtension(filePath);
 
                     mappingFilePath = Path.Combine(root, oldFileName + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss", CultureInfo.InvariantCulture) + extension);
                     File.Move(filePath, mappingFilePath);
                     File.Delete(filePath);
                     File.Delete(file.LocalFileName);  
                 }
 
                 return new HttpResponseMessage()
                 {
                     Content = new StringContent(sb.ToString())
                 };
                 //}
                 //    else
                 //    {
                 //        return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                 //    }
 
             }
             catch (System.Exception e)
             {
                 log.Error("MappingController-PostFormData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
             }
         }
 
 
         [HttpGet]
         [Authorize]
         public JQGridModel GetAllMappingDetails( int page, int rows, string sidx, string sord)
         {
             try
             {
                 IList<ViewMapperDataModel> responseData = mapperManager.GetAllMappingDetails(User.Identity.Name);
 
                 return new JQGridModel
                 {
                     ViewMappingData = responseData,
                     Page = page,
                     PageSize = 1,
                     SortColumn = sidx,
                     SortOrder = sord,
                     ModelStatus = true,
                     S_errMsg = "Success"
                 };
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetAllMappingDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new JQGridModel
                 {
                     ViewMappingData = null,
                     Page = page,
                     PageSize = 1,
                     SortColumn = sidx,
                     SortOrder = sord,
                     ModelStatus = false,
                     S_errMsg = "Unable to process your request.Please try again"
                 };
 
             }
         }
 
         [HttpGet]
         [Authorize]
         public JQGridModel GetSelectedMappingDetails(int susbsyToTransactionMapperId)
         {
             try
             {
                 IList<ViewSelectedMapperDataModel> responseData = mapperManager.GetSelectedMappingDetails(susbsyToTransactionMapperId);
 
                 return new JQGridModel
                 {
                     ViewSelectedMappingData = responseData,
                    // Page = page,
                     PageSize = 1,
                    // SortColumn = sidx,
                    // SortOrder = sord,
                     ModelStatus = true,
                     S_errMsg = "Success"
                 };
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetSelectedMappingDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new JQGridModel
                 {
                     ViewSelectedMappingData = null,
                    // Page = page,
                     PageSize = 1,
                    // SortColumn = sidx,
                    // SortOrder = sord,
                     ModelStatus = false,
                     S_errMsg = "Unable to process your request.Please try again"
                 };
 
             }
         }
 
         [HttpPost]
         public ResponseModel EditMappedData(MappingModel mappingModel)
         {
             try
             {
                 return mapperManager.EditHeaderFields(mappingModel, User.Identity.Name);
             }
             catch (Exception e)
             {
                 log.Error("MappingController-EditMappedData of Username" + User.Identity.Name + " ,Api Exception:-Message:" + e.Message);
                 return new ResponseModel();
             }
         }
 
         [HttpGet]
         public IList<EditMapperHeaderModel> GetFilDetailsForEdit(int susbsyToTransactionMapperId)
         {
             try
             {
                 IList<EditMapperHeaderModel> fileDetails = mapperManager.GetFilDetailsForEdit(susbsyToTransactionMapperId);
                 return fileDetails;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetFileDetailsFoeEdit of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);               
                 return new List<EditMapperHeaderModel>();
             }
         }
 
         [HttpGet]
         public IList<MapperEmailModel> GetEmailDetailsForEdit(int susbsyToTransactionMapperId)
         {
             try
             {
                 IList<MapperEmailModel> emailDetails = mapperManager.GetEmailIdsForEdit(susbsyToTransactionMapperId);
                 return emailDetails;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetEmailDetailsForEdit of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new List<MapperEmailModel>();
             }
         }
 
         [HttpGet]
         public IList<EditHeaderModel> GetFileHeaders(int susbsyToTransactionMapperId)
         {
             try
             {
                 IList<EditHeaderModel> headerData = mapperManager.GetHeadersForEdit(susbsyToTransactionMapperId);
                 return headerData;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetFileHeaders of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new List<EditHeaderModel>();
             }
         }
 
         [HttpGet]
         public IList<EditMapperHeaderModel> GetMappedDetails(int susbsyToTransactionMapperId)
         {
             try
             {
                 IList<EditMapperHeaderModel> responseData = mapperManager.GetMappedFileDetailsForEdit(susbsyToTransactionMapperId);
                 return responseData;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetMappedDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new List<EditMapperHeaderModel>();
             }
         }
 
         [HttpPost]
         public object DeleteMapping(int susbsyToTransactionMapperId)
         {
             try
             {
                 bool f = mapperManager.DeleteSelectedMappingDetails(susbsyToTransactionMapperId);
                 if (f == true)
                 {
                     return true;
                 }
                 return null;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-DeleteMapping of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new { Result = "Error", Message = "Unable to process your request.Please try again" };
             }
         }
 
         [HttpGet]
         public HttpResponseMessage DownloadMapperData(string mapperName)
         {
             try
             {
                 log.Info("Download mapper");
                 string localFilePath = mapperManager.MapperFileDate(mapperName);
                 FileInfo file = new FileInfo(localFilePath);
                 HttpResponseMessage message = new HttpResponseMessage(HttpStatusCode.OK);
                 var stream = new FileStream(localFilePath, FileMode.Open);
                 message.Content = new StreamContent(stream);
                 message.Content.Headers.ContentType = new MediaTypeHeaderValue("Application/x-msexcel");
                 message.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                 message.Content.Headers.ContentDisposition.FileName = file.Name;
                 return message;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetMappedDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 HttpResponseMessage result = null;
                 return result = Request.CreateResponse(HttpStatusCode.Gone);
             }
         }
 
         [HttpGet]
         [Authorize]
         public IList<ViewMapperDataModel> GetAllMappingDetailsForUsers()
         {
             try
             {
                 IList<ViewMapperDataModel> responseData = mapperManager.GetAllMappingDetails(User.Identity.Name);
                 return responseData;
 
             }
             catch (Exception ex)
             {
                 log.Error("MappingController-GetAllMappingDetails of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 IList<ViewMapperDataModel> responseData = null;
                 return responseData;
 
             }
         }
     }
 }



