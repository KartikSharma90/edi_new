﻿using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;


namespace GoodpackEDI.Controllers
{
    public class TransactionSSCODataController : ODataController
    {
        private ITransactionManager transactionManager = new TransactionManager();

        [EnableQuery]
        public IQueryable<TransactionDataModel> Get(int SubsyCode, int transactionCode)
        {
            var responseData = transactionManager.GetSSCTransBatchDetails(User.Identity.Name, SubsyCode, transactionCode);
            return (from n in responseData
                    select new TransactionDataModel
                    {
                        FileName = n.FileName,
                        CreatedDate = n.CreatedDate,
                        ErrorCount = n.ErrorCount,
                        MapperName = n.MapperName,
                        SubsyName = n.SubsyName,
                        SuccessCount = n.SuccessCount,
                        TotalRecords = n.TotalRecords,
                        TransactionName = n.TransactionName,
                        TransactionType = n.TransactionType,
                        UniqueId = n.UniqueId
                    }).OrderBy(i=>i.UniqueId).AsQueryable();
        }
    }
}
