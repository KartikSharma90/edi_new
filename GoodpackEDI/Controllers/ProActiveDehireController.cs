﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using System.Web.Http;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Net.Http;
using System.Net;
using GPArchitecture.GoodPackBusinessLayer.ProActiveDeHireManagementLayer;
using GPArchitecture.Models.ViewModels;


namespace GoodpackEDI.Controllers
{
    
    public class ProActiveDehireController : ApiController
    {
        //
        // GET: /ProActiveDehire/
        private static readonly ILog log = LogManager.GetLogger(typeof(ProActiveDehireController));


        [HttpPost]
        public HttpResponseMessage SubmitProActiveDehire(HttpRequestMessage request,JObject submitData, string Sdate, string Edate, string Consigneecode, string Bintype, string Key)
        {
            try
            {
                IProActiveDeHireManager dehireManager = new ProActiveDeHireManager();
                if (string.IsNullOrEmpty(Sdate) || string.IsNullOrEmpty(Edate) || string.IsNullOrEmpty(Consigneecode) || string.IsNullOrEmpty(Bintype) || string.IsNullOrEmpty(Key))
                    return request.CreateResponse(HttpStatusCode.NotAcceptable, new ProActiveDehireReturnModel { Status = "false", Message = "Some parameters are empty" });

                DateTime startDate = Convert.ToDateTime(Sdate);
                DateTime endDate = Convert.ToDateTime(Edate);
                bool isValidKey = dehireManager.ProActiveDeHireUrlValidator(Consigneecode, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), Bintype, Key);
                if (!isValidKey)
                    return request.CreateResponse(HttpStatusCode.NotAcceptable, new ProActiveDehireReturnModel { Status = "false", Message = "Code is invalid." });
                ProActiveDehireReturnModel returnValue=  dehireManager.SubmitDehireData(submitData, Sdate, Edate, Consigneecode, Bintype, Key);
                return request.CreateResponse(HttpStatusCode.OK, returnValue);
            }
            catch(Exception e)
            {
                log.Error("SubmitProActiveDehire error -" + e);
                return request.CreateResponse(HttpStatusCode. InternalServerError , new ProActiveDehireReturnModel { Status = "false", Message = "InternalServerError" });
            }
        }      
    }
}
