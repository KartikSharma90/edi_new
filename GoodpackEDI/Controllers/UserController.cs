﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Security;
using log4net;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.UserManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.SubsiEntityManagementLayer;


namespace GoodpackEDI.Controllers
{
    public class UserController : ApiController
    {
        private IPrevilegeManager previlegeManager = new PrevilegeManager();
        private ISubsiManager subsiManager = new SubsiManager();
        private int pageNumber;
        private ResponseModel responseModel = new ResponseModel();
        private static readonly ILog log = LogManager.GetLogger(typeof(UserController));

        /// <summary>
        ///  POST: /User/Login
        ///  Login Api for internal and external User
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public LoginResponseModel Login(UserLoginModel loginModel)
        {
            log.Info("User Login with username : " + loginModel.Username);
            LoginResponseModel loginResponseModel = new LoginResponseModel();

            try
            {
                //Calls the IAuthenticationManager for user login
                if ((loginModel.Username != "") && (loginModel.Password != ""))
                {
                    IAuthenticationManager authManager = new GPArchitecture.GoodPackBusinessLayer.UserManagementLayer.AuthenticationManager();
                    return authManager.AuthenticateUser(loginModel);
                }
                else
                {
                    loginResponseModel.Success = false;
                    loginResponseModel.Message = "Username/Password cannot be empty";
                    return loginResponseModel;
                }

            }
            catch (Exception e)
            {
                log.Error("Login Api Exception:-Message:" + e.Message);
                log.Error("Login Api Exception:-Model:" + loginModel);
                loginResponseModel.Success = false;
                loginResponseModel.Message = "Problem occured with communicating with server.Please try again";
                return loginResponseModel;
            }
        }


        /// <summary>
        /// POST: /User/UserList
        /// Get list of registered users in the company - Internal,External and both
        /// </summary>
        /// <param name="id">Type of userList-Internal,External or Both</param>
        /// <param name="jtPageSize">PageSizeNumber</param>
        /// <param name="jtStartIndex">RowStartIndex</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object UserList(int jtStartIndex, int jtPageSize)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    if (jtStartIndex == 0)
                    {
                        pageNumber = 1;
                    }
                    else
                    {
                        pageNumber = (jtStartIndex / jtPageSize) + 1;
                    }
                    UserManager userManager = new UserManager();
                    IList<UserDetailsModel> userData = userManager.GetUsers(User.Identity.Name);
                    int userListCount = userData.Count;       //_repository.PersonRepository.GetAllPeople();
                    return new { Result = "OK", Records = userData, TotalRecordCount = userListCount };
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserList-method of UserController." + "jtStartIndex:" + jtStartIndex + ", jtPageSize:" + jtPageSize + "Api Exception:-Message:" + ex.Message);
                log.Error("UserList-Username: " + User.Identity.Name);
                return null;
            }
        }


        /// <summary>
        /// POST: /User/Create
        /// 
        /// </summary>
        /// <param name="registerModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object Create(JTableModel jtableModel)
        {
            //Calls the Registration Manager for user registeration
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    if (ModelState.IsValid)
                    {
                        IRegistrationManager registerManager = new RegistrationManager();
                        ResponseModel responseModel = registerManager.UserRegistration(jtableModel.record, User.Identity.Name);
                        bool isSuccess = responseModel.Success.HasValue ? (bool)responseModel.Success : false;
                        if (isSuccess)
                        {
                            return new { Result = "OK", Record = jtableModel };
                        }
                        else
                        {
                            return new { Result = "ERROR", Message = responseModel.Message };

                        }
                    }
                    else
                    {
                        responseModel.Success = false;
                        responseModel.Message = "Registration is incomplete.All fields are mandatory";
                    }
                    return responseModel;
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }

            }
            catch (Exception ex)
            {
                log.Error("Register Api Exception:-Message of Create method in UserController:" + ex.Message + "jtableModel:" + jtableModel);
                responseModel.Success = false;
                responseModel.Message = "Registration Incomplete.Unable to process your request.";
                return responseModel;
            }
        }


        /// <summary>
        /// POST: /User/Update
        /// Updating the user details
        /// </summary>
        /// <param name="userDetailsModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object Update(JTableModel userDetailsModel)
        {
            //Record userDetailsModel = (Record)userModel;
            //Calls the Registration Manager for user registeration
            UserDetailsModel registerModel = new UserDetailsModel();
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {

                    if (ModelState.IsValid)
                    {

                        UserManager manageUser = new UserManager();
                        if (manageUser.UpdateUser(userDetailsModel.record))
                        {
                            return new { Result = "OK" };
                        }
                        else
                        {
                            return new { Result = "ERROR", Message = "You dont have privilege to edit this user" };
                        }
                    }
                    else
                    {
                        return new { Result = "ERROR", Message = "All fields are manadatory" };
                    }
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("Update-UpdateUser of " + userDetailsModel.record.Username + "Api Exception:-Message:" + ex.Message);
                log.Error("Update-Username: " + User.Identity.Name + " Model: " + userDetailsModel.record);
                return new { Result = "ERROR", Message = "Unable to update the record" };
            }
        }



        /// <summary>
        /// POST: /User/AllLocations
        /// Get All Locations of user's company in drop down list
        /// </summary>
        /// <param name="locationModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object AllRoles()
        {
            try
            {
                IRoleManager roleManager = new RoleManager();
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                return roleManager.GetAllRoles();
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("All roles of UserController " + User.Identity.Name + "Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }



        /// <summary>
        /// POST: /User/Delete
        /// Delete user based on user id
        /// </summary>
        /// <param name="userModel">Model for deleting user</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object Delete(UserDetailsModel userModel)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                UserManager userManager = new UserManager();
                if (userManager.DeleteUser(userModel.UserId))
                {
                    return new { Result = "OK" };
                }
                else
                {
                    return new { Result = "ERROR", Message = "You dont have privilege to delete this user" };
                }
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("Delete of User Controller " + userModel.UserId + "Api Exception:-Message:" + ex.Message);
                log.Error("Delete of User Controller .Delete-Username: " + User.Identity.Name);
                return new { Result = "ERROR", Message = "Unable to process your request." };
            }
        }



        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object AllPrevileges(int roleId)
        {
            try
            {

                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                return previlegeManager.GetPrevileges(roleId);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController AllPrevileges-GetPrevileges  of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object MappedPrevilege(int roleId)
        {
            try
            {

                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                return previlegeManager.MappedPrevilege(roleId);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController MappedPrevilege of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object SavePrivilege(List<PrivilegeModel> privilegeModel, int roleId)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                    bool status = previlegeManager.CreatePrivilege(privilegeModel, roleId);
                    return new { Success = status };
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController SavePrivilege of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return null;
            }
        }

        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public object AllSubsies(/*int previlegeCode*/)
        {
            try
            {
               //// EnumAppPrevileges previlege = getPrevilegeType(previlegeCode);

              ////  if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, previlege))
             ////   {
                    return subsiManager.GetAllSubsies(User.Identity.Name);
            ////    }
            ////    else
            ////    {
            ////        return new { Result = "ERROR", Message = "Not Authorized to do this action" };
           ////     }
            }
            catch (Exception ex)
            {
                log.Error("UserController AllSubsies of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }
        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public object AllUsers(int roleId)
        {
            try
            {
                UserManager userManager = new UserManager();
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.Administrator))
                {
                return userManager.GetAllUsers(roleId);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController AllUsers of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

       

        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public object Subsies(int userId,bool isMapping)
        {
            try
            {
                ISubsiManager subsiManager = new SubsiManager();
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.SubsiMapping))
                {
                return subsiManager.GetSubsiData(userId,isMapping);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController Subsies of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

        /// <summary>
        /// POST: /User/AllPrevileges
        /// Get all previleges of user
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public object SaveSubsi(List<SubsiModel> subsiModel, int userId)
        {
            try
            {
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.SubsiMapping))
                {
                    ISubsiManager subsiManager = new SubsiManager();
                    return subsiManager.MapUserToSubsi(subsiModel, userId);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
            }
            catch (Exception ex)
            {
                log.Error("UserController SaveSubsi of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return null;
            }
        }


        /// <summary>
        /// GET: /User/Logoff
        /// User Log off
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public bool Logoff()
        {
            try
            {
                FormsAuthentication.SignOut();
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Logoff-Username: " + User.Identity.Name + " Api Exception " + ex.Message);
                return false;
            }
        }


        EnumAppPrevileges getPrevilegeType(int previlegeCode) 
        {
            switch (previlegeCode) 
            {
                case 1:
                    return EnumAppPrevileges.DataLookUp;
                default:
                    return EnumAppPrevileges.SubsiMapping;            
            }
        }
        public bool ChangePassowrd(ChangePassowrdModel changepasswordModel)
        {
            UserManager userManag = new UserManager();
            return userManag.ChangePassword(changepasswordModel);
        }

    }
}
