﻿using GPArchitecture.GoodPackBusinessLayer.TransactionManagementLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionODataController : ODataController
    {
        private ITransactionManager transactionManager = new TransactionManager();
        [EnableQuery]
        public IQueryable<TransactionDataModel> Get(int subsyCode,int transactionCode)
        {
            var responseData = transactionManager.GetSCTransBatchDetails(User.Identity.Name, subsyCode, transactionCode);
            return (from n in responseData
                    select new TransactionDataModel
                    {
                        FileName = n.FileName,
                        CreatedDate = n.CreatedDate,
                        ErrorCount = n.ErrorCount,
                        MapperName = n.MapperName,
                        SubsyName = n.SubsyName,
                        SuccessCount = n.SuccessCount,
                        TotalRecords = n.TotalRecords,
                        TransactionName = n.TransactionName,
                        TransactionType = n.TransactionType,
                        UniqueId = n.UniqueId,
                        DeletedRecordCount=n.DeletedCount
                    }).OrderByDescending(i => i.UniqueId).AsQueryable();
        }
    }
}
