﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using System.Data.Entity.Infrastructure;
using GPArchitecture.Utilities;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.UserManagementLayer;
using GPArchitecture.Models;
using GPArchitecture.GoodPackBusinessLayer.LookUpManagementLayer;

namespace GoodpackEDI.Controllers
{
    public class LookupController : ApiController
    {
        private IEntityManager entityManager = new EntityManager();
        private IPrevilegeManager previlegeManager = new PrevilegeManager();
        private static readonly ILog log = LogManager.GetLogger(typeof(LookupController));
        ResponseModel responseModel = new ResponseModel();
        ILookupManager lookupManager = new LookupManager();

        [HttpGet]
        [Authorize]
        public object AllEntityforSelectedSubsy(int subsiCode)
        {
            try
            {
                return entityManager.GetAllEntityforSubsy(subsiCode);
            }
            catch (Exception ex)
            {
                log.Error("LookupController-AllEntityforSelectedSubsy of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }


        [HttpGet]
        [Authorize]
        public object AllMapperforSelectedSubsy(int subsiCode)
        {
            try
            {
                
                if (previlegeManager.IsPrevilegeToAccess(User.Identity.Name, EnumAppPrevileges.DataLookUp))
                {
                     return entityManager.GetAllMapperforSelectedSubsy(subsiCode,User.Identity.Name);
                }
                else
                {
                    return new { Result = "ERROR", Message = "Not Authorized to do this action" };
                }
               
            }
            catch (Exception ex)
            {
                log.Error("LookupController-AllMapperforSelectedSubsy of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

         [HttpGet]
        [Authorize]
        public object GetAllLookupNames()
        {
            try
            {
                return entityManager.GetAllLookupNames();
            }
            catch (Exception ex)
            {
                log.Error("LookupController-GetAllLookupNames of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                return new { Result = "Error", Message = "Unable to process your request.Please try again" };
            }
        }

         [HttpPost]
         public object AddLookup(ArrayList rowDetails)
         {
             try
             {

                 lookupManager.AddLookup(rowDetails, User.Identity.Name);
                 responseModel.Success = true;
                 responseModel.Message = "LookUp Values has been successfully added.";
             }
             catch (DbUpdateException exe)
             {
                 log.Error("LookupController-AddLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + exe.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Entered Combination/Combinations already exists";

             }
             catch (Exception ex)
             {
                 log.Error("LookupController-AddLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Failed to add Lookup Value.Please check and try again";
             }
             return responseModel;
         }
         [HttpPost]
         public object AddLookupDialog(ArrayList rowDetails)
         {
             try
             {

                 lookupManager.AddLookupForUpload(rowDetails, User.Identity.Name);
                 responseModel.Success = true;
                 responseModel.Message = "LookUp Values has been successfully added.";
             }
             catch (DbUpdateException exe)
             {
                 log.Error("LookupController-AddLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + exe.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Entered Combination/Combinations already exists";

             }
             catch (Exception ex)
             {
                 log.Error("LookupController-AddLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Failed to add Lookup Value.Please check and try again";
             }
             return responseModel;
         }
         [HttpPut]
         [Authorize]
         public object EditLookup(LookupModel LookupModelData)
         {
             try
             {
                 lookupManager.EditLookup(LookupModelData, User.Identity.Name);
                 responseModel.Success = true;
                 responseModel.Message = "LookUp Values has been successfully edited.";
             }
             catch (DbUpdateException exe)
             {
                 log.Error("LookupController-EditLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + exe.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Entered Combination/Combinations already exists";

             }
             catch (Exception ex)
             {
                 log.Error("LookupController-EditLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Failed to edit Lookup Value.Please check and try again";
             }
             return responseModel;
         }

         [HttpPut]
         [Authorize]
         public object EditLookupNew(LookUpModelUpdate LookupModelData)
         {
             try
             {
                 lookupManager.EditLookupNew(LookupModelData, User.Identity.Name);
                 responseModel.Success = true;
                 responseModel.Message = "LookUp Values has been successfully edited.";
             }
             catch (DbUpdateException exe)
             {
                 log.Error("LookupController-EditLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + exe.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Entered Combination/Combinations already exists";

             }
             catch (Exception ex)
             {
                 log.Error("LookupController-EditLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Failed to edit Lookup Value.Please check and try again";
             }
             return responseModel;
         }

         [HttpDelete]
         [Authorize]
         public object DeleteLookup(int lookupRowId)
         {
             try
             {
                 lookupManager.DeleteSelectedLookupDetails(lookupRowId, User.Identity.Name);

                 responseModel.Success = true;
                 responseModel.Message = "LookUp Values has been successfully deleted.";

             }
             catch (Exception ex)
             {
                 log.Error("LookupController-DeleteLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 responseModel.Success = false;
                 responseModel.Message = "Failed to delete Lookup Value.Please check and try again";
             }
             return responseModel;
         }
         [HttpGet]
         [Authorize]
         public JQGridModel ViewLookup(int susbyId, int lookupNameId, string mapperName, int page, int rows, string sidx, string sord)
         {
             try
             {
                 List<ViewLookupModel> responseData = lookupManager.GetLookupDetails(User.Identity.Name, susbyId, lookupNameId,mapperName);

                 return new JQGridModel
                 {
                     LookupData = responseData,
                     Page = page,
                     PageSize = 1,
                     SortColumn = sidx,
                     SortOrder = sord,
                     ModelStatus = true,
                     S_errMsg = "Success"
                 };
             }
             catch (Exception ex)
             {
                 log.Error("LookupController-ViewLookup of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new JQGridModel
                 {
                     LookupData = null,
                     Page = page,
                     PageSize = 1,
                     SortColumn = sidx,
                     SortOrder = sord,
                     ModelStatus = false,
                     S_errMsg = "Unable to process your request.Please try again"
                 };

             }
         }
        
         [HttpGet]
         [Authorize]
         public object GetisMapperASN(string mapperName)
         {
             try
             {
                 return new { Status = entityManager.GetisMapperASN(mapperName) };
             }
             catch (Exception ex)
             {
                 log.Error("LookupController-GetAllLookupNames of Username" + User.Identity.Name + " ,Api Exception:-Message:" + ex.Message);
                 return new { Result = "Error", Message = "Unable to process your request.Please try again" };
             }
         }
    }
}
