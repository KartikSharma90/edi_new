﻿using GPArchitecture.GoodPackBusinessLayer.ReportManagerLayer;
using GPArchitecture.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;

namespace GoodpackEDI.Controllers
{
    public class TransactionSOSearchODataController : ODataController
    {
        private IReportManager reportManager = new ReportManager();
        [EnableQuery]
        public IQueryable<ReportSOTransactionModel> Get(int transactionId)
        {
           
            var responseData = reportManager.GetSOTransactionReport(transactionId, User.Identity.Name);
            return (from n in responseData
                    select new ReportSOTransactionModel
                    {
                        FileName = n.FileName,
                        UploadedDate = n.UploadedDate,
                        Batchid = n.Batchid,
                        CustomerNumber = n.CustomerNumber,
                        CustomerPONumber = n.CustomerPONumber,
                        CustomerPODate = n.CustomerPODate,
                        MaterialNumber = n.MaterialNumber,
                        FromLocation = n.FromLocation,
                        Consignee = n.Consignee,
                        SOQuantity = n.SOQuantity,
                        POD = n.POD,
                        POL = n.POL,
                        ETA = n.ETA,
                        ETD = n.ETD,
                        ReqDeliveryDate = n.ReqDeliveryDate,
                        Mode = n.Mode,
                        UniqueId=n.UniqueId,
                        Status = n.Status//,
                        //LineContent=n.LineContent
                    }).AsQueryable();

        }
    }
}
