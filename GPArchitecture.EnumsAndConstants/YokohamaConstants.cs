﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.EnumsAndConstants
{
    public class YokohamaConstants
    {
        public const string SINumber = "SI Number";
        public const string Plant = "Plant";
        public const string Filler = "Filler";
    }
    public class YokohamaGenValue
    {
        public string FieldName { get; set; }
        public string key { get; set; }
        public string value { get; set; }
    }
}