﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.EnumsAndConstants
{
    public class GoodyearFieldModel
    {
        public static string GoodyearSubsiName = "GoodyearCorporation";
        private int fieldSeq;
        private int length;
        private string fieldName;

        public int FieldSeq
        {
            get { return fieldSeq; }
            set { fieldSeq = value; }
        }
        public int Length
        {
            get { return length; }
            set { length = value; }
        }
        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }
        
    }
}