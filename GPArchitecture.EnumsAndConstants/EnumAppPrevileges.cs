﻿
namespace GPArchitecture.EnumsAndConstants
{
    public enum EnumAppPrevileges
    {
        Administrator,        
        ShipmentConfirmation,
        SubsiMapping,
        AddLookup,
        SalesOrder,
        MappingConfiguration,
        DataLookUp,
        AddMapping,
        ScannedShipmentConfirmation,
        SalesOrderMassEdit,
        AdvancedShipmentNotification,
        SalesOrderCancel,
        ErrorEmailDetails,
     };
}