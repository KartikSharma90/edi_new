﻿
namespace GPArchitecture.EnumsAndConstants
{
    public static class ConstantUtilities
    {       
        public static string RegSuccessfullMessage = "Successfully Registered";
        public static string RegUserAlreadyRegisteredMessage = "User Already Registered";      
        public static string SAPUsername = System.Configuration.ConfigurationManager.AppSettings["SAPUsername"].ToString();
        public static string SAPPassword = System.Configuration.ConfigurationManager.AppSettings["SAPPassword"].ToString();
        public static string ADDomain = "CUSTPORTAL";  
        public static string Admin="Admin";
        public static string Status = "completed";

        public static char[] delimiters = { '|' };
        public static string[] delimitersNew = new string[] { "|", ",", ";" };
        public static string Quantity = "1";
        public static string DateFormat = "dd.MM.yyyy";

        public const string SemiColonDelimited = "SemiColon Delimited";
        public const string Excel = "Excel";
        public const string CSV = "CSV";       
        public const string Semi_Colon = ";";
        public const string Comma = ",";
        public const string Fixed = "FIXED";
        public const string Service = "GPMapper";
        public const string LookupName_CustomerCode = "Customer Code";
        public const string Packer = "Packer";
        public const string Consignee = "Consignee";
        public const string OrderQuantity = "Order quantity";
        public const string SIQuantity = "Quantity";
        public const string CustomerCode = "Customer Code";
        public const string POL = "Port";
        public const string POD = "Port";
        public const string Port = "Port";
        public const string DateValidator = "DATE";
        public const string ShippingLine="Shipping Line";
        public const string GoodyearSOMapper = "GoodYearSalesOrder";
        public const string GoodyearSCMapper = "GoodYearShipmentConfirmation";
        public const string SalesDocumentItem = "10";
        public const string ContinentalSOMapper = "ContinentalSOMapper";



        public const string AdminForSc = "AdminForSC";


        public const string InputType = "InputType";
        
        public const string FileData = "FileData";
        
        public const string CharacterValidator = "Character";
        public const string ValueValidator = "Value";
        public const string FileHeader = "Header";
        public const string MapperName = "Mapper";
        public const string SAPField = "SAPField";
        public const string CommonValidator = "CommonValidator";
        public const string TransactionType = "TransactionType";
        public const string MonthDateFormat = "dd-mmm-yyyy";

        public const string DateFormatValidationMessage = "Invalid date format for ";
        public const string DateFormatValidationeMapperMessage = "Valid Formate is  ";
        public const string ValueValidationMessage = "Value cannot be empty for ";
        public const string CharacterValidationMessage = "Character shouldn't exceed ";
        public const string NewLine = "\n<br></br>";
    
    }
}