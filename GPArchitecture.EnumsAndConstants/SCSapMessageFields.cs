﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.EnumsAndConstants
{
    public static class SCSapMessageFields
    {
         public const string DocumentType = "Document Type";
         public const string ITRNumber = "ITR Number";
         public const string ReferenceITRNumber = "Reference ITR Number";
         public const string FromLocation = "From Location";
         public const string ToLocation = "To Location";
         public const string Headertext = "Header text";
         public const string OceanFreightIndicator = "Ocean Freight Indicator";
         public const string CustomerReferenceNumber = "Customer Reference Number";
         public const string FlightBookingCodes = "Flight Booking Codes";
         public const string VesselETD = "Vessel ETD";
         public const string VesselETA = "Vessel ETA";
         public const string VesselATD = "Vessel ATD";
         public const string VesselATA = "Vessel ATA";
         public const string ShippingLine = "Shipping Line";
         public const string VesselName = "Vessel Name";
         public const string VoyageReference = "Voyage Reference";
         public const string POL = "POL";
         public const string POD = "POD";
         public const string CustomDocumentReference = "Custom Document Reference";
         public const string TranshipmentPort = "Transhipment Port";
         public const string TranshipmentETD = "Transhipment ETD";
         public const string TranshipmentETA = "Transhipment ETA";
         public const string ItemNo = "Item No.";
         public const string MaterialNumber = "Material Number";
         public const string Quantity = "Quantity";
         public const string ContainerNumber = "Container Number";

         public const string CustomerNumber = "Customer Number";
         public const string SINumber = "SI Number";
         public const string SalesDocument = "Sales Document";
         public const string SalesDocumentItem = "Sales Document Item";
         public const string GoodsIssueDate = "Goods Issue Date";
         public const string ETDDate = "ETD Date";
         public const string DTADate = "DTA Date";
         public const string Remarks = "Remarks";
         public const string Damage = "Damage";
         public const string SupplementFlag = "Supplement Flag";

         public const string BatchNumber = "Batch Number";
         public const string TwoDPin = "2D Pin No";
         public const string FRMLocation = "FRM Location";
         public const string TOLocation = "TO Location";
         public const string MvtType="Mvt Type";
         public const string CustomerPoNumber="Customer PO Number";
         public const string UnitOfMeasure="Base Unit of Measure";
         public const string ETADate="ETA Date";
         public const string ScanDate="Scan Date";
         public const string Status="Status";
         public const string CustomerCode="Customer Code";
         public const string DateOfRecord = "Date on which record was created";
         public const string LineId = "LineId";
         public const string EdiNumber = "EDINumber";
         public const string EdiReference = "EDIReference";
         public const string DeviceId = "Device Id";

         public const string DRYAGE_SHIPMENT = "DRYAGE_SHIPMENT";
         public const string PLANT = "PLANT";
         public const string SKU = "SKU";
         public const string SKU_TEXT = "SKU_TEXT";
         public const string SHIP_TO = "SHIP_TO";
         public const string ST_NAME = "ST_NAME";
         public const string BATCH = "BATCH";
         public const string ADDFIELD1 = "ADDFIELD1";
         public const string ADDFIELD2 = "ADDFIELD2";
         public const string ADDFIELD3 = "ADDFIELD3";
         public const string ADDFIELD4 = "ADDFIELD4";
         public const string ADDFIELD5 = "ADDFIELD5";


    }
}