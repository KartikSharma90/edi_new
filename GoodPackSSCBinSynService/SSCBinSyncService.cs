﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackSSCBinSynService
{
    public partial class SSCBinSyncService : ServiceBase
    {
        internal static ILog log = LogManager.GetLogger(typeof(SSCBinSyncService));
        public System.Timers.Timer sscBinSyncTimer;
        private static int NoofAttempts = 0;
        SSCBinSync binsync = new SSCBinSync();
        public SSCBinSyncService()
        {
            log.Info("Entering into constructor");
            InitializeComponent();
            log.Info("Leaving into constructor");
        }
        public void debugger()
        {
            log.Info("Entering into Start method");
            OnStart(null);
            log.Info("Leaving Start method");
        }
        protected override void OnStart(string[] args)
        {
            log.Debug("Entering TermTripUpdater OnStart");
            this.sscBinSyncTimer = new System.Timers.Timer();
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            this.sscBinSyncTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.sscBinSyncTimer.Elapsed += sscBinSyncTimer_Elapsed;
            this.sscBinSyncTimer.Enabled = true;
            this.sscBinSyncTimer.Start();
            log.Debug("Leaving TermTripUpdater Service OnStart");
            // while (true) { }
        }

        void sscBinSyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            log.Debug("sscBinSyncTimer Start");
            if (!binsync.isProcessing)
                binsync.SSCBinSyncFunction();
            log.Debug("sscBinSyncTimer Start_End");
        }

        protected override void OnStop()
        {
            this.sscBinSyncTimer.Stop();
            while (binsync.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    log.Warn("TermTrip Services Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
