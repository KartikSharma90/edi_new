﻿using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPArchitecture.SAPService;
using log4net;
using GoodpackEDI.ViewModels;

namespace GoodPackSSCBinSynService
{
    public class SSCBinSync
    {
        internal static ILog log = LogManager.GetLogger(typeof(SSCBinSync));
        public bool isProcessing = false;
        private readonly GoodpackEDIEntities context = null;
        public SSCBinSync()
        {
            context = new GoodpackEDIEntities();
        }
        public void SSCBinSyncFunction()
        {
            try
            {
                isProcessing = true;
                log.Info("SSCBinSyncFunction entered");
                List<ResponseModel> responseModel = new List<ResponseModel>();
                SSCBinNumberUploadService service = new SSCBinNumberUploadService();
                //to get the details of the line number referenced to Trn_SSCTransLineNew
                var binList = (from s in context.Trn_ScannedShipmentDetails
                               join x in context.Trn_SSCTransLineNew
                               on s.Id equals x.LineNumber
                               where x.Trn_LineStatus.StatusCode == "SUCCESS" && s.SAPBinStatus == null
                               select new { s.Barcode, s.CustomerReferenceNumber, s.CustomerCode, s.Id,s.BatchId }).ToList();
// log.Info(binList);
                foreach (var customerDetail in binList)
                {
                    try
                    {
//To get the details of all scanned bins under the customer reference -batchid
                        var scannedDetails = context.Trn_ScannedShipmentDetails.Where(i => i.BatchId == customerDetail.BatchId && i.CustomerReferenceNumber == customerDetail.CustomerReferenceNumber && i.SAPBinStatus == null).Select(i => new { i.Barcode, i.CustomerReferenceNumber, customerDetail.CustomerCode,i.Id }).ToList();
                        foreach (var barcodeDetails in scannedDetails)
                        {
                            log.Info(" Data CustCode:-" + barcodeDetails.CustomerCode + " CustRefNo-" + barcodeDetails.CustomerReferenceNumber + "Barcode:-" + barcodeDetails.Barcode + "ScannedDetailId:-" + barcodeDetails.Id);
                            responseModel = service.SSCBinSyncService(barcodeDetails.CustomerCode, barcodeDetails.CustomerReferenceNumber, barcodeDetails.Barcode);
                            foreach (var responseVal in responseModel)
                            {
                                log.Info("Sap status-" + responseVal.Success);
                                if (responseVal.Success != null)
                                    UpdateSSCBinNumberSAPStatus(barcodeDetails.Id, responseVal.Success, responseVal.Message);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("SSCBinSyncFunction Error -" + e);
                    }
                }
                isProcessing = false;
                log.Info("SSCBinSyncFunction left");
            }
            catch (Exception e)
            {
                isProcessing = false;
                log.Error("SSCBinSyncFunction-" + e);
            }
        }
        public void UpdateSSCBinNumberSAPStatus(int Id, bool? status, string message)
        {
            Trn_ScannedShipmentDetails sscShipmentDetails = context.Trn_ScannedShipmentDetails.FirstOrDefault(i => i.Id == Id);
            var statusVal = (bool)status ? context.Trn_LineStatus.Where(i => i.StatusCode == "SUCCESS").Select(i => i.Id).FirstOrDefault()
                                                      : context.Trn_LineStatus.Where(i => i.StatusCode == "ERROR").Select(i => i.Id).FirstOrDefault();
            sscShipmentDetails.SAPBinStatus = statusVal;
            sscShipmentDetails.SAPBinSyncMessage = message;           
            context.SaveChanges();
        }
    }
}
