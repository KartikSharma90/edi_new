﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ResponseLineDataModel
    {
        public bool Status { get; set; }
        public int LineId { get; set; }
        public List<string> LineMessage { get; set; }
    }
}