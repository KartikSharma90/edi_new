﻿using System;

namespace GPArchitecture.Models
{
    public class Record
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailId { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public string FullName { get; set; }
        public bool ReceiveAdminEmail { get; set; }
        public bool ReceiveNotifications { get; set; }
        public string Company { get; set; }
        //Role Id in Jtable format
        public int Value { get; set; }      
                      
                                       
    }
}