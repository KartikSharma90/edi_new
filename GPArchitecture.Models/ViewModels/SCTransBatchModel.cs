﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SCTransBatchModel
    {
        public int UniqueId { get; set; }
        public string SubsyName { get; set; }
        public string TransactionName { get; set; }
        public string TransactionType { get; set; }
        public string FileName { get; set; }
        public int? TotalRecords { get; set; }
        public int SuccessCount { get; set; }
        public int ErrorCount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string MapperName { get; set; }
        public int? DeletedCount { get; set; }  
    }
}