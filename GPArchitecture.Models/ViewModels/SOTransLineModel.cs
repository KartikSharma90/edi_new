﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SOTransLineModel
    {
        public string CustomerPONumber { get; set; }
        public string CustomerPODate { get; set; }
        public string CustomerNumber { get; set; }
        public string ReqDeliveryDate { get; set; }
        public string MaterialNumber { get; set; }
        public string FromLocation { get; set; }
        public string SOQuantity { get; set; }
        public string Status { get; set; }
        public string Consignee { get; set; }
        

        public string ETD { get; set; }
        public string ETA { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }

        public string LineContent { get; set; }
        public string SAPStatus { get; set; }
        public int Batchid { get; set; }
        public int UniqueId { get; set; }
        public string Mode { get; set; }
    }
}