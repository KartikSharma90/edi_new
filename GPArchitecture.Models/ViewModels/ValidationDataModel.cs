﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Models.ViewModels
{
    public class ValidationDataModel
    {
        public string DateString { get; set; }
        public List<CharacterList> CharList { get; set; }
        public List<BoolList> BoolList { get; set; }
        //public ValidationDataModel()
        //{
        //    CharList = new CharacterList();
        //    BoolList = new BoolList();
        //}
             
    }
    public class CharacterList
    {
        public int? MaxLength { get; set; }
        public string SAPName { get; set; }
    }
    public class BoolList
    {
        public bool IsNullAllowed { get; set; }
        public string SAPName { get; set; }
    }
}
