﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ReasonModel
    {

        public string ReasonType { get; set; }
        public string ReasonDescription { get; set; }
        public int ReasonCode { get; set; }
    }
}