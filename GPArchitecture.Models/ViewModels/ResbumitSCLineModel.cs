﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ResbumitSCLineModel
    {
        public int BatchId { get; set; }
        public List<int> LineIds { get; set; }
    }
}