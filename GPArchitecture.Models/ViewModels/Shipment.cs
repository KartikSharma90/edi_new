﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{

    public class ShipmentSync
    {
        public string CustomerReference { get; set; }
        public string ConsigneeId { get; set; }
        public string SINumber { get; set; }
        public string ETA { get; set; }
        public string ETD { get; set; }
        public string BinType { get; set; }
        public string Barcode { get; set; }
    }
    public class ShipmentData
    {
        public List<ShipmentSync> scandata { get; set; }
        public string DeviceId { get; set; }
        public ShipmentData()
        {
            scandata = new List<ShipmentSync>();
        }
    }
    public class ShipmentResponse
    {
        public string CustomerReference { get; set; }
        public bool Status { get; set; }
        public string Message  { get; set; }
    }
    public class RegisterScannerResponse
    {
        public string message { get; set; }
        public bool status { get; set; }
        public RegisterScanner registerScanner { get; set; }

    }
    public class RegisterScanner
    {
        public string Vertical { get; set; }
        public List<string> BinTypes { get; set; }
    }	

}