﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Models.ViewModels
{
    public class LSPCRModel
    {
        public string JobId { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public string From { get; set; }
        public string FromAddress { get; set; }
        public string To { get; set; }
        public string ToAddress { get; set; }
        public string SKU { get; set; }
        public string Quantity { get; set; }
        public string ItrQuantity { get; set; }
        public string ProposedPickUpDate { get; set; }
        public string ConfirmedPickUpDate { get; set; }
        public string Vendor { get; set; }
        public string Remarks { get; set; }
        public string Name { get; set; }
        public string status { get; set; }
    }
}
