﻿
namespace GPArchitecture.Models
{
    public class DropdownModel
    {
        public int Value { get; set; }
        public string DisplayText { get; set; }
    }
}