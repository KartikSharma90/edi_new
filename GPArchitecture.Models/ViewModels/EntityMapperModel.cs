﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class EntityMapperModel
    {
        public string Vertical { get; set; }
        public int EntityCode { get; set; }
        public string ContractNumber { get; set; }
    }
}