﻿
namespace GPArchitecture.Models
{
    public class UserLoginDataModel
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public bool IsActive { get; set; }
       
    }
}