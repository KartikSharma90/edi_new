﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SOResponseModel
    {
        public string RowNumber { get; set; }
        public string ItemNumber { get; set; }
        public string PONumber { get; set; }
        public string MessageType { get; set; }
        public string MessageContent { get; set; }
    }
}