﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class VesselTransitTimeModel
    {
        public int Id { get; set; }
        public string POL { get; set; }
        public string POD { get; set; }
        public int TransitTime { get; set; }
        public string ShippingLine { get; set; }
        public int VoyageScheduleLookupVarianceInDay { get; set; }
    }
}