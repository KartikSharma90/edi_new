﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class DeleteLineModel
    {
        public int TransactionId { get; set; }
        public string DeleteComment { get; set; }
        public string  DeleteIds { get; set; }
    }
}