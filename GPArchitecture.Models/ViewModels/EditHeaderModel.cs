﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class EditHeaderModel
    {
        public string HeaderName { get; set; }
        public int SRNO { get; set; }
    }
}