﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SCSubsiToTransactionModel
    {
        public int Id { get; set; }
        public int SubsiId { get; set; }
        public int MessageFormatId { get; set; }
        public int TransactionCodeId { get; set; }
	    public int MinFieldValueCount { get; set; }
        public bool IsHeaderPresent { get; set; }        
    }
}