﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ViewMapperDataModel
    {
        public int UniqueId { get; set; }
        public string SubsyName { get; set; }
        public string TransactionType { get; set; }
        public string FileType { get; set; }
        public string MapperName { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy  { get; set; }
    }
}