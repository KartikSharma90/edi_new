﻿
namespace GPArchitecture.Models
{
    public class LoginResponseModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public PrivilegeAccessModel AccessLinks { get; set; }
    }
}