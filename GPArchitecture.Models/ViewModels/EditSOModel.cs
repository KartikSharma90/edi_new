﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class EditSOModel
    {
        public string customerCode { get; set; }
        public List<string> customerPNos { get; set; }
        public  List<string> deliveryLocations { get; set; }
        public List<string> delDates { get; set; }
        public List<string> matNos { get; set; }
        public List<string> orderQtys { get; set; }
        public string materialNo { get; set; }
        public int orderQty { get; set; }
        public string deliveryDt { get; set; }
        public List<string> reason1 { get; set; }
        public List<string> reason2 { get; set; }
        public List<string> reason3 { get; set; }


    }
}