﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SCTransMappingModel
    {
        //Ref_TransactionType and Ref_SAPMessageFields Models
        public int Id { get; set; }
        public string DefaultValue { get; set; }
        public string SAPFieldName { get; set; }
        public int FieldSequence { get; set; }
        public string FieldType { get; set; }
        public string InputType { get; set; }
        public int MaxLength { get; set; }
        public string Note { get; set; }
        public string Operation { get; set; }
        public string OperationParam { get; set; }
        public int TransactionId { get; set; }
        public string Format { get; set; }
        public string TransactionName { get; set; }
        public string TransactionCode { get; set; }
        public bool IsMandatory { get; set; }
        public string ValidationMessage { get; set; }
        public bool SkipOnValidationFailure { get; set; }

    }
}