﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPArchitecture.Models.ViewModels
{
    public class ProActiveDehireModel
    {
        public List<string> BinList { get; set; }
        public List<DateTime> Dates { get; set; }
        public string ConsigneeCode { get; set; }
    }

    public class ProActiveDehireReturnModel
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public List<ProactiveResponseData> ResponseData { get; set; }
    }
    public class ProactiveResponseData
    {
        public string CollectionDate { get; set; }
        public string BinType { get; set; }
        public string Quantity { get; set; }
        public string Remark { get; set; }
        public string ResponseMessage { get; set; }
        public bool Status { get; set; }
    }
    public class ProActiveDehireSubmitModel
    {
        public string CollectionDate { get; set; }
        //public string MB3 { get; set; }
        //public string MB4{ get; set; }
        //public string MB5 { get; set; }
        //public string MB7 { get; set; }
        //public string MB5H{ get; set; }
        //public string MB5HE{ get; set; }
        public Dictionary<string, string> BinQty { get; set; }
        public string Remark { get; set; }
        public string Consigneecode { get; set; }
    }
    public class DehireViewModel
    {
        public ProActiveDehireModel dehire { get; set; }
        public List<ProActiveDehireSubmitModel> dehireSubmit { get; set; }
    }
}
