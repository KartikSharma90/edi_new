﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class UserToSubsiModel
    {
        public IList<SubsiModel> SubsiList { get; set; }
        public IList<SubsiModel> MappedSubsi { get; set; }
    }
}