﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ErrorEmailReturnModel
    {

        public List<ErrorEmailDetailsModel> EmailBatchModel { get; set; }
        public int totalRecordCount { get; set; }

    }
}