﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class CancelSOModel
    {
        public string customerCode { get; set; }
        public List<SOEditLine> LineContent { get; set; }
        public string reason1 { get; set; }
    }
}