﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class SOEditLine
    {
        public string customerPONos { get; set; }
        public string materialNos { get; set; }
        public string orderQtys { get; set; }
        public string deliveryDates { get; set; }
        public string deliveryLocations { get; set; }
    }
}