﻿
namespace GPArchitecture.Models
{
    public class UserRegistrationModel
    {

        public string RegUsername { get; set; }
        public string RegPassword { get; set; }
        public string Fullname { get; set; }       
        public string EmailId { get; set; }
        public string Company { get; set; }
        public int RoleId { get; set; }       
        public bool IsActive { get; set; }       
        public bool ReceiveAdminEmail { get; set; }
        public bool ReceiveNotifications { get; set; }      
       
    }
}