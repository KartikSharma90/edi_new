﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class JQGridModel
    {
        public bool ModelStatus { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortOrder { get; set; }
        public string S_errMsg { get; set; }
        public int totalrecords { get; set; }
        public int totalpages { get; set; }
        public List<SalesOrderModel> sData { get; set; }
        public List<SCTransBatchModel> TransactionData { get; set; }
        public List<SCTransLineModel> TranLineData { get; set; }
        public List<ViewLookupModel> LookupData { get; set; }
        public IList<ViewMapperDataModel> ViewMappingData { get; set; }
        public IList<ViewSelectedMapperDataModel> ViewSelectedMappingData { get; set; }
        public List<SSCTransLineModel> TranSSCLineData { get; set; }
        public List<SOTransLineModel> TranSOLineData { get; set; }
        public List<ASNTransLineModel> TranASNLineData { get; set; }
        public List<ErrorEmailDetailsModel> ErrorEmailData { get; set; }
    }
}