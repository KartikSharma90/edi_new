﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class MapperDataModel
    {
        public int Id { get; set; }
        public int SourceFieldSequence { get; set; }
        public int MappingSubsiId { get; set; }
        public int SapFieldSeq { get; set; }
        public string InputDateFormat { get; set; }
        public string FixedValue { get; set; }
        public int SourceFieldSeq2 { get; set; }
        public int SourceFieldSeq3 { get; set; }
        public string MappingType { get; set; }
        public int SourceFieldSeq1SubStringStartPosition { get; set; }
        public int SourceFieldSeq1SubStringLength { get; set; } 

    }
}