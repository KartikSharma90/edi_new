﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ViewSelectedMapperDataModel
    {
        public string SAPField { get; set; }
        public string InputField { get; set; }
        public string Notes { get; set; }
    }
}