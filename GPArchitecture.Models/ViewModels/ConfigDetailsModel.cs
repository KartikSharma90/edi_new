﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ConfigDetailsModel
    {
        public int Id { get; set; }
        public string ConfigItem { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string LastUpdatedBy { get; set; }
        public System.DateTime LastUpdatedDate { get; set; }
    }
}