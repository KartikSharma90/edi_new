﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class EntityModel
    {
        public int EntityCode { get; set; }
        public string EntityName { get; set; }
    }
}