﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class ErrorEmailDetailsModel
    {
        public int UniqueId { get; set; }
        public string SubsyName { get; set; }
        public string EmailId { get; set; }
        public string TransactionName { get; set; }
        public string FileName { get; set; }
        public string FileContent { get; set; }
        public string MapperName { get; set; }
        public string CreatedDate { get; set; }
        public string ErrorMessage { get; set; }

    }
}