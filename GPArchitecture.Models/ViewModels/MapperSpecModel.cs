﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Models
{
    public class MapperSpecModel
    {
        public IList<MappingFieldSpecModel> SpecModel { get; set; }
        public SCSubsiToTransactionModel SubsiTransModel { get; set; }
    }
}