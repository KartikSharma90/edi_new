﻿using log4net;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using LumenWorks.Framework.IO.Csv;
namespace GPArchitecture.Utilities
{
    public class ExcelParser
    {

        private static readonly ILog log = LogManager.GetLogger(typeof(ExcelParser));

        public static string ExcelToCSV(string path, int columnCount, int sheetindex)
        {
            StringBuilder csvStringBuilder = new StringBuilder();

            try
            {
                //Choose the class according to Excel file type
                IWorkbook workbook;
                if (path.EndsWith(".xls"))
                {
                    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        workbook = new HSSFWorkbook(file);
                    }
                }
                else
                {
                    using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                    {
                        workbook = new XSSFWorkbook(file);
                    }
                }
                ISheet sheet = workbook.GetSheetAt(sheetindex);
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                //Use the column count of header in case that end values are missing in the rows
                if (columnCount < 1)
                {
                    IRow hdrrow = sheet.GetRow(0);
                    columnCount = hdrrow.LastCellNum;
                }
                //the below loop attempts to create the message content in a format similar to CSV so that
                //the same parser can be used

                IRow row;
                int rowCount=0;
                int rowNumber = 0;
                bool isEmpty = false;
                while (rows.MoveNext())
                {
                    int itrationCount = 0;
                    isEmpty = false;
                    row = (IRow)rows.Current;
                    for (int i = 0; i < columnCount; i++)
                    {
                        
                            ICell cell = row.GetCell(i);
                            try
                            {
                            if (string.IsNullOrEmpty(cell==null ? null: cell.ToString().Trim()))
                            {
                                itrationCount++;
                                if (columnCount == itrationCount)
                                {
                                    isEmpty = true;
                                 
                                }

                            }
                            else
                            {
                                if (cell.CellType == CellType.Formula)
                                {                                    
                                    if (cell.CachedFormulaResultType == CellType.Numeric && DateUtil.IsCellDateFormatted(cell))
                                    {
                                        csvStringBuilder.Append(cell.DateCellValue.ToString("dd-MM-yyyy"));
                                    }
                                    else if (cell.CachedFormulaResultType == CellType.Numeric)
                                    {
                                        csvStringBuilder.Append(cell.NumericCellValue.ToString("#"));
                                    }
                                    else
                                    {
                                        if (checkIsDate(cell))
                                        {
                                            csvStringBuilder.Append(cell.DateCellValue.ToString("dd-MM-yyyy"));
                                        }
                                        else
                                        {
                                            csvStringBuilder.Append(@"""")
                                                .Append(cell.StringCellValue.ToString().Replace("\r\n", "").Replace("\"", @""""""))
                                                .Append(@"""");
                                        }
                                    }
                                }
                                else
                                {

                                    //  string date = cell.DateCellValue.ToString();
                                    if (cell.CellType == CellType.Numeric && DateUtil.IsCellDateFormatted(cell))
                                    {
                                        csvStringBuilder.Append(cell.DateCellValue.ToString("dd-MM-yyyy"));
                                    }
                                    else if (cell.CellType == CellType.Numeric)
                                    {
                                        csvStringBuilder.Append(cell.NumericCellValue.ToString("#.####"));

                                    }
                                    else
                                    {
                                        if (checkIsDate(cell))
                                        {
                                            DateTime dt = DateTime.ParseExact(cell.ToString(), "dd/MM/yyyy", null);
                                            csvStringBuilder.Append(dt.ToString("dd-MM-yyyy"));
                                        }
                                        else
                                        {
                                            csvStringBuilder.Append(@"""")
                                            .Append(cell.ToString().Replace("\r\n", "").Replace("\"", @""""""))
                                            .Append(@"""");
                                        }
                                    }
                                }
                            }
                            if (i < columnCount - 1)
                                csvStringBuilder.Append("" + ConstantUtilities.delimiters[0]);
                        }
                        catch (Exception e)                        {
                            log.Error(" EXCELtoCSV in ExcelParser .path:" + path + ", columnCount:" + columnCount + ", sheetindex:" + sheetindex + ".  Exception : Message- " + e.Message);
                            csvStringBuilder.Append(@"""")
                                           .Append(cell.ToString().Replace("\r\n", "").Replace("\"", @""""""))
                                           .Append(@"""");
                            if (i < columnCount - 1)
                                csvStringBuilder.Append("" + ConstantUtilities.delimiters[0]);
                        }
                        
                    }
                    if (rowCount == 0)
                    {
                        csvStringBuilder.Replace("\n", " ");
                        rowCount++;
                    }
                    if (isEmpty)
                    {                        
                        if (csvStringBuilder.ToString().LastIndexOf(Environment.NewLine) != -1)
                        {
                            StringBuilder strold = new StringBuilder();
                            strold.Append(csvStringBuilder.ToString().Substring(0, csvStringBuilder.ToString().LastIndexOf(Environment.NewLine)));
                            csvStringBuilder = strold;
                        }
                    }
                    csvStringBuilder.Append(Environment.NewLine);
                    rowNumber++;
                }
            }
            finally
            { }
            return csvStringBuilder.ToString();
        }

        //public void DeleteRow(this ISheet sheet, IRow row)
        //{
        //    sheet.RemoveRow(row);   // this only deletes all the cell values

        //    int rowIndex = row.RowNum;

        //    int lastRowNum = sheet.LastRowNum;

        //    if (rowIndex >= 0 && rowIndex < lastRowNum)
        //    {
        //        sheet.ShiftRows(rowIndex + 1, lastRowNum, -1);
        //    }
        //}
        private static bool checkIsDate(ICell cell)
        {
            try
            {
                if (cell.CellType != CellType.Blank)
                {
                    DateTime temp;
                    if (DateTime.TryParse(cell.ToString(), out temp))
                    {
                        return true;
                    }                   
                    else                       
                        return false;
                }
                else
                {
                    return false;                
                }
            }
            catch (Exception e)
            {
                return false;
            }
        
        }

        public static string GetSumitomoFileContent(string message)
        {
            bool isSuccess = true;
            StringBuilder fileContent = new StringBuilder();
            try
            {
                CsvReader csvReader = new CsvReader(
                    new StringReader(message)
                    , false
                    , '|');
                while (csvReader.ReadNextRecord())
                {
                    List<string> fieldContentList = new List<string>();
                    // If isSuccess is false, then end processing
                    if (!isSuccess)
                        break;

                    int iContainsFieldCount = 0;
                    string fieldValue = null;
                    for (int ctr = 0; ctr < csvReader.FieldCount; ctr++)
                    {
                        //Modified to count number of non-empty fields
                        if (!string.IsNullOrEmpty(csvReader[ctr]))
                        {
                            iContainsFieldCount++;
                        }
                    }

                    //If number of non-empty fields is less than a minimum configured number, ignore the record
                    // If field count is greater than 18 ,then check if it's the field header row
                    if (iContainsFieldCount < 18)
                    {
                        continue;
                    }
                    else
                    {
                        for (int clm = 0; clm < csvReader.FieldCount; clm++)
                        {
                            fieldValue = csvReader[clm];
                            fieldContentList.Add(fieldValue);
                        }
                        for (int i = 0; i < fieldContentList.Count; i++)
                        {
                            string value = fieldContentList[i];
                            fileContent.Append('"');
                            fileContent.Append(value);
                            fileContent.Append('"');
                            if (i < fieldContentList.Count - 1)
                                fileContent.Append("|");
                        }
                    }
                    fileContent.Append(Environment.NewLine);
                }
                return fileContent.ToString();
            }
            catch (Exception ex)
            {
                log.Error("Error On Creating Sumitomo File Contents", ex);
                throw ex;
            }
        }

    }
}