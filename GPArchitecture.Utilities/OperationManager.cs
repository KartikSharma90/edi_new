﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using log4net;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;

namespace GPArchitecture.Utilities
{   
    public class OperationManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(OperationManager));
        public static string ExecuteOperation(SCTransactionModel objGPCustomerMessage, string operationName, string operationParam)
        {
            return ExecuteOperation(objGPCustomerMessage, operationName, operationParam, string.Empty,string.Empty);
        }
        public static string ExecuteOperation(SCTransactionModel objGPCustomerMessage, string operationName, string operationParam, string sourceField,string mapperName)
        {
            return ExecuteOperation(objGPCustomerMessage, operationName, operationParam, sourceField, string.Empty, string.Empty, string.Empty,mapperName);
        }

        public static string ExecuteOperation(SCTransactionModel objGPCustomerMessage, string operationName, string operationParam, string sourceField1, string sourceField2, string sourceField3, string targetFieldDateFormat,string mapperName)
        {
            string returnVal = String.Empty;
            switch (operationName)
            {
                case "CustomerCodeLookup":
                    returnVal = CustomerCodeLookup(objGPCustomerMessage, operationName, sourceField1,mapperName);
                    break;
                case "VerticalLookup_By_CustomerCode":
                    returnVal = VerticalLookup_By_CustomerCode(objGPCustomerMessage, operationName, sourceField1);
                    break;
                case "VerticalLookup_By_CustomerName":
                    returnVal = VerticalLookup_By_CustomerName(objGPCustomerMessage, operationName, sourceField1);
                    break;
                case "ContractNumberLookup_By_CustomerName":
                    returnVal = ContractNumberLookup_By_CustomerName(objGPCustomerMessage, operationName, sourceField1);
                    break;
                case "ContractNumberLookup_By_CustomerCode":
                    returnVal = ContractNumberLookup_By_CustomerCode(objGPCustomerMessage, operationName);
                    break;
                case "ContractNumberLookup_By_CustomerCode_Direct":
                    returnVal = ContractNumberLookup_By_CustomerCode(operationParam);
                    break;
                case "SubsidiaryLookup_By_CustomerCode":
                    returnVal = SubsidiaryLookup_By_CustomerCode(objGPCustomerMessage, operationName, sourceField1);
                    break;
                case "SubsidiaryLookup_By_CustomerName":
                    returnVal = SubsidiaryLookup_By_CustomerName(objGPCustomerMessage, operationName, sourceField1,mapperName);
                    break;
                case "GetSAPCustomerCode":
                    returnVal = GetSAPCustomerCode(objGPCustomerMessage);
                    break;
                case "GetTransactionReceivedDate":
                    returnVal = GetTransactionReceivedDate(objGPCustomerMessage);
                    break;
                case "GetOrderDate":
                    returnVal = GetOrderDate(objGPCustomerMessage);
                    break;
                //case "CustomerCodeLookUpSC":
                //    returnVal = CustomerCodeLookUpSC(objGPCustomerMessage, operationName, sourceField1, sourceField2,mapperName);
                //    break;
                case "DeriveVesselTransitTime":
                    returnVal = DeriveETDByVesselTransitTime(objGPCustomerMessage, operationName, sourceField1, sourceField2, sourceField3, targetFieldDateFormat);
                    break;
                case "CalculateCheckDigitIn8DigitBarcode":
                    returnVal = CalculateCheckDigitInBarcode(sourceField1);
                    break;
                
                //case "BinPairingLookup":
                //    returnVal = BinPairingLookup(objGPCustomerMessage, operationName, sourceField1);
                //    break;
                default:
                    returnVal = DataVerificationManagerNew.GPLookup(objGPCustomerMessage.SubsiId, operationParam, sourceField1, mapperName);
                    break;
            }
            return returnVal;

        }

        private static string CustomerCodeLookup(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField,string mapperName)
        {
            string CustomerCode = string.Empty;

            // Perform reverse lookup to get customer code
            CustomerCode = DataVerificationManagerNew.GPLookUpForSC(objGPCustomerMessage.SubsiId, sourceField, mapperName);

            //// If customer code is not found based on the name provided, return the customer code of
            //// customer owning the transaction
            //if (CustomerCode == sourceField)
            //{
            //    CustomerCode = GetSAPCustomerCode(objGPCustomerMessage);
            //}

            return CustomerCode;
        }

        private static string DeriveETDByVesselTransitTime(SCTransactionModel objGPCustomerMessage, string operationName, string polValue, string podValue, string arrivalDateString_ddMMyyyy, string targetDateFormat)
        {
            // Init vars
            int vesselPortToPortTransitTimeInDays = 0;
            string etdValue = string.Empty;
            DateTime etd = DateTime.MinValue;
            DateTime etaAtDestinationPort = DateTime.MinValue;
            DateTime closestDateByAdd = DateTime.MinValue;
            DateTime closestDateBySubstract = DateTime.MinValue;
            DateTime arrivalDate;
            string query;
            bool fetchedETAFromVoyageSchedulingMaster = false;


            string eTA = string.Empty;
            int voyageScheduleLookupVarianceInDay = 0;
            string shippingLiner = string.Empty;
            string mapperName = string.Empty;
            //DataTable etaMasterVoyageSchedulingResult;

            // Cleanup target date format
            targetDateFormat = DataVerificationManagerNew.CleanDateFormat(targetDateFormat);

            //Mapper Name is fetching for Continental SO
            mapperName = GPArchitecture.Cache.Config.getConfig(ConstantUtilities.ContinentalSOMapper);

            // Perform data lookup on POL and POD-->change done for continental
            polValue = DataVerificationManagerNew.GPLookup(objGPCustomerMessage.SubsiId, "POL", polValue, false, false, mapperName);
            podValue = DataVerificationManagerNew.GPLookup(objGPCustomerMessage.SubsiId, "POD", podValue, false, false, mapperName);

         
            //Get transit time of vessel to get from POL to POD from REF_VESSEL_TRANSIT_TIME
            if (!string.IsNullOrEmpty(podValue) && !string.IsNullOrEmpty(polValue))
            {
                // Validate and convert provided arrival date
                DateTime.TryParseExact(arrivalDateString_ddMMyyyy, "ddMMyyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out arrivalDate);
                if (arrivalDate == DateTime.MinValue)
                {
                    return etdValue;
                }

                // Get TransitTime and variance from REF_VESSEL_TRANSIT_TIME

                VesselTransitTimeModel vesselTransitTime = DataVerificationManagerNew.GetVesselTransitTime(podValue, polValue);
                if (vesselTransitTime != null)
                {              
                    shippingLiner = vesselTransitTime.ShippingLine != null ? vesselTransitTime.ShippingLine : shippingLiner;
                    vesselPortToPortTransitTimeInDays = vesselTransitTime.TransitTime != null ? vesselTransitTime.TransitTime * 7 : vesselPortToPortTransitTimeInDays;
                    voyageScheduleLookupVarianceInDay = vesselTransitTime.VoyageScheduleLookupVarianceInDay != null ? vesselTransitTime.VoyageScheduleLookupVarianceInDay : voyageScheduleLookupVarianceInDay;

                }
                else
                {
                    return etdValue;
                    log.Info("Transit time is not Added for POL :" + polValue + "and POD :" + podValue);
                }

                // Get ETD date by subtracting the transit time in days from vessel arrival date to POD
                etd = arrivalDate.AddDays(-vesselPortToPortTransitTimeInDays);
                log.Info("ETD is generated as :"+etd);
                

                // Get date that vessel will be arriving to the port of loading from MSTR_VOYAGE_SCHEDULING
                DateTime ETAatDestinationPortValue = DataVerificationManagerNew.GetVoyageSchedule(shippingLiner, polValue, etd.AddDays(-voyageScheduleLookupVarianceInDay).ToString("yyyy-MM-dd"), etd.AddDays(voyageScheduleLookupVarianceInDay).ToString("yyyy-MM-dd"));
                       


               // etaMasterVoyageSchedulingResult = SQLDbHandler.ExecuteQuery("conn", query, null, DbCommandType.STRING_QUERY);

                //if (etaMasterVoyageSchedulingResult != null && etaMasterVoyageSchedulingResult.Rows.Count > 0)
                //{
                if(ETAatDestinationPortValue!=null)
                {
                    // Set default value for closest date to ETD
                    DateTime closestETADateToETD = DateTime.MinValue;

                    // Loop through each result from query
                    //foreach (DataRow row in etaMasterVoyageSchedulingResult.Rows)
                    //{
                    //    if (row[0] != null)
                    //    {
                            DateTime rowResultETADateAtDestinationPort = ETAatDestinationPortValue;

                            //TimeSpan diffBetweenETDAndClostestETADateToETDTimeSpan = etd - closestETADateToETD;
                            //TimeSpan diffBetweenETDAndETAFromResultRowTimeSpan = etd - rowResultETADateAtDestinationPort;

                            long diffBetweenETDAndClostestETADateToETD = Math.Abs(etd.Ticks - closestETADateToETD.Ticks);  // Convert.ToInt32(Math.Abs(diffBetweenETDAndClostestETADateToETDTimeSpan.TotalDays));
                            long diffBetweenETDAndETAFromResultRow = Math.Abs(etd.Ticks - rowResultETADateAtDestinationPort.Ticks);//Convert.ToInt32(Math.Abs(diffBetweenETDAndETAFromResultRowTimeSpan.TotalDays));

                            // If the ETA date retrieved from the table is closer to the ETD date, then assign the ETA date
                            // to the "closest ETA date to ETD" variable
                            if (diffBetweenETDAndETAFromResultRow < diffBetweenETDAndClostestETADateToETD)
                            {
                                closestETADateToETD = rowResultETADateAtDestinationPort;
                                // Set flag to true to indicate that we will be setting a new value to  ETD with the ETA of vessel to POL
                                fetchedETAFromVoyageSchedulingMaster = true;
                            }
                            // If date diff is equal, then take the earlier date.
                            else if (diffBetweenETDAndClostestETADateToETD == diffBetweenETDAndETAFromResultRow)
                            {
                                if (rowResultETADateAtDestinationPort < closestETADateToETD)
                                {
                                    closestETADateToETD = rowResultETADateAtDestinationPort;
                                    // Set flag to true to indicate that we will be setting a new value to  ETD with the ETA of vessel to POL
                                    fetchedETAFromVoyageSchedulingMaster = true;
                                }
                            }
                    //    }

                    //}//-- end foreach

                    // Assign new ETD date if ETA was fetched from the Voyage Scheduling Master table
                    if (fetchedETAFromVoyageSchedulingMaster)
                    {
                        etd = closestETADateToETD;
                    }

                }//if etaMasterVoyageSchedulingResult != null loop

            }
            etdValue = etd.ToString(targetDateFormat);
            return etdValue;
        }
        private static string CustomerCodeLookUpSC(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField, string sourceField2,string mapperName)
        {
            long customerCode;
            EntitiesModel customer = null;
            string customerCodeStr = CustomerCodeLookup(objGPCustomerMessage, operationParam, sourceField2,mapperName);

            if (long.TryParse(customerCodeStr, out customerCode))
            {
                customer = DataVerificationManagerNew.GetCustomer(customerCode);
            }

            //if (customer == null || customer.CustomerCode <= 1)
            //{
            //    if (!string.IsNullOrEmpty(sourceField2))
            //    {

            //        string query = string.Format("SELECT top 1 CustomerCode FROM TRANS_LINE WHERE SOURCEREFERENCE='{0}'"
            //                         , sourceField2);

            //        SqlConnection sqlConnection = new SqlConnection(DBConnection.SqlConnString);
            //        SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
            //        sqlConnection.Open();
            //        object result = sqlCommand.ExecuteScalar();
            //        if (result != null)
            //        {
            //            customerCodeStr = result.ToString();
            //        }
            //        sqlConnection.Close();
            //    }
            //}
            //else
            //{
            if (customer !=null)
            {
                customerCodeStr = customer.CustomerCode.ToString();
            }
            //}
            return customerCodeStr;
        }

        private static string VerticalLookup_By_CustomerCode(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField)
        {
            EntityMapperModel mapperModel = new EntityMapperModel();
            string VerticalValue = string.Empty;
            long custCode = objGPCustomerMessage.CustomerCode;
            string custName = null;
            mapperModel = DataVerificationManagerNew.VerticalLookup_By_CustomerCode_Value(objGPCustomerMessage, operationParam, sourceField, custCode, custName);
            if (mapperModel != null)
            {
                VerticalValue = mapperModel.Vertical;
            }
            return VerticalValue;
        }
        private static string VerticalLookup_By_CustomerName(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField)
        {
            EntityMapperModel mapperModel = new EntityMapperModel();
            string VerticalValue = string.Empty;
            long custCode = 0;
            string custName = objGPCustomerMessage.CustomerName;
            mapperModel = DataVerificationManagerNew.VerticalLookup_By_CustomerCode_Value(objGPCustomerMessage, operationParam, sourceField, custCode, custName);
            if (mapperModel != null)
            {
                VerticalValue = mapperModel.Vertical;
            }
            return VerticalValue;
        }
        private static string ContractNumberLookup_By_CustomerName(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField)
        {
            string ContractNumber = string.Empty;
            EntityMapperModel mapperModel = new EntityMapperModel();
            long custCode = 0;
            string custName = objGPCustomerMessage.CustomerName;
            mapperModel = DataVerificationManagerNew.VerticalLookup_By_CustomerCode_Value(objGPCustomerMessage, operationParam, sourceField, custCode, custName);
            if (mapperModel != null)
            {
                ContractNumber = mapperModel.ContractNumber;
            }
            return ContractNumber;
        }
        private static string ContractNumberLookup_By_CustomerCode(SCTransactionModel objGPCustomerMessage, string operationParam)
        {
            string ContractNumber = string.Empty;
            EntityMapperModel mapperModel = new EntityMapperModel();
            long custCode = objGPCustomerMessage.CustomerCode;
            string custName = null;
            mapperModel = DataVerificationManagerNew.VerticalLookup_By_CustomerCode_Value(objGPCustomerMessage, operationParam, null, custCode, custName);
            if (mapperModel != null)
            {
                ContractNumber = mapperModel.ContractNumber;
            }
            return ContractNumber;
        }
        private static string ContractNumberLookup_By_CustomerCode(string customerCode)
        {
            string ContractNumber = string.Empty;            

            long customerCodeLong;
            if (!long.TryParse(customerCode, out customerCodeLong))
            {
                customerCodeLong = 0;
            }
            EntityMapperModel mapperModel = new EntityMapperModel();
            long custCode = customerCodeLong;
            string custName = null;
            mapperModel = DataVerificationManagerNew.VerticalLookup_By_CustomerCode_Value(null, null, null, custCode, custName);
            if (mapperModel != null)
            {
                ContractNumber = mapperModel.ContractNumber;
            }
            return ContractNumber;
        }


        private static string SubsidiaryLookup_By_CustomerCode(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField)
        {
            string Subsidiary = string.Empty;
            long custCode;

            if (long.TryParse(sourceField, out custCode))
            {
                EntitiesModel objCustomer = DataVerificationManagerNew.GetCustomer(custCode);
                if (objCustomer != null)
                    Subsidiary = objCustomer.Subsidiary;

                // If null, set to empty string
                if (Subsidiary == null)
                    Subsidiary = string.Empty;
            }
            return Subsidiary;
        }
        private static string SubsidiaryLookup_By_CustomerName(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField,string mapperName)
        {
            string subsidiary = string.Empty;
            string customerCode;

            // Get Customer Code for customer
            customerCode = CustomerCodeLookup(objGPCustomerMessage, operationParam, sourceField,mapperName);

            // Get customer subsidiary
            subsidiary = SubsidiaryLookup_By_CustomerCode(objGPCustomerMessage, operationParam, customerCode);

            return subsidiary;
        }

        private static string GetSAPCustomerCode(SCTransactionModel objGPCustomerMessage)
        {
            return objGPCustomerMessage.CustomerCode.ToString();
        }

        private static string GetTransactionReceivedDate(SCTransactionModel objGPCustomerMessage)
        {
            string returnVal = string.Empty;
            if (objGPCustomerMessage.DateCreated!=null)
            {
                returnVal = string.Format(
                    "{0}{1}{2}",
                    objGPCustomerMessage.DateCreated.Day.ToString("00"),
                    objGPCustomerMessage.DateCreated.Month.ToString("00"),
                    objGPCustomerMessage.DateCreated.Year.ToString("0000")
                );
            }
            return returnVal;

        }

        //For future use-- To get the Order date for processing data
        private static string GetOrderDate(SCTransactionModel objGPCustomerMessage)
        {
            string returnVal = string.Empty;
            if (objGPCustomerMessage.DateCreated != null)
            {
                returnVal = string.Format(
                    "{0}{1}{2}",
                    objGPCustomerMessage.DateCreated.Day.ToString("00"),
                    objGPCustomerMessage.DateCreated.Month.ToString("00"),
                    objGPCustomerMessage.DateCreated.Year.ToString("0000")
                );
            }
            return returnVal;

        }

        //Below method calculates the 9th digit in an 8 digit barcode and outputs the value of final 9digit barcode
        private static string CalculateCheckDigitInBarcode(string sourceField)
        {
            string strBarcode = "";
            //sourceField will be returned if sourceField is non-numeric or more than 9 digits
            if (DataVerificationManagerNew.IsNumeric(sourceField) && sourceField.Length < 9)
            {
                //if sourceField is less than 8 digits, insert leading 0's and continue with calculation of check digit
                if (sourceField.Length < 8)
                {
                    sourceField = sourceField.PadLeft(8, '0');
                }

                int iCheckDigit = 0;
                //Multiply odd numbered digits by 1 and even by 3 and subtract their sum from 100000
                iCheckDigit = 100000 - ((Convert.ToInt32(sourceField[0].ToString()) * 1) + (Convert.ToInt32(sourceField[1].ToString()) * 3) + (Convert.ToInt32(sourceField[2].ToString()) * 1)
                    + (Convert.ToInt32(sourceField[3].ToString()) * 3) + (Convert.ToInt32(sourceField[4].ToString()) * 1) + (Convert.ToInt32(sourceField[5].ToString()) * 3)
                    + (Convert.ToInt32(sourceField[6].ToString()) * 1) + (Convert.ToInt32(sourceField[7].ToString()) * 3));

                //append the last digit of above calculation to sourceField
                strBarcode = sourceField + iCheckDigit.ToString().Substring(iCheckDigit.ToString().Length - 1);
            }
            else
                return sourceField;

            return strBarcode;
        }

        //private static string CustomerCodeLookupForBP(GPCustomerMessage objGPCustomerMessage, string operationParam, string sourceField)
        //{
        //    string CustCode = "";
        //    string query = string.Format("select top 1 CustomerCode from REF_LOOKUP where LookupName='{0}' and LookupCode='{1}' ORDER BY CustomerCode DESC"
        //                        , "Packer",sourceField);

        //    SqlConnection sqlConnection = new SqlConnection(DBConnection.SqlConnString);
        //    SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
        //    sqlConnection.Open();
        //    object result = sqlCommand.ExecuteScalar();
        //    if (result != null)
        //    {
        //        CustCode = result.ToString();
        //    }
        //    sqlConnection.Close();

        //    return CustCode;
        //}

        //Doing Bin Pairing Lookup
        //private static string BinPairingLookup(SCTransactionModel objGPCustomerMessage, string operationParam, string sourceField)
        //{

        //  //TO DO after Viveks potion on look up


        //    string binPairingValue = string.Empty;
        //    int? customerGroupId = null;
        //    //TRANSACTION_TYPES? transactionType = null;
        //    REF_LOOKUP2 objLookUP = new REF_LOOKUP2();
            
        //    DateTime? disableDate = null;
        //    DateTime enableDate = DateTime.MinValue;
        //    string notes = string.Empty;


        //    if (!string.IsNullOrEmpty(sourceField))
        //    {
        //        binPairingValue = objLookUP.SelectLookup(customerGroupId, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerCode, null, "Packer", sourceField);

        //    }
        //    if (!string.IsNullOrEmpty(binPairingValue))
        //    {
        //        objLookUP.UpdateLookupForBinPairing(customerGroupId, objGPCustomerMessage.CustomerHeadCompany, objGPCustomerMessage.CustomerCode, null, "Packer", sourceField, true);
        //    }

        //    return binPairingValue;
        //}

    }
}