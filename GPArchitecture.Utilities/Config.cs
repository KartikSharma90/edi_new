﻿using GPArchitecture.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GPArchitecture.Utilities
{
    public class Config
    {
        private static Dictionary<string, string> objCFG_MAIN;
        public static string getConfig(string Key)
        {
            string strValue;
            if (objCFG_MAIN == null)
            {
                objCFG_MAIN = new Dictionary<string, string>();
                IList<ConfigModel> configModel = null;
                using (var context = new GoodpackEDIEntities())
                {
                    configModel = (from s in context.Gen_Configuration
                                   select new ConfigModel
                                   {
                                       ConfigItem = s.ConfigItem,
                                       Value = s.Value,
                                       Description = s.Description
                                   }).ToList();
                }
                foreach (var item in configModel)
                {
                    if (!objCFG_MAIN.Keys.Contains(item.ConfigItem.ToString()))
                        objCFG_MAIN.Add(item.ConfigItem.ToString(), item.Value.ToString());
                }
            }
            objCFG_MAIN.TryGetValue(Key, out strValue);
            return strValue;
        }
    }
    class ConfigModel
    {
        public string ConfigItem { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}