﻿
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace GPArchitecture.Utilities
{
    public class SourceMessage
    {
        #region Private Members
        private int id;
        private string customerHeadCompany;
        private string sourceMessageFormat;
        private int minFieldValCount;
        private bool isHeaderPresent;
        private string service;
        private Dictionary<int, SourceMessageField> sourceMessageFields = new Dictionary<int, SourceMessageField>();
        private string originalLineContent;
        private StringBuilder headerRecordBuilder = new StringBuilder();
        #endregion

        #region CTORs
        /// <summary>
        /// Generic Initializer
        /// </summary>
        public SourceMessage()
        { }

        /// <summary>
        /// Use to create a hard copy of another source message object
        /// </summary>
        /// <param name="copyFrom"></param>
        public SourceMessage(SourceMessage copyFrom)
        {
            this.id = copyFrom.ID;
            this.customerHeadCompany = copyFrom.CustomerHeadCompany;
            this.sourceMessageFormat = copyFrom.SourceMessageFormat;
            this.sourceMessageFields = new Dictionary<int, SourceMessageField>();
            //this.MinFieldValCount = copyFrom.MinFieldValCount;
            foreach (KeyValuePair<int, SourceMessageField> objSMField in copyFrom.Fields)
            {
                sourceMessageFields.Add(objSMField.Key, new SourceMessageField(objSMField.Value.FieldName, objSMField.Value.Length));
            }
        }

        /// <summary>
        /// Constructor for SourceMessage object
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerCode"></param>
        /// <param name="transactionType"></param>
        /// <param name="messageFormat"></param>
        /// <param name="fields"></param>
        public SourceMessage(int id, int customerCode, TRANSACTION_TYPES transactionType, string messageFormat, Dictionary<int, SourceMessageField> fields)
        {
            this.id = id;
            this.TransactionType = transactionType;
            this.sourceMessageFormat = messageFormat;
            this.sourceMessageFields = fields;
        }
        #endregion

        public int ID
        {
            set { id = value; }
            get { return id; }
        }
        public string CustomerHeadCompany
        {
            set { customerHeadCompany = value; }
            get { return customerHeadCompany; }
        }
        public string SourceMessageFormat
        {
            set { sourceMessageFormat = value; }
            get { return sourceMessageFormat; }
        }
        public string Service
        {
            set { service = value; }
            get { return service; }
        }

        public TRANSACTION_TYPES TransactionType;
        public int MinFieldValCount
        {
            set { minFieldValCount = value; }
            get { return minFieldValCount; }
        }
        public bool IsHeaderPresent
        {
            set { isHeaderPresent = value; }
            get { return isHeaderPresent; }
        }
        public Dictionary<int, SourceMessageField> Fields
        {
            set { sourceMessageFields = value; }
            get { return sourceMessageFields; }
        }
        public string OriginalLineContent
        {
            set { originalLineContent = value; }
            get { return originalLineContent; }
        }

        /// <summary>
        /// If header row exists, this method will build that header row for later use
        /// </summary>
        /// <returns></returns>
        public string GetHeaderRow()
        {
            // Only process if header is present and the header record builder is empty. This is to prevent the header row from getting
            // created more than once
            if (this.isHeaderPresent && headerRecordBuilder.Length == 0)
            {
                // Set the delimiter before proceeding
                string delimiter = string.Empty;
               int messageId= Convert.ToInt32(sourceMessageFormat);
                using (var context=new GoodpackEDIEntities())
                {
                    string messageFormat = (from s in context.Ref_MessageFormat
                                            where s.Id == messageId
                                            select s.MessageFormat).FirstOrDefault();
                    this.sourceMessageFormat = messageFormat;
                }

                switch (this.sourceMessageFormat.ToUpper())
                {
                    case "CSV":
                    case "EXCEL":
                        delimiter = ",";
                        break;
                    case "SEMICOLON DELIMITED":
                        delimiter = ";";
                        break;
                    case "TAB_DELIMITED":
                        delimiter = "\t";
                        break;
                }//-- end switch(this.sourceMessageFormat.ToUpper())

                // Loop through each field
                foreach (KeyValuePair<int, SourceMessageField> sourceFieldsList in this.sourceMessageFields)
                {
                    // If length of header record builder is greater than zero, append delimiter
                    if (headerRecordBuilder.Length > 0)
                    {
                        headerRecordBuilder.Append(delimiter);
                    }

                    // Append name of source field. Remove delimiter, too, in the process to simplify result
                    headerRecordBuilder.Append(sourceFieldsList.Value.FieldName.Replace(delimiter, string.Empty));
                }

            }//-- end if statement

            return headerRecordBuilder.ToString();
        }
    }

    public class SourceMessageField
    {
        private string strValue;
        private string strFieldName;
        private int intLength;
        public SourceMessageField()
        {
        }
        public SourceMessageField(string fieldName, int length)
        {
            this.FieldName = fieldName;
            this.intLength = length;
        }
        public int Length
        {
            set { intLength = value; }
            get { return intLength; }
        }
        public string FieldName
        {
            set { strFieldName = value; }
            get { return strFieldName; }
        }
        public string Value
        {
            set { strValue = value; }
            get { return strValue; }
        }
    }
}