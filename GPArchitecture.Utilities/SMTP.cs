﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using log4net;

namespace GPArchitecture.Utilities
{
    public class SMTP
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SMTP));
        public bool Send(EmailMessage objMailMessage)
        {
            try
            {
                int iSmtpPort = 0;
                string strMailDropFolder = Config.getConfig("OutboundMailQueueFolder");
                string strSmtpServer = Config.getConfig("SMTP_SERVER");
                iSmtpPort = int.Parse(Config.getConfig("SMTP_PORT"));
                MailMessage objMessage = (MailMessage)objMailMessage.MailMessage;

                // Check if recipient list contain recipients.  If are no recipients, exit method.
                if (objMailMessage.To.Count == 0)
                {
                    return true;
                }

                int i = 0;
                foreach (Attachment objAttachment in objMailMessage.AttachmentList)
                {
                    objMessage.Attachments.Add(objAttachment);
                    i++;
                }

                // Create Client
                SmtpClient smtp = new SmtpClient(strSmtpServer);
                // Check if SSL will be used                    
                if (Config.getConfig("SMTP_SSL").ToLower() == "true")
                {
                    smtp.EnableSsl = true;
                }
                // Set timeout
                int timeout;
                if (!int.TryParse(Config.getConfig("SMTP_TIMEOUT").ToLower(), out timeout))
                {
                    smtp.Timeout = 20000;
                }
                else
                {
                    smtp.Timeout = timeout;
                }
                // Set Delivery method
                if (string.IsNullOrEmpty(strMailDropFolder) || (strMailDropFolder.Equals("-", StringComparison.InvariantCultureIgnoreCase)))
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                else
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtp.PickupDirectoryLocation = strMailDropFolder;
                }
                // Check if credentials will be sent first to server                    
                if (Config.getConfig("SMTP_USE_DEFAULT_CREDENTIALS").ToLower() == "true")
                {
                    smtp.UseDefaultCredentials = true;
                }
                else
                {
                    smtp.UseDefaultCredentials = false;
                }
                // set Credentials
                System.Net.NetworkCredential auth = new System.Net.NetworkCredential(Config.getConfig("SMTP_USERNAME"), Config.getConfig("SMTP_PASS"));
                smtp.Credentials = auth;
                // Set port
                smtp.Port = iSmtpPort;

                // Set email
                smtp.Send(objMessage);

                // Log successful sending of email
                StringBuilder logMessage = new StringBuilder();

                // Get recipient list
                logMessage.Append(objMailMessage.Subject)
                    .Append(Environment.NewLine)
                    .Append("Recipients: ");
                foreach (MailAddress recipient in objMailMessage.To)
                {
                    logMessage.Append(recipient.Address)
                        .Append(",");
                }
                logMessage.Append(Environment.NewLine)
                    .Append("Body: ")
                    .Append(objMailMessage.Body);

                return true;
            }
            catch (Exception objException)
            {
                log.Error("SMTP Send method-EmailMessage From:" + objMailMessage.From + ",EmailMessage To:" + objMailMessage.To + ",Emailmessage Mail Message:" + objMailMessage.MailMessage + ", Emailmessage Subject:" + objMailMessage.Subject);
                throw objException;
            }
        }
    }

    [Serializable()]
    public class EmailMessage
    {
        private MailMessage objMailMessage;
        private string strName;
        List<Attachment> objlstFileStream = new List<Attachment>();
        /// <summary>
        /// Constructor for this class
        /// </summary>
        public EmailMessage()
        {
            objMailMessage = new MailMessage();
        }

        /// <summary>
        /// FileStream for Email attachments.
        /// </summary>
        public List<Attachment> AttachmentList
        {
            get { return objlstFileStream; }
            set { objlstFileStream = value; }
        }

        /// <summary>
        /// We can set collection of To address
        /// </summary>
        public MailAddressCollection To
        {
            get { return objMailMessage.To; }
        }

        /// <summary>
        /// We can set collection of CC address
        /// </summary>
        public MailAddressCollection CC
        {
            get { return objMailMessage.CC; }
        }

        /// <summary>
        /// We can set collection of BCC address
        /// </summary>
        public MailAddressCollection Bcc
        {
            get { return objMailMessage.Bcc; }
        }

        /// <summary>
        /// Mail Subject
        /// </summary>
        public string Subject
        {
            get { return objMailMessage.Subject; }
            set { objMailMessage.Subject = value; }
        }

        /// <summary>
        /// To set the Mail content
        /// </summary>
        public string Body
        {
            get { return objMailMessage.Body; }
            set { objMailMessage.Body = value; }
        }

        /// <summary>
        /// True / False to set the message body.
        /// </summary>
        public bool ISBodyHTML
        {
            get { return objMailMessage.IsBodyHtml; }
            set { objMailMessage.IsBodyHtml = value; }
        }

        /// <summary>
        /// Returns Mail Message object
        /// </summary>
        internal MailMessage MailMessage
        {
            get { return objMailMessage; }
        }

        /// <summary>
        /// From address for the message object
        /// </summary>
        public MailAddress From
        {
            get { return objMailMessage.From; }
            set { objMailMessage.From = value; }
        }

        /// <summary>
        // Returns the Name of the Mail Message
        /// </summary>
        public virtual string Name
        {
            get { return strName; }
            set { strName = value; }
        }
    }
}