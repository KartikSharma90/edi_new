﻿using GPArchitecture.DataLayer.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Parsers;
using GPArchitecture.CommonTypes;


namespace GPArchitecture.Utilities
{
    public class GPTools
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(GPTools));
        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="messageKeyPlaceholders"></param>
        /// <param name="recipients"></param>
        /// <param name="templateName"></param>
        /// <param name="forCustomer"></param>
        /// <param name="attachments"></param>
        public static void SendEmail(Dictionary<string, string> messageKeyPlaceholders, string[] recipients, string templateName, List<Attachment> attachments)
        {
            try
            {
                // If there are no recipients, exit method
                if (recipients == null || recipients.Length < 1)
                    return;

                // Get email body
                string emailSubject;
                string emailBody;
                GPTools.GetEmailTemplate(templateName, out emailSubject, out emailBody);

                // If template is not found, throw exception
                if (string.IsNullOrEmpty(emailBody))
                {
                    //throw new System.Exception("Email template for event code " + templateName + " was not found in REF_EMAIL_TEMPLATE table.");
                    return;
                }

                // Replace placeholders in message template
                foreach (KeyValuePair<string, string> messageKey in messageKeyPlaceholders)
                {
                    emailBody = emailBody.Replace(messageKey.Key, messageKey.Value);
                }
                foreach (KeyValuePair<string, string> messageKey in messageKeyPlaceholders)
                {
                    emailSubject = emailSubject.Replace(messageKey.Key, messageKey.Value);
                }


                // Create mail message object
                EmailMessage objEmailMessage = new EmailMessage();
                objEmailMessage.From = new MailAddress(Config.getConfig("FromEmailAddress"));
                objEmailMessage.Subject = emailSubject;
                objEmailMessage.Body = emailBody;
                objEmailMessage.ISBodyHTML = true;

                // Email recipient address list
                MailAddressCollection objMailAddressCollection = objEmailMessage.To;
                foreach (string recipient in recipients)
                {
                    objMailAddressCollection.Add(new MailAddress(recipient));
                }

                // Add email attachments
                if (attachments != null)
                {
                    objEmailMessage.AttachmentList = attachments;
                }

                // Send email 
                SMTP objSMTPSendMail = new SMTP();

                // Retry if failed
                int retryCount = 0;
                bool isSuccess = false;
                while (!isSuccess)
                {
                    try
                    {
                        objSMTPSendMail.Send(objEmailMessage);
                        isSuccess = true;
                    }
                    catch (System.Exception ex)
                    {
                        objEmailMessage.MailMessage.Attachments.Clear();
                        retryCount++;
                        if (retryCount > 3)
                            throw ex;
                    }
                }
            }
            catch (System.Exception ex)
            {
                log.Error(" SendEmail in GPTools .recipients:" + recipients + ", templateName:" + templateName + ".  Exception : Message- " + ex.Message);
                return;
            }
            finally
            {
                // Release attachment resources
                if (attachments != null)
                {
                    foreach (Attachment attachment in attachments)
                    {
                        attachment.Dispose();
                    }
                }
            }
        }


        public static void GetEmailTemplate(string eventCode, out string subject, out string messageTemplate)
        {
            subject = string.Empty;
            messageTemplate = string.Empty;
            using (var context = new GoodpackEDIEntities())
            {
                Gen_EmailTemplate emailTemplate = context.Gen_EmailTemplate.Where(i => i.EventCode == eventCode).FirstOrDefault();
                subject = emailTemplate.Subject.ToString();
                messageTemplate = emailTemplate.Template.ToString();
            }             
        }

        /// <summary>
        /// Performs deep object clone
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T DeepClone<T>(T obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }// End DeepClone()


        public static void createFolder(string path)
        {
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(path);
            if (!objDirectoryInfo.Exists)
            {
                string newDir = objDirectoryInfo.Name;
                // Combine the new file name with the path
                string newPath = System.IO.Path.Combine(objDirectoryInfo.Parent.FullName, newDir);
                objDirectoryInfo = Directory.CreateDirectory(newPath);
                createFolder(objDirectoryInfo.Parent.FullName);
            }
        }


        public static GPArchitecture.CommonTypes.SourceMessage LoadSourceMessageSpecs(int subsiId, TRANSACTION_TYPES transType, string service)
        {
            GPArchitecture.CommonTypes.SourceMessage specModel = new GPArchitecture.CommonTypes.SourceMessage();
            // Create the array to store the field specs.
            GoodyearFieldModel[] fileModel = new GoodyearFieldModel[26];

            // Populate the model array 
            for (int i = 0; i < 26; i++)
            {
                fileModel[i] = new GoodyearFieldModel();
            }

            // Assign details.
            fileModel[0].FieldSeq = 1;
            fileModel[0].FieldName = "Cust PO";
            fileModel[0].Length = 20;
            fileModel[1].FieldSeq = 2;
            fileModel[1].FieldName = "SO Number";
            fileModel[1].Length = 10;
            fileModel[2].FieldSeq = 3;
            fileModel[2].FieldName = "SI Number";
            fileModel[2].Length = 8;
            fileModel[3].FieldSeq = 4;
            fileModel[3].FieldName = "Ver";
            fileModel[3].Length = 2;
            fileModel[4].FieldSeq = 5;
            fileModel[4].FieldName = "SI Status";
            fileModel[4].Length = 1;
            fileModel[5].FieldSeq = 6;
            fileModel[5].FieldName = "SI Qty in Number of Units";
            fileModel[5].Length = 13;
            fileModel[6].FieldSeq = 7;
            fileModel[6].FieldName = "Rubber Grade";
            fileModel[6].Length = 40;
            fileModel[7].FieldSeq = 8;
            fileModel[7].FieldName = "Supplier";
            fileModel[7].Length = 35;
            fileModel[8].FieldSeq = 9;
            fileModel[8].FieldName = "Factory Code";
            fileModel[8].Length = 10;
            fileModel[9].FieldSeq = 10;
            fileModel[9].FieldName = "Packaging Type";
            fileModel[9].Length = 40;
            fileModel[10].FieldSeq = 11;
            fileModel[10].FieldName = "Load Port";
            fileModel[10].Length = 30;
            fileModel[11].FieldSeq = 12;
            fileModel[11].FieldName = "Load Date";
            fileModel[11].Length = 8;
            fileModel[12].FieldSeq = 13;
            fileModel[12].FieldName = "Shipping Line";
            fileModel[12].Length = 35;
            fileModel[13].FieldSeq = 14;
            fileModel[13].FieldName = "Vessel Name";
            fileModel[13].Length = 20;
            fileModel[14].FieldSeq = 15;
            fileModel[14].FieldName = "Voyage Number";
            fileModel[14].Length = 20;
            fileModel[15].FieldSeq = 16;
            fileModel[15].FieldName = "BOL Number";
            fileModel[15].Length = 20;
            fileModel[16].FieldSeq = 17;
            fileModel[16].FieldName = "BOL Qty";
            fileModel[16].Length = 13;
            fileModel[17].FieldSeq = 18;
            fileModel[17].FieldName = "Container Number";
            fileModel[17].Length = 20;
            fileModel[18].FieldSeq = 19;
            fileModel[18].FieldName = "Container Qty";
            fileModel[18].Length = 13;
            fileModel[19].FieldSeq = 20;
            fileModel[19].FieldName = "Ocean ETD";
            fileModel[19].Length = 8;
            fileModel[20].FieldSeq = 21;
            fileModel[20].FieldName = "Arrival Port";
            fileModel[20].Length = 30;
            fileModel[21].FieldSeq = 22;
            fileModel[21].FieldName = "Arrival Date";
            fileModel[21].Length = 8;
            fileModel[22].FieldSeq = 23;
            fileModel[22].FieldName = "Customer Code";
            fileModel[22].Length = 10;
            fileModel[23].FieldSeq = 24;
            fileModel[23].FieldName = "Customer Name";
            fileModel[23].Length = 35;
            fileModel[24].FieldSeq = 25;
            fileModel[24].FieldName = "Booking Number";
            fileModel[24].Length = 20;
            fileModel[25].FieldSeq = 26;
            fileModel[25].FieldName = "End of Record";
            fileModel[25].Length = 1;


            using (var context = new GoodpackEDIEntities())
            {

                specModel.ID = 192;
                specModel.IsHeaderPresent = false;
                specModel.MinFieldValCount = 4;
                specModel.SourceMessageFormat = "Fixed_Field_Length";
                specModel.CustomerHeadCompany = subsiId.ToString();

                GPArchitecture.CommonTypes.SourceMessageField messageField;
                for (int i = 0; i < 26; i++)
                {
                    messageField = new GPArchitecture.CommonTypes.SourceMessageField();
                    messageField.FieldName = fileModel[i].FieldName;
                    messageField.Length = fileModel[i].Length;
                    messageField.Value = fileModel[i].FieldSeq.ToString();
                    specModel.Fields.Add(i, messageField);
                }

            }
            return specModel;

        }

        

        public static List<GPArchitecture.CommonTypes.SourceMessage> Parse(GPCustomerMessage objGPCustomerMessage, GPArchitecture.CommonTypes.SourceMessage objSourceMessage, ref bool isSuccess, ref string errorDesc)
        {
            List<GPArchitecture.CommonTypes.SourceMessage> parsedMessage = null;

            errorDesc = string.Empty;

            string strFileExt = objGPCustomerMessage.BatchFileName.Substring(objGPCustomerMessage.BatchFileName.LastIndexOf('.'));
            if (objSourceMessage.TransactionType == TRANSACTION_TYPES.FC)
            {
                if (strFileExt == ".xls" || strFileExt == ".xlsx" || strFileExt == ".XLS" || strFileExt == ".XLSX")
                    objSourceMessage.SourceMessageFormat = MessageFormats.Excel;
                else if (strFileExt == ".csv")
                    objSourceMessage.SourceMessageFormat = MessageFormats.CSV;
            }

            switch (objSourceMessage.SourceMessageFormat)
            {
                case MessageFormats.CSV:
                case MessageFormats.Excel:
                    parsedMessage = GenericDelimiterParser.Parse(objSourceMessage, objGPCustomerMessage.Message, objSourceMessage.IsHeaderPresent, ',', ref isSuccess, ref errorDesc);
                    break;
                case MessageFormats.FixedFieldLength:
                    parsedMessage = FixedFieldLengthParser.Parse(objSourceMessage, objGPCustomerMessage.Message, ref isSuccess, ref errorDesc);
                    break;
                case MessageFormats.SemiColonDelimited:
                    parsedMessage = GenericDelimiterParser.Parse(objSourceMessage, objGPCustomerMessage.Message, objSourceMessage.IsHeaderPresent, ';', ref isSuccess, ref errorDesc);
                    break;
            }

            if (!isSuccess)
            {
                log4net.GlobalContext.Properties["CustomerCode"] = objGPCustomerMessage.CustomerCode.ToString();
                log4net.GlobalContext.Properties["TransactionType"] = objGPCustomerMessage.TransactionType.ToString();
                log4net.GlobalContext.Properties["FileName"] = objGPCustomerMessage.BatchFileName.ToString();
                GPTools.log.Error(errorDesc);
                return null;
            }
            return parsedMessage;
        }//-- End Parse()

        /// <summary>
        /// Store the skipped records to temp folder.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="path"></param>
        /// <param name="Extension"></param>
        /// <returns></returns>
        public static string WriteToFile(string content, string destinationFolderPath, string fileExtension)
        {
            // Init vars
            bool fileCreated = false;
            int counter = 0;
            string fullpath;

            // Create the subfolder
            if (!destinationFolderPath.EndsWith(@"\"))
            {
                destinationFolderPath += @"\";
            }

            // Set default value for full path
            fullpath = destinationFolderPath;

            try
            {
                DirectoryInfo objDirectoryInfo = new DirectoryInfo(destinationFolderPath);
                ;

                // Create directory if it doesn't exist yet
                if (!objDirectoryInfo.Exists)
                {
                    createFolder(objDirectoryInfo.FullName);
                }

                // Add dot to header of file extension if it isn't there yet
                if (!fileExtension.StartsWith("."))
                {
                    fileExtension = "." + fileExtension;
                }

                while (!fileCreated)
                {
                    if (counter == 0)
                    {
                        fullpath = destinationFolderPath + (Guid.NewGuid().ToString()) + fileExtension;
                    }
                    else
                    {
                        fullpath = destinationFolderPath + Guid.NewGuid().ToString() + counter.ToString() + fileExtension;
                    }

                    // If file exists, increment counter and repeat process
                    if (File.Exists(fullpath))
                    {
                        // increment counter
                        counter++;
                        continue;
                    }

                    // Write file
                    using (System.IO.StreamWriter objStreamWriter = new System.IO.StreamWriter(fullpath, false))
                    {
                        objStreamWriter.Write(content);
                        fileCreated = true;
                    }

                }// end while (!fileCreated)
            }

            catch (System.Exception ex)
            {
                throw new System.Exception("Error saving file to " + fullpath + ". " + ex.Message, ex);
            }

            return fullpath;

        }

        public static long ConvertStringToLong(string numberValue)
        {
            long longValue;

            if (long.TryParse(numberValue, out longValue))
            {
                return longValue;
            }
            return long.MinValue;
        }
    }
}