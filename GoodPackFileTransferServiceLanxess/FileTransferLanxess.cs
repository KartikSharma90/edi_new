﻿using GPArchitecture.Adapters.FTP;
using GPArchitecture.Adapters.SFTP;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.SCManagerLayer;
using GoodPackFileTransferServiceLanxess.BusinessLayer.SOManagementLayer;
using GPArchitecture.GoodPackBusinessLayer.SSCManagerLayer;
using GPArchitecture.Models;
using GPArchitecture.Parsers;
using GPArchitecture.Utilities;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer;
using GPArchitecture.Tools;
using GPArchitecture;

namespace GoodPackFileTransferServiceLanxess
{
    public class FileTransferLanxess
    {
        internal static ILog log = LogManager.GetLogger(typeof(FileTransferLanxess));
        public bool isProcessing = false;
        private readonly GoodpackEDIEntities context = null;
        public FileTransferLanxess() 
        {
            context = new GoodpackEDIEntities();
        }
        public void SFtpDownload()
        {
            isProcessing = true;
            try
            {                
                DownloadFilesFromSFTPToLocal();
            }
            catch (Exception e) { log.Error("FileTransfer DownloadFilesFromSFTPToLocal error-" + e); }
            try
            
            {
                ProcessDownloadedSFTPFiles();
            }
            catch (Exception e) { log.Error("FileTransfer ProcessDownloadedSFTPFiles error-" + e);   }
            try
            {
                ProcessSAPRespone();
            }
            catch (Exception e) { log.Error("FileTransfer ProcessSAPRespone error-" + e);  }
            finally
            {
                isProcessing = false;
            }
        }
        private void ProcessSAPRespone()
        {
            using (var context=new GoodpackEDIEntities())
            {
                List<int> processedBatchIds = null;
                List<int> processedLineIds = null;
                var filePath = context.Ref_SAPFileSourcePath.ToList();
                var filePoller = new FilePolling(filePath.Select(i => i.FilePath).ToList(), GPArchitecture.Cache.SupportedFileTypes.GetSupportedFileTypes());
                FileInfo[] responseFiles = null;
                try
                {
                    responseFiles = filePoller.GetFiles();
                }
                catch (Exception exp)
                {                    
                    log.Error("Unable to Access Source Directory", exp);
                    return;
                }
                if (responseFiles == null)
                    return;
                foreach (FileInfo responseFileInfo in responseFiles)
                {
                    var value = context.Ref_SAPFileSourcePath.Where(i => i.FilePath == responseFileInfo.Directory.FullName).Select(i => new { i.Ref_TransactionTypes.TransactionCode, i.MapperName }).FirstOrDefault();
                    TRANSACTION_TYPES transactionType = (value.TransactionCode == "SSC") ? TRANSACTION_TYPES.SSC : (value.TransactionCode == "SO") ? TRANSACTION_TYPES.SO : TRANSACTION_TYPES.SC;
                    var subsi=context.Trn_SubsiToTransactionFileMapper_Lanxess.Where(i=>i.Filename==value.MapperName).Select(i=>i.EDISubsiId).FirstOrDefault();
                    GPArchitecture.CommonTypes.SourceMessage srcMessage = FileTransferLanxess.LoadSourceMessageSpecs(subsi, transactionType, "GPMapper");
                    if (srcMessage != null && srcMessage.Fields != null && srcMessage.Fields.Count < 1)
                    {
                        //Tools.SendEmail_SourceMsgFailed(objCustTran.Key.ToString(), responseFileInfo.FullName);
                        log.Error("Error: SAP Response Source Messages was not found ");
                        return;
                    }
                    string messageContent = LoadFileContent(responseFileInfo.FullName);
                    if (messageContent.Length < 1)
                        continue;
                    bool isParseSuccess = false;
                    string errorDesc = string.Empty;
                    IList<GPArchitecture.CommonTypes.SourceMessage> messageLines = CharacterDelimitedParser.Parse(srcMessage, messageContent, "\t", false, ref isParseSuccess, ref errorDesc);
                    if (isParseSuccess)
                    {
                        if(transactionType==TRANSACTION_TYPES.SO)
                        {

                        }
                        else if (transactionType == TRANSACTION_TYPES.SC)
                        {

                        }
                        else if (transactionType == TRANSACTION_TYPES.SSC)
                        {
                            List<SCSAPResponse> sscSAPResponseProcessed = null;
                            sscSAPResponseProcessed = ProcessSCResponse(messageLines, TRANSACTION_TYPES.SSC, out errorDesc);
                            if (sscSAPResponseProcessed != null || sscSAPResponseProcessed.Count != 0)
                            {
                                // Get distinct line Ids
                                processedLineIds = (from p in sscSAPResponseProcessed
                                                    select p.TranLineId).Distinct().ToList();

                                // Get distinct batch Ids
                                processedBatchIds = (from p in sscSAPResponseProcessed
                                                     select p.BatchId).Distinct().ToList();
                            }
                        }
                    }
                }
            }
        }

        private void DownloadFilesFromSFTPToLocal()
        {
            try
            {
                List<Trn_SFTP_File_Transfer> ftpDownloadList = (from s in context.Trn_SFTP_File_Transfer
                                                                where s.FileTransferType == "DOWNLOAD" && s.IsLanxess == true
                                                                select s).ToList();
                foreach (Trn_SFTP_File_Transfer files in ftpDownloadList)
                {
                    Gen_SFTPAccount ftpAccntDetail = context.Gen_SFTPAccount.Where(i => i.SftpId == files.SftpID).FirstOrDefault();
                    Sftp ftpClient = new Sftp(ftpAccntDetail);
                    ftpClient.OpenConnection();
                    if (ftpClient.IsConnected)
                    {
                        string fromLocation = (files.LocationFrom.LastIndexOf(@"/") == files.LocationFrom.Length - 1) ? files.LocationFrom : files.LocationFrom + "/";
                        bool hasDownloaded = ftpClient.DownloadFile(fromLocation, files.LocationTo, files.IsDeleteFile);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("DownloadFilesFromSFTPToLocal error-" + e);
            }
        }

        private void ProcessDownloadedSFTPFiles()
        {
            try
            {
                List<Trn_SFTP_File_Transfer> ftpDownloadList = (from s in context.Trn_SFTP_File_Transfer
                                                                where s.FileTransferType == "DOWNLOAD" && s.IsLanxess == true
                                                                select s).ToList();
                foreach (Trn_SFTP_File_Transfer files in ftpDownloadList)
                {
                    string toLocation = (files.LocationTo.LastIndexOf(@"\") == files.LocationTo.Length - 1) ? files.LocationTo : files.LocationTo + @"\";
                    string[] filePaths = Directory.GetFiles(toLocation);
                    var model= context.Trn_SubsiToTransactionFileMapper_Lanxess.Where(i => i.Filename.Trim() == files.MapperName).Select(i =>new { i.Ref_TransactionTypes.TransactionCode,i.Ref_MessageFormat.MessageFormat}).FirstOrDefault();
                    string transactionType = model.TransactionCode;
                    foreach (string path in filePaths)
                    {
                        int subsi = Convert.ToInt32(files.SubsiId);
                        string fileName = Path.GetFileName(path);
                        GPCustomerMessage objGPCustomerMessage = new GPCustomerMessage();
                        objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                        objGPCustomerMessage.BatchFileName = fileName;
                        objGPCustomerMessage.TransactionType = (transactionType == "SO") ? TRANSACTION_TYPES.SO : (transactionType == "SC") ? TRANSACTION_TYPES.SC : (transactionType == "SSC") ? TRANSACTION_TYPES.SSC : TRANSACTION_TYPES.ASN;
                        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
                        objGPCustomerMessage.DateCreated = DateTime.Now;
                        FileInfo objFileInfo = new FileInfo(path);
                        objGPCustomerMessage.BatchFileSourceAddress = objFileInfo.Directory.ToString();
                        objGPCustomerMessage.MapperName = files.MapperName;
                        objGPCustomerMessage.Username = context.Trn_SubsiToTransactionFileMapper_Lanxess.Where(i => i.Filename == files.MapperName).Select(i => i.Gen_User.Username).FirstOrDefault();
                        objGPCustomerMessage.SubsiId = subsi;

                        bool isValid = Validate(objGPCustomerMessage);

                        SCTransactionModel scTransactionModel = new SCTransactionModel();                       
                        scTransactionModel.BatchFileName = fileName;
                        scTransactionModel.BatchFileSourceAddress = path;
                        scTransactionModel.Message = objGPCustomerMessage.Message;
                        scTransactionModel.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
                        scTransactionModel.TransactionType = objGPCustomerMessage.TransactionType;
                        scTransactionModel.StatusCode = BatchStatus.RCVD;
                        scTransactionModel.DateCreated = DateTime.Now;
                        scTransactionModel.Service = "GPMapper";
                        scTransactionModel.SubsiId = subsi;
                        scTransactionModel.UserId = context.Trn_SubsiToTransactionFileMapper_Lanxess.Where(i => i.Filename == files.MapperName).Select(i => i.LastUpdatedBy).FirstOrDefault();
                        GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer.SCTransactionBatch scTransBatch = new GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer.SCTransactionBatch();


                        if (isValid)
                        {
                            ClearSourceFile(objGPCustomerMessage);
                            if (subsi > 0)
                            {
                                if (transactionType == "SO")
                                {
                                    try
                                    {
                                        //Insert transBatch
                                        // objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(scTransactionModel, files.MapperName);
                                        // scTransactionModel.BatchID = objGPCustomerMessage.BatchID;

                                        scTransactionModel.BatchID = objGPCustomerMessage.BatchID;
                                        ProcessFile(scTransactionModel, files.MapperName);
                                    }
                                    catch (Exception e)
                                    {
                                        log.Error("ProcessDownloadedSFTPFiles -SO processing -" + e);
                                        ProcessErrorFile(objGPCustomerMessage);
                                    }
                                }
                                else if (transactionType == "SC")
                                {
                                    try
                                    {
                                        //Insert transBatch
                                        objGPCustomerMessage.BatchID = scTransBatch.AddSCBatchFileData(scTransactionModel, files.MapperName);
                                        scTransactionModel.BatchID = objGPCustomerMessage.BatchID;
                                        ProcessFile(scTransactionModel, files.MapperName);
                                    }
                                    catch (Exception e)
                                    {
                                        log.Error("ProcessDownloadedSFTPFiles -SC processing -" + e);
                                        ProcessErrorFile(objGPCustomerMessage);
                                    }
                                }
                                else if (transactionType.Trim() == "SSC")
                                {
                                    try
                                    {
                                        //Insert transBatch
                                        objGPCustomerMessage.BatchID = scTransBatch.AddBatchFileDataLanxess(scTransactionModel, files.MapperName);
                                        scTransactionModel.BatchID = objGPCustomerMessage.BatchID;
                                        ProcessFile(scTransactionModel, files.MapperName);

                                        //string copyfileName = objGPCustomerMessage.MapperName + "_" + Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName) + "_" + DateTime.Now.ToString("dd-MMM-yyyyHH.MM.ss") + Path.GetExtension(objGPCustomerMessage.BatchFileSourceAddress);
                                        //string destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpProcessedFilePath").Select(i => i.Value).FirstOrDefault() + "\\" + copyfileName;
                                        //System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                                    }
                                    catch (Exception e)
                                    {
                                        log.Error("ProcessDownloadedSFTPFiles -SSC processing -" + e);
                                        ProcessErrorFile(objGPCustomerMessage);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("ProcessDownloadedFiles error-" + e);
            }
        }

        private bool ReadFileData(GPCustomerMessage objGPCustomerMessage, string path, SourceMessage sourceMessage)
        {
            bool isValid = true;
            FileInfo objFileInfo = new FileInfo(path);
            try
            {
                if (objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoShipmentConfirmation") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName) || GPArchitecture.Cache.Config.getConfigList("SumitomoShipmentConfirmation").Contains(objGPCustomerMessage.MapperName))
                {
                    objGPCustomerMessage.SubsiName = "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE";
                }
                if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                {
                    if ((objGPCustomerMessage.MapperName == GPArchitecture.Cache.Config.getConfig("SumitomoSalesOrder") || GPArchitecture.Cache.Config.getConfigList("SumitomoSalesOrder").Contains(objGPCustomerMessage.MapperName)) && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                    {
                        objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 21, 0);
                    }
                    else
                    {
                        objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objFileInfo.FullName, 0, 0);
                    }
                }
                else
                {
                    using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                    {
                        string message = objStreamReader.ReadToEnd();
                        if (sourceMessage.SourceMessageFormat == MessageFormats.SemiColonDelimited)
                            objGPCustomerMessage.Message = message.Replace(";", "|");
                        else
                            objGPCustomerMessage.Message = message.Replace(",", "|");
                        objStreamReader.Close();
                        objStreamReader.Dispose();
                    }
                }
            }
            catch (Exception exp)
            {
                isValid = false;
                objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                log.Error("File is not in correct format", exp);
            }
            return isValid;
        }

        private void ProcessFile(SCTransactionModel objGPCustomerMessage, string mapperFile)
        {
            GoodPackFileTransferServiceLanxess.BusinessLayer.GPMapper mapper = new GoodPackFileTransferServiceLanxess.BusinessLayer.GPMapper();
            MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);

            //Insert into Trn_TransLineLanxess
            GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer.SCTransactionBatch scTransBatch = new GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer.SCTransactionBatch();
            int i = 0;
            List<string> ediNumber = new List<string>();
            List<int> trnsLineItems = new List<int>();
            foreach (LineReferences objLineReferences in mappingOutput.LineReferenceItems)
            {
                objLineReferences.StatusCode = LineStatus.NEW;
                string customerReference = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.CustomerReferenceNumber];
                string itrNumber = objGPCustomerMessage.mappingServiceValues[i][SCSapMessageFields.ITRNumber];
                ediNumber.Add(objLineReferences.EdiNumber);
                int transID = 0;               
                switch (objGPCustomerMessage.TransactionType)
                {
                    case TRANSACTION_TYPES.SC: transID = scTransBatch.InsertTransLineLanxess(objGPCustomerMessage, objLineReferences, null, customerReference);
                        break;
                    case TRANSACTION_TYPES.SSC: transID = scTransBatch.InsertTransLineLanxess(objGPCustomerMessage, objLineReferences, null, customerReference);
                        break;
                }
                trnsLineItems.Add(transID);
                i++;
            }
            foreach(var transLineId in trnsLineItems)
            {
                InsertTransLineContent(transLineId, objGPCustomerMessage.BatchID, LineStatus.NEW ,"New");   
            }
            if ((mappingOutput.ErrorCode == ErrorCodes.V_SKIPRECORDS || mappingOutput.ErrorCode ==ErrorCodes.NONE) && mappingOutput.IsSuccess)
            {
                string strDropPath = "";
                string strFullFileName = "";
                string strTempPath = "";
                string strDropPathCopyFC = "";
                string strCopyFullFileName = "";
                string tempSAPReqPath = Config.getConfig("SAPRequestTempFolder"); 
                try
                {
                    switch (objGPCustomerMessage.TransactionType)
                    {
                        case TRANSACTION_TYPES.SO:
                            strDropPath = Config.getConfig("ReqOutPathSO");
                            break;
                        case TRANSACTION_TYPES.SC:
                            strDropPath = Config.getConfig("ReqOutPathSC");
                            break;                   
                        case TRANSACTION_TYPES.SSC:
                            strDropPath = Config.getConfig("ReqOutPathSSC");
                            break;
                    }
                    // Create the subfolder
                    DirectoryInfo objDirectoryInfo = new DirectoryInfo(strDropPath);
                    if (!objDirectoryInfo.Exists)
                        GPTools.createFolder(objDirectoryInfo.FullName);
                    string filename = String.Format("{0}{1:yyyyMMdd}_{2}.txt", objGPCustomerMessage.TransactionType.ToString(), DateTime.UtcNow, objGPCustomerMessage.BatchID.ToString());
                    strFullFileName = String.Format("{0}\\{1}", strDropPath, filename);
                    strTempPath = String.Format("{0}{1}", tempSAPReqPath, filename);
                    if (!System.IO.File.Exists(strFullFileName))
                    {
                        using (System.IO.StreamWriter objStreamWriter = new System.IO.StreamWriter(strTempPath, false))
                        {
                            objStreamWriter.Write(mappingOutput.OutputMessage);
                        }                        
                        File.Move(strTempPath, strFullFileName);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }

        private void ProcessErrorFile(GPCustomerMessage objGPCustomerMessage)
        {
            string copyfileName = objGPCustomerMessage.MapperName + "_" + Path.GetFileNameWithoutExtension(objGPCustomerMessage.BatchFileName) + "_" + DateTime.Now.ToString("dd-MMM-yyyyHH.MM.ss") + Path.GetExtension(objGPCustomerMessage.BatchFileSourceAddress);
            string destinationFilePath = context.Gen_Configuration.Where(i => i.ConfigItem == "FtpNonProcessedFilePath").Select(i => i.Value).FirstOrDefault() + "//" + copyfileName;
            System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
            string transType = objGPCustomerMessage.TransactionType.ToString();
            using (var context1 = new GoodpackEDIEntities())
            {
                System.IO.File.Move(objGPCustomerMessage.BatchFileSourceAddress, destinationFilePath);
                string strEmail = "";
                int transactionId = (from s in context.Ref_TransactionTypes
                                     where s.TransactionCode == transType
                                     select s.Id).FirstOrDefault();
                var strMapperid = context1.Trn_SubsiToTransactionFileMapper.Where(i => i.EDISubsiId == objGPCustomerMessage.SubsiId).Select(i => i.Id).FirstOrDefault();
                strEmail = String.Join(",", context1.Trn_MapperNameToEmailIdMapper.Where(i => i.MapperId == context1.Trn_SubsiToTransactionFileMapper.Where(j => j.EDISubsiId == objGPCustomerMessage.SubsiId).Select(j => j.Id).FirstOrDefault()).Select(i => i.EmailId).ToList());
                Gen_TransactionValidationTracker validator = new Gen_TransactionValidationTracker();
                validator.CreatedDateTime = DateTime.Now;
                validator.EmailId = strEmail;
                validator.FileContent = objGPCustomerMessage.Message;
                //need to update the path
                validator.FilePath = destinationFilePath;
                validator.FileName = objGPCustomerMessage.BatchFileName;
                validator.FileType = SOURCE_TYPES.FTP_SERVICE.ToString();
                validator.MapperName = objGPCustomerMessage.MapperName;
                validator.SubsiId = objGPCustomerMessage.SubsiId;
                validator.ValidationErrorMessage = "Unable to process the file";
                validator.TransactionId = transactionId;
                validator.isValidEmail = true;
                context.Gen_TransactionValidationTracker.Add(validator);
                context.SaveChanges();
            }
        }

        protected bool Validate(GPCustomerMessage objGPCustomerMessage)
        {
            bool isValidFile = false;
            isValidFile = ValidateBase(objGPCustomerMessage);
            if (isValidFile)
            {
                FileInfo objFileInfo = new FileInfo(objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName);
                if (!objFileInfo.Exists)
                {
                    isValidFile = false;
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                }
                else
                {
                    try
                    {
                        //KeyValuePair<TRANSACTION_TYPES, long> objCustTran = EntityMaster.DeriveCustomerAndTransFromFilePath(objGPCustomerMessage.BatchFileSourceAddress);
                        //if (objCustTran.Value >= 0)
                        //{
                        //    Customer objCustomer = EntityMaster.GetCustomer(objCustTran.Value);
                        //    objGPCustomerMessage.CustomerName = objCustomer.CustomerName;
                        //    objGPCustomerMessage.TransactionType = objCustTran.Key;
                        //    objGPCustomerMessage.CustomerCode = objCustTran.Value;
                        //    objGPCustomerMessage.CustomerHeadCompany = objCustomer.CustomerHeadCompany.CustomerHeadCompanyName;
                        //}

                        //Create the Message content separately for Excel files
                        if (objGPCustomerMessage.BatchFileName.EndsWith("xls") || objGPCustomerMessage.BatchFileName.EndsWith("xlsx") || objGPCustomerMessage.BatchFileName.EndsWith("XLS") || objGPCustomerMessage.BatchFileName.EndsWith("XLSX"))
                        {
                            if (objGPCustomerMessage.CustomerName == "SUMITOMO RUBBER ASIA (TYRE) PTE - SINGAPORE" && objGPCustomerMessage.TransactionType == TRANSACTION_TYPES.SO)
                            {
                                objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName, 21, 0);
                            }
                            else
                            {
                                objGPCustomerMessage.Message = ExcelParser.ExcelToCSV(objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName);
                            }
                        }
                        else
                        {
                            using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                            {
                                objGPCustomerMessage.Message = objStreamReader.ReadToEnd();
                                objStreamReader.Close();
                                objStreamReader.Dispose();
                            }
                        }
                    }
                    catch (Exception exp)
                    {
                        isValidFile = false;
                        objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                        log.Info("File is not in correct format", exp);
                    }
                }
            }
            return isValidFile;
        }

        protected virtual bool ValidateBase(GPCustomerMessage objGPCustomerMessage)
        {
            log.Debug("Entering Validate method");

            // Init vars
            bool blnValidFileType = true;

            // Perform validation
            blnValidFileType &= this.Validate_IsFileTypeSupported(objGPCustomerMessage);

            log.Debug("Leaving Validate method");
            return blnValidFileType;
        }

        private bool Validate_IsFileTypeSupported(GPCustomerMessage objGPCustomerMessage)
        {
            log.Debug("Entering Validate_IsFileTypeSupported method");
            bool blnValidFileType = false;
            try
            {
                FILE_TYPES fileType;
                blnValidFileType = GPArchitecture.Cache.SupportedFileTypes.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                if (blnValidFileType == false)
                {
                    objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                    objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                    log.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                }
            }
            catch (Exception exp)
            {
                log.Error("Error while Validating the file", exp);
                throw exp;
            }
            log.Debug("Leaving Validate_IsFileTypeSupported method");
            return blnValidFileType;
        }

        protected void ClearSourceFile(GPCustomerMessage objGPCustomerMessage)
        {
            string strSourcePath = objGPCustomerMessage.BatchFileSourceAddress + "\\" + objGPCustomerMessage.BatchFileName;
            try
            {
                File.Delete(strSourcePath);
            }
            catch (Exception exp)
            {
                log.Error("Error deleting customer file " + strSourcePath, exp);
            }
        }

        private string LoadFileContent(string filePathName)
        {
            string fileContent = "";
            FileInfo objFileInfo = new FileInfo(filePathName);
            if (!objFileInfo.Exists)
            {
                log.Warn("Error: SAP Response response File was not found ");
            }
            else
            {
                try
                {
                    using (System.IO.StreamReader objStreamReader = objFileInfo.OpenText())
                    {
                        fileContent = objStreamReader.ReadToEnd();
                        objStreamReader.Close();
                        objStreamReader.Dispose();
                    }
                }
                catch (Exception exp)
                {
                    log.Error("File is not in correct format", exp);
                }
            }
            return fileContent;
        }

        private List<SCSAPResponse> ProcessSCResponse(IList<GPArchitecture.CommonTypes.SourceMessage> responseMessage, TRANSACTION_TYPES transType, out string errorMessage)
        {
            log.Debug("Entering ProcessSCResponse method");
            //ResponseProcessorDAFactory objRespDAFactory = new ResponseProcessorDAFactory(DBConnection.SqlConnString);
            StringBuilder responseErrorMessageBuilder = new StringBuilder();
            List<SCSAPResponse> scSAPResponses = new List<SCSAPResponse>();
            List<SCSAPResponse> scSAPResponsesProcessed = new List<SCSAPResponse>();

            try
            {
                // Load responses into  SAP Response object list
                foreach (GPArchitecture.CommonTypes.SourceMessage responseMessageLine in responseMessage)
                {
                    SCSAPResponse scSAPResponse = new SCSAPResponse();
                    scSAPResponse.SerialNumber = responseMessageLine.Fields[1].Value.Trim();
                    scSAPResponse.ITRType = responseMessageLine.Fields[2].Value.Trim();
                    scSAPResponse.ITRNumber = responseMessageLine.Fields[3].Value.Trim();
                    scSAPResponse.Status = SAPResponseStatus.ConvertSAPResponseCodeToStateCode(responseMessageLine.Fields[4].Value.Trim());
                    scSAPResponse.ResponseMessage = responseMessageLine.Fields[5].Value.Trim();
                    if (responseMessageLine.Fields.Count() >= 6)
                    {
                        scSAPResponse.NewITRNumber = responseMessageLine.Fields[6].Value.Trim();
                    }
                    if (responseMessageLine.Fields.Count > 11)
                    {
                        scSAPResponse.SINumber = (responseMessageLine.Fields[12].Value != null) ? responseMessageLine.Fields[12].Value.Trim() : string.Empty;
                        scSAPResponse.CustomerCode = GPTools.ConvertStringToLong((responseMessageLine.Fields[13].Value != null) ? responseMessageLine.Fields[13].Value.Trim() : string.Empty);
                    }
                    // Add to list
                    scSAPResponses.Add(scSAPResponse);
                }

                // Loop through each response line and store processed SC responses in container
                foreach (SCSAPResponse scSAPResponse in scSAPResponses)
                {
                    // Collect response message if SC SAP Response line status is either warning or error
                    if (scSAPResponse.Status == SAPResponseStates.ERROR || scSAPResponse.Status == SAPResponseStates.WARNING)
                    {
                        responseErrorMessageBuilder.Append(scSAPResponse.ITRNumber)
                            .Append(":")
                            .Append(scSAPResponse.ResponseMessage)
                            .Append(Environment.NewLine);
                    }

                    // Get matching transaction details from TRANS_BATCH and TRANS_LINE table
                    List<Trn_TransBatchLanxess> transBatchList = new List<Trn_TransBatchLanxess>();
                    if (transType == TRANSACTION_TYPES.PSSC || transType == TRANSACTION_TYPES.SSC)
                    {
                        transBatchList = FindSSCTransactionLine(scSAPResponse.ITRNumber, transType);
                    } 

                    // Loop through each matching SC transaction lines pulled from the transactional tables
                    foreach (Trn_TransBatchLanxess transBatch in transBatchList)
                    {
                        // Loop through each line item
                        foreach (Trn_TransLineLanxess transLine in transBatch.Trn_TransLineLanxess)
                        {

                            SCSAPResponse processedSAPResponse = null;

                            // Clone SC Response being processsed
                            processedSAPResponse = GPTools.DeepClone<SCSAPResponse>(scSAPResponse);
                            processedSAPResponse.BatchId = transBatch.Id;
                            processedSAPResponse.TranLineId = transLine.Id;
                            processedSAPResponse.CustomerCode = (processedSAPResponse.CustomerCode < 1) ? long.Parse(transLine.CustomerCode) : processedSAPResponse.CustomerCode;
                            processedSAPResponse.SINumber = (string.IsNullOrEmpty(processedSAPResponse.SINumber)) ? transLine.SourceReference : processedSAPResponse.SINumber;
                            processedSAPResponse.TranLineSourceReference = transLine.SourceReference;
                            processedSAPResponse.TranLineStatus = DetermineLineStatus(LineStatus.SUBMITTED, scSAPResponse.Status);
                            processedSAPResponse.TranLineGPReference = (!string.IsNullOrEmpty(scSAPResponse.NewITRNumber)) ? scSAPResponse.NewITRNumber : processedSAPResponse.TranLineSourceReference;
                            processedSAPResponse.ResponseMessage = scSAPResponse.ResponseMessage;

                            // Add SC response to processed response object
                            scSAPResponsesProcessed.Add(processedSAPResponse);

                            //Add sc response to TRANS_LINE_CONTENT 
                            UpdateTransLineContent(processedSAPResponse.TranLineId, processedSAPResponse.BatchId, processedSAPResponse.TranLineStatus, processedSAPResponse.ResponseMessage);

                        }//-- end foreach loop on TRANS_LINE
                    }//--end foreach loop on TRANS_BATCH

                }//-- end foreach look on SC responses                
            }
            catch (Exception ex)
            {
              //  Tools.SendEmail_ErrorGen(ResponseProcessorService.Service, "Error encountered in ResponseFileProcessor of Response Processor Service. " + ex.Message, transType.ToString());
                log.Error("Error while proceesing ProcessSCResponse method", ex);

                // return null
                errorMessage = ex.Message;
                return null;
            }
            finally
            {
                log.Debug("Leaving ProcessSCResponse method");
            }
            errorMessage = responseErrorMessageBuilder.ToString();
            return scSAPResponsesProcessed;
        }

        public List<Trn_TransBatchLanxess> FindSSCTransactionLine(string ediGeneratedItrNumber, TRANSACTION_TYPES transactionType)
        {
            List<Trn_TransBatchLanxess> transBatchList = new List<Trn_TransBatchLanxess>();
            string transTypeStr = null;
            // Validate value of transaction Type
            if (transactionType != TRANSACTION_TYPES.SSC && transactionType != TRANSACTION_TYPES.PSSC)
            {
                return transBatchList;
            }
            else
            {
                transTypeStr = transactionType.ToString();
            }
            using (var dataContext = new GoodpackEDIEntities())
            {
                // Get records from TRANS_LINE and TRANS_BATCH table which matches the customer po number (source reference)
                // and the line number where the line status is not success
                // and the transaction isn't closed yet
                var data = from p in context.Trn_TransLineLanxess
                           where p.EDIReference == ediGeneratedItrNumber
                           && p.Trn_LineStatus.StatusCode != "SUCCESS"
                           select new
                           {
                               p.BatchId,
                               p.Id,
                               p.LineNumber,
                               p.SourceReference,
                               p.GPReference,
                               p.EDIReference,
                               p.CustomerRefNumber,
                               p.BinType,
                               p.CustomerCode
                           };
                foreach (var dataResultItem in data)
                {
                    Trn_TransBatchLanxess transBatch;
                    if (transBatchList.Count(batch => batch.Id == dataResultItem.BatchId) == 0)
                    {
                        transBatch = new Trn_TransBatchLanxess();
                        transBatch.Id  = dataResultItem.BatchId;
                        // Add to list
                        transBatchList.Add(transBatch);
                    }
                    else
                    {
                        transBatch = transBatchList.Single(batch => batch.Id == dataResultItem.BatchId);
                    }
                    Trn_TransLineLanxess transLine = new Trn_TransLineLanxess();
                    transLine.Trn_TransBatchLanxess = transBatch;
                    transLine.Id = dataResultItem.Id;
                    transLine.LineNumber = dataResultItem.LineNumber;
                    transLine.SourceReference = dataResultItem.SourceReference;
                    transLine.GPReference = dataResultItem.GPReference;
                    transLine.EDIReference = dataResultItem.EDIReference;
                    transLine.BinType = dataResultItem.BinType;
                    transLine.CustomerCode = dataResultItem.CustomerCode;

                    transBatch.Trn_TransLineLanxess.Add(transLine);
                } 
            }
            return transBatchList;
        }//-- End FindSCTransactionLine()  

        public static GPArchitecture.CommonTypes.SourceMessage LoadSourceMessageSpecs(int subsiId, TRANSACTION_TYPES transType, string service)
        {
            GPArchitecture.CommonTypes.SourceMessage specModel = new GPArchitecture.CommonTypes.SourceMessage();
            // Create the array to store the field specs.
            //GoodyearFieldModel[] fileModel = new GoodyearFieldModel[26];
            GoodyearFieldModel[] fileModel = new GoodyearFieldModel[13];
            // Populate the model array 
            for (int i = 0; i < 13; i++)
            {
                fileModel[i] = new GoodyearFieldModel();
            }

            // Assign details.
            fileModel[0].FieldSeq = 1;
            fileModel[0].FieldName = "Serial Number";
            fileModel[0].Length = 10;
            fileModel[1].FieldSeq = 2;
            fileModel[1].FieldName = "ITR Type";
            fileModel[1].Length = 4;
            fileModel[2].FieldSeq = 3;
            fileModel[2].FieldName = "ITR Number";
            fileModel[2].Length = 10;
            fileModel[3].FieldSeq = 4;
            fileModel[3].FieldName = "Status";
            fileModel[3].Length = 1;
            fileModel[4].FieldSeq = 5;
            fileModel[4].FieldName = "Message";
            fileModel[4].Length = 200;
            fileModel[5].FieldSeq = 6;
            fileModel[5].FieldName = "New ITR Number";
            fileModel[5].Length = 200;
            fileModel[6].FieldSeq = 7;
            fileModel[6].FieldName = "New Item Number";
            fileModel[6].Length = 10;
            fileModel[7].FieldSeq = 8;
            fileModel[7].FieldName = "Legacy (ITR Number)";
            fileModel[7].Length = 10;
            fileModel[8].FieldSeq = 9;
            fileModel[8].FieldName = "Legacy (Item Number)";
            fileModel[8].Length = 10;
            fileModel[9].FieldSeq = 10;
            fileModel[9].FieldName = "Legacy (Material)";
            fileModel[9].Length = 18;
            fileModel[10].FieldSeq = 11;
            fileModel[10].FieldName = "Legacy (Quantity)";
            fileModel[10].Length = 18;
            fileModel[11].FieldSeq = 12;
            fileModel[11].FieldName = "SI Number";
            fileModel[11].Length = 200;
            fileModel[12].FieldSeq = 13;
            fileModel[12].FieldName = "Customer Code";
            fileModel[12].Length = 200;
            //fileModel[13].FieldSeq = 14;
            //fileModel[13].FieldName = "Vessel Name";
            //fileModel[13].Length = 20;
            //fileModel[14].FieldSeq = 15;
            //fileModel[14].FieldName = "Voyage Number";
            //fileModel[14].Length = 20;
            //fileModel[15].FieldSeq = 16;
            //fileModel[15].FieldName = "BOL Number";
            //fileModel[15].Length = 20;
            //fileModel[16].FieldSeq = 17;
            //fileModel[16].FieldName = "BOL Qty";
            //fileModel[16].Length = 13;
            //fileModel[17].FieldSeq = 18;
            //fileModel[17].FieldName = "Container Number";
            //fileModel[17].Length = 20;
            //fileModel[18].FieldSeq = 19;
            //fileModel[18].FieldName = "Container Qty";
            //fileModel[18].Length = 13;
            //fileModel[19].FieldSeq = 20;
            //fileModel[19].FieldName = "Ocean ETD";
            //fileModel[19].Length = 8;
            //fileModel[20].FieldSeq = 21;
            //fileModel[20].FieldName = "Arrival Port";
            //fileModel[20].Length = 30;
            //fileModel[21].FieldSeq = 22;
            //fileModel[21].FieldName = "Arrival Date";
            //fileModel[21].Length = 8;
            //fileModel[22].FieldSeq = 23;
            //fileModel[22].FieldName = "Customer Code";
            //fileModel[22].Length = 10;
            //fileModel[23].FieldSeq = 24;
            //fileModel[23].FieldName = "Customer Name";
            //fileModel[23].Length = 35;
            //fileModel[24].FieldSeq = 25;
            //fileModel[24].FieldName = "Booking Number";
            //fileModel[24].Length = 20;
            //fileModel[25].FieldSeq = 26;
            //fileModel[25].FieldName = "End of Record";
            //fileModel[25].Length = 1;


            using (var context = new GoodpackEDIEntities())
            {

                specModel.ID = 192;
                specModel.IsHeaderPresent = false;
                specModel.MinFieldValCount = 4;
                specModel.SourceMessageFormat = "Tab_Delimited";
                specModel.CustomerHeadCompany = subsiId.ToString();
                specModel.TransactionType = TRANSACTION_TYPES.SSC;
                GPArchitecture.CommonTypes.SourceMessageField messageField;
                for (int i = 0; i < 13; i++)
                {
                    messageField = new GPArchitecture.CommonTypes.SourceMessageField();
                    messageField.FieldName = fileModel[i].FieldName;
                    messageField.Length = fileModel[i].Length;
                    messageField.Value = fileModel[i].FieldSeq.ToString();
                    specModel.Fields.Add(i+1, messageField);
                }

            }
            return specModel;

        }

        public static LineStatus DetermineLineStatus(LineStatus currentStatus, SAPResponseStates responseStatus)
        {
            LineStatus newStatus;

            if (currentStatus == LineStatus.ERROR || responseStatus == SAPResponseStates.ERROR)
            {
                newStatus = LineStatus.ERROR;
            }
            else if (responseStatus == SAPResponseStates.SUCCESS)
            {
                newStatus = LineStatus.SUCCESS;
            }
            else if (responseStatus == SAPResponseStates.INFORMATION)
            {
                newStatus = LineStatus.INFORMATION;
            }
            else
            {
                newStatus = LineStatus.SUBMITTED;
            }
            return newStatus;
        }

        private bool InsertTransLineContent(int lineId, int batchId, LineStatus status,string resonseMessage)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_TransLineContentLanxess lineContent = new Trn_TransLineContentLanxess();                                  
                isSuccess = false;
                lineStatus = status.ToString();                   
                log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == lineStatus
                                    select s.Id).FirstOrDefault();
                lineContent.BatchID = batchId;
                lineContent.LineID = lineId;
                lineContent.StatusID = lineStatusId;
                lineContent.SapResponseMessage = resonseMessage;
                context.Trn_TransLineContentLanxess.Add(lineContent);
                context.SaveChanges();                
            }
            return isSuccess;
        }

        private bool UpdateTransLineContent(int lineId, int batchId, LineStatus status, string resonseMessage)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_TransLineContentLanxess lineContent = context.Trn_TransLineContentLanxess.Where(i => i.LineID == lineId).FirstOrDefault();
                isSuccess = false;
                lineStatus = status.ToString();
                log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == lineStatus
                                    select s.Id).FirstOrDefault();
                lineContent.BatchID = batchId;
                lineContent.LineID = lineId;
                lineContent.StatusID = lineStatusId;
                lineContent.SapResponseMessage = resonseMessage;               
                context.SaveChanges();
            }
            return isSuccess;
        }
    }
}
