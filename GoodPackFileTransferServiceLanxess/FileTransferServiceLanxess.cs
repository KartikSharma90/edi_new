﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackFileTransferServiceLanxess
{
    public partial class FileTransferServiceLanxess : ServiceBase
    {
        internal static ILog log = LogManager.GetLogger(typeof(FileTransferServiceLanxess));
        public System.Timers.Timer fileTransferLanxessSyncTimer;
        private static int NoofAttempts = 0;
        FileTransferLanxess filtransferLanxess = new FileTransferLanxess();

        public FileTransferServiceLanxess()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log.Debug("Entering FileTransferServiceLanxess OnStart");
            this.fileTransferLanxessSyncTimer = new System.Timers.Timer();
            TimeSpan objTimeInterval = new TimeSpan(0, 0, int.Parse(GPArchitecture.Cache.Config.getConfig("FileExchangingInterval")));
            this.fileTransferLanxessSyncTimer.Interval = objTimeInterval.TotalMilliseconds;
            this.fileTransferLanxessSyncTimer.Elapsed += fileTransferLanxessSyncTimer_Elapsed;
            this.fileTransferLanxessSyncTimer.Enabled = true;
            this.fileTransferLanxessSyncTimer.Start();
            log.Debug("Leaving FileTransferServiceLanxess Service OnStart");
            while (true) { }
        }

        void fileTransferLanxessSyncTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!filtransferLanxess.isProcessing)
                filtransferLanxess.SFtpDownload();
        }

        public void debugger()
        {
            log.Info("Entering into Start method");
            OnStart(null);
            log.Info("Leaving Start method");
        }
        protected override void OnStop()
        {
            this.fileTransferLanxessSyncTimer.Stop();
            while (filtransferLanxess.isProcessing)
            {
                int timeMillisec = 5000;
                NoofAttempts++;
                this.RequestAdditionalTime(timeMillisec);
                System.Threading.Thread.Sleep(timeMillisec);
                if (NoofAttempts > 2)
                {
                    log.Warn("TermTrip Services Stops Abruptly");
                    break;
                }
            }
            this.ExitCode = 0;
        }
    }
}
