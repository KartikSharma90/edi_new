﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer
{
    public class SCDataVerificationManager
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SCDataVerificationManager));

        public static int GenerateSequenceNum(string sequenceName)
        {

            BigInteger maxVal = 9999999999;
            using (var context = new GoodpackEDIEntities())
            {
                Ref_Sequence seq = (from s in context.Ref_Sequence
                                    select s).FirstOrDefault();

                if (seq == null)
                {
                    Ref_Sequence sequenceData = new Ref_Sequence();
                    sequenceData.Seed = 1;
                    sequenceData.SeqName = sequenceName;
                    context.Ref_Sequence.Add(sequenceData);
                    context.SaveChanges();
                }
                else
                {
                    Ref_Sequence sequence = context.Ref_Sequence.FirstOrDefault(x => x.SeqName == sequenceName);

                    // set @NewSeqVal = Seed = case when Seed = @MaxVal Then 1 else Seed+1 end
                    if (sequence != null)
                    {
                        if (sequence.Seed == maxVal)
                        {
                            sequence.Seed = 1;
                        }
                        else
                        {
                            sequence.Seed = sequence.Seed + 1;
                        }
                        context.SaveChanges();
                    }
                }

                string strSeqNum = (from s in context.Ref_Sequence
                                    where s.SeqName == sequenceName
                                    select s.Seed).FirstOrDefault().ToString();
                int intSeq = 0;
                int.TryParse(strSeqNum, out intSeq);
                return intSeq;
            }

        }

        public static string ExtractDateElements(string targetFieldDateFormat, string sourceFieldDataFormat, string sourceFieldValue, out bool isSuccess, out string errorMessage)
        {
            // Init vars
            sourceFieldValue = sourceFieldValue.Replace(".", "/");
            sourceFieldValue = sourceFieldValue.Replace("-", "/");
            sourceFieldDataFormat = sourceFieldDataFormat.Replace(".", "/");
            sourceFieldDataFormat = sourceFieldDataFormat.Replace("-", "/");
            char[] delimiter = sourceFieldDataFormat.Substring(2, 1).ToCharArray();
            string[] date = sourceFieldValue.Split(delimiter);
            string returnDateTimeValue = string.Empty;
            errorMessage = string.Empty;
            isSuccess = true;
            if (sourceFieldDataFormat != null)
            {
                sourceFieldDataFormat = sourceFieldDataFormat.ToUpper();
            }

            try
            {
                // Check length of date field
                if (sourceFieldValue.Length < 6)
                {
                    throw new Exception(
                        string.Format("The date field value of {0} is too short.  A minimum of 6 numeric characters is expected.", sourceFieldValue)
                        );
                }
                // YYYY is expected if date format contains WW
                if (sourceFieldDataFormat.Contains("WW")
                    && !sourceFieldDataFormat.Contains("YYYY"))
                {
                    throw new Exception(
                        string.Format("YYYY is expected in the date format if it contains WW.", sourceFieldDataFormat)
                        );
                }

                // Preformat source field value and source field date format
                sourceFieldDataFormat = sourceFieldDataFormat.ToUpper().Trim();
                sourceFieldValue = sourceFieldValue.Trim();
                targetFieldDateFormat = targetFieldDateFormat.ToUpper();

                // Crop characters from the source field value if it is longer than the source field date format
                // i.e. MM-YYYY, WW-YYYY, DD-YYYY
                if (sourceFieldValue.Length > sourceFieldDataFormat.Length)
                    sourceFieldValue = sourceFieldValue.Substring(0, sourceFieldDataFormat.Length);
                // If the second character in the source field value is not numeric and the first portion of the date format
                // is not MMM, then add a leading zero
                if (!IsNumeric(sourceFieldValue[1].ToString()) && !sourceFieldDataFormat.StartsWith("MMM"))
                {
                    sourceFieldValue = sourceFieldValue.Insert(0, "0");
                }
                // If the format is MMM-DD, but the day part has only one digit, then insert a leading zero into the day part
                if (sourceFieldDataFormat.StartsWith("MMM") && !IsNumeric(sourceFieldValue[3].ToString()) && !IsNumeric(sourceFieldValue[5].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(4, "0");
                }
                // If the format is MM-DD but the day part has only one digit, then insert a leading zero into the day part
                if (sourceFieldDataFormat.StartsWith("MM") && !sourceFieldDataFormat.StartsWith("MMM") && !IsNumeric(sourceFieldValue[2].ToString()) && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is DD-MM but the month part has only one digit, then insert a leading zero into the month part
                if (sourceFieldDataFormat.StartsWith("DD") && sourceFieldDataFormat.Contains("MM") && !sourceFieldDataFormat.Contains("MMM") && !IsNumeric(sourceFieldValue[2].ToString()) && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YYYY-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[6].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }
                // If the format is YYYY-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[6].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }
                // If the format is YYYY-DD-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[7].ToString())
                    && sourceFieldValue.Length < 10
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(8, "0");
                }
                // If the format is YYYY-MM-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[5].ToString())
                    && sourceFieldValue.Length < 10
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(8, "0");
                }
                // If the format is YY-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YY-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[4].ToString()))
                {
                    sourceFieldValue = sourceFieldValue.Insert(3, "0");
                }
                // If the format is YY-DD-MM but the MM part only has 1 digit, then insert a leading zero into the MM part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("MM")
                    && !sourceFieldDataFormat.Contains("MMM")
                    && !IsNumeric(sourceFieldValue[2].ToString())
                    && !IsNumeric(sourceFieldValue[5].ToString())
                    && sourceFieldValue.Length < 8
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(6, "0");
                }
                // If the format is YY-MM-DD but the DD part only has 1 digit, then insert a leading zero into the DD part
                if (sourceFieldDataFormat.StartsWith("YY")
                    && !sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("DD")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && !IsNumeric(sourceFieldValue[7].ToString())
                    && sourceFieldValue.Length < 8
                    )
                {
                    sourceFieldValue = sourceFieldValue.Insert(6, "0");
                }
                // if the format is YYYY-WW  but the WW part only has 1 digit, then insert a leading zero into WW Part
                if (sourceFieldDataFormat.StartsWith("YYYY")
                    && sourceFieldDataFormat.Contains("WW")
                    && !IsNumeric(sourceFieldValue[4].ToString())
                    && sourceFieldValue.Length < 6)
                {
                    sourceFieldValue = sourceFieldValue.Insert(5, "0");
                }

                // Correct the casing of characters in source field data format to be valid during formatting
                sourceFieldDataFormat = CleanDateFormat(sourceFieldDataFormat);

                // Correct the casing of characters in target data format to be valid during formatting
                targetFieldDateFormat = CleanDateFormat(targetFieldDateFormat);

                //Convert to date time
                DateTime result;

                if (sourceFieldDataFormat.Contains("WW"))
                {
                    // Get location of WW and yyyy
                    int startLocationOfWWInString = sourceFieldDataFormat.IndexOf("WW");
                    int startLocationOfYYYYInString = sourceFieldDataFormat.IndexOf("yyyy");

                    // If index of search keys are non-zero (not found in Date Format string), then throw exception
                    if (startLocationOfWWInString < 0 || startLocationOfYYYYInString < 0)
                    {
                        throw new System.Exception(
                            string.Format("Expected WW and YYYY in the date format provided ({0}).", sourceFieldDataFormat)
                            );
                    }

                    // Get value of WW and YYYY
                    string wwString = sourceFieldValue.Substring(startLocationOfWWInString, 2);
                    string yyyyString = sourceFieldValue.Substring(startLocationOfYYYYInString, 4);

                    // Get monday of given week number of the year
                    result = GetFirstDayOfWeek(
                        int.Parse(yyyyString) // Year
                        , int.Parse(wwString) // Week number
                        , DayOfWeek.Monday
                        );
                }
                else
                {

                    result = DateTime.ParseExact(sourceFieldValue, sourceFieldDataFormat, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal);
                }

                // Get the date according to desired format

                returnDateTimeValue = result.ToString(targetFieldDateFormat);
                // return returnDateTimeValue;
            }
            catch (Exception ex)
            {
                log.Error(" ExtractDateElements in SCDataVerificationManager.targetFieldDateFormat:" + targetFieldDateFormat + ", sourceFieldDataFormat:" + sourceFieldDataFormat + ",sourceFieldValue:" + sourceFieldValue + ",isSuccess:" + isSuccess + ",errorMessage:" + errorMessage + ". Exception : Message- " + ex.Message);
                isSuccess = false;
                errorMessage = string.Format(
                    "Error encountered while extracting date elements from source message field value '{0}' with date format '{1}'.  If the original file is in MS Excel format, the date fields are probabaly not date formatted. {2}"
                    , sourceFieldValue
                    , sourceFieldDataFormat
                    , ex.Message
                    );
                //throw ex;
            }
            return returnDateTimeValue;
        }

        public static EntitiesModel GetCustomer(long customerId)
        {
            
            EntitiesModel entitiesModel = new EntitiesModel();
            using (var context = new GoodpackEDIEntities())
            {
                entitiesModel = (from a in context.Gen_EDISubsies
                                 join e in context.Gen_EDIEntities
                                     on a.Id equals e.SubsiId
                                 where e.EntityCode == customerId
                                 select new EntitiesModel
                                 {
                                     //  ContractEnd =e.ContractEndDate,
                                     ContractNumber = e.ContractNumber,
                                     //  ContractStart=e.ContractStartDate,
                                     CustomerCode = e.EntityCode,
                                     //CustomerHeadCompany=e.SubsiId,
                                     CustomerName = e.EntityName,
                                     CustomerStakeholderEmailList = e.EmailIdList,
                                     FileSourcePath = e.SourcePath,
                                     GoodpackSubsidiaryEmailList = e.GoodpackSubsidaryEmailIdList,
                                     IsEnabled = e.IsEnabled,
                                     Subsidiary = e.GoodpackSubsidary,
                                     Vertical = e.Vertical
                                 }
                            ).FirstOrDefault();
                return entitiesModel;
            }

        }
        public static bool IsNumeric(string number)
        {
            int tempInt;
            return int.TryParse(number, out tempInt);
        }
        public static DateTime GetFirstDayOfWeek(int year, int week, DayOfWeek dayOfTheWeek)
        {
            // If the week day of the first day of the year is greater than the given day of the week,
            // then subtract by week by 2 to get to the week of the previous year
            if (new DateTime(year, 1, 1).DayOfWeek > dayOfTheWeek)
            {
                week = week - 2;
            }
            else
            {
                week = week - 1;
            }

            return GetWeek1Day1(year, dayOfTheWeek).AddDays(7 * week);
        }
        public static DateTime GetWeek1Day1(int year, DayOfWeek firstDayOfWeek)
        {
            DateTime date = new DateTime(year, 1, 1);

            // Move towards firstDayOfWeek
            date = date.AddDays(firstDayOfWeek - date.DayOfWeek);

            // Either 1 or 52 or 53
            int weekOfYear = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstFullWeek, firstDayOfWeek);

            // Move forwards 1 week if week is 52 or 53
            date = date.AddDays(7 * System.Math.Sign(weekOfYear - 1));

            return date;
        }
        public static string CleanDateFormat(string dateFormat)
        {
            return dateFormat.ToUpper()
                 .Replace("YYYY", "yyyy")
                 .Replace("YY", "yy")
                 .Replace("DD", "dd");
        }

        

    }
}
