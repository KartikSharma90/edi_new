﻿using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.GoodPackBusinessLayer.TermTripManagementLayer;
using GPArchitecture.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackFileTransferServiceLanxess.BusinessLayer.SCManagementLayer
{
    public class SCTransactionBatch
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SCTransactionBatch));

        public int AddBatchFileDataLanxess(SCTransactionModel transBtachDetails, string mapperFile)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string transactionType = transBtachDetails.TransactionType.ToString();
                int transTypeID = (from s in context.Ref_TransactionTypes
                                   where s.TransactionCode == transactionType
                                   select s.Id).FirstOrDefault();
                string statusCode = transBtachDetails.StatusCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();


                Trn_TransBatchLanxess scFileData = new Trn_TransBatchLanxess();
                scFileData.UserId = transBtachDetails.UserId;
                scFileData.SubsyId = transBtachDetails.SubsiId;
                scFileData.TransactionId = transTypeID;
                scFileData.MapperFileName = mapperFile;
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.FileName = transBtachDetails.BatchFileName;
                scFileData.FileSource = transBtachDetails.BatchFileSourceType.ToString();
                scFileData.FileSourceAddress = transBtachDetails.BatchFileSourceAddress;
                scFileData.BatchContent = transBtachDetails.Message;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateCreated = DateTime.Now;
                scFileData.DateUpdated = null;
                scFileData.PODate = null;
                scFileData.RecordCountTotal = 0;
                scFileData.RecordCountSkipValFailure = 0;
                scFileData.RecordCountFieldValFailure = 0;
                scFileData.RecordCountSAPError = 0;
                scFileData.RecordCountSuccess = 0;
                scFileData.RecordCountSAPInProgress = 0;
                scFileData.DateExportedToTargetSystem = null;
                scFileData.IsTestMode = true;
                scFileData.EmailId = transBtachDetails.EmailId;
                context.Trn_TransBatchLanxess.Add(scFileData);
                context.SaveChanges();
                return scFileData.Id;
            }
        }

        public int InsertTransLineLanxess(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference, string customerReference)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_TransLineLanxess transLine = new Trn_TransLineLanxess();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = lineReference.LineContentItems;
                transLine.CustomerCode = lineReference.CustomerCode;
                transLine.BinType = lineReference.MaterialCode;
                transLine.CustomerRefNumber = customerReference;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }

                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                context.Trn_TransLineLanxess.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }
        
        public int AddSCBatchFileData(SCTransactionModel transBtachDetails, string mapperFile)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string transactionType = transBtachDetails.TransactionType.ToString();
                int transTypeID = (from s in context.Ref_TransactionTypes
                                   where s.TransactionCode == transactionType
                                   select s.Id).FirstOrDefault();
                string statusCode = transBtachDetails.StatusCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();


                Trn_SCTransBatch scFileData = new Trn_SCTransBatch();
                scFileData.UserId = transBtachDetails.UserId;
                scFileData.SubsyId = transBtachDetails.SubsiId;
                scFileData.TransactionId = transTypeID;
                scFileData.MapperFileName = mapperFile;
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.FileName = transBtachDetails.BatchFileName;
                scFileData.FileSource = transBtachDetails.BatchFileSourceType.ToString();
                scFileData.FileSourceAddress = transBtachDetails.BatchFileSourceAddress;
                scFileData.BatchContent = transBtachDetails.Message;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateCreated = DateTime.Now;
                scFileData.DateUpdated = null;
                scFileData.PODate = null;
                scFileData.RecordCountTotal = 0;
                scFileData.RecordCountSkipValFailure = 0;
                scFileData.RecordCountFieldValFailure = 0;
                scFileData.RecordCountSAPError = 0;
                scFileData.RecordCountSuccess = 0;
                scFileData.RecordCountSAPInProgress = 0;
                scFileData.DateExportedToTargetSystem = null;
                scFileData.IsTestMode = true;
                scFileData.EmailId = transBtachDetails.EmailId;
                context.Trn_SCTransBatch.Add(scFileData);
                context.SaveChanges();
                int scFileId = (from s in context.Trn_SCTransBatch
                                where (s.Id == scFileData.Id)
                                select s.Id).FirstOrDefault();


                return scFileId;


            }

        }

        public int AddSOBatchFileData(SCTransactionModel transBtachDetails, string mapperFile)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string transactionType = transBtachDetails.TransactionType.ToString();
                int transTypeID = (from s in context.Ref_TransactionTypes
                                   where s.TransactionCode == transactionType
                                   select s.Id).FirstOrDefault();
                string statusCode = transBtachDetails.StatusCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();


                Trn_SCTransBatch scFileData = new Trn_SCTransBatch();
                scFileData.UserId = transBtachDetails.UserId;
                scFileData.SubsyId = transBtachDetails.SubsiId;
                scFileData.TransactionId = transTypeID;
                scFileData.MapperFileName = mapperFile;
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.FileName = transBtachDetails.BatchFileName;
                scFileData.FileSource = transBtachDetails.BatchFileSourceType.ToString();
                scFileData.FileSourceAddress = transBtachDetails.BatchFileSourceAddress;
                scFileData.BatchContent = transBtachDetails.Message;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateCreated = DateTime.Now;
                scFileData.DateUpdated = null;
                scFileData.PODate = null;
                scFileData.RecordCountTotal = 0;
                scFileData.RecordCountSkipValFailure = 0;
                scFileData.RecordCountFieldValFailure = 0;
                scFileData.RecordCountSAPError = 0;
                scFileData.RecordCountSuccess = 0;
                scFileData.RecordCountSAPInProgress = 0;
                scFileData.DateExportedToTargetSystem = null;
                scFileData.IsTestMode = false;
                scFileData.EmailId = transBtachDetails.EmailId;
                context.Trn_SCTransBatch.Add(scFileData);
                context.SaveChanges();
                int scFileId = (from s in context.Trn_SCTransBatch
                                where (s.Id == scFileData.Id)
                                select s.Id).FirstOrDefault();


                return scFileId;


            }

        }

        public void UpdateSCBatchFileData(SCTransactionModel transBtachDetails)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = transBtachDetails.StatusCode.ToString();
                string errorCode = transBtachDetails.ErrorCode.ToString();
                int statusID = (from s in context.Ref_BatchStatus
                                where s.StatusCode == statusCode
                                select s.Id).FirstOrDefault();
                int errorCodeID = (from s in context.Ref_ErrorCodes
                                   where s.ErrorCode == errorCode
                                   select s.Id).FirstOrDefault();

                Trn_SCTransBatch scFileData = context.Trn_SCTransBatch.FirstOrDefault(x => (x.Id == transBtachDetails.BatchID)); //&& x.StatusId != 10 && x.StatusId != 12 && x.StatusId != 13));//Give where condition in valuetype
                scFileData.StatusId = statusID;
                scFileData.ErrorId = errorCodeID;
                scFileData.ErrorDescription = transBtachDetails.ErrorDesc;
                scFileData.DateUpdated = DateTime.Now;
                context.SaveChanges();
            }

        }

        public void UpdateSCBatchCounts(int BatchId, int RecordCountTotal, int RecordCountSkipValFailure, int RecordCountFieldValFailure,
            int RecordCountSAPInProgress, int RecordCountSAPError, int RecordCountSuccess, string strService)
        {
            using (var context = new GoodpackEDIEntities())
            {
                if (strService == "Request")
                {
                    Trn_SCTransBatch scBatch = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scBatch.RecordCountTotal = RecordCountTotal;
                    scBatch.RecordCountSkipValFailure = RecordCountSkipValFailure;
                    scBatch.RecordCountFieldValFailure = RecordCountFieldValFailure;
                    // scBatch.RecordCountSuccess = RecordCountSuccess;
                    scBatch.RecordCountSAPInProgress = RecordCountSAPInProgress;
                    context.SaveChanges();

                    if (RecordCountSAPInProgress != 0)
                    {
                        Trn_SCTransBatch scBatchData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                        scBatchData.DateExportedToTargetSystem = DateTime.Now;
                        context.SaveChanges();
                    }
                }
                else
                {
                    //UPDATE TRANS_BATCH SET RecordCountSuccess=@SuccessCount, RecordCountSAPError=@SAPErrorCount

                    //   where BatchID=@BatchId

                    Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scData.RecordCountSuccess = RecordCountSuccess;
                    scData.RecordCountSAPError = RecordCountSAPError;
                    context.SaveChanges();

                    Trn_SCTransBatch scTransData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == BatchId);
                    scTransData.RecordCountSAPInProgress = RecordCountTotal - RecordCountSkipValFailure - RecordCountFieldValFailure - RecordCountSAPError - RecordCountSuccess;
                    context.SaveChanges();
                }
            }
        }

        public void UpdateSCBatchCounts(int batchId, int errorCount, int successCount)
        {
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SCTransBatch scData = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scData.RecordCountSuccess = successCount;
                scData.RecordCountSAPError = errorCount;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransBatch table of batch Id:" + batchId + " with success count:" + successCount + "and error count:" + errorCount);
            }
        }


        public int InsertTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference, string customerReference)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_SCTransLine transLine = new Trn_SCTransLine();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = lineReference.LineContentItems;
                transLine.CustomerCode = lineReference.CustomerCode;
                transLine.BinType = lineReference.MaterialCode;
                transLine.CustomerRefNumber = customerReference;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }

                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                context.Trn_SCTransLine.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }
        public List<int> InsertTransLineBulk(SCTransactionModel transactionModel, IList<LineReferences> lineReferenceData, string gpReference, List<Dictionary<string, string>> scDataVal, int BatchId, int subsiId, LineStatus? statusCode)
        {
            List<int> transLineIds = new List<int>();
            using (var context = new GoodpackEDIEntities())
            {
                int i = 0;
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();
                foreach (LineReferences lineReference in lineReferenceData)
                {
                    //string statusCodeVal = lineReference.StatusCode.ToString();
                    //int statusID = (from s in context.Trn_LineStatus
                    //                where s.StatusCode == statusCodeVal
                    //                select s.Id).FirstOrDefault();
                    Trn_SCTransLine transLine = new Trn_SCTransLine();
                    transLine.BatchId = transactionModel.BatchID;
                    transLine.LastUpdated = DateTime.Now;
                    transLine.LineContent = lineReference.LineContentItems;
                    transLine.CustomerCode = lineReference.CustomerCode;
                    transLine.BinType = lineReference.MaterialCode;
                    transLine.CustomerRefNumber = scDataVal[i][SCSapMessageFields.CustomerReferenceNumber];
                    if (lineReference.LineNumber == null)
                    {
                        transLine.LineNumber = 0;
                    }
                    else
                    {
                        transLine.LineNumber = int.Parse(lineReference.LineNumber);
                    }

                    transLine.SourceReference = lineReference.SourceReferenceCode;
                    transLine.GPReference = gpReference;
                    transLine.EDIReference = lineReference.EdiNumber;
                    transLine.StatusId = statusID;
                    transLine.SubsiId = transactionModel.SubsiId;
                    transLine.IsActive = true;

                    transLine.CustomerCode = scDataVal[i][SCSapMessageFields.CustomerNumber];
                    string dateIssue = scDataVal[i][SCSapMessageFields.ETDDate];
                    if ((dateIssue != "") || (dateIssue != null))
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        transLine.ETD = yearVal + "-" + monthVal + "-" + dateVal;
                    }

                    string dateIssued = scDataVal[i][SCSapMessageFields.DTADate];
                    if ((dateIssued != "") || (dateIssued != null))
                    {
                        string dateVal = dateIssued.Substring(0, 2);
                        string monthVal = dateIssued.Substring(2, 2);
                        string yearVal = dateIssued.Substring(4, 4);
                        transLine.ETA = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }

                    if (scDataVal[i][SCSapMessageFields.FromLocation].Length != 10)
                    {
                        transLine.FromLocation = "0000" + scDataVal[i][SCSapMessageFields.FromLocation];
                    }
                    else
                    {
                        transLine.FromLocation = scDataVal[i][SCSapMessageFields.FromLocation];
                    }
                    transLine.LastUpdated = DateTime.Now;
                    transLine.Remarks = scDataVal[i][SCSapMessageFields.Remarks];
                    transLine.SINumber = scDataVal[i][SCSapMessageFields.SINumber];
                    if (scDataVal[i][SCSapMessageFields.Quantity].Length != 0)
                    {
                        int s;
                        int.TryParse(scDataVal[i][SCSapMessageFields.Quantity], out s);
                        if (s == 0)
                            throw new InvalidCastException("Quantity should be integer");
                        transLine.Quantity = Convert.ToInt32(scDataVal[i][SCSapMessageFields.Quantity]);

                    }
                    else
                    {
                        transLine.Quantity = 0;
                    }
                    if (scDataVal[i][SCSapMessageFields.ToLocation].Length != 10)
                    {
                        transLine.ToLocation = "0000" + scDataVal[i][SCSapMessageFields.ToLocation];
                    }
                    else
                    {
                        transLine.ToLocation = scDataVal[i][SCSapMessageFields.ToLocation];
                    }
                    transLine.BinType = scDataVal[i][SCSapMessageFields.MaterialNumber];
                    transLine.CustomerRefNumber = scDataVal[i][SCSapMessageFields.CustomerReferenceNumber];
                    transLine.ItrType = scDataVal[i][SCSapMessageFields.DocumentType];
                    transLine.ItrNumber = scDataVal[i][SCSapMessageFields.ITRNumber];
                    transLine.RefItrNumber = scDataVal[i][SCSapMessageFields.ReferenceITRNumber];
                    transLine.ItemNumber = scDataVal[i][SCSapMessageFields.ItemNo];
                    transLine.ContainerNumber = scDataVal[i][SCSapMessageFields.ContainerNumber];
                    transLine.SalesDocument = scDataVal[i][SCSapMessageFields.SalesDocument];
                    if (transLine.SalesDocument.Length != 0)
                    {
                        transLine.SalesDocumentItem = ConstantUtilities.SalesDocumentItem;
                    }
                    context.Trn_SCTransLine.Add(transLine);
                    i++;
                    //  transLineIds.Add(transLine.Id);
                }
                context.SaveChanges();
                transLineIds = context.Trn_SCTransLine.Where(x => x.BatchId == transactionModel.BatchID).Select(x => x.Id).ToList();
                return transLineIds;
            }
        }

        public int InsertASNTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference, string customerReference)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_ASNTransLine transLine = new Trn_ASNTransLine();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = lineReference.LineContentItems;
                transLine.CustomerCode = lineReference.CustomerCode;
                transLine.BinType = lineReference.MaterialCode;
                transLine.CustomerRefNumber = customerReference;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }

                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                context.Trn_ASNTransLine.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }

        public int InsertSSCTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference, string customerReference, string itrNumber)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_SSCTransLine transLine = new Trn_SSCTransLine();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.BatchNumber = lineReference.BatchNumber;
                transLine.LineContent = lineReference.LineContentItems;
                transLine.CustomerCode = lineReference.CustomerCode;
                transLine.MaterialNumber = lineReference.MaterialCode;
                transLine.CustomerReferenceNumber = customerReference;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }
                transLine.ItrNumber = itrNumber;
                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                context.Trn_SSCTransLine.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }
        public int InsertSOTransLine(SCTransactionModel transactionModel, LineReferences lineReference, string gpReference)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = lineReference.StatusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_SOTransLine transLine = new Trn_SOTransLine();
                transLine.BatchId = transactionModel.BatchID;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = lineReference.LineContentItems;
                if (lineReference.LineNumber == null)
                {
                    transLine.LineNumber = 0;
                }
                else
                {
                    transLine.LineNumber = int.Parse(lineReference.LineNumber);
                }

                transLine.SourceReference = lineReference.SourceReferenceCode;
                transLine.GPReference = gpReference;
                transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = transactionModel.SubsiId;
                transLine.IsActive = true;
                transLine.InTestMode = false;
                context.Trn_SOTransLine.Add(transLine);
                context.SaveChanges();
                return transLine.Id;
            }
        }

        //public void UpdateTransLine()
        public void UpdateTransLine(int batchId, int subsiId, LineStatus? statusCode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                string checkstatusValue = LineStatus.NEW.ToString();
                int checkStatusID = (from s in context.Trn_LineStatus
                                     where s.StatusCode == checkstatusValue
                                     select s.Id).FirstOrDefault();

                var transLine = (context.Trn_SCTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();

                foreach (var item in transLine)
                {
                    if (item.StatusId != 0)
                        item.StatusId = statusID;
                    item.SubsiId = subsiId;
                    item.BatchId = batchId;
                    context.SaveChanges();
                }
            }
        }
        //public void UpdateTransLine()
        public void UpdateASNTransLine(int batchId, int subsiId, LineStatus? statusCode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                string checkstatusValue = LineStatus.NEW.ToString();
                int checkStatusID = (from s in context.Trn_LineStatus
                                     where s.StatusCode == checkstatusValue
                                     select s.Id).FirstOrDefault();

                var transLine = (context.Trn_ASNTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();

                foreach (var item in transLine)
                {
                    if (item.StatusId != 0)
                        item.StatusId = statusID;
                    item.SubsiId = subsiId;
                    item.BatchId = batchId;
                    context.SaveChanges();
                }
            }
        }

        public void UpdateSOTransLine(int batchId, int subsiId, LineStatus? statusCode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                string checkstatusValue = LineStatus.NEW.ToString();
                int checkStatusID = (from s in context.Trn_LineStatus
                                     where s.StatusCode == checkstatusValue
                                     select s.Id).FirstOrDefault();

                var transLine = (context.Trn_SOTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();

                foreach (var item in transLine)
                {
                    if (item.StatusId != 0)
                        item.StatusId = statusID;
                    item.SubsiId = subsiId;
                    item.BatchId = batchId;
                    context.SaveChanges();
                }
            }
        }

        public bool InsertSOTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SOTransLineContent lineContent = new Trn_SOTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SOTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }
        public bool InsertSOChangeCancelTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SOTransLineContent lineContent = new Trn_SOTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SOTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }

        public bool InsertTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SCTransLineContent lineContent = new Trn_SCTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SCTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }

        public bool InsertTransLineContentAndUpdateResponseTransLineAndTransBatch(int lineId, int batchId, List<ResponseModel> responseModel, bool status, int successCount, int errorCount)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SCTransLineContent lineContent = new Trn_SCTransLineContent();
                LineStatus? statusCode;
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();

                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();

                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SCTransLineContent.Add(lineContent);


                    Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                    transLine.StatusId = lineStatusId;
                    transLine.InTestMode = status;
                    try
                    {
                        if (responseModel[i].ResponseItems != null || responseModel[i].ResponseItems.ToString() != "")
                            transLine.ItrNumber = responseModel[i].ResponseItems.ToString();
                    }
                    catch (Exception e) { }
                    int transLineId = (from s in context.Trn_SCTransLine
                                       where ((s.BatchId == batchId) && (s.StatusId == lineStatusId))
                                       select s.Id).FirstOrDefault();
                    string batchStatusVal = null;


                    if (transLineId == 0)
                    {
                        batchStatusVal = BatchStatus.CLOSED.ToString();
                    }
                    else
                    {
                        batchStatusVal = BatchStatus.ERROR.ToString();
                    }
                    log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                    int batchStatusId = (from s in context.Ref_BatchStatus
                                         where s.StatusCode == batchStatusVal
                                         select s.Id).FirstOrDefault();
                    Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                    ITirmTripManager termtrip = new TirmTripManager();
                    scDataValue.StatusId = batchStatusId;
                    scDataValue.IsTestMode = status;
                    var value = scDataValue.Trn_SCTransLine.Select(x => x.CustomerCode).FirstOrDefault();
                    scDataValue.IsTermTrip = termtrip.IsTirmTripORNot(value.Length > 6 ? value.Substring(4, 6) : value);
                    scDataValue.TermTripSAPResponseReceived = false;

                    scDataValue.RecordCountSAPError = errorCount;
                    scDataValue.RecordCountSuccess = successCount;
                    context.SaveChanges();


                    log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + lineStatus);

                }
            }
            return isSuccess;
        }
        public bool InsertASNTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_ASNTransLineContent lineContent = new Trn_ASNTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineID = lineId;
                    lineContent.StatusID = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_ASNTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }
        public bool InsertSSCTransLineContent(int lineId, int batchId, List<ResponseModel> responseModel)
        {
            string lineStatus = null;
            bool isSuccess = true;
            using (var context = new GoodpackEDIEntities())
            {
                Trn_SSCTransLineContent lineContent = new Trn_SSCTransLineContent();
                for (int i = 0; i < responseModel.Count; i++)
                {
                    bool flag = responseModel[i].Success != null ? (bool)(responseModel[i].Success) : false;
                    if (flag)
                    {
                        lineStatus = LineStatus.SUCCESS.ToString();
                    }
                    else
                    {
                        isSuccess = false;
                        lineStatus = LineStatus.ERROR.ToString();
                    }
                    log.Info("Line Status is :" + lineStatus + "of Line Id:" + lineId);
                    int lineStatusId = (from s in context.Trn_LineStatus
                                        where s.StatusCode == lineStatus
                                        select s.Id).FirstOrDefault();
                    lineContent.BatchID = batchId;
                    lineContent.LineId = lineId;
                    lineContent.StatusId = lineStatusId;
                    lineContent.SapResponseMessage = responseModel[i].Message;
                    context.Trn_SSCTransLineContent.Add(lineContent);
                    context.SaveChanges();
                }
            }
            return isSuccess;
        }
        public void UpdateTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_SCTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                ITirmTripManager termtrip = new TirmTripManager();
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = InTestMode;
                var value = scDataValue.Trn_SCTransLine.Select(x => x.CustomerCode).FirstOrDefault();
                scDataValue.IsTermTrip = termtrip.IsTirmTripORNot(value.Length > 6 ? value.Substring(4, 6) : value);
                scDataValue.TermTripSAPResponseReceived = false;
                // scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess+successCount;
                // scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess??0;
                scDataValue.RecordCountSAPError = errorCount;
                scDataValue.RecordCountSuccess = successCount;
                context.SaveChanges();
            }
        }
        public void UpdateTransBatch(int batchId, int successCount, int errorCount, bool InTestMode, bool isTermTrip)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_SCTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                batchStatusId = isTermTrip ? context.Ref_BatchStatus.Where(x => x.StatusCode == "RCVD").Select(x => x.Id).SingleOrDefault() : batchStatusId;
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = InTestMode;
                scDataValue.IsTermTrip = isTermTrip;
                scDataValue.TermTripSAPResponseReceived = false;
                scDataValue.RecordCountSAPError = errorCount;
                scDataValue.RecordCountSuccess = successCount;
                context.SaveChanges();
            }
        }
        public void UpdateASNTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_ASNTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_ASNTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = InTestMode;
                // scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess+successCount;
                // scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess??0;
                scDataValue.RecordCountSAPError = errorCount;
                scDataValue.RecordCountSuccess = successCount;
                context.SaveChanges();
            }
        }



        public void UpdateResponseSSCTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SSCTransLine transLine = (context.Trn_SSCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                context.SaveChanges();
                log.Info("Updated Trn_SSCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateSOTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_SOTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_SOTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = false;
                //scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess + successCount;
                //scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess ?? 0;
                scDataValue.RecordCountSAPError = errorCount;
                scDataValue.RecordCountSuccess = successCount;
                context.SaveChanges();
            }
        }

        public void UpdateResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status, Trn_SCTransLine lineDetail)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                transLine.CustomerCode = lineDetail.CustomerCode;
                transLine.FromLocation = lineDetail.FromLocation;
                transLine.ToLocation = lineDetail.ToLocation;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status, Trn_SCTransLine lineDetail, bool isTermTrip)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = 0;
                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();
                statusID = isTermTrip ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : (from s in context.Trn_LineStatus
                                                                                                                                            where s.StatusCode == statusCodeVal
                                                                                                                                            select s.Id).FirstOrDefault();
                Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                transLine.CustomerCode = lineDetail.CustomerCode;
                transLine.FromLocation = lineDetail.FromLocation;
                transLine.ToLocation = lineDetail.ToLocation;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status, Trn_SCTransLine lineDetail, bool isTermTrip, string itrNumber, string message)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = 0;
                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();
                if (statusCodeVal == "ERROR")
                    statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();
                else
                    statusID = isTermTrip ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : (from s in context.Trn_LineStatus
                                                                                                                                                where s.StatusCode == statusCodeVal
                                                                                                                                                select s.Id).FirstOrDefault();
                Trn_SCTransLine transLine = (context.Trn_SCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                transLine.CustomerCode = lineDetail.CustomerCode;
                transLine.FromLocation = lineDetail.FromLocation;
                transLine.ToLocation = lineDetail.ToLocation;
                transLine.ItrNumber = itrNumber;
                if (isTermTrip)
                    transLine.TermTripSAP = false;
                context.SaveChanges();
                UpdateResponseTransLineContent(lineId, statusID, message, isTermTrip);
                log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        private void UpdateResponseTransLineContent(int lineId, int statusId, string message, bool isTermTrip)
        {
            using (var context = new GoodpackEDIEntities())
            {
                try
                {
                    Trn_SCTransLineContent lineContent = context.Trn_SCTransLineContent.Where(i => i.LineID == lineId).FirstOrDefault();
                    lineContent.StatusID = statusId;
                    if (isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() == statusId)
                        lineContent.SapResponseMessage = "Waiting For SAP";
                    else
                        lineContent.SapResponseMessage = message;
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    log.Error("UpdateResponseTransLineContent error-" + e);
                }
            }
        }
        public void UpdateASNResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_ASNTransLine transLine = (context.Trn_ASNTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                context.SaveChanges();
                log.Info("Updated Trn_SCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateSSCResponseTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SSCTransLine transLine = (context.Trn_SSCTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = status;
                context.SaveChanges();
                log.Info("Updated Trn_SSCTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }
        public void UpdateResponseSOTransLine(int batchId, LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                //string checkstatusValue = LineStatus.SUBMITTED.ToString();
                //int checkStatusID = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == checkstatusValue
                //                     select s.Id).FirstOrDefault();

                Trn_SOTransLine transLine = (context.Trn_SOTransLine.Where(x => (x.Id == lineId)).FirstOrDefault());
                transLine.StatusId = statusID;
                transLine.InTestMode = false;
                context.SaveChanges();
                log.Info("Updated Trn_SOTransLine table of line Id:" + lineId + " with status :" + statusCodeVal);
            }
        }

        public IList<GenericItemModel> ScTestResult(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;
                }
                return itemModel;
            }
        }
        public IList<GenericItemModel> ScTestResultNew(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                List<Trn_SCTransLine> transLine = context.Trn_SCTransLine.Where(x => trnsLineItems.Contains(x.Id)).ToList();
                var statusList = (from s in context.Trn_LineStatus
                                  select new { s.StatusCode, s.Id }).ToList();
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];

                    int statusId = transLine[i].StatusId;
                    string statusCode = statusList.Where(x => x.Id == statusId).Select(x => x.StatusCode).FirstOrDefault();
                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;
                }
                return itemModel;
            }
        }
        public IList<GenericItemModel> ASNTestResult(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_ASNTransLine transLine = context.Trn_ASNTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }

        public IList<GenericItemModel> ScTestResultGoodyear(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i].LineId = lineId;
                    itemModel[i].Status = statusCode;

                }
                return itemModel;
            }
        }

        public IList<GenericItemModel> SScTestResult(IList<GenericItemModel> itemModel, List<Dictionary<string, string>> sscData)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < sscData.Count; i++)
                {
                    int lineId = Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                    Trn_SSCTransLine transLine = context.Trn_SSCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }


        public IList<GenericItemModel> SScWebServiceResult(IList<GenericItemModel> itemModel, List<Dictionary<string, string>> sscData, List<ResponseModel> responseModel)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < sscData.Count; i++)
                {
                    int lineId = Convert.ToInt32(sscData[i][SCSapMessageFields.LineId]);
                    Trn_SSCTransLine transLine = context.Trn_SSCTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i].LineId = lineId;
                    itemModel[i].Status = statusCode;
                    if (statusCode == "ERROR")
                    {
                        itemModel[i].StatusCode = BatchStatus.ERROR;
                    }
                    else
                    {
                        itemModel[i].StatusCode = BatchStatus.ERROR;
                    }
                    itemModel[i].ResponseMessage = new List<string>();
                    for (int j = 0; j < responseModel.Count; j++)
                    {
                        itemModel[i].ResponseMessage.Add(responseModel[j].Message);
                    }

                }
                return itemModel;
            }
        }

        public IList<GenericItemModel> SoTestResult(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }
        public IList<GenericItemModel> SoTestResultGoodyear(IList<GenericItemModel> itemModel, List<int> trnsLineItems)
        {
            using (var context = new GoodpackEDIEntities())
            {
                for (int i = 0; i < trnsLineItems.Count; i++)
                {
                    int lineId = trnsLineItems[i];
                    Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => x.Id == lineId).FirstOrDefault();
                    int statusId = transLine.StatusId;
                    string statusCode = (from s in context.Trn_LineStatus
                                         where s.Id == statusId
                                         select s.StatusCode).FirstOrDefault();

                    itemModel[i + 1].LineId = lineId;
                    itemModel[i + 1].Status = statusCode;

                }
                return itemModel;
            }
        }

        public void UpdateSAPTranLineContent(int batchId, LineStatus? statusCode, int lineId, List<ResponseModel> responseModel, bool isTermTrip)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusVal = statusCode.ToString();
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusVal
                                    select s.Id).FirstOrDefault();
                //string successVal = LineStatus.IN_SAP.ToString();
                //int lineSuccessId = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == successVal
                //                     select s.Id).FirstOrDefault();
                IList<Trn_SCTransLineContent> lineContents = (from s in context.Trn_SCTransLineContent
                                                              where ((s.BatchID == batchId) && (s.LineID == lineId))
                                                              select s).ToList();
                if (responseModel.Count != 0)
                {
                    foreach (var item in lineContents)
                    {
                        context.Trn_SCTransLineContent.Remove(item);
                        context.SaveChanges();
                        log.Info("Removed all the lineContent with item:" + item.Id);
                    }
                    lineStatusId = isTermTrip && context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId
                                                    ? context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault() : lineStatusId;
                    foreach (var items in responseModel)
                    {
                        Trn_SCTransLineContent lineContentData = new Trn_SCTransLineContent();
                        lineContentData.BatchID = batchId;
                        lineContentData.LineID = lineId;
                        lineContentData.SapResponseMessage = isTermTrip && (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineStatusId)
                                                                ? "Waiting for SAP response " : items.Message;
                        lineContentData.StatusID = lineStatusId;
                        context.Trn_SCTransLineContent.Add(lineContentData);
                        context.SaveChanges();
                        log.Info("Inserted all the lineContent of lineId:" + lineId);
                    }
                }
            }
        }

        public void UpdateSAPSSCTranLineContent(int batchId, LineStatus? statusCode, int lineId, List<ResponseModel> responseModel)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusVal = statusCode.ToString();
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusVal
                                    select s.Id).FirstOrDefault();
                //string successVal = LineStatus.IN_SAP.ToString();
                //int lineSuccessId = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == successVal
                //                     select s.Id).FirstOrDefault();
                IList<Trn_SSCTransLineContent> lineContents = (from s in context.Trn_SSCTransLineContent
                                                               where ((s.BatchID == batchId) && (s.LineId == lineId))
                                                               select s).ToList();
                if (responseModel.Count != 0)
                {
                    foreach (var item in lineContents)
                    {
                        context.Trn_SSCTransLineContent.Remove(item);
                        context.SaveChanges();
                        log.Info("Removed all the lineContent with item:" + item.Id);
                    }
                    foreach (var items in responseModel)
                    {
                        Trn_SSCTransLineContent lineContentData = new Trn_SSCTransLineContent();
                        lineContentData.BatchID = batchId;
                        lineContentData.LineId = lineId;
                        lineContentData.SapResponseMessage = items.Message;
                        lineContentData.StatusId = lineStatusId;
                        context.Trn_SSCTransLineContent.Add(lineContentData);
                        context.SaveChanges();
                        log.Info("Inserted all the lineContent of lineId:" + lineId);
                    }
                }
            }
        }
        public void UpdateSAPTransLine(LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {

                string statusValue = statusCode.ToString();
                int lineSuccessId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusValue
                                     select s.Id).FirstOrDefault();
                IList<Trn_SCTransLine> lineContents = (from s in context.Trn_SCTransLine
                                                       where (s.Id == lineId)
                                                       select s).ToList();
                ITirmTripManager termtrip = new TirmTripManager();

                foreach (var item in lineContents)
                {
                    bool? boolValue = null;
                    if (termtrip.IsTirmTripORNot(item.CustomerCode))
                    {
                        boolValue = false;
                        lineSuccessId = context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault();
                    }
                    item.TermTripSAP = boolValue;
                    item.StatusId = lineSuccessId;
                    item.InTestMode = status;
                    context.SaveChanges();
                }
            }
        }
        public void UpdateSAPTransLine(LineStatus? statusCode, int lineId, bool status, List<ResponseModel> response)
        {
            using (var context = new GoodpackEDIEntities())
            {

                string statusValue = statusCode.ToString();
                int lineSuccessId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusValue
                                     select s.Id).FirstOrDefault();
                IList<Trn_SCTransLine> lineContents = (from s in context.Trn_SCTransLine
                                                       where (s.Id == lineId)
                                                       select s).ToList();
                ITirmTripManager termtrip = new TirmTripManager();

                foreach (var item in lineContents)
                {
                    bool? boolValue = null;
                    if (termtrip.IsTirmTripORNot(item.CustomerCode))
                    {
                        boolValue = false;
                        if (context.Trn_LineStatus.Where(x => x.StatusCode == "ERROR").Select(x => x.Id).SingleOrDefault() != lineSuccessId)
                        {
                            lineSuccessId = context.Trn_LineStatus.Where(x => x.StatusCode == "WAITING").Select(x => x.Id).SingleOrDefault();
                        }
                    }
                    item.TermTripSAP = boolValue;
                    item.StatusId = lineSuccessId;
                    item.InTestMode = status;
                    item.ItrNumber = (response[0].ResponseItems == null) ? item.ItrNumber : response[0].ResponseItems.ToString();
                    context.SaveChanges();
                }
            }
        }
        public void UpdateSAPASNTranLineContent(int batchId, LineStatus? statusCode, int lineId, List<ResponseModel> responseModel)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusVal = statusCode.ToString();
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusVal
                                    select s.Id).FirstOrDefault();
                //string successVal = LineStatus.IN_SAP.ToString();
                //int lineSuccessId = (from s in context.Trn_LineStatus
                //                     where s.StatusCode == successVal
                //                     select s.Id).FirstOrDefault();
                IList<Trn_ASNTransLineContent> lineContents = (from s in context.Trn_ASNTransLineContent
                                                               where ((s.BatchID == batchId) && (s.LineID == lineId))
                                                               select s).ToList();
                if (responseModel.Count != 0)
                {
                    foreach (var item in lineContents)
                    {
                        context.Trn_ASNTransLineContent.Remove(item);
                        context.SaveChanges();
                        log.Info("Removed all the lineContent with item:" + item.Id);
                    }
                    foreach (var items in responseModel)
                    {
                        Trn_ASNTransLineContent lineContentData = new Trn_ASNTransLineContent();
                        lineContentData.BatchID = batchId;
                        lineContentData.LineID = lineId;
                        lineContentData.SapResponseMessage = items.Message;
                        lineContentData.StatusID = lineStatusId;
                        context.Trn_ASNTransLineContent.Add(lineContentData);
                        context.SaveChanges();
                        log.Info("Inserted all the lineContent of lineId:" + lineId);
                    }
                }
            }
        }
        public void UpdateSAPASNTransLine(LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {

                string statusValue = statusCode.ToString();
                int lineSuccessId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusValue
                                     select s.Id).FirstOrDefault();
                IList<Trn_ASNTransLine> lineContents = (from s in context.Trn_ASNTransLine
                                                        where (s.Id == lineId)
                                                        select s).ToList();
                foreach (var item in lineContents)
                {
                    item.StatusId = lineSuccessId;
                    item.InTestMode = status;
                    context.SaveChanges();
                }

            }
        }


        public void UpdateSSCTransLine(int batchId, int subsiId, LineStatus? statusCode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCodeVal = statusCode.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                string checkstatusValue = LineStatus.NEW.ToString();
                int checkStatusID = (from s in context.Trn_LineStatus
                                     where s.StatusCode == checkstatusValue
                                     select s.Id).FirstOrDefault();

                var transLine = (context.Trn_SSCTransLine.Where(x => (x.BatchId == batchId) && (x.StatusId == checkStatusID) && (x.SubsiId == subsiId))).ToList();

                foreach (var item in transLine)
                {
                    if (item.StatusId != 0)
                        item.StatusId = statusID;
                    item.SubsiId = subsiId;
                    item.BatchId = batchId;
                    context.SaveChanges();
                }
            }
        }


        public void UpdateSAPSSCTransLine(LineStatus? statusCode, int lineId, bool status)
        {
            using (var context = new GoodpackEDIEntities())
            {

                string statusValue = statusCode.ToString();
                int lineSuccessId = (from s in context.Trn_LineStatus
                                     where s.StatusCode == statusValue
                                     select s.Id).FirstOrDefault();
                IList<Trn_SSCTransLine> lineContents = (from s in context.Trn_SSCTransLine
                                                        where (s.Id == lineId)
                                                        select s).ToList();
                foreach (var item in lineContents)
                {
                    item.StatusId = lineSuccessId;
                    item.InTestMode = status;
                    context.SaveChanges();
                }

            }
        }

        public void UpdateSAPTransBatch(int batchId, BatchStatus? statusCode, bool IsTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusVal = LineStatus.ERROR.ToString();
                int lineStatusId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusVal
                                    select s.Id).FirstOrDefault();
                string successVal = statusCode.ToString();
                int batchSuccessId = (from s in context.Ref_BatchStatus
                                      where s.StatusCode == successVal
                                      select s.Id).FirstOrDefault();
                Trn_SCTransLine trnLine = (from s in context.Trn_SCTransLine
                                           where ((s.BatchId == batchId) && (s.StatusId == lineStatusId))
                                           select s).FirstOrDefault();

                Trn_SCTransBatch transBatch = context.Trn_SCTransBatch.Where(x => x.Id == batchId).FirstOrDefault();
                if (trnLine == null)
                {
                    transBatch.StatusId = batchSuccessId;
                    transBatch.IsTestMode = IsTestMode;
                    context.SaveChanges();
                }

            }
        }
        public int UpdateSOTransLineForSOChangeCancelData(Dictionary<string, string> soDataVal, SCTransactionModel model, string[] messageData)
        {
            using (var context = new GoodpackEDIEntities())
            {
                int i = 1;
                string statusCodeVal = LineStatus.IN_SAP.ToString();
                int statusID = (from s in context.Trn_LineStatus
                                where s.StatusCode == statusCodeVal
                                select s.Id).FirstOrDefault();

                Trn_SOTransLine transLine = new Trn_SOTransLine();
                transLine.BatchId = model.BatchID; ;
                transLine.LastUpdated = DateTime.Now;
                transLine.LineContent = messageData[i].ToString();
                transLine.LineNumber = i;
                //transLine.SourceReference = lineReference.SourceReferenceCode;
                //transLine.GPReference = gpReference;
                //transLine.EDIReference = lineReference.EdiNumber;
                transLine.StatusId = statusID;
                transLine.SubsiId = model.SubsiId;
                transLine.IsActive = true;
                transLine.InTestMode = false;
                context.Trn_SOTransLine.Add(transLine);
                context.SaveChanges();
                int trLineID = transLine.Id;
                transLine = context.Trn_SOTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                if (transLine != null)
                {

                    // transLine.SalesOrganisation = soDataVal[SOSapMessageFields.SalesOrganization];
                    transLine.DistributionChannel = "00";
                    transLine.Division = "00";
                    // transLine.SalesOffice = soDataVal[SOSapMessageFields.SalesOffice];
                    transLine.CustomerPONumber = soDataVal[SOChangeCancelSAPFields.CustomerPONumber];
                    // transLine.CustomerPODate = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    // transLine.CustomerPODate = custPODate;
                    transLine.CustomerNumber = soDataVal[SOChangeCancelSAPFields.Customer];
                    transLine.ReqDeliveryDate = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    // transLine.ItemNumber = soDataVal[SOSapMessageFields.SOItemnumber];
                    transLine.MaterialNumber = soDataVal[SOChangeCancelSAPFields.Material];
                    transLine.FromLocation = soDataVal[SOChangeCancelSAPFields.Packer];
                    // transLine.ScheduleLineDate = scDataVal[SOSapMessageFields.Orderdate];
                    transLine.OrderDate = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    transLine.DeliveryDate = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    transLine.SOQuantity = soDataVal[SOChangeCancelSAPFields.OrderQuantity];
                    transLine.ETA = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    transLine.ETD = soDataVal[SOChangeCancelSAPFields.Requestdeliverydate];
                    transLine.ReasonETD = soDataVal[SOChangeCancelSAPFields.ReasonforETDchange];
                    transLine.ReasonSKU = soDataVal[SOChangeCancelSAPFields.ReasonforSKUchange];
                    transLine.ReasonQuantity = soDataVal[SOChangeCancelSAPFields.ReasonforQuantityChange];
                    transLine.ReasonCancel = soDataVal[SOChangeCancelSAPFields.ReasonforCancellation];
                    // transLine.POD = soDataVal[SOSapMessageFields.POD];
                    // transLine.POL = soDataVal[SOSapMessageFields.POL];
                    // transLine.Consignee = soDataVal[SOSapMessageFields.Consignee];
                    // transLine.VesselName = soDataVal[SOSapMessageFields.Vesselname];
                    // transLine.VoyageReference = soDataVal[SOSapMessageFields.Voyagereference];
                    // transLine.ShippingLine = soDataVal[SOSapMessageFields.Shippingline];
                    // transLine.Remarks = soDataVal[SOSapMessageFields.ItemRemarks];
                    // transLine.DeliveryBlockOrder = soDataVal[SOSapMessageFields.DeliveryBlockOrders];                  
                    context.SaveChanges();

                }
                i++;
                return trLineID;

            }

        }
        public void UpdateTransLineForData(int trLineID, Dictionary<string, string> scDataVal)
        {
            try
            {
                using (var context = new GoodpackEDIEntities())
                {

                    Trn_SCTransLine transLine = context.Trn_SCTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                    if (transLine != null)
                    {

                        transLine.CustomerCode = scDataVal[SCSapMessageFields.CustomerNumber];
                        string dateIssue = scDataVal[SCSapMessageFields.ETDDate];
                        if ((dateIssue != "") || (dateIssue != null))
                        {
                            string dateVal = dateIssue.Substring(0, 2);
                            string monthVal = dateIssue.Substring(2, 2);
                            string yearVal = dateIssue.Substring(4, 4);
                            transLine.ETD = yearVal + "-" + monthVal + "-" + dateVal;
                        }

                        string dateIssued = scDataVal[SCSapMessageFields.DTADate];
                        if ((dateIssued != "") || (dateIssued != null))
                        {
                            string dateVal = dateIssued.Substring(0, 2);
                            string monthVal = dateIssued.Substring(2, 2);
                            string yearVal = dateIssued.Substring(4, 4);
                            transLine.ETA = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                        }

                        if (scDataVal[SCSapMessageFields.FromLocation].Length != 10)
                        {
                            transLine.FromLocation = "0000" + scDataVal[SCSapMessageFields.FromLocation];
                        }
                        else
                        {
                            transLine.FromLocation = scDataVal[SCSapMessageFields.FromLocation];
                        }
                        transLine.LastUpdated = DateTime.Now;
                        transLine.Remarks = scDataVal[SCSapMessageFields.Remarks];
                        transLine.SINumber = scDataVal[SCSapMessageFields.SINumber];
                        if (scDataVal[SCSapMessageFields.Quantity].Length != 0)
                        {
                            int s;
                            int.TryParse(scDataVal[SCSapMessageFields.Quantity], out s);
                            if (s == 0)
                                throw new InvalidCastException("Quantity should be integer");
                            transLine.Quantity = Convert.ToInt32(scDataVal[SCSapMessageFields.Quantity]);

                        }
                        else
                        {
                            transLine.Quantity = 0;
                        }
                        if (scDataVal[SCSapMessageFields.ToLocation].Length != 10)
                        {
                            transLine.ToLocation = "0000" + scDataVal[SCSapMessageFields.ToLocation];
                        }
                        else
                        {
                            transLine.ToLocation = scDataVal[SCSapMessageFields.ToLocation];
                        }
                        transLine.BinType = scDataVal[SCSapMessageFields.MaterialNumber];
                        transLine.CustomerRefNumber = scDataVal[SCSapMessageFields.CustomerReferenceNumber];
                        transLine.ItrType = scDataVal[SCSapMessageFields.DocumentType];
                        transLine.ItrNumber = scDataVal[SCSapMessageFields.ITRNumber];
                        transLine.RefItrNumber = scDataVal[SCSapMessageFields.ReferenceITRNumber];
                        transLine.ItemNumber = scDataVal[SCSapMessageFields.ItemNo];
                        transLine.ContainerNumber = scDataVal[SCSapMessageFields.ContainerNumber];
                        transLine.SalesDocument = scDataVal[SCSapMessageFields.SalesDocument];
                        if (transLine.SalesDocument.Length != 0)
                        {
                            transLine.SalesDocumentItem = ConstantUtilities.SalesDocumentItem;
                        }
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("UpdateTransLineForData-" + e);
                throw e;
            }
        }
        public void UpdateASNTransLineForData(int trLineID, Dictionary<string, string> scDataVal)
        {
            using (var context = new GoodpackEDIEntities())
            {

                Trn_ASNTransLine transLine = context.Trn_ASNTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                if (transLine != null)
                {

                    transLine.CustomerCode = scDataVal[SCSapMessageFields.CustomerNumber];
                    string dateIssue = scDataVal[SCSapMessageFields.ETDDate];
                    if ((dateIssue != "") || (dateIssue != null))
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        transLine.ETD = yearVal + "-" + monthVal + "-" + dateVal;
                    }

                    string dateIssued = scDataVal[SCSapMessageFields.DTADate];
                    if ((dateIssued != "") || (dateIssued != null))
                    {
                        string dateVal = dateIssued.Substring(0, 2);
                        string monthVal = dateIssued.Substring(2, 2);
                        string yearVal = dateIssued.Substring(4, 4);
                        transLine.ETA = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }

                    if (scDataVal[SCSapMessageFields.FromLocation].Length != 10)
                    {
                        transLine.FromLocation = "0000" + scDataVal[SCSapMessageFields.FromLocation];
                    }
                    else
                    {
                        transLine.FromLocation = scDataVal[SCSapMessageFields.FromLocation];
                    }
                    transLine.LastUpdated = DateTime.Now;
                    transLine.Remarks = scDataVal[SCSapMessageFields.Remarks];
                    transLine.SINumber = scDataVal[SCSapMessageFields.SINumber];
                    if (scDataVal[SCSapMessageFields.Quantity].Length != 0)
                    {
                        transLine.Quantity = Convert.ToInt32(scDataVal[SCSapMessageFields.Quantity]);
                    }
                    else
                    {
                        transLine.Quantity = 0;
                    }
                    if (scDataVal[SCSapMessageFields.ToLocation].Length != 10)
                    {
                        transLine.ToLocation = "0000" + scDataVal[SCSapMessageFields.ToLocation];
                    }
                    else
                    {
                        transLine.ToLocation = scDataVal[SCSapMessageFields.ToLocation];
                    }
                    transLine.BinType = scDataVal[SCSapMessageFields.MaterialNumber];
                    transLine.CustomerRefNumber = scDataVal[SCSapMessageFields.CustomerReferenceNumber];
                    transLine.ItrType = scDataVal[SCSapMessageFields.DocumentType];
                    transLine.ItrNumber = scDataVal[SCSapMessageFields.ITRNumber];
                    transLine.RefItrNumber = scDataVal[SCSapMessageFields.ReferenceITRNumber];
                    transLine.ItemNumber = scDataVal[SCSapMessageFields.ItemNo];
                    transLine.ContainerNumber = scDataVal[SCSapMessageFields.ContainerNumber];
                    transLine.SalesDocument = scDataVal[SCSapMessageFields.SalesDocument];
                    if (transLine.SalesDocument.Length != 0)
                    {
                        transLine.SalesDocumentItem = ConstantUtilities.SalesDocumentItem;
                    }
                    context.SaveChanges();
                }
            }
        }
        public void UpdateSOTransLineForData(int trLineID, Dictionary<string, string> scDataVal, string custPODate)
        {
            using (var context = new GoodpackEDIEntities())
            {

                Trn_SOTransLine transLine = context.Trn_SOTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                if (transLine != null)
                {

                    transLine.SalesOrganisation = scDataVal[SOSapMessageFields.SalesOrganization];
                    transLine.DistributionChannel = scDataVal[SOSapMessageFields.DistributionChannel];
                    transLine.Division = scDataVal[SOSapMessageFields.Division];
                    transLine.SalesOffice = scDataVal[SOSapMessageFields.SalesOffice];
                    transLine.CustomerPONumber = scDataVal[SOSapMessageFields.CustPOnumber];
                    // transLine.CustomerPODate = scDataVal[SOSapMessageFields.CustomerPOdate];
                    transLine.CustomerPODate = custPODate;
                    transLine.CustomerNumber = scDataVal[SOSapMessageFields.Customer];
                    transLine.ReqDeliveryDate = scDataVal[SOSapMessageFields.Requestdeliverydate];
                    transLine.ItemNumber = scDataVal[SOSapMessageFields.SOItemnumber];
                    transLine.MaterialNumber = scDataVal[SOSapMessageFields.Material];
                    transLine.FromLocation = scDataVal[SOSapMessageFields.Packer];
                    // transLine.ScheduleLineDate = scDataVal[SOSapMessageFields.Orderdate];
                    transLine.OrderDate = scDataVal[SOSapMessageFields.Orderdate];
                    transLine.DeliveryDate = scDataVal[SOSapMessageFields.DeliveryDate];
                    transLine.SOQuantity = scDataVal[SOSapMessageFields.Orderquantity];
                    transLine.ETA = scDataVal[SOSapMessageFields.VesselETA];
                    transLine.ETD = scDataVal[SOSapMessageFields.VesselETD];
                    transLine.POD = scDataVal[SOSapMessageFields.POD];
                    transLine.POL = scDataVal[SOSapMessageFields.POL];
                    transLine.Consignee = scDataVal[SOSapMessageFields.Consignee];
                    transLine.VesselName = scDataVal[SOSapMessageFields.Vesselname];
                    transLine.VoyageReference = scDataVal[SOSapMessageFields.Voyagereference];
                    transLine.ShippingLine = scDataVal[SOSapMessageFields.Shippingline];
                    transLine.Remarks = scDataVal[SOSapMessageFields.ItemRemarks];
                    transLine.DeliveryBlockOrder = scDataVal[SOSapMessageFields.DeliveryBlockOrders];
                    context.SaveChanges();
                }
            }
        }

        public void UpdateSSCTransLineForData(int trLineID, Dictionary<string, string> scDataVal)
        {
            using (var context = new GoodpackEDIEntities())
            {

                Trn_SSCTransLine transLine = context.Trn_SSCTransLine.Where(x => (x.Id == trLineID)).FirstOrDefault();

                if (transLine != null)
                {
                    transLine.BatchNumber = scDataVal[SCSapMessageFields.BatchNumber];
                    transLine.CustomerCode = scDataVal[SCSapMessageFields.CustomerCode];
                    string dateIssue = scDataVal[SCSapMessageFields.ETDDate];
                    if ((dateIssue != "") || (dateIssue != null))
                    {
                        string dateVal = dateIssue.Substring(0, 2);
                        string monthVal = dateIssue.Substring(2, 2);
                        string yearVal = dateIssue.Substring(4, 4);
                        transLine.ETDDate = yearVal + "-" + monthVal + "-" + dateVal;
                    }

                    string dateIssued = scDataVal[SCSapMessageFields.ETADate];
                    if ((dateIssued != "") || (dateIssued != null))
                    {
                        string dateVal = dateIssued.Substring(0, 2);
                        string monthVal = dateIssued.Substring(2, 2);
                        string yearVal = dateIssued.Substring(4, 4);
                        transLine.ETADate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                    string scanDate = scDataVal[SCSapMessageFields.ScanDate];
                    if ((scanDate != "") || (scanDate != null))
                    {
                        string dateVal = scanDate.Substring(0, 2);
                        string monthVal = scanDate.Substring(2, 2);
                        string yearVal = scanDate.Substring(4, 4);
                        transLine.ScanDate = yearVal + "-" + monthVal + "-" + dateVal;//DateTime.ParseExact(dateIssue, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString();
                    }
                    transLine.MvtType = scDataVal[SCSapMessageFields.MvtType];

                    if (scDataVal[SCSapMessageFields.FRMLocation].Length != 10)
                    {
                        transLine.FromLocation = "0000" + scDataVal[SCSapMessageFields.FRMLocation];
                    }
                    else
                    {
                        transLine.FromLocation = scDataVal[SCSapMessageFields.FRMLocation];
                    }
                    transLine.LastUpdated = DateTime.Now;

                    if (scDataVal[SCSapMessageFields.TOLocation].Length != 10)
                    {
                        transLine.ToLocation = "0000" + scDataVal[SCSapMessageFields.TOLocation];
                    }
                    else
                    {
                        transLine.ToLocation = scDataVal[SCSapMessageFields.TOLocation];
                    }

                    transLine.MaterialNumber = scDataVal[SCSapMessageFields.MaterialNumber];
                    transLine.CustomerReferenceNumber = scDataVal[SCSapMessageFields.CustomerReferenceNumber];
                    transLine.MaterialNumber = scDataVal[SCSapMessageFields.MaterialNumber];
                    transLine.TwoDPinNo = scDataVal[SCSapMessageFields.TwoDPin];
                    context.SaveChanges();
                }
            }
        }

        public void UpdateSSCTransBatch(int batchId, int successCount, int errorCount, bool InTestMode)
        {
            using (var context = new GoodpackEDIEntities())
            {
                string statusCode = LineStatus.ERROR.ToString();
                int statusCodeId = (from s in context.Trn_LineStatus
                                    where s.StatusCode == statusCode
                                    select s.Id).FirstOrDefault();
                int transLine = (from s in context.Trn_SSCTransLine
                                 where ((s.BatchId == batchId) && (s.StatusId == statusCodeId))
                                 select s.Id).FirstOrDefault();
                string batchStatusVal = null;


                if (transLine == 0)
                {
                    batchStatusVal = BatchStatus.CLOSED.ToString();
                }
                else
                {
                    batchStatusVal = BatchStatus.ERROR.ToString();
                }
                log.Info("Updating Trn_SCTransBatch table with batch status:" + batchStatusVal);
                int batchStatusId = (from s in context.Ref_BatchStatus
                                     where s.StatusCode == batchStatusVal
                                     select s.Id).FirstOrDefault();
                Trn_SCTransBatch scDataValue = context.Trn_SCTransBatch.FirstOrDefault(x => x.Id == batchId);
                scDataValue.StatusId = batchStatusId;
                scDataValue.IsTestMode = InTestMode;
                scDataValue.RecordCountSuccess = scDataValue.RecordCountSuccess + successCount;
                scDataValue.RecordCountSAPError = scDataValue.RecordCountTotal - scDataValue.RecordCountSuccess ?? 0;
                context.SaveChanges();
            }
        }
    }
}
