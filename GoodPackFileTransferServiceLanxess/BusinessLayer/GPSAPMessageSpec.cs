﻿using GPArchitecture.CommonTypes;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackFileTransferServiceLanxess.BusinessLayer
{
    public class GPSAPMessageSpec
    {
        private static List<RefTransactionTypeModel> objLstTargetMessage = null;
        private static readonly ILog log = LogManager.GetLogger(typeof(GPSAPMessageSpec));
        public static RefTransactionTypeModel GetTargetMessage(TRANSACTION_TYPES objTransactionType, TargetMessageFormat objTargetMessageFormat)
        {
            objLstTargetMessage = null;
            if (objLstTargetMessage == null)
            {
                Initialize();
            }
            IList<RefTransactionTypeModel> objlstTargetMsgSpecs = null;
            lock (objLstTargetMessage)
            {
                var listHeader = from header in objLstTargetMessage
                                 where header.TransactionType == objTransactionType && header.Format == objTargetMessageFormat.ToString()
                                 select header;

                objlstTargetMsgSpecs = listHeader.ToList<RefTransactionTypeModel>();
            }
            return objlstTargetMsgSpecs != null ? objlstTargetMsgSpecs[0] : null;
        }

        public static void Initialize()
        {
            IList<SCTransMappingModel> mapperModel;
            try
            {
                if (objLstTargetMessage == null)
                {

                    using (var context = new GoodpackEDIEntities())
                    {

                        mapperModel = (from s in context.Ref_SAPMessageFields_Lanxess
                                       join p in context.Ref_TransactionTypes
                                       on s.TransactionId equals p.Id
                                       select new SCTransMappingModel
                                       {
                                           DefaultValue = s.DefaultValue,
                                           FieldSequence = s.FieldSequence,
                                           FieldType = s.FieldType,
                                           Format = p.Format,
                                           Id = p.Id,
                                           InputType = s.InputType,
                                           IsMandatory = s.IsMandatory,
                                           MaxLength = s.MaxLength ?? 0,
                                           Note = s.Note,
                                           Operation = s.Operation,
                                           OperationParam = s.OperationParam,
                                           SAPFieldName = s.SAPFieldName,
                                           SkipOnValidationFailure = s.SkipOnValidationFailure,
                                           TransactionCode = p.TransactionCode,
                                           TransactionId = s.TransactionId,
                                           TransactionName = p.TransactionName,
                                           ValidationMessage = s.ValidationMessage

                                       }).ToList();


                    }

                    objLstTargetMessage = new List<RefTransactionTypeModel>();


                    int intTargetMessageID = 0;
                    int intTargetMessageFieldID = 0;
                    SAPMessageFieldsModel objTargetMessageField;
                    RefTransactionTypeModel objTargetMessage = null;
                    bool blnTmp = false;
                    foreach (var drTargetMessage in mapperModel)
                    {

                        if (int.Parse(drTargetMessage.TransactionId.ToString()) != intTargetMessageID || objTargetMessage == null)
                        {
                            blnTmp = true;
                            objTargetMessage = new RefTransactionTypeModel();
                            objTargetMessage.ID = drTargetMessage.TransactionId;
                            objTargetMessage.TargetMessageName = drTargetMessage.TransactionName;
                            objTargetMessage.Format = drTargetMessage.Format;
                            objTargetMessage.TransactionType = EnumConverTo.TranType(drTargetMessage.TransactionCode);

                            // Set target message ID holder value
                            intTargetMessageID = objTargetMessage.ID;
                        }

                        // Get field details
                        objTargetMessageField = new SAPMessageFieldsModel();
                        intTargetMessageFieldID = drTargetMessage.Id;
                        objTargetMessageField.ID = intTargetMessageFieldID;
                        objTargetMessageField.FieldName = drTargetMessage.SAPFieldName;
                        objTargetMessageField.FieldSeq = drTargetMessage.FieldSequence;
                        objTargetMessageField.FieldType = (TargetMessageFieldTypes)EnumConverTo.GenericEnumConverter(typeof(TargetMessageFieldTypes), drTargetMessage.FieldType);
                        objTargetMessageField.InputType = EnumConverTo.TargetMessageInputs(drTargetMessage.InputType);
                        objTargetMessageField.DefaultValue = drTargetMessage.DefaultValue;
                        if (!string.IsNullOrEmpty(drTargetMessage.Operation))
                            objTargetMessageField.OperationName = drTargetMessage.Operation;
                        if (!string.IsNullOrEmpty(drTargetMessage.OperationParam))
                            objTargetMessageField.OperationParam = drTargetMessage.OperationParam;
                        if (!string.IsNullOrEmpty(drTargetMessage.MaxLength.ToString()))
                            objTargetMessageField.MaxLength = drTargetMessage.MaxLength;
                        objTargetMessageField.FieldSeq = drTargetMessage.FieldSequence;
                        if (!string.IsNullOrEmpty(drTargetMessage.Note))
                            objTargetMessageField.Note = drTargetMessage.Note;
                        objTargetMessageField.IsMandatory = drTargetMessage.IsMandatory;
                        if (drTargetMessage.ValidationMessage != null)
                            objTargetMessageField.ValidationError = drTargetMessage.ValidationMessage;
                        if (drTargetMessage.SkipOnValidationFailure != null)
                            objTargetMessageField.SkipOnValidationFailure = drTargetMessage.SkipOnValidationFailure;

                        // Add field to message spec
                        objTargetMessage.Fields.Add(objTargetMessageField.FieldSeq, objTargetMessageField);

                        // If message spec does not exist in spec list, add
                        if (!objLstTargetMessage.Contains(objTargetMessage))
                        {
                            objLstTargetMessage.Add(objTargetMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(" Initialize in GPSAPMessageSpec ." + "" + ". Api Exception : Message- " + ex.Message);
            }
        }
    }
}
