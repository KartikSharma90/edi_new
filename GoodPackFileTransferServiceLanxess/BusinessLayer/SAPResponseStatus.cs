﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPArchitecture
{
    public class SAPResponseStatus
    {
        public static SAPResponseStates ConvertSAPResponseCodeToStateCode(string sapStatusCode)
        {
            SAPResponseStates status = SAPResponseStates.INFORMATION;
            switch (sapStatusCode)
            {
                case "I":
                    status = SAPResponseStates.INFORMATION;
                    break;
                case "W":
                    status = SAPResponseStates.SUCCESS; // CR 2013-007 - Warnings from SAP are to be translated to success
                    break;
                case "S":
                    status = SAPResponseStates.SUCCESS;
                    break;
                case "E":
                    status = SAPResponseStates.ERROR;
                    break;
                case "R":
                    status = SAPResponseStates.ERROR;
                    break;
                default:
                    throw new System.Exception("Unsupported SAP Status Code received: " + sapStatusCode);

            }

            return status;
        }
    }

    public enum SAPResponseStates
    {
        INFORMATION=1
        , WARNING= 2
        , SUCCESS = 3
        , ERROR =4
        ,REJECT=5
    }
}
