﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodPackFileTransferServiceLanxess.BusinessLayer.SSCManagementLayer
{
    public class MsgSapSSCLine
    {

        // Field list
        StringBuilder message;

        // Fields
        public string BatchNumber;
        string MvtType;
        string FromLocation;
        string ToLocation;

        public string CustomerReferenceNumber;
        string CustomerPONumber;
        public string ITRNumber;
        public string MaterialNumber;
        string Quantity;
        string BaseUOM;
        string ContainerNumber;
        string ETADate;
        string ETDDate;
        string CustomerName;
        string ScanDate;
        string Status;
        public string CustomerCode;
        string CreatedDate;
        string EntryTime;
        private string psscLineContent;
        string logisticsServiceProvider;
        string lanxessDONumber;
        string boxNumber;
        string incrementalNumber;
        string movementdirection;
        string LXSstocktransferPONo;
        string Remarks;

        public MsgSapSSCLine(IList<string> fieldListSrc)
        {
            if (fieldListSrc.Count < 19)
            {
                throw new Exception("The SAP Scanned SC line does not have enough fields.");
            }

            BatchNumber = fieldListSrc[0];
            MvtType = fieldListSrc[1];
            FromLocation = fieldListSrc[2];
            ToLocation = fieldListSrc[3];
            TwoDPin = fieldListSrc[4];
            CustomerReferenceNumber = fieldListSrc[5];
            CustomerPONumber = fieldListSrc[6];
            ITRNumber = fieldListSrc[7];
            MaterialNumber = fieldListSrc[8];
            Quantity = fieldListSrc[9];
            BaseUOM = fieldListSrc[10];
            ContainerNumber = fieldListSrc[11];
            ETADate = fieldListSrc[12];
            ETDDate = fieldListSrc[13];
            CustomerName = fieldListSrc[14];
            ScanDate = fieldListSrc[15];
            Status = fieldListSrc[16];
            CustomerCode = fieldListSrc[17];
            CreatedDate = fieldListSrc[18];
            EntryTime = fieldListSrc[19];
            logisticsServiceProvider = fieldListSrc[20];
            lanxessDONumber = fieldListSrc[21];
            boxNumber = fieldListSrc[22];
            incrementalNumber = fieldListSrc[23];
            movementdirection = fieldListSrc[24];
            LXSstocktransferPONo = fieldListSrc[25];
            Remarks = fieldListSrc[26];
        }

        public long getPackerCode()
        {
            if (FromLocation == "" || FromLocation == null)
            { return -1; }
            else
                return long.Parse(FromLocation);
        }

        public void GenerateSAPMessage()
        {
            message = new StringBuilder();
            message.Append(BatchNumber).Append("\t");
            message.Append(MvtType).Append("\t");
            message.Append(FromLocation).Append("\t");
            message.Append(ToLocation).Append("\t");
            message.Append(TwoDPin).Append("\t");
            message.Append(CustomerReferenceNumber).Append("\t");
            message.Append(CustomerPONumber).Append("\t");
            message.Append(ITRNumber).Append("\t");
            message.Append(MaterialNumber).Append("\t");
            message.Append(Quantity).Append("\t");
            message.Append(BaseUOM).Append("\t");
            message.Append(ContainerNumber).Append("\t");
            message.Append(ETADate).Append("\t");
            message.Append(ETDDate).Append("\t");
            message.Append(CustomerName).Append("\t");
            message.Append(ScanDate).Append("\t");
            message.Append(Status).Append("\t");
            message.Append(CustomerCode).Append("\t");
            message.Append(CreatedDate);
            message.Append(EntryTime).Append("\t");
            message.Append(logisticsServiceProvider).Append("\t");
            message.Append(lanxessDONumber).Append("\t");
            message.Append(boxNumber).Append("\t");
            message.Append(incrementalNumber).Append("\t");
            message.Append(movementdirection).Append("\t");
            message.Append(LXSstocktransferPONo).Append("\t");
            message.Append(Remarks); 
        }


        public string Message
        {
            get { return this.message.ToString(); }
        }

        public string PSSCLineContent
        {
            get { return this.psscLineContent; }
            set { this.psscLineContent = value; }
        }

        public string TwoDPin { get; set; }
    }
}
