﻿using GPArchitecture.DataLayer.Models;
 using GPArchitecture.EnumsAndConstants;
 using GPArchitecture.GoodPackBusinessLayer.GenericValidationManagementLayer;
 using GPArchitecture.GoodPackBusinessLayer.DecodeValueManagementLayer;
 using GPArchitecture.Models;
 using GPArchitecture.Parsers;
 using log4net;
 using System;
 using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Text;
 using System.Threading.Tasks;
 using System.Text.RegularExpressions;
 
 using GPArchitecture.Utilities;
 using GPArchitecture.GoodPackBusinessLayer.SOManagementLayer;
 using GPArchitecture.SAPService;
 using System.Net.Mail;
 
 namespace GoodPackFileTransferServiceLanxess.BusinessLayer.SOManagementLayer
 {
     public class SOTransactionManager
     {
         private static readonly ILog log = LogManager.GetLogger(typeof(SOTransactionManager));
         private string scPath;
         private SCTransactionModel objGPCustomerMessage;
         private StringBuilder errorDescription = new StringBuilder();
         private IList<GenericItemModel> itemModelData = new List<GenericItemModel>();
         
         public bool SOFileData(string mapperName, int subsiId, int fileType, string path,string type)
         {
             scPath = path;
             bool isValidFile = false;
             string message = "";
             objGPCustomerMessage = new SCTransactionModel();
             FileInfo fileInfo = null;
             if (fileType == 1)
             {
 
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                 objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                 objGPCustomerMessage.BatchFileName = fileInfo.Name;
             }
             else if (fileType == 2)
             {
                 scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
             }
             else if (fileType == 3)
             {
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileName = Path.GetFileName(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
             }
             
             objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
             IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
            
             isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
             string[] data;
             data = SOData(fileInfo, out message);
          
             IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
             return isValidFile;
         }
 
         public IList<GenericItemModel> SOFileData(string mapperName, int subsiId, int fileType, string path)
         {
             log.Debug("Traansaction Manager -SOFileData Begin :-" + path + "Mapper Name :-" + mapperName);
             scPath = path;
             bool isValidFile = false;
             string message = "";
             objGPCustomerMessage = new SCTransactionModel();
             FileInfo fileInfo = null;
             if (fileType == 1)
             {
 
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
                 objGPCustomerMessage.BatchFileSourceAddress = fileInfo.Directory.FullName;
                 objGPCustomerMessage.BatchFileName = fileInfo.Name;
             }
             else if (fileType == 2)
             {
                 scPath = Path.Combine(objGPCustomerMessage.BatchFileSourceAddress, objGPCustomerMessage.BatchFileName);
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
             }
             else if (fileType == 3)
             {
                 fileInfo = new FileInfo(scPath);
                 objGPCustomerMessage.BatchFileName = Path.GetFileName(scPath);
                 objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FTP_SERVICE;
             }
 
             //bool countLimit = CountFileLines();
             //if (countLimit)
             //{
             objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
             IList<GenericItemModel> scDataModel = new List<GenericItemModel>();
             GenericItemModel itemModel;            
             isValidFile = this.Validate_IsFileTypeSupported(objGPCustomerMessage);
             if (isValidFile)
             {
                 string scData;
                 bool isFirstVal = true;
                 GPArchitecture.CommonTypes.SourceMessage sourceMessage = null;
                 sourceMessage = GPTools.LoadSourceMessageSpecs(subsiId, TRANSACTION_TYPES.SO, "");
                 int i = 0;
                 string[] data;               
                 data = SOData(fileInfo, out message);
                
                 //   data = data.ToString().Replace(",", "|");
                 IList<Trn_MapperFieldSpecs> mappedData = ValidateData(mapperName);
                 foreach (var item in data)
                 {
                     if (item.Length != 0)
                     {
                         i++;
                         string val = item.Replace("\"", "").Replace("\"", "");
                         string[] stringData = val.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
                         //char[] arrChar = { ',', '|'};
                         //string[] stringData = val.Split(arrChar, StringSplitOptions.None);
 
                         if (isFirstVal)
                         {
                             isFirstVal = false;
                             if (stringData.Length == mappedData.Count)
                             {
                                 bool isMappedData = false;
                                 for (int j = 0; j < mappedData.Count; j++)
                                 {
                                     if (stringData[j] == mappedData[j].FieldName)
                                     {
                                         isMappedData = true;
                                         break;
                                     }
                                 }
                                 if (isMappedData)
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Id = i;
                                     itemModel.Name = val;
                                     itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                     scDataModel.Add(itemModel);
                                     itemModel = new GenericItemModel();
                                     i++;
                                     itemModel.Id = i;
                                     itemModel.Name = SAPHeaders(mapperName).Replace("\"", "").Replace("\r", "");
                                     scDataModel.Add(itemModel);
                                     ///////////////////////////
                                 }
                                 else
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Status = "false";
                                     itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                     itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                     itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file " + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                     scDataModel.Add(itemModel);
                                     return scDataModel;
                                 }
                             }
                             else
                             {
                                 itemModel = new GenericItemModel();
                                 itemModel.Status = "false";
                                 itemModel.DataValues = message;
                                 itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                 itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                 itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                 itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                 itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file" + objGPCustomerMessage.BatchFileName + " and Mapper:" + mapperName;
                                 scDataModel.Add(itemModel);
                                 return scDataModel;
                             }
                         }
                         else
                         {
                             if (stringData.Length == mappedData.Count)
                             {
                                 int counter = 0;
                                 for (int j = 0; j < stringData.Length; j++)
                                 {
                                     if (stringData[j] == "")
                                     {
                                         counter++;
                                     }
                                 }
                                 if (counter != stringData.Length)
                                 {
                                     itemModel = new GenericItemModel();
                                     itemModel.Id = i;
                                     itemModel.DataValues = message;
                                     itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                     itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                     itemModel.Message = objGPCustomerMessage.Message;
                                     itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                     itemModel.Name = item.Replace("\"", "");
                                     //SAP headers and items in the file
                                     string dataWithDecodeValue = CheckForDecodeValueValidation(item.Replace("\"", ""), scDataModel[1].Name, mapperName, subsiId);
                                     itemModel.DecodeValue = dataWithDecodeValue;
                                     scDataModel.Add(itemModel);
                                 }
                             }
                             else
                             {
                                 itemModel = new GenericItemModel();
                                 itemModel.Status = "false";
                                 itemModel.DataValues = message;
                                 itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                                 itemModel.FileName = objGPCustomerMessage.BatchFileName;
                                 itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                                 itemModel.Message = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file";
                                 itemModel.ErrorMessage = "Wrong input file uploaded for mapper " + mapperName + ".Please check the uploaded file and Mapper";
                                 scDataModel.Add(itemModel);
                                 return scDataModel;
                             }
                         }
                     }
                 }
                 scDataModel[scDataModel.Count - 1].ErrorMessage = errorDescription.ToString();
                 return scDataModel;
             }
             else
             {
                 itemModel = new GenericItemModel();
                 itemModel.Status = "false";
                 itemModel.DataValues = message;
                 itemModel.BatchFileSourceAddress = objGPCustomerMessage.BatchFileSourceAddress;
                 itemModel.FileName = objGPCustomerMessage.BatchFileName;
                 itemModel.BatchId = objGPCustomerMessage.BatchID.ToString();
                 itemModel.Message = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                 itemModel.ErrorMessage = "Wrong input file format for mapper " + mapperName + ".Please check the uploaded file";
                 scDataModel.Add(itemModel);
                 return scDataModel;
             }
         }
         //public IList<GenericItemModel> verifySOData(string mapperFile, string username, string subsiId, string custPOdate, IList<GenericItemModel> itemModel, bool isFileData)
         //{
         //    try
         //    {
         //        objGPCustomerMessage = new SCTransactionModel();
         //        GenericItemModel itemModelVal = itemModel[1];
         //        List<string> dataValue;
         //        List<string> fileName;
         //        List<string> filePath;
         //        List<string> message;
         //        List<string> emailId;
         //        dataValue = itemModel.Select(x => x.DataValues).Distinct().ToList();
         //        fileName = itemModel.Select(x => x.FileName).Distinct().ToList();
         //        filePath = itemModel.Select(x => x.BatchFileSourceAddress).Distinct().ToList();
         //        message = itemModel.Select(x => x.Message).Distinct().ToList();
         //        emailId = itemModel.Select(x => x.EmailId).Distinct().ToList();
         //        objGPCustomerMessage.Message = dataValue[0].ToString();               
         //        objGPCustomerMessage.BatchFileName = fileName[0].ToString();
         //        objGPCustomerMessage.BatchFileSourceAddress = filePath[0].ToString();
         //        objGPCustomerMessage.Message = message[0] == null ? "" : message[0].ToString();
         //        objGPCustomerMessage.EmailId = emailId[0] == null ? "" : emailId[0].ToString();
                 
         //        if (isFileData)
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.FILE_SYSTEM;
         //            itemModel.RemoveAt(1);
         //            using (var context = new GoodpackEDIEntities())
         //            {
         //                objGPCustomerMessage.EmailId = (from s in context.Gen_User
         //                                                where s.Username == username
         //                                                select s.EmailId).FirstOrDefault();
         //            }
         //        }
         //        else
         //        {
         //            objGPCustomerMessage.BatchFileSourceType = SOURCE_TYPES.EMAIL;
         //        }
 
         //        using (var context = new GoodpackEDIEntities())
         //        {
         //            int userId = (from s in context.Gen_User
         //                          where s.Username == username
         //                          select s.Id).FirstOrDefault();
         //            objGPCustomerMessage.UserId = userId;
         //        }
 
         //        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
         //        objGPCustomerMessage.DateCreated = DateTime.Now;
         //        objGPCustomerMessage.Service = "GPMapper";
         //        objGPCustomerMessage.SubsiId = Convert.ToInt32(subsiId);
         //        objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.SO;
         //        // bool processOneLineOnly = false;
         //        List<int> trnsLineItems = new List<int>();
         //        // mapper = new GPArchitecture.MsgTransformation.GPMapper(objGPCustomerMessage);
         //        GPMapper mapper = new GPMapper();
         //        MapOutput mappingOutput = mapper.ProcessMap(false, mapperFile, objGPCustomerMessage);
 
                
 
 
         //        Gen_User userDetails;
         //        using (var dataContext = new GoodpackEDIEntities())
         //        {
         //            userDetails = dataContext.Gen_User.Where(i => i.Username == username).FirstOrDefault();
         //        }
 
         //        Dictionary<string, string> placeholders = new Dictionary<string, string>();
         //        placeholders.Add("$$CustomerName$$", userDetails.FullName.ToString());
         //        placeholders.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
         //        placeholders.Add("$$UserName$$", userDetails.FullName.ToString());
         //        placeholders.Add("$$DateTime$$", System.DateTime.Now.ToString("dd-MM-yyyy"));
         //        placeholders.Add("$$EmailID$$", objGPCustomerMessage.EmailId);
         //        placeholders.Add("$$FileName$$", objGPCustomerMessage.BatchFileName);
         //        placeholders.Add("$$SubsyName$$", getSubsyNameFromId(objGPCustomerMessage.SubsiId));
         //        placeholders.Add("$$TotalCount$$", objGPCustomerMessage.RecordCountTotal.ToString());
         //        placeholders.Add("$$SuccessCount$$", objGPCustomerMessage.RecordCountSuccess.ToString());
         //        placeholders.Add("$$ErrorCount$$", objGPCustomerMessage.RecordCountSAPError.ToString());
         //        if (objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.EMAIL)
         //        {
         //            //send email to customer and internal users of goodpack
         //            //emailid of customer get from batchfilesource address
 
         //            string[] customerEmail = new string[] { objGPCustomerMessage.EmailId };
         //            List<Attachment> attachments1 = new List<Attachment>();
         //            string strFileSO;
         //            strFileSO = objGPCustomerMessage.BatchFileSourceAddress;
         //            strFileSO += "\\" + objGPCustomerMessage.BatchFileName;
 
         //            Attachment attachment1 = new Attachment(strFileSO);
         //            attachment1.Name = objGPCustomerMessage.BatchFileName;
         //            attachments1.Add(attachment1);
         //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
 
 
         //            GPTools.SendEmail(placeholders, customerEmail, "NOTIFY_CUSTOMER_SO_EMAIL", attachments1);
         //            List<Attachment> attachments2 = new List<Attachment>();
         //            Attachment attachment2 = new Attachment(strFileSO);
         //            attachment2.Name = objGPCustomerMessage.BatchFileName;
         //            attachments2.Add(attachment2);
         //            log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
         //            GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_SO_EMAIL", attachments2);
         //        }
         //        else
         //        {
         //            if (mapperFile == GPArchitecture.Cache.Config.getConfig(ConstantUtilities.GoodyearSOMapper) && objGPCustomerMessage.BatchFileSourceType == SOURCE_TYPES.FILE_SYSTEM)
         //            {
         //                //goodyear notification
         //                List<Attachment> attachments = new List<Attachment>();
         //                string strGoodyearSO;
         //                strGoodyearSO = objGPCustomerMessage.BatchFileSourceAddress;
         //                // strGoodyearSO += "\\"+ objGPCustomerMessage.BatchFileName ;
                        
 
         //                Attachment attachment = new Attachment(strGoodyearSO);
         //                attachment.Name = objGPCustomerMessage.BatchFileName + ".csv";
         //                attachments.Add(attachment);
         //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
         //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_GOODYEAR_SO_CUSTOMER", attachments);
         //            }
         //            else
         //            {
         //                //fileupload SO
 
         //                log.Info("Attachment for email in path" + objGPCustomerMessage.BatchFileSourceAddress);
         //                // GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", attachments);
         //                GPTools.SendEmail(placeholders, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_SO_FILE_UPLOAD", null);
         //            }
         //        }
         //        log.Info("Email send to customer" + userDetails.FullName.ToString());
         //        for (int i = 0; i < genericModel.Count; i++)
         //        {
         //            itemModelData.Add(genericModel[i]);
         //            if (i == 0)
         //            {
         //                itemModelData.Add(itemModelVal);
         //            }
         //        }
         //        log.Info("Leaving from verifyData in SCManager");
         //        return itemModelData;
 
         //    }
         //    catch (Exception exp)
         //    {
 
         //        log.Error(" fileUpload in SCManager for SO Transaction verifyData method.mapperFile:" + mapperFile + ", username:" + username + ", subsiId:" + subsiId + ", transactionType:" + objGPCustomerMessage.TransactionType + ", itemModel:" + itemModel + ". Api Exception : Message- " + exp);
         //        IList<GenericItemModel> genericModel = new List<GenericItemModel>();
         //        GenericItemModel itemsModel = new GenericItemModel();
         //        objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
         //        objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
         //        objGPCustomerMessage.ErrorDesc = exp.Message;
         //        itemsModel.Status = "false";
         //        itemsModel.Message = "Unable to process your request.";
         //        // Update TRANS_BATCH
         //        genericModel.Add(itemsModel);
         //        scTransBatch.UpdateSCBatchFileData(objGPCustomerMessage);
         //        Dictionary<string, string> placeholder = new Dictionary<string, string>();
         //        // placeholder.Add("$$CustomerName$$", "Goodpack Customer");
         //        placeholder.Add("$$BatchID$$", objGPCustomerMessage.BatchID.ToString());
         //        GPTools.SendEmail(placeholder, getAllRecipients(mapperFile), "NOTIFY_CUSTOMER_ERROR", null);
         //        return genericModel;
 
         //    }
 
         //}
         private bool Validate_IsFileTypeSupported(SCTransactionModel objGPCustomerMessage)
         {
             //objILog.Debug("Entering Validate_IsFileTypeSupported method");
             bool blnValidFileType = false;
             try
             {
                 FILE_TYPES fileType;
                 blnValidFileType = this.IsFileTypeSupported(objGPCustomerMessage.BatchFileName, out fileType);
                 if (blnValidFileType == false)
                 {
                     objGPCustomerMessage.ErrorCode = ErrorCodes.V_FILE;
                     objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                     //objILog.Warn("Unsupported file received: " + objGPCustomerMessage.BatchFileName);
                 }
             }
             catch (Exception exp)
             {
                 log.Error(" fileUpload in SCManager Validate_IsFileTypeSupported method.objGPCustomerMessage:" + objGPCustomerMessage + ". Api Exception : Message- " + exp.Message);
                 throw exp;
             }
             //objILog.Debug("Leaving Validate_IsFileTypeSupported method");
             return blnValidFileType;
         }
         private IList<Trn_MapperFieldSpecs> ValidateData(string fileName)
         {
             using (var context = new GoodpackEDIEntities())
             {
                 IList<Trn_MapperFieldSpecs> mapperFields = (from s in context.Trn_SubsiToTransactionFileMapper
                                                             join p in context.Trn_MapperFieldSpecs on s.Id equals p.SubsiToTransactionMapperId
                                                             where (s.Filename == fileName)
                                                             select p).ToList();
 
                 return mapperFields;
             }
 
         }
         private bool IsFileTypeSupported(string strFileName, out FILE_TYPES enumFileType)
         {
             bool blnValidFileType = false;
             //get value from db file types table
             IList<string> lstFileExtns = new List<string>();
             lstFileExtns.Add(".txt");
             lstFileExtns.Add(".csv");
             lstFileExtns.Add(".xls");
             lstFileExtns.Add(".xlsx");
             enumFileType = FILE_TYPES.CSV;
             foreach (string strFileExt in lstFileExtns)
             {
                 if (strFileName.ToLower().EndsWith(strFileExt, StringComparison.CurrentCultureIgnoreCase))
                 {
                     if (strFileExt.ToLower() == ".csv")
                         enumFileType = FILE_TYPES.CSV;
                     else
                         enumFileType = FILE_TYPES.TEXT;
                     blnValidFileType = true;
                     break;
                 }
             }
             return blnValidFileType;
         }
         private string[] SOData(FileInfo fileInfo, out string ObjMessage)
         {
             string scData;
             if (scPath.EndsWith("xls") || scPath.EndsWith("xlsx") || scPath.EndsWith("XLS") || scPath.EndsWith("XLSX"))
             {             
                 scData = ExcelParser.ExcelToCSV(scPath, 0, 0);                
                 scData = GetDataWithoutEmptyColumns(scData);
                 objGPCustomerMessage.Message = scData;
                 ObjMessage = scData;
             }
             else
             {
                 using (System.IO.StreamReader objStreamReader = fileInfo.OpenText())
                 {
                     var line = "";
                     List<String> lines = new List<string>();
                     bool firstItem = true;
                     int indexValue = 0;                    
                     scData = string.Join(Environment.NewLine, lines.ToArray());
                     Regex regData = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)",RegexOptions.Compiled | RegexOptions.IgnoreCase);
                     scData = regData.Replace(scData, "|");                  
                     objGPCustomerMessage.Message = scData;
                     objStreamReader.Close();
                     objStreamReader.Dispose();
                     ObjMessage = scData;
                 }
             }            
             scData = scData.Replace(",", "|");
             return scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
 
         }
         private string CheckForDecodeValueValidation(string fileData, string headers, string mapperName, int subsiId)
         {
             StringBuilder fileDecodedData = new StringBuilder();
 
             string[] fileValues = fileData.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
             string[] headerValues = headers.Split(ConstantUtilities.delimiters, StringSplitOptions.None);
             IDecodeValueManager decodeValueManager = new DecodeValueManager();
             for (int i = 0; i < headerValues.Length; i++)
             {
                 string sapFieldName = headerValues[i];
                 string inputType = String.Empty;
                 int lookupNameId = 0;
                 int transactionId;
                 if (sapFieldName != "")
                 {
                     using (var context = new GoodpackEDIEntities())
                     {
                         transactionId = (from s in context.Trn_SubsiToTransactionFileMapper
                                          where s.Filename == mapperName
                                          select s.TransactionCodeId).FirstOrDefault();
 
                         inputType = (from s in context.Ref_SAPMessageFields
                                      where ((s.SAPFieldName == sapFieldName) && (s.TransactionId == transactionId))
                                      select s.InputType).FirstOrDefault();
                         //  lookupNameId = (from s in context.Ref_LookupNames where s.LookupName == sapFieldName select s.Id).FirstOrDefault();
                     }
                     switch (sapFieldName.ToUpper())
                     {
                         case "CUSTOMER CODE":
                         case "CUSTOMER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             break;
                         case "CUSTOMER NUMBER": lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             break;
                         case "CONSIGNEE": lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                             break;
                         case "PACKER": lookupNameId = GetLookupId(ConstantUtilities.Packer);
                             break;
                         case "PORT": lookupNameId = GetLookupId(ConstantUtilities.Port);
                             break;
                         case "POL": lookupNameId = GetLookupId(ConstantUtilities.POL);
                             break;
                         case "POD": lookupNameId = GetLookupId(ConstantUtilities.POD);
                             break;
                         case "FRM LOCATION":
                         case "FROM LOCATION": if (fileValues[i].StartsWith("3") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.CustomerCode);
                             }
                             else
                             {
                                 if (transactionId == 12)
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                                 }
                                 else
                                 {
                                     lookupNameId = GetLookupId(ConstantUtilities.Packer);
                                 }
                             }
                             break;
                         case "TO LOCATION": if (fileValues[i].StartsWith("5") && fileValues[i].Length == 6)
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Packer);
                             }
                             else
                             {
                                 lookupNameId = GetLookupId(ConstantUtilities.Consignee);
                             }
                             break;
                         case "SHIPPING LINE": lookupNameId = GetLookupId(ConstantUtilities.ShippingLine);
                             break;
                     }
                     TargetMessageInputTypes inputSAPType =
                             (TargetMessageInputTypes)Enum.Parse(typeof(TargetMessageInputTypes), inputType);
 
 
                     if (decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).InputTypeFinder(inputSAPType))
                     {
                         string decodeValue = decodeValueManager.DecodeType(TRANSACTION_TYPES.SC).FindDecodeValue(fileValues[i], mapperName, subsiId, lookupNameId);
                         if (decodeValue != null)
                         {
                             if (decodeValue.Length != 0)
                                 errorDescription.Append(CheckValidation(inputType, mapperName, decodeValue, sapFieldName));
                             else
                                 errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                             fileDecodedData.Append(decodeValue);
 
                         }
                         else
                         {
                             bool lookupName = GetLookupIsNull(sapFieldName.ToUpper(), transactionId);
                             if (!lookupName || !string.IsNullOrEmpty(fileValues[i]))
                             {
                                 errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                                 //if (!string.IsNullOrEmpty(errorDescription.ToString()))                           
                                 fileDecodedData.Append("false");
                             }
                         }
                     }
                     else
                     {
                         errorDescription.Append(CheckValidation(inputType, mapperName, fileValues[i], sapFieldName));
                     }
 
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
                 else
                 {
                     fileDecodedData.Append("" + ConstantUtilities.delimiters[0]);
                 }
             }
             fileDecodedData.Remove(fileDecodedData.Length - 1, 1);
             return fileDecodedData.ToString();
         }
 
         private int GetLookupId(string LookupName)
         {
 
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_LookupNames
                         where s.LookupName == LookupName
                         select s.Id).FirstOrDefault();
 
             }
 
         }
         private string SAPHeaders(string filename)
         {
             StringBuilder sapFields = new StringBuilder();
             using (var context = new GoodpackEDIEntities())
             {
                 int fileMapperId = (from s in context.Trn_SubsiToTransactionFileMapper
                                     where s.Filename == filename
                                     select s.Id).FirstOrDefault();
                 List<Trn_MapperFieldSpecs> specs = (from s in context.Trn_MapperFieldSpecs
                                                     where s.SubsiToTransactionMapperId == fileMapperId
                                                     select s).ToList();
                 Trn_MappingSubsi mapperData = (from s in context.Trn_MappingSubsi
                                                where s.SubsiToTransactionFileMapperId == fileMapperId
                                                select s).FirstOrDefault();
                 foreach (var item in specs)
                 {
                     int sapFieldSequenceId = (from s in context.Trn_MappingConfiguration
                                               where (((s.SourceFieldSequence == item.FieldSequence) || (s.SourceFieldSeq2 == item.FieldSequence)) && (s.MappingSubsiId == mapperData.Id))
                                               select s.SapFieldSeq).FirstOrDefault();
                     string sapField = (from s in context.Ref_SAPMessageFields
                                        where ((s.FieldSequence == sapFieldSequenceId) && (s.TransactionId == mapperData.SapMessageFieldId))
                                        select s.SAPFieldName).FirstOrDefault();
                     sapFields.Append(sapField);
                     sapFields.Append("" + ConstantUtilities.delimiters[0]);
                 }
                 return sapFields.Remove(sapFields.Length - 1, 1).ToString();
             }
         }
 
         private bool GetLookupIsNull(string lookupName, int transactionId)
         {
 
             using (var context = new GoodpackEDIEntities())
             {
                 return (from s in context.Ref_SAPMessageFields
                         where s.SAPFieldName == lookupName && s.TransactionId == transactionId
                         select s.IsAllowNullValue).FirstOrDefault();
 
             }
 
         }
         private string GetDataWithoutEmptyColumns(string scData)
         {
             bool firstItem = true;
             string returnValue = "";
             int indexValue = 0;
             List<String> lines = new List<string>();
             string[] lineArray = scData.Split(new string[] { "\r\n" }, StringSplitOptions.None);
             foreach (string linedata in lineArray)
             {
                 if (linedata != "")
                 {
                     if (firstItem)
                     {
                         if (linedata.LastIndexOf('|') + 1 == linedata.Length)
                         {
                             if (linedata.LastIndexOf("||") + 2 == linedata.Length)
                             {
                                 string tempLine = linedata.Replace("||", "");
                                 string lineValue = (tempLine.LastIndexOf('|') + 1 == tempLine.Length) ? tempLine.Remove(tempLine.LastIndexOf('|')) : tempLine;
                                 returnValue = returnValue + lineValue.Trim() + Environment.NewLine;
                                 indexValue = lineValue.Split('|').Length;
 
                             }
                             else
                             {
                                 returnValue = returnValue + linedata.Remove(linedata.LastIndexOf('|')).Trim() + Environment.NewLine;
                                 indexValue = linedata.Remove(linedata.LastIndexOf('|')).Split('|').Length;
                             }
                             firstItem = false;
                         }
                         else
                         {
                             returnValue = returnValue + linedata.ToString().Trim() + Environment.NewLine;
                             indexValue = linedata.Split('|').Length;
                             firstItem = false;
                         }
                     }
                     else
                     {
                         string[] linedetails;
                         linedetails = linedata.Split('|').Select(e => e.Replace(" \"", "\"")).ToArray();
                         returnValue = returnValue + string.Join("|", linedetails, 0, indexValue) + Environment.NewLine;
                     }
                 }
             }
             return returnValue;
         }
         private string CheckValidation(string inputType, string mapperName, string fileValue, string sapField)
         {
             IValidationTypeManger validator = new ValidationTypeManager();
             Dictionary<string, string> validatorParams = new Dictionary<string, string>();
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.InputType, inputType);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.MapperName, mapperName);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.FileData, fileValue);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.SAPField, sapField);
             validatorParams.Add(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.TransactionType, objGPCustomerMessage.TransactionType.ToString());
             switch (inputType)
             {
                 case GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator:
                     return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.DateValidator).DataValidator(validatorParams);
 
                 default:
                     return validator.ValidationType(GPArchitecture.SAPService.ConstantUtils.ConstantUtilities.CommonValidator).DataValidator(validatorParams);
             }
 
 
         }
     }
 
 }

 

