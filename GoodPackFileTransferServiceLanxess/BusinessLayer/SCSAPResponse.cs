﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.IO;
using GPArchitecture.CommonTypes;

namespace GPArchitecture
{
    [Serializable]
    public class SCSAPResponse : SAPResponse
    {
        #region Private Members
        private string serialNumber;
        private string itrType;
        private string itrNumber;
        private SAPResponseStates status;
        private string responseMessage;
        private string newItrNumber;
        private string newItemNumber;
        private string legacyItrNumber;
        private string legacyItemNumber;
        private string legacyMaterial;
        private string legacyQuantity;
        private string siNumber;
        private long customerCode;
        #endregion

        #region CTORs
        public SCSAPResponse()
        {
            this.siNumber = string.Empty;
            this.serialNumber = string.Empty;
            this.itrType = string.Empty;
            this.itrNumber = string.Empty;
            this.responseMessage = string.Empty;
            this.newItrNumber = string.Empty;
            this.newItemNumber = string.Empty;
            this.siNumber = string.Empty;
            this.customerCode = 0;
        }
        #endregion

        #region Public Properties
        public string SerialNumber { get { return this.serialNumber; } set { this.serialNumber = value; } }
        public string ITRType { get { return this.itrType; } set { this.itrType = value; } }
        public string ITRNumber { get { return this.itrNumber; } set { this.itrNumber = value; } }
        public SAPResponseStates Status { get { return this.status; } set { this.status = value; } }
        public string ResponseMessage { get { return this.responseMessage; } set { this.responseMessage = value;}}
        public string NewITRNumber { get { return this.newItrNumber; } set { this.newItrNumber = value; } }
        public string NewItemNumber { get { return this.newItemNumber; } set { this.newItemNumber = value; } }
        public string LegacyITRNumber { get { return this.legacyItrNumber; } set { this.legacyItrNumber = value; } }
        public string LegacyItemNumber { get { return this.legacyItemNumber; } set { this.legacyItemNumber = value; } }
        public string LegacyMaterial { get { return this.legacyMaterial; } set { this.legacyMaterial = value; } }
        public string LegacyQuantity { get { return this.legacyQuantity; } set { this.legacyQuantity = value; } }
        public string SINumber { get { return this.siNumber; } set { this.siNumber = value; } }
        public long CustomerCode { get { return this.customerCode; } set { this.customerCode = value; } }
        #endregion

        #region Public Status Methods
        public static string BuildCSV(List<SCSAPResponse> scResponses)
        {
            StringBuilder csvContent = new StringBuilder();
            csvContent.Append("\"").Append("Batch Id").Append("\"").Append(",");
            csvContent.Append("\"").Append("Customer Code").Append("\"").Append(",");            
            csvContent.Append("\"").Append("Serial Number").Append("\"").Append(",");
            csvContent.Append("\"").Append("SI Number").Append("\"").Append(",");
            csvContent.Append("\"").Append("ITR Type").Append("\"").Append(",");
            //csvContent.Append("\"").Append("EDI ID").Append("\"").Append(",");
            csvContent.Append("\"").Append("Status").Append("\"").Append(",");
            csvContent.Append("\"").Append("Response Message").Append("\"").Append(",");
            csvContent.Append("\"").Append("ITR Number").Append("\"").Append(",");
            csvContent.Append("\"").Append("Item Number").Append("\"");
            csvContent.Append(Environment.NewLine);
            
            // Validate content list
            if (scResponses != null)
            {
                int listItemCounter = 1;

                foreach (SCSAPResponse scResponse in scResponses)
                {
                    csvContent.Append("\"").Append(scResponse.BatchId).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.CustomerCode).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.SerialNumber.Replace("\"", "\"\"")).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.SINumber.Replace("\"", "\"\"")).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.ITRType.Replace("\"", "\"\"")).Append("\"").Append(",");
                    //csvContent.Append("\"").Append(scResponse.ITRNumber.Replace("\"", "\"\"")).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.status.ToString()).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.ResponseMessage.Replace("\"", "\"\"")).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.NewITRNumber.Replace("\"", "\"\"")).Append("\"").Append(",");
                    csvContent.Append("\"").Append(scResponse.NewItemNumber.Replace("\"", "\"\"")).Append("\"");

                    if (listItemCounter < scResponses.Count)
                    {
                        csvContent.Append(Environment.NewLine);
                    }

                    // Increment counter
                    listItemCounter++;
                }
            }

            return csvContent.ToString();
        }//-- end BuildCSV()
        #endregion
    }
}
