﻿using GPArchitecture.EnumsAndConstants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GPArchitecture
{
    [Serializable]
    public abstract class SAPResponse
    {
        #region Public Members
        public int BatchId;
        public long CustomerCode;
        public int TranLineId;
        public string TranLineSourceReference;
        public string TranLineGPReference;
        public LineStatus TranLineStatus;
        #endregion
    }
}
