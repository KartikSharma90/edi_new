﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace GoodPackFileTransferServiceLanxess
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        internal static ILog log = LogManager.GetLogger(typeof(Program));
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("Entered in Program.cs");
             #if DEBUG

            FileTransferServiceLanxess service = new FileTransferServiceLanxess();
            service.debugger();
            # else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new FileTransferServiceLanxess() 
            };
            ServiceBase.Run(ServicesToRun);
# endif
        }
    }
}
