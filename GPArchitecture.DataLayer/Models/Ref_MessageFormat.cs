//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ref_MessageFormat
    {
        public Ref_MessageFormat()
        {
            this.Trn_SubsiToTransactionFileMapper = new HashSet<Trn_SubsiToTransactionFileMapper>();
            this.Trn_SubsiToTransactionFileMapper_Lanxess = new HashSet<Trn_SubsiToTransactionFileMapper_Lanxess>();
        }
    
        public int Id { get; set; }
        public string MessageFormat { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDropdownDisplay { get; set; }
    
        public virtual ICollection<Trn_SubsiToTransactionFileMapper> Trn_SubsiToTransactionFileMapper { get; set; }
        public virtual ICollection<Trn_SubsiToTransactionFileMapper_Lanxess> Trn_SubsiToTransactionFileMapper_Lanxess { get; set; }
    }
}
