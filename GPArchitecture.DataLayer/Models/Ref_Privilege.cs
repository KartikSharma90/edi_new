//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ref_Privilege
    {
        public Ref_Privilege()
        {
            this.Trn_RoleToPrivilegeMapper = new HashSet<Trn_RoleToPrivilegeMapper>();
        }
    
        public int Id { get; set; }
        public string PrivilegeName { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateLastEdited { get; set; }
        public string EnumPrivilegeName { get; set; }
    
        public virtual ICollection<Trn_RoleToPrivilegeMapper> Trn_RoleToPrivilegeMapper { get; set; }
    }
}
