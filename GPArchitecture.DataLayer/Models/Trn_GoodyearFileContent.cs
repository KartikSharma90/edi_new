//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trn_GoodyearFileContent
    {
        public int LineId { get; set; }
        public string CustPO { get; set; }
        public string SONumber { get; set; }
        public string SINumber { get; set; }
        public string Ver { get; set; }
        public string SIStatus { get; set; }
        public string SIQtyinUnits { get; set; }
        public string RubberGrade { get; set; }
        public string Supplier { get; set; }
        public string FactoryCode { get; set; }
        public string PackagingType { get; set; }
        public string LoadPort { get; set; }
        public string LoadDate { get; set; }
        public string ShippingLine { get; set; }
        public string VesselName { get; set; }
        public string VoyageNumber { get; set; }
        public string BOLNumber { get; set; }
        public string BOLQty { get; set; }
        public string ContainerNumber { get; set; }
        public string ContainerQty { get; set; }
        public string OceanETD { get; set; }
        public string ArrivalPort { get; set; }
        public string ArrivalDate { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string BookingNumber { get; set; }
        public string EndOfRecord { get; set; }
        public string FileName { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    }
}
