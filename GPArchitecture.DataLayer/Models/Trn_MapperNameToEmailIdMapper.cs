//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trn_MapperNameToEmailIdMapper
    {
        public int Id { get; set; }
        public int MapperId { get; set; }
        public string EmailId { get; set; }
        public int TransactionId { get; set; }
    }
}
