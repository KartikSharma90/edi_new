//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trn_Vertical
    {
        public Trn_Vertical()
        {
            this.Trn_DeviceDetails = new HashSet<Trn_DeviceDetails>();
        }
    
        public string Name { get; set; }
        public string Details { get; set; }
    
        public virtual ICollection<Trn_DeviceDetails> Trn_DeviceDetails { get; set; }
    }
}
