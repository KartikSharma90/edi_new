//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gen_FileTransfer
    {
        public int ID { get; set; }
        public string EventType { get; set; }
        public string FileTransferType { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public string FileConversion { get; set; }
        public int SftpID { get; set; }
        public bool IsDeleteFile { get; set; }
    
        public virtual Gen_FTPAccount Gen_FTPAccount { get; set; }
    }
}
