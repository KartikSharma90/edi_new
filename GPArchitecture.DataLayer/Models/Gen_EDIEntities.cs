//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GPArchitecture.DataLayer.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gen_EDIEntities
    {
        public Gen_EDIEntities()
        {
            this.Trn_LookupForGroup = new HashSet<Trn_LookupForGroup>();
        }
    
        public int Id { get; set; }
        public int SubsiId { get; set; }
        public int EntityCode { get; set; }
        public string EntityName { get; set; }
        public string SearchTerm { get; set; }
        public string EmailIdList { get; set; }
        public string ContractNumber { get; set; }
        public System.DateTime ContractStartDate { get; set; }
        public System.DateTime ContractEndDate { get; set; }
        public string Vertical { get; set; }
        public string GoodpackSubsidary { get; set; }
        public string GoodpackSubsidaryEmailIdList { get; set; }
        public bool IsEnabled { get; set; }
        public string SourcePath { get; set; }
        public Nullable<int> EntityTypeId { get; set; }
    
        public virtual Gen_EDISubsies Gen_EDISubsies { get; set; }
        public virtual Ref_EntityTypes Ref_EntityTypes { get; set; }
        public virtual ICollection<Trn_LookupForGroup> Trn_LookupForGroup { get; set; }
    }
}
