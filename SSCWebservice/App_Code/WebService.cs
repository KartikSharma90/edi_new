﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using GPArchitecture.CommonTypes;
using GPArchitecture.WSTypes;
using GPArchitecture.Cache;
using GPArchitecture.Accessors;
using System.Text;
using GPArchitecture.DataLayer.Models;
using GPArchitecture.Models;
using GPArchitecture.EnumsAndConstants;
using GPArchitecture.Utilities;
using GPArchitecture.GoodPackBusinessLayer.PSSCManagementLayer;

//using GoodpackEDI.Utilities;
/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService {
   
    private static volatile object objLockObject = new object();
    private string packerCode;
    private string customerHeadCompany;
    private const string PSSCMapperKey = "PSSCMapperName";
    private const string Delimiter = "|";
    public WebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

     [WebMethod]
    public List<GenericItemModel> SubmitPackerSSCTransaction(PackerSSCInfo[] sscInfoArr)
    {
        string responseMessage = string.Empty;
        GPArchitecture.WSTypes.WSTypeAccessor.packerSSInfoArr = sscInfoArr;
        // create the GP customer/Packer message
        SCTransactionModel objGPCustomerMessage = new SCTransactionModel();
        objGPCustomerMessage.BatchFileSourceType =SOURCE_TYPES.WEB_SERVICE;
        objGPCustomerMessage.BatchFileSourceAddress = "psscwebservice@goodpack.com";
        objGPCustomerMessage.BatchFileName = "through webservice";
        objGPCustomerMessage.TransactionType = TRANSACTION_TYPES.PSSC;
        objGPCustomerMessage.StatusCode = BatchStatus.RCVD;
        objGPCustomerMessage.DateCreated = DateTime.Now;

        // Decode the source customer head company from the packer code

        string mapperName = GetMapperName();
        objGPCustomerMessage.SubsiId = EntityMaster.DeriveSubsiIdFromMapperName(mapperName);
        objGPCustomerMessage.Message = CreateMessageForPSSC();

        objGPCustomerMessage.BatchFileVersion = -1;

        // Get the customer name and code from the source file.  Use the mapper to achieve this.
        GPMapper mapper = null;
        MapOutput mapResult = null;
        List<GenericItemModel> genericModel = new List<GenericItemModel>();
        try
        {
            // Set mapping service
            objGPCustomerMessage.Service = "GPMapper";
            mapper = new GPMapper();
            mapResult = mapper.ProcessMap(true, mapperName, objGPCustomerMessage);
            objGPCustomerMessage.CustomerCode = mapResult.CustomerCode;
            objGPCustomerMessage.CustomerName = mapResult.CustomerName;
            objGPCustomerMessage.UserId = EntityMaster.DeriveUserIdFromMapperName(mapperName);
        }
        catch (Exception e)
        {
            objGPCustomerMessage.ErrorDesc = e.Message;
            objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;
            responseMessage = e.Message;           
            GenericItemModel itemsModel = new GenericItemModel();
            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
            objGPCustomerMessage.ErrorCode = ErrorCodes.E_GEN;
            objGPCustomerMessage.ErrorDesc = e.Message;
            itemsModel.Status = "false";
            itemsModel.Message = "Unable to process your request.";
            // Update TRANS_BATCH
            genericModel.Add(itemsModel);           
        }

        if (objGPCustomerMessage.ErrorCode == ErrorCodes.E_GEN)
        {
            responseMessage = objGPCustomerMessage.ErrorDesc;
            return genericModel;;
        }

        else
        {
            //Insert into TRANS_BATCH Table

            try
            {
                 //PSSCTransactionManager
               PSSCTransactionManager psscManager = new PSSCTransactionManager();
               List<GenericItemModel> genModel=(List<GenericItemModel>) psscManager.DataProcessing(mapResult, objGPCustomerMessage, mapperName);
               //if (genModel.Count != 0) { 
               
               //}
                //InsertBatchTransaction(objGPCustomerMessage);
               return genModel;// = "success";
            }

            catch (Exception e)
            {
                objGPCustomerMessage.ErrorDesc = e.Message;
                objGPCustomerMessage.ErrorCode = ErrorCodes.V_CUST;
                responseMessage = e.Message;
                GenericItemModel itemsModel = new GenericItemModel();
                objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
                itemsModel.Status = "false";
                itemsModel.Message = "Unable to process your request.";              
                genericModel.Add(itemsModel);
                return genericModel;
            }
            // continue with the process         
        }
        // return responseMessage;
    }


    private bool PerformInactiveCustomerOrContractExceptionProcess(SCTransactionModel objGPCustomerMessage, EntitiesModel customer)
    {
        if (customer == null || !customer.IsEnabled)
        {
            objGPCustomerMessage.ErrorDesc = "Customer is disabled in master customer data.";
            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
            objGPCustomerMessage.ErrorCode = ErrorCodes.V_CONTRCT;
            return false;
        }
        else if (customer.ContractEnd.ToUniversalTime() < DateTime.UtcNow)
        {
            objGPCustomerMessage.ErrorDesc = "Customer contract has expired.";
            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
            objGPCustomerMessage.ErrorCode = ErrorCodes.V_CONTRCT;
            return false;
        }
        else if (customer.ContractEnd.ToUniversalTime() > DateTime.UtcNow)
        {
            objGPCustomerMessage.ErrorDesc = "Customer contract will be active in a future date.";
            objGPCustomerMessage.StatusCode = BatchStatus.ERROR;
            objGPCustomerMessage.ErrorCode = ErrorCodes.V_CONTRCT;
            return false;
        }
        else if (!customer.CustomerHeadCompany.IsEnabled)
        {
            objGPCustomerMessage.ErrorDesc = "Customer head company is disabled in master data.";
            objGPCustomerMessage.StatusCode =BatchStatus.ERROR;
            objGPCustomerMessage.ErrorCode = ErrorCodes.V_CONTRCT;
            return false;
        }
        return true;

    }

    private string getPackercode()
    {
        PackerSSCInfo[] sscInfoArr = GPArchitecture.WSTypes.WSTypeAccessor.packerSSInfoArr;
        foreach (GPArchitecture.WSTypes.PackerSSCInfo psscInfo in sscInfoArr)
        {
            packerCode = psscInfo.PackerCode;
            break;
        }

        return packerCode;
    }

    private string CreateMessageForPSSC()
    {
        PackerSSCInfo[] sscInfoArr = GPArchitecture.WSTypes.WSTypeAccessor.packerSSInfoArr;
        StringBuilder messageString = new StringBuilder();
        messageString.Append("\" Bin Number \"|\"Packer Code\"|\"SI Number\"|\"ETA Date\"|\"ETD Date\"|\"Customer Reference Number\"|\"Bin Type\"|\"Destination\"|\"Customer Name\"\n");
        foreach (GPArchitecture.WSTypes.PackerSSCInfo psscInfo in sscInfoArr)
        {
            messageString.Append(psscInfo.BinNumber);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.PackerCode);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.SiNumber);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.ETADate);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.ETDDate);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.custRefNum);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.BinType);
            messageString.Append(Delimiter);

            messageString.Append(psscInfo.Destination);
            messageString.Append(Delimiter);

            psscInfo.CustomerName = customerHeadCompany;
            messageString.Append(psscInfo.CustomerName);
            messageString.Append("\n");

        }
        return messageString.ToString();

    }

    private string GetMapperName()
    {
        using (var context=new GoodpackEDIEntities())
        {
            return (from s in context.Gen_Configuration
                    where s.ConfigItem == PSSCMapperKey
                    select s.Value).FirstOrDefault();
        }
    }
    
}
